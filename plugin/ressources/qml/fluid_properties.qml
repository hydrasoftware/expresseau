<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis version="3.22.8-Białowieża" styleCategories="AllStyleCategories" hasScaleBasedVisibilityFlag="0" maxScale="0" minScale="1e+08" readOnly="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
    <Private>0</Private>
  </flags>
  <temporal endField="" endExpression="" durationField="" durationUnit="min" enabled="0" mode="0" startField="" accumulate="0" limitMode="0" startExpression="" fixedDuration="0">
    <fixedRange>
      <start></start>
      <end></end>
    </fixedRange>
  </temporal>
  <customproperties>
    <Option type="Map">
      <Option type="QString" name="dualview/previewExpressions" value="id"/>
      <Option type="QString" name="embeddedWidgets/count" value="0"/>
      <Option type="invalid" name="variableNames"/>
      <Option type="invalid" name="variableValues"/>
    </Option>
  </customproperties>
  <geometryOptions removeDuplicateNodes="0" geometryPrecision="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <legend type="default-vector" showLabelLegend="0"/>
  <referencedLayers/>
  <fieldConfiguration>
    <field name="id" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option type="Map">
            <Option type="bool" name="AllowNull" value="true"/>
            <Option type="int" name="Max" value="2147483647"/>
            <Option type="int" name="Min" value="-2147483648"/>
            <Option type="int" name="Precision" value="0"/>
            <Option type="int" name="Step" value="1"/>
            <Option type="QString" name="Style" value="SpinBox"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="volumic_mass_kg_m3" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="temperature_c" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="kinematic_viscosity_m2_s" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="volumic_elasticity_module_n_m2" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias field="id" name="" index="0"/>
    <alias field="volumic_mass_kg_m3" name="Volumic mass (kg/m3)" index="1"/>
    <alias field="temperature_c" name="Temperature (°C)" index="2"/>
    <alias field="kinematic_viscosity_m2_s" name="Kinematic viscosity (m²/s)" index="3"/>
    <alias field="volumic_elasticity_module_n_m2" name="Volumic elasticity module (N/m²)" index="4"/>
  </aliases>
  <defaults>
    <default applyOnUpdate="0" field="id" expression=""/>
    <default applyOnUpdate="0" field="volumic_mass_kg_m3" expression=""/>
    <default applyOnUpdate="0" field="temperature_c" expression=""/>
    <default applyOnUpdate="0" field="kinematic_viscosity_m2_s" expression=""/>
    <default applyOnUpdate="0" field="volumic_elasticity_module_n_m2" expression=""/>
  </defaults>
  <constraints>
    <constraint unique_strength="1" notnull_strength="1" exp_strength="0" field="id" constraints="3"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" field="volumic_mass_kg_m3" constraints="0"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" field="temperature_c" constraints="0"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" field="kinematic_viscosity_m2_s" constraints="0"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" field="volumic_elasticity_module_n_m2" constraints="0"/>
  </constraints>
  <constraintExpressions>
    <constraint desc="" field="id" exp=""/>
    <constraint desc="" field="volumic_mass_kg_m3" exp=""/>
    <constraint desc="" field="temperature_c" exp=""/>
    <constraint desc="" field="kinematic_viscosity_m2_s" exp=""/>
    <constraint desc="" field="volumic_elasticity_module_n_m2" exp=""/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction key="Canvas" value="{00000000-0000-0000-0000-000000000000}"/>
  </attributeactions>
  <attributetableconfig sortExpression="" sortOrder="0" actionWidgetStyle="dropDown">
    <columns>
      <column type="field" width="-1" name="volumic_mass_kg_m3" hidden="0"/>
      <column type="field" width="-1" name="temperature_c" hidden="0"/>
      <column type="field" width="-1" name="kinematic_viscosity_m2_s" hidden="0"/>
      <column type="field" width="-1" name="volumic_elasticity_module_n_m2" hidden="0"/>
      <column type="field" width="-1" name="id" hidden="0"/>
      <column type="actions" width="-1" hidden="1"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appelée à l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalités à vos formulaires.

Entrez le nom de la fonction dans le champ "Fonction d'initialisation Python".
Voici un exemple à suivre:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
    geom = feature.geometry()
    control = dialog.findChild(QWidget, "MyLineEdit")

]]></editforminitcode>
  <featformsuppress>2</featformsuppress>
  <editorlayout>tablayout</editorlayout>
  <attributeEditorForm>
    <attributeEditorField name="volumic_mass_kg_m3" index="1" showLabel="1"/>
    <attributeEditorField name="temperature_c" index="2" showLabel="1"/>
    <attributeEditorField name="kinematic_viscosity_m2_s" index="3" showLabel="1"/>
    <attributeEditorField name="volumic_elasticity_module_n_m2" index="4" showLabel="1"/>
  </attributeEditorForm>
  <editable>
    <field name="id" editable="1"/>
    <field name="kinematic_viscosity_m2_s" editable="1"/>
    <field name="temperature_c" editable="1"/>
    <field name="volumic_elasticity_module_n_m2" editable="1"/>
    <field name="volumic_mass_kg_m3" editable="1"/>
  </editable>
  <labelOnTop>
    <field labelOnTop="0" name="id"/>
    <field labelOnTop="0" name="kinematic_viscosity_m2_s"/>
    <field labelOnTop="0" name="temperature_c"/>
    <field labelOnTop="0" name="volumic_elasticity_module_n_m2"/>
    <field labelOnTop="0" name="volumic_mass_kg_m3"/>
  </labelOnTop>
  <reuseLastValue>
    <field name="id" reuseLastValue="0"/>
    <field name="kinematic_viscosity_m2_s" reuseLastValue="0"/>
    <field name="temperature_c" reuseLastValue="0"/>
    <field name="volumic_elasticity_module_n_m2" reuseLastValue="0"/>
    <field name="volumic_mass_kg_m3" reuseLastValue="0"/>
  </reuseLastValue>
  <dataDefinedFieldProperties/>
  <widgets/>
  <previewExpression>"id"</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>4</layerGeometryType>
</qgis>
