<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis simplifyDrawingHints="0" simplifyMaxScale="1" version="3.22.8-Białowieża" simplifyAlgorithm="0" simplifyLocal="1" simplifyDrawingTol="1" styleCategories="AllStyleCategories" hasScaleBasedVisibilityFlag="1" maxScale="0" labelsEnabled="0" minScale="10000" symbologyReferenceScale="-1" readOnly="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
    <Private>0</Private>
  </flags>
  <temporal endField="" endExpression="" durationField="" durationUnit="min" enabled="0" mode="0" startField="" accumulate="0" limitMode="0" startExpression="" fixedDuration="0">
    <fixedRange>
      <start></start>
      <end></end>
    </fixedRange>
  </temporal>
  <renderer-v2 type="singleSymbol" enableorderby="0" referencescale="-1" forceraster="0" symbollevels="0">
    <symbols>
      <symbol type="marker" clip_to_extent="1" alpha="1" name="0" force_rhr="0">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" name="name" value=""/>
            <Option name="properties"/>
            <Option type="QString" name="type" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer locked="0" enabled="1" class="SimpleMarker" pass="0">
          <Option type="Map">
            <Option type="QString" name="angle" value="0"/>
            <Option type="QString" name="cap_style" value="square"/>
            <Option type="QString" name="color" value="0,0,0,255"/>
            <Option type="QString" name="horizontal_anchor_point" value="1"/>
            <Option type="QString" name="joinstyle" value="bevel"/>
            <Option type="QString" name="name" value="circle"/>
            <Option type="QString" name="offset" value="0,0"/>
            <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="offset_unit" value="MM"/>
            <Option type="QString" name="outline_color" value="255,255,255,255"/>
            <Option type="QString" name="outline_style" value="solid"/>
            <Option type="QString" name="outline_width" value="0"/>
            <Option type="QString" name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="outline_width_unit" value="MM"/>
            <Option type="QString" name="scale_method" value="diameter"/>
            <Option type="QString" name="size" value="1.7"/>
            <Option type="QString" name="size_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="size_unit" value="MM"/>
            <Option type="QString" name="vertical_anchor_point" value="1"/>
          </Option>
          <prop v="0" k="angle"/>
          <prop v="square" k="cap_style"/>
          <prop v="0,0,0,255" k="color"/>
          <prop v="1" k="horizontal_anchor_point"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="circle" k="name"/>
          <prop v="0,0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="255,255,255,255" k="outline_color"/>
          <prop v="solid" k="outline_style"/>
          <prop v="0" k="outline_width"/>
          <prop v="3x:0,0,0,0,0,0" k="outline_width_map_unit_scale"/>
          <prop v="MM" k="outline_width_unit"/>
          <prop v="diameter" k="scale_method"/>
          <prop v="1.7" k="size"/>
          <prop v="3x:0,0,0,0,0,0" k="size_map_unit_scale"/>
          <prop v="MM" k="size_unit"/>
          <prop v="1" k="vertical_anchor_point"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option type="Map" name="properties">
                <Option type="Map" name="fillColor">
                  <Option type="bool" name="active" value="true"/>
                  <Option type="QString" name="expression" value="case when &quot;model&quot;=@current_model then '#FF000000' else '#FFAAAAAA' end"/>
                  <Option type="int" name="type" value="3"/>
                </Option>
                <Option type="Map" name="outlineColor">
                  <Option type="bool" name="active" value="false"/>
                  <Option type="QString" name="expression" value="set_color_part(@value, 'alpha', (case when &quot;model&quot;=@current_model then 1 else 0.4 end) * 255 )"/>
                  <Option type="int" name="type" value="3"/>
                </Option>
              </Option>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
    <rotation/>
    <sizescale/>
  </renderer-v2>
  <customproperties>
    <Option type="Map">
      <Option type="QString" name="dualview/previewExpressions" value="&quot;name&quot;"/>
      <Option type="QString" name="embeddedWidgets/count" value="0"/>
      <Option type="invalid" name="variableNames"/>
      <Option type="invalid" name="variableValues"/>
    </Option>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer diagramType="Histogram" attributeLegend="1">
    <DiagramCategory backgroundColor="#ffffff" sizeScale="3x:0,0,0,0,0,0" showAxis="0" spacingUnitScale="3x:0,0,0,0,0,0" minimumSize="0" minScaleDenominator="0" scaleDependency="Area" scaleBasedVisibility="0" opacity="1" lineSizeType="MM" spacing="0" barWidth="5" diagramOrientation="Up" maxScaleDenominator="1e+08" lineSizeScale="3x:0,0,0,0,0,0" rotationOffset="270" width="15" spacingUnit="MM" direction="1" sizeType="MM" height="15" labelPlacementMethod="XHeight" penWidth="0" enabled="0" penAlpha="255" penColor="#000000" backgroundAlpha="255">
      <fontProperties style="" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0"/>
      <attribute field="" color="#000000" label=""/>
      <axisSymbol>
        <symbol type="line" clip_to_extent="1" alpha="1" name="" force_rhr="0">
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
          <layer locked="0" enabled="1" class="SimpleLine" pass="0">
            <Option type="Map">
              <Option type="QString" name="align_dash_pattern" value="0"/>
              <Option type="QString" name="capstyle" value="square"/>
              <Option type="QString" name="customdash" value="5;2"/>
              <Option type="QString" name="customdash_map_unit_scale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="customdash_unit" value="MM"/>
              <Option type="QString" name="dash_pattern_offset" value="0"/>
              <Option type="QString" name="dash_pattern_offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="dash_pattern_offset_unit" value="MM"/>
              <Option type="QString" name="draw_inside_polygon" value="0"/>
              <Option type="QString" name="joinstyle" value="bevel"/>
              <Option type="QString" name="line_color" value="35,35,35,255"/>
              <Option type="QString" name="line_style" value="solid"/>
              <Option type="QString" name="line_width" value="0.26"/>
              <Option type="QString" name="line_width_unit" value="MM"/>
              <Option type="QString" name="offset" value="0"/>
              <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="offset_unit" value="MM"/>
              <Option type="QString" name="ring_filter" value="0"/>
              <Option type="QString" name="trim_distance_end" value="0"/>
              <Option type="QString" name="trim_distance_end_map_unit_scale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="trim_distance_end_unit" value="MM"/>
              <Option type="QString" name="trim_distance_start" value="0"/>
              <Option type="QString" name="trim_distance_start_map_unit_scale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="trim_distance_start_unit" value="MM"/>
              <Option type="QString" name="tweak_dash_pattern_on_corners" value="0"/>
              <Option type="QString" name="use_custom_dash" value="0"/>
              <Option type="QString" name="width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            </Option>
            <prop v="0" k="align_dash_pattern"/>
            <prop v="square" k="capstyle"/>
            <prop v="5;2" k="customdash"/>
            <prop v="3x:0,0,0,0,0,0" k="customdash_map_unit_scale"/>
            <prop v="MM" k="customdash_unit"/>
            <prop v="0" k="dash_pattern_offset"/>
            <prop v="3x:0,0,0,0,0,0" k="dash_pattern_offset_map_unit_scale"/>
            <prop v="MM" k="dash_pattern_offset_unit"/>
            <prop v="0" k="draw_inside_polygon"/>
            <prop v="bevel" k="joinstyle"/>
            <prop v="35,35,35,255" k="line_color"/>
            <prop v="solid" k="line_style"/>
            <prop v="0.26" k="line_width"/>
            <prop v="MM" k="line_width_unit"/>
            <prop v="0" k="offset"/>
            <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
            <prop v="MM" k="offset_unit"/>
            <prop v="0" k="ring_filter"/>
            <prop v="0" k="trim_distance_end"/>
            <prop v="3x:0,0,0,0,0,0" k="trim_distance_end_map_unit_scale"/>
            <prop v="MM" k="trim_distance_end_unit"/>
            <prop v="0" k="trim_distance_start"/>
            <prop v="3x:0,0,0,0,0,0" k="trim_distance_start_map_unit_scale"/>
            <prop v="MM" k="trim_distance_start_unit"/>
            <prop v="0" k="tweak_dash_pattern_on_corners"/>
            <prop v="0" k="use_custom_dash"/>
            <prop v="3x:0,0,0,0,0,0" k="width_map_unit_scale"/>
            <data_defined_properties>
              <Option type="Map">
                <Option type="QString" name="name" value=""/>
                <Option name="properties"/>
                <Option type="QString" name="type" value="collection"/>
              </Option>
            </data_defined_properties>
          </layer>
        </symbol>
      </axisSymbol>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings priority="0" showAll="1" dist="0" zIndex="0" placement="0" linePlacementFlags="18" obstacle="0">
    <properties>
      <Option type="Map">
        <Option type="QString" name="name" value=""/>
        <Option name="properties"/>
        <Option type="QString" name="type" value="collection"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions removeDuplicateNodes="0" geometryPrecision="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <legend type="default-vector" showLabelLegend="0"/>
  <referencedLayers>
    <relation strength="Association" id="User_node_e99cb92c_fa59_4d47_9133_8d8eafe5b0bf_domestic_curve_Hourly_modulation_curve_4639739c_eefd_4961_bdd5_37b97dcd12dd_name" layerName="Hourly modulation curve" name="User node domestic curve" providerKey="postgres" referencedLayer="Hourly_modulation_curve_4639739c_eefd_4961_bdd5_37b97dcd12dd" layerId="Hourly_modulation_curve_4639739c_eefd_4961_bdd5_37b97dcd12dd" referencingLayer="User_node_e99cb92c_fa59_4d47_9133_8d8eafe5b0bf" dataSource="dbname='est110722bis' service='expresseau' sslmode=disable key='name' checkPrimaryKeyUnicity='0' table=&quot;api&quot;.&quot;hourly_modulation_curve&quot;">
      <fieldRef referencingField="domestic_curve" referencedField="name"/>
    </relation>
    <relation strength="Association" id="User_node_e99cb92c_fa59_4d47_9133_8d8eafe5b0bf_industrial_curve_Hourly_modulation_curve_4639739c_eefd_4961_bdd5_37b97dcd12dd_name" layerName="Hourly modulation curve" name="User node industrial curve" providerKey="postgres" referencedLayer="Hourly_modulation_curve_4639739c_eefd_4961_bdd5_37b97dcd12dd" layerId="Hourly_modulation_curve_4639739c_eefd_4961_bdd5_37b97dcd12dd" referencingLayer="User_node_e99cb92c_fa59_4d47_9133_8d8eafe5b0bf" dataSource="dbname='est110722bis' service='expresseau' sslmode=disable key='name' checkPrimaryKeyUnicity='0' table=&quot;api&quot;.&quot;hourly_modulation_curve&quot;">
      <fieldRef referencingField="industrial_curve" referencedField="name"/>
    </relation>
  </referencedLayers>
  <fieldConfiguration>
    <field name="id" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="name" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="domestic_curve" configurationFlags="None">
      <editWidget type="RelationReference">
        <config>
          <Option type="Map">
            <Option type="bool" name="AllowAddFeatures" value="true"/>
            <Option type="bool" name="AllowNULL" value="false"/>
            <Option type="bool" name="MapIdentification" value="false"/>
            <Option type="bool" name="OrderByValue" value="true"/>
            <Option type="bool" name="ReadOnly" value="false"/>
            <Option type="QString" name="ReferencedLayerDataSource" value="dbname='est110722bis' service='expresseau' sslmode=disable key='name' checkPrimaryKeyUnicity='0' table=&quot;api&quot;.&quot;hourly_modulation_curve&quot;"/>
            <Option type="QString" name="ReferencedLayerId" value="Hourly_modulation_curve_4639739c_eefd_4961_bdd5_37b97dcd12dd"/>
            <Option type="QString" name="ReferencedLayerName" value="Hourly modulation curve"/>
            <Option type="QString" name="ReferencedLayerProviderKey" value="postgres"/>
            <Option type="QString" name="Relation" value="User_node_e99cb92c_fa59_4d47_9133_8d8eafe5b0bf_domestic_curve_Hourly_modulation_curve_4639739c_eefd_4961_bdd5_37b97dcd12dd_name"/>
            <Option type="bool" name="ShowForm" value="false"/>
            <Option type="bool" name="ShowOpenFormButton" value="true"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="industrial_curve" configurationFlags="None">
      <editWidget type="RelationReference">
        <config>
          <Option type="Map">
            <Option type="bool" name="AllowAddFeatures" value="true"/>
            <Option type="bool" name="AllowNULL" value="false"/>
            <Option type="bool" name="MapIdentification" value="false"/>
            <Option type="bool" name="OrderByValue" value="true"/>
            <Option type="bool" name="ReadOnly" value="false"/>
            <Option type="QString" name="ReferencedLayerDataSource" value="dbname='est110722bis' service='expresseau' sslmode=disable key='name' checkPrimaryKeyUnicity='0' table=&quot;api&quot;.&quot;hourly_modulation_curve&quot;"/>
            <Option type="QString" name="ReferencedLayerId" value="Hourly_modulation_curve_4639739c_eefd_4961_bdd5_37b97dcd12dd"/>
            <Option type="QString" name="ReferencedLayerName" value="Hourly modulation curve"/>
            <Option type="QString" name="ReferencedLayerProviderKey" value="postgres"/>
            <Option type="QString" name="Relation" value="User_node_e99cb92c_fa59_4d47_9133_8d8eafe5b0bf_industrial_curve_Hourly_modulation_curve_4639739c_eefd_4961_bdd5_37b97dcd12dd_name"/>
            <Option type="bool" name="ShowForm" value="false"/>
            <Option type="bool" name="ShowOpenFormButton" value="true"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="q0" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="fire_hydrant" configurationFlags="None">
      <editWidget type="CheckBox">
        <config>
          <Option type="Map">
            <Option type="QString" name="CheckedState" value=""/>
            <Option type="int" name="TextDisplayMethod" value="0"/>
            <Option type="QString" name="UncheckedState" value=""/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="model" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="zground" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="comment" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="true"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="_sector" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="depth" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias field="id" name="" index="0"/>
    <alias field="name" name="Name" index="1"/>
    <alias field="domestic_curve" name="Domestic modulation curve" index="2"/>
    <alias field="industrial_curve" name="Industrial modulation curve" index="3"/>
    <alias field="q0" name="Q0 (m3/hr)" index="4"/>
    <alias field="fire_hydrant" name="Fire hydrant" index="5"/>
    <alias field="model" name="Model" index="6"/>
    <alias field="zground" name="Z ground (mNGF)" index="7"/>
    <alias field="comment" name="Comment" index="8"/>
    <alias field="_sector" name="Sector" index="9"/>
    <alias field="depth" name="Depth (m)" index="10"/>
  </aliases>
  <defaults>
    <default applyOnUpdate="0" field="id" expression=""/>
    <default applyOnUpdate="0" field="name" expression=""/>
    <default applyOnUpdate="0" field="domestic_curve" expression=""/>
    <default applyOnUpdate="0" field="industrial_curve" expression=""/>
    <default applyOnUpdate="0" field="q0" expression=""/>
    <default applyOnUpdate="0" field="fire_hydrant" expression=""/>
    <default applyOnUpdate="0" field="model" expression="@current_model"/>
    <default applyOnUpdate="0" field="zground" expression=""/>
    <default applyOnUpdate="0" field="comment" expression="''"/>
    <default applyOnUpdate="0" field="_sector" expression="array_first(&#xd;&#xa;&#x9;overlay_intersects(&#xd;&#xa;&#x9;&#x9;layer:='Water_delivery_sector_82089ab5_c8f4_4177_b0c7_86365f1ec364',&#xd;&#xa;&#x9;&#x9;expression:=&quot;name&quot;,&#xd;&#xa;&#x9;&#x9;limit:=1&#xd;&#xa;&#x9;)&#xd;&#xa;)&#xd;&#xa;"/>
    <default applyOnUpdate="0" field="depth" expression=""/>
  </defaults>
  <constraints>
    <constraint unique_strength="1" notnull_strength="1" exp_strength="0" field="id" constraints="3"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" field="name" constraints="0"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" field="domestic_curve" constraints="0"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" field="industrial_curve" constraints="0"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" field="q0" constraints="0"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" field="fire_hydrant" constraints="0"/>
    <constraint unique_strength="0" notnull_strength="1" exp_strength="0" field="model" constraints="1"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" field="zground" constraints="0"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" field="comment" constraints="0"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" field="_sector" constraints="0"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" field="depth" constraints="0"/>
  </constraints>
  <constraintExpressions>
    <constraint desc="" field="id" exp=""/>
    <constraint desc="" field="name" exp=""/>
    <constraint desc="" field="domestic_curve" exp=""/>
    <constraint desc="" field="industrial_curve" exp=""/>
    <constraint desc="" field="q0" exp=""/>
    <constraint desc="" field="fire_hydrant" exp=""/>
    <constraint desc="" field="model" exp=""/>
    <constraint desc="" field="zground" exp=""/>
    <constraint desc="" field="comment" exp=""/>
    <constraint desc="" field="_sector" exp=""/>
    <constraint desc="" field="depth" exp=""/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction key="Canvas" value="{00000000-0000-0000-0000-000000000000}"/>
    <actionsetting type="1" shortTitle="" isEnabledOnlyWhenEditable="0" capture="0" id="{1d802c42-9811-4cb5-89a9-2330b486f416}" name="Open results" icon="" notificationMessage="" action="from expresseau.gui.result_dialog import ResultWindow&#xa;&#xa;if 'result_window' in locals() and result_window.isVisible():&#xa;    result_window.set_result([%id%],&#xa;                    '[% decode_uri(@layer, 'table')%]',&#xa;                    '[% @current_scenario%]')&#xa;    result_window.hide()&#xa;    result_window.show()&#xa;else:&#xa;    result_window = ResultWindow([%id%],&#xa;                    '[% decode_uri(@layer, 'table')%]',&#xa;                    '[% @current_scenario%]',&#xa;                    '[% @project_basename%]')&#xa;    result_window.open()&#xa;">
      <actionScope id="Feature"/>
      <actionScope id="Layer"/>
    </actionsetting>
  </attributeactions>
  <attributetableconfig sortExpression="&quot;name&quot;" sortOrder="0" actionWidgetStyle="dropDown">
    <columns>
      <column type="field" width="-1" name="name" hidden="0"/>
      <column type="actions" width="-1" hidden="1"/>
      <column type="field" width="-1" name="comment" hidden="0"/>
      <column type="field" width="-1" name="zground" hidden="0"/>
      <column type="field" width="-1" name="depth" hidden="0"/>
      <column type="field" width="-1" name="q0" hidden="0"/>
      <column type="field" width="-1" name="fire_hydrant" hidden="0"/>
      <column type="field" width="-1" name="_sector" hidden="0"/>
      <column type="field" width="-1" name="model" hidden="0"/>
      <column type="field" width="204" name="domestic_curve" hidden="0"/>
      <column type="field" width="169" name="industrial_curve" hidden="0"/>
      <column type="field" width="-1" name="id" hidden="0"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1">/../../.expresseau/Documents</editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath>/../../.expresseau/Documents</editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appelée à l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalités à vos formulaires.

Entrez le nom de la fonction dans le champ
"Fonction d'initialisation Python"
Voici un exemple à suivre:
"""
from PyQt4.QtGui import QWidget

def my_form_open(dialog, layer, feature):
⇥geom = feature.geometry()
⇥control = dialog.findChild(QWidget, "MyLineEdit")
]]></editforminitcode>
  <featformsuppress>2</featformsuppress>
  <editorlayout>tablayout</editorlayout>
  <attributeEditorForm>
    <attributeEditorField name="name" index="1" showLabel="1"/>
    <attributeEditorField name="model" index="6" showLabel="1"/>
    <attributeEditorField name="comment" index="8" showLabel="1"/>
    <attributeEditorField name="zground" index="7" showLabel="1"/>
    <attributeEditorField name="depth" index="10" showLabel="1"/>
    <attributeEditorField name="q0" index="4" showLabel="1"/>
    <attributeEditorField name="fire_hydrant" index="5" showLabel="1"/>
    <attributeEditorContainer visibilityExpressionEnabled="1" visibilityExpression="&quot;_sector&quot; is not null" name="Domestic consommation" columnCount="2" groupBox="1" showLabel="1">
      <attributeEditorField name="domestic_curve" index="2" showLabel="1"/>
      <attributeEditorField name="domestic_volume" index="-1" showLabel="1"/>
      <attributeEditorField name="_sector" index="9" showLabel="1"/>
      <attributeEditorField name="_domestic_weight_in_sector" index="-1" showLabel="1"/>
    </attributeEditorContainer>
    <attributeEditorContainer visibilityExpressionEnabled="1" visibilityExpression="&quot;_sector&quot; is not null" name="Industrial consommation" columnCount="2" groupBox="1" showLabel="1">
      <attributeEditorField name="industrial_curve" index="3" showLabel="1"/>
      <attributeEditorField name="industrial_volume" index="-1" showLabel="1"/>
    </attributeEditorContainer>
  </attributeEditorForm>
  <editable>
    <field name="_domestic_weight_in_sector" editable="0"/>
    <field name="_id" editable="0"/>
    <field name="_sector" editable="0"/>
    <field name="comment" editable="1"/>
    <field name="configuration" editable="1"/>
    <field name="configuration_json" editable="1"/>
    <field name="contribution_coef" editable="1"/>
    <field name="def_by_sector" editable="1"/>
    <field name="depth" editable="1"/>
    <field name="domestic_curve" editable="1"/>
    <field name="domestic_volume" editable="1"/>
    <field name="excluded" editable="1"/>
    <field name="fire_hydrant" editable="1"/>
    <field name="id" editable="1"/>
    <field name="industrial_curve" editable="1"/>
    <field name="industrial_volume" editable="1"/>
    <field name="model" editable="1"/>
    <field name="name" editable="1"/>
    <field name="q0" editable="1"/>
    <field name="validity" editable="1"/>
    <field name="zground" editable="1"/>
  </editable>
  <labelOnTop>
    <field labelOnTop="0" name="_domestic_weight_in_sector"/>
    <field labelOnTop="0" name="_id"/>
    <field labelOnTop="0" name="_sector"/>
    <field labelOnTop="0" name="comment"/>
    <field labelOnTop="0" name="configuration"/>
    <field labelOnTop="0" name="configuration_json"/>
    <field labelOnTop="0" name="contribution_coef"/>
    <field labelOnTop="0" name="def_by_sector"/>
    <field labelOnTop="0" name="depth"/>
    <field labelOnTop="0" name="domestic_curve"/>
    <field labelOnTop="0" name="domestic_volume"/>
    <field labelOnTop="0" name="excluded"/>
    <field labelOnTop="0" name="fire_hydrant"/>
    <field labelOnTop="0" name="id"/>
    <field labelOnTop="0" name="industrial_curve"/>
    <field labelOnTop="0" name="industrial_volume"/>
    <field labelOnTop="0" name="model"/>
    <field labelOnTop="0" name="name"/>
    <field labelOnTop="0" name="q0"/>
    <field labelOnTop="0" name="validity"/>
    <field labelOnTop="0" name="zground"/>
  </labelOnTop>
  <reuseLastValue>
    <field name="_sector" reuseLastValue="0"/>
    <field name="comment" reuseLastValue="0"/>
    <field name="depth" reuseLastValue="0"/>
    <field name="domestic_curve" reuseLastValue="0"/>
    <field name="fire_hydrant" reuseLastValue="0"/>
    <field name="id" reuseLastValue="0"/>
    <field name="industrial_curve" reuseLastValue="0"/>
    <field name="model" reuseLastValue="0"/>
    <field name="name" reuseLastValue="0"/>
    <field name="q0" reuseLastValue="0"/>
    <field name="zground" reuseLastValue="0"/>
  </reuseLastValue>
  <dataDefinedFieldProperties/>
  <widgets/>
  <previewExpression>"name"</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>0</layerGeometryType>
</qgis>
