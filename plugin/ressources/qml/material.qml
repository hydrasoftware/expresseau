<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis version="3.22.8-Białowieża" styleCategories="AllStyleCategories" hasScaleBasedVisibilityFlag="0" maxScale="0" minScale="1e+08" readOnly="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
    <Private>0</Private>
  </flags>
  <temporal endField="" endExpression="" durationField="" durationUnit="min" enabled="0" mode="0" startField="" accumulate="0" limitMode="0" startExpression="" fixedDuration="0">
    <fixedRange>
      <start></start>
      <end></end>
    </fixedRange>
  </temporal>
  <customproperties>
    <Option type="Map">
      <Option type="int" name="embeddedWidgets/count" value="0"/>
      <Option type="invalid" name="variableNames"/>
      <Option type="invalid" name="variableValues"/>
    </Option>
  </customproperties>
  <geometryOptions removeDuplicateNodes="0" geometryPrecision="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <legend type="default-vector" showLabelLegend="0"/>
  <referencedLayers/>
  <fieldConfiguration>
    <field name="name" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="roughness_mm" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="elasticity_n_m2" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias field="name" name="Name" index="0"/>
    <alias field="roughness_mm" name="Roughness (mm)" index="1"/>
    <alias field="elasticity_n_m2" name="Elasticity (N/m²)" index="2"/>
  </aliases>
  <defaults>
    <default applyOnUpdate="0" field="name" expression=""/>
    <default applyOnUpdate="0" field="roughness_mm" expression=""/>
    <default applyOnUpdate="0" field="elasticity_n_m2" expression=""/>
  </defaults>
  <constraints>
    <constraint unique_strength="1" notnull_strength="1" exp_strength="0" field="name" constraints="3"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" field="roughness_mm" constraints="0"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" field="elasticity_n_m2" constraints="0"/>
  </constraints>
  <constraintExpressions>
    <constraint desc="" field="name" exp=""/>
    <constraint desc="" field="roughness_mm" exp=""/>
    <constraint desc="" field="elasticity_n_m2" exp=""/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction key="Canvas" value="{00000000-0000-0000-0000-000000000000}"/>
  </attributeactions>
  <attributetableconfig sortExpression="" sortOrder="0" actionWidgetStyle="dropDown">
    <columns>
      <column type="field" width="-1" name="name" hidden="0"/>
      <column type="field" width="-1" name="roughness_mm" hidden="0"/>
      <column type="field" width="-1" name="elasticity_n_m2" hidden="0"/>
      <column type="actions" width="-1" hidden="1"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appelée à l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalités à vos formulaires.

Entrez le nom de la fonction dans le champ "Fonction d'initialisation Python".
Voici un exemple à suivre:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
	geom = feature.geometry()
	control = dialog.findChild(QWidget, "MyLineEdit")

]]></editforminitcode>
  <featformsuppress>2</featformsuppress>
  <editorlayout>tablayout</editorlayout>
  <attributeEditorForm>
    <attributeEditorField name="name" index="0" showLabel="1"/>
    <attributeEditorField name="roughness_mm" index="1" showLabel="1"/>
    <attributeEditorField name="elasticity_n_m2" index="2" showLabel="1"/>
  </attributeEditorForm>
  <editable>
    <field name="elasticity_n_m2" editable="1"/>
    <field name="name" editable="1"/>
    <field name="roughness_mm" editable="1"/>
  </editable>
  <labelOnTop>
    <field labelOnTop="0" name="elasticity_n_m2"/>
    <field labelOnTop="0" name="name"/>
    <field labelOnTop="0" name="roughness_mm"/>
  </labelOnTop>
  <reuseLastValue>
    <field name="elasticity_n_m2" reuseLastValue="0"/>
    <field name="name" reuseLastValue="0"/>
    <field name="roughness_mm" reuseLastValue="0"/>
  </reuseLastValue>
  <dataDefinedFieldProperties/>
  <widgets>
    <widget name="pipe_link__material_material_7_name">
      <config type="Map">
        <Option type="QString" name="nm-rel" value=""/>
      </config>
    </widget>
  </widgets>
  <previewExpression>name</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>4</layerGeometryType>
</qgis>
