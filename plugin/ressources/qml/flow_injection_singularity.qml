<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis simplifyLocal="1" hasScaleBasedVisibilityFlag="0" minScale="100000000" simplifyMaxScale="1" version="3.22.16-Białowieża" simplifyAlgorithm="0" labelsEnabled="0" simplifyDrawingHints="0" styleCategories="AllStyleCategories" symbologyReferenceScale="-1" simplifyDrawingTol="1" maxScale="0" readOnly="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
    <Private>0</Private>
  </flags>
  <temporal limitMode="0" enabled="0" durationField="id" fixedDuration="0" endExpression="" startField="" startExpression="" mode="0" endField="" durationUnit="min" accumulate="0">
    <fixedRange>
      <start></start>
      <end></end>
    </fixedRange>
  </temporal>
  <renderer-v2 type="singleSymbol" enableorderby="0" referencescale="-1" symbollevels="0" forceraster="0">
    <symbols>
      <symbol force_rhr="0" type="marker" clip_to_extent="1" name="0" alpha="1">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" value="" name="name"/>
            <Option name="properties"/>
            <Option type="QString" value="collection" name="type"/>
          </Option>
        </data_defined_properties>
        <layer locked="0" enabled="1" class="SimpleMarker" pass="0">
          <Option type="Map">
            <Option type="QString" value="0" name="angle"/>
            <Option type="QString" value="square" name="cap_style"/>
            <Option type="QString" value="255,255,255,255" name="color"/>
            <Option type="QString" value="1" name="horizontal_anchor_point"/>
            <Option type="QString" value="bevel" name="joinstyle"/>
            <Option type="QString" value="square" name="name"/>
            <Option type="QString" value="0,0" name="offset"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
            <Option type="QString" value="MM" name="offset_unit"/>
            <Option type="QString" value="35,35,35,255" name="outline_color"/>
            <Option type="QString" value="solid" name="outline_style"/>
            <Option type="QString" value="0" name="outline_width"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale"/>
            <Option type="QString" value="MM" name="outline_width_unit"/>
            <Option type="QString" value="diameter" name="scale_method"/>
            <Option type="QString" value="3" name="size"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="size_map_unit_scale"/>
            <Option type="QString" value="MM" name="size_unit"/>
            <Option type="QString" value="1" name="vertical_anchor_point"/>
          </Option>
          <prop v="0" k="angle"/>
          <prop v="square" k="cap_style"/>
          <prop v="255,255,255,255" k="color"/>
          <prop v="1" k="horizontal_anchor_point"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="square" k="name"/>
          <prop v="0,0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="35,35,35,255" k="outline_color"/>
          <prop v="solid" k="outline_style"/>
          <prop v="0" k="outline_width"/>
          <prop v="3x:0,0,0,0,0,0" k="outline_width_map_unit_scale"/>
          <prop v="MM" k="outline_width_unit"/>
          <prop v="diameter" k="scale_method"/>
          <prop v="3" k="size"/>
          <prop v="3x:0,0,0,0,0,0" k="size_map_unit_scale"/>
          <prop v="MM" k="size_unit"/>
          <prop v="1" k="vertical_anchor_point"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option type="Map" name="properties">
                <Option type="Map" name="outlineColor">
                  <Option type="bool" value="true" name="active"/>
                  <Option type="QString" value="case when &quot;_model&quot;=@current_model then '#FF000000' else '#FFAAAAAA' end" name="expression"/>
                  <Option type="int" value="3" name="type"/>
                </Option>
              </Option>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
        <layer locked="0" enabled="1" class="SimpleMarker" pass="0">
          <Option type="Map">
            <Option type="QString" value="180" name="angle"/>
            <Option type="QString" value="square" name="cap_style"/>
            <Option type="QString" value="255,127,0,255" name="color"/>
            <Option type="QString" value="1" name="horizontal_anchor_point"/>
            <Option type="QString" value="bevel" name="joinstyle"/>
            <Option type="QString" value="circle" name="name"/>
            <Option type="QString" value="0,0" name="offset"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
            <Option type="QString" value="MM" name="offset_unit"/>
            <Option type="QString" value="0,0,0,255" name="outline_color"/>
            <Option type="QString" value="solid" name="outline_style"/>
            <Option type="QString" value="0" name="outline_width"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale"/>
            <Option type="QString" value="MM" name="outline_width_unit"/>
            <Option type="QString" value="diameter" name="scale_method"/>
            <Option type="QString" value="2" name="size"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="size_map_unit_scale"/>
            <Option type="QString" value="MM" name="size_unit"/>
            <Option type="QString" value="1" name="vertical_anchor_point"/>
          </Option>
          <prop v="180" k="angle"/>
          <prop v="square" k="cap_style"/>
          <prop v="255,127,0,255" k="color"/>
          <prop v="1" k="horizontal_anchor_point"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="circle" k="name"/>
          <prop v="0,0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="0,0,0,255" k="outline_color"/>
          <prop v="solid" k="outline_style"/>
          <prop v="0" k="outline_width"/>
          <prop v="3x:0,0,0,0,0,0" k="outline_width_map_unit_scale"/>
          <prop v="MM" k="outline_width_unit"/>
          <prop v="diameter" k="scale_method"/>
          <prop v="2" k="size"/>
          <prop v="3x:0,0,0,0,0,0" k="size_map_unit_scale"/>
          <prop v="MM" k="size_unit"/>
          <prop v="1" k="vertical_anchor_point"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option type="Map" name="properties">
                <Option type="Map" name="fillColor">
                  <Option type="bool" value="true" name="active"/>
                  <Option type="QString" value="case when &quot;_model&quot;=@current_model then '#FF8C00' else '#ffdda9' end" name="expression"/>
                  <Option type="int" value="3" name="type"/>
                </Option>
                <Option type="Map" name="outlineColor">
                  <Option type="bool" value="true" name="active"/>
                  <Option type="QString" value="case when &quot;_model&quot;=@current_model then '#FF000000' else '#FFAAAAAA' end" name="expression"/>
                  <Option type="int" value="3" name="type"/>
                </Option>
              </Option>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
    <rotation/>
    <sizescale/>
  </renderer-v2>
  <customproperties>
    <Option type="Map">
      <Option type="QString" value="&quot;name&quot;" name="dualview/previewExpressions"/>
      <Option type="QString" value="0" name="embeddedWidgets/count"/>
      <Option name="variableNames"/>
      <Option name="variableValues"/>
    </Option>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer attributeLegend="1" diagramType="Histogram">
    <DiagramCategory penColor="#000000" maxScaleDenominator="1e+08" spacingUnit="MM" sizeType="MM" scaleBasedVisibility="0" rotationOffset="270" showAxis="0" penAlpha="255" minimumSize="0" spacingUnitScale="3x:0,0,0,0,0,0" minScaleDenominator="0" barWidth="5" enabled="0" diagramOrientation="Up" labelPlacementMethod="XHeight" height="15" backgroundAlpha="255" backgroundColor="#ffffff" scaleDependency="Area" opacity="1" lineSizeScale="3x:0,0,0,0,0,0" width="15" sizeScale="3x:0,0,0,0,0,0" direction="1" spacing="0" penWidth="0" lineSizeType="MM">
      <fontProperties style="" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0"/>
      <attribute field="" color="#000000" colorOpacity="1" label=""/>
      <axisSymbol>
        <symbol force_rhr="0" type="line" clip_to_extent="1" name="" alpha="1">
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
          <layer locked="0" enabled="1" class="SimpleLine" pass="0">
            <Option type="Map">
              <Option type="QString" value="0" name="align_dash_pattern"/>
              <Option type="QString" value="square" name="capstyle"/>
              <Option type="QString" value="5;2" name="customdash"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="customdash_map_unit_scale"/>
              <Option type="QString" value="MM" name="customdash_unit"/>
              <Option type="QString" value="0" name="dash_pattern_offset"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="dash_pattern_offset_map_unit_scale"/>
              <Option type="QString" value="MM" name="dash_pattern_offset_unit"/>
              <Option type="QString" value="0" name="draw_inside_polygon"/>
              <Option type="QString" value="bevel" name="joinstyle"/>
              <Option type="QString" value="35,35,35,255" name="line_color"/>
              <Option type="QString" value="solid" name="line_style"/>
              <Option type="QString" value="0.26" name="line_width"/>
              <Option type="QString" value="MM" name="line_width_unit"/>
              <Option type="QString" value="0" name="offset"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
              <Option type="QString" value="MM" name="offset_unit"/>
              <Option type="QString" value="0" name="ring_filter"/>
              <Option type="QString" value="0" name="trim_distance_end"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="trim_distance_end_map_unit_scale"/>
              <Option type="QString" value="MM" name="trim_distance_end_unit"/>
              <Option type="QString" value="0" name="trim_distance_start"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="trim_distance_start_map_unit_scale"/>
              <Option type="QString" value="MM" name="trim_distance_start_unit"/>
              <Option type="QString" value="0" name="tweak_dash_pattern_on_corners"/>
              <Option type="QString" value="0" name="use_custom_dash"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="width_map_unit_scale"/>
            </Option>
            <prop v="0" k="align_dash_pattern"/>
            <prop v="square" k="capstyle"/>
            <prop v="5;2" k="customdash"/>
            <prop v="3x:0,0,0,0,0,0" k="customdash_map_unit_scale"/>
            <prop v="MM" k="customdash_unit"/>
            <prop v="0" k="dash_pattern_offset"/>
            <prop v="3x:0,0,0,0,0,0" k="dash_pattern_offset_map_unit_scale"/>
            <prop v="MM" k="dash_pattern_offset_unit"/>
            <prop v="0" k="draw_inside_polygon"/>
            <prop v="bevel" k="joinstyle"/>
            <prop v="35,35,35,255" k="line_color"/>
            <prop v="solid" k="line_style"/>
            <prop v="0.26" k="line_width"/>
            <prop v="MM" k="line_width_unit"/>
            <prop v="0" k="offset"/>
            <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
            <prop v="MM" k="offset_unit"/>
            <prop v="0" k="ring_filter"/>
            <prop v="0" k="trim_distance_end"/>
            <prop v="3x:0,0,0,0,0,0" k="trim_distance_end_map_unit_scale"/>
            <prop v="MM" k="trim_distance_end_unit"/>
            <prop v="0" k="trim_distance_start"/>
            <prop v="3x:0,0,0,0,0,0" k="trim_distance_start_map_unit_scale"/>
            <prop v="MM" k="trim_distance_start_unit"/>
            <prop v="0" k="tweak_dash_pattern_on_corners"/>
            <prop v="0" k="use_custom_dash"/>
            <prop v="3x:0,0,0,0,0,0" k="width_map_unit_scale"/>
            <data_defined_properties>
              <Option type="Map">
                <Option type="QString" value="" name="name"/>
                <Option name="properties"/>
                <Option type="QString" value="collection" name="type"/>
              </Option>
            </data_defined_properties>
          </layer>
        </symbol>
      </axisSymbol>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings obstacle="0" dist="0" zIndex="0" placement="0" linePlacementFlags="18" priority="0" showAll="1">
    <properties>
      <Option type="Map">
        <Option type="QString" value="" name="name"/>
        <Option name="properties"/>
        <Option type="QString" value="collection" name="type"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions geometryPrecision="0" removeDuplicateNodes="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <legend showLabelLegend="0" type="default-vector"/>
  <referencedLayers>
    <relation layerName="All nodes" dataSource="dbname='sem_2023_03_21_11_mars' service='expresseau' sslmode=disable key='id' checkPrimaryKeyUnicity='0' table=&quot;api&quot;.&quot;node&quot; (geom)" id="Flow_injection_5f015749_1161_4ecb_a00e_12b499f0815a_id_All_nodes_02c70dcd_db1e_496d_9eb3_78b70bccb6fe_id" providerKey="postgres" referencingLayer="Flow_injection_5f015749_1161_4ecb_a00e_12b499f0815a" strength="Association" layerId="All_nodes_02c70dcd_db1e_496d_9eb3_78b70bccb6fe" name="Flow injection id" referencedLayer="All_nodes_02c70dcd_db1e_496d_9eb3_78b70bccb6fe">
      <fieldRef referencedField="id" referencingField="id"/>
    </relation>
  </referencedLayers>
  <fieldConfiguration>
    <field name="id" configurationFlags="None">
      <editWidget type="RelationReference">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="AllowAddFeatures"/>
            <Option type="bool" value="false" name="AllowNULL"/>
            <Option type="bool" value="true" name="MapIdentification"/>
            <Option type="bool" value="false" name="OrderByValue"/>
            <Option type="bool" value="true" name="ReadOnly"/>
            <Option type="invalid" name="Relation"/>
            <Option type="bool" value="false" name="ShowForm"/>
            <Option type="bool" value="false" name="ShowOpenFormButton"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="name" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="q0" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="external_file" configurationFlags="None">
      <editWidget type="CheckBox">
        <config>
          <Option type="Map">
            <Option type="QString" value="" name="CheckedState"/>
            <Option type="int" value="0" name="TextDisplayMethod"/>
            <Option type="QString" value="" name="UncheckedState"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="cyclic" configurationFlags="None">
      <editWidget type="CheckBox">
        <config>
          <Option type="Map">
            <Option type="QString" value="" name="CheckedState"/>
            <Option type="int" value="0" name="TextDisplayMethod"/>
            <Option type="QString" value="" name="UncheckedState"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="chlorine" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="trihalomethane" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="passive_tracer" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="travel_time" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="contact_time" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="comment" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="true" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="_model" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias field="id" name="Node" index="0"/>
    <alias field="name" name="Name" index="1"/>
    <alias field="q0" name="Q injection (m3/hr)" index="2"/>
    <alias field="external_file" name="External file" index="3"/>
    <alias field="cyclic" name="Cyclic" index="4"/>
    <alias field="chlorine" name="Chlorine (mg/l)" index="5"/>
    <alias field="trihalomethane" name="Trihalomethane (mg/l)" index="6"/>
    <alias field="passive_tracer" name="Passive tracer (mg/l)" index="7"/>
    <alias field="travel_time" name="Travel time(h)" index="8"/>
    <alias field="contact_time" name="Contact time (h)" index="9"/>
    <alias field="comment" name="Comment" index="10"/>
    <alias field="_model" name="Model" index="11"/>
  </aliases>
  <defaults>
    <default expression="attribute(&#xd;&#xa;&#x9;array_first(&#xd;&#xa;&#x9;&#x9;array_filter(&#xd;&#xa;&#x9;&#x9;&#x9;overlay_intersects(&#xd;&#xa;&#x9;&#x9;&#x9;&#x9;&#x9;layer:='All_nodes_02c70dcd_db1e_496d_9eb3_78b70bccb6fe',&#xd;&#xa;&#x9;&#x9;&#x9;&#x9;&#x9;expression:=$currentfeature&#xd;&#xa;&#x9;&#x9;&#x9;),&#xd;&#xa;&#x9;&#x9;&#x9;intersects($geometry, geometry(@element))&#xd;&#xa;&#x9;&#x9;)&#xd;&#xa;&#x9;),&#xd;&#xa;&#x9;'id'&#xd;&#xa;)" field="id" applyOnUpdate="1"/>
    <default expression="" field="name" applyOnUpdate="0"/>
    <default expression="" field="q0" applyOnUpdate="0"/>
    <default expression="" field="external_file" applyOnUpdate="0"/>
    <default expression="" field="cyclic" applyOnUpdate="0"/>
    <default expression="" field="chlorine" applyOnUpdate="0"/>
    <default expression="" field="trihalomethane" applyOnUpdate="0"/>
    <default expression="" field="passive_tracer" applyOnUpdate="0"/>
    <default expression="" field="travel_time" applyOnUpdate="0"/>
    <default expression="" field="contact_time" applyOnUpdate="0"/>
    <default expression="" field="comment" applyOnUpdate="0"/>
    <default expression="attribute(&#xd;&#xa;&#x9;array_first(&#xd;&#xa;&#x9;&#x9;array_filter(&#xd;&#xa;&#x9;&#x9;&#x9;overlay_intersects(&#xd;&#xa;&#x9;&#x9;&#x9;&#x9;&#x9;layer:='All_nodes_02c70dcd_db1e_496d_9eb3_78b70bccb6fe',&#xd;&#xa;&#x9;&#x9;&#x9;&#x9;&#x9;expression:=$currentfeature&#xd;&#xa;&#x9;&#x9;&#x9;),&#xd;&#xa;&#x9;&#x9;&#x9;intersects($geometry, geometry(@element))&#xd;&#xa;&#x9;&#x9;)&#xd;&#xa;&#x9;),&#xd;&#xa;&#x9;'model'&#xd;&#xa;)" field="_model" applyOnUpdate="0"/>
  </defaults>
  <constraints>
    <constraint field="id" unique_strength="1" notnull_strength="1" exp_strength="0" constraints="3"/>
    <constraint field="name" unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0"/>
    <constraint field="q0" unique_strength="0" notnull_strength="1" exp_strength="0" constraints="1"/>
    <constraint field="external_file" unique_strength="0" notnull_strength="1" exp_strength="0" constraints="1"/>
    <constraint field="cyclic" unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0"/>
    <constraint field="chlorine" unique_strength="0" notnull_strength="1" exp_strength="1" constraints="5"/>
    <constraint field="trihalomethane" unique_strength="0" notnull_strength="1" exp_strength="1" constraints="5"/>
    <constraint field="passive_tracer" unique_strength="0" notnull_strength="1" exp_strength="1" constraints="5"/>
    <constraint field="travel_time" unique_strength="0" notnull_strength="1" exp_strength="2" constraints="5"/>
    <constraint field="contact_time" unique_strength="0" notnull_strength="1" exp_strength="2" constraints="5"/>
    <constraint field="comment" unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0"/>
    <constraint field="_model" unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0"/>
  </constraints>
  <constraintExpressions>
    <constraint field="id" exp="" desc=""/>
    <constraint field="name" exp="" desc=""/>
    <constraint field="q0" exp="" desc=""/>
    <constraint field="external_file" exp="" desc=""/>
    <constraint field="cyclic" exp="" desc=""/>
    <constraint field="chlorine" exp="chlorine>=0" desc=""/>
    <constraint field="trihalomethane" exp="trihalomethane>=0" desc=""/>
    <constraint field="passive_tracer" exp="passive_tracer>=0" desc=""/>
    <constraint field="travel_time" exp="travel_time>=0" desc=""/>
    <constraint field="contact_time" exp="contact_time >=0" desc=""/>
    <constraint field="comment" exp="" desc=""/>
    <constraint field="_model" exp="" desc=""/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction key="Canvas" value="{00000000-0000-0000-0000-000000000000}"/>
    <actionsetting icon="" action="from expresseau.gui.result_dialog import ResultWindow&#xa;&#xa;if 'result_window' in locals() and result_window.isVisible():&#xa;    result_window.set_result([%id%],&#xa;                    '[% decode_uri(@layer, 'table')%]',&#xa;                    '[% @current_scenario%]')&#xa;    result_window.hide()&#xa;    result_window.show()&#xa;else:&#xa;    result_window = ResultWindow([%id%],&#xa;                    '[% decode_uri(@layer, 'table')%]',&#xa;                    '[% @current_scenario%]',&#xa;                    '[% @project_basename%]')&#xa;    result_window.open()&#xa;" notificationMessage="" type="1" id="{e701b68b-4c2e-4075-9541-27eb5d23ba14}" isEnabledOnlyWhenEditable="0" shortTitle="" name="Open results" capture="0">
      <actionScope id="Feature"/>
      <actionScope id="Layer"/>
    </actionsetting>
  </attributeactions>
  <attributetableconfig sortOrder="0" actionWidgetStyle="dropDown" sortExpression="">
    <columns>
      <column type="field" hidden="0" name="name" width="-1"/>
      <column type="field" hidden="0" name="q0" width="-1"/>
      <column type="field" hidden="0" name="trihalomethane" width="-1"/>
      <column type="actions" hidden="1" width="-1"/>
      <column type="field" hidden="0" name="comment" width="-1"/>
      <column type="field" hidden="0" name="passive_tracer" width="-1"/>
      <column type="field" hidden="0" name="external_file" width="-1"/>
      <column type="field" hidden="0" name="id" width="-1"/>
      <column type="field" hidden="0" name="chlorine" width="-1"/>
      <column type="field" hidden="0" name="cyclic" width="-1"/>
      <column type="field" hidden="0" name="travel_time" width="-1"/>
      <column type="field" hidden="0" name="contact_time" width="-1"/>
      <column type="field" hidden="0" name="_model" width="-1"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appelée à l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalités à vos formulaires.

Entrez le nom de la fonction dans le champ "Fonction d'initialisation Python".
Voici un exemple à suivre:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
    geom = feature.geometry()
    control = dialog.findChild(QWidget, "MyLineEdit")

]]></editforminitcode>
  <featformsuppress>2</featformsuppress>
  <editorlayout>tablayout</editorlayout>
  <attributeEditorForm>
    <attributeEditorField showLabel="1" name="name" index="1"/>
    <attributeEditorField showLabel="1" name="_model" index="11"/>
    <attributeEditorField showLabel="1" name="id" index="0"/>
    <attributeEditorField showLabel="1" name="comment" index="10"/>
    <attributeEditorField showLabel="1" name="q0" index="2"/>
    <attributeEditorContainer groupBox="1" columnCount="2" visibilityExpressionEnabled="0" showLabel="0" name="" visibilityExpression="">
      <attributeEditorField showLabel="1" name="external_file" index="3"/>
      <attributeEditorField showLabel="1" name="cyclic" index="4"/>
    </attributeEditorContainer>
    <attributeEditorContainer groupBox="1" columnCount="3" visibilityExpressionEnabled="0" showLabel="1" name="Concentrations" visibilityExpression="">
      <attributeEditorField showLabel="1" name="chlorine" index="5"/>
      <attributeEditorField showLabel="1" name="trihalomethane" index="6"/>
      <attributeEditorField showLabel="1" name="passive_tracer" index="7"/>
    </attributeEditorContainer>
    <attributeEditorContainer groupBox="1" columnCount="2" visibilityExpressionEnabled="0" showLabel="1" name="Times" visibilityExpression="">
      <attributeEditorField showLabel="1" name="travel_time" index="8"/>
      <attributeEditorField showLabel="1" name="contact_time" index="9"/>
    </attributeEditorContainer>
  </attributeEditorForm>
  <editable>
    <field editable="1" name="_id"/>
    <field editable="0" name="_model"/>
    <field editable="1" name="chlore"/>
    <field editable="1" name="chlore_injection"/>
    <field editable="1" name="chlorine"/>
    <field editable="1" name="chlorine_control"/>
    <field editable="1" name="chlorine_mode"/>
    <field editable="1" name="comment"/>
    <field editable="1" name="configuration"/>
    <field editable="1" name="configuration_json"/>
    <field editable="1" name="contact_time"/>
    <field editable="1" name="cyclic"/>
    <field editable="1" name="external_file"/>
    <field editable="1" name="florures"/>
    <field editable="0" name="id"/>
    <field editable="1" name="model"/>
    <field editable="1" name="name"/>
    <field editable="0" name="node"/>
    <field editable="1" name="passive_tracer"/>
    <field editable="1" name="q0"/>
    <field editable="1" name="target_concentration"/>
    <field editable="1" name="target_model"/>
    <field editable="1" name="target_node"/>
    <field editable="1" name="travel_time"/>
    <field editable="1" name="trihalomethane"/>
    <field editable="1" name="validity"/>
  </editable>
  <labelOnTop>
    <field name="_id" labelOnTop="0"/>
    <field name="_model" labelOnTop="0"/>
    <field name="chlore" labelOnTop="0"/>
    <field name="chlore_injection" labelOnTop="0"/>
    <field name="chlorine" labelOnTop="0"/>
    <field name="chlorine_control" labelOnTop="0"/>
    <field name="chlorine_mode" labelOnTop="0"/>
    <field name="comment" labelOnTop="0"/>
    <field name="configuration" labelOnTop="0"/>
    <field name="configuration_json" labelOnTop="0"/>
    <field name="contact_time" labelOnTop="0"/>
    <field name="cyclic" labelOnTop="0"/>
    <field name="external_file" labelOnTop="0"/>
    <field name="florures" labelOnTop="0"/>
    <field name="id" labelOnTop="0"/>
    <field name="model" labelOnTop="0"/>
    <field name="name" labelOnTop="0"/>
    <field name="node" labelOnTop="0"/>
    <field name="passive_tracer" labelOnTop="0"/>
    <field name="q0" labelOnTop="0"/>
    <field name="target_concentration" labelOnTop="0"/>
    <field name="target_model" labelOnTop="0"/>
    <field name="target_node" labelOnTop="0"/>
    <field name="travel_time" labelOnTop="0"/>
    <field name="trihalomethane" labelOnTop="0"/>
    <field name="validity" labelOnTop="0"/>
  </labelOnTop>
  <reuseLastValue>
    <field reuseLastValue="0" name="_model"/>
    <field reuseLastValue="0" name="chlorine"/>
    <field reuseLastValue="0" name="comment"/>
    <field reuseLastValue="0" name="contact_time"/>
    <field reuseLastValue="0" name="cyclic"/>
    <field reuseLastValue="0" name="external_file"/>
    <field reuseLastValue="0" name="id"/>
    <field reuseLastValue="0" name="name"/>
    <field reuseLastValue="0" name="passive_tracer"/>
    <field reuseLastValue="0" name="q0"/>
    <field reuseLastValue="0" name="travel_time"/>
    <field reuseLastValue="0" name="trihalomethane"/>
  </reuseLastValue>
  <dataDefinedFieldProperties/>
  <widgets/>
  <previewExpression>"name"</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>0</layerGeometryType>
</qgis>
