<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis hasScaleBasedVisibilityFlag="0" maxScale="0" readOnly="0" minScale="1e+08" version="3.22.16-Białowieża" styleCategories="AllStyleCategories">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
    <Private>0</Private>
  </flags>
  <temporal startExpression="" endField="" mode="0" durationField="" fixedDuration="0" enabled="0" accumulate="0" limitMode="0" endExpression="" durationUnit="min" startField="">
    <fixedRange>
      <start></start>
      <end></end>
    </fixedRange>
  </temporal>
  <customproperties>
    <Option type="Map">
      <Option type="List" name="dualview/previewExpressions">
        <Option type="QString" value="&quot;t&quot;"/>
      </Option>
      <Option type="int" name="embeddedWidgets/count" value="0"/>
      <Option name="variableNames"/>
      <Option name="variableValues"/>
    </Option>
  </customproperties>
  <geometryOptions removeDuplicateNodes="0" geometryPrecision="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <legend showLabelLegend="0" type="default-vector"/>
  <referencedLayers>
    <relation referencedLayer="Sensor_062d5b29_9dbc_4f36_a3ce_3c09b7686be9" layerName="Sensor" name="Sensor" layerId="Sensor_062d5b29_9dbc_4f36_a3ce_3c09b7686be9" providerKey="postgres" id="Measure_75706e2c_31cd_42c6_8de3_0b231d1b879e_sensor_Sensor_062d5b29_9dbc_4f36_a3ce_3c09b7686be9_id" strength="Association" dataSource="dbname='config_bis_xtest' service='expresseau' sslmode=disable key='id' checkPrimaryKeyUnicity='0' table=&quot;api&quot;.&quot;sensor&quot; (geom)" referencingLayer="Measure_75706e2c_31cd_42c6_8de3_0b231d1b879e">
      <fieldRef referencingField="sensor" referencedField="id"/>
    </relation>
  </referencedLayers>
  <fieldConfiguration>
    <field configurationFlags="None" name="t">
      <editWidget type="DateTime">
        <config>
          <Option type="Map">
            <Option type="bool" name="allow_null" value="true"/>
            <Option type="bool" name="calendar_popup" value="true"/>
            <Option type="QString" name="display_format" value="M/d/yy HH:mm:ss"/>
            <Option type="QString" name="field_format" value="M/d/yy HH:mm:ss"/>
            <Option type="bool" name="field_iso_format" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="value">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="sensor">
      <editWidget type="RelationReference">
        <config>
          <Option type="Map">
            <Option type="bool" name="AllowAddFeatures" value="false"/>
            <Option type="bool" name="AllowNULL" value="false"/>
            <Option type="bool" name="MapIdentification" value="false"/>
            <Option type="bool" name="OrderByValue" value="false"/>
            <Option type="bool" name="ReadOnly" value="false"/>
            <Option type="QString" name="ReferencedLayerDataSource" value="dbname='config_bis_xtest' service='expresseau' sslmode=disable key='id' checkPrimaryKeyUnicity='0' table=&quot;api&quot;.&quot;sensor&quot; (geom)"/>
            <Option type="QString" name="ReferencedLayerId" value="Sensor_062d5b29_9dbc_4f36_a3ce_3c09b7686be9"/>
            <Option type="QString" name="ReferencedLayerName" value="Sensor"/>
            <Option type="QString" name="ReferencedLayerProviderKey" value="postgres"/>
            <Option type="QString" name="Relation" value="Measure_75706e2c_31cd_42c6_8de3_0b231d1b879e_sensor_Sensor_062d5b29_9dbc_4f36_a3ce_3c09b7686be9_id"/>
            <Option type="bool" name="ShowForm" value="false"/>
            <Option type="bool" name="ShowOpenFormButton" value="true"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="creation_date">
      <editWidget type="DateTime">
        <config>
          <Option type="Map">
            <Option type="bool" name="allow_null" value="true"/>
            <Option type="bool" name="calendar_popup" value="true"/>
            <Option type="QString" name="display_format" value="M/d/yy HH:mm:ss"/>
            <Option type="QString" name="field_format" value="M/d/yy HH:mm:ss"/>
            <Option type="bool" name="field_iso_format" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias index="0" name="Timestamp" field="t"/>
    <alias index="1" name="Value" field="value"/>
    <alias index="2" name="Sensor" field="sensor"/>
    <alias index="3" name="Creation date" field="creation_date"/>
  </aliases>
  <defaults>
    <default applyOnUpdate="0" expression="" field="t"/>
    <default applyOnUpdate="0" expression="" field="value"/>
    <default applyOnUpdate="0" expression="" field="sensor"/>
    <default applyOnUpdate="0" expression="" field="creation_date"/>
  </defaults>
  <constraints>
    <constraint constraints="1" exp_strength="0" notnull_strength="1" field="t" unique_strength="0"/>
    <constraint constraints="1" exp_strength="0" notnull_strength="1" field="value" unique_strength="0"/>
    <constraint constraints="1" exp_strength="0" notnull_strength="1" field="sensor" unique_strength="0"/>
    <constraint constraints="1" exp_strength="0" notnull_strength="1" field="creation_date" unique_strength="0"/>
  </constraints>
  <constraintExpressions>
    <constraint exp="" field="t" desc=""/>
    <constraint exp="" field="value" desc=""/>
    <constraint exp="" field="sensor" desc=""/>
    <constraint exp="" field="creation_date" desc=""/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction value="{00000000-0000-0000-0000-000000000000}" key="Canvas"/>
  </attributeactions>
  <attributetableconfig sortExpression="" sortOrder="0" actionWidgetStyle="dropDown">
    <columns>
      <column width="-1" type="field" name="t" hidden="0"/>
      <column width="-1" type="field" name="value" hidden="0"/>
      <column width="-1" type="field" name="sensor" hidden="0"/>
      <column width="-1" type="field" name="creation_date" hidden="0"/>
      <column width="-1" type="actions" hidden="1"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
QGIS forms can have a Python function that is called when the form is
opened.

Use this function to add extra logic to your forms.

Enter the name of the function in the "Python Init function"
field.
An example follows:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
	geom = feature.geometry()
	control = dialog.findChild(QWidget, "MyLineEdit")
]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>tablayout</editorlayout>
  <attributeEditorForm>
    <attributeEditorField index="0" name="t" showLabel="1"/>
    <attributeEditorField index="1" name="value" showLabel="1"/>
    <attributeEditorField index="2" name="sensor" showLabel="1"/>
    <attributeEditorField index="3" name="creation_date" showLabel="1"/>
  </attributeEditorForm>
  <editable>
    <field name="creation_date" editable="1"/>
    <field name="sensor" editable="1"/>
    <field name="t" editable="1"/>
    <field name="value" editable="1"/>
  </editable>
  <labelOnTop>
    <field name="creation_date" labelOnTop="0"/>
    <field name="sensor" labelOnTop="0"/>
    <field name="t" labelOnTop="0"/>
    <field name="value" labelOnTop="0"/>
  </labelOnTop>
  <reuseLastValue>
    <field name="creation_date" reuseLastValue="0"/>
    <field name="sensor" reuseLastValue="0"/>
    <field name="t" reuseLastValue="0"/>
    <field name="value" reuseLastValue="0"/>
  </reuseLastValue>
  <dataDefinedFieldProperties/>
  <widgets/>
  <previewExpression>"t"</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>4</layerGeometryType>
</qgis>
