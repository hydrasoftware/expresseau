<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis symbologyReferenceScale="-1" readOnly="0" maxScale="0" simplifyAlgorithm="0" styleCategories="AllStyleCategories" simplifyMaxScale="1" version="3.28.8-Firenze" simplifyDrawingHints="0" minScale="100000000" labelsEnabled="0" simplifyLocal="1" simplifyDrawingTol="1" hasScaleBasedVisibilityFlag="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
    <Private>0</Private>
  </flags>
  <temporal startField="" accumulate="0" durationField="id" limitMode="0" fixedDuration="0" mode="0" endExpression="" durationUnit="min" endField="" startExpression="" enabled="0">
    <fixedRange>
      <start></start>
      <end></end>
    </fixedRange>
  </temporal>
  <elevation type="IndividualFeatures" showMarkerSymbolInSurfacePlots="0" zoffset="0" zscale="1" symbology="Line" extrusion="0" clamping="Terrain" extrusionEnabled="0" binding="Centroid" respectLayerSymbol="1">
    <data-defined-properties>
      <Option type="Map">
        <Option type="QString" value="" name="name"/>
        <Option name="properties"/>
        <Option type="QString" value="collection" name="type"/>
      </Option>
    </data-defined-properties>
    <profileLineSymbol>
      <symbol type="line" name="" is_animated="0" clip_to_extent="1" frame_rate="10" alpha="1" force_rhr="0">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" value="" name="name"/>
            <Option name="properties"/>
            <Option type="QString" value="collection" name="type"/>
          </Option>
        </data_defined_properties>
        <layer class="SimpleLine" locked="0" enabled="1" pass="0">
          <Option type="Map">
            <Option type="QString" value="0" name="align_dash_pattern"/>
            <Option type="QString" value="square" name="capstyle"/>
            <Option type="QString" value="5;2" name="customdash"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="customdash_map_unit_scale"/>
            <Option type="QString" value="MM" name="customdash_unit"/>
            <Option type="QString" value="0" name="dash_pattern_offset"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="dash_pattern_offset_map_unit_scale"/>
            <Option type="QString" value="MM" name="dash_pattern_offset_unit"/>
            <Option type="QString" value="0" name="draw_inside_polygon"/>
            <Option type="QString" value="bevel" name="joinstyle"/>
            <Option type="QString" value="225,89,137,255" name="line_color"/>
            <Option type="QString" value="solid" name="line_style"/>
            <Option type="QString" value="0.6" name="line_width"/>
            <Option type="QString" value="MM" name="line_width_unit"/>
            <Option type="QString" value="0" name="offset"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
            <Option type="QString" value="MM" name="offset_unit"/>
            <Option type="QString" value="0" name="ring_filter"/>
            <Option type="QString" value="0" name="trim_distance_end"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="trim_distance_end_map_unit_scale"/>
            <Option type="QString" value="MM" name="trim_distance_end_unit"/>
            <Option type="QString" value="0" name="trim_distance_start"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="trim_distance_start_map_unit_scale"/>
            <Option type="QString" value="MM" name="trim_distance_start_unit"/>
            <Option type="QString" value="0" name="tweak_dash_pattern_on_corners"/>
            <Option type="QString" value="0" name="use_custom_dash"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="width_map_unit_scale"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileLineSymbol>
    <profileFillSymbol>
      <symbol type="fill" name="" is_animated="0" clip_to_extent="1" frame_rate="10" alpha="1" force_rhr="0">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" value="" name="name"/>
            <Option name="properties"/>
            <Option type="QString" value="collection" name="type"/>
          </Option>
        </data_defined_properties>
        <layer class="SimpleFill" locked="0" enabled="1" pass="0">
          <Option type="Map">
            <Option type="QString" value="3x:0,0,0,0,0,0" name="border_width_map_unit_scale"/>
            <Option type="QString" value="225,89,137,255" name="color"/>
            <Option type="QString" value="bevel" name="joinstyle"/>
            <Option type="QString" value="0,0" name="offset"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
            <Option type="QString" value="MM" name="offset_unit"/>
            <Option type="QString" value="161,64,98,255" name="outline_color"/>
            <Option type="QString" value="solid" name="outline_style"/>
            <Option type="QString" value="0.2" name="outline_width"/>
            <Option type="QString" value="MM" name="outline_width_unit"/>
            <Option type="QString" value="solid" name="style"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileFillSymbol>
    <profileMarkerSymbol>
      <symbol type="marker" name="" is_animated="0" clip_to_extent="1" frame_rate="10" alpha="1" force_rhr="0">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" value="" name="name"/>
            <Option name="properties"/>
            <Option type="QString" value="collection" name="type"/>
          </Option>
        </data_defined_properties>
        <layer class="SimpleMarker" locked="0" enabled="1" pass="0">
          <Option type="Map">
            <Option type="QString" value="0" name="angle"/>
            <Option type="QString" value="square" name="cap_style"/>
            <Option type="QString" value="225,89,137,255" name="color"/>
            <Option type="QString" value="1" name="horizontal_anchor_point"/>
            <Option type="QString" value="bevel" name="joinstyle"/>
            <Option type="QString" value="diamond" name="name"/>
            <Option type="QString" value="0,0" name="offset"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
            <Option type="QString" value="MM" name="offset_unit"/>
            <Option type="QString" value="161,64,98,255" name="outline_color"/>
            <Option type="QString" value="solid" name="outline_style"/>
            <Option type="QString" value="0.2" name="outline_width"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale"/>
            <Option type="QString" value="MM" name="outline_width_unit"/>
            <Option type="QString" value="diameter" name="scale_method"/>
            <Option type="QString" value="3" name="size"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="size_map_unit_scale"/>
            <Option type="QString" value="MM" name="size_unit"/>
            <Option type="QString" value="1" name="vertical_anchor_point"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileMarkerSymbol>
  </elevation>
  <renderer-v2 enableorderby="0" type="singleSymbol" symbollevels="0" referencescale="-1" forceraster="0">
    <symbols>
      <symbol type="marker" name="0" is_animated="0" clip_to_extent="1" frame_rate="10" alpha="1" force_rhr="0">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" value="" name="name"/>
            <Option name="properties"/>
            <Option type="QString" value="collection" name="type"/>
          </Option>
        </data_defined_properties>
        <layer class="SimpleMarker" locked="0" enabled="1" pass="0">
          <Option type="Map">
            <Option type="QString" value="0" name="angle"/>
            <Option type="QString" value="square" name="cap_style"/>
            <Option type="QString" value="166,206,227,255" name="color"/>
            <Option type="QString" value="1" name="horizontal_anchor_point"/>
            <Option type="QString" value="bevel" name="joinstyle"/>
            <Option type="QString" value="cross_fill" name="name"/>
            <Option type="QString" value="0,0" name="offset"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
            <Option type="QString" value="MM" name="offset_unit"/>
            <Option type="QString" value="35,35,35,255" name="outline_color"/>
            <Option type="QString" value="solid" name="outline_style"/>
            <Option type="QString" value="0" name="outline_width"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale"/>
            <Option type="QString" value="MM" name="outline_width_unit"/>
            <Option type="QString" value="diameter" name="scale_method"/>
            <Option type="QString" value="4" name="size"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="size_map_unit_scale"/>
            <Option type="QString" value="MM" name="size_unit"/>
            <Option type="QString" value="1" name="vertical_anchor_point"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option type="Map" name="properties">
                <Option type="Map" name="fillColor">
                  <Option type="bool" value="true" name="active"/>
                  <Option type="QString" value="set_color_part(@value, 'alpha', (case when &quot;_model&quot;=@current_model then 1 else 0.4 end) * 255 )" name="expression"/>
                  <Option type="int" value="3" name="type"/>
                </Option>
                <Option type="Map" name="outlineColor">
                  <Option type="bool" value="true" name="active"/>
                  <Option type="QString" value="case when &quot;model&quot;=@current_model then '#FF000000' else '#FFAAAAAA' end" name="expression"/>
                  <Option type="int" value="3" name="type"/>
                </Option>
              </Option>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
    <rotation/>
    <sizescale/>
  </renderer-v2>
  <customproperties>
    <Option type="Map">
      <Option type="List" name="dualview/previewExpressions">
        <Option type="QString" value="&quot;name&quot;"/>
      </Option>
      <Option type="int" value="0" name="embeddedWidgets/count"/>
      <Option name="variableNames"/>
      <Option name="variableValues"/>
    </Option>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer diagramType="Histogram" attributeLegend="1">
    <DiagramCategory penAlpha="255" height="15" backgroundColor="#ffffff" minScaleDenominator="0" labelPlacementMethod="XHeight" diagramOrientation="Up" scaleDependency="Area" lineSizeType="MM" spacing="0" lineSizeScale="3x:0,0,0,0,0,0" scaleBasedVisibility="0" sizeScale="3x:0,0,0,0,0,0" spacingUnitScale="3x:0,0,0,0,0,0" enabled="0" backgroundAlpha="255" maxScaleDenominator="1e+08" barWidth="5" width="15" minimumSize="0" direction="1" showAxis="0" rotationOffset="270" opacity="1" penColor="#000000" spacingUnit="MM" penWidth="0" sizeType="MM">
      <fontProperties bold="0" italic="0" underline="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" strikethrough="0" style=""/>
      <attribute field="" colorOpacity="1" label="" color="#000000"/>
      <axisSymbol>
        <symbol type="line" name="" is_animated="0" clip_to_extent="1" frame_rate="10" alpha="1" force_rhr="0">
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
          <layer class="SimpleLine" locked="0" enabled="1" pass="0">
            <Option type="Map">
              <Option type="QString" value="0" name="align_dash_pattern"/>
              <Option type="QString" value="square" name="capstyle"/>
              <Option type="QString" value="5;2" name="customdash"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="customdash_map_unit_scale"/>
              <Option type="QString" value="MM" name="customdash_unit"/>
              <Option type="QString" value="0" name="dash_pattern_offset"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="dash_pattern_offset_map_unit_scale"/>
              <Option type="QString" value="MM" name="dash_pattern_offset_unit"/>
              <Option type="QString" value="0" name="draw_inside_polygon"/>
              <Option type="QString" value="bevel" name="joinstyle"/>
              <Option type="QString" value="35,35,35,255" name="line_color"/>
              <Option type="QString" value="solid" name="line_style"/>
              <Option type="QString" value="0.26" name="line_width"/>
              <Option type="QString" value="MM" name="line_width_unit"/>
              <Option type="QString" value="0" name="offset"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
              <Option type="QString" value="MM" name="offset_unit"/>
              <Option type="QString" value="0" name="ring_filter"/>
              <Option type="QString" value="0" name="trim_distance_end"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="trim_distance_end_map_unit_scale"/>
              <Option type="QString" value="MM" name="trim_distance_end_unit"/>
              <Option type="QString" value="0" name="trim_distance_start"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="trim_distance_start_map_unit_scale"/>
              <Option type="QString" value="MM" name="trim_distance_start_unit"/>
              <Option type="QString" value="0" name="tweak_dash_pattern_on_corners"/>
              <Option type="QString" value="0" name="use_custom_dash"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="width_map_unit_scale"/>
            </Option>
            <data_defined_properties>
              <Option type="Map">
                <Option type="QString" value="" name="name"/>
                <Option name="properties"/>
                <Option type="QString" value="collection" name="type"/>
              </Option>
            </data_defined_properties>
          </layer>
        </symbol>
      </axisSymbol>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings linePlacementFlags="18" priority="0" zIndex="0" placement="0" showAll="1" obstacle="0" dist="0">
    <properties>
      <Option type="Map">
        <Option type="QString" value="" name="name"/>
        <Option name="properties"/>
        <Option type="QString" value="collection" name="type"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions geometryPrecision="0" removeDuplicateNodes="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <legend type="default-vector" showLabelLegend="0"/>
  <referencedLayers>
    <relation layerId="All_nodes_fd7e8c9e_d922_4e3b_9b9d_24fcd27d2734" name="Imposed piezometry id" dataSource="dbname='tutoriel_essentiel' service='expresseau' sslmode=disable key='id' checkPrimaryKeyUnicity='0' table=&quot;api&quot;.&quot;node&quot; (geom)" referencingLayer="Imposed_piezometry_b77a382b_806a_4327_880a_4c2f792f4fed" id="Imposed_piezometry_b77a382b_806a_4327_880a_4c2f792f4fed_id_All_nodes_fd7e8c9e_d922_4e3b_9b9d_24fcd27d2734_id" layerName="All nodes" strength="Association" providerKey="postgres" referencedLayer="All_nodes_fd7e8c9e_d922_4e3b_9b9d_24fcd27d2734">
      <fieldRef referencingField="id" referencedField="id"/>
    </relation>
  </referencedLayers>
  <fieldConfiguration>
    <field name="id" configurationFlags="None">
      <editWidget type="RelationReference">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="AllowAddFeatures"/>
            <Option type="bool" value="false" name="AllowNULL"/>
            <Option type="bool" value="true" name="MapIdentification"/>
            <Option type="bool" value="false" name="OrderByValue"/>
            <Option type="bool" value="true" name="ReadOnly"/>
            <Option type="QString" value="dbname='tutoriel_essentiel' service='expresseau' sslmode=disable key='id' checkPrimaryKeyUnicity='0' table=&quot;api&quot;.&quot;node&quot; (geom)" name="ReferencedLayerDataSource"/>
            <Option type="QString" value="All_nodes_fd7e8c9e_d922_4e3b_9b9d_24fcd27d2734" name="ReferencedLayerId"/>
            <Option type="QString" value="All nodes" name="ReferencedLayerName"/>
            <Option type="QString" value="postgres" name="ReferencedLayerProviderKey"/>
            <Option type="QString" value="Imposed_piezometry_b77a382b_806a_4327_880a_4c2f792f4fed_id_All_nodes_fd7e8c9e_d922_4e3b_9b9d_24fcd27d2734_id" name="Relation"/>
            <Option type="bool" value="false" name="ShowForm"/>
            <Option type="bool" value="false" name="ShowOpenFormButton"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="name" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="z0" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="chlorine" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="trihalomethane" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="passive_tracer" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="external_file" configurationFlags="None">
      <editWidget type="CheckBox">
        <config>
          <Option type="Map">
            <Option type="QString" value="" name="CheckedState"/>
            <Option type="int" value="0" name="TextDisplayMethod"/>
            <Option type="QString" value="" name="UncheckedState"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="cyclic" configurationFlags="None">
      <editWidget type="CheckBox">
        <config>
          <Option type="Map">
            <Option type="QString" value="" name="CheckedState"/>
            <Option type="int" value="0" name="TextDisplayMethod"/>
            <Option type="QString" value="" name="UncheckedState"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="travel_time" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="contact_time" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="comment" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="true" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="_model" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias field="id" name="Node" index="0"/>
    <alias field="name" name="Name" index="1"/>
    <alias field="z0" name="Imposed Z (mNGF)" index="2"/>
    <alias field="chlorine" name="Chlorine (mg/l)" index="3"/>
    <alias field="trihalomethane" name="Trihalomethane (mg/l)" index="4"/>
    <alias field="passive_tracer" name="Passive tracer (mg/l)" index="5"/>
    <alias field="external_file" name="External file" index="6"/>
    <alias field="cyclic" name="Cyclic" index="7"/>
    <alias field="travel_time" name="Travel time (h)" index="8"/>
    <alias field="contact_time" name="Contact time (h)" index="9"/>
    <alias field="comment" name="Comment" index="10"/>
    <alias field="_model" name="Model" index="11"/>
  </aliases>
  <defaults>
    <default field="id" applyOnUpdate="1" expression="attribute(&#xd;&#xa;&#x9;array_first(&#xd;&#xa;&#x9;&#x9;array_filter(&#xd;&#xa;&#x9;&#x9;&#x9;overlay_intersects(&#xd;&#xa;&#x9;&#x9;&#x9;&#x9;&#x9;layer:='All_nodes_fd7e8c9e_d922_4e3b_9b9d_24fcd27d2734',&#xd;&#xa;&#x9;&#x9;&#x9;&#x9;&#x9;expression:=$currentfeature&#xd;&#xa;&#x9;&#x9;&#x9;),&#xd;&#xa;&#x9;&#x9;&#x9;intersects($geometry, geometry(@element))&#xd;&#xa;&#x9;&#x9;)&#xd;&#xa;&#x9;),&#xd;&#xa;&#x9;'id'&#xd;&#xa;)"/>
    <default field="name" applyOnUpdate="0" expression=""/>
    <default field="z0" applyOnUpdate="0" expression=""/>
    <default field="chlorine" applyOnUpdate="0" expression=""/>
    <default field="trihalomethane" applyOnUpdate="0" expression=""/>
    <default field="passive_tracer" applyOnUpdate="0" expression=""/>
    <default field="external_file" applyOnUpdate="0" expression=""/>
    <default field="cyclic" applyOnUpdate="0" expression=""/>
    <default field="travel_time" applyOnUpdate="0" expression=""/>
    <default field="contact_time" applyOnUpdate="0" expression=""/>
    <default field="comment" applyOnUpdate="0" expression=""/>
    <default field="_model" applyOnUpdate="0" expression="attribute(&#xd;&#xa;&#x9;array_first(&#xd;&#xa;&#x9;&#x9;array_filter(&#xd;&#xa;&#x9;&#x9;&#x9;overlay_intersects(&#xd;&#xa;&#x9;&#x9;&#x9;&#x9;&#x9;layer:='All_nodes_fd7e8c9e_d922_4e3b_9b9d_24fcd27d2734',&#xd;&#xa;&#x9;&#x9;&#x9;&#x9;&#x9;expression:=$currentfeature&#xd;&#xa;&#x9;&#x9;&#x9;),&#xd;&#xa;&#x9;&#x9;&#x9;intersects($geometry, geometry(@element))&#xd;&#xa;&#x9;&#x9;)&#xd;&#xa;&#x9;),&#xd;&#xa;&#x9;'model'&#xd;&#xa;)"/>
  </defaults>
  <constraints>
    <constraint field="id" constraints="3" unique_strength="1" exp_strength="0" notnull_strength="1"/>
    <constraint field="name" constraints="1" unique_strength="0" exp_strength="0" notnull_strength="1"/>
    <constraint field="z0" constraints="1" unique_strength="0" exp_strength="0" notnull_strength="1"/>
    <constraint field="chlorine" constraints="5" unique_strength="0" exp_strength="1" notnull_strength="1"/>
    <constraint field="trihalomethane" constraints="5" unique_strength="0" exp_strength="1" notnull_strength="1"/>
    <constraint field="passive_tracer" constraints="5" unique_strength="0" exp_strength="1" notnull_strength="1"/>
    <constraint field="external_file" constraints="1" unique_strength="0" exp_strength="0" notnull_strength="1"/>
    <constraint field="cyclic" constraints="1" unique_strength="0" exp_strength="0" notnull_strength="1"/>
    <constraint field="travel_time" constraints="5" unique_strength="0" exp_strength="1" notnull_strength="1"/>
    <constraint field="contact_time" constraints="5" unique_strength="0" exp_strength="1" notnull_strength="1"/>
    <constraint field="comment" constraints="0" unique_strength="0" exp_strength="0" notnull_strength="0"/>
    <constraint field="_model" constraints="0" unique_strength="0" exp_strength="0" notnull_strength="0"/>
  </constraints>
  <constraintExpressions>
    <constraint field="id" exp="" desc=""/>
    <constraint field="name" exp="" desc=""/>
    <constraint field="z0" exp="" desc=""/>
    <constraint field="chlorine" exp="&quot;chlorine&quot; >= 0" desc=""/>
    <constraint field="trihalomethane" exp="&quot;trihalomethane&quot; >= 0" desc=""/>
    <constraint field="passive_tracer" exp="&quot;passive_tracer&quot; >= 0" desc=""/>
    <constraint field="external_file" exp="" desc=""/>
    <constraint field="cyclic" exp="" desc=""/>
    <constraint field="travel_time" exp="&quot;travel_time&quot; >= 0" desc=""/>
    <constraint field="contact_time" exp="&quot;contact_time&quot; >= 0" desc=""/>
    <constraint field="comment" exp="" desc=""/>
    <constraint field="_model" exp="" desc=""/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction value="{00000000-0000-0000-0000-000000000000}" key="Canvas"/>
    <actionsetting type="1" name="Open results" isEnabledOnlyWhenEditable="0" action="from expresseau.gui.result_dialog import ResultWindow&#xa;&#xa;if 'result_window' in locals() and result_window.isVisible():&#xa;    result_window.set_result([%id%],&#xa;                    '[% decode_uri(@layer, 'table')%]',&#xa;                    '[% @current_scenario%]')&#xa;    result_window.hide()&#xa;    result_window.show()&#xa;else:&#xa;    result_window = ResultWindow([%id%],&#xa;                    '[% decode_uri(@layer, 'table')%]',&#xa;                    '[% @current_scenario%]',&#xa;                    '[% @project_basename%]')&#xa;    result_window.open()&#xa;" id="{0c8daabc-cf68-452f-9a77-74d04098e96c}" icon="" shortTitle="" capture="0" notificationMessage="">
      <actionScope id="Layer"/>
      <actionScope id="Feature"/>
    </actionsetting>
  </attributeactions>
  <attributetableconfig sortOrder="0" actionWidgetStyle="dropDown" sortExpression="">
    <columns>
      <column type="field" hidden="0" name="name" width="-1"/>
      <column type="field" hidden="0" name="z0" width="268"/>
      <column type="field" hidden="0" name="trihalomethane" width="-1"/>
      <column type="actions" hidden="1" width="-1"/>
      <column type="field" hidden="0" name="comment" width="-1"/>
      <column type="field" hidden="0" name="passive_tracer" width="-1"/>
      <column type="field" hidden="0" name="id" width="-1"/>
      <column type="field" hidden="0" name="chlorine" width="-1"/>
      <column type="field" hidden="0" name="external_file" width="-1"/>
      <column type="field" hidden="0" name="cyclic" width="-1"/>
      <column type="field" hidden="0" name="travel_time" width="-1"/>
      <column type="field" hidden="0" name="contact_time" width="-1"/>
      <column type="field" hidden="0" name="_model" width="-1"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appelée à l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalités à vos formulaires.

Entrez le nom de la fonction dans le champ "Fonction d'initialisation Python".
Voici un exemple à suivre:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
    geom = feature.geometry()
    control = dialog.findChild(QWidget, "MyLineEdit")

]]></editforminitcode>
  <featformsuppress>2</featformsuppress>
  <editorlayout>tablayout</editorlayout>
  <attributeEditorForm>
    <labelStyle overrideLabelFont="0" overrideLabelColor="0" labelColor="0,0,0,255">
      <labelFont bold="0" italic="0" underline="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" strikethrough="0" style=""/>
    </labelStyle>
    <attributeEditorField name="name" showLabel="1" index="1">
      <labelStyle overrideLabelFont="0" overrideLabelColor="0" labelColor="0,0,0,255">
        <labelFont bold="0" italic="0" underline="0" description="MS Shell Dlg 2,9.6,-1,5,50,0,0,0,0,0" strikethrough="0" style=""/>
      </labelStyle>
    </attributeEditorField>
    <attributeEditorField name="_model" showLabel="1" index="11">
      <labelStyle overrideLabelFont="0" overrideLabelColor="0" labelColor="0,0,0,255">
        <labelFont bold="0" italic="0" underline="0" description="MS Shell Dlg 2,9.6,-1,5,50,0,0,0,0,0" strikethrough="0" style=""/>
      </labelStyle>
    </attributeEditorField>
    <attributeEditorField name="id" showLabel="1" index="0">
      <labelStyle overrideLabelFont="0" overrideLabelColor="0" labelColor="0,0,0,255">
        <labelFont bold="0" italic="0" underline="0" description="MS Shell Dlg 2,9.6,-1,5,50,0,0,0,0,0" strikethrough="0" style=""/>
      </labelStyle>
    </attributeEditorField>
    <attributeEditorField name="comment" showLabel="1" index="10">
      <labelStyle overrideLabelFont="0" overrideLabelColor="0" labelColor="0,0,0,255">
        <labelFont bold="0" italic="0" underline="0" description="MS Shell Dlg 2,9.6,-1,5,50,0,0,0,0,0" strikethrough="0" style=""/>
      </labelStyle>
    </attributeEditorField>
    <attributeEditorContainer collapsedExpressionEnabled="0" name="Hydraulic boundary conditions" visibilityExpressionEnabled="0" showLabel="1" columnCount="3" groupBox="1" collapsedExpression="" collapsed="0" visibilityExpression="">
      <labelStyle overrideLabelFont="0" overrideLabelColor="0" labelColor="0,0,0,255">
        <labelFont bold="0" italic="0" underline="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" strikethrough="0" style=""/>
      </labelStyle>
      <attributeEditorField name="z0" showLabel="1" index="2">
        <labelStyle overrideLabelFont="0" overrideLabelColor="0" labelColor="0,0,0,255">
          <labelFont bold="0" italic="0" underline="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" strikethrough="0" style=""/>
        </labelStyle>
      </attributeEditorField>
      <attributeEditorContainer collapsedExpressionEnabled="0" name="external file ?" visibilityExpressionEnabled="0" showLabel="1" columnCount="1" groupBox="1" collapsedExpression="" collapsed="0" visibilityExpression="">
        <labelStyle overrideLabelFont="0" overrideLabelColor="0" labelColor="0,0,0,255">
          <labelFont bold="0" italic="0" underline="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" strikethrough="0" style=""/>
        </labelStyle>
        <attributeEditorField name="external_file" showLabel="1" index="6">
          <labelStyle overrideLabelFont="0" overrideLabelColor="0" labelColor="0,0,0,255">
            <labelFont bold="0" italic="0" underline="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" strikethrough="0" style=""/>
          </labelStyle>
        </attributeEditorField>
        <attributeEditorField name="cyclic" showLabel="1" index="7">
          <labelStyle overrideLabelFont="0" overrideLabelColor="0" labelColor="0,0,0,255">
            <labelFont bold="0" italic="0" underline="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" strikethrough="0" style=""/>
          </labelStyle>
        </attributeEditorField>
      </attributeEditorContainer>
    </attributeEditorContainer>
    <attributeEditorContainer collapsedExpressionEnabled="0" name="Quality boundary conditions" visibilityExpressionEnabled="0" showLabel="1" columnCount="1" groupBox="1" collapsedExpression="" collapsed="0" visibilityExpression="">
      <labelStyle overrideLabelFont="0" overrideLabelColor="0" labelColor="0,0,0,255">
        <labelFont bold="0" italic="0" underline="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" strikethrough="0" style=""/>
      </labelStyle>
      <attributeEditorContainer collapsedExpressionEnabled="0" name="Concentrations" visibilityExpressionEnabled="0" showLabel="1" columnCount="3" groupBox="1" collapsedExpression="" collapsed="0" visibilityExpression="">
        <labelStyle overrideLabelFont="0" overrideLabelColor="0" labelColor="0,0,0,255">
          <labelFont bold="0" italic="0" underline="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" strikethrough="0" style=""/>
        </labelStyle>
        <attributeEditorField name="chlorine" showLabel="1" index="3">
          <labelStyle overrideLabelFont="0" overrideLabelColor="0" labelColor="0,0,0,255">
            <labelFont bold="0" italic="0" underline="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" strikethrough="0" style=""/>
          </labelStyle>
        </attributeEditorField>
        <attributeEditorField name="trihalomethane" showLabel="1" index="4">
          <labelStyle overrideLabelFont="0" overrideLabelColor="0" labelColor="0,0,0,255">
            <labelFont bold="0" italic="0" underline="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" strikethrough="0" style=""/>
          </labelStyle>
        </attributeEditorField>
        <attributeEditorField name="passive_tracer" showLabel="1" index="5">
          <labelStyle overrideLabelFont="0" overrideLabelColor="0" labelColor="0,0,0,255">
            <labelFont bold="0" italic="0" underline="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" strikethrough="0" style=""/>
          </labelStyle>
        </attributeEditorField>
      </attributeEditorContainer>
      <attributeEditorContainer collapsedExpressionEnabled="0" name="Times" visibilityExpressionEnabled="0" showLabel="1" columnCount="2" groupBox="1" collapsedExpression="" collapsed="0" visibilityExpression="">
        <labelStyle overrideLabelFont="0" overrideLabelColor="0" labelColor="0,0,0,255">
          <labelFont bold="0" italic="0" underline="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" strikethrough="0" style=""/>
        </labelStyle>
        <attributeEditorField name="travel_time" showLabel="1" index="8">
          <labelStyle overrideLabelFont="0" overrideLabelColor="0" labelColor="0,0,0,255">
            <labelFont bold="0" italic="0" underline="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" strikethrough="0" style=""/>
          </labelStyle>
        </attributeEditorField>
        <attributeEditorField name="contact_time" showLabel="1" index="9">
          <labelStyle overrideLabelFont="0" overrideLabelColor="0" labelColor="0,0,0,255">
            <labelFont bold="0" italic="0" underline="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" strikethrough="0" style=""/>
          </labelStyle>
        </attributeEditorField>
      </attributeEditorContainer>
    </attributeEditorContainer>
  </attributeEditorForm>
  <editable>
    <field name="_id" editable="1"/>
    <field name="_model" editable="0"/>
    <field name="chlore" editable="1"/>
    <field name="chlorine" editable="1"/>
    <field name="comment" editable="1"/>
    <field name="configuration" editable="1"/>
    <field name="configuration_json" editable="1"/>
    <field name="contact_time" editable="1"/>
    <field name="cyclic" editable="1"/>
    <field name="external_file" editable="1"/>
    <field name="florures" editable="1"/>
    <field name="id" editable="0"/>
    <field name="model" editable="1"/>
    <field name="name" editable="1"/>
    <field name="node" editable="0"/>
    <field name="passive_tracer" editable="1"/>
    <field name="travel_time" editable="1"/>
    <field name="trihalomethane" editable="1"/>
    <field name="validity" editable="1"/>
    <field name="z0" editable="1"/>
  </editable>
  <labelOnTop>
    <field labelOnTop="0" name="_id"/>
    <field labelOnTop="0" name="_model"/>
    <field labelOnTop="0" name="chlore"/>
    <field labelOnTop="0" name="chlorine"/>
    <field labelOnTop="0" name="comment"/>
    <field labelOnTop="0" name="configuration"/>
    <field labelOnTop="0" name="configuration_json"/>
    <field labelOnTop="0" name="contact_time"/>
    <field labelOnTop="0" name="cyclic"/>
    <field labelOnTop="0" name="external_file"/>
    <field labelOnTop="0" name="florures"/>
    <field labelOnTop="0" name="id"/>
    <field labelOnTop="0" name="model"/>
    <field labelOnTop="0" name="name"/>
    <field labelOnTop="0" name="node"/>
    <field labelOnTop="0" name="passive_tracer"/>
    <field labelOnTop="0" name="travel_time"/>
    <field labelOnTop="0" name="trihalomethane"/>
    <field labelOnTop="0" name="validity"/>
    <field labelOnTop="0" name="z0"/>
  </labelOnTop>
  <reuseLastValue>
    <field name="_model" reuseLastValue="0"/>
    <field name="chlorine" reuseLastValue="0"/>
    <field name="comment" reuseLastValue="0"/>
    <field name="contact_time" reuseLastValue="0"/>
    <field name="cyclic" reuseLastValue="0"/>
    <field name="external_file" reuseLastValue="0"/>
    <field name="id" reuseLastValue="0"/>
    <field name="name" reuseLastValue="0"/>
    <field name="passive_tracer" reuseLastValue="0"/>
    <field name="travel_time" reuseLastValue="0"/>
    <field name="trihalomethane" reuseLastValue="0"/>
    <field name="z0" reuseLastValue="0"/>
  </reuseLastValue>
  <dataDefinedFieldProperties/>
  <widgets/>
  <previewExpression>"name"</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>0</layerGeometryType>
</qgis>
