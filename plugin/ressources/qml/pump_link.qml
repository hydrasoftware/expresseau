<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis hasScaleBasedVisibilityFlag="0" simplifyLocal="1" maxScale="0" labelsEnabled="0" version="3.22.0-Białowieża" minScale="100000000" simplifyDrawingTol="1" styleCategories="AllStyleCategories" symbologyReferenceScale="-1" simplifyDrawingHints="1" readOnly="0" simplifyAlgorithm="0" simplifyMaxScale="1">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
    <Private>0</Private>
  </flags>
  <temporal limitMode="0" startField="" endExpression="" endField="" durationField="id" mode="0" accumulate="0" startExpression="" enabled="0" fixedDuration="0" durationUnit="min">
    <fixedRange>
      <start></start>
      <end></end>
    </fixedRange>
  </temporal>
  <renderer-v2 enableorderby="0" referencescale="-1" type="singleSymbol" symbollevels="0" forceraster="0">
    <symbols>
      <symbol alpha="1" type="line" clip_to_extent="1" force_rhr="0" name="0">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" value="" name="name"/>
            <Option name="properties"/>
            <Option type="QString" value="collection" name="type"/>
          </Option>
        </data_defined_properties>
        <layer locked="0" pass="0" class="SimpleLine" enabled="1">
          <Option type="Map">
            <Option type="QString" value="0" name="align_dash_pattern"/>
            <Option type="QString" value="square" name="capstyle"/>
            <Option type="QString" value="5;2" name="customdash"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="customdash_map_unit_scale"/>
            <Option type="QString" value="MM" name="customdash_unit"/>
            <Option type="QString" value="0" name="dash_pattern_offset"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="dash_pattern_offset_map_unit_scale"/>
            <Option type="QString" value="MM" name="dash_pattern_offset_unit"/>
            <Option type="QString" value="0" name="draw_inside_polygon"/>
            <Option type="QString" value="bevel" name="joinstyle"/>
            <Option type="QString" value="0,0,0,255" name="line_color"/>
            <Option type="QString" value="solid" name="line_style"/>
            <Option type="QString" value="0.26" name="line_width"/>
            <Option type="QString" value="MM" name="line_width_unit"/>
            <Option type="QString" value="0" name="offset"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
            <Option type="QString" value="MM" name="offset_unit"/>
            <Option type="QString" value="0" name="ring_filter"/>
            <Option type="QString" value="0" name="trim_distance_end"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="trim_distance_end_map_unit_scale"/>
            <Option type="QString" value="MM" name="trim_distance_end_unit"/>
            <Option type="QString" value="0" name="trim_distance_start"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="trim_distance_start_map_unit_scale"/>
            <Option type="QString" value="MM" name="trim_distance_start_unit"/>
            <Option type="QString" value="0" name="tweak_dash_pattern_on_corners"/>
            <Option type="QString" value="0" name="use_custom_dash"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="width_map_unit_scale"/>
          </Option>
          <prop k="align_dash_pattern" v="0"/>
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="dash_pattern_offset" v="0"/>
          <prop k="dash_pattern_offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="dash_pattern_offset_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="0,0,0,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="0.26"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="ring_filter" v="0"/>
          <prop k="trim_distance_end" v="0"/>
          <prop k="trim_distance_end_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="trim_distance_end_unit" v="MM"/>
          <prop k="trim_distance_start" v="0"/>
          <prop k="trim_distance_start_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="trim_distance_start_unit" v="MM"/>
          <prop k="tweak_dash_pattern_on_corners" v="0"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option type="Map" name="properties">
                <Option type="Map" name="outlineColor">
                  <Option type="bool" value="true" name="active"/>
                  <Option type="QString" value="set_color_part(@value, 'alpha', (case when &quot;_model&quot;=@current_model then 1 else 0.4 end) * 255 )" name="expression"/>
                  <Option type="int" value="3" name="type"/>
                </Option>
                <Option type="Map" name="outlineWidth">
                  <Option type="bool" value="false" name="active"/>
                  <Option type="QString" value="" name="expression"/>
                  <Option type="int" value="3" name="type"/>
                </Option>
              </Option>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
        <layer locked="0" pass="0" class="MarkerLine" enabled="1">
          <Option type="Map">
            <Option type="QString" value="4" name="average_angle_length"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="average_angle_map_unit_scale"/>
            <Option type="QString" value="MM" name="average_angle_unit"/>
            <Option type="QString" value="3" name="interval"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="interval_map_unit_scale"/>
            <Option type="QString" value="MM" name="interval_unit"/>
            <Option type="QString" value="0" name="offset"/>
            <Option type="QString" value="0" name="offset_along_line"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_along_line_map_unit_scale"/>
            <Option type="QString" value="MM" name="offset_along_line_unit"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
            <Option type="QString" value="MM" name="offset_unit"/>
            <Option type="QString" value="centralpoint" name="placement"/>
            <Option type="QString" value="0" name="ring_filter"/>
            <Option type="QString" value="1" name="rotate"/>
          </Option>
          <prop k="average_angle_length" v="4"/>
          <prop k="average_angle_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="average_angle_unit" v="MM"/>
          <prop k="interval" v="3"/>
          <prop k="interval_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="interval_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_along_line" v="0"/>
          <prop k="offset_along_line_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_along_line_unit" v="MM"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="placement" v="centralpoint"/>
          <prop k="ring_filter" v="0"/>
          <prop k="rotate" v="1"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
          <symbol alpha="1" type="marker" clip_to_extent="1" force_rhr="0" name="@0@1">
            <data_defined_properties>
              <Option type="Map">
                <Option type="QString" value="" name="name"/>
                <Option name="properties"/>
                <Option type="QString" value="collection" name="type"/>
              </Option>
            </data_defined_properties>
            <layer locked="0" pass="0" class="SvgMarker" enabled="1">
              <Option type="Map">
                <Option type="QString" value="0" name="angle"/>
                <Option type="QString" value="255,255,255,255" name="color"/>
                <Option type="QString" value="0" name="fixedAspectRatio"/>
                <Option type="QString" value="1" name="horizontal_anchor_point"/>
                <Option type="QString" value="AEP_STATION_POMPAGE.svg" name="name"/>
                <Option type="QString" value="0,0" name="offset"/>
                <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
                <Option type="QString" value="MM" name="offset_unit"/>
                <Option type="QString" value="255,140,0,255" name="outline_color"/>
                <Option type="QString" value="0.4" name="outline_width"/>
                <Option type="QString" value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale"/>
                <Option type="QString" value="MM" name="outline_width_unit"/>
                <Option name="parameters"/>
                <Option type="QString" value="diameter" name="scale_method"/>
                <Option type="QString" value="4" name="size"/>
                <Option type="QString" value="3x:0,0,0,0,0,0" name="size_map_unit_scale"/>
                <Option type="QString" value="MM" name="size_unit"/>
                <Option type="QString" value="1" name="vertical_anchor_point"/>
              </Option>
              <prop k="angle" v="0"/>
              <prop k="color" v="255,255,255,255"/>
              <prop k="fixedAspectRatio" v="0"/>
              <prop k="horizontal_anchor_point" v="1"/>
              <prop k="name" v="AEP_STATION_POMPAGE.svg"/>
              <prop k="offset" v="0,0"/>
              <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="offset_unit" v="MM"/>
              <prop k="outline_color" v="255,140,0,255"/>
              <prop k="outline_width" v="0.4"/>
              <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="outline_width_unit" v="MM"/>
              <prop k="parameters" v=""/>
              <prop k="scale_method" v="diameter"/>
              <prop k="size" v="4"/>
              <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="size_unit" v="MM"/>
              <prop k="vertical_anchor_point" v="1"/>
              <data_defined_properties>
                <Option type="Map">
                  <Option type="QString" value="" name="name"/>
                  <Option type="Map" name="properties">
                    <Option type="Map" name="outlineColor">
                      <Option type="bool" value="true" name="active"/>
                      <Option type="QString" value="case when &quot;_model&quot;=@current_model then '#FF8C00' else '#ffdda9' end" name="expression"/>
                      <Option type="int" value="3" name="type"/>
                    </Option>
                  </Option>
                  <Option type="QString" value="collection" name="type"/>
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
      </symbol>
    </symbols>
    <rotation/>
    <sizescale/>
  </renderer-v2>
  <customproperties>
    <Option type="Map">
      <Option type="List" name="dualview/previewExpressions">
        <Option type="QString" value="&quot;name&quot;"/>
      </Option>
      <Option type="QString" value="0" name="embeddedWidgets/count"/>
      <Option name="variableNames"/>
      <Option name="variableValues"/>
    </Option>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer attributeLegend="1" diagramType="Histogram">
    <DiagramCategory sizeType="MM" direction="1" lineSizeScale="3x:0,0,0,0,0,0" opacity="1" showAxis="0" penColor="#000000" labelPlacementMethod="XHeight" barWidth="5" rotationOffset="270" backgroundColor="#ffffff" height="15" minScaleDenominator="0" scaleDependency="Area" width="15" spacingUnit="MM" minimumSize="0" enabled="0" maxScaleDenominator="1e+08" spacingUnitScale="3x:0,0,0,0,0,0" diagramOrientation="Up" spacing="0" penWidth="0" scaleBasedVisibility="0" sizeScale="3x:0,0,0,0,0,0" lineSizeType="MM" backgroundAlpha="255" penAlpha="255">
      <fontProperties description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" style=""/>
      <attribute field="" color="#000000" label=""/>
      <axisSymbol>
        <symbol alpha="1" type="line" clip_to_extent="1" force_rhr="0" name="">
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
          <layer locked="0" pass="0" class="SimpleLine" enabled="1">
            <Option type="Map">
              <Option type="QString" value="0" name="align_dash_pattern"/>
              <Option type="QString" value="square" name="capstyle"/>
              <Option type="QString" value="5;2" name="customdash"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="customdash_map_unit_scale"/>
              <Option type="QString" value="MM" name="customdash_unit"/>
              <Option type="QString" value="0" name="dash_pattern_offset"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="dash_pattern_offset_map_unit_scale"/>
              <Option type="QString" value="MM" name="dash_pattern_offset_unit"/>
              <Option type="QString" value="0" name="draw_inside_polygon"/>
              <Option type="QString" value="bevel" name="joinstyle"/>
              <Option type="QString" value="35,35,35,255" name="line_color"/>
              <Option type="QString" value="solid" name="line_style"/>
              <Option type="QString" value="0.26" name="line_width"/>
              <Option type="QString" value="MM" name="line_width_unit"/>
              <Option type="QString" value="0" name="offset"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
              <Option type="QString" value="MM" name="offset_unit"/>
              <Option type="QString" value="0" name="ring_filter"/>
              <Option type="QString" value="0" name="trim_distance_end"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="trim_distance_end_map_unit_scale"/>
              <Option type="QString" value="MM" name="trim_distance_end_unit"/>
              <Option type="QString" value="0" name="trim_distance_start"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="trim_distance_start_map_unit_scale"/>
              <Option type="QString" value="MM" name="trim_distance_start_unit"/>
              <Option type="QString" value="0" name="tweak_dash_pattern_on_corners"/>
              <Option type="QString" value="0" name="use_custom_dash"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="width_map_unit_scale"/>
            </Option>
            <prop k="align_dash_pattern" v="0"/>
            <prop k="capstyle" v="square"/>
            <prop k="customdash" v="5;2"/>
            <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <prop k="customdash_unit" v="MM"/>
            <prop k="dash_pattern_offset" v="0"/>
            <prop k="dash_pattern_offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <prop k="dash_pattern_offset_unit" v="MM"/>
            <prop k="draw_inside_polygon" v="0"/>
            <prop k="joinstyle" v="bevel"/>
            <prop k="line_color" v="35,35,35,255"/>
            <prop k="line_style" v="solid"/>
            <prop k="line_width" v="0.26"/>
            <prop k="line_width_unit" v="MM"/>
            <prop k="offset" v="0"/>
            <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <prop k="offset_unit" v="MM"/>
            <prop k="ring_filter" v="0"/>
            <prop k="trim_distance_end" v="0"/>
            <prop k="trim_distance_end_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <prop k="trim_distance_end_unit" v="MM"/>
            <prop k="trim_distance_start" v="0"/>
            <prop k="trim_distance_start_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <prop k="trim_distance_start_unit" v="MM"/>
            <prop k="tweak_dash_pattern_on_corners" v="0"/>
            <prop k="use_custom_dash" v="0"/>
            <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <data_defined_properties>
              <Option type="Map">
                <Option type="QString" value="" name="name"/>
                <Option name="properties"/>
                <Option type="QString" value="collection" name="type"/>
              </Option>
            </data_defined_properties>
          </layer>
        </symbol>
      </axisSymbol>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings obstacle="0" placement="2" priority="0" zIndex="0" linePlacementFlags="18" dist="0" showAll="1">
    <properties>
      <Option type="Map">
        <Option type="QString" value="" name="name"/>
        <Option name="properties"/>
        <Option type="QString" value="collection" name="type"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions removeDuplicateNodes="0" geometryPrecision="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <legend showLabelLegend="0" type="default-vector"/>
  <referencedLayers>
    <relation strength="Association" providerKey="postgres" id="Pump_1042291c_ed4a_4f98_86c5_043441f242e3_down_All_nodes_8e8f2e8a_42d1_4137_bad7_c8a6dc5f71d2_id" referencedLayer="All_nodes_8e8f2e8a_42d1_4137_bad7_c8a6dc5f71d2" layerId="All_nodes_8e8f2e8a_42d1_4137_bad7_c8a6dc5f71d2" name="Pump down" referencingLayer="Pump_1042291c_ed4a_4f98_86c5_043441f242e3" layerName="All nodes" dataSource="dbname='toto' service='expresseau' sslmode=disable key='id' checkPrimaryKeyUnicity='0' table=&quot;api&quot;.&quot;node&quot; (geom)">
      <fieldRef referencingField="down" referencedField="id"/>
    </relation>
    <relation strength="Association" providerKey="postgres" id="Pump_1042291c_ed4a_4f98_86c5_043441f242e3_target_node_All_nodes_8e8f2e8a_42d1_4137_bad7_c8a6dc5f71d2_id" referencedLayer="All_nodes_8e8f2e8a_42d1_4137_bad7_c8a6dc5f71d2" layerId="All_nodes_8e8f2e8a_42d1_4137_bad7_c8a6dc5f71d2" name="Pump target node" referencingLayer="Pump_1042291c_ed4a_4f98_86c5_043441f242e3" layerName="All nodes" dataSource="dbname='toto' service='expresseau' sslmode=disable key='id' checkPrimaryKeyUnicity='0' table=&quot;api&quot;.&quot;node&quot; (geom)">
      <fieldRef referencingField="target_node" referencedField="id"/>
    </relation>
    <relation strength="Association" providerKey="postgres" id="Pump_1042291c_ed4a_4f98_86c5_043441f242e3_up_All_nodes_8e8f2e8a_42d1_4137_bad7_c8a6dc5f71d2_id" referencedLayer="All_nodes_8e8f2e8a_42d1_4137_bad7_c8a6dc5f71d2" layerId="All_nodes_8e8f2e8a_42d1_4137_bad7_c8a6dc5f71d2" name="Pump up" referencingLayer="Pump_1042291c_ed4a_4f98_86c5_043441f242e3" layerName="All nodes" dataSource="dbname='toto' service='expresseau' sslmode=disable key='id' checkPrimaryKeyUnicity='0' table=&quot;api&quot;.&quot;node&quot; (geom)">
      <fieldRef referencingField="up" referencedField="id"/>
    </relation>
  </referencedLayers>
  <fieldConfiguration>
    <field configurationFlags="None" name="id">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="name">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="pq_array">
      <editWidget type="Array">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="check_valve">
      <editWidget type="CheckBox">
        <config>
          <Option type="Map">
            <Option type="QString" value="" name="CheckedState"/>
            <Option type="int" value="0" name="TextDisplayMethod"/>
            <Option type="QString" value="" name="UncheckedState"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="rotation_speed">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="inertia">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="pump_regulation_option">
      <editWidget type="Enumeration">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="speed_reduction_coef">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="regul_q">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="target_node">
      <editWidget type="RelationReference">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="AllowAddFeatures"/>
            <Option type="bool" value="true" name="AllowNULL"/>
            <Option type="bool" value="true" name="MapIdentification"/>
            <Option type="bool" value="false" name="OrderByValue"/>
            <Option type="bool" value="false" name="ReadOnly"/>
            <Option type="QString" value="dbname='toto' service='expresseau' sslmode=disable key='id' checkPrimaryKeyUnicity='0' table=&quot;api&quot;.&quot;node&quot; (geom)" name="ReferencedLayerDataSource"/>
            <Option type="QString" value="All_nodes_8e8f2e8a_42d1_4137_bad7_c8a6dc5f71d2" name="ReferencedLayerId"/>
            <Option type="QString" value="All nodes" name="ReferencedLayerName"/>
            <Option type="QString" value="postgres" name="ReferencedLayerProviderKey"/>
            <Option type="QString" value="Pump_1042291c_ed4a_4f98_86c5_043441f242e3_target_node_All_nodes_8e8f2e8a_42d1_4137_bad7_c8a6dc5f71d2_id" name="Relation"/>
            <Option type="bool" value="false" name="ShowForm"/>
            <Option type="bool" value="false" name="ShowOpenFormButton"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="regul_z_start">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="regul_z_stop">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="regul_p">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="external_file">
      <editWidget type="CheckBox">
        <config>
          <Option type="Map">
            <Option type="QString" value="" name="CheckedState"/>
            <Option type="int" value="0" name="TextDisplayMethod"/>
            <Option type="QString" value="" name="UncheckedState"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="cyclic">
      <editWidget type="CheckBox">
        <config>
          <Option type="Map">
            <Option type="QString" value="" name="CheckedState"/>
            <Option type="int" value="0" name="TextDisplayMethod"/>
            <Option type="QString" value="" name="UncheckedState"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="regul_z">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="regul_proportional">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="regul_integral">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="regul_derivative">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="comment">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="true" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="down">
      <editWidget type="RelationReference">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="AllowAddFeatures"/>
            <Option type="bool" value="false" name="AllowNULL"/>
            <Option type="bool" value="true" name="MapIdentification"/>
            <Option type="bool" value="false" name="OrderByValue"/>
            <Option type="bool" value="true" name="ReadOnly"/>
            <Option type="QString" value="dbname='toto' service='expresseau' sslmode=disable key='id' checkPrimaryKeyUnicity='0' table=&quot;api&quot;.&quot;node&quot; (geom)" name="ReferencedLayerDataSource"/>
            <Option type="QString" value="All_nodes_8e8f2e8a_42d1_4137_bad7_c8a6dc5f71d2" name="ReferencedLayerId"/>
            <Option type="QString" value="All nodes" name="ReferencedLayerName"/>
            <Option type="QString" value="postgres" name="ReferencedLayerProviderKey"/>
            <Option type="QString" value="Pump_1042291c_ed4a_4f98_86c5_043441f242e3_down_All_nodes_8e8f2e8a_42d1_4137_bad7_c8a6dc5f71d2_id" name="Relation"/>
            <Option type="bool" value="false" name="ShowForm"/>
            <Option type="bool" value="false" name="ShowOpenFormButton"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="up">
      <editWidget type="RelationReference">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="AllowAddFeatures"/>
            <Option type="bool" value="false" name="AllowNULL"/>
            <Option type="bool" value="true" name="MapIdentification"/>
            <Option type="bool" value="false" name="OrderByValue"/>
            <Option type="bool" value="true" name="ReadOnly"/>
            <Option type="QString" value="dbname='toto' service='expresseau' sslmode=disable key='id' checkPrimaryKeyUnicity='0' table=&quot;api&quot;.&quot;node&quot; (geom)" name="ReferencedLayerDataSource"/>
            <Option type="QString" value="All_nodes_8e8f2e8a_42d1_4137_bad7_c8a6dc5f71d2" name="ReferencedLayerId"/>
            <Option type="QString" value="All nodes" name="ReferencedLayerName"/>
            <Option type="QString" value="postgres" name="ReferencedLayerProviderKey"/>
            <Option type="QString" value="Pump_1042291c_ed4a_4f98_86c5_043441f242e3_up_All_nodes_8e8f2e8a_42d1_4137_bad7_c8a6dc5f71d2_id" name="Relation"/>
            <Option type="bool" value="false" name="ShowForm"/>
            <Option type="bool" value="false" name="ShowOpenFormButton"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="_model">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias index="0" field="id" name=""/>
    <alias index="1" field="name" name="Name"/>
    <alias index="2" field="pq_array" name="Pump curve"/>
    <alias index="3" field="check_valve" name="Check valve ?"/>
    <alias index="4" field="rotation_speed" name="Rotation speed (rpm)"/>
    <alias index="5" field="inertia" name="Inertia (kg.m²)"/>
    <alias index="6" field="pump_regulation_option" name="Pump regulation option"/>
    <alias index="7" field="speed_reduction_coef" name="Rotation speed factor (0-> 1)"/>
    <alias index="8" field="regul_q" name="Controlledl flow rate (m3/h)"/>
    <alias index="9" field="target_node" name="Target node"/>
    <alias index="10" field="regul_z_start" name="Pump Z start (m)"/>
    <alias index="11" field="regul_z_stop" name="Pump Z stop (m)"/>
    <alias index="12" field="regul_p" name="Controlled pressure (mCE)"/>
    <alias index="13" field="external_file" name="External file"/>
    <alias index="14" field="cyclic" name="Cyclic"/>
    <alias index="15" field="regul_z" name="Water level (mNGF)"/>
    <alias index="16" field="regul_proportional" name="Proportional (m3/s)"/>
    <alias index="17" field="regul_integral" name="Integral (m3/s/s)"/>
    <alias index="18" field="regul_derivative" name="Derivative (m3)"/>
    <alias index="19" field="comment" name=""/>
    <alias index="20" field="down" name="Node down"/>
    <alias index="21" field="up" name="Node up"/>
    <alias index="22" field="_model" name="Model"/>
  </aliases>
  <defaults>
    <default field="id" expression="" applyOnUpdate="0"/>
    <default field="name" expression="" applyOnUpdate="0"/>
    <default field="pq_array" expression="" applyOnUpdate="0"/>
    <default field="check_valve" expression="" applyOnUpdate="0"/>
    <default field="rotation_speed" expression="" applyOnUpdate="0"/>
    <default field="inertia" expression="" applyOnUpdate="0"/>
    <default field="pump_regulation_option" expression="" applyOnUpdate="0"/>
    <default field="speed_reduction_coef" expression="" applyOnUpdate="0"/>
    <default field="regul_q" expression="" applyOnUpdate="0"/>
    <default field="target_node" expression="" applyOnUpdate="0"/>
    <default field="regul_z_start" expression="" applyOnUpdate="0"/>
    <default field="regul_z_stop" expression="" applyOnUpdate="0"/>
    <default field="regul_p" expression="" applyOnUpdate="0"/>
    <default field="external_file" expression="" applyOnUpdate="0"/>
    <default field="cyclic" expression="" applyOnUpdate="0"/>
    <default field="regul_z" expression="" applyOnUpdate="0"/>
    <default field="regul_proportional" expression="" applyOnUpdate="0"/>
    <default field="regul_integral" expression="" applyOnUpdate="0"/>
    <default field="regul_derivative" expression="" applyOnUpdate="0"/>
    <default field="comment" expression="" applyOnUpdate="0"/>
    <default field="down" expression="attribute(&#xd;&#xa;&#x9;array_first(&#xd;&#xa;&#x9;&#x9;array_filter(&#xd;&#xa;&#x9;&#x9;&#x9;overlay_intersects(&#xd;&#xa;&#x9;&#x9;&#x9;&#x9;&#x9;layer:='All_nodes_8e8f2e8a_42d1_4137_bad7_c8a6dc5f71d2',&#xd;&#xa;&#x9;&#x9;&#x9;&#x9;&#x9;expression:=$currentfeature&#xd;&#xa;&#x9;&#x9;&#x9;),&#xd;&#xa;&#x9;&#x9;&#x9;intersects(end_point($geometry), geometry(@element))&#xd;&#xa;&#x9;&#x9;)&#xd;&#xa;&#x9;),&#xd;&#xa;&#x9;'id'&#xd;&#xa;)" applyOnUpdate="1"/>
    <default field="up" expression="attribute(&#xd;&#xa;&#x9;array_first(&#xd;&#xa;&#x9;&#x9;array_filter(&#xd;&#xa;&#x9;&#x9;&#x9;overlay_intersects(&#xd;&#xa;&#x9;&#x9;&#x9;&#x9;&#x9;layer:='All_nodes_8e8f2e8a_42d1_4137_bad7_c8a6dc5f71d2',&#xd;&#xa;&#x9;&#x9;&#x9;&#x9;&#x9;expression:=$currentfeature&#xd;&#xa;&#x9;&#x9;&#x9;),&#xd;&#xa;&#x9;&#x9;&#x9;intersects(start_point($geometry), geometry(@element))&#xd;&#xa;&#x9;&#x9;)&#xd;&#xa;&#x9;),&#xd;&#xa;&#x9;'id'&#xd;&#xa;)" applyOnUpdate="1"/>
    <default field="_model" expression="attribute(&#xd;&#xa;&#x9;array_first(&#xd;&#xa;&#x9;&#x9;array_filter(&#xd;&#xa;&#x9;&#x9;&#x9;overlay_intersects(&#xd;&#xa;&#x9;&#x9;&#x9;&#x9;&#x9;layer:='All_nodes_8e8f2e8a_42d1_4137_bad7_c8a6dc5f71d2',&#xd;&#xa;&#x9;&#x9;&#x9;&#x9;&#x9;expression:=$currentfeature&#xd;&#xa;&#x9;&#x9;&#x9;),&#xd;&#xa;&#x9;&#x9;&#x9;intersects(start_point($geometry), geometry(@element))&#xd;&#xa;&#x9;&#x9;)&#xd;&#xa;&#x9;),&#xd;&#xa;&#x9;'model'&#xd;&#xa;)" applyOnUpdate="0"/>
  </defaults>
  <constraints>
    <constraint notnull_strength="1" constraints="3" field="id" unique_strength="1" exp_strength="0"/>
    <constraint notnull_strength="1" constraints="1" field="name" unique_strength="0" exp_strength="0"/>
    <constraint notnull_strength="0" constraints="4" field="pq_array" unique_strength="0" exp_strength="1"/>
    <constraint notnull_strength="1" constraints="1" field="check_valve" unique_strength="0" exp_strength="0"/>
    <constraint notnull_strength="1" constraints="5" field="rotation_speed" unique_strength="0" exp_strength="1"/>
    <constraint notnull_strength="1" constraints="5" field="inertia" unique_strength="0" exp_strength="1"/>
    <constraint notnull_strength="1" constraints="1" field="pump_regulation_option" unique_strength="0" exp_strength="0"/>
    <constraint notnull_strength="0" constraints="4" field="speed_reduction_coef" unique_strength="0" exp_strength="1"/>
    <constraint notnull_strength="0" constraints="4" field="regul_q" unique_strength="0" exp_strength="1"/>
    <constraint notnull_strength="0" constraints="4" field="target_node" unique_strength="0" exp_strength="1"/>
    <constraint notnull_strength="0" constraints="4" field="regul_z_start" unique_strength="0" exp_strength="1"/>
    <constraint notnull_strength="0" constraints="4" field="regul_z_stop" unique_strength="0" exp_strength="1"/>
    <constraint notnull_strength="0" constraints="4" field="regul_p" unique_strength="0" exp_strength="1"/>
    <constraint notnull_strength="0" constraints="0" field="external_file" unique_strength="0" exp_strength="0"/>
    <constraint notnull_strength="0" constraints="0" field="cyclic" unique_strength="0" exp_strength="0"/>
    <constraint notnull_strength="0" constraints="4" field="regul_z" unique_strength="0" exp_strength="1"/>
    <constraint notnull_strength="0" constraints="4" field="regul_proportional" unique_strength="0" exp_strength="1"/>
    <constraint notnull_strength="0" constraints="4" field="regul_integral" unique_strength="0" exp_strength="1"/>
    <constraint notnull_strength="0" constraints="4" field="regul_derivative" unique_strength="0" exp_strength="1"/>
    <constraint notnull_strength="0" constraints="0" field="comment" unique_strength="0" exp_strength="0"/>
    <constraint notnull_strength="1" constraints="1" field="down" unique_strength="0" exp_strength="0"/>
    <constraint notnull_strength="1" constraints="1" field="up" unique_strength="0" exp_strength="0"/>
    <constraint notnull_strength="0" constraints="0" field="_model" unique_strength="0" exp_strength="0"/>
  </constraints>
  <constraintExpressions>
    <constraint field="id" desc="" exp=""/>
    <constraint field="name" desc="" exp=""/>
    <constraint field="pq_array" desc="" exp="pg_array(pq_array)[0][1] = 0&#xd;&#xa;and&#xd;&#xa;pg_array(pq_array)[-1][0] = 0&#xd;&#xa;and&#xd;&#xa;pg_array_sorted(pq_array,1,'asc')&#xd;&#xa;and&#xd;&#xa;pg_array_sorted(pq_array,0,'desc')&#xd;&#xa;and&#xd;&#xa;pg_array(pq_array)[0][0] != 0&#xd;&#xa;and&#xd;&#xa;pg_array(pq_array)[-1][1] != 0&#xd;&#xa;&#xd;&#xa;"/>
    <constraint field="check_valve" desc="" exp=""/>
    <constraint field="rotation_speed" desc="" exp="&quot;rotation_speed&quot; >= 0"/>
    <constraint field="inertia" desc="" exp="&quot;inertia&quot; >= 0"/>
    <constraint field="pump_regulation_option" desc="" exp=""/>
    <constraint field="speed_reduction_coef" desc="" exp="&quot;pump_regulation_option&quot; != 'No regulation' OR ( &quot;speed_reduction_coef&quot; is not null AND &quot;speed_reduction_coef&quot; >= 0 AND  &quot;speed_reduction_coef&quot; &lt;= 1)"/>
    <constraint field="regul_q" desc="" exp="&quot;pump_regulation_option&quot; != 'Flow regulation' OR ( &quot;regul_q&quot; is not null AND &quot;regul_q&quot; >= 0)"/>
    <constraint field="target_node" desc="" exp="(&quot;pump_regulation_option&quot; in ('No regulation', 'Flow regulation')) OR &quot;target_node&quot; is not null"/>
    <constraint field="regul_z_start" desc="" exp="&quot;pump_regulation_option&quot; != 'Stop-start regulation' OR &quot;regul_z_start&quot; is not null"/>
    <constraint field="regul_z_stop" desc="" exp="&quot;pump_regulation_option&quot; != 'Stop-start regulation' OR &quot;regul_z_stop&quot; is not null"/>
    <constraint field="regul_p" desc="" exp="&quot;pump_regulation_option&quot; != 'Pressure regulation' OR ( &quot;regul_p&quot; is not null AND &quot;regul_p&quot; >= 0)"/>
    <constraint field="external_file" desc="" exp=""/>
    <constraint field="cyclic" desc="" exp=""/>
    <constraint field="regul_z" desc="" exp="(pump_regulation_option != 'Water level in reservoir') or regul_z is not null"/>
    <constraint field="regul_proportional" desc="" exp="(pump_regulation_option != 'Water level in reservoir') or regul_proportional is not null"/>
    <constraint field="regul_integral" desc="" exp="(pump_regulation_option != 'Water level in reservoir') or regul_integral is not null"/>
    <constraint field="regul_derivative" desc="" exp="(pump_regulation_option != 'Water level in reservoir') or regul_derivative is not null"/>
    <constraint field="comment" desc="" exp=""/>
    <constraint field="down" desc="" exp=""/>
    <constraint field="up" desc="" exp=""/>
    <constraint field="_model" desc="" exp=""/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction key="Canvas" value="{00000000-0000-0000-0000-000000000000}"/>
    <actionsetting type="1" isEnabledOnlyWhenEditable="0" capture="0" notificationMessage="" id="{2b4fc019-e1cb-435e-b3a8-88c1edce4e13}" icon="" action="from expresseau.gui.result_dialog import ResultWindow&#xa;&#xa;if 'result_window' in locals() and result_window.isVisible():&#xa;    result_window.set_result([%id%],&#xa;                    '[% decode_uri(@layer, 'table')%]',&#xa;                    '[% @current_scenario%]')&#xa;    result_window.hide()&#xa;    result_window.show()&#xa;else:&#xa;    result_window = ResultWindow([%id%],&#xa;                    '[% decode_uri(@layer, 'table')%]',&#xa;                    '[% @current_scenario%]',&#xa;                    '[% @project_basename%]')&#xa;    result_window.open()&#xa;" name="Open results" shortTitle="">
      <actionScope id="Feature"/>
      <actionScope id="Layer"/>
    </actionsetting>
  </attributeactions>
  <attributetableconfig actionWidgetStyle="dropDown" sortOrder="1" sortExpression="&quot;_model&quot;">
    <columns>
      <column width="-1" hidden="0" type="field" name="name"/>
      <column width="-1" hidden="0" type="field" name="up"/>
      <column width="-1" hidden="0" type="field" name="down"/>
      <column width="-1" hidden="0" type="field" name="pq_array"/>
      <column width="-1" hidden="0" type="field" name="speed_reduction_coef"/>
      <column width="-1" hidden="0" type="field" name="check_valve"/>
      <column width="-1" hidden="0" type="field" name="rotation_speed"/>
      <column width="-1" hidden="0" type="field" name="inertia"/>
      <column width="-1" hidden="0" type="field" name="pump_regulation_option"/>
      <column width="-1" hidden="0" type="field" name="regul_q"/>
      <column width="-1" hidden="0" type="field" name="regul_p"/>
      <column width="-1" hidden="1" type="actions"/>
      <column width="-1" hidden="0" type="field" name="comment"/>
      <column width="-1" hidden="0" type="field" name="target_node"/>
      <column width="-1" hidden="0" type="field" name="regul_z_start"/>
      <column width="-1" hidden="0" type="field" name="regul_z_stop"/>
      <column width="-1" hidden="0" type="field" name="id"/>
      <column width="-1" hidden="0" type="field" name="external_file"/>
      <column width="-1" hidden="0" type="field" name="cyclic"/>
      <column width="-1" hidden="0" type="field" name="regul_z"/>
      <column width="-1" hidden="0" type="field" name="regul_proportional"/>
      <column width="-1" hidden="0" type="field" name="regul_integral"/>
      <column width="-1" hidden="0" type="field" name="regul_derivative"/>
      <column width="-1" hidden="0" type="field" name="_model"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit>form_open</editforminit>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
QGIS forms can have a Python function that is called when the form is
opened.

Use this function to add extra logic to your forms.

Enter the name of the function in the "Python Init function"
field.
An example follows:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
    geom = feature.geometry()
    control = dialog.findChild(QWidget, "MyLineEdit")
]]></editforminitcode>
  <featformsuppress>2</featformsuppress>
  <editorlayout>tablayout</editorlayout>
  <attributeEditorForm>
    <attributeEditorField index="1" showLabel="1" name="name"/>
    <attributeEditorField index="22" showLabel="1" name="_model"/>
    <attributeEditorField index="19" showLabel="1" name="comment"/>
    <attributeEditorContainer showLabel="1" columnCount="2" visibilityExpressionEnabled="0" visibilityExpression="" name="Nodes" groupBox="1">
      <attributeEditorField index="21" showLabel="1" name="up"/>
      <attributeEditorField index="20" showLabel="1" name="down"/>
    </attributeEditorContainer>
    <attributeEditorField index="2" showLabel="1" name="pq_array"/>
    <attributeEditorField index="3" showLabel="1" name="check_valve"/>
    <attributeEditorField index="4" showLabel="1" name="rotation_speed"/>
    <attributeEditorField index="5" showLabel="1" name="inertia"/>
    <attributeEditorField index="6" showLabel="1" name="pump_regulation_option"/>
    <attributeEditorContainer showLabel="1" columnCount="1" visibilityExpressionEnabled="1" visibilityExpression="&quot;pump_regulation_option&quot; = 'No regulation'" name="No regulation" groupBox="1">
      <attributeEditorField index="7" showLabel="1" name="speed_reduction_coef"/>
    </attributeEditorContainer>
    <attributeEditorContainer showLabel="1" columnCount="1" visibilityExpressionEnabled="1" visibilityExpression="&quot;pump_regulation_option&quot; = 'Flow regulation'" name="Flow regulation" groupBox="1">
      <attributeEditorField index="8" showLabel="1" name="regul_q"/>
    </attributeEditorContainer>
    <attributeEditorContainer showLabel="1" columnCount="3" visibilityExpressionEnabled="1" visibilityExpression="&quot;pump_regulation_option&quot; = 'Stop-start regulation'" name="Stop-start regulation" groupBox="1">
      <attributeEditorField index="9" showLabel="1" name="target_node"/>
      <attributeEditorField index="10" showLabel="1" name="regul_z_start"/>
      <attributeEditorField index="11" showLabel="1" name="regul_z_stop"/>
    </attributeEditorContainer>
    <attributeEditorContainer showLabel="1" columnCount="4" visibilityExpressionEnabled="1" visibilityExpression="&quot;pump_regulation_option&quot; = 'Pressure regulation'" name="Pressure regulation" groupBox="1">
      <attributeEditorField index="9" showLabel="1" name="target_node"/>
      <attributeEditorField index="12" showLabel="1" name="regul_p"/>
      <attributeEditorField index="13" showLabel="1" name="external_file"/>
      <attributeEditorField index="14" showLabel="1" name="cyclic"/>
    </attributeEditorContainer>
    <attributeEditorContainer showLabel="1" columnCount="4" visibilityExpressionEnabled="1" visibilityExpression="&quot;pump_regulation_option&quot; = 'Water level in reservoir'" name="Water level in reservoir" groupBox="1">
      <attributeEditorField index="9" showLabel="1" name="target_node"/>
      <attributeEditorField index="15" showLabel="1" name="regul_z"/>
      <attributeEditorField index="13" showLabel="1" name="external_file"/>
      <attributeEditorField index="14" showLabel="1" name="cyclic"/>
      <attributeEditorField index="16" showLabel="1" name="regul_proportional"/>
      <attributeEditorField index="17" showLabel="1" name="regul_integral"/>
      <attributeEditorField index="18" showLabel="1" name="regul_derivative"/>
    </attributeEditorContainer>
  </attributeEditorForm>
  <editable>
    <field editable="1" name="_id"/>
    <field editable="0" name="_model"/>
    <field editable="1" name="check_valve"/>
    <field editable="1" name="comment"/>
    <field editable="1" name="configuration"/>
    <field editable="1" name="configuration_json"/>
    <field editable="1" name="cyclic"/>
    <field editable="0" name="down"/>
    <field editable="1" name="external_file"/>
    <field editable="1" name="id"/>
    <field editable="1" name="inertia"/>
    <field editable="1" name="model"/>
    <field editable="1" name="name"/>
    <field editable="1" name="pq_array"/>
    <field editable="1" name="pump_regulation_option"/>
    <field editable="1" name="regul_derivative"/>
    <field editable="1" name="regul_integral"/>
    <field editable="1" name="regul_p"/>
    <field editable="1" name="regul_proportional"/>
    <field editable="1" name="regul_q"/>
    <field editable="1" name="regul_z"/>
    <field editable="1" name="regul_z_start"/>
    <field editable="1" name="regul_z_stop"/>
    <field editable="1" name="rotation_speed"/>
    <field editable="1" name="speed_reduction_coef"/>
    <field editable="1" name="target_node"/>
    <field editable="0" name="up"/>
    <field editable="1" name="validity"/>
  </editable>
  <labelOnTop>
    <field labelOnTop="0" name="_id"/>
    <field labelOnTop="0" name="_model"/>
    <field labelOnTop="0" name="check_valve"/>
    <field labelOnTop="0" name="comment"/>
    <field labelOnTop="0" name="configuration"/>
    <field labelOnTop="0" name="configuration_json"/>
    <field labelOnTop="0" name="cyclic"/>
    <field labelOnTop="0" name="down"/>
    <field labelOnTop="0" name="external_file"/>
    <field labelOnTop="0" name="id"/>
    <field labelOnTop="0" name="inertia"/>
    <field labelOnTop="0" name="model"/>
    <field labelOnTop="0" name="name"/>
    <field labelOnTop="0" name="pq_array"/>
    <field labelOnTop="0" name="pump_regulation_option"/>
    <field labelOnTop="0" name="regul_derivative"/>
    <field labelOnTop="0" name="regul_integral"/>
    <field labelOnTop="0" name="regul_p"/>
    <field labelOnTop="0" name="regul_proportional"/>
    <field labelOnTop="0" name="regul_q"/>
    <field labelOnTop="0" name="regul_z"/>
    <field labelOnTop="0" name="regul_z_start"/>
    <field labelOnTop="0" name="regul_z_stop"/>
    <field labelOnTop="0" name="rotation_speed"/>
    <field labelOnTop="0" name="speed_reduction_coef"/>
    <field labelOnTop="0" name="target_node"/>
    <field labelOnTop="0" name="up"/>
    <field labelOnTop="0" name="validity"/>
  </labelOnTop>
  <reuseLastValue>
    <field reuseLastValue="0" name="_model"/>
    <field reuseLastValue="0" name="check_valve"/>
    <field reuseLastValue="0" name="comment"/>
    <field reuseLastValue="0" name="cyclic"/>
    <field reuseLastValue="0" name="down"/>
    <field reuseLastValue="0" name="external_file"/>
    <field reuseLastValue="0" name="id"/>
    <field reuseLastValue="0" name="inertia"/>
    <field reuseLastValue="0" name="name"/>
    <field reuseLastValue="0" name="pq_array"/>
    <field reuseLastValue="0" name="pump_regulation_option"/>
    <field reuseLastValue="0" name="regul_derivative"/>
    <field reuseLastValue="0" name="regul_integral"/>
    <field reuseLastValue="0" name="regul_p"/>
    <field reuseLastValue="0" name="regul_proportional"/>
    <field reuseLastValue="0" name="regul_q"/>
    <field reuseLastValue="0" name="regul_z"/>
    <field reuseLastValue="0" name="regul_z_start"/>
    <field reuseLastValue="0" name="regul_z_stop"/>
    <field reuseLastValue="0" name="rotation_speed"/>
    <field reuseLastValue="0" name="speed_reduction_coef"/>
    <field reuseLastValue="0" name="target_node"/>
    <field reuseLastValue="0" name="up"/>
  </reuseLastValue>
  <dataDefinedFieldProperties/>
  <widgets/>
  <previewExpression>"name"</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>1</layerGeometryType>
</qgis>
