<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis version="3.22.8-Białowieża" styleCategories="AllStyleCategories" hasScaleBasedVisibilityFlag="0" maxScale="0" minScale="1e+08" readOnly="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
    <Private>0</Private>
  </flags>
  <temporal endField="" endExpression="" durationField="" durationUnit="min" enabled="0" mode="0" startField="" accumulate="0" limitMode="0" startExpression="" fixedDuration="0">
    <fixedRange>
      <start></start>
      <end></end>
    </fixedRange>
  </temporal>
  <customproperties>
    <Option type="Map">
      <Option type="int" name="embeddedWidgets/count" value="0"/>
      <Option type="invalid" name="variableNames"/>
      <Option type="invalid" name="variableValues"/>
    </Option>
  </customproperties>
  <geometryOptions removeDuplicateNodes="0" geometryPrecision="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <legend type="default-vector" showLabelLegend="0"/>
  <referencedLayers/>
  <fieldConfiguration>
    <field name="pipe_maximum_speed" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="pipe_minimum_speed" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="node_maximum_pressure" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="node_minimum_pressure" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="minimum_service_pressure" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="id" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option type="Map">
            <Option type="bool" name="AllowNull" value="true"/>
            <Option type="int" name="Max" value="2147483647"/>
            <Option type="int" name="Min" value="-2147483648"/>
            <Option type="int" name="Precision" value="0"/>
            <Option type="int" name="Step" value="1"/>
            <Option type="QString" name="Style" value="SpinBox"/>
          </Option>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias field="pipe_maximum_speed" name="Pipe maximum speed (m/s)" index="0"/>
    <alias field="pipe_minimum_speed" name="Pipe minimum speed (m/s)" index="1"/>
    <alias field="node_maximum_pressure" name="Node maximum pressure (mCE)" index="2"/>
    <alias field="node_minimum_pressure" name="Node minimum pressure (mCE)" index="3"/>
    <alias field="minimum_service_pressure" name="Minimum service pressure (mCE)" index="4"/>
    <alias field="id" name="" index="5"/>
  </aliases>
  <defaults>
    <default applyOnUpdate="0" field="pipe_maximum_speed" expression=""/>
    <default applyOnUpdate="0" field="pipe_minimum_speed" expression=""/>
    <default applyOnUpdate="0" field="node_maximum_pressure" expression=""/>
    <default applyOnUpdate="0" field="node_minimum_pressure" expression=""/>
    <default applyOnUpdate="0" field="minimum_service_pressure" expression=""/>
    <default applyOnUpdate="0" field="id" expression=""/>
  </defaults>
  <constraints>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" field="pipe_maximum_speed" constraints="0"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" field="pipe_minimum_speed" constraints="0"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" field="node_maximum_pressure" constraints="0"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" field="node_minimum_pressure" constraints="0"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" field="minimum_service_pressure" constraints="0"/>
    <constraint unique_strength="1" notnull_strength="1" exp_strength="0" field="id" constraints="3"/>
  </constraints>
  <constraintExpressions>
    <constraint desc="" field="pipe_maximum_speed" exp=""/>
    <constraint desc="" field="pipe_minimum_speed" exp=""/>
    <constraint desc="" field="node_maximum_pressure" exp=""/>
    <constraint desc="" field="node_minimum_pressure" exp=""/>
    <constraint desc="" field="minimum_service_pressure" exp=""/>
    <constraint desc="" field="id" exp=""/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction key="Canvas" value="{00000000-0000-0000-0000-000000000000}"/>
  </attributeactions>
  <attributetableconfig sortExpression="&quot;pipe_maximum_speed&quot;" sortOrder="0" actionWidgetStyle="dropDown">
    <columns>
      <column type="field" width="-1" name="pipe_maximum_speed" hidden="0"/>
      <column type="field" width="-1" name="pipe_minimum_speed" hidden="0"/>
      <column type="field" width="-1" name="node_maximum_pressure" hidden="0"/>
      <column type="field" width="-1" name="node_minimum_pressure" hidden="0"/>
      <column type="field" width="-1" name="minimum_service_pressure" hidden="0"/>
      <column type="field" width="-1" name="id" hidden="0"/>
      <column type="actions" width="-1" hidden="1"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appelée à l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalités à vos formulaires.

Entrez le nom de la fonction dans le champ "Fonction d'initialisation Python".
Voici un exemple à suivre:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
    geom = feature.geometry()
    control = dialog.findChild(QWidget, "MyLineEdit")

]]></editforminitcode>
  <featformsuppress>2</featformsuppress>
  <editorlayout>tablayout</editorlayout>
  <attributeEditorForm>
    <attributeEditorContainer visibilityExpressionEnabled="0" visibilityExpression="" name="Pipes" columnCount="2" groupBox="1" showLabel="1">
      <attributeEditorField name="pipe_maximum_speed" index="0" showLabel="1"/>
      <attributeEditorField name="pipe_minimum_speed" index="1" showLabel="1"/>
    </attributeEditorContainer>
    <attributeEditorContainer visibilityExpressionEnabled="0" visibilityExpression="" name="Nodes" columnCount="2" groupBox="1" showLabel="1">
      <attributeEditorField name="node_maximum_pressure" index="2" showLabel="1"/>
      <attributeEditorField name="node_minimum_pressure" index="3" showLabel="1"/>
    </attributeEditorContainer>
    <attributeEditorContainer visibilityExpressionEnabled="0" visibilityExpression="" name="Delivery point" columnCount="2" groupBox="1" showLabel="1">
      <attributeEditorField name="minimum_service_pressure" index="4" showLabel="1"/>
    </attributeEditorContainer>
  </attributeEditorForm>
  <editable>
    <field name="id" editable="1"/>
    <field name="minimum_service_pressure" editable="1"/>
    <field name="node_maximum_pressure" editable="1"/>
    <field name="node_minimum_pressure" editable="1"/>
    <field name="pipe_maximum_speed" editable="1"/>
    <field name="pipe_minimum_speed" editable="1"/>
  </editable>
  <labelOnTop>
    <field labelOnTop="0" name="id"/>
    <field labelOnTop="0" name="minimum_service_pressure"/>
    <field labelOnTop="0" name="node_maximum_pressure"/>
    <field labelOnTop="0" name="node_minimum_pressure"/>
    <field labelOnTop="0" name="pipe_maximum_speed"/>
    <field labelOnTop="0" name="pipe_minimum_speed"/>
  </labelOnTop>
  <reuseLastValue>
    <field name="id" reuseLastValue="0"/>
    <field name="minimum_service_pressure" reuseLastValue="0"/>
    <field name="node_maximum_pressure" reuseLastValue="0"/>
    <field name="node_minimum_pressure" reuseLastValue="0"/>
    <field name="pipe_maximum_speed" reuseLastValue="0"/>
    <field name="pipe_minimum_speed" reuseLastValue="0"/>
  </reuseLastValue>
  <dataDefinedFieldProperties/>
  <widgets/>
  <previewExpression>"id"</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>4</layerGeometryType>
</qgis>
