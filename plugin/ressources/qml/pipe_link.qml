<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis symbologyReferenceScale="-1" readOnly="0" maxScale="0" simplifyAlgorithm="0" styleCategories="AllStyleCategories" simplifyMaxScale="1" version="3.28.8-Firenze" simplifyDrawingHints="1" minScale="100000000" labelsEnabled="0" simplifyLocal="1" simplifyDrawingTol="1" hasScaleBasedVisibilityFlag="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
    <Private>0</Private>
  </flags>
  <temporal startField="" accumulate="0" durationField="" limitMode="0" fixedDuration="0" mode="0" endExpression="" durationUnit="min" endField="" startExpression="" enabled="0">
    <fixedRange>
      <start></start>
      <end></end>
    </fixedRange>
  </temporal>
  <elevation type="IndividualFeatures" showMarkerSymbolInSurfacePlots="0" zoffset="0" zscale="1" symbology="Line" extrusion="0" clamping="Terrain" extrusionEnabled="0" binding="Centroid" respectLayerSymbol="1">
    <data-defined-properties>
      <Option type="Map">
        <Option type="QString" value="" name="name"/>
        <Option name="properties"/>
        <Option type="QString" value="collection" name="type"/>
      </Option>
    </data-defined-properties>
    <profileLineSymbol>
      <symbol type="line" name="" is_animated="0" clip_to_extent="1" frame_rate="10" alpha="1" force_rhr="0">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" value="" name="name"/>
            <Option name="properties"/>
            <Option type="QString" value="collection" name="type"/>
          </Option>
        </data_defined_properties>
        <layer class="SimpleLine" locked="0" enabled="1" pass="0">
          <Option type="Map">
            <Option type="QString" value="0" name="align_dash_pattern"/>
            <Option type="QString" value="square" name="capstyle"/>
            <Option type="QString" value="5;2" name="customdash"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="customdash_map_unit_scale"/>
            <Option type="QString" value="MM" name="customdash_unit"/>
            <Option type="QString" value="0" name="dash_pattern_offset"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="dash_pattern_offset_map_unit_scale"/>
            <Option type="QString" value="MM" name="dash_pattern_offset_unit"/>
            <Option type="QString" value="0" name="draw_inside_polygon"/>
            <Option type="QString" value="bevel" name="joinstyle"/>
            <Option type="QString" value="243,166,178,255" name="line_color"/>
            <Option type="QString" value="solid" name="line_style"/>
            <Option type="QString" value="0.6" name="line_width"/>
            <Option type="QString" value="MM" name="line_width_unit"/>
            <Option type="QString" value="0" name="offset"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
            <Option type="QString" value="MM" name="offset_unit"/>
            <Option type="QString" value="0" name="ring_filter"/>
            <Option type="QString" value="0" name="trim_distance_end"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="trim_distance_end_map_unit_scale"/>
            <Option type="QString" value="MM" name="trim_distance_end_unit"/>
            <Option type="QString" value="0" name="trim_distance_start"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="trim_distance_start_map_unit_scale"/>
            <Option type="QString" value="MM" name="trim_distance_start_unit"/>
            <Option type="QString" value="0" name="tweak_dash_pattern_on_corners"/>
            <Option type="QString" value="0" name="use_custom_dash"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="width_map_unit_scale"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileLineSymbol>
    <profileFillSymbol>
      <symbol type="fill" name="" is_animated="0" clip_to_extent="1" frame_rate="10" alpha="1" force_rhr="0">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" value="" name="name"/>
            <Option name="properties"/>
            <Option type="QString" value="collection" name="type"/>
          </Option>
        </data_defined_properties>
        <layer class="SimpleFill" locked="0" enabled="1" pass="0">
          <Option type="Map">
            <Option type="QString" value="3x:0,0,0,0,0,0" name="border_width_map_unit_scale"/>
            <Option type="QString" value="243,166,178,255" name="color"/>
            <Option type="QString" value="bevel" name="joinstyle"/>
            <Option type="QString" value="0,0" name="offset"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
            <Option type="QString" value="MM" name="offset_unit"/>
            <Option type="QString" value="174,119,127,255" name="outline_color"/>
            <Option type="QString" value="solid" name="outline_style"/>
            <Option type="QString" value="0.2" name="outline_width"/>
            <Option type="QString" value="MM" name="outline_width_unit"/>
            <Option type="QString" value="solid" name="style"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileFillSymbol>
    <profileMarkerSymbol>
      <symbol type="marker" name="" is_animated="0" clip_to_extent="1" frame_rate="10" alpha="1" force_rhr="0">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" value="" name="name"/>
            <Option name="properties"/>
            <Option type="QString" value="collection" name="type"/>
          </Option>
        </data_defined_properties>
        <layer class="SimpleMarker" locked="0" enabled="1" pass="0">
          <Option type="Map">
            <Option type="QString" value="0" name="angle"/>
            <Option type="QString" value="square" name="cap_style"/>
            <Option type="QString" value="243,166,178,255" name="color"/>
            <Option type="QString" value="1" name="horizontal_anchor_point"/>
            <Option type="QString" value="bevel" name="joinstyle"/>
            <Option type="QString" value="diamond" name="name"/>
            <Option type="QString" value="0,0" name="offset"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
            <Option type="QString" value="MM" name="offset_unit"/>
            <Option type="QString" value="174,119,127,255" name="outline_color"/>
            <Option type="QString" value="solid" name="outline_style"/>
            <Option type="QString" value="0.2" name="outline_width"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale"/>
            <Option type="QString" value="MM" name="outline_width_unit"/>
            <Option type="QString" value="diameter" name="scale_method"/>
            <Option type="QString" value="3" name="size"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="size_map_unit_scale"/>
            <Option type="QString" value="MM" name="size_unit"/>
            <Option type="QString" value="1" name="vertical_anchor_point"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileMarkerSymbol>
  </elevation>
  <renderer-v2 enableorderby="0" type="singleSymbol" symbollevels="0" referencescale="-1" forceraster="0">
    <symbols>
      <symbol type="line" name="0" is_animated="0" clip_to_extent="1" frame_rate="10" alpha="1" force_rhr="0">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" value="" name="name"/>
            <Option name="properties"/>
            <Option type="QString" value="collection" name="type"/>
          </Option>
        </data_defined_properties>
        <layer class="MarkerLine" locked="0" enabled="1" pass="0">
          <Option type="Map">
            <Option type="QString" value="4" name="average_angle_length"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="average_angle_map_unit_scale"/>
            <Option type="QString" value="MM" name="average_angle_unit"/>
            <Option type="QString" value="3" name="interval"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="interval_map_unit_scale"/>
            <Option type="QString" value="MM" name="interval_unit"/>
            <Option type="QString" value="0" name="offset"/>
            <Option type="QString" value="0" name="offset_along_line"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_along_line_map_unit_scale"/>
            <Option type="QString" value="MM" name="offset_along_line_unit"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
            <Option type="QString" value="MM" name="offset_unit"/>
            <Option type="bool" value="true" name="place_on_every_part"/>
            <Option type="QString" value="Interval" name="placements"/>
            <Option type="QString" value="0" name="ring_filter"/>
            <Option type="QString" value="1" name="rotate"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
          <symbol type="marker" name="@0@0" is_animated="0" clip_to_extent="1" frame_rate="10" alpha="1" force_rhr="0">
            <data_defined_properties>
              <Option type="Map">
                <Option type="QString" value="" name="name"/>
                <Option name="properties"/>
                <Option type="QString" value="collection" name="type"/>
              </Option>
            </data_defined_properties>
            <layer class="SimpleMarker" locked="0" enabled="1" pass="0">
              <Option type="Map">
                <Option type="QString" value="0" name="angle"/>
                <Option type="QString" value="square" name="cap_style"/>
                <Option type="QString" value="255,0,0,255" name="color"/>
                <Option type="QString" value="1" name="horizontal_anchor_point"/>
                <Option type="QString" value="bevel" name="joinstyle"/>
                <Option type="QString" value="cross2" name="name"/>
                <Option type="QString" value="0,0" name="offset"/>
                <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
                <Option type="QString" value="MM" name="offset_unit"/>
                <Option type="QString" value="35,35,35,255" name="outline_color"/>
                <Option type="QString" value="solid" name="outline_style"/>
                <Option type="QString" value="0" name="outline_width"/>
                <Option type="QString" value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale"/>
                <Option type="QString" value="MM" name="outline_width_unit"/>
                <Option type="QString" value="diameter" name="scale_method"/>
                <Option type="QString" value="0" name="size"/>
                <Option type="QString" value="3x:0,0,0,0,0,0" name="size_map_unit_scale"/>
                <Option type="QString" value="MM" name="size_unit"/>
                <Option type="QString" value="1" name="vertical_anchor_point"/>
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option type="QString" value="" name="name"/>
                  <Option type="Map" name="properties">
                    <Option type="Map" name="outlineColor">
                      <Option type="bool" value="true" name="active"/>
                      <Option type="QString" value="case when &quot;_model&quot;=@current_model then '#FF000000' else '#FFAAAAAA' end" name="expression"/>
                      <Option type="int" value="3" name="type"/>
                    </Option>
                    <Option type="Map" name="size">
                      <Option type="bool" value="true" name="active"/>
                      <Option type="QString" value="case when status_open is false then 2 else 0 end" name="expression"/>
                      <Option type="int" value="3" name="type"/>
                    </Option>
                  </Option>
                  <Option type="QString" value="collection" name="type"/>
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
        <layer class="SimpleLine" locked="0" enabled="1" pass="0">
          <Option type="Map">
            <Option type="QString" value="0" name="align_dash_pattern"/>
            <Option type="QString" value="square" name="capstyle"/>
            <Option type="QString" value="5;2" name="customdash"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="customdash_map_unit_scale"/>
            <Option type="QString" value="MM" name="customdash_unit"/>
            <Option type="QString" value="0" name="dash_pattern_offset"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="dash_pattern_offset_map_unit_scale"/>
            <Option type="QString" value="MM" name="dash_pattern_offset_unit"/>
            <Option type="QString" value="0" name="draw_inside_polygon"/>
            <Option type="QString" value="bevel" name="joinstyle"/>
            <Option type="QString" value="35,35,35,255" name="line_color"/>
            <Option type="QString" value="solid" name="line_style"/>
            <Option type="QString" value="0.26" name="line_width"/>
            <Option type="QString" value="MM" name="line_width_unit"/>
            <Option type="QString" value="0" name="offset"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
            <Option type="QString" value="MM" name="offset_unit"/>
            <Option type="QString" value="0" name="ring_filter"/>
            <Option type="QString" value="0" name="trim_distance_end"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="trim_distance_end_map_unit_scale"/>
            <Option type="QString" value="MM" name="trim_distance_end_unit"/>
            <Option type="QString" value="0" name="trim_distance_start"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="trim_distance_start_map_unit_scale"/>
            <Option type="QString" value="MM" name="trim_distance_start_unit"/>
            <Option type="QString" value="0" name="tweak_dash_pattern_on_corners"/>
            <Option type="QString" value="0" name="use_custom_dash"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="width_map_unit_scale"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option type="Map" name="properties">
                <Option type="Map" name="outlineColor">
                  <Option type="bool" value="true" name="active"/>
                  <Option type="QString" value="case when &quot;_model&quot;=@current_model then '#FF000000' else '#FFAAAAAA' end" name="expression"/>
                  <Option type="int" value="3" name="type"/>
                </Option>
                <Option type="Map" name="outlineWidth">
                  <Option type="bool" value="true" name="active"/>
                  <Option type="QString" value="0.666667*(coalesce(scale_linear(&quot;diameter&quot;, 0.01, 3, 0.1, 4), 0))" name="expression"/>
                  <Option type="int" value="3" name="type"/>
                </Option>
              </Option>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
        <layer class="MarkerLine" locked="0" enabled="1" pass="0">
          <Option type="Map">
            <Option type="QString" value="4" name="average_angle_length"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="average_angle_map_unit_scale"/>
            <Option type="QString" value="MM" name="average_angle_unit"/>
            <Option type="QString" value="3" name="interval"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="interval_map_unit_scale"/>
            <Option type="QString" value="MM" name="interval_unit"/>
            <Option type="QString" value="0" name="offset"/>
            <Option type="QString" value="0" name="offset_along_line"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_along_line_map_unit_scale"/>
            <Option type="QString" value="MM" name="offset_along_line_unit"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
            <Option type="QString" value="MM" name="offset_unit"/>
            <Option type="bool" value="true" name="place_on_every_part"/>
            <Option type="QString" value="CentralPoint" name="placements"/>
            <Option type="QString" value="0" name="ring_filter"/>
            <Option type="QString" value="1" name="rotate"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
          <symbol type="marker" name="@0@2" is_animated="0" clip_to_extent="1" frame_rate="10" alpha="1" force_rhr="0">
            <data_defined_properties>
              <Option type="Map">
                <Option type="QString" value="" name="name"/>
                <Option name="properties"/>
                <Option type="QString" value="collection" name="type"/>
              </Option>
            </data_defined_properties>
            <layer class="SimpleMarker" locked="0" enabled="1" pass="0">
              <Option type="Map">
                <Option type="QString" value="0" name="angle"/>
                <Option type="QString" value="square" name="cap_style"/>
                <Option type="QString" value="255,0,0,255" name="color"/>
                <Option type="QString" value="1" name="horizontal_anchor_point"/>
                <Option type="QString" value="bevel" name="joinstyle"/>
                <Option type="QString" value="arrowhead" name="name"/>
                <Option type="QString" value="0,0" name="offset"/>
                <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
                <Option type="QString" value="MM" name="offset_unit"/>
                <Option type="QString" value="35,35,35,255" name="outline_color"/>
                <Option type="QString" value="solid" name="outline_style"/>
                <Option type="QString" value="0" name="outline_width"/>
                <Option type="QString" value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale"/>
                <Option type="QString" value="MM" name="outline_width_unit"/>
                <Option type="QString" value="diameter" name="scale_method"/>
                <Option type="QString" value="1.2" name="size"/>
                <Option type="QString" value="3x:0,0,0,0,0,0" name="size_map_unit_scale"/>
                <Option type="QString" value="MM" name="size_unit"/>
                <Option type="QString" value="1" name="vertical_anchor_point"/>
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option type="QString" value="" name="name"/>
                  <Option type="Map" name="properties">
                    <Option type="Map" name="outlineColor">
                      <Option type="bool" value="true" name="active"/>
                      <Option type="QString" value="case when &quot;_model&quot;=@current_model then '#FF000000' else '#FFAAAAAA' end" name="expression"/>
                      <Option type="int" value="3" name="type"/>
                    </Option>
                    <Option type="Map" name="size">
                      <Option type="bool" value="true" name="active"/>
                      <Option type="QString" value="case when @map_scale > 10000 then 0 else 1.2 end" name="expression"/>
                      <Option type="int" value="3" name="type"/>
                    </Option>
                  </Option>
                  <Option type="QString" value="collection" name="type"/>
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
        <layer class="MarkerLine" locked="0" enabled="1" pass="0">
          <Option type="Map">
            <Option type="QString" value="4" name="average_angle_length"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="average_angle_map_unit_scale"/>
            <Option type="QString" value="MM" name="average_angle_unit"/>
            <Option type="QString" value="3" name="interval"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="interval_map_unit_scale"/>
            <Option type="QString" value="MM" name="interval_unit"/>
            <Option type="QString" value="0" name="offset"/>
            <Option type="QString" value="0" name="offset_along_line"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_along_line_map_unit_scale"/>
            <Option type="QString" value="MM" name="offset_along_line_unit"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
            <Option type="QString" value="MM" name="offset_unit"/>
            <Option type="bool" value="true" name="place_on_every_part"/>
            <Option type="QString" value="CentralPoint" name="placements"/>
            <Option type="QString" value="0" name="ring_filter"/>
            <Option type="QString" value="1" name="rotate"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
          <symbol type="marker" name="@0@3" is_animated="0" clip_to_extent="1" frame_rate="10" alpha="1" force_rhr="0">
            <data_defined_properties>
              <Option type="Map">
                <Option type="QString" value="" name="name"/>
                <Option name="properties"/>
                <Option type="QString" value="collection" name="type"/>
              </Option>
            </data_defined_properties>
            <layer class="SvgMarker" locked="0" enabled="1" pass="0">
              <Option type="Map">
                <Option type="QString" value="60" name="angle"/>
                <Option type="QString" value="255,255,255,255" name="color"/>
                <Option type="QString" value="0" name="fixedAspectRatio"/>
                <Option type="QString" value="1" name="horizontal_anchor_point"/>
                <Option type="QString" value="AEP_COMPTEUR_DEBIT.svg" name="name"/>
                <Option type="QString" value="0,0" name="offset"/>
                <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
                <Option type="QString" value="MM" name="offset_unit"/>
                <Option type="QString" value="0,0,0,255" name="outline_color"/>
                <Option type="QString" value="0.2" name="outline_width"/>
                <Option type="QString" value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale"/>
                <Option type="QString" value="MM" name="outline_width_unit"/>
                <Option name="parameters"/>
                <Option type="QString" value="diameter" name="scale_method"/>
                <Option type="QString" value="0" name="size"/>
                <Option type="QString" value="3x:0,0,0,0,0,0" name="size_map_unit_scale"/>
                <Option type="QString" value="MM" name="size_unit"/>
                <Option type="QString" value="1" name="vertical_anchor_point"/>
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option type="QString" value="" name="name"/>
                  <Option type="Map" name="properties">
                    <Option type="Map" name="height">
                      <Option type="bool" value="true" name="active"/>
                      <Option type="QString" value="case when @map_scale > 10000 then&#xd;&#xa;&#x9;0 &#xd;&#xa;else &#xd;&#xa;&#x9;case when meter then &#xd;&#xa;&#x9;&#x9;4&#xd;&#xa;&#x9;else &#xd;&#xa;&#x9;&#x9;0 &#xd;&#xa;&#x9;&#x9;end&#xd;&#xa;end" name="expression"/>
                      <Option type="int" value="3" name="type"/>
                    </Option>
                    <Option type="Map" name="outlineColor">
                      <Option type="bool" value="true" name="active"/>
                      <Option type="QString" value="case when &quot;_model&quot;=@current_model then '#FF000000' else '#FFAAAAAA' end" name="expression"/>
                      <Option type="int" value="3" name="type"/>
                    </Option>
                    <Option type="Map" name="width">
                      <Option type="bool" value="true" name="active"/>
                      <Option type="QString" value="case when @map_scale > 10000 then&#xd;&#xa;&#x9;0 &#xd;&#xa;else &#xd;&#xa;&#x9;case when meter then &#xd;&#xa;&#x9;&#x9;4&#xd;&#xa;&#x9;else &#xd;&#xa;&#x9;&#x9;0 &#xd;&#xa;&#x9;&#x9;end&#xd;&#xa;end" name="expression"/>
                      <Option type="int" value="3" name="type"/>
                    </Option>
                  </Option>
                  <Option type="QString" value="collection" name="type"/>
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
      </symbol>
    </symbols>
    <rotation/>
    <sizescale/>
  </renderer-v2>
  <customproperties>
    <Option type="Map">
      <Option type="List" name="dualview/previewExpressions">
        <Option type="QString" value="&quot;name&quot;"/>
      </Option>
      <Option type="QString" value="0" name="embeddedWidgets/count"/>
      <Option name="variableNames"/>
      <Option name="variableValues"/>
    </Option>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer diagramType="Histogram" attributeLegend="1">
    <DiagramCategory penAlpha="255" height="15" backgroundColor="#ffffff" minScaleDenominator="0" labelPlacementMethod="XHeight" diagramOrientation="Up" scaleDependency="Area" lineSizeType="MM" spacing="0" lineSizeScale="3x:0,0,0,0,0,0" scaleBasedVisibility="0" sizeScale="3x:0,0,0,0,0,0" spacingUnitScale="3x:0,0,0,0,0,0" enabled="0" backgroundAlpha="255" maxScaleDenominator="1e+08" barWidth="5" width="15" minimumSize="0" direction="1" showAxis="0" rotationOffset="270" opacity="1" penColor="#000000" spacingUnit="MM" penWidth="0" sizeType="MM">
      <fontProperties bold="0" italic="0" underline="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" strikethrough="0" style=""/>
      <attribute field="" colorOpacity="1" label="" color="#000000"/>
      <axisSymbol>
        <symbol type="line" name="" is_animated="0" clip_to_extent="1" frame_rate="10" alpha="1" force_rhr="0">
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
          <layer class="SimpleLine" locked="0" enabled="1" pass="0">
            <Option type="Map">
              <Option type="QString" value="0" name="align_dash_pattern"/>
              <Option type="QString" value="square" name="capstyle"/>
              <Option type="QString" value="5;2" name="customdash"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="customdash_map_unit_scale"/>
              <Option type="QString" value="MM" name="customdash_unit"/>
              <Option type="QString" value="0" name="dash_pattern_offset"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="dash_pattern_offset_map_unit_scale"/>
              <Option type="QString" value="MM" name="dash_pattern_offset_unit"/>
              <Option type="QString" value="0" name="draw_inside_polygon"/>
              <Option type="QString" value="bevel" name="joinstyle"/>
              <Option type="QString" value="35,35,35,255" name="line_color"/>
              <Option type="QString" value="solid" name="line_style"/>
              <Option type="QString" value="0.26" name="line_width"/>
              <Option type="QString" value="MM" name="line_width_unit"/>
              <Option type="QString" value="0" name="offset"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
              <Option type="QString" value="MM" name="offset_unit"/>
              <Option type="QString" value="0" name="ring_filter"/>
              <Option type="QString" value="0" name="trim_distance_end"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="trim_distance_end_map_unit_scale"/>
              <Option type="QString" value="MM" name="trim_distance_end_unit"/>
              <Option type="QString" value="0" name="trim_distance_start"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="trim_distance_start_map_unit_scale"/>
              <Option type="QString" value="MM" name="trim_distance_start_unit"/>
              <Option type="QString" value="0" name="tweak_dash_pattern_on_corners"/>
              <Option type="QString" value="0" name="use_custom_dash"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="width_map_unit_scale"/>
            </Option>
            <data_defined_properties>
              <Option type="Map">
                <Option type="QString" value="" name="name"/>
                <Option name="properties"/>
                <Option type="QString" value="collection" name="type"/>
              </Option>
            </data_defined_properties>
          </layer>
        </symbol>
      </axisSymbol>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings linePlacementFlags="18" priority="0" zIndex="0" placement="2" showAll="1" obstacle="0" dist="0">
    <properties>
      <Option type="Map">
        <Option type="QString" value="" name="name"/>
        <Option name="properties"/>
        <Option type="QString" value="collection" name="type"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions geometryPrecision="0" removeDuplicateNodes="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <legend type="default-vector" showLabelLegend="0"/>
  <referencedLayers>
    <relation layerId="All_nodes_fd7e8c9e_d922_4e3b_9b9d_24fcd27d2734" name="Pipe down" dataSource="dbname='tutoriel_essentiel' service='expresseau' sslmode=disable key='id' checkPrimaryKeyUnicity='0' table=&quot;api&quot;.&quot;node&quot; (geom)" referencingLayer="Pipe_4fe05e39_78f6_4dd4_b59e_466a15fb4935" id="Pipe_4fe05e39_78f6_4dd4_b59e_466a15fb4935_down_All_nodes_fd7e8c9e_d922_4e3b_9b9d_24fcd27d2734_id" layerName="All nodes" strength="Association" providerKey="postgres" referencedLayer="All_nodes_fd7e8c9e_d922_4e3b_9b9d_24fcd27d2734">
      <fieldRef referencingField="down" referencedField="id"/>
    </relation>
    <relation layerId="Material_45aa3841_2ae7_4137_b2e8_5bb50fa6deba" name="Pipe material" dataSource="dbname='tutoriel_essentiel' service='expresseau' sslmode=disable key='name' checkPrimaryKeyUnicity='0' table=&quot;api&quot;.&quot;material&quot;" referencingLayer="Pipe_4fe05e39_78f6_4dd4_b59e_466a15fb4935" id="Pipe_4fe05e39_78f6_4dd4_b59e_466a15fb4935_material_Material_45aa3841_2ae7_4137_b2e8_5bb50fa6deba_name" layerName="Material" strength="Association" providerKey="postgres" referencedLayer="Material_45aa3841_2ae7_4137_b2e8_5bb50fa6deba">
      <fieldRef referencingField="material" referencedField="name"/>
    </relation>
    <relation layerId="All_nodes_fd7e8c9e_d922_4e3b_9b9d_24fcd27d2734" name="Pipe up" dataSource="dbname='tutoriel_essentiel' service='expresseau' sslmode=disable key='id' checkPrimaryKeyUnicity='0' table=&quot;api&quot;.&quot;node&quot; (geom)" referencingLayer="Pipe_4fe05e39_78f6_4dd4_b59e_466a15fb4935" id="Pipe_4fe05e39_78f6_4dd4_b59e_466a15fb4935_up_All_nodes_fd7e8c9e_d922_4e3b_9b9d_24fcd27d2734_id" layerName="All nodes" strength="Association" providerKey="postgres" referencedLayer="All_nodes_fd7e8c9e_d922_4e3b_9b9d_24fcd27d2734">
      <fieldRef referencingField="up" referencedField="id"/>
    </relation>
  </referencedLayers>
  <fieldConfiguration>
    <field name="id" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="name" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="diameter" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="thickness_mm" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="material" configurationFlags="None">
      <editWidget type="RelationReference">
        <config>
          <Option type="Map">
            <Option type="bool" value="true" name="AllowAddFeatures"/>
            <Option type="bool" value="false" name="AllowNULL"/>
            <Option type="bool" value="false" name="MapIdentification"/>
            <Option type="bool" value="true" name="OrderByValue"/>
            <Option type="bool" value="false" name="ReadOnly"/>
            <Option type="QString" value="dbname='tutoriel_essentiel' service='expresseau' sslmode=disable key='name' checkPrimaryKeyUnicity='0' table=&quot;api&quot;.&quot;material&quot;" name="ReferencedLayerDataSource"/>
            <Option type="QString" value="Material_45aa3841_2ae7_4137_b2e8_5bb50fa6deba" name="ReferencedLayerId"/>
            <Option type="QString" value="Material" name="ReferencedLayerName"/>
            <Option type="QString" value="postgres" name="ReferencedLayerProviderKey"/>
            <Option type="QString" value="Pipe_4fe05e39_78f6_4dd4_b59e_466a15fb4935_material_Material_45aa3841_2ae7_4137_b2e8_5bb50fa6deba_name" name="Relation"/>
            <Option type="bool" value="false" name="ShowForm"/>
            <Option type="bool" value="true" name="ShowOpenFormButton"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="status_open" configurationFlags="None">
      <editWidget type="CheckBox">
        <config>
          <Option type="Map">
            <Option type="QString" value="" name="CheckedState"/>
            <Option type="int" value="0" name="TextDisplayMethod"/>
            <Option type="QString" value="" name="UncheckedState"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="meter" configurationFlags="None">
      <editWidget type="CheckBox">
        <config>
          <Option type="Map">
            <Option type="QString" value="" name="CheckedState"/>
            <Option type="int" value="0" name="TextDisplayMethod"/>
            <Option type="QString" value="" name="UncheckedState"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="overload_length" configurationFlags="None">
      <editWidget type="CheckBox">
        <config>
          <Option type="Map">
            <Option type="QString" value="" name="CheckedState"/>
            <Option type="int" value="0" name="TextDisplayMethod"/>
            <Option type="QString" value="" name="UncheckedState"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="overloaded_length" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="overload_celerity" configurationFlags="None">
      <editWidget type="CheckBox">
        <config>
          <Option type="Map">
            <Option type="QString" value="" name="CheckedState"/>
            <Option type="int" value="0" name="TextDisplayMethod"/>
            <Option type="QString" value="" name="UncheckedState"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="overloaded_celerity" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="overload_roughness_mm" configurationFlags="None">
      <editWidget type="CheckBox">
        <config>
          <Option type="Map">
            <Option type="QString" value="" name="CheckedState"/>
            <Option type="int" value="0" name="TextDisplayMethod"/>
            <Option type="QString" value="" name="UncheckedState"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="overloaded_roughness_mm" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="is_feeder" configurationFlags="None">
      <editWidget type="CheckBox">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="installation_date" configurationFlags="None">
      <editWidget type="DateTime">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="compute_contact_time" configurationFlags="None">
      <editWidget type="CheckBox">
        <config>
          <Option type="Map">
            <Option type="QString" value="" name="CheckedState"/>
            <Option type="int" value="0" name="TextDisplayMethod"/>
            <Option type="QString" value="" name="UncheckedState"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="comment" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="true" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="down" configurationFlags="None">
      <editWidget type="RelationReference">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="AllowAddFeatures"/>
            <Option type="bool" value="false" name="AllowNULL"/>
            <Option type="bool" value="true" name="MapIdentification"/>
            <Option type="bool" value="false" name="OrderByValue"/>
            <Option type="bool" value="true" name="ReadOnly"/>
            <Option type="QString" value="dbname='tutoriel_essentiel' service='expresseau' sslmode=disable key='id' checkPrimaryKeyUnicity='0' table=&quot;api&quot;.&quot;node&quot; (geom)" name="ReferencedLayerDataSource"/>
            <Option type="QString" value="All_nodes_fd7e8c9e_d922_4e3b_9b9d_24fcd27d2734" name="ReferencedLayerId"/>
            <Option type="QString" value="All nodes" name="ReferencedLayerName"/>
            <Option type="QString" value="postgres" name="ReferencedLayerProviderKey"/>
            <Option type="QString" value="Pipe_4fe05e39_78f6_4dd4_b59e_466a15fb4935_down_All_nodes_fd7e8c9e_d922_4e3b_9b9d_24fcd27d2734_id" name="Relation"/>
            <Option type="bool" value="false" name="ShowForm"/>
            <Option type="bool" value="false" name="ShowOpenFormButton"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="up" configurationFlags="None">
      <editWidget type="RelationReference">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="AllowAddFeatures"/>
            <Option type="bool" value="false" name="AllowNULL"/>
            <Option type="bool" value="true" name="MapIdentification"/>
            <Option type="bool" value="false" name="OrderByValue"/>
            <Option type="bool" value="true" name="ReadOnly"/>
            <Option type="QString" value="dbname='tutoriel_essentiel' service='expresseau' sslmode=disable key='id' checkPrimaryKeyUnicity='0' table=&quot;api&quot;.&quot;node&quot; (geom)" name="ReferencedLayerDataSource"/>
            <Option type="QString" value="All_nodes_fd7e8c9e_d922_4e3b_9b9d_24fcd27d2734" name="ReferencedLayerId"/>
            <Option type="QString" value="All nodes" name="ReferencedLayerName"/>
            <Option type="QString" value="postgres" name="ReferencedLayerProviderKey"/>
            <Option type="QString" value="Pipe_4fe05e39_78f6_4dd4_b59e_466a15fb4935_up_All_nodes_fd7e8c9e_d922_4e3b_9b9d_24fcd27d2734_id" name="Relation"/>
            <Option type="bool" value="false" name="ShowForm"/>
            <Option type="bool" value="false" name="ShowOpenFormButton"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="_model" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="_roughness_mm" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="_elasticity_n_m2" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="_celerity" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias field="id" name="" index="0"/>
    <alias field="name" name="Name" index="1"/>
    <alias field="diameter" name="Pipe diameter (m)" index="2"/>
    <alias field="thickness_mm" name="Pipe thickness (mm)" index="3"/>
    <alias field="material" name="Material" index="4"/>
    <alias field="status_open" name="Open ?" index="5"/>
    <alias field="meter" name="Meter on pipe ?" index="6"/>
    <alias field="overload_length" name="'Force custom length ? (Geometric length is '||round($length, 2)||'m)'" index="7"/>
    <alias field="overloaded_length" name="Custom length (m)" index="8"/>
    <alias field="overload_celerity" name="Force custom celerity ?" index="9"/>
    <alias field="overloaded_celerity" name="Custom Celerity (m/s)" index="10"/>
    <alias field="overload_roughness_mm" name="Force custom roughness ?" index="11"/>
    <alias field="overloaded_roughness_mm" name="Custom roughness (mm)" index="12"/>
    <alias field="is_feeder" name="" index="13"/>
    <alias field="installation_date" name="" index="14"/>
    <alias field="compute_contact_time" name="Compute contact time ?" index="15"/>
    <alias field="comment" name="Comment" index="16"/>
    <alias field="down" name="Node down" index="17"/>
    <alias field="up" name="Node up" index="18"/>
    <alias field="_model" name="Model" index="19"/>
    <alias field="_roughness_mm" name="Material roughness (mm)" index="20"/>
    <alias field="_elasticity_n_m2" name="Elasticity (N/m2)" index="21"/>
    <alias field="_celerity" name="Calculated Celerity (m/s)" index="22"/>
  </aliases>
  <defaults>
    <default field="id" applyOnUpdate="0" expression=""/>
    <default field="name" applyOnUpdate="0" expression=""/>
    <default field="diameter" applyOnUpdate="0" expression=""/>
    <default field="thickness_mm" applyOnUpdate="0" expression=""/>
    <default field="material" applyOnUpdate="0" expression=""/>
    <default field="status_open" applyOnUpdate="0" expression=""/>
    <default field="meter" applyOnUpdate="0" expression=""/>
    <default field="overload_length" applyOnUpdate="0" expression=""/>
    <default field="overloaded_length" applyOnUpdate="0" expression=""/>
    <default field="overload_celerity" applyOnUpdate="0" expression=""/>
    <default field="overloaded_celerity" applyOnUpdate="0" expression=""/>
    <default field="overload_roughness_mm" applyOnUpdate="0" expression=""/>
    <default field="overloaded_roughness_mm" applyOnUpdate="0" expression=""/>
    <default field="is_feeder" applyOnUpdate="0" expression=""/>
    <default field="installation_date" applyOnUpdate="0" expression=""/>
    <default field="compute_contact_time" applyOnUpdate="0" expression=""/>
    <default field="comment" applyOnUpdate="0" expression=""/>
    <default field="down" applyOnUpdate="0" expression="attribute(&#xd;&#xa;&#x9;array_first(&#xd;&#xa;&#x9;&#x9;array_filter(&#xd;&#xa;&#x9;&#x9;&#x9;overlay_intersects(&#xd;&#xa;&#x9;&#x9;&#x9;&#x9;&#x9;layer:='All_nodes_fd7e8c9e_d922_4e3b_9b9d_24fcd27d2734',&#xd;&#xa;&#x9;&#x9;&#x9;&#x9;&#x9;expression:=$currentfeature&#xd;&#xa;&#x9;&#x9;&#x9;),&#xd;&#xa;&#x9;&#x9;&#x9;intersects(end_point($geometry), geometry(@element))&#xd;&#xa;&#x9;&#x9;)&#xd;&#xa;&#x9;),&#xd;&#xa;&#x9;'id'&#xd;&#xa;)"/>
    <default field="up" applyOnUpdate="0" expression="attribute(&#xd;&#xa;&#x9;array_first(&#xd;&#xa;&#x9;&#x9;array_filter(&#xd;&#xa;&#x9;&#x9;&#x9;overlay_intersects(&#xd;&#xa;&#x9;&#x9;&#x9;&#x9;&#x9;layer:='All_nodes_fd7e8c9e_d922_4e3b_9b9d_24fcd27d2734',&#xd;&#xa;&#x9;&#x9;&#x9;&#x9;&#x9;expression:=$currentfeature&#xd;&#xa;&#x9;&#x9;&#x9;),&#xd;&#xa;&#x9;&#x9;&#x9;intersects(start_point($geometry), geometry(@element))&#xd;&#xa;&#x9;&#x9;)&#xd;&#xa;&#x9;),&#xd;&#xa;&#x9;'id'&#xd;&#xa;)"/>
    <default field="_model" applyOnUpdate="0" expression="attribute(&#xd;&#xa;&#x9;array_first(&#xd;&#xa;&#x9;&#x9;array_filter(&#xd;&#xa;&#x9;&#x9;&#x9;overlay_intersects(&#xd;&#xa;&#x9;&#x9;&#x9;&#x9;&#x9;layer:='All_nodes_fd7e8c9e_d922_4e3b_9b9d_24fcd27d2734',&#xd;&#xa;&#x9;&#x9;&#x9;&#x9;&#x9;expression:=$currentfeature&#xd;&#xa;&#x9;&#x9;&#x9;),&#xd;&#xa;&#x9;&#x9;&#x9;intersects(start_point($geometry), geometry(@element))&#xd;&#xa;&#x9;&#x9;)&#xd;&#xa;&#x9;),&#xd;&#xa;&#x9;'model'&#xd;&#xa;)"/>
    <default field="_roughness_mm" applyOnUpdate="1" expression="attribute(&#xd;&#xa;&#x9;get_feature(&#xd;&#xa;&#x9;&#x9;layer:='Material_45aa3841_2ae7_4137_b2e8_5bb50fa6deba',&#xd;&#xa;&#x9;&#x9;attribute:='name',&#xd;&#xa;&#x9;&#x9;value:=coalesce( &quot;material&quot;, 'inconnu')&#xd;&#xa;&#x9;),&#xd;&#xa;&#x9;'roughness_mm'&#xd;&#xa;)"/>
    <default field="_elasticity_n_m2" applyOnUpdate="1" expression="attribute(&#xd;&#xa;&#x9;get_feature(&#xd;&#xa;&#x9;&#x9;layer:='Material_45aa3841_2ae7_4137_b2e8_5bb50fa6deba',&#xd;&#xa;&#x9;&#x9;attribute:='name',&#xd;&#xa;&#x9;&#x9;value:=coalesce( &quot;material&quot;, 'inconnu')&#xd;&#xa;&#x9;),&#xd;&#xa;&#x9;'elasticity_n_m2'&#xd;&#xa;)"/>
    <default field="_celerity" applyOnUpdate="1" expression="with_variable(&#xd;&#xa;&#x9;name:='fluid_properties',&#xd;&#xa;&#x9;value:=get_feature(&#xd;&#xa;&#x9;&#x9;layer:='Fluid_properties_f1201a7b_0454_413d_a129_454a26d2f720',&#xd;&#xa;&#x9;&#x9;attribute:='id',&#xd;&#xa;&#x9;&#x9;value:='Fluid properties'&#xd;&#xa;&#x9;&#x9;),&#xd;&#xa;&#x9;expression:= 1 / (&#xd;&#xa;&#x9;&#x9;sqrt(&#xd;&#xa;&#x9;&#x9;&#x9;attribute(@fluid_properties, 'volumic_mass_kg_m3') /&#xd;&#xa;&#x9;&#x9;&#x9;attribute(@fluid_properties, 'volumic_elasticity_module_N_m2')&#xd;&#xa;&#x9;&#x9;&#x9;+&#xd;&#xa;&#x9;&#x9;&#x9;attribute(@fluid_properties, 'volumic_mass_kg_m3') *  &quot;diameter&quot; /&#xd;&#xa;&#x9;&#x9;&#x9;(&quot;_elasticity_n_m2&quot; * &quot;thickness_mm&quot; / 1000)&#xd;&#xa;&#x9;&#x9;)&#xd;&#xa;&#x9;)&#x9;&#xd;&#xa;)"/>
  </defaults>
  <constraints>
    <constraint field="id" constraints="3" unique_strength="1" exp_strength="0" notnull_strength="1"/>
    <constraint field="name" constraints="0" unique_strength="0" exp_strength="0" notnull_strength="0"/>
    <constraint field="diameter" constraints="1" unique_strength="0" exp_strength="0" notnull_strength="1"/>
    <constraint field="thickness_mm" constraints="0" unique_strength="0" exp_strength="0" notnull_strength="0"/>
    <constraint field="material" constraints="0" unique_strength="0" exp_strength="0" notnull_strength="0"/>
    <constraint field="status_open" constraints="0" unique_strength="0" exp_strength="0" notnull_strength="0"/>
    <constraint field="meter" constraints="0" unique_strength="0" exp_strength="0" notnull_strength="0"/>
    <constraint field="overload_length" constraints="0" unique_strength="0" exp_strength="0" notnull_strength="0"/>
    <constraint field="overloaded_length" constraints="0" unique_strength="0" exp_strength="0" notnull_strength="0"/>
    <constraint field="overload_celerity" constraints="0" unique_strength="0" exp_strength="0" notnull_strength="0"/>
    <constraint field="overloaded_celerity" constraints="0" unique_strength="0" exp_strength="0" notnull_strength="0"/>
    <constraint field="overload_roughness_mm" constraints="0" unique_strength="0" exp_strength="0" notnull_strength="0"/>
    <constraint field="overloaded_roughness_mm" constraints="0" unique_strength="0" exp_strength="0" notnull_strength="0"/>
    <constraint field="is_feeder" constraints="0" unique_strength="0" exp_strength="0" notnull_strength="0"/>
    <constraint field="installation_date" constraints="0" unique_strength="0" exp_strength="0" notnull_strength="0"/>
    <constraint field="compute_contact_time" constraints="0" unique_strength="0" exp_strength="0" notnull_strength="0"/>
    <constraint field="comment" constraints="0" unique_strength="0" exp_strength="0" notnull_strength="0"/>
    <constraint field="down" constraints="1" unique_strength="0" exp_strength="0" notnull_strength="1"/>
    <constraint field="up" constraints="1" unique_strength="0" exp_strength="0" notnull_strength="1"/>
    <constraint field="_model" constraints="0" unique_strength="0" exp_strength="0" notnull_strength="0"/>
    <constraint field="_roughness_mm" constraints="0" unique_strength="0" exp_strength="0" notnull_strength="0"/>
    <constraint field="_elasticity_n_m2" constraints="0" unique_strength="0" exp_strength="0" notnull_strength="0"/>
    <constraint field="_celerity" constraints="0" unique_strength="0" exp_strength="0" notnull_strength="0"/>
  </constraints>
  <constraintExpressions>
    <constraint field="id" exp="" desc=""/>
    <constraint field="name" exp="" desc=""/>
    <constraint field="diameter" exp="" desc=""/>
    <constraint field="thickness_mm" exp="" desc=""/>
    <constraint field="material" exp="" desc=""/>
    <constraint field="status_open" exp="" desc=""/>
    <constraint field="meter" exp="" desc=""/>
    <constraint field="overload_length" exp="" desc=""/>
    <constraint field="overloaded_length" exp="" desc=""/>
    <constraint field="overload_celerity" exp="" desc=""/>
    <constraint field="overloaded_celerity" exp="" desc=""/>
    <constraint field="overload_roughness_mm" exp="" desc=""/>
    <constraint field="overloaded_roughness_mm" exp="" desc=""/>
    <constraint field="is_feeder" exp="" desc=""/>
    <constraint field="installation_date" exp="" desc=""/>
    <constraint field="compute_contact_time" exp="" desc=""/>
    <constraint field="comment" exp="" desc=""/>
    <constraint field="down" exp="" desc=""/>
    <constraint field="up" exp="" desc=""/>
    <constraint field="_model" exp="" desc=""/>
    <constraint field="_roughness_mm" exp="" desc=""/>
    <constraint field="_elasticity_n_m2" exp="" desc=""/>
    <constraint field="_celerity" exp="" desc=""/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction value="{00000000-0000-0000-0000-000000000000}" key="Canvas"/>
    <actionsetting type="1" name="Open results" isEnabledOnlyWhenEditable="0" action="from expresseau.gui.result_dialog import ResultWindow&#xd;&#xa;&#xd;&#xa;if 'result_window' in locals() and result_window.isVisible():&#xd;&#xa;    result_window.set_result([%id%],&#xd;&#xa;                    '[% decode_uri(@layer, 'table')%]',&#xd;&#xa;                    '[% @current_scenario%]')&#xd;&#xa;    result_window.hide()&#xd;&#xa;    result_window.show()&#xd;&#xa;else:&#xd;&#xa;    result_window = ResultWindow([%id%],&#xd;&#xa;                    '[% decode_uri(@layer, 'table')%]',&#xd;&#xa;                    '[% @current_scenario%]',&#xd;&#xa;                    '[% @project_basename%]')&#xd;&#xa;    result_window.open()" id="{2695e6d7-8d6e-4c65-870b-1adb7c7a6f7b}" icon="" shortTitle="" capture="0" notificationMessage="">
      <actionScope id="Layer"/>
      <actionScope id="Feature"/>
    </actionsetting>
  </attributeactions>
  <attributetableconfig sortOrder="0" actionWidgetStyle="dropDown" sortExpression="&quot;name&quot;">
    <columns>
      <column type="field" hidden="0" name="name" width="203"/>
      <column type="field" hidden="0" name="up" width="248"/>
      <column type="field" hidden="0" name="down" width="183"/>
      <column type="actions" hidden="1" width="-1"/>
      <column type="field" hidden="0" name="diameter" width="-1"/>
      <column type="field" hidden="0" name="material" width="-1"/>
      <column type="field" hidden="0" name="_roughness_mm" width="-1"/>
      <column type="field" hidden="0" name="_elasticity_n_m2" width="-1"/>
      <column type="field" hidden="0" name="meter" width="-1"/>
      <column type="field" hidden="0" name="comment" width="-1"/>
      <column type="field" hidden="0" name="thickness_mm" width="-1"/>
      <column type="field" hidden="0" name="_celerity" width="-1"/>
      <column type="field" hidden="0" name="status_open" width="-1"/>
      <column type="field" hidden="0" name="overloaded_length" width="-1"/>
      <column type="field" hidden="0" name="overload_length" width="-1"/>
      <column type="field" hidden="0" name="overload_celerity" width="-1"/>
      <column type="field" hidden="0" name="overloaded_celerity" width="-1"/>
      <column type="field" hidden="0" name="id" width="-1"/>
      <column type="field" hidden="0" name="overload_roughness_mm" width="-1"/>
      <column type="field" hidden="0" name="overloaded_roughness_mm" width="-1"/>
      <column type="field" hidden="0" name="is_feeder" width="-1"/>
      <column type="field" hidden="0" name="installation_date" width="-1"/>
      <column type="field" hidden="0" name="compute_contact_time" width="215"/>
      <column type="field" hidden="0" name="_model" width="-1"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1">WINDOWS/System32</editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath>WINDOWS/System32</editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appelée à l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalités à vos formulaires.

Entrez le nom de la fonction dans le champ
"Fonction d'initialisation Python"
Voici un exemple à suivre:
"""
from PyQt4.QtGui import QWidget

def my_form_open(dialog, layer, feature):
⇥geom = feature.geometry()
⇥control = dialog.findChild(QWidget, "MyLineEdit")
]]></editforminitcode>
  <featformsuppress>2</featformsuppress>
  <editorlayout>tablayout</editorlayout>
  <attributeEditorForm>
    <labelStyle overrideLabelFont="0" overrideLabelColor="0" labelColor="0,0,0,255">
      <labelFont bold="0" italic="0" underline="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" strikethrough="0" style=""/>
    </labelStyle>
    <attributeEditorField name="name" showLabel="1" index="1">
      <labelStyle overrideLabelFont="0" overrideLabelColor="0" labelColor="0,0,0,255">
        <labelFont bold="0" italic="0" underline="0" description="MS Shell Dlg 2,12,-1,5,50,0,0,0,0,0" strikethrough="0" style=""/>
      </labelStyle>
    </attributeEditorField>
    <attributeEditorField name="_model" showLabel="1" index="19">
      <labelStyle overrideLabelFont="0" overrideLabelColor="0" labelColor="0,0,0,255">
        <labelFont bold="0" italic="0" underline="0" description="MS Shell Dlg 2,12,-1,5,50,0,0,0,0,0" strikethrough="0" style=""/>
      </labelStyle>
    </attributeEditorField>
    <attributeEditorField name="comment" showLabel="1" index="16">
      <labelStyle overrideLabelFont="0" overrideLabelColor="0" labelColor="0,0,0,255">
        <labelFont bold="0" italic="0" underline="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" strikethrough="0" style=""/>
      </labelStyle>
    </attributeEditorField>
    <attributeEditorContainer collapsedExpressionEnabled="0" name="Parameters" visibilityExpressionEnabled="0" showLabel="1" columnCount="4" groupBox="1" collapsedExpression="" collapsed="0" visibilityExpression="">
      <labelStyle overrideLabelFont="0" overrideLabelColor="0" labelColor="0,0,0,255">
        <labelFont bold="0" italic="0" underline="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" strikethrough="0" style=""/>
      </labelStyle>
      <attributeEditorField name="up" showLabel="1" index="18">
        <labelStyle overrideLabelFont="0" overrideLabelColor="0" labelColor="0,0,0,255">
          <labelFont bold="0" italic="0" underline="0" description="MS Shell Dlg 2,12,-1,5,50,0,0,0,0,0" strikethrough="0" style=""/>
        </labelStyle>
      </attributeEditorField>
      <attributeEditorField name="down" showLabel="1" index="17">
        <labelStyle overrideLabelFont="0" overrideLabelColor="0" labelColor="0,0,0,255">
          <labelFont bold="0" italic="0" underline="0" description="MS Shell Dlg 2,12,-1,5,50,0,0,0,0,0" strikethrough="0" style=""/>
        </labelStyle>
      </attributeEditorField>
      <attributeEditorField name="diameter" showLabel="1" index="2">
        <labelStyle overrideLabelFont="0" overrideLabelColor="0" labelColor="0,0,0,255">
          <labelFont bold="0" italic="0" underline="0" description="MS Shell Dlg 2,12,-1,5,50,0,0,0,0,0" strikethrough="0" style=""/>
        </labelStyle>
      </attributeEditorField>
      <attributeEditorField name="thickness_mm" showLabel="1" index="3">
        <labelStyle overrideLabelFont="0" overrideLabelColor="0" labelColor="0,0,0,255">
          <labelFont bold="0" italic="0" underline="0" description="MS Shell Dlg 2,12,-1,5,50,0,0,0,0,0" strikethrough="0" style=""/>
        </labelStyle>
      </attributeEditorField>
    </attributeEditorContainer>
    <attributeEditorContainer collapsedExpressionEnabled="0" name="Length" visibilityExpressionEnabled="0" showLabel="1" columnCount="3" groupBox="1" collapsedExpression="" collapsed="0" visibilityExpression="">
      <labelStyle overrideLabelFont="0" overrideLabelColor="0" labelColor="0,0,0,255">
        <labelFont bold="0" italic="0" underline="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" strikethrough="0" style=""/>
      </labelStyle>
      <attributeEditorField name="overload_length" showLabel="1" index="7">
        <labelStyle overrideLabelFont="0" overrideLabelColor="0" labelColor="0,0,0,255">
          <labelFont bold="0" italic="0" underline="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" strikethrough="0" style=""/>
        </labelStyle>
      </attributeEditorField>
      <attributeEditorContainer collapsedExpressionEnabled="0" name="" visibilityExpressionEnabled="1" showLabel="1" columnCount="1" groupBox="1" collapsedExpression="" collapsed="0" visibilityExpression="&quot;overload_length&quot;">
        <labelStyle overrideLabelFont="0" overrideLabelColor="0" labelColor="0,0,0,255">
          <labelFont bold="0" italic="0" underline="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" strikethrough="0" style=""/>
        </labelStyle>
        <attributeEditorField name="overloaded_length" showLabel="1" index="8">
          <labelStyle overrideLabelFont="0" overrideLabelColor="1" labelColor="0,0,252,255">
            <labelFont bold="0" italic="0" underline="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" strikethrough="0" style=""/>
          </labelStyle>
        </attributeEditorField>
      </attributeEditorContainer>
    </attributeEditorContainer>
    <attributeEditorContainer collapsedExpressionEnabled="0" name="Other parameters" visibilityExpressionEnabled="0" showLabel="1" columnCount="4" groupBox="1" collapsedExpression="" collapsed="0" visibilityExpression="">
      <labelStyle overrideLabelFont="0" overrideLabelColor="0" labelColor="0,0,0,255">
        <labelFont bold="0" italic="0" underline="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" strikethrough="0" style=""/>
      </labelStyle>
      <attributeEditorField name="status_open" showLabel="1" index="5">
        <labelStyle overrideLabelFont="0" overrideLabelColor="0" labelColor="0,0,0,255">
          <labelFont bold="0" italic="0" underline="0" description="MS Shell Dlg 2,12,-1,5,50,0,0,0,0,0" strikethrough="0" style=""/>
        </labelStyle>
      </attributeEditorField>
      <attributeEditorField name="meter" showLabel="1" index="6">
        <labelStyle overrideLabelFont="0" overrideLabelColor="0" labelColor="0,0,0,255">
          <labelFont bold="0" italic="0" underline="0" description="MS Shell Dlg 2,12,-1,5,50,0,0,0,0,0" strikethrough="0" style=""/>
        </labelStyle>
      </attributeEditorField>
      <attributeEditorField name="compute_contact_time" showLabel="1" index="15">
        <labelStyle overrideLabelFont="0" overrideLabelColor="0" labelColor="0,0,0,255">
          <labelFont bold="0" italic="0" underline="0" description="MS Shell Dlg 2,12,-1,5,50,0,0,0,0,0" strikethrough="0" style=""/>
        </labelStyle>
      </attributeEditorField>
    </attributeEditorContainer>
    <attributeEditorContainer collapsedExpressionEnabled="0" name="Material settings" visibilityExpressionEnabled="0" showLabel="1" columnCount="2" groupBox="1" collapsedExpression="" collapsed="0" visibilityExpression="">
      <labelStyle overrideLabelFont="0" overrideLabelColor="0" labelColor="0,0,0,255">
        <labelFont bold="0" italic="0" underline="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" strikethrough="0" style=""/>
      </labelStyle>
      <attributeEditorField name="material" showLabel="1" index="4">
        <labelStyle overrideLabelFont="0" overrideLabelColor="0" labelColor="0,0,0,255">
          <labelFont bold="0" italic="0" underline="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" strikethrough="0" style=""/>
        </labelStyle>
      </attributeEditorField>
      <attributeEditorField name="_elasticity_n_m2" showLabel="1" index="21">
        <labelStyle overrideLabelFont="0" overrideLabelColor="0" labelColor="0,0,0,255">
          <labelFont bold="0" italic="0" underline="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" strikethrough="0" style=""/>
        </labelStyle>
      </attributeEditorField>
    </attributeEditorContainer>
    <attributeEditorContainer collapsedExpressionEnabled="0" name="Roughness" visibilityExpressionEnabled="0" showLabel="1" columnCount="3" groupBox="1" collapsedExpression="" collapsed="0" visibilityExpression="">
      <labelStyle overrideLabelFont="0" overrideLabelColor="0" labelColor="0,0,0,255">
        <labelFont bold="0" italic="0" underline="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" strikethrough="0" style=""/>
      </labelStyle>
      <attributeEditorField name="overload_roughness_mm" showLabel="1" index="11">
        <labelStyle overrideLabelFont="0" overrideLabelColor="0" labelColor="0,0,0,255">
          <labelFont bold="0" italic="0" underline="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" strikethrough="0" style=""/>
        </labelStyle>
      </attributeEditorField>
      <attributeEditorContainer collapsedExpressionEnabled="0" name="" visibilityExpressionEnabled="1" showLabel="1" columnCount="1" groupBox="1" collapsedExpression="" collapsed="0" visibilityExpression="not overload_roughness_mm">
        <labelStyle overrideLabelFont="0" overrideLabelColor="0" labelColor="0,0,0,255">
          <labelFont bold="0" italic="0" underline="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" strikethrough="0" style=""/>
        </labelStyle>
        <attributeEditorField name="_roughness_mm" showLabel="1" index="20">
          <labelStyle overrideLabelFont="0" overrideLabelColor="0" labelColor="248,0,0,255">
            <labelFont bold="0" italic="0" underline="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" strikethrough="0" style=""/>
          </labelStyle>
        </attributeEditorField>
      </attributeEditorContainer>
      <attributeEditorContainer collapsedExpressionEnabled="0" name="" visibilityExpressionEnabled="1" showLabel="1" columnCount="1" groupBox="1" collapsedExpression="" collapsed="0" visibilityExpression="&quot;overload_roughness_mm&quot;">
        <labelStyle overrideLabelFont="0" overrideLabelColor="0" labelColor="0,0,0,255">
          <labelFont bold="0" italic="0" underline="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" strikethrough="0" style=""/>
        </labelStyle>
        <attributeEditorField name="overloaded_roughness_mm" showLabel="1" index="12">
          <labelStyle overrideLabelFont="0" overrideLabelColor="1" labelColor="15,0,228,255">
            <labelFont bold="0" italic="0" underline="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" strikethrough="0" style=""/>
          </labelStyle>
        </attributeEditorField>
      </attributeEditorContainer>
    </attributeEditorContainer>
    <attributeEditorContainer collapsedExpressionEnabled="0" name="Celerity" visibilityExpressionEnabled="0" showLabel="1" columnCount="3" groupBox="1" collapsedExpression="" collapsed="0" visibilityExpression="">
      <labelStyle overrideLabelFont="0" overrideLabelColor="0" labelColor="0,0,0,255">
        <labelFont bold="0" italic="0" underline="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" strikethrough="0" style=""/>
      </labelStyle>
      <attributeEditorField name="overload_celerity" showLabel="1" index="9">
        <labelStyle overrideLabelFont="0" overrideLabelColor="0" labelColor="0,0,0,255">
          <labelFont bold="0" italic="0" underline="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" strikethrough="0" style=""/>
        </labelStyle>
      </attributeEditorField>
      <attributeEditorContainer collapsedExpressionEnabled="0" name="" visibilityExpressionEnabled="1" showLabel="1" columnCount="1" groupBox="1" collapsedExpression="" collapsed="0" visibilityExpression="not overload_celerity">
        <labelStyle overrideLabelFont="0" overrideLabelColor="0" labelColor="0,0,0,255">
          <labelFont bold="0" italic="0" underline="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" strikethrough="0" style=""/>
        </labelStyle>
        <attributeEditorField name="_celerity" showLabel="1" index="22">
          <labelStyle overrideLabelFont="0" overrideLabelColor="0" labelColor="0,0,0,255">
            <labelFont bold="0" italic="0" underline="0" description="MS Shell Dlg 2,12,-1,5,50,0,0,0,0,0" strikethrough="0" style=""/>
          </labelStyle>
        </attributeEditorField>
      </attributeEditorContainer>
      <attributeEditorContainer collapsedExpressionEnabled="0" name="" visibilityExpressionEnabled="1" showLabel="1" columnCount="1" groupBox="1" collapsedExpression="" collapsed="0" visibilityExpression="&quot;overload_celerity&quot;">
        <labelStyle overrideLabelFont="0" overrideLabelColor="0" labelColor="0,0,0,255">
          <labelFont bold="0" italic="0" underline="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" strikethrough="0" style=""/>
        </labelStyle>
        <attributeEditorField name="overloaded_celerity" showLabel="1" index="10">
          <labelStyle overrideLabelFont="0" overrideLabelColor="1" labelColor="0,0,255,255">
            <labelFont bold="0" italic="0" underline="0" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" strikethrough="0" style=""/>
          </labelStyle>
        </attributeEditorField>
      </attributeEditorContainer>
    </attributeEditorContainer>
  </attributeEditorForm>
  <editable>
    <field name="_celerity" editable="0"/>
    <field name="_elasticity_n_m2" editable="0"/>
    <field name="_id" editable="1"/>
    <field name="_material_name" editable="1"/>
    <field name="_model" editable="0"/>
    <field name="_roughness_mm" editable="0"/>
    <field name="comment" editable="1"/>
    <field name="compute_contact_time" editable="1"/>
    <field name="configuration" editable="1"/>
    <field name="configuration_json" editable="1"/>
    <field name="diameter" editable="1"/>
    <field name="down" editable="0"/>
    <field name="id" editable="1"/>
    <field name="installation_date" editable="1"/>
    <field name="is_feeder" editable="1"/>
    <field name="material" editable="1"/>
    <field name="meter" editable="1"/>
    <field name="model" editable="1"/>
    <field name="name" editable="1"/>
    <field name="overload_celerity" editable="1"/>
    <field name="overload_length" editable="1"/>
    <field name="overload_roughness_mm" editable="1"/>
    <field name="overloaded_celerity" editable="1"/>
    <field name="overloaded_length" editable="1"/>
    <field name="overloaded_roughness_mm" editable="1"/>
    <field name="status" editable="1"/>
    <field name="status_open" editable="1"/>
    <field name="thickness" editable="1"/>
    <field name="thickness_mm" editable="1"/>
    <field name="up" editable="0"/>
    <field name="validity" editable="1"/>
  </editable>
  <labelOnTop>
    <field labelOnTop="0" name="_celerity"/>
    <field labelOnTop="0" name="_elasticity_n_m2"/>
    <field labelOnTop="0" name="_id"/>
    <field labelOnTop="0" name="_material_name"/>
    <field labelOnTop="0" name="_model"/>
    <field labelOnTop="0" name="_roughness_mm"/>
    <field labelOnTop="0" name="comment"/>
    <field labelOnTop="0" name="compute_contact_time"/>
    <field labelOnTop="0" name="configuration"/>
    <field labelOnTop="0" name="configuration_json"/>
    <field labelOnTop="0" name="diameter"/>
    <field labelOnTop="0" name="down"/>
    <field labelOnTop="0" name="id"/>
    <field labelOnTop="0" name="installation_date"/>
    <field labelOnTop="0" name="is_feeder"/>
    <field labelOnTop="0" name="material"/>
    <field labelOnTop="0" name="meter"/>
    <field labelOnTop="0" name="model"/>
    <field labelOnTop="0" name="name"/>
    <field labelOnTop="0" name="overload_celerity"/>
    <field labelOnTop="0" name="overload_length"/>
    <field labelOnTop="0" name="overload_roughness_mm"/>
    <field labelOnTop="0" name="overloaded_celerity"/>
    <field labelOnTop="0" name="overloaded_length"/>
    <field labelOnTop="0" name="overloaded_roughness_mm"/>
    <field labelOnTop="0" name="status"/>
    <field labelOnTop="0" name="status_open"/>
    <field labelOnTop="0" name="thickness"/>
    <field labelOnTop="0" name="thickness_mm"/>
    <field labelOnTop="0" name="up"/>
    <field labelOnTop="0" name="validity"/>
  </labelOnTop>
  <reuseLastValue>
    <field name="_celerity" reuseLastValue="0"/>
    <field name="_elasticity_n_m2" reuseLastValue="0"/>
    <field name="_model" reuseLastValue="0"/>
    <field name="_roughness_mm" reuseLastValue="0"/>
    <field name="comment" reuseLastValue="0"/>
    <field name="compute_contact_time" reuseLastValue="0"/>
    <field name="diameter" reuseLastValue="0"/>
    <field name="down" reuseLastValue="0"/>
    <field name="id" reuseLastValue="0"/>
    <field name="installation_date" reuseLastValue="0"/>
    <field name="is_feeder" reuseLastValue="0"/>
    <field name="material" reuseLastValue="0"/>
    <field name="meter" reuseLastValue="0"/>
    <field name="name" reuseLastValue="0"/>
    <field name="overload_celerity" reuseLastValue="0"/>
    <field name="overload_length" reuseLastValue="0"/>
    <field name="overload_roughness_mm" reuseLastValue="0"/>
    <field name="overloaded_celerity" reuseLastValue="0"/>
    <field name="overloaded_length" reuseLastValue="0"/>
    <field name="overloaded_roughness_mm" reuseLastValue="0"/>
    <field name="status_open" reuseLastValue="0"/>
    <field name="thickness_mm" reuseLastValue="0"/>
    <field name="up" reuseLastValue="0"/>
  </reuseLastValue>
  <dataDefinedFieldProperties>
    <field name="overload_length">
      <Option type="Map">
        <Option type="QString" value="" name="name"/>
        <Option type="Map" name="properties">
          <Option type="Map" name="dataDefinedAlias">
            <Option type="bool" value="true" name="active"/>
            <Option type="QString" value="'Define custom length ? (Geometric length is '||round($length, 2)||'m)'" name="expression"/>
            <Option type="int" value="3" name="type"/>
          </Option>
        </Option>
        <Option type="QString" value="collection" name="type"/>
      </Option>
    </field>
  </dataDefinedFieldProperties>
  <widgets/>
  <previewExpression>"name"</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>1</layerGeometryType>
</qgis>
