<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis minScale="100000000" simplifyAlgorithm="0" simplifyLocal="1" simplifyMaxScale="1" symbologyReferenceScale="-1" hasScaleBasedVisibilityFlag="0" simplifyDrawingHints="1" version="3.22.11-Białowieża" styleCategories="AllStyleCategories" readOnly="0" labelsEnabled="0" simplifyDrawingTol="1" maxScale="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
    <Private>0</Private>
  </flags>
  <temporal durationField="" mode="0" endExpression="" fixedDuration="0" endField="" enabled="0" startField="" startExpression="" accumulate="0" limitMode="0" durationUnit="min">
    <fixedRange>
      <start></start>
      <end></end>
    </fixedRange>
  </temporal>
  <renderer-v2 symbollevels="0" type="singleSymbol" forceraster="0" enableorderby="0" referencescale="-1">
    <symbols>
      <symbol force_rhr="0" type="line" clip_to_extent="1" alpha="1" name="0">
        <data_defined_properties>
          <Option type="Map">
            <Option value="" type="QString" name="name"/>
            <Option name="properties"/>
            <Option value="collection" type="QString" name="type"/>
          </Option>
        </data_defined_properties>
        <layer class="SimpleLine" pass="0" enabled="1" locked="0">
          <Option type="Map">
            <Option value="0" type="QString" name="align_dash_pattern"/>
            <Option value="square" type="QString" name="capstyle"/>
            <Option value="5;2" type="QString" name="customdash"/>
            <Option value="3x:0,0,0,0,0,0" type="QString" name="customdash_map_unit_scale"/>
            <Option value="MM" type="QString" name="customdash_unit"/>
            <Option value="0" type="QString" name="dash_pattern_offset"/>
            <Option value="3x:0,0,0,0,0,0" type="QString" name="dash_pattern_offset_map_unit_scale"/>
            <Option value="MM" type="QString" name="dash_pattern_offset_unit"/>
            <Option value="0" type="QString" name="draw_inside_polygon"/>
            <Option value="bevel" type="QString" name="joinstyle"/>
            <Option value="13,219,116,255" type="QString" name="line_color"/>
            <Option value="solid" type="QString" name="line_style"/>
            <Option value="0.26" type="QString" name="line_width"/>
            <Option value="MM" type="QString" name="line_width_unit"/>
            <Option value="0" type="QString" name="offset"/>
            <Option value="3x:0,0,0,0,0,0" type="QString" name="offset_map_unit_scale"/>
            <Option value="MM" type="QString" name="offset_unit"/>
            <Option value="0" type="QString" name="ring_filter"/>
            <Option value="0" type="QString" name="trim_distance_end"/>
            <Option value="3x:0,0,0,0,0,0" type="QString" name="trim_distance_end_map_unit_scale"/>
            <Option value="MM" type="QString" name="trim_distance_end_unit"/>
            <Option value="0" type="QString" name="trim_distance_start"/>
            <Option value="3x:0,0,0,0,0,0" type="QString" name="trim_distance_start_map_unit_scale"/>
            <Option value="MM" type="QString" name="trim_distance_start_unit"/>
            <Option value="0" type="QString" name="tweak_dash_pattern_on_corners"/>
            <Option value="0" type="QString" name="use_custom_dash"/>
            <Option value="3x:0,0,0,0,0,0" type="QString" name="width_map_unit_scale"/>
          </Option>
          <prop k="align_dash_pattern" v="0"/>
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="dash_pattern_offset" v="0"/>
          <prop k="dash_pattern_offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="dash_pattern_offset_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="13,219,116,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="0.26"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="ring_filter" v="0"/>
          <prop k="trim_distance_end" v="0"/>
          <prop k="trim_distance_end_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="trim_distance_end_unit" v="MM"/>
          <prop k="trim_distance_start" v="0"/>
          <prop k="trim_distance_start_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="trim_distance_start_unit" v="MM"/>
          <prop k="tweak_dash_pattern_on_corners" v="0"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option type="Map" name="properties">
                <Option type="Map" name="outlineColor">
                  <Option value="true" type="bool" name="active"/>
                  <Option value="color_hsv(rand(0,360,sector), 100, 80)" type="QString" name="expression"/>
                  <Option value="3" type="int" name="type"/>
                </Option>
                <Option type="Map" name="outlineWidth">
                  <Option value="true" type="bool" name="active"/>
                  <Option value="0.666667*(coalesce(scale_linear(&quot;diameter&quot;, 0.01, 3, 0.1, 4), 0))*1.5" type="QString" name="expression"/>
                  <Option value="3" type="int" name="type"/>
                </Option>
              </Option>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
    <rotation/>
    <sizescale/>
  </renderer-v2>
  <customproperties>
    <Option type="Map">
      <Option type="List" name="dualview/previewExpressions">
        <Option value="&quot;name&quot;" type="QString"/>
      </Option>
      <Option value="0" type="QString" name="embeddedWidgets/count"/>
      <Option name="variableNames"/>
      <Option name="variableValues"/>
    </Option>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer diagramType="Histogram" attributeLegend="1">
    <DiagramCategory barWidth="5" scaleBasedVisibility="0" sizeType="MM" height="15" maxScaleDenominator="1e+08" spacing="0" spacingUnitScale="3x:0,0,0,0,0,0" backgroundColor="#ffffff" labelPlacementMethod="XHeight" scaleDependency="Area" minScaleDenominator="0" rotationOffset="270" sizeScale="3x:0,0,0,0,0,0" opacity="1" minimumSize="0" penColor="#000000" width="15" spacingUnit="MM" lineSizeType="MM" diagramOrientation="Up" penWidth="0" backgroundAlpha="255" penAlpha="255" direction="1" showAxis="0" enabled="0" lineSizeScale="3x:0,0,0,0,0,0">
      <fontProperties description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" style=""/>
      <attribute label="" colorOpacity="1" field="" color="#000000"/>
      <axisSymbol>
        <symbol force_rhr="0" type="line" clip_to_extent="1" alpha="1" name="">
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
          <layer class="SimpleLine" pass="0" enabled="1" locked="0">
            <Option type="Map">
              <Option value="0" type="QString" name="align_dash_pattern"/>
              <Option value="square" type="QString" name="capstyle"/>
              <Option value="5;2" type="QString" name="customdash"/>
              <Option value="3x:0,0,0,0,0,0" type="QString" name="customdash_map_unit_scale"/>
              <Option value="MM" type="QString" name="customdash_unit"/>
              <Option value="0" type="QString" name="dash_pattern_offset"/>
              <Option value="3x:0,0,0,0,0,0" type="QString" name="dash_pattern_offset_map_unit_scale"/>
              <Option value="MM" type="QString" name="dash_pattern_offset_unit"/>
              <Option value="0" type="QString" name="draw_inside_polygon"/>
              <Option value="bevel" type="QString" name="joinstyle"/>
              <Option value="35,35,35,255" type="QString" name="line_color"/>
              <Option value="solid" type="QString" name="line_style"/>
              <Option value="0.26" type="QString" name="line_width"/>
              <Option value="MM" type="QString" name="line_width_unit"/>
              <Option value="0" type="QString" name="offset"/>
              <Option value="3x:0,0,0,0,0,0" type="QString" name="offset_map_unit_scale"/>
              <Option value="MM" type="QString" name="offset_unit"/>
              <Option value="0" type="QString" name="ring_filter"/>
              <Option value="0" type="QString" name="trim_distance_end"/>
              <Option value="3x:0,0,0,0,0,0" type="QString" name="trim_distance_end_map_unit_scale"/>
              <Option value="MM" type="QString" name="trim_distance_end_unit"/>
              <Option value="0" type="QString" name="trim_distance_start"/>
              <Option value="3x:0,0,0,0,0,0" type="QString" name="trim_distance_start_map_unit_scale"/>
              <Option value="MM" type="QString" name="trim_distance_start_unit"/>
              <Option value="0" type="QString" name="tweak_dash_pattern_on_corners"/>
              <Option value="0" type="QString" name="use_custom_dash"/>
              <Option value="3x:0,0,0,0,0,0" type="QString" name="width_map_unit_scale"/>
            </Option>
            <prop k="align_dash_pattern" v="0"/>
            <prop k="capstyle" v="square"/>
            <prop k="customdash" v="5;2"/>
            <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <prop k="customdash_unit" v="MM"/>
            <prop k="dash_pattern_offset" v="0"/>
            <prop k="dash_pattern_offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <prop k="dash_pattern_offset_unit" v="MM"/>
            <prop k="draw_inside_polygon" v="0"/>
            <prop k="joinstyle" v="bevel"/>
            <prop k="line_color" v="35,35,35,255"/>
            <prop k="line_style" v="solid"/>
            <prop k="line_width" v="0.26"/>
            <prop k="line_width_unit" v="MM"/>
            <prop k="offset" v="0"/>
            <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <prop k="offset_unit" v="MM"/>
            <prop k="ring_filter" v="0"/>
            <prop k="trim_distance_end" v="0"/>
            <prop k="trim_distance_end_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <prop k="trim_distance_end_unit" v="MM"/>
            <prop k="trim_distance_start" v="0"/>
            <prop k="trim_distance_start_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <prop k="trim_distance_start_unit" v="MM"/>
            <prop k="tweak_dash_pattern_on_corners" v="0"/>
            <prop k="use_custom_dash" v="0"/>
            <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <data_defined_properties>
              <Option type="Map">
                <Option value="" type="QString" name="name"/>
                <Option name="properties"/>
                <Option value="collection" type="QString" name="type"/>
              </Option>
            </data_defined_properties>
          </layer>
        </symbol>
      </axisSymbol>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings dist="0" priority="0" obstacle="0" showAll="1" placement="2" linePlacementFlags="18" zIndex="0">
    <properties>
      <Option type="Map">
        <Option value="" type="QString" name="name"/>
        <Option name="properties"/>
        <Option value="collection" type="QString" name="type"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions removeDuplicateNodes="0" geometryPrecision="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <legend type="default-vector" showLabelLegend="0"/>
  <referencedLayers/>
  <fieldConfiguration>
    <field name="id" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="sector" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="_type" configurationFlags="None">
      <editWidget type="Enumeration">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="diameter" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="IsMultiline"/>
            <Option value="false" type="bool" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias field="id" name="" index="0"/>
    <alias field="sector" name="" index="1"/>
    <alias field="_type" name="" index="2"/>
    <alias field="diameter" name="Pipe diameter (m)" index="3"/>
  </aliases>
  <defaults>
    <default field="id" expression="" applyOnUpdate="0"/>
    <default field="sector" expression="" applyOnUpdate="0"/>
    <default field="_type" expression="" applyOnUpdate="0"/>
    <default field="diameter" expression="" applyOnUpdate="0"/>
  </defaults>
  <constraints>
    <constraint notnull_strength="1" constraints="3" exp_strength="0" field="id" unique_strength="1"/>
    <constraint notnull_strength="0" constraints="0" exp_strength="0" field="sector" unique_strength="0"/>
    <constraint notnull_strength="0" constraints="0" exp_strength="0" field="_type" unique_strength="0"/>
    <constraint notnull_strength="1" constraints="1" exp_strength="0" field="diameter" unique_strength="0"/>
  </constraints>
  <constraintExpressions>
    <constraint desc="" field="id" exp=""/>
    <constraint desc="" field="sector" exp=""/>
    <constraint desc="" field="_type" exp=""/>
    <constraint desc="" field="diameter" exp=""/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction value="{00000000-0000-0000-0000-000000000000}" key="Canvas"/>
  </attributeactions>
  <attributetableconfig actionWidgetStyle="dropDown" sortExpression="&quot;sector&quot;" sortOrder="1">
    <columns>
      <column type="actions" width="-1" hidden="1"/>
      <column type="field" width="-1" name="diameter" hidden="0"/>
      <column type="field" width="-1" name="sector" hidden="0"/>
      <column type="field" width="-1" name="_type" hidden="0"/>
      <column type="field" width="-1" name="id" hidden="0"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1">WINDOWS/System32</editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath>WINDOWS/System32</editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appelée à l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalités à vos formulaires.

Entrez le nom de la fonction dans le champ
"Fonction d'initialisation Python"
Voici un exemple à suivre:
"""
from PyQt4.QtGui import QWidget

def my_form_open(dialog, layer, feature):
⇥geom = feature.geometry()
⇥control = dialog.findChild(QWidget, "MyLineEdit")
]]></editforminitcode>
  <featformsuppress>2</featformsuppress>
  <editorlayout>tablayout</editorlayout>
  <attributeEditorForm>
    <attributeEditorField showLabel="1" name="name" index="-1"/>
    <attributeEditorField showLabel="1" name="_model" index="-1"/>
    <attributeEditorField showLabel="1" name="comment" index="-1"/>
    <attributeEditorContainer visibilityExpression="" showLabel="1" visibilityExpressionEnabled="0" columnCount="2" name="Nodes" groupBox="1">
      <attributeEditorField showLabel="1" name="up" index="-1"/>
      <attributeEditorField showLabel="1" name="down" index="-1"/>
    </attributeEditorContainer>
    <attributeEditorField showLabel="1" name="diameter" index="3"/>
    <attributeEditorField showLabel="1" name="thickness_mm" index="-1"/>
    <attributeEditorContainer visibilityExpression="" showLabel="1" visibilityExpressionEnabled="0" columnCount="1" name="Length" groupBox="1">
      <attributeEditorField showLabel="1" name="overload_length" index="-1"/>
      <attributeEditorContainer visibilityExpression="&quot;overload_length&quot;" showLabel="1" visibilityExpressionEnabled="1" columnCount="1" name="" groupBox="1">
        <attributeEditorField showLabel="1" name="overloaded_length" index="-1"/>
      </attributeEditorContainer>
    </attributeEditorContainer>
    <attributeEditorField showLabel="1" name="status_open" index="-1"/>
    <attributeEditorField showLabel="1" name="meter" index="-1"/>
    <attributeEditorContainer visibilityExpression="" showLabel="1" visibilityExpressionEnabled="0" columnCount="1" name="Material settings" groupBox="1">
      <attributeEditorField showLabel="1" name="material" index="-1"/>
      <attributeEditorField showLabel="1" name="_elasticity_n_m2" index="-1"/>
    </attributeEditorContainer>
    <attributeEditorContainer visibilityExpression="" showLabel="1" visibilityExpressionEnabled="0" columnCount="1" name="Roughness" groupBox="0">
      <attributeEditorField showLabel="1" name="overload_roughness_mm" index="-1"/>
      <attributeEditorContainer visibilityExpression="not overload_roughness_mm" showLabel="1" visibilityExpressionEnabled="1" columnCount="1" name="Material roughness" groupBox="1">
        <attributeEditorField showLabel="1" name="_roughness_mm" index="-1"/>
      </attributeEditorContainer>
      <attributeEditorContainer visibilityExpression="&quot;overload_roughness_mm&quot;" showLabel="1" visibilityExpressionEnabled="1" columnCount="1" name="Custom roughness" groupBox="1">
        <attributeEditorField showLabel="1" name="overloaded_roughness_mm" index="-1"/>
      </attributeEditorContainer>
    </attributeEditorContainer>
    <attributeEditorContainer visibilityExpression="" showLabel="1" visibilityExpressionEnabled="0" columnCount="1" name="Celerity" groupBox="1">
      <attributeEditorField showLabel="1" name="overload_celerity" index="-1"/>
      <attributeEditorContainer visibilityExpression="not overload_celerity" showLabel="1" visibilityExpressionEnabled="1" columnCount="1" name="Calculated celerity" groupBox="1">
        <attributeEditorField showLabel="1" name="_celerity" index="-1"/>
      </attributeEditorContainer>
      <attributeEditorContainer visibilityExpression="&quot;overload_celerity&quot;" showLabel="1" visibilityExpressionEnabled="1" columnCount="1" name="Custom celerity" groupBox="1">
        <attributeEditorField showLabel="1" name="overloaded_celerity" index="-1"/>
      </attributeEditorContainer>
    </attributeEditorContainer>
  </attributeEditorForm>
  <editable>
    <field name="_celerity" editable="0"/>
    <field name="_elasticity_n_m2" editable="0"/>
    <field name="_id" editable="1"/>
    <field name="_material_name" editable="1"/>
    <field name="_model" editable="0"/>
    <field name="_roughness_mm" editable="0"/>
    <field name="_type" editable="1"/>
    <field name="comment" editable="1"/>
    <field name="configuration" editable="1"/>
    <field name="configuration_json" editable="1"/>
    <field name="diameter" editable="1"/>
    <field name="down" editable="0"/>
    <field name="id" editable="1"/>
    <field name="material" editable="1"/>
    <field name="meter" editable="1"/>
    <field name="model" editable="1"/>
    <field name="name" editable="1"/>
    <field name="overload_celerity" editable="1"/>
    <field name="overload_length" editable="1"/>
    <field name="overload_roughness_mm" editable="1"/>
    <field name="overloaded_celerity" editable="1"/>
    <field name="overloaded_length" editable="1"/>
    <field name="overloaded_roughness_mm" editable="1"/>
    <field name="sector" editable="1"/>
    <field name="status" editable="1"/>
    <field name="status_open" editable="1"/>
    <field name="thickness" editable="1"/>
    <field name="thickness_mm" editable="1"/>
    <field name="up" editable="0"/>
    <field name="validity" editable="1"/>
  </editable>
  <labelOnTop>
    <field labelOnTop="0" name="_celerity"/>
    <field labelOnTop="0" name="_elasticity_n_m2"/>
    <field labelOnTop="0" name="_id"/>
    <field labelOnTop="0" name="_material_name"/>
    <field labelOnTop="0" name="_model"/>
    <field labelOnTop="0" name="_roughness_mm"/>
    <field labelOnTop="0" name="_type"/>
    <field labelOnTop="0" name="comment"/>
    <field labelOnTop="0" name="configuration"/>
    <field labelOnTop="0" name="configuration_json"/>
    <field labelOnTop="0" name="diameter"/>
    <field labelOnTop="0" name="down"/>
    <field labelOnTop="0" name="id"/>
    <field labelOnTop="0" name="material"/>
    <field labelOnTop="0" name="meter"/>
    <field labelOnTop="0" name="model"/>
    <field labelOnTop="0" name="name"/>
    <field labelOnTop="0" name="overload_celerity"/>
    <field labelOnTop="0" name="overload_length"/>
    <field labelOnTop="0" name="overload_roughness_mm"/>
    <field labelOnTop="0" name="overloaded_celerity"/>
    <field labelOnTop="0" name="overloaded_length"/>
    <field labelOnTop="0" name="overloaded_roughness_mm"/>
    <field labelOnTop="0" name="sector"/>
    <field labelOnTop="0" name="status"/>
    <field labelOnTop="0" name="status_open"/>
    <field labelOnTop="0" name="thickness"/>
    <field labelOnTop="0" name="thickness_mm"/>
    <field labelOnTop="0" name="up"/>
    <field labelOnTop="0" name="validity"/>
  </labelOnTop>
  <reuseLastValue>
    <field reuseLastValue="0" name="_celerity"/>
    <field reuseLastValue="0" name="_elasticity_n_m2"/>
    <field reuseLastValue="0" name="_model"/>
    <field reuseLastValue="0" name="_roughness_mm"/>
    <field reuseLastValue="0" name="_type"/>
    <field reuseLastValue="0" name="comment"/>
    <field reuseLastValue="0" name="diameter"/>
    <field reuseLastValue="0" name="down"/>
    <field reuseLastValue="0" name="id"/>
    <field reuseLastValue="0" name="material"/>
    <field reuseLastValue="0" name="meter"/>
    <field reuseLastValue="0" name="name"/>
    <field reuseLastValue="0" name="overload_celerity"/>
    <field reuseLastValue="0" name="overload_length"/>
    <field reuseLastValue="0" name="overload_roughness_mm"/>
    <field reuseLastValue="0" name="overloaded_celerity"/>
    <field reuseLastValue="0" name="overloaded_length"/>
    <field reuseLastValue="0" name="overloaded_roughness_mm"/>
    <field reuseLastValue="0" name="sector"/>
    <field reuseLastValue="0" name="status_open"/>
    <field reuseLastValue="0" name="thickness_mm"/>
    <field reuseLastValue="0" name="up"/>
  </reuseLastValue>
  <dataDefinedFieldProperties>
    <field name="overload_length">
      <Option type="Map">
        <Option value="" type="QString" name="name"/>
        <Option type="Map" name="properties">
          <Option type="Map" name="dataDefinedAlias">
            <Option value="true" type="bool" name="active"/>
            <Option value="'Define custom length ? (Geometric length is '||round($length, 2)||'m)'" type="QString" name="expression"/>
            <Option value="3" type="int" name="type"/>
          </Option>
        </Option>
        <Option value="collection" type="QString" name="type"/>
      </Option>
    </field>
  </dataDefinedFieldProperties>
  <widgets/>
  <previewExpression>"name"</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>1</layerGeometryType>
</qgis>
