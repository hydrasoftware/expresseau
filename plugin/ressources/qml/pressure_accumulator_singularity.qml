<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis simplifyDrawingHints="0" simplifyMaxScale="1" version="3.22.8-Białowieża" simplifyAlgorithm="0" simplifyLocal="1" simplifyDrawingTol="1" styleCategories="AllStyleCategories" hasScaleBasedVisibilityFlag="0" maxScale="0" labelsEnabled="0" minScale="100000000" symbologyReferenceScale="-1" readOnly="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
    <Private>0</Private>
  </flags>
  <temporal endField="" endExpression="" durationField="id" durationUnit="min" enabled="0" mode="0" startField="" accumulate="0" limitMode="0" startExpression="" fixedDuration="0">
    <fixedRange>
      <start></start>
      <end></end>
    </fixedRange>
  </temporal>
  <renderer-v2 type="singleSymbol" enableorderby="0" referencescale="-1" forceraster="0" symbollevels="0">
    <symbols>
      <symbol type="marker" clip_to_extent="1" alpha="1" name="0" force_rhr="0">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" name="name" value=""/>
            <Option name="properties"/>
            <Option type="QString" name="type" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer locked="0" enabled="1" class="SimpleMarker" pass="0">
          <Option type="Map">
            <Option type="QString" name="angle" value="0"/>
            <Option type="QString" name="cap_style" value="square"/>
            <Option type="QString" name="color" value="100,149,237,255"/>
            <Option type="QString" name="horizontal_anchor_point" value="1"/>
            <Option type="QString" name="joinstyle" value="bevel"/>
            <Option type="QString" name="name" value="square_with_corners"/>
            <Option type="QString" name="offset" value="0,0"/>
            <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="offset_unit" value="MM"/>
            <Option type="QString" name="outline_color" value="0,0,0,255"/>
            <Option type="QString" name="outline_style" value="solid"/>
            <Option type="QString" name="outline_width" value="0"/>
            <Option type="QString" name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="outline_width_unit" value="MM"/>
            <Option type="QString" name="scale_method" value="diameter"/>
            <Option type="QString" name="size" value="3"/>
            <Option type="QString" name="size_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="size_unit" value="MM"/>
            <Option type="QString" name="vertical_anchor_point" value="1"/>
          </Option>
          <prop v="0" k="angle"/>
          <prop v="square" k="cap_style"/>
          <prop v="100,149,237,255" k="color"/>
          <prop v="1" k="horizontal_anchor_point"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="square_with_corners" k="name"/>
          <prop v="0,0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="0,0,0,255" k="outline_color"/>
          <prop v="solid" k="outline_style"/>
          <prop v="0" k="outline_width"/>
          <prop v="3x:0,0,0,0,0,0" k="outline_width_map_unit_scale"/>
          <prop v="MM" k="outline_width_unit"/>
          <prop v="diameter" k="scale_method"/>
          <prop v="3" k="size"/>
          <prop v="3x:0,0,0,0,0,0" k="size_map_unit_scale"/>
          <prop v="MM" k="size_unit"/>
          <prop v="1" k="vertical_anchor_point"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option type="Map" name="properties">
                <Option type="Map" name="fillColor">
                  <Option type="bool" name="active" value="true"/>
                  <Option type="QString" name="expression" value="case when &quot;_model&quot;=@current_model then '#6495ed' else '#B0E0E6' end"/>
                  <Option type="int" name="type" value="3"/>
                </Option>
                <Option type="Map" name="outlineColor">
                  <Option type="bool" name="active" value="true"/>
                  <Option type="QString" name="expression" value="case when &quot;_model&quot;=@current_model then '#FF000000' else '#FFAAAAAA' end"/>
                  <Option type="int" name="type" value="3"/>
                </Option>
              </Option>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
    <rotation/>
    <sizescale/>
  </renderer-v2>
  <customproperties>
    <Option type="Map">
      <Option type="QString" name="dualview/previewExpressions" value="name"/>
      <Option type="QString" name="embeddedWidgets/count" value="0"/>
      <Option name="variableNames"/>
      <Option name="variableValues"/>
    </Option>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer diagramType="Histogram" attributeLegend="1">
    <DiagramCategory backgroundColor="#ffffff" sizeScale="3x:0,0,0,0,0,0" showAxis="0" spacingUnitScale="3x:0,0,0,0,0,0" minimumSize="0" minScaleDenominator="0" scaleDependency="Area" scaleBasedVisibility="0" opacity="1" lineSizeType="MM" spacing="0" barWidth="5" diagramOrientation="Up" maxScaleDenominator="1e+08" lineSizeScale="3x:0,0,0,0,0,0" rotationOffset="270" width="15" spacingUnit="MM" direction="1" sizeType="MM" height="15" labelPlacementMethod="XHeight" penWidth="0" enabled="0" penAlpha="255" penColor="#000000" backgroundAlpha="255">
      <fontProperties style="" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0"/>
      <attribute field="" color="#000000" label=""/>
      <axisSymbol>
        <symbol type="line" clip_to_extent="1" alpha="1" name="" force_rhr="0">
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
          <layer locked="0" enabled="1" class="SimpleLine" pass="0">
            <Option type="Map">
              <Option type="QString" name="align_dash_pattern" value="0"/>
              <Option type="QString" name="capstyle" value="square"/>
              <Option type="QString" name="customdash" value="5;2"/>
              <Option type="QString" name="customdash_map_unit_scale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="customdash_unit" value="MM"/>
              <Option type="QString" name="dash_pattern_offset" value="0"/>
              <Option type="QString" name="dash_pattern_offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="dash_pattern_offset_unit" value="MM"/>
              <Option type="QString" name="draw_inside_polygon" value="0"/>
              <Option type="QString" name="joinstyle" value="bevel"/>
              <Option type="QString" name="line_color" value="35,35,35,255"/>
              <Option type="QString" name="line_style" value="solid"/>
              <Option type="QString" name="line_width" value="0.26"/>
              <Option type="QString" name="line_width_unit" value="MM"/>
              <Option type="QString" name="offset" value="0"/>
              <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="offset_unit" value="MM"/>
              <Option type="QString" name="ring_filter" value="0"/>
              <Option type="QString" name="trim_distance_end" value="0"/>
              <Option type="QString" name="trim_distance_end_map_unit_scale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="trim_distance_end_unit" value="MM"/>
              <Option type="QString" name="trim_distance_start" value="0"/>
              <Option type="QString" name="trim_distance_start_map_unit_scale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="trim_distance_start_unit" value="MM"/>
              <Option type="QString" name="tweak_dash_pattern_on_corners" value="0"/>
              <Option type="QString" name="use_custom_dash" value="0"/>
              <Option type="QString" name="width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            </Option>
            <prop v="0" k="align_dash_pattern"/>
            <prop v="square" k="capstyle"/>
            <prop v="5;2" k="customdash"/>
            <prop v="3x:0,0,0,0,0,0" k="customdash_map_unit_scale"/>
            <prop v="MM" k="customdash_unit"/>
            <prop v="0" k="dash_pattern_offset"/>
            <prop v="3x:0,0,0,0,0,0" k="dash_pattern_offset_map_unit_scale"/>
            <prop v="MM" k="dash_pattern_offset_unit"/>
            <prop v="0" k="draw_inside_polygon"/>
            <prop v="bevel" k="joinstyle"/>
            <prop v="35,35,35,255" k="line_color"/>
            <prop v="solid" k="line_style"/>
            <prop v="0.26" k="line_width"/>
            <prop v="MM" k="line_width_unit"/>
            <prop v="0" k="offset"/>
            <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
            <prop v="MM" k="offset_unit"/>
            <prop v="0" k="ring_filter"/>
            <prop v="0" k="trim_distance_end"/>
            <prop v="3x:0,0,0,0,0,0" k="trim_distance_end_map_unit_scale"/>
            <prop v="MM" k="trim_distance_end_unit"/>
            <prop v="0" k="trim_distance_start"/>
            <prop v="3x:0,0,0,0,0,0" k="trim_distance_start_map_unit_scale"/>
            <prop v="MM" k="trim_distance_start_unit"/>
            <prop v="0" k="tweak_dash_pattern_on_corners"/>
            <prop v="0" k="use_custom_dash"/>
            <prop v="3x:0,0,0,0,0,0" k="width_map_unit_scale"/>
            <data_defined_properties>
              <Option type="Map">
                <Option type="QString" name="name" value=""/>
                <Option name="properties"/>
                <Option type="QString" name="type" value="collection"/>
              </Option>
            </data_defined_properties>
          </layer>
        </symbol>
      </axisSymbol>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings priority="0" showAll="1" dist="0" zIndex="0" placement="0" linePlacementFlags="18" obstacle="0">
    <properties>
      <Option type="Map">
        <Option type="QString" name="name" value=""/>
        <Option name="properties"/>
        <Option type="QString" name="type" value="collection"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions removeDuplicateNodes="0" geometryPrecision="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <legend type="default-vector" showLabelLegend="0"/>
  <referencedLayers>
    <relation strength="Association" id="Pressure_accumulator_e59010bd_8eb1_44c5_aaf0_7f41a9bc1a52_id_All_nodes_27bc14f1_f2c6_42ed_b685_bc5cdd0edb8b_id" layerName="All nodes" name="Pressure accumulator id" providerKey="postgres" referencedLayer="All_nodes_27bc14f1_f2c6_42ed_b685_bc5cdd0edb8b" layerId="All_nodes_27bc14f1_f2c6_42ed_b685_bc5cdd0edb8b" referencingLayer="Pressure_accumulator_e59010bd_8eb1_44c5_aaf0_7f41a9bc1a52" dataSource="dbname='est110722bis' service='expresseau' sslmode=disable key='id' checkPrimaryKeyUnicity='0' table=&quot;api&quot;.&quot;node&quot; (geom)">
      <fieldRef referencingField="id" referencedField="id"/>
    </relation>
  </referencedLayers>
  <fieldConfiguration>
    <field name="id" configurationFlags="None">
      <editWidget type="RelationReference">
        <config>
          <Option type="Map">
            <Option type="bool" name="AllowAddFeatures" value="false"/>
            <Option type="bool" name="AllowNULL" value="false"/>
            <Option type="bool" name="MapIdentification" value="true"/>
            <Option type="bool" name="OrderByValue" value="false"/>
            <Option type="bool" name="ReadOnly" value="true"/>
            <Option type="QString" name="ReferencedLayerDataSource" value="dbname='est110722bis' service='expresseau' sslmode=disable key='id' checkPrimaryKeyUnicity='0' table=&quot;api&quot;.&quot;node&quot; (geom)"/>
            <Option type="QString" name="ReferencedLayerId" value="All_nodes_27bc14f1_f2c6_42ed_b685_bc5cdd0edb8b"/>
            <Option type="QString" name="ReferencedLayerName" value="All nodes"/>
            <Option type="QString" name="ReferencedLayerProviderKey" value="postgres"/>
            <Option type="QString" name="Relation" value="Pressure_accumulator_e59010bd_8eb1_44c5_aaf0_7f41a9bc1a52_id_All_nodes_27bc14f1_f2c6_42ed_b685_bc5cdd0edb8b_id"/>
            <Option type="bool" name="ShowForm" value="false"/>
            <Option type="bool" name="ShowOpenFormButton" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="name" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="v0" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="z_connection" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="filling_pressure" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="comment" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="true"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="_model" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias field="id" name="Node" index="0"/>
    <alias field="name" name="Name" index="1"/>
    <alias field="v0" name="Volume (m3)" index="2"/>
    <alias field="z_connection" name="Z connection (mNGF)" index="3"/>
    <alias field="filling_pressure" name="Filling pressure (mCE)" index="4"/>
    <alias field="comment" name="Comment" index="5"/>
    <alias field="_model" name="Model" index="6"/>
  </aliases>
  <defaults>
    <default applyOnUpdate="1" field="id" expression="attribute(&#xd;&#xa;&#x9;array_first(&#xd;&#xa;&#x9;&#x9;array_filter(&#xd;&#xa;&#x9;&#x9;&#x9;overlay_intersects(&#xd;&#xa;&#x9;&#x9;&#x9;&#x9;&#x9;layer:='All_nodes_27bc14f1_f2c6_42ed_b685_bc5cdd0edb8b',&#xd;&#xa;&#x9;&#x9;&#x9;&#x9;&#x9;expression:=$currentfeature&#xd;&#xa;&#x9;&#x9;&#x9;),&#xd;&#xa;&#x9;&#x9;&#x9;intersects($geometry, geometry(@element))&#xd;&#xa;&#x9;&#x9;)&#xd;&#xa;&#x9;),&#xd;&#xa;&#x9;'id'&#xd;&#xa;)"/>
    <default applyOnUpdate="0" field="name" expression=""/>
    <default applyOnUpdate="0" field="v0" expression=""/>
    <default applyOnUpdate="0" field="z_connection" expression=""/>
    <default applyOnUpdate="0" field="filling_pressure" expression=""/>
    <default applyOnUpdate="0" field="comment" expression=""/>
    <default applyOnUpdate="0" field="_model" expression="attribute(&#xd;&#xa;&#x9;array_first(&#xd;&#xa;&#x9;&#x9;array_filter(&#xd;&#xa;&#x9;&#x9;&#x9;overlay_intersects(&#xd;&#xa;&#x9;&#x9;&#x9;&#x9;&#x9;layer:='All_nodes_27bc14f1_f2c6_42ed_b685_bc5cdd0edb8b',&#xd;&#xa;&#x9;&#x9;&#x9;&#x9;&#x9;expression:=$currentfeature&#xd;&#xa;&#x9;&#x9;&#x9;),&#xd;&#xa;&#x9;&#x9;&#x9;intersects($geometry, geometry(@element))&#xd;&#xa;&#x9;&#x9;)&#xd;&#xa;&#x9;),&#xd;&#xa;&#x9;'model'&#xd;&#xa;)"/>
  </defaults>
  <constraints>
    <constraint unique_strength="1" notnull_strength="1" exp_strength="0" field="id" constraints="3"/>
    <constraint unique_strength="0" notnull_strength="1" exp_strength="0" field="name" constraints="1"/>
    <constraint unique_strength="0" notnull_strength="1" exp_strength="1" field="v0" constraints="5"/>
    <constraint unique_strength="0" notnull_strength="1" exp_strength="0" field="z_connection" constraints="1"/>
    <constraint unique_strength="0" notnull_strength="1" exp_strength="0" field="filling_pressure" constraints="1"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" field="comment" constraints="0"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" field="_model" constraints="0"/>
  </constraints>
  <constraintExpressions>
    <constraint desc="" field="id" exp=""/>
    <constraint desc="" field="name" exp=""/>
    <constraint desc="" field="v0" exp=" &quot;v0&quot; >= 0"/>
    <constraint desc="" field="z_connection" exp=""/>
    <constraint desc="" field="filling_pressure" exp=""/>
    <constraint desc="" field="comment" exp=""/>
    <constraint desc="" field="_model" exp=""/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction key="Canvas" value="{00000000-0000-0000-0000-000000000000}"/>
    <actionsetting type="1" shortTitle="" isEnabledOnlyWhenEditable="0" capture="0" id="{32fa6fee-0e68-447a-b573-734604d202c2}" name="Open results" icon="" notificationMessage="" action="from expresseau.gui.result_dialog import ResultWindow&#xa;&#xa;if 'result_window' in locals() and result_window.isVisible():&#xa;    result_window.set_result([%id%],&#xa;                    '[% decode_uri(@layer, 'table')%]',&#xa;                    '[% @current_scenario%]')&#xa;    result_window.hide()&#xa;    result_window.show()&#xa;else:&#xa;    result_window = ResultWindow([%id%],&#xa;                    '[% decode_uri(@layer, 'table')%]',&#xa;                    '[% @current_scenario%]',&#xa;                    '[% @project_basename%]')&#xa;    result_window.open()&#xa;">
      <actionScope id="Feature"/>
      <actionScope id="Layer"/>
    </actionsetting>
  </attributeactions>
  <attributetableconfig sortExpression="" sortOrder="0" actionWidgetStyle="dropDown">
    <columns>
      <column type="field" width="-1" name="name" hidden="0"/>
      <column type="field" width="-1" name="v0" hidden="0"/>
      <column type="field" width="-1" name="z_connection" hidden="0"/>
      <column type="field" width="-1" name="filling_pressure" hidden="0"/>
      <column type="actions" width="-1" hidden="1"/>
      <column type="field" width="-1" name="comment" hidden="0"/>
      <column type="field" width="-1" name="id" hidden="0"/>
      <column type="field" width="-1" name="_model" hidden="0"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit>form_open</editforminit>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appelée à l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalités à vos formulaires.

Entrez le nom de la fonction dans le champ "Fonction d'initialisation Python".
Voici un exemple à suivre:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
	geom = feature.geometry()
	control = dialog.findChild(QWidget, "MyLineEdit")

]]></editforminitcode>
  <featformsuppress>2</featformsuppress>
  <editorlayout>tablayout</editorlayout>
  <attributeEditorForm>
    <attributeEditorField name="name" index="1" showLabel="1"/>
    <attributeEditorField name="_model" index="6" showLabel="1"/>
    <attributeEditorField name="id" index="0" showLabel="1"/>
    <attributeEditorField name="comment" index="5" showLabel="1"/>
    <attributeEditorField name="v0" index="2" showLabel="1"/>
    <attributeEditorField name="z_connection" index="3" showLabel="1"/>
    <attributeEditorField name="filling_pressure" index="4" showLabel="1"/>
  </attributeEditorForm>
  <editable>
    <field name="_id" editable="1"/>
    <field name="_model" editable="0"/>
    <field name="comment" editable="1"/>
    <field name="configuration" editable="1"/>
    <field name="configuration_json" editable="1"/>
    <field name="filling_pressure" editable="1"/>
    <field name="id" editable="0"/>
    <field name="model" editable="1"/>
    <field name="name" editable="1"/>
    <field name="node" editable="0"/>
    <field name="v0" editable="1"/>
    <field name="validity" editable="1"/>
    <field name="z_connection" editable="1"/>
  </editable>
  <labelOnTop>
    <field labelOnTop="0" name="_id"/>
    <field labelOnTop="0" name="_model"/>
    <field labelOnTop="0" name="comment"/>
    <field labelOnTop="0" name="configuration"/>
    <field labelOnTop="0" name="configuration_json"/>
    <field labelOnTop="0" name="filling_pressure"/>
    <field labelOnTop="0" name="id"/>
    <field labelOnTop="0" name="model"/>
    <field labelOnTop="0" name="name"/>
    <field labelOnTop="0" name="node"/>
    <field labelOnTop="0" name="v0"/>
    <field labelOnTop="0" name="validity"/>
    <field labelOnTop="0" name="z_connection"/>
  </labelOnTop>
  <reuseLastValue>
    <field name="_model" reuseLastValue="0"/>
    <field name="comment" reuseLastValue="0"/>
    <field name="filling_pressure" reuseLastValue="0"/>
    <field name="id" reuseLastValue="0"/>
    <field name="name" reuseLastValue="0"/>
    <field name="v0" reuseLastValue="0"/>
    <field name="z_connection" reuseLastValue="0"/>
  </reuseLastValue>
  <dataDefinedFieldProperties/>
  <widgets/>
  <previewExpression>"name"</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>0</layerGeometryType>
</qgis>
