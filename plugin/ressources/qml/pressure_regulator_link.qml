<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis styleCategories="AllStyleCategories" labelsEnabled="0" simplifyLocal="1" simplifyMaxScale="1" readOnly="0" hasScaleBasedVisibilityFlag="0" symbologyReferenceScale="-1" version="3.22.16-Białowieża" maxScale="0" minScale="100000000" simplifyAlgorithm="0" simplifyDrawingTol="1" simplifyDrawingHints="1">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
    <Private>0</Private>
  </flags>
  <temporal durationUnit="min" accumulate="0" enabled="0" endExpression="" endField="" mode="0" limitMode="0" startField="" startExpression="" durationField="" fixedDuration="0">
    <fixedRange>
      <start></start>
      <end></end>
    </fixedRange>
  </temporal>
  <renderer-v2 type="singleSymbol" referencescale="-1" forceraster="0" symbollevels="0" enableorderby="0">
    <symbols>
      <symbol force_rhr="0" alpha="1" type="line" name="0" clip_to_extent="1">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" value="" name="name"/>
            <Option name="properties"/>
            <Option type="QString" value="collection" name="type"/>
          </Option>
        </data_defined_properties>
        <layer enabled="1" locked="0" pass="0" class="SimpleLine">
          <Option type="Map">
            <Option type="QString" value="0" name="align_dash_pattern"/>
            <Option type="QString" value="square" name="capstyle"/>
            <Option type="QString" value="5;2" name="customdash"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="customdash_map_unit_scale"/>
            <Option type="QString" value="MM" name="customdash_unit"/>
            <Option type="QString" value="0" name="dash_pattern_offset"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="dash_pattern_offset_map_unit_scale"/>
            <Option type="QString" value="MM" name="dash_pattern_offset_unit"/>
            <Option type="QString" value="0" name="draw_inside_polygon"/>
            <Option type="QString" value="bevel" name="joinstyle"/>
            <Option type="QString" value="0,0,0,255" name="line_color"/>
            <Option type="QString" value="solid" name="line_style"/>
            <Option type="QString" value="0.26" name="line_width"/>
            <Option type="QString" value="MM" name="line_width_unit"/>
            <Option type="QString" value="0" name="offset"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
            <Option type="QString" value="MM" name="offset_unit"/>
            <Option type="QString" value="0" name="ring_filter"/>
            <Option type="QString" value="0" name="trim_distance_end"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="trim_distance_end_map_unit_scale"/>
            <Option type="QString" value="MM" name="trim_distance_end_unit"/>
            <Option type="QString" value="0" name="trim_distance_start"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="trim_distance_start_map_unit_scale"/>
            <Option type="QString" value="MM" name="trim_distance_start_unit"/>
            <Option type="QString" value="0" name="tweak_dash_pattern_on_corners"/>
            <Option type="QString" value="0" name="use_custom_dash"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="width_map_unit_scale"/>
          </Option>
          <prop k="align_dash_pattern" v="0"/>
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="dash_pattern_offset" v="0"/>
          <prop k="dash_pattern_offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="dash_pattern_offset_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="0,0,0,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="0.26"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="ring_filter" v="0"/>
          <prop k="trim_distance_end" v="0"/>
          <prop k="trim_distance_end_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="trim_distance_end_unit" v="MM"/>
          <prop k="trim_distance_start" v="0"/>
          <prop k="trim_distance_start_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="trim_distance_start_unit" v="MM"/>
          <prop k="tweak_dash_pattern_on_corners" v="0"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option type="Map" name="properties">
                <Option type="Map" name="outlineColor">
                  <Option type="bool" value="true" name="active"/>
                  <Option type="QString" value="case when &quot;_model&quot;=@current_model then '#FF000000' else '#FFAAAAAA' end" name="expression"/>
                  <Option type="int" value="3" name="type"/>
                </Option>
              </Option>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
        <layer enabled="1" locked="0" pass="0" class="MarkerLine">
          <Option type="Map">
            <Option type="QString" value="4" name="average_angle_length"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="average_angle_map_unit_scale"/>
            <Option type="QString" value="MM" name="average_angle_unit"/>
            <Option type="QString" value="3" name="interval"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="interval_map_unit_scale"/>
            <Option type="QString" value="MM" name="interval_unit"/>
            <Option type="QString" value="0" name="offset"/>
            <Option type="QString" value="0" name="offset_along_line"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_along_line_map_unit_scale"/>
            <Option type="QString" value="MM" name="offset_along_line_unit"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
            <Option type="QString" value="MM" name="offset_unit"/>
            <Option type="QString" value="centralpoint" name="placement"/>
            <Option type="QString" value="0" name="ring_filter"/>
            <Option type="QString" value="1" name="rotate"/>
          </Option>
          <prop k="average_angle_length" v="4"/>
          <prop k="average_angle_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="average_angle_unit" v="MM"/>
          <prop k="interval" v="3"/>
          <prop k="interval_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="interval_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_along_line" v="0"/>
          <prop k="offset_along_line_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_along_line_unit" v="MM"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="placement" v="centralpoint"/>
          <prop k="ring_filter" v="0"/>
          <prop k="rotate" v="1"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
          <symbol force_rhr="0" alpha="1" type="marker" name="@0@1" clip_to_extent="1">
            <data_defined_properties>
              <Option type="Map">
                <Option type="QString" value="" name="name"/>
                <Option name="properties"/>
                <Option type="QString" value="collection" name="type"/>
              </Option>
            </data_defined_properties>
            <layer enabled="1" locked="0" pass="0" class="SvgMarker">
              <Option type="Map">
                <Option type="QString" value="0" name="angle"/>
                <Option type="QString" value="45,45,255,255" name="color"/>
                <Option type="QString" value="0" name="fixedAspectRatio"/>
                <Option type="QString" value="1" name="horizontal_anchor_point"/>
                <Option type="QString" value="AEP_REGULATEUR_DEBIT_PRESSION.svg" name="name"/>
                <Option type="QString" value="0.14999999999999999,0.10000000000000001" name="offset"/>
                <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
                <Option type="QString" value="MM" name="offset_unit"/>
                <Option type="QString" value="0,0,0,255" name="outline_color"/>
                <Option type="QString" value="0" name="outline_width"/>
                <Option type="QString" value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale"/>
                <Option type="QString" value="MM" name="outline_width_unit"/>
                <Option name="parameters"/>
                <Option type="QString" value="diameter" name="scale_method"/>
                <Option type="QString" value="3" name="size"/>
                <Option type="QString" value="3x:0,0,0,0,0,0" name="size_map_unit_scale"/>
                <Option type="QString" value="MM" name="size_unit"/>
                <Option type="QString" value="1" name="vertical_anchor_point"/>
              </Option>
              <prop k="angle" v="0"/>
              <prop k="color" v="45,45,255,255"/>
              <prop k="fixedAspectRatio" v="0"/>
              <prop k="horizontal_anchor_point" v="1"/>
              <prop k="name" v="AEP_REGULATEUR_DEBIT_PRESSION.svg"/>
              <prop k="offset" v="0.14999999999999999,0.10000000000000001"/>
              <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="offset_unit" v="MM"/>
              <prop k="outline_color" v="0,0,0,255"/>
              <prop k="outline_width" v="0"/>
              <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="outline_width_unit" v="MM"/>
              <prop k="parameters" v=""/>
              <prop k="scale_method" v="diameter"/>
              <prop k="size" v="3"/>
              <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="size_unit" v="MM"/>
              <prop k="vertical_anchor_point" v="1"/>
              <data_defined_properties>
                <Option type="Map">
                  <Option type="QString" value="" name="name"/>
                  <Option type="Map" name="properties">
                    <Option type="Map" name="fillColor">
                      <Option type="bool" value="true" name="active"/>
                      <Option type="QString" value="set_color_part(@value, 'alpha', (case when &quot;_model&quot;=@current_model then 1 else 0.4 end) * 255 )" name="expression"/>
                      <Option type="int" value="3" name="type"/>
                    </Option>
                    <Option type="Map" name="outlineColor">
                      <Option type="bool" value="true" name="active"/>
                      <Option type="QString" value="case when &quot;_model&quot;=@current_model then '#FF000000' else '#FFAAAAAA' end" name="expression"/>
                      <Option type="int" value="3" name="type"/>
                    </Option>
                  </Option>
                  <Option type="QString" value="collection" name="type"/>
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
      </symbol>
    </symbols>
    <rotation/>
    <sizescale/>
  </renderer-v2>
  <customproperties>
    <Option type="Map">
      <Option type="QString" value="&quot;name&quot;" name="dualview/previewExpressions"/>
      <Option type="QString" value="0" name="embeddedWidgets/count"/>
      <Option name="variableNames"/>
      <Option name="variableValues"/>
    </Option>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer diagramType="Histogram" attributeLegend="1">
    <DiagramCategory lineSizeType="MM" penAlpha="255" diagramOrientation="Up" scaleBasedVisibility="0" direction="1" enabled="0" spacingUnitScale="3x:0,0,0,0,0,0" spacingUnit="MM" rotationOffset="270" width="15" penColor="#000000" scaleDependency="Area" backgroundColor="#ffffff" sizeScale="3x:0,0,0,0,0,0" barWidth="5" lineSizeScale="3x:0,0,0,0,0,0" spacing="0" showAxis="0" minimumSize="0" minScaleDenominator="0" height="15" penWidth="0" labelPlacementMethod="XHeight" sizeType="MM" backgroundAlpha="255" opacity="1" maxScaleDenominator="1e+08">
      <fontProperties description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" style=""/>
      <attribute color="#000000" field="" label="" colorOpacity="1"/>
      <axisSymbol>
        <symbol force_rhr="0" alpha="1" type="line" name="" clip_to_extent="1">
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
          <layer enabled="1" locked="0" pass="0" class="SimpleLine">
            <Option type="Map">
              <Option type="QString" value="0" name="align_dash_pattern"/>
              <Option type="QString" value="square" name="capstyle"/>
              <Option type="QString" value="5;2" name="customdash"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="customdash_map_unit_scale"/>
              <Option type="QString" value="MM" name="customdash_unit"/>
              <Option type="QString" value="0" name="dash_pattern_offset"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="dash_pattern_offset_map_unit_scale"/>
              <Option type="QString" value="MM" name="dash_pattern_offset_unit"/>
              <Option type="QString" value="0" name="draw_inside_polygon"/>
              <Option type="QString" value="bevel" name="joinstyle"/>
              <Option type="QString" value="35,35,35,255" name="line_color"/>
              <Option type="QString" value="solid" name="line_style"/>
              <Option type="QString" value="0.26" name="line_width"/>
              <Option type="QString" value="MM" name="line_width_unit"/>
              <Option type="QString" value="0" name="offset"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
              <Option type="QString" value="MM" name="offset_unit"/>
              <Option type="QString" value="0" name="ring_filter"/>
              <Option type="QString" value="0" name="trim_distance_end"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="trim_distance_end_map_unit_scale"/>
              <Option type="QString" value="MM" name="trim_distance_end_unit"/>
              <Option type="QString" value="0" name="trim_distance_start"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="trim_distance_start_map_unit_scale"/>
              <Option type="QString" value="MM" name="trim_distance_start_unit"/>
              <Option type="QString" value="0" name="tweak_dash_pattern_on_corners"/>
              <Option type="QString" value="0" name="use_custom_dash"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="width_map_unit_scale"/>
            </Option>
            <prop k="align_dash_pattern" v="0"/>
            <prop k="capstyle" v="square"/>
            <prop k="customdash" v="5;2"/>
            <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <prop k="customdash_unit" v="MM"/>
            <prop k="dash_pattern_offset" v="0"/>
            <prop k="dash_pattern_offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <prop k="dash_pattern_offset_unit" v="MM"/>
            <prop k="draw_inside_polygon" v="0"/>
            <prop k="joinstyle" v="bevel"/>
            <prop k="line_color" v="35,35,35,255"/>
            <prop k="line_style" v="solid"/>
            <prop k="line_width" v="0.26"/>
            <prop k="line_width_unit" v="MM"/>
            <prop k="offset" v="0"/>
            <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <prop k="offset_unit" v="MM"/>
            <prop k="ring_filter" v="0"/>
            <prop k="trim_distance_end" v="0"/>
            <prop k="trim_distance_end_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <prop k="trim_distance_end_unit" v="MM"/>
            <prop k="trim_distance_start" v="0"/>
            <prop k="trim_distance_start_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <prop k="trim_distance_start_unit" v="MM"/>
            <prop k="tweak_dash_pattern_on_corners" v="0"/>
            <prop k="use_custom_dash" v="0"/>
            <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <data_defined_properties>
              <Option type="Map">
                <Option type="QString" value="" name="name"/>
                <Option name="properties"/>
                <Option type="QString" value="collection" name="type"/>
              </Option>
            </data_defined_properties>
          </layer>
        </symbol>
      </axisSymbol>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings priority="0" linePlacementFlags="18" dist="0" zIndex="0" placement="2" obstacle="0" showAll="1">
    <properties>
      <Option type="Map">
        <Option type="QString" value="" name="name"/>
        <Option name="properties"/>
        <Option type="QString" value="collection" name="type"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions removeDuplicateNodes="0" geometryPrecision="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <legend type="default-vector" showLabelLegend="0"/>
  <referencedLayers>
    <relation providerKey="postgres" strength="Association" name="Pressure regulator down" referencingLayer="Pressure_regulator_5d1b74c6_1124_462a_89a2_adc490520eb3" id="Pressure_regulator_5d1b74c6_1124_462a_89a2_adc490520eb3_down_All_nodes_81d0c7bd_aa4a_4b37_bf4f_af3dd85067ef_id" referencedLayer="All_nodes_81d0c7bd_aa4a_4b37_bf4f_af3dd85067ef" layerName="All nodes" layerId="All_nodes_81d0c7bd_aa4a_4b37_bf4f_af3dd85067ef" dataSource="dbname='cabbalr' service='linux' sslmode=disable key='id' checkPrimaryKeyUnicity='0' table=&quot;api&quot;.&quot;node&quot; (geom)">
      <fieldRef referencingField="down" referencedField="id"/>
    </relation>
    <relation providerKey="postgres" strength="Association" name="Pressure regulator up" referencingLayer="Pressure_regulator_5d1b74c6_1124_462a_89a2_adc490520eb3" id="Pressure_regulator_5d1b74c6_1124_462a_89a2_adc490520eb3_up_All_nodes_81d0c7bd_aa4a_4b37_bf4f_af3dd85067ef_id" referencedLayer="All_nodes_81d0c7bd_aa4a_4b37_bf4f_af3dd85067ef" layerName="All nodes" layerId="All_nodes_81d0c7bd_aa4a_4b37_bf4f_af3dd85067ef" dataSource="dbname='cabbalr' service='linux' sslmode=disable key='id' checkPrimaryKeyUnicity='0' table=&quot;api&quot;.&quot;node&quot; (geom)">
      <fieldRef referencingField="up" referencedField="id"/>
    </relation>
  </referencedLayers>
  <fieldConfiguration>
    <field name="id" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="name" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="section" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="headloss_coef" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="pressure_regulation_option" configurationFlags="None">
      <editWidget type="Enumeration">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="regul_p" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="regul_pq_array" configurationFlags="None">
      <editWidget type="Array">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="pressure_regulation_mode" configurationFlags="None">
      <editWidget type="Enumeration">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="pid_p" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="pid_i" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="pid_d" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="comment" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="true" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="down" configurationFlags="None">
      <editWidget type="RelationReference">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="AllowAddFeatures"/>
            <Option type="bool" value="false" name="AllowNULL"/>
            <Option type="bool" value="true" name="MapIdentification"/>
            <Option type="bool" value="false" name="OrderByValue"/>
            <Option type="bool" value="true" name="ReadOnly"/>
            <Option type="QString" value="dbname='cabbalr' service='linux' sslmode=disable key='id' checkPrimaryKeyUnicity='0' table=&quot;api&quot;.&quot;node&quot; (geom)" name="ReferencedLayerDataSource"/>
            <Option type="QString" value="All_nodes_81d0c7bd_aa4a_4b37_bf4f_af3dd85067ef" name="ReferencedLayerId"/>
            <Option type="QString" value="All nodes" name="ReferencedLayerName"/>
            <Option type="QString" value="postgres" name="ReferencedLayerProviderKey"/>
            <Option type="QString" value="Pressure_regulator_5d1b74c6_1124_462a_89a2_adc490520eb3_down_All_nodes_81d0c7bd_aa4a_4b37_bf4f_af3dd85067ef_id" name="Relation"/>
            <Option type="bool" value="false" name="ShowForm"/>
            <Option type="bool" value="false" name="ShowOpenFormButton"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="up" configurationFlags="None">
      <editWidget type="RelationReference">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="AllowAddFeatures"/>
            <Option type="bool" value="false" name="AllowNULL"/>
            <Option type="bool" value="true" name="MapIdentification"/>
            <Option type="bool" value="false" name="OrderByValue"/>
            <Option type="bool" value="true" name="ReadOnly"/>
            <Option type="QString" value="dbname='cabbalr' service='linux' sslmode=disable key='id' checkPrimaryKeyUnicity='0' table=&quot;api&quot;.&quot;node&quot; (geom)" name="ReferencedLayerDataSource"/>
            <Option type="QString" value="All_nodes_81d0c7bd_aa4a_4b37_bf4f_af3dd85067ef" name="ReferencedLayerId"/>
            <Option type="QString" value="All nodes" name="ReferencedLayerName"/>
            <Option type="QString" value="postgres" name="ReferencedLayerProviderKey"/>
            <Option type="QString" value="Pressure_regulator_5d1b74c6_1124_462a_89a2_adc490520eb3_up_All_nodes_81d0c7bd_aa4a_4b37_bf4f_af3dd85067ef_id" name="Relation"/>
            <Option type="bool" value="false" name="ShowForm"/>
            <Option type="bool" value="false" name="ShowOpenFormButton"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="_model" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="false" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias field="id" name="" index="0"/>
    <alias field="name" name="Name" index="1"/>
    <alias field="section" name="Full section (m²)" index="2"/>
    <alias field="headloss_coef" name="Headloss coefficient" index="3"/>
    <alias field="pressure_regulation_option" name="Pressure regulation option" index="4"/>
    <alias field="regul_p" name="Controlled pressure (mCE)" index="5"/>
    <alias field="regul_pq_array" name="Curve P(Q)" index="6"/>
    <alias field="pressure_regulation_mode" name="Pressure regulation mode" index="7"/>
    <alias field="pid_p" name="P" index="8"/>
    <alias field="pid_i" name="I" index="9"/>
    <alias field="pid_d" name="D" index="10"/>
    <alias field="comment" name="Comment" index="11"/>
    <alias field="down" name="Node down" index="12"/>
    <alias field="up" name="Node up" index="13"/>
    <alias field="_model" name="Model" index="14"/>
  </aliases>
  <defaults>
    <default field="id" applyOnUpdate="0" expression=""/>
    <default field="name" applyOnUpdate="0" expression=""/>
    <default field="section" applyOnUpdate="0" expression=""/>
    <default field="headloss_coef" applyOnUpdate="0" expression=""/>
    <default field="pressure_regulation_option" applyOnUpdate="0" expression=""/>
    <default field="regul_p" applyOnUpdate="0" expression=""/>
    <default field="regul_pq_array" applyOnUpdate="0" expression=""/>
    <default field="pressure_regulation_mode" applyOnUpdate="0" expression=""/>
    <default field="pid_p" applyOnUpdate="0" expression=""/>
    <default field="pid_i" applyOnUpdate="0" expression=""/>
    <default field="pid_d" applyOnUpdate="0" expression=""/>
    <default field="comment" applyOnUpdate="0" expression=""/>
    <default field="down" applyOnUpdate="1" expression="attribute(&#xd;&#xa;&#x9;array_first(&#xd;&#xa;&#x9;&#x9;array_filter(&#xd;&#xa;&#x9;&#x9;&#x9;overlay_intersects(&#xd;&#xa;&#x9;&#x9;&#x9;&#x9;&#x9;layer:='All_nodes_81d0c7bd_aa4a_4b37_bf4f_af3dd85067ef',&#xd;&#xa;&#x9;&#x9;&#x9;&#x9;&#x9;expression:=$currentfeature&#xd;&#xa;&#x9;&#x9;&#x9;),&#xd;&#xa;&#x9;&#x9;&#x9;intersects(end_point($geometry), geometry(@element))&#xd;&#xa;&#x9;&#x9;)&#xd;&#xa;&#x9;),&#xd;&#xa;&#x9;'id'&#xd;&#xa;)"/>
    <default field="up" applyOnUpdate="1" expression="attribute(&#xd;&#xa;&#x9;array_first(&#xd;&#xa;&#x9;&#x9;array_filter(&#xd;&#xa;&#x9;&#x9;&#x9;overlay_intersects(&#xd;&#xa;&#x9;&#x9;&#x9;&#x9;&#x9;layer:='All_nodes_81d0c7bd_aa4a_4b37_bf4f_af3dd85067ef',&#xd;&#xa;&#x9;&#x9;&#x9;&#x9;&#x9;expression:=$currentfeature&#xd;&#xa;&#x9;&#x9;&#x9;),&#xd;&#xa;&#x9;&#x9;&#x9;intersects(start_point($geometry), geometry(@element))&#xd;&#xa;&#x9;&#x9;)&#xd;&#xa;&#x9;),&#xd;&#xa;&#x9;'id'&#xd;&#xa;)"/>
    <default field="_model" applyOnUpdate="0" expression="attribute(&#xd;&#xa;&#x9;array_first(&#xd;&#xa;&#x9;&#x9;array_filter(&#xd;&#xa;&#x9;&#x9;&#x9;overlay_intersects(&#xd;&#xa;&#x9;&#x9;&#x9;&#x9;&#x9;layer:='All_nodes_81d0c7bd_aa4a_4b37_bf4f_af3dd85067ef',&#xd;&#xa;&#x9;&#x9;&#x9;&#x9;&#x9;expression:=$currentfeature&#xd;&#xa;&#x9;&#x9;&#x9;),&#xd;&#xa;&#x9;&#x9;&#x9;intersects(start_point($geometry), geometry(@element))&#xd;&#xa;&#x9;&#x9;)&#xd;&#xa;&#x9;),&#xd;&#xa;&#x9;'model'&#xd;&#xa;)"/>
  </defaults>
  <constraints>
    <constraint field="id" notnull_strength="1" constraints="3" exp_strength="0" unique_strength="1"/>
    <constraint field="name" notnull_strength="1" constraints="1" exp_strength="0" unique_strength="0"/>
    <constraint field="section" notnull_strength="1" constraints="5" exp_strength="1" unique_strength="0"/>
    <constraint field="headloss_coef" notnull_strength="1" constraints="5" exp_strength="1" unique_strength="0"/>
    <constraint field="pressure_regulation_option" notnull_strength="1" constraints="1" exp_strength="0" unique_strength="0"/>
    <constraint field="regul_p" notnull_strength="0" constraints="4" exp_strength="1" unique_strength="0"/>
    <constraint field="regul_pq_array" notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint field="pressure_regulation_mode" notnull_strength="1" constraints="1" exp_strength="0" unique_strength="0"/>
    <constraint field="pid_p" notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint field="pid_i" notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint field="pid_d" notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint field="comment" notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0"/>
    <constraint field="down" notnull_strength="1" constraints="1" exp_strength="0" unique_strength="0"/>
    <constraint field="up" notnull_strength="1" constraints="1" exp_strength="0" unique_strength="0"/>
    <constraint field="_model" notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0"/>
  </constraints>
  <constraintExpressions>
    <constraint field="id" exp="" desc=""/>
    <constraint field="name" exp="" desc=""/>
    <constraint field="section" exp="&quot;section&quot; >= 0" desc=""/>
    <constraint field="headloss_coef" exp="&quot;headloss_coef&quot; >= 0" desc=""/>
    <constraint field="pressure_regulation_option" exp="" desc=""/>
    <constraint field="regul_p" exp=" &quot;pressure_regulation_option&quot; != 'Constant pressure' OR ( &quot;regul_p&quot; is not null and  &quot;regul_p&quot; >= 0)" desc=""/>
    <constraint field="regul_pq_array" exp="" desc=""/>
    <constraint field="pressure_regulation_mode" exp="" desc=""/>
    <constraint field="pid_p" exp="" desc=""/>
    <constraint field="pid_i" exp="" desc=""/>
    <constraint field="pid_d" exp="" desc=""/>
    <constraint field="comment" exp="" desc=""/>
    <constraint field="down" exp="" desc=""/>
    <constraint field="up" exp="" desc=""/>
    <constraint field="_model" exp="" desc=""/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction value="{00000000-0000-0000-0000-000000000000}" key="Canvas"/>
    <actionsetting icon="" type="1" shortTitle="" action="from expresseau.gui.result_dialog import ResultWindow&#xa;&#xa;if 'result_window' in locals() and result_window.isVisible():&#xa;    result_window.set_result([%id%],&#xa;                    '[% decode_uri(@layer, 'table')%]',&#xa;                    '[% @current_scenario%]')&#xa;    result_window.hide()&#xa;    result_window.show()&#xa;else:&#xa;    result_window = ResultWindow([%id%],&#xa;                    '[% decode_uri(@layer, 'table')%]',&#xa;                    '[% @current_scenario%]',&#xa;                    '[% @project_basename%]')&#xa;    result_window.open()&#xa;" notificationMessage="" name="Open results" id="{9925bfde-09c2-4dfb-a49c-ad12f7ff918d}" isEnabledOnlyWhenEditable="0" capture="0">
      <actionScope id="Layer"/>
      <actionScope id="Feature"/>
    </actionsetting>
  </attributeactions>
  <attributetableconfig sortOrder="0" sortExpression="" actionWidgetStyle="dropDown">
    <columns>
      <column width="-1" type="field" hidden="0" name="name"/>
      <column width="-1" type="field" hidden="0" name="up"/>
      <column width="-1" type="field" hidden="0" name="down"/>
      <column width="-1" type="field" hidden="0" name="section"/>
      <column width="-1" type="field" hidden="0" name="headloss_coef"/>
      <column width="-1" type="field" hidden="0" name="pressure_regulation_option"/>
      <column width="-1" type="field" hidden="0" name="regul_p"/>
      <column width="-1" type="field" hidden="0" name="regul_pq_array"/>
      <column width="-1" type="actions" hidden="1"/>
      <column width="-1" type="field" hidden="0" name="pressure_regulation_mode"/>
      <column width="-1" type="field" hidden="0" name="comment"/>
      <column width="-1" type="field" hidden="0" name="id"/>
      <column width="-1" type="field" hidden="0" name="pid_p"/>
      <column width="-1" type="field" hidden="0" name="pid_i"/>
      <column width="-1" type="field" hidden="0" name="pid_d"/>
      <column width="-1" type="field" hidden="0" name="_model"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit>form_open</editforminit>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
QGIS forms can have a Python function that is called when the form is
opened.

Use this function to add extra logic to your forms.

Enter the name of the function in the "Python Init function"
field.
An example follows:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
    geom = feature.geometry()
    control = dialog.findChild(QWidget, "MyLineEdit")
]]></editforminitcode>
  <featformsuppress>2</featformsuppress>
  <editorlayout>tablayout</editorlayout>
  <attributeEditorForm>
    <attributeEditorField showLabel="1" name="name" index="1"/>
    <attributeEditorField showLabel="1" name="_model" index="14"/>
    <attributeEditorField showLabel="1" name="comment" index="11"/>
    <attributeEditorContainer visibilityExpression="" showLabel="1" columnCount="2" name="Nodes" groupBox="1" visibilityExpressionEnabled="0">
      <attributeEditorField showLabel="1" name="up" index="13"/>
      <attributeEditorField showLabel="1" name="down" index="12"/>
    </attributeEditorContainer>
    <attributeEditorField showLabel="1" name="section" index="2"/>
    <attributeEditorField showLabel="1" name="headloss_coef" index="3"/>
    <attributeEditorContainer visibilityExpression="" showLabel="1" columnCount="1" name="Pressure regulation" groupBox="1" visibilityExpressionEnabled="0">
      <attributeEditorField showLabel="1" name="pressure_regulation_mode" index="7"/>
      <attributeEditorField showLabel="1" name="pressure_regulation_option" index="4"/>
      <attributeEditorContainer visibilityExpression=" &quot;pressure_regulation_option&quot;  = 'Constant pressure'" showLabel="1" columnCount="1" name="" groupBox="1" visibilityExpressionEnabled="1">
        <attributeEditorField showLabel="1" name="regul_p" index="5"/>
      </attributeEditorContainer>
      <attributeEditorContainer visibilityExpression=" &quot;pressure_regulation_option&quot;  = 'Curve P(Q)'" showLabel="1" columnCount="1" name="" groupBox="1" visibilityExpressionEnabled="1">
        <attributeEditorField showLabel="1" name="regul_pq_array" index="6"/>
      </attributeEditorContainer>
      <attributeEditorContainer visibilityExpression="" showLabel="1" columnCount="3" name="PID" groupBox="1" visibilityExpressionEnabled="0">
        <attributeEditorField showLabel="1" name="pid_p" index="8"/>
        <attributeEditorField showLabel="1" name="pid_i" index="9"/>
        <attributeEditorField showLabel="1" name="pid_d" index="10"/>
      </attributeEditorContainer>
    </attributeEditorContainer>
  </attributeEditorForm>
  <editable>
    <field editable="1" name="_id"/>
    <field editable="0" name="_model"/>
    <field editable="1" name="comment"/>
    <field editable="1" name="configuration"/>
    <field editable="1" name="configuration_json"/>
    <field editable="0" name="down"/>
    <field editable="1" name="headloss_coef"/>
    <field editable="1" name="id"/>
    <field editable="1" name="model"/>
    <field editable="1" name="name"/>
    <field editable="1" name="pid_d"/>
    <field editable="0" name="pid_i"/>
    <field editable="1" name="pid_p"/>
    <field editable="1" name="pressure_regulation_mode"/>
    <field editable="1" name="pressure_regulation_option"/>
    <field editable="1" name="regul_p"/>
    <field editable="1" name="regul_pq_array"/>
    <field editable="1" name="regulation_mode"/>
    <field editable="1" name="section"/>
    <field editable="0" name="up"/>
    <field editable="1" name="validity"/>
  </editable>
  <labelOnTop>
    <field labelOnTop="0" name="_id"/>
    <field labelOnTop="0" name="_model"/>
    <field labelOnTop="0" name="comment"/>
    <field labelOnTop="0" name="configuration"/>
    <field labelOnTop="0" name="configuration_json"/>
    <field labelOnTop="0" name="down"/>
    <field labelOnTop="0" name="headloss_coef"/>
    <field labelOnTop="0" name="id"/>
    <field labelOnTop="0" name="model"/>
    <field labelOnTop="0" name="name"/>
    <field labelOnTop="0" name="pid_d"/>
    <field labelOnTop="0" name="pid_i"/>
    <field labelOnTop="0" name="pid_p"/>
    <field labelOnTop="0" name="pressure_regulation_mode"/>
    <field labelOnTop="0" name="pressure_regulation_option"/>
    <field labelOnTop="0" name="regul_p"/>
    <field labelOnTop="0" name="regul_pq_array"/>
    <field labelOnTop="0" name="regulation_mode"/>
    <field labelOnTop="0" name="section"/>
    <field labelOnTop="0" name="up"/>
    <field labelOnTop="0" name="validity"/>
  </labelOnTop>
  <reuseLastValue>
    <field name="_model" reuseLastValue="0"/>
    <field name="comment" reuseLastValue="0"/>
    <field name="down" reuseLastValue="0"/>
    <field name="headloss_coef" reuseLastValue="0"/>
    <field name="id" reuseLastValue="0"/>
    <field name="name" reuseLastValue="0"/>
    <field name="pid_d" reuseLastValue="0"/>
    <field name="pid_i" reuseLastValue="0"/>
    <field name="pid_p" reuseLastValue="0"/>
    <field name="pressure_regulation_mode" reuseLastValue="0"/>
    <field name="pressure_regulation_option" reuseLastValue="0"/>
    <field name="regul_p" reuseLastValue="0"/>
    <field name="regul_pq_array" reuseLastValue="0"/>
    <field name="section" reuseLastValue="0"/>
    <field name="up" reuseLastValue="0"/>
  </reuseLastValue>
  <dataDefinedFieldProperties/>
  <widgets/>
  <previewExpression>"name"</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>1</layerGeometryType>
</qgis>
