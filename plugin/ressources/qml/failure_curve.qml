<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis version="3.22.8-Białowieża" styleCategories="AllStyleCategories" hasScaleBasedVisibilityFlag="0" maxScale="0" minScale="1e+08" readOnly="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
    <Private>0</Private>
  </flags>
  <temporal endField="" endExpression="" durationField="" durationUnit="min" enabled="0" mode="0" startField="" accumulate="0" limitMode="0" startExpression="" fixedDuration="0">
    <fixedRange>
      <start></start>
      <end></end>
    </fixedRange>
  </temporal>
  <customproperties>
    <Option type="Map">
      <Option type="int" name="embeddedWidgets/count" value="0"/>
      <Option type="invalid" name="variableNames"/>
      <Option type="invalid" name="variableValues"/>
    </Option>
  </customproperties>
  <geometryOptions removeDuplicateNodes="0" geometryPrecision="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <legend type="default-vector" showLabelLegend="0"/>
  <referencedLayers/>
  <fieldConfiguration>
    <field name="name" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="comment" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="alpt_array" configurationFlags="None">
      <editWidget type="Array">
        <config>
          <Option type="Map">
            <Option type="bool" name="IsMultiline" value="false"/>
            <Option type="bool" name="UseHtml" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias field="name" name="Name" index="0"/>
    <alias field="comment" name="Comment" index="1"/>
    <alias field="alpt_array" name="Alpha(t) curve" index="2"/>
  </aliases>
  <defaults>
    <default applyOnUpdate="0" field="name" expression=""/>
    <default applyOnUpdate="0" field="comment" expression=""/>
    <default applyOnUpdate="0" field="alpt_array" expression=""/>
  </defaults>
  <constraints>
    <constraint unique_strength="1" notnull_strength="1" exp_strength="0" field="name" constraints="3"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" field="comment" constraints="0"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" field="alpt_array" constraints="0"/>
  </constraints>
  <constraintExpressions>
    <constraint desc="" field="name" exp=""/>
    <constraint desc="" field="comment" exp=""/>
    <constraint desc="" field="alpt_array" exp=""/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction key="Canvas" value="{00000000-0000-0000-0000-000000000000}"/>
  </attributeactions>
  <attributetableconfig sortExpression="" sortOrder="0" actionWidgetStyle="dropDown">
    <columns>
      <column type="field" width="-1" name="name" hidden="0"/>
      <column type="field" width="-1" name="comment" hidden="0"/>
      <column type="field" width="-1" name="alpt_array" hidden="0"/>
      <column type="actions" width="-1" hidden="1"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit>form_open</editforminit>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[from expresseau.qgis.qgis_form_open import expresseau_form_open

def form_open(dialog, layer, feature):
    expresseau_form_open(dialog, layer, feature)]]></editforminitcode>
  <featformsuppress>2</featformsuppress>
  <editorlayout>tablayout</editorlayout>
  <attributeEditorForm>
    <attributeEditorField name="name" index="0" showLabel="1"/>
    <attributeEditorField name="comment" index="1" showLabel="1"/>
    <attributeEditorField name="alpt_array" index="2" showLabel="1"/>
  </attributeEditorForm>
  <editable>
    <field name="alpt_array" editable="1"/>
    <field name="comment" editable="1"/>
    <field name="name" editable="1"/>
  </editable>
  <labelOnTop>
    <field labelOnTop="0" name="alpt_array"/>
    <field labelOnTop="0" name="comment"/>
    <field labelOnTop="0" name="name"/>
  </labelOnTop>
  <reuseLastValue>
    <field name="alpt_array" reuseLastValue="0"/>
    <field name="comment" reuseLastValue="0"/>
    <field name="name" reuseLastValue="0"/>
  </reuseLastValue>
  <dataDefinedFieldProperties/>
  <widgets/>
  <previewExpression>"name"</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>4</layerGeometryType>
</qgis>
