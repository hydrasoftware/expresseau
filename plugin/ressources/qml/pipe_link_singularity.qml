<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis readOnly="0" styleCategories="AllStyleCategories" version="3.22.11-Białowieża" hasScaleBasedVisibilityFlag="0" minScale="1e+08" maxScale="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
    <Private>0</Private>
  </flags>
  <temporal enabled="0" endField="" durationUnit="min" fixedDuration="0" accumulate="0" limitMode="0" startField="" startExpression="" endExpression="" mode="0" durationField="id">
    <fixedRange>
      <start></start>
      <end></end>
    </fixedRange>
  </temporal>
  <customproperties>
    <Option type="Map">
      <Option type="List" name="dualview/previewExpressions">
        <Option value="COALESCE( &quot;id&quot;, '&lt;NULL>' )" type="QString"/>
        <Option value="COALESCE( &quot;id&quot;, '&lt;NULL>' )" type="QString"/>
        <Option value="COALESCE( &quot;id&quot;, '&lt;NULL>' )" type="QString"/>
        <Option value="COALESCE( &quot;id&quot;, '&lt;NULL>' )" type="QString"/>
        <Option value="&quot;type_singularity&quot;" type="QString"/>
        <Option value="COALESCE( &quot;id&quot;, '&lt;NULL>' )" type="QString"/>
        <Option value="COALESCE( &quot;id&quot;, '&lt;NULL>' )" type="QString"/>
        <Option value="COALESCE( &quot;id&quot;, '&lt;NULL>' )" type="QString"/>
      </Option>
      <Option value="0" type="int" name="embeddedWidgets/count"/>
      <Option name="variableNames"/>
      <Option name="variableValues"/>
    </Option>
  </customproperties>
  <geometryOptions removeDuplicateNodes="0" geometryPrecision="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <legend type="default-vector" showLabelLegend="0"/>
  <referencedLayers>
    <relation providerKey="postgres" layerId="All_nodes_fb4443f1_a294_4e9f_8026_66bd994d1e77" id="Pipe_singularity_108aeb0d_6dde_4465_b7d0_7a26bf6da1c2_id_All_nodes_bea2b5c4_a767_444e_a7e5_03c6c4d1affc_id" dataSource="dbname='pipe_xtest' service='expresseau' sslmode=disable key='id' checkPrimaryKeyUnicity='0' table=&quot;api&quot;.&quot;node&quot; (geom)" layerName="All nodes" referencedLayer="All_nodes_fb4443f1_a294_4e9f_8026_66bd994d1e77" strength="Association" referencingLayer="Pipe_singularity_cf0e17d6_9e04_4d7a_9efb_40830e619d42" name="Pipe singularity id">
      <fieldRef referencedField="id" referencingField="id"/>
    </relation>
    <relation providerKey="postgres" layerId="Pipe_55997f50_d740_41ce_be96_60afcc557711" id="Pipe_singularity_108aeb0d_6dde_4465_b7d0_7a26bf6da1c2_pipe_link_Pipe_af4345c3_732f_4c9e_9aff_a99677fe7422_id" dataSource="dbname='pipe_xtest' service='expresseau' sslmode=disable key='id' checkPrimaryKeyUnicity='0' table=&quot;api&quot;.&quot;pipe_link&quot; (geom)" layerName="Pipe" referencedLayer="Pipe_55997f50_d740_41ce_be96_60afcc557711" strength="Association" referencingLayer="Pipe_singularity_cf0e17d6_9e04_4d7a_9efb_40830e619d42" name="Pipe singularity pipe">
      <fieldRef referencedField="id" referencingField="pipe_link"/>
    </relation>
    <relation providerKey="postgres" layerId="All_nodes_fb4443f1_a294_4e9f_8026_66bd994d1e77" id="Pipe_singularity_cf0e17d6_9e04_4d7a_9efb_40830e619d42_id_All_nodes_fb4443f1_a294_4e9f_8026_66bd994d1e77_id" dataSource="dbname='pipe_xtest' service='expresseau' sslmode=disable key='id' checkPrimaryKeyUnicity='0' table=&quot;api&quot;.&quot;node&quot; (geom)" layerName="All nodes" referencedLayer="All_nodes_fb4443f1_a294_4e9f_8026_66bd994d1e77" strength="Association" referencingLayer="Pipe_singularity_cf0e17d6_9e04_4d7a_9efb_40830e619d42" name="Pipe singularity id">
      <fieldRef referencedField="id" referencingField="id"/>
    </relation>
    <relation providerKey="postgres" layerId="Pipe_55997f50_d740_41ce_be96_60afcc557711" id="Pipe_singularity_cf0e17d6_9e04_4d7a_9efb_40830e619d42_pipe_link_Pipe_55997f50_d740_41ce_be96_60afcc557711_id" dataSource="dbname='pipe_xtest' service='expresseau' sslmode=disable key='id' checkPrimaryKeyUnicity='0' table=&quot;api&quot;.&quot;pipe_link&quot; (geom)" layerName="Pipe" referencedLayer="Pipe_55997f50_d740_41ce_be96_60afcc557711" strength="Association" referencingLayer="Pipe_singularity_cf0e17d6_9e04_4d7a_9efb_40830e619d42" name="Pipe singularity pipe">
      <fieldRef referencedField="id" referencingField="pipe_link"/>
    </relation>
  </referencedLayers>
  <fieldConfiguration>
    <field configurationFlags="None" name="id">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="IsMultiline"/>
            <Option value="false" type="bool" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="pipe_link">
      <editWidget type="RelationReference">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="AllowAddFeatures"/>
            <Option value="false" type="bool" name="AllowNULL"/>
            <Option value="true" type="bool" name="MapIdentification"/>
            <Option value="false" type="bool" name="OrderByValue"/>
            <Option value="false" type="bool" name="ReadOnly"/>
            <Option value="dbname='pipe_xtest' service='expresseau' sslmode=disable key='id' checkPrimaryKeyUnicity='0' table=&quot;api&quot;.&quot;pipe_link&quot; (geom)" type="QString" name="ReferencedLayerDataSource"/>
            <Option value="Pipe_af4345c3_732f_4c9e_9aff_a99677fe7422" type="QString" name="ReferencedLayerId"/>
            <Option value="Pipe" type="QString" name="ReferencedLayerName"/>
            <Option value="postgres" type="QString" name="ReferencedLayerProviderKey"/>
            <Option value="Pipe_singularity_108aeb0d_6dde_4465_b7d0_7a26bf6da1c2_pipe_link_Pipe_af4345c3_732f_4c9e_9aff_a99677fe7422_id" type="QString" name="Relation"/>
            <Option value="false" type="bool" name="ShowForm"/>
            <Option value="true" type="bool" name="ShowOpenFormButton"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="number">
      <editWidget type="Range">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="AllowNull"/>
            <Option value="2147483647" type="int" name="Max"/>
            <Option value="1" type="int" name="Min"/>
            <Option value="0" type="int" name="Precision"/>
            <Option value="1" type="int" name="Step"/>
            <Option value="SpinBox" type="QString" name="Style"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="type_singularity">
      <editWidget type="Enumeration">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="bend_angle">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="IsMultiline"/>
            <Option value="false" type="bool" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="bend_radius">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="IsMultiline"/>
            <Option value="false" type="bool" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="bend_is_smooth">
      <editWidget type="CheckBox">
        <config>
          <Option type="Map">
            <Option value="" type="QString" name="CheckedState"/>
            <Option value="1" type="int" name="TextDisplayMethod"/>
            <Option value="" type="QString" name="UncheckedState"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="borda_section">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="IsMultiline"/>
            <Option value="false" type="bool" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="borda_coef">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="IsMultiline"/>
            <Option value="false" type="bool" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="_k_equivalent">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" type="bool" name="IsMultiline"/>
            <Option value="false" type="bool" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias index="0" field="id" name=""/>
    <alias index="1" field="pipe_link" name="Pipe"/>
    <alias index="2" field="number" name="Number of singularities"/>
    <alias index="3" field="type_singularity" name="Singularity type"/>
    <alias index="4" field="bend_angle" name="Angle (deg)"/>
    <alias index="5" field="bend_radius" name="Radius (m)"/>
    <alias index="6" field="bend_is_smooth" name=""/>
    <alias index="7" field="borda_section" name="Section (m²)"/>
    <alias index="8" field="borda_coef" name="Coefficient"/>
    <alias index="9" field="_k_equivalent" name="K"/>
  </aliases>
  <defaults>
    <default field="id" applyOnUpdate="0" expression=""/>
    <default field="pipe_link" applyOnUpdate="0" expression=""/>
    <default field="number" applyOnUpdate="0" expression=""/>
    <default field="type_singularity" applyOnUpdate="0" expression=""/>
    <default field="bend_angle" applyOnUpdate="0" expression=""/>
    <default field="bend_radius" applyOnUpdate="0" expression=""/>
    <default field="bend_is_smooth" applyOnUpdate="0" expression=""/>
    <default field="borda_section" applyOnUpdate="0" expression=""/>
    <default field="borda_coef" applyOnUpdate="0" expression=""/>
    <default field="_k_equivalent" applyOnUpdate="0" expression=""/>
  </defaults>
  <constraints>
    <constraint field="id" unique_strength="1" constraints="3" notnull_strength="1" exp_strength="0"/>
    <constraint field="pipe_link" unique_strength="0" constraints="1" notnull_strength="1" exp_strength="0"/>
    <constraint field="number" unique_strength="0" constraints="0" notnull_strength="0" exp_strength="0"/>
    <constraint field="type_singularity" unique_strength="0" constraints="1" notnull_strength="1" exp_strength="0"/>
    <constraint field="bend_angle" unique_strength="0" constraints="4" notnull_strength="0" exp_strength="1"/>
    <constraint field="bend_radius" unique_strength="0" constraints="0" notnull_strength="0" exp_strength="0"/>
    <constraint field="bend_is_smooth" unique_strength="0" constraints="4" notnull_strength="0" exp_strength="1"/>
    <constraint field="borda_section" unique_strength="0" constraints="4" notnull_strength="0" exp_strength="1"/>
    <constraint field="borda_coef" unique_strength="0" constraints="4" notnull_strength="0" exp_strength="1"/>
    <constraint field="_k_equivalent" unique_strength="0" constraints="0" notnull_strength="0" exp_strength="0"/>
  </constraints>
  <constraintExpressions>
    <constraint field="id" exp="" desc=""/>
    <constraint field="pipe_link" exp="" desc=""/>
    <constraint field="number" exp="" desc=""/>
    <constraint field="type_singularity" exp="" desc=""/>
    <constraint field="bend_angle" exp="(bend_angle is not null and bend_angle>0) or type_singularity!='Bend'" desc=""/>
    <constraint field="bend_radius" exp="" desc=""/>
    <constraint field="bend_is_smooth" exp="(bend_is_smooth is not null) or type_singularity!='Bend'" desc=""/>
    <constraint field="borda_section" exp="(borda_section is not null and borda_section>0) or type_singularity!='Borda headloss'" desc=""/>
    <constraint field="borda_coef" exp="(borda_coef is not null and borda_coef>0) or type_singularity!='Borda headloss'" desc=""/>
    <constraint field="_k_equivalent" exp="" desc=""/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction value="{00000000-0000-0000-0000-000000000000}" key="Canvas"/>
  </attributeactions>
  <attributetableconfig actionWidgetStyle="dropDown" sortExpression="" sortOrder="0">
    <columns>
      <column width="342" hidden="0" type="field" name="id"/>
      <column width="-1" hidden="0" type="field" name="pipe_link"/>
      <column width="-1" hidden="0" type="field" name="number"/>
      <column width="-1" hidden="0" type="field" name="type_singularity"/>
      <column width="-1" hidden="0" type="field" name="bend_angle"/>
      <column width="-1" hidden="0" type="field" name="borda_section"/>
      <column width="-1" hidden="0" type="field" name="borda_coef"/>
      <column width="-1" hidden="0" type="field" name="_k_equivalent"/>
      <column width="-1" hidden="0" type="field" name="bend_radius"/>
      <column width="-1" hidden="0" type="field" name="bend_is_smooth"/>
      <column width="-1" hidden="1" type="actions"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
QGIS forms can have a Python function that is called when the form is
opened.

Use this function to add extra logic to your forms.

Enter the name of the function in the "Python Init function"
field.
An example follows:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
    geom = feature.geometry()
    control = dialog.findChild(QWidget, "MyLineEdit")
]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>tablayout</editorlayout>
  <attributeEditorForm>
    <attributeEditorField index="1" showLabel="1" name="pipe_link"/>
    <attributeEditorField index="3" showLabel="1" name="type_singularity"/>
    <attributeEditorContainer visibilityExpression="type_singularity='Bend'" showLabel="1" groupBox="0" visibilityExpressionEnabled="1" columnCount="1" name="Bend">
      <attributeEditorField index="6" showLabel="1" name="bend_is_smooth"/>
      <attributeEditorField index="4" showLabel="1" name="bend_angle"/>
      <attributeEditorField index="5" showLabel="1" name="bend_radius"/>
    </attributeEditorContainer>
    <attributeEditorContainer visibilityExpression="type_singularity='Borda headloss'" showLabel="1" groupBox="0" visibilityExpressionEnabled="1" columnCount="1" name="Borda">
      <attributeEditorField index="7" showLabel="1" name="borda_section"/>
      <attributeEditorField index="8" showLabel="1" name="borda_coef"/>
    </attributeEditorContainer>
    <attributeEditorField index="2" showLabel="1" name="number"/>
    <attributeEditorField index="9" showLabel="1" name="_k_equivalent"/>
  </attributeEditorForm>
  <editable>
    <field editable="0" name="_k_equivalent"/>
    <field editable="1" name="bend_angle"/>
    <field editable="1" name="bend_is_smooth"/>
    <field editable="1" name="bend_radius"/>
    <field editable="1" name="borda_coef"/>
    <field editable="1" name="borda_section"/>
    <field editable="1" name="id"/>
    <field editable="1" name="number"/>
    <field editable="1" name="pipe_link"/>
    <field editable="1" name="type_singularity"/>
  </editable>
  <labelOnTop>
    <field labelOnTop="0" name="_k_equivalent"/>
    <field labelOnTop="0" name="bend_angle"/>
    <field labelOnTop="0" name="bend_is_smooth"/>
    <field labelOnTop="0" name="bend_radius"/>
    <field labelOnTop="0" name="borda_coef"/>
    <field labelOnTop="0" name="borda_section"/>
    <field labelOnTop="0" name="id"/>
    <field labelOnTop="0" name="number"/>
    <field labelOnTop="0" name="pipe_link"/>
    <field labelOnTop="0" name="type_singularity"/>
  </labelOnTop>
  <reuseLastValue>
    <field name="_k_equivalent" reuseLastValue="0"/>
    <field name="bend_angle" reuseLastValue="0"/>
    <field name="bend_is_smooth" reuseLastValue="0"/>
    <field name="bend_radius" reuseLastValue="0"/>
    <field name="borda_coef" reuseLastValue="0"/>
    <field name="borda_section" reuseLastValue="0"/>
    <field name="id" reuseLastValue="0"/>
    <field name="number" reuseLastValue="0"/>
    <field name="pipe_link" reuseLastValue="0"/>
    <field name="type_singularity" reuseLastValue="0"/>
  </reuseLastValue>
  <dataDefinedFieldProperties/>
  <widgets/>
  <previewExpression>COALESCE( "id", '&lt;NULL>' )</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>4</layerGeometryType>
</qgis>
