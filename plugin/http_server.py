import json
import os
import urllib
from qgis.PyQt.QtCore import QObject
from qgis.PyQt.QtNetwork import QTcpServer, QHostAddress
from .utility.log import LogManager, ConsoleLogger
from .database import autoconnection
from .project import Project

EXPRESSEAU_DIR = os.path.join(os.path.expanduser('~'), ".expresseau")

def reply(connection, msg=None):
    if msg:
        connection.write(msg)
    connection.disconnectFromHost()

class HttpServer(QObject):

    def __del__(self):
        self.log_manager.notice("shutdown http server")
        self.tcp_server and self.tcp_server.close()

    def __init__(self, iface=None, host="127.0.0.1", port=5001, log_manager= LogManager(ConsoleLogger()), parent=None):
        super().__init__(parent=parent)
        self.iface = iface
        self.log_manager = log_manager
        self.log_manager.notice(f"start tcp server http://{host}:{port}/")
        self.tcp_server = QTcpServer()
        if not self.tcp_server.listen(QHostAddress(host), port):
            self.log_manager.warning(f"cannot start tcp server http://{host}:{port}/")
            self.tcp_server = None
        else:
            self.tcp_server.newConnection.connect(self.tcp_connect)

    def tcp_read(self):
        connection = self.sender()
        try:
            req = connection.readAll().data().decode('utf8')
            # lecture des infos dans l'url
            splt = req.split()
            if len(splt) < 2:
                return reply(connection, b"HTTP/1.1 100 Continue")

            method, url = splt[:2]

            if '?' in url:

                param = dict([p.split('=') for p in url.split('?')[-1].split('&')])
                url = url.split('?')[0]
            else:
                param = None

            if url.startswith('/graph/') :
                # dans ce cas on lance la recherche de l'élément dans qgis
                elem = url.split('/')[-1]
                self.iface and self.iface.locatorSearch(f"af {elem}")
                return reply(connection, b"HTTP/1.1 204 No Content")

            elif url.startswith('/comment') and method=='GET':
                # dans ce cas on récupère les commentaires existants dans la base
                if param is None:
                    return reply(connection, b"HTTP/1.1 400 Bad Request")

                with autoconnection(param['dbname'], service=param['service'], debug=False) as con, con.cursor() as cur:

                    cur.execute('''
                        select coalesce(n.model, l._model, s._model) as model, coalesce(n.name, l.name, s.name) as name, c.txt
                        from api.comment as c
                        left join api.node n on n.id = c.node
                        left join api.link l on l.id = c.link
                        left join api.singularity s on s.id = c.singularity
                        ''')
                    data = json.dumps({f"{model}_{name}": txt for model, name, txt in cur.fetchall()}).encode('utf8')

                    return reply(connection, f"HTTP/1.1 200 OK\nContent-Type: application/json\nContent-Length: {len(data)}\n\n".encode('ascii') + data)

            elif url.startswith('/comment') and method=='POST' :
                # dans ce cas on envoit le commentaire dans la base
                if param is None:
                    return reply(connection, b"HTTP/1.1 400 Bad Request")

                # lecture des données du formulaire dans le corps de la requete http
                in_data = False
                data = {}
                for ll in req.split('\n'):
                    if in_data:
                        k, v = ll.strip().split('=')
                        data[k] = v
                    if ll.strip() == '':
                        in_data = True

                with autoconnection(param['dbname'], service=param['service'], debug=False) as con, con.cursor() as cur:
                    nlss = (param['id'] if param['table'].endswith('_node') else None,
                            param['id'] if param['table'].endswith('_link') else None,
                            param['id'] if param['table'].endswith('_singularity') else None,
                            param['scenario'])

                    cur.execute('''
                    update api.comment set txt = %s where (node=%s or link=%s or singularity=%s) and scenario=%s;

                    insert into api.comment(txt, node, link, singularity, scenario)
                    select %s, %s, %s, %s, %s
                    where not exists (select 1 from api.comment where (node=%s or link=%s or singularity=%s) and scenario=%s);
                    ''', ((urllib.parse.unquote_plus(data['message']),)+nlss)*2 + nlss)

                return reply(connection, b"HTTP/1.1 204 No Content")

            else:
                project_name, scn_name = url.split('/')[1], url.split('/')[3]
                project = Project(project_name, debug=True)
                pth = [project.directory]  + url.split('/')[2:]
                file_ = os.path.join(*pth)
                if os.path.exists(file_):
                    with open(file_) as h:
                        data = h.read().encode('utf8')
                    if url.endswith('.html'):
                        mime = 'text/html'
                    elif url.endswith('.svg'):
                        mime = 'image/svg+xml'
                    else:
                        mime = 'text/plain'
                    return reply(connection, f"HTTP/1.1 200 OK\nContent-Type: {mime}\nContent-Length: {len(data)}\n\n".encode('ascii') + data)
                else:
                    return reply(connection, b"HTTP/1.1 404 Not Found")
        except:
            reply(connection, b"HTTP/1.1 500 Internal Server Error")
            raise

        reply(connection)

    def tcp_connect(self):
        connection = self.tcp_server.nextPendingConnection()
        connection.readyRead.connect(self.tcp_read)


if __name__=='__main__':
    import signal
    import sys
    from qgis.PyQt.QtCore import QCoreApplication
    app = QCoreApplication(sys.argv)
    server = HttpServer(
        port=int(sys.argv[1]) if len(sys.argv) > 1 else 5001,
        host=sys.arv[2] if len(sys.argv) == 3 else "127.0.0.1")

    signal.signal(signal.SIGINT, signal.SIG_DFL)
    app.exec_()