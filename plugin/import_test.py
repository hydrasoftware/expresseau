
import os
import re

for root, dirs, files in os.walk(os.path.dirname(__file__)):
    for file_ in files:
        if file_.endswith(".py") and not file_.endswith("_test.py") and not file_=='__main__.py':
            module = re.sub('.*plugin', 'plugin', os.path.normpath(root)).replace('/', '.').replace('\\', '.')+(('.'+file_[:-3]) if not file_=='__init__.py' else '')
            imp = f"import {module}"
            #print(imp)
            exec(imp)
print('ok')
