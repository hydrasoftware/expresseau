# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of XPRESSO, a QGIS plugin for hydraulics                             ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

"""
Initializes the plugin, making it known to QGIS
"""

import os
import re
import requests
import xml.etree.ElementTree as ET
from distutils.version import StrictVersion
from qgis.utils import pluginMetadata
from qgis.core import Qgis, QgsMessageLog
from qgis.PyQt.QtWidgets import QPushButton
import subprocess
import importlib
import site
from pathlib import Path

if os.name == 'nt':
    # install wstar and dependency in local python directories
    qgis_version = str(Qgis.QGIS_VERSION_INT)[:3]

    if qgis_version == '322':
        requirements = Path(__file__).parent / f'requirements-{qgis_version}.txt'
    else:
        requirements = Path(__file__).parent / 'requirements.txt'

    try:
        import wstar
        wstar = importlib.reload(wstar)
        with open(requirements) as r:
            for line in r:
                if line[:5] == 'wstar':
                    version = line.strip().split('wstar==')[1]
        if not hasattr(wstar, '__version__') or version != wstar.__version__:
            raise ModuleNotFoundError

    except ModuleNotFoundError:
        QgsMessageLog.logMessage("Missing wstar, installing with pip", level=Qgis.Warning)
        cwd = Path(__file__).parent / 'dist'
        cmd_whl = ["python", "-m", "pip", "install", "--find-links", str(cwd), str(requirements)]
        cmd_req = ["python", "-m", "pip", "install", "-r", str(requirements)]
        wheel_install_success = False
        if cwd.is_dir() and list(cwd.glob('*.whl')):
            # official distribution includes .whl in dist
            cmd = cmd_whl
            try:
                subprocess.check_output(cmd, creationflags=subprocess.CREATE_NO_WINDOW, cwd=str(cwd), universal_newlines=True)
                wheel_install_success = True
            except subprocess.CalledProcessError:
                pass

        if not wheel_install_success:
            # development version or uses install from wstar gitlab repository
            cmd = cmd_req
            cwd = Path(__file__).parent
            try:
                subprocess.check_output(cmd, creationflags=subprocess.CREATE_NO_WINDOW, cwd=str(cwd), stderr=subprocess.STDOUT, universal_newlines=True)
            except subprocess.CalledProcessError as exc:
                raise RuntimeError(f"{' '.join(cmd)}\n{exc.output}")

        # make sure new modules will be found from now on
        importlib.reload(site)
        importlib.invalidate_caches()

_expresseau_dir = Path('~').expanduser() / ".expresseau"

# create expresseau directory
if not _expresseau_dir.is_dir():
    os.makedirs(_expresseau_dir)

# reset log file
with open(_expresseau_dir / 'expresseau.log', 'w') as f:
    pass

# ENVIRONMENTS
if 'PGSERVICEFILE' not in os.environ: # .pg_service.conf not set in environment
    os.environ['PGSERVICEFILE'] = str(Path('~').expanduser() / ".pg_service.conf")

if os.name == 'nt' and subprocess.call("where pg_dump") == 1: # pgdump.exe not in path
    os.environ['PATH'] = os.environ['PATH'] + f";{Path(__file__).parent / 'bin'};"

def open_plugin_manager():
    import pyplugin_installer
    pyplugin_installer.instance().showPluginManagerWhenReady()

def classFactory(iface):
    # Check if plugin version is the latest one:
    try:
        expresseau_plugins = ET.fromstring(requests.get('https://hydra-software.net/telechargement/expresseau.xml', headers={'User-Agent': 'whatever'}, timeout=2).text)
        for plugin in expresseau_plugins:
            # read each plugin's metadata from xml file
            if plugin.attrib['name'] == 'expresseau':
                qgis_minimum_version = plugin.find('qgis_minimum_version').text
                # if plugin is expresseau and to be used with qgis >3, check available version
                if StrictVersion(qgis_minimum_version) > StrictVersion('3.0'):
                    current_version = pluginMetadata('expresseau', 'version')
                    available_version = plugin.attrib['version']
                    if re.match(r'\d+\.\d+\.\d+', current_version) and StrictVersion(current_version) < StrictVersion(available_version):
                        widget = iface.messageBar().createMessage("New expresseau version", f"Version {available_version} is available for expresseau ! Open the QGIS plugin manager to download it.")
                        button = QPushButton(widget)
                        button.setText("Plugin manager")
                        button.clicked.connect(open_plugin_manager)
                        widget.layout().addWidget(button)
                        iface.messageBar().pushWidget(widget, level=Qgis.Info, duration=30)
                    break
    except requests.exceptions.Timeout:
        pass
    except requests.exceptions.ConnectionError:
        pass
    except requests.exceptions.SSLError:
        pass

    from .plugin import Expresseau
    return Expresseau(iface)
