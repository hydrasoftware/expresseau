[general]
name=expresseau
qgisMinimumVersion=3.22
description=Simulation of drinking water distribution networks
about=Expresseau allow steady-state, gradualy-transient and fast-transient simulation of presurized water distribution networks. The model is stored in a PostgreSQL/PostGIS database.
version=version_placeholder
author=setec hydratec
email=support@hydra-software.net
repository=https://gitlab.com/hydrasoftware/expresseau.git
tags=hydraulics, simulation
experimental=True
deprecated=False
homepage=http://hydra-software.net/
icon=ressources/images/expresseau_logo.png

