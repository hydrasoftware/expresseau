<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR" sourcelanguage="en_US">
<context>
    <name>user_node.qml</name>
    <message>
        <location filename="../ressources/qml/user_node.qml" line="1"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../ressources/qml/user_node.qml" line="1"/>
        <source>Domestic modulation curve</source>
        <translation>Courbe de modulation domestique</translation>
    </message>
    <message>
        <location filename="../ressources/qml/user_node.qml" line="1"/>
        <source>Industrial modulation curve</source>
        <translation>Courbe de modulation industrielle</translation>
    </message>
    <message>
        <location filename="../ressources/qml/user_node.qml" line="1"/>
        <source>Q0 (m3/hr)</source>
        <translation/>
    </message>
    <message>
        <location filename="../ressources/qml/user_node.qml" line="1"/>
        <source>Fire hydrant</source>
        <translation>Poteau incendie</translation>
    </message>
    <message>
        <location filename="../ressources/qml/user_node.qml" line="1"/>
        <source>Model</source>
        <translation>Modèle</translation>
    </message>
    <message>
        <location filename="../ressources/qml/user_node.qml" line="1"/>
        <source>Z ground (mNGF)</source>
        <translation>Z terrain (mNGF)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/user_node.qml" line="1"/>
        <source>Comment</source>
        <translation>Commentaire</translation>
    </message>
    <message>
        <location filename="../ressources/qml/user_node.qml" line="1"/>
        <source>Sector</source>
        <translation>Secteur</translation>
    </message>
    <message>
        <location filename="../ressources/qml/user_node.qml" line="1"/>
        <source>Depth (m)</source>
        <translation>Profondeur (m)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/user_node.qml" line="1"/>
        <source>Hourly modulation curve</source>
        <translation>Courbe de modulation horaire</translation>
    </message>
    <message>
        <location filename="../ressources/qml/user_node.qml" line="1"/>
        <source>Domestic consommation</source>
        <translation>Consommation domestique</translation>
    </message>
    <message>
        <location filename="../ressources/qml/user_node.qml" line="1"/>
        <source>Industrial consommation</source>
        <translation>Consommation industrielle</translation>
    </message>
</context>
<context>
    <name>pressure_regulator_link.qml</name>
    <message>
        <location filename="../ressources/qml/pressure_regulator_link.qml" line="1"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pressure_regulator_link.qml" line="1"/>
        <source>Full section (m²)</source>
        <translation>Section complète (m²)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pressure_regulator_link.qml" line="1"/>
        <source>Headloss coefficient</source>
        <translation>Coef. de perte de charge</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pressure_regulator_link.qml" line="1"/>
        <source>Pressure regulation option</source>
        <translation>Option de régulation de pression</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pressure_regulator_link.qml" line="1"/>
        <source>Controlled pressure (mCE)</source>
        <translation>Pression controlée (mCE)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pressure_regulator_link.qml" line="1"/>
        <source>Curve P(Q)</source>
        <translation>Courbe P(Q)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pressure_regulator_link.qml" line="1"/>
        <source>Pressure regulation mode</source>
        <translation>Mode de régulation de pression</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pressure_regulator_link.qml" line="1"/>
        <source>Comment</source>
        <translation>Commentaire</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pressure_regulator_link.qml" line="1"/>
        <source>Node down</source>
        <translation>Noeud aval</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pressure_regulator_link.qml" line="1"/>
        <source>Node up</source>
        <translation>Noeud amont</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pressure_regulator_link.qml" line="1"/>
        <source>Model</source>
        <translation>Modèle</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pressure_regulator_link.qml" line="1"/>
        <source>Nodes</source>
        <translation>Noeuds</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pressure_regulator_link.qml" line="1"/>
        <source>Pressure regulation</source>
        <translation>Régulation de pression</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pressure_regulator_link.qml" line="1"/>
        <source>All nodes</source>
        <translation>Tous les noeuds</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pressure_regulator_link.qml" line="1"/>
        <source>P</source>
        <translation>P</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pressure_regulator_link.qml" line="1"/>
        <source>I</source>
        <translation>I</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pressure_regulator_link.qml" line="1"/>
        <source>D</source>
        <translation>D</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pressure_regulator_link.qml" line="1"/>
        <source>PID</source>
        <translation>PID</translation>
    </message>
</context>
<context>
    <name>reservoir_node.qml</name>
    <message>
        <location filename="../ressources/qml/reservoir_node.qml" line="1"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../ressources/qml/reservoir_node.qml" line="1"/>
        <source>Curve S(Z)</source>
        <translation>Courbe S(Z)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/reservoir_node.qml" line="1"/>
        <source>Alimentation mode</source>
        <translation>Mode d'alimentation</translation>
    </message>
    <message>
        <location filename="../ressources/qml/reservoir_node.qml" line="1"/>
        <source>Z initial (mNGF)</source>
        <translation/>
    </message>
    <message>
        <location filename="../ressources/qml/reservoir_node.qml" line="1"/>
        <source>Z overflow (mNGF)</source>
        <translation>Z surverse (mNGF)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/reservoir_node.qml" line="1"/>
        <source>Valve mode (in)</source>
        <translation>Mode clapet (entrant)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/reservoir_node.qml" line="1"/>
        <source>Valve mode (out)</source>
        <translation>Mode clapet (sortant)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/reservoir_node.qml" line="1"/>
        <source>Mixing mode</source>
        <translation>Mode mélange</translation>
    </message>
    <message>
        <location filename="../ressources/qml/reservoir_node.qml" line="1"/>
        <source>Model</source>
        <translation>Modèle</translation>
    </message>
    <message>
        <location filename="../ressources/qml/reservoir_node.qml" line="1"/>
        <source>Z ground (mNGF)</source>
        <translation>Z terrain (mNGF)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/reservoir_node.qml" line="1"/>
        <source>Comment</source>
        <translation>Commentaire</translation>
    </message>
    <message>
        <location filename="../ressources/qml/reservoir_node.qml" line="1"/>
        <source>Depth (m)</source>
        <translation>Profondeur (m)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/reservoir_node.qml" line="1"/>
        <source>Tank geometry</source>
        <translation>Géométrie du réservoir</translation>
    </message>
    <message>
        <location filename="../ressources/qml/reservoir_node.qml" line="1"/>
        <source>In and out flow control valves</source>
        <translation>Vannes de controle d'entrée/sortie</translation>
    </message>
    <message>
        <location filename="../ressources/qml/reservoir_node.qml" line="1"/>
        <source>Reset travel time</source>
        <translation>Réinitialise le temps de séjour</translation>
    </message>
</context>
<context>
    <name>surge_tank_singularity.qml</name>
    <message>
        <location filename="../ressources/qml/surge_tank_singularity.qml" line="1"/>
        <source>Node</source>
        <translation>Noeud</translation>
    </message>
    <message>
        <location filename="../ressources/qml/surge_tank_singularity.qml" line="1"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../ressources/qml/surge_tank_singularity.qml" line="1"/>
        <source>Curve S(Z)</source>
        <translation>Courbe S(Z)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/surge_tank_singularity.qml" line="1"/>
        <source>Z initial (mNGF)</source>
        <translation/>
    </message>
    <message>
        <location filename="../ressources/qml/surge_tank_singularity.qml" line="1"/>
        <source>Pressurized ?</source>
        <translation>Pressurisé ?</translation>
    </message>
    <message>
        <location filename="../ressources/qml/surge_tank_singularity.qml" line="1"/>
        <source>Orifice diameter (m)</source>
        <translation>Diamètre orifice (m)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/surge_tank_singularity.qml" line="1"/>
        <source>Inwards headloss coef.</source>
        <translation>Coef. perte de charge entrant</translation>
    </message>
    <message>
        <location filename="../ressources/qml/surge_tank_singularity.qml" line="1"/>
        <source>Outwards headloss coef.</source>
        <translation>Coef. perte de charge sortant</translation>
    </message>
    <message>
        <location filename="../ressources/qml/surge_tank_singularity.qml" line="1"/>
        <source>Comment</source>
        <translation>Commentaire</translation>
    </message>
    <message>
        <location filename="../ressources/qml/surge_tank_singularity.qml" line="1"/>
        <source>Model</source>
        <translation>Modèle</translation>
    </message>
    <message>
        <location filename="../ressources/qml/surge_tank_singularity.qml" line="1"/>
        <source>Nodes</source>
        <translation>Noeuds</translation>
    </message>
    <message>
        <location filename="../ressources/qml/surge_tank_singularity.qml" line="1"/>
        <source>Filling curve</source>
        <translation>Courbe de remplissage</translation>
    </message>
    <message>
        <location filename="../ressources/qml/surge_tank_singularity.qml" line="1"/>
        <source>Headloss coefficients</source>
        <translation>Coef de perte de charge</translation>
    </message>
    <message>
        <location filename="../ressources/qml/surge_tank_singularity.qml" line="1"/>
        <source>With cover</source>
        <translation>Avec couvercle</translation>
    </message>
    <message>
        <location filename="../ressources/qml/surge_tank_singularity.qml" line="1"/>
        <source>All nodes</source>
        <translation>Tous les noeuds</translation>
    </message>
</context>
<context>
    <name>flow_injection_singularity.qml</name>
    <message>
        <location filename="../ressources/qml/flow_injection_singularity.qml" line="1"/>
        <source>Node</source>
        <translation>Noeud</translation>
    </message>
    <message>
        <location filename="../ressources/qml/flow_injection_singularity.qml" line="1"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../ressources/qml/flow_injection_singularity.qml" line="1"/>
        <source>Q injection (m3/hr)</source>
        <translation/>
    </message>
    <message>
        <location filename="../ressources/qml/flow_injection_singularity.qml" line="1"/>
        <source>External file</source>
        <translation>Fichier externe</translation>
    </message>
    <message>
        <location filename="../ressources/qml/flow_injection_singularity.qml" line="1"/>
        <source>Trihalomethane concentration (mg/l)</source>
        <translation>Concentration trihalométhane (mg/l)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/flow_injection_singularity.qml" line="1"/>
        <source>Fluorures concentration (mg/l)</source>
        <translation>Concentration fluorures (mg/l)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/flow_injection_singularity.qml" line="1"/>
        <source>Comment</source>
        <translation>Commentaire</translation>
    </message>
    <message>
        <location filename="../ressources/qml/flow_injection_singularity.qml" line="1"/>
        <source>Model</source>
        <translation>Modèle</translation>
    </message>
    <message>
        <location filename="../ressources/qml/flow_injection_singularity.qml" line="1"/>
        <source>Nodes</source>
        <translation>Noeuds</translation>
    </message>
    <message>
        <location filename="../ressources/qml/flow_injection_singularity.qml" line="1"/>
        <source>Concentrations</source>
        <translation/>
    </message>
    <message>
        <location filename="../ressources/qml/flow_injection_singularity.qml" line="1"/>
        <source>Chlorine (mg/l)</source>
        <translation>Chlore (mg/l)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/flow_injection_singularity.qml" line="1"/>
        <source>Trihalomethane (mg/l)</source>
        <translation>Trihalométhane (mg/l)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/flow_injection_singularity.qml" line="1"/>
        <source>Passive tracer (mg/l)</source>
        <translation>Traceur passif (mg/l)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/flow_injection_singularity.qml" line="1"/>
        <source>All nodes</source>
        <translation>Tous les noeuds</translation>
    </message>
    <message>
        <location filename="../ressources/qml/flow_injection_singularity.qml" line="1"/>
        <source>Cyclic</source>
        <translation>Cyclique</translation>
    </message>
    <message>
        <location filename="../ressources/qml/flow_injection_singularity.qml" line="1"/>
        <source>Travel time(h)</source>
        <translation>Temps de séjour (h)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/flow_injection_singularity.qml" line="1"/>
        <source>Contact time (h)</source>
        <translation>Temps de contact (h)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/flow_injection_singularity.qml" line="1"/>
        <source>Times</source>
        <translation>Temps</translation>
    </message>
</context>
<context>
    <name>imposed_piezometry_singularity.qml</name>
    <message>
        <location filename="../ressources/qml/imposed_piezometry_singularity.qml" line="1"/>
        <source>Node</source>
        <translation>Noeud</translation>
    </message>
    <message>
        <location filename="../ressources/qml/imposed_piezometry_singularity.qml" line="1"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../ressources/qml/imposed_piezometry_singularity.qml" line="1"/>
        <source>Imposed Z (mNGF)</source>
        <translation>Z imposé (mNGF)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/imposed_piezometry_singularity.qml" line="1"/>
        <source>Trihalomethane concentration (mg/l)</source>
        <translation>Concentration trihalométhane (mg/l)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/imposed_piezometry_singularity.qml" line="1"/>
        <source>Passive tracer concentration (mg/l)</source>
        <translation>Concentration traceur passsif (mg/l)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/imposed_piezometry_singularity.qml" line="1"/>
        <source>Comment</source>
        <translation>Commentaire</translation>
    </message>
    <message>
        <location filename="../ressources/qml/imposed_piezometry_singularity.qml" line="1"/>
        <source>Model</source>
        <translation>Modèle</translation>
    </message>
    <message>
        <location filename="../ressources/qml/imposed_piezometry_singularity.qml" line="1"/>
        <source>Nodes</source>
        <translation>Noeuds</translation>
    </message>
    <message>
        <location filename="../ressources/qml/imposed_piezometry_singularity.qml" line="1"/>
        <source>Concentrations</source>
        <translation/>
    </message>
    <message>
        <location filename="../ressources/qml/imposed_piezometry_singularity.qml" line="1"/>
        <source>Chlorine (mg/l)</source>
        <translation>Chlore (mg/l)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/imposed_piezometry_singularity.qml" line="1"/>
        <source>Trihalomethane (mg/l)</source>
        <translation>Trihalométhane (mg/l)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/imposed_piezometry_singularity.qml" line="1"/>
        <source>Passive tracer (mg/l)</source>
        <translation>Traceur passif (mg/l)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/imposed_piezometry_singularity.qml" line="1"/>
        <source>All nodes</source>
        <translation>Tous les noeuds</translation>
    </message>
    <message>
        <location filename="../ressources/qml/imposed_piezometry_singularity.qml" line="1"/>
        <source>External file</source>
        <translation>Fichier externe</translation>
    </message>
    <message>
        <location filename="../ressources/qml/imposed_piezometry_singularity.qml" line="1"/>
        <source>Cyclic</source>
        <translation>Cyclique</translation>
    </message>
    <message>
        <location filename="../ressources/qml/imposed_piezometry_singularity.qml" line="1"/>
        <source>Travel time (h)</source>
        <translation>Temps de séjour (h)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/imposed_piezometry_singularity.qml" line="1"/>
        <source>Contact time (h)</source>
        <translation>Temps de contact (h)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/imposed_piezometry_singularity.qml" line="1"/>
        <source>Times</source>
        <translation>Temps</translation>
    </message>
</context>
<context>
    <name>pipe_link.qml</name>
    <message>
        <location filename="../ressources/qml/pipe_link.qml" line="1"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pipe_link.qml" line="1"/>
        <source>Pipe diameter (m)</source>
        <translation>Diamètre canalisation (m)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pipe_link.qml" line="1"/>
        <source>Pipe thickness (mm)</source>
        <translation>Epaisseur paroi (mm)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pipe_link.qml" line="1"/>
        <source>Material</source>
        <translation>Matériau</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pipe_link.qml" line="1"/>
        <source>Open ?</source>
        <translation>Ouvert ?</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pipe_link.qml" line="1"/>
        <source>Meter on pipe ?</source>
        <translation>Compteur sur la canalisation ?</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pipe_link.qml" line="1"/>
        <source>'Force custom length ? (Geometric length is '||round($length, 2)||'m)'</source>
        <translation>'Forcer la longeur ? (La longueur géométrique est '||round($length, 2)||'m)'</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pipe_link.qml" line="1"/>
        <source>Length (m)</source>
        <translation>Longueur (m)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pipe_link.qml" line="1"/>
        <source>Force custom celerity ?</source>
        <translation>Forcer la célérité ?</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pipe_link.qml" line="1"/>
        <source>Celerity (m/s)</source>
        <translation>Célérité (m/s)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pipe_link.qml" line="1"/>
        <source>Force custom roughness ?</source>
        <translation>Forcer la rugosité ?</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pipe_link.qml" line="1"/>
        <source>Roughness (mm)</source>
        <translation>Rugosité (mm)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pipe_link.qml" line="1"/>
        <source>Comment</source>
        <translation>Commentaire</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pipe_link.qml" line="1"/>
        <source>Node down</source>
        <translation>Noeud aval</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pipe_link.qml" line="1"/>
        <source>Node up</source>
        <translation>Noeud amont</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pipe_link.qml" line="1"/>
        <source>Model</source>
        <translation>Modèle</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pipe_link.qml" line="1"/>
        <source>Elasticity (N/m2)</source>
        <translation>Elasticité (N/m²)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pipe_link.qml" line="1"/>
        <source>Nodes</source>
        <translation>Noeuds</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pipe_link.qml" line="1"/>
        <source>Material settings</source>
        <translation>Définition du matériau</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pipe_link.qml" line="1"/>
        <source>Custom roughness</source>
        <translation>Rugosité forcée</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pipe_link.qml" line="1"/>
        <source>Material roughness</source>
        <translation>Rugosité matériau</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pipe_link.qml" line="1"/>
        <source>Custom celerity</source>
        <translation>Célérité forcée</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pipe_link.qml" line="1"/>
        <source>Calculated celerity</source>
        <translation>Célérité calculée</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pipe_link.qml" line="1"/>
        <source>Length</source>
        <translation>Longueur</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pipe_link.qml" line="1"/>
        <source>Roughness</source>
        <translation>Rugosité</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pipe_link.qml" line="1"/>
        <source>Celerity</source>
        <translation>Célérité</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pipe_link.qml" line="1"/>
        <source>All nodes</source>
        <translation>Tous les noeuds</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pipe_link.qml" line="1"/>
        <source>Custom Celerity (m/s)</source>
        <translation>Célérité personalisée (m/s)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pipe_link.qml" line="1"/>
        <source>Custom roughness (mm)</source>
        <translation>Rugosité personalisée (mm)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pipe_link.qml" line="1"/>
        <source>Compute contact time ?</source>
        <translation>Calcul du temps de contact ?</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pipe_link.qml" line="1"/>
        <source>Material roughness (mm)</source>
        <translation>Rugosité du matériau (mm)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pipe_link.qml" line="1"/>
        <source>Calculated Celerity (m/s)</source>
        <translation>Célérité calculée (m/s)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pipe_link.qml" line="1"/>
        <source>Parameters</source>
        <translation>Paramètres</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pipe_link.qml" line="1"/>
        <source>Other parameters</source>
        <translation>Autres paramètres</translation>
    </message>
</context>
<context>
    <name>pump_link.qml</name>
    <message>
        <location filename="../ressources/qml/pump_link.qml" line="1"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pump_link.qml" line="1"/>
        <source>Pump curve</source>
        <translation>Courbe de pompe</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pump_link.qml" line="1"/>
        <source>Check valve ?</source>
        <translation>Clapet ?</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pump_link.qml" line="1"/>
        <source>Rotation speed (rpm)</source>
        <translation>Vitesse de rotation (tour/min)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pump_link.qml" line="1"/>
        <source>Inertia (kg.m²)</source>
        <translation>Inertie (kg.m²)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pump_link.qml" line="1"/>
        <source>Pump regulation option</source>
        <translation>Option de régulation</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pump_link.qml" line="1"/>
        <source>Rotation speed factor (0-&gt; 1)</source>
        <translation>Facteur de vitesse de rotation (0-&gt;1)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pump_link.qml" line="1"/>
        <source>Controlledl flow rate (m3/h)</source>
        <translation>Débit controlé (m3/h)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pump_link.qml" line="1"/>
        <source>Target node</source>
        <translation>Noeud cible</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pump_link.qml" line="1"/>
        <source>Pump Z start (m)</source>
        <translation>Z démarrage (mNGF)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pump_link.qml" line="1"/>
        <source>Pump Z stop (m)</source>
        <translation>Z arrêt (mNGF)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pump_link.qml" line="1"/>
        <source>Controlled pressure (mCE)</source>
        <translation>Pression controlée en aval de la pompe (mCE)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pump_link.qml" line="1"/>
        <source>Node down</source>
        <translation>Noeud aval</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pump_link.qml" line="1"/>
        <source>Node up</source>
        <translation>Noeud amont</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pump_link.qml" line="1"/>
        <source>Model</source>
        <translation>Modèle</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pump_link.qml" line="1"/>
        <source>Nodes</source>
        <translation>Noeuds</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pump_link.qml" line="1"/>
        <source>No regulation</source>
        <translation>Pas de régulation</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pump_link.qml" line="1"/>
        <source>Flow regulation</source>
        <translation>Régulation de débit</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pump_link.qml" line="1"/>
        <source>Stop-start regulation</source>
        <translation>Régulation arret/marche</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pump_link.qml" line="1"/>
        <source>Pressure regulation</source>
        <translation>Régulation de pression</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pump_link.qml" line="1"/>
        <source>All nodes</source>
        <translation>Tous les noeuds</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pump_link.qml" line="1"/>
        <source>External file</source>
        <translation>Fichier externe</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pump_link.qml" line="1"/>
        <source>Cyclic</source>
        <translation>Cyclique</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pump_link.qml" line="1"/>
        <source>Water level (mNGF)</source>
        <translation>Cote piezométrique (mNGF)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pump_link.qml" line="1"/>
        <source>Proportional (m3/s)</source>
        <translation>Proportionnel (m3/s)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pump_link.qml" line="1"/>
        <source>Integral (m3/s/s)</source>
        <translation>Intégral (m3/s/s)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pump_link.qml" line="1"/>
        <source>Derivative (m3)</source>
        <translation>Dérivé (m3)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pump_link.qml" line="1"/>
        <source>Water level in reservoir</source>
        <translation>Marnage cible dans le réservoir</translation>
    </message>
</context>
<context>
    <name>check_valve_link.qml</name>
    <message>
        <location filename="../ressources/qml/check_valve_link.qml" line="1"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../ressources/qml/check_valve_link.qml" line="1"/>
        <source>Full section (m²)</source>
        <translation>Section complète (m²)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/check_valve_link.qml" line="1"/>
        <source>Headloss coefficient</source>
        <translation>Coef. perte de charge</translation>
    </message>
    <message>
        <location filename="../ressources/qml/check_valve_link.qml" line="1"/>
        <source>Opening pressure (mCE)</source>
        <translation>Pression d'ouverture (mCE)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/check_valve_link.qml" line="1"/>
        <source>Closing mode</source>
        <translation>Mode de fermeture</translation>
    </message>
    <message>
        <location filename="../ressources/qml/check_valve_link.qml" line="1"/>
        <source>Cd coefficient</source>
        <translation>Coef. Cd</translation>
    </message>
    <message>
        <location filename="../ressources/qml/check_valve_link.qml" line="1"/>
        <source>Comment</source>
        <translation>Commentaire</translation>
    </message>
    <message>
        <location filename="../ressources/qml/check_valve_link.qml" line="1"/>
        <source>Node down</source>
        <translation>Noeud aval</translation>
    </message>
    <message>
        <location filename="../ressources/qml/check_valve_link.qml" line="1"/>
        <source>Node up</source>
        <translation>Noeud amont</translation>
    </message>
    <message>
        <location filename="../ressources/qml/check_valve_link.qml" line="1"/>
        <source>Model</source>
        <translation>Modèle</translation>
    </message>
    <message>
        <location filename="../ressources/qml/check_valve_link.qml" line="1"/>
        <source>Nodes</source>
        <translation>Noeuds</translation>
    </message>
    <message>
        <location filename="../ressources/qml/check_valve_link.qml" line="1"/>
        <source>All nodes</source>
        <translation>Tous les noeuds</translation>
    </message>
</context>
<context>
    <name>flow_regulator_link.qml</name>
    <message>
        <location filename="../ressources/qml/flow_regulator_link.qml" line="1"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../ressources/qml/flow_regulator_link.qml" line="1"/>
        <source>Full section (m²)</source>
        <translation>Section complète (m²)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/flow_regulator_link.qml" line="1"/>
        <source>Headloss coefficient</source>
        <translation>Coef perte de charge</translation>
    </message>
    <message>
        <location filename="../ressources/qml/flow_regulator_link.qml" line="1"/>
        <source>Controled flow rate (m3/h)</source>
        <translation>Débit controlé (m3/h)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/flow_regulator_link.qml" line="1"/>
        <source>Comment</source>
        <translation>Commentaire</translation>
    </message>
    <message>
        <location filename="../ressources/qml/flow_regulator_link.qml" line="1"/>
        <source>Node down</source>
        <translation>Noeud aval</translation>
    </message>
    <message>
        <location filename="../ressources/qml/flow_regulator_link.qml" line="1"/>
        <source>Node up</source>
        <translation>Noeud amont</translation>
    </message>
    <message>
        <location filename="../ressources/qml/flow_regulator_link.qml" line="1"/>
        <source>Model</source>
        <translation>Modèle</translation>
    </message>
    <message>
        <location filename="../ressources/qml/flow_regulator_link.qml" line="1"/>
        <source>Nodes</source>
        <translation>Noeuds</translation>
    </message>
    <message>
        <location filename="../ressources/qml/flow_regulator_link.qml" line="1"/>
        <source>All nodes</source>
        <translation>Tous les noeuds</translation>
    </message>
</context>
<context>
    <name>valve_link.qml</name>
    <message>
        <location filename="../ressources/qml/valve_link.qml" line="1"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../ressources/qml/valve_link.qml" line="1"/>
        <source>Full section (m²)</source>
        <translation>Section complète (m²)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/valve_link.qml" line="1"/>
        <source>Headloss coefficient</source>
        <translation>Coef. de perte de charge</translation>
    </message>
    <message>
        <location filename="../ressources/qml/valve_link.qml" line="1"/>
        <source>Section opening coef. (0 -&gt; 1)</source>
        <translation>Coef. d'ouverture (0-&gt;1)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/valve_link.qml" line="1"/>
        <source>Comment</source>
        <translation>Commentaire</translation>
    </message>
    <message>
        <location filename="../ressources/qml/valve_link.qml" line="1"/>
        <source>Node down</source>
        <translation>Noeud aval</translation>
    </message>
    <message>
        <location filename="../ressources/qml/valve_link.qml" line="1"/>
        <source>Node up</source>
        <translation>Noeud amont</translation>
    </message>
    <message>
        <location filename="../ressources/qml/valve_link.qml" line="1"/>
        <source>Model</source>
        <translation>Modèle</translation>
    </message>
    <message>
        <location filename="../ressources/qml/valve_link.qml" line="1"/>
        <source>Nodes</source>
        <translation>Noeuds</translation>
    </message>
    <message>
        <location filename="../ressources/qml/valve_link.qml" line="1"/>
        <source>All nodes</source>
        <translation>Tous les noeuds</translation>
    </message>
</context>
<context>
    <name>pressure_accumulator_singularity.qml</name>
    <message>
        <location filename="../ressources/qml/pressure_accumulator_singularity.qml" line="1"/>
        <source>Node</source>
        <translation>Noeud</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pressure_accumulator_singularity.qml" line="1"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pressure_accumulator_singularity.qml" line="1"/>
        <source>Volume (m3)</source>
        <translation/>
    </message>
    <message>
        <location filename="../ressources/qml/pressure_accumulator_singularity.qml" line="1"/>
        <source>Z connection (mNGF)</source>
        <translation>Z connexion (mNGF)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pressure_accumulator_singularity.qml" line="1"/>
        <source>Filling pressure (mCE)</source>
        <translation>Pression de prégonflage (mCE)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pressure_accumulator_singularity.qml" line="1"/>
        <source>Comment</source>
        <translation>Commentaire</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pressure_accumulator_singularity.qml" line="1"/>
        <source>Model</source>
        <translation>Modèle</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pressure_accumulator_singularity.qml" line="1"/>
        <source>Nodes</source>
        <translation>Noeuds</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pressure_accumulator_singularity.qml" line="1"/>
        <source>All nodes</source>
        <translation>Tous les noeuds</translation>
    </message>
</context>
<context>
    <name>chlorine_injection_singularity.qml</name>
    <message>
        <location filename="../ressources/qml/chlorine_injection_singularity.qml" line="1"/>
        <source>Node</source>
        <translation>Noeud</translation>
    </message>
    <message>
        <location filename="../ressources/qml/chlorine_injection_singularity.qml" line="1"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../ressources/qml/chlorine_injection_singularity.qml" line="1"/>
        <source>Target concentration (mg/l)</source>
        <translation>Concentration cible (mg/l)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/chlorine_injection_singularity.qml" line="1"/>
        <source>Target node</source>
        <translation>Noeud cible</translation>
    </message>
    <message>
        <location filename="../ressources/qml/chlorine_injection_singularity.qml" line="1"/>
        <source>Comment</source>
        <translation>Commentaire</translation>
    </message>
    <message>
        <location filename="../ressources/qml/chlorine_injection_singularity.qml" line="1"/>
        <source>Model</source>
        <translation>Modèle</translation>
    </message>
    <message>
        <location filename="../ressources/qml/chlorine_injection_singularity.qml" line="1"/>
        <source>Nodes</source>
        <translation>Noeuds</translation>
    </message>
    <message>
        <location filename="../ressources/qml/chlorine_injection_singularity.qml" line="1"/>
        <source>Control</source>
        <translation>Controle</translation>
    </message>
    <message>
        <location filename="../ressources/qml/chlorine_injection_singularity.qml" line="1"/>
        <source>Concentrations</source>
        <translation>Concentrations</translation>
    </message>
    <message>
        <location filename="../ressources/qml/chlorine_injection_singularity.qml" line="1"/>
        <source>All nodes</source>
        <translation>Tous les noeuds</translation>
    </message>
</context>
<context>
    <name>water_delivery_sector.qml</name>
    <message>
        <location filename="../ressources/qml/water_delivery_sector.qml" line="1"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../ressources/qml/water_delivery_sector.qml" line="1"/>
        <source>Comment</source>
        <translation>Commentaire</translation>
    </message>
    <message>
        <location filename="../ressources/qml/water_delivery_sector.qml" line="1"/>
        <source>Leak efficiency</source>
        <translation>Rendement</translation>
    </message>
</context>
<context>
    <name>water_delivery_point.qml</name>
    <message>
        <location filename="../ressources/qml/water_delivery_point.qml" line="1"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../ressources/qml/water_delivery_point.qml" line="1"/>
        <source>Comment</source>
        <translation>Commentaire</translation>
    </message>
    <message>
        <location filename="../ressources/qml/water_delivery_point.qml" line="1"/>
        <source>Volume (m3/day)</source>
        <translation>Volume (m3/jour)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/water_delivery_point.qml" line="1"/>
        <source>Industrial ?</source>
        <translation>Industriel ?</translation>
    </message>
    <message>
        <location filename="../ressources/qml/water_delivery_point.qml" line="1"/>
        <source>Source pipe link</source>
        <translation>Canalisation source</translation>
    </message>
    <message>
        <location filename="../ressources/qml/water_delivery_point.qml" line="1"/>
        <source>Pipe</source>
        <translation>Canalisation</translation>
    </message>
    <message>
        <location filename="../ressources/qml/water_delivery_point.qml" line="1"/>
        <source>Source pipe</source>
        <translation>Canalisation source</translation>
    </message>
</context>
<context>
    <name>headloss_link.qml</name>
    <message>
        <location filename="../ressources/qml/headloss_link.qml" line="1"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../ressources/qml/headloss_link.qml" line="1"/>
        <source>Full section (m²)</source>
        <translation>Section complète (m²)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/headloss_link.qml" line="1"/>
        <source>Headloss coef. 1</source>
        <translation>Coef. perte de charge 1</translation>
    </message>
    <message>
        <location filename="../ressources/qml/headloss_link.qml" line="1"/>
        <source>Headloss coef. 2</source>
        <translation>Coef. perte de charge 2</translation>
    </message>
    <message>
        <location filename="../ressources/qml/headloss_link.qml" line="1"/>
        <source>Comment</source>
        <translation>Commentaire</translation>
    </message>
    <message>
        <location filename="../ressources/qml/headloss_link.qml" line="1"/>
        <source>Node down</source>
        <translation>Noeud aval</translation>
    </message>
    <message>
        <location filename="../ressources/qml/headloss_link.qml" line="1"/>
        <source>Node up</source>
        <translation>Noeud amont</translation>
    </message>
    <message>
        <location filename="../ressources/qml/headloss_link.qml" line="1"/>
        <source>Model</source>
        <translation>Modèle</translation>
    </message>
    <message>
        <location filename="../ressources/qml/headloss_link.qml" line="1"/>
        <source>Nodes</source>
        <translation>Noeuds</translation>
    </message>
    <message>
        <location filename="../ressources/qml/headloss_link.qml" line="1"/>
        <source>Stricklers</source>
        <translation/>
    </message>
    <message>
        <location filename="../ressources/qml/headloss_link.qml" line="1"/>
        <source>All nodes</source>
        <translation>Tous les noeuds</translation>
    </message>
</context>
<context>
    <name>model_connection_singularity.qml</name>
    <message>
        <location filename="../ressources/qml/model_connection_singularity.qml" line="1"/>
        <source>Node</source>
        <translation>Noeud</translation>
    </message>
    <message>
        <location filename="../ressources/qml/model_connection_singularity.qml" line="1"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../ressources/qml/model_connection_singularity.qml" line="1"/>
        <source>Cascade mode</source>
        <translation>Mode cascade</translation>
    </message>
    <message>
        <location filename="../ressources/qml/model_connection_singularity.qml" line="1"/>
        <source>Curve Z(t)</source>
        <translation>Courbe Z(t)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/model_connection_singularity.qml" line="1"/>
        <source>Curve Q(t)</source>
        <translation>Courbe Q(t)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/model_connection_singularity.qml" line="1"/>
        <source>Comment</source>
        <translation>Commentaire</translation>
    </message>
    <message>
        <location filename="../ressources/qml/model_connection_singularity.qml" line="1"/>
        <source>Model</source>
        <translation>Modèle</translation>
    </message>
    <message>
        <location filename="../ressources/qml/model_connection_singularity.qml" line="1"/>
        <source>Nodes</source>
        <translation>Noeuds</translation>
    </message>
    <message>
        <location filename="../ressources/qml/model_connection_singularity.qml" line="1"/>
        <source>All nodes</source>
        <translation>Tous les noeuds</translation>
    </message>
    <message>
        <location filename="../ressources/qml/model_connection_singularity.qml" line="1"/>
        <source>External file</source>
        <translation>Fichier externe</translation>
    </message>
    <message>
        <location filename="../ressources/qml/model_connection_singularity.qml" line="1"/>
        <source>Chlorine (mg/l)</source>
        <translation>Chlore (mg/l)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/model_connection_singularity.qml" line="1"/>
        <source>Trihalomethane (mg/l)</source>
        <translation>Trihalométhane (mg/l)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/model_connection_singularity.qml" line="1"/>
        <source>Passive tracer (mg/l)</source>
        <translation>Traceur passif (mg/l)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/model_connection_singularity.qml" line="1"/>
        <source>Concentrations</source>
        <translation>Concentrations</translation>
    </message>
    <message>
        <location filename="../ressources/qml/model_connection_singularity.qml" line="1"/>
        <source>Cyclic</source>
        <translation>Cyclique</translation>
    </message>
    <message>
        <location filename="../ressources/qml/model_connection_singularity.qml" line="1"/>
        <source>Travel time (h)</source>
        <translation>Temps de séjour (h)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/model_connection_singularity.qml" line="1"/>
        <source>Contact time (h)</source>
        <translation>Temps de contact (h)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/model_connection_singularity.qml" line="1"/>
        <source>Times</source>
        <translation>Temps</translation>
    </message>
</context>
<context>
    <name>air_relief_valve_singularity.qml</name>
    <message>
        <location filename="../ressources/qml/air_relief_valve_singularity.qml" line="1"/>
        <source>Node</source>
        <translation>Noeud</translation>
    </message>
    <message>
        <location filename="../ressources/qml/air_relief_valve_singularity.qml" line="1"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../ressources/qml/air_relief_valve_singularity.qml" line="1"/>
        <source>Orifice diameter (m)</source>
        <translation>Diamètre orifice (m)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/air_relief_valve_singularity.qml" line="1"/>
        <source>Contraction coef</source>
        <translation>Coef. de contraction</translation>
    </message>
    <message>
        <location filename="../ressources/qml/air_relief_valve_singularity.qml" line="1"/>
        <source>Comment</source>
        <translation>Commentaire</translation>
    </message>
    <message>
        <location filename="../ressources/qml/air_relief_valve_singularity.qml" line="1"/>
        <source>Model</source>
        <translation>Modèle</translation>
    </message>
    <message>
        <location filename="../ressources/qml/air_relief_valve_singularity.qml" line="1"/>
        <source>Nodes</source>
        <translation>Noeuds</translation>
    </message>
    <message>
        <location filename="../ressources/qml/air_relief_valve_singularity.qml" line="1"/>
        <source>All nodes</source>
        <translation>Tous les noeuds</translation>
    </message>
</context>
<context>
    <name>thresold_warning_settings.qml</name>
    <message>
        <location filename="../ressources/qml/thresold_warning_settings.qml" line="1"/>
        <source>Pipe maximum speed (m/s)</source>
        <translation>Vitesse max canalisation (m/s)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/thresold_warning_settings.qml" line="1"/>
        <source>Pipe minimum speed (m/s)</source>
        <translation>Vitesse min canalisation (m/s)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/thresold_warning_settings.qml" line="1"/>
        <source>Node maximum pressure (mCE)</source>
        <translation>Pression max au noeud (mCE)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/thresold_warning_settings.qml" line="1"/>
        <source>Node minimum pressure (mCE)</source>
        <translation>Pression min au noeud (mCE)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/thresold_warning_settings.qml" line="1"/>
        <source>Minimum service pressure (mCE)</source>
        <translation>Pession de service min (mCE)</translation>
    </message>
</context>
<context>
    <name>pipe_material.qml</name>
    <message>
        <location filename="../ressources/qml/pipe_material.qml" line="1"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pipe_material.qml" line="1"/>
        <source>Roughness (mm)</source>
        <translation>Rugosité (mm)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pipe_material.qml" line="1"/>
        <source>Elasticity (N/m²)</source>
        <translation>Elasticité (N/m²)</translation>
    </message>
</context>
<context>
    <name>failure_curve.qml</name>
    <message>
        <location filename="../ressources/qml/failure_curve.qml" line="1"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../ressources/qml/failure_curve.qml" line="1"/>
        <source>Comment</source>
        <translation>Commentaire</translation>
    </message>
    <message>
        <location filename="../ressources/qml/failure_curve.qml" line="1"/>
        <source>Alpha(t) curve</source>
        <translation>Courbe Alpha(t)</translation>
    </message>
</context>
<context>
    <name>fluid_properties.qml</name>
    <message>
        <location filename="../ressources/qml/fluid_properties.qml" line="1"/>
        <source>Volumic mass (kg/m3)</source>
        <translation>Masse volumique (kg/m3)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/fluid_properties.qml" line="1"/>
        <source>Temperature (°C)</source>
        <translation>Température (°C)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/fluid_properties.qml" line="1"/>
        <source>Kinematic viscosity (m²/s)</source>
        <translation>Viscosité cynématique (m²/s)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/fluid_properties.qml" line="1"/>
        <source>Volumic elasticity module (N/m²)</source>
        <translation>Compressibilité (N/m²)</translation>
    </message>
</context>
<context>
    <name>hourly_modulation_curve.qml</name>
    <message>
        <location filename="../ressources/qml/hourly_modulation_curve.qml" line="1"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../ressources/qml/hourly_modulation_curve.qml" line="1"/>
        <source>Comment</source>
        <translation>Commentaire</translation>
    </message>
    <message>
        <location filename="../ressources/qml/hourly_modulation_curve.qml" line="1"/>
        <source>Modulation curves</source>
        <translation>Courbes de modulation</translation>
    </message>
</context>
<context>
    <name>threshold_warning_settings.qml</name>
    <message>
        <location filename="../ressources/qml/threshold_warning_settings.qml" line="1"/>
        <source>Pipe maximum speed (m/s)</source>
        <translation>Vitesse max canalisation (m/s)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/threshold_warning_settings.qml" line="1"/>
        <source>Pipe minimum speed (m/s)</source>
        <translation>Vitesse min canalisation (m/s)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/threshold_warning_settings.qml" line="1"/>
        <source>Node maximum pressure (mCE)</source>
        <translation>Pression max au noeud (mCE)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/threshold_warning_settings.qml" line="1"/>
        <source>Node minimum pressure (mCE)</source>
        <translation>Pression min au noeud (mCE)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/threshold_warning_settings.qml" line="1"/>
        <source>Minimum service pressure (mCE)</source>
        <translation>Pession de service min (mCE)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/threshold_warning_settings.qml" line="1"/>
        <source>Pipes</source>
        <translation>Canalisations</translation>
    </message>
    <message>
        <location filename="../ressources/qml/threshold_warning_settings.qml" line="1"/>
        <source>Nodes</source>
        <translation>Noeuds</translation>
    </message>
    <message>
        <location filename="../ressources/qml/threshold_warning_settings.qml" line="1"/>
        <source>Delivery point</source>
        <translation>Points de consommation</translation>
    </message>
</context>
<context>
    <name>material.qml</name>
    <message>
        <location filename="../ressources/qml/material.qml" line="1"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../ressources/qml/material.qml" line="1"/>
        <source>Roughness (mm)</source>
        <translation>Rugosité (mm)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/material.qml" line="1"/>
        <source>Elasticity (N/m²)</source>
        <translation>Elasticité (N/m²)</translation>
    </message>
</context>
<context>
    <name>imposed_piezzometry.qml</name>
    <message>
        <location filename="../ressources/qml/imposed_piezzometry.qml" line="1"/>
        <source>Node</source>
        <translation>Noeud</translation>
    </message>
    <message>
        <location filename="../ressources/qml/imposed_piezzometry.qml" line="1"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../ressources/qml/imposed_piezzometry.qml" line="1"/>
        <source>Imposed Z (mNGF)</source>
        <translation>Z imposé (mNGF)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/imposed_piezzometry.qml" line="1"/>
        <source>Chlorine (mg/l)</source>
        <translation>Chlore (mg/l)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/imposed_piezzometry.qml" line="1"/>
        <source>Trihalomethane (mg/l)</source>
        <translation>Trihalométhane (mg/l)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/imposed_piezzometry.qml" line="1"/>
        <source>Passive tracer (mg/l)</source>
        <translation>Traceur passif (mg/l)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/imposed_piezzometry.qml" line="1"/>
        <source>External file</source>
        <translation>Fichier externe</translation>
    </message>
    <message>
        <location filename="../ressources/qml/imposed_piezzometry.qml" line="1"/>
        <source>Comment</source>
        <translation>Commentaire</translation>
    </message>
    <message>
        <location filename="../ressources/qml/imposed_piezzometry.qml" line="1"/>
        <source>Model</source>
        <translation>Modèle</translation>
    </message>
    <message>
        <location filename="../ressources/qml/imposed_piezzometry.qml" line="1"/>
        <source>All nodes</source>
        <translation>Tous les noeuds</translation>
    </message>
    <message>
        <location filename="../ressources/qml/imposed_piezzometry.qml" line="1"/>
        <source>Concentrations</source>
        <translation>Concentrations</translation>
    </message>
</context>
<context>
    <name>model_connection.qml</name>
    <message>
        <location filename="../ressources/qml/model_connection.qml" line="1"/>
        <source>Node</source>
        <translation>Noeud</translation>
    </message>
    <message>
        <location filename="../ressources/qml/model_connection.qml" line="1"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../ressources/qml/model_connection.qml" line="1"/>
        <source>Cascade mode</source>
        <translation>Mode cascade</translation>
    </message>
    <message>
        <location filename="../ressources/qml/model_connection.qml" line="1"/>
        <source>Curve Z(t)</source>
        <translation>Courbe Z(t)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/model_connection.qml" line="1"/>
        <source>Curve Q(t)</source>
        <translation>Courbe Q(t)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/model_connection.qml" line="1"/>
        <source>External file</source>
        <translation>Fichier externe</translation>
    </message>
    <message>
        <location filename="../ressources/qml/model_connection.qml" line="1"/>
        <source>Chlorine (mg/l)</source>
        <translation>Chlore (mg/l)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/model_connection.qml" line="1"/>
        <source>Trihalomethane (mg/l)</source>
        <translation>Trihalométhane (mg/l)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/model_connection.qml" line="1"/>
        <source>Passive tracer (mg/l)</source>
        <translation>Traceur passif (mg/l)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/model_connection.qml" line="1"/>
        <source>Comment</source>
        <translation>Commentaire</translation>
    </message>
    <message>
        <location filename="../ressources/qml/model_connection.qml" line="1"/>
        <source>Model</source>
        <translation>Modèle</translation>
    </message>
    <message>
        <location filename="../ressources/qml/model_connection.qml" line="1"/>
        <source>All nodes</source>
        <translation>Tous les noeuds</translation>
    </message>
    <message>
        <location filename="../ressources/qml/model_connection.qml" line="1"/>
        <source>Concentrations</source>
        <translation>Concentrations</translation>
    </message>
</context>
<context>
    <name>flow_injection.qml</name>
    <message>
        <location filename="../ressources/qml/flow_injection.qml" line="1"/>
        <source>Node</source>
        <translation>Noeud</translation>
    </message>
    <message>
        <location filename="../ressources/qml/flow_injection.qml" line="1"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../ressources/qml/flow_injection.qml" line="1"/>
        <source>Q injection (m3/hr)</source>
        <translation>Débit injecté (m3/h)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/flow_injection.qml" line="1"/>
        <source>External file</source>
        <translation>Fichier externe</translation>
    </message>
    <message>
        <location filename="../ressources/qml/flow_injection.qml" line="1"/>
        <source>Chlorine (mg/l)</source>
        <translation>Chlore (mg/l)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/flow_injection.qml" line="1"/>
        <source>Trihalomethane (mg/l)</source>
        <translation>Trihalométhane (mg/l)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/flow_injection.qml" line="1"/>
        <source>Passive tracer (mg/l)</source>
        <translation>Traceur passif (mg/l)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/flow_injection.qml" line="1"/>
        <source>Comment</source>
        <translation>Commentaire</translation>
    </message>
    <message>
        <location filename="../ressources/qml/flow_injection.qml" line="1"/>
        <source>Model</source>
        <translation>Modèle</translation>
    </message>
    <message>
        <location filename="../ressources/qml/flow_injection.qml" line="1"/>
        <source>All nodes</source>
        <translation>Tous les noeuds</translation>
    </message>
    <message>
        <location filename="../ressources/qml/flow_injection.qml" line="1"/>
        <source>Concentrations</source>
        <translation>Concentrations</translation>
    </message>
</context>
<context>
    <name>sector.qml</name>
    <message>
        <location filename="../ressources/qml/sector.qml" line="1"/>
        <source>Pipe diameter (m)</source>
        <translation>Diamètre orifice (m)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/sector.qml" line="1"/>
        <source>Nodes</source>
        <translation>Noeuds</translation>
    </message>
    <message>
        <location filename="../ressources/qml/sector.qml" line="1"/>
        <source>Length</source>
        <translation>Longueur</translation>
    </message>
    <message>
        <location filename="../ressources/qml/sector.qml" line="1"/>
        <source>Material settings</source>
        <translation>Définition du matériau</translation>
    </message>
    <message>
        <location filename="../ressources/qml/sector.qml" line="1"/>
        <source>Roughness</source>
        <translation>Rugosité</translation>
    </message>
    <message>
        <location filename="../ressources/qml/sector.qml" line="1"/>
        <source>Material roughness</source>
        <translation>Rugosité matériau</translation>
    </message>
    <message>
        <location filename="../ressources/qml/sector.qml" line="1"/>
        <source>Custom roughness</source>
        <translation>Rugosité forcée</translation>
    </message>
    <message>
        <location filename="../ressources/qml/sector.qml" line="1"/>
        <source>Celerity</source>
        <translation>Célérité</translation>
    </message>
    <message>
        <location filename="../ressources/qml/sector.qml" line="1"/>
        <source>Calculated celerity</source>
        <translation>Célérité calculée</translation>
    </message>
    <message>
        <location filename="../ressources/qml/sector.qml" line="1"/>
        <source>Custom celerity</source>
        <translation>Célérité forcée</translation>
    </message>
</context>
<context>
    <name>pipe_link_singularity.qml</name>
    <message>
        <location filename="../ressources/qml/pipe_link_singularity.qml" line="1"/>
        <source>Pipe</source>
        <translation>Canalisation</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pipe_link_singularity.qml" line="1"/>
        <source>Number of singularities</source>
        <translation>Nombre de singularités</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pipe_link_singularity.qml" line="1"/>
        <source>Singularity type</source>
        <translation>Type de singularité</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pipe_link_singularity.qml" line="1"/>
        <source>Section</source>
        <translation>Section (m²)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pipe_link_singularity.qml" line="1"/>
        <source>All nodes</source>
        <translation>Tous les noeuds</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pipe_link_singularity.qml" line="1"/>
        <source>Bend</source>
        <translation>Coude</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pipe_link_singularity.qml" line="1"/>
        <source>Borda</source>
        <translation>Borda</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pipe_link_singularity.qml" line="1"/>
        <source>K</source>
        <translation>K</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pipe_link_singularity.qml" line="1"/>
        <source>Angle (deg)</source>
        <translation>Angle (deg)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pipe_link_singularity.qml" line="1"/>
        <source>Radius (m)</source>
        <translation>Rayon (m)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pipe_link_singularity.qml" line="1"/>
        <source>Section (m²)</source>
        <translation>Section (m²)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/pipe_link_singularity.qml" line="1"/>
        <source>Coefficient</source>
        <translation>Coefficient</translation>
    </message>
</context>
<context>
    <name>fire_hydrant.qml</name>
    <message>
        <location filename="../ressources/qml/fire_hydrant.qml" line="1"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../ressources/qml/fire_hydrant.qml" line="1"/>
        <source>Comment</source>
        <translation>Commentaire</translation>
    </message>
    <message>
        <location filename="../ressources/qml/fire_hydrant.qml" line="1"/>
        <source>Diameter(m)</source>
        <translation>Diamètre (m)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/fire_hydrant.qml" line="1"/>
        <source>Theoretical flow (m3/h)</source>
        <translation>Débit théorique (m3/h)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/fire_hydrant.qml" line="1"/>
        <source>Draw time (h)</source>
        <translation>Temps de tirage (h)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/fire_hydrant.qml" line="1"/>
        <source>Source pipe</source>
        <translation>Canalisation source</translation>
    </message>
    <message>
        <location filename="../ressources/qml/fire_hydrant.qml" line="1"/>
        <source>User node</source>
        <translation>Noeud</translation>
    </message>
    <message>
        <location filename="../ressources/qml/fire_hydrant.qml" line="1"/>
        <source>Pipe</source>
        <translation>Canalisation</translation>
    </message>
</context>
<context>
    <name>wd_loss_broken_pipe.qml</name>
    <message>
        <location filename="../ressources/qml/wd_loss_broken_pipe.qml" line="1"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../ressources/qml/wd_loss_broken_pipe.qml" line="1"/>
        <source>Pipe diameter (m)</source>
        <translation>Diamètre canalisation (m)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/wd_loss_broken_pipe.qml" line="1"/>
        <source>Pipe thickness (mm)</source>
        <translation>Epaisseur paroi (mm)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/wd_loss_broken_pipe.qml" line="1"/>
        <source>Material</source>
        <translation>Matériau</translation>
    </message>
    <message>
        <location filename="../ressources/qml/wd_loss_broken_pipe.qml" line="1"/>
        <source>Open ?</source>
        <translation>Ouvert ?</translation>
    </message>
    <message>
        <location filename="../ressources/qml/wd_loss_broken_pipe.qml" line="1"/>
        <source>Meter on pipe ?</source>
        <translation>Compteur sur la canalisation ?</translation>
    </message>
    <message>
        <location filename="../ressources/qml/wd_loss_broken_pipe.qml" line="1"/>
        <source>'Force custom length ? (Geometric length is '||round($length, 2)||'m)'</source>
        <translation>'Forcer la longeur ? (La longueur géométrique est '||round($length, 2)||'m)'</translation>
    </message>
    <message>
        <location filename="../ressources/qml/wd_loss_broken_pipe.qml" line="1"/>
        <source>Length (m)</source>
        <translation>Longueur (m)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/wd_loss_broken_pipe.qml" line="1"/>
        <source>Force custom celerity ?</source>
        <translation>Forcer la célérité ?</translation>
    </message>
    <message>
        <location filename="../ressources/qml/wd_loss_broken_pipe.qml" line="1"/>
        <source>Celerity (m/s)</source>
        <translation>Célérité (m/s)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/wd_loss_broken_pipe.qml" line="1"/>
        <source>Force custom roughness ?</source>
        <translation>Forcer la rugosité ?</translation>
    </message>
    <message>
        <location filename="../ressources/qml/wd_loss_broken_pipe.qml" line="1"/>
        <source>Roughness (mm)</source>
        <translation>Rugosité (mm)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/wd_loss_broken_pipe.qml" line="1"/>
        <source>Comment</source>
        <translation>Commentaire</translation>
    </message>
    <message>
        <location filename="../ressources/qml/wd_loss_broken_pipe.qml" line="1"/>
        <source>Node down</source>
        <translation>Noeud aval</translation>
    </message>
    <message>
        <location filename="../ressources/qml/wd_loss_broken_pipe.qml" line="1"/>
        <source>Node up</source>
        <translation>Noeud amont</translation>
    </message>
    <message>
        <location filename="../ressources/qml/wd_loss_broken_pipe.qml" line="1"/>
        <source>Model</source>
        <translation>Modèle</translation>
    </message>
    <message>
        <location filename="../ressources/qml/wd_loss_broken_pipe.qml" line="1"/>
        <source>Elasticity (N/m2)</source>
        <translation>Elasticité (N/m²)</translation>
    </message>
    <message>
        <location filename="../ressources/qml/wd_loss_broken_pipe.qml" line="1"/>
        <source>All nodes</source>
        <translation>Tous les noeuds</translation>
    </message>
    <message>
        <location filename="../ressources/qml/wd_loss_broken_pipe.qml" line="1"/>
        <source>Nodes</source>
        <translation>Noeuds</translation>
    </message>
    <message>
        <location filename="../ressources/qml/wd_loss_broken_pipe.qml" line="1"/>
        <source>Length</source>
        <translation>Longueur</translation>
    </message>
    <message>
        <location filename="../ressources/qml/wd_loss_broken_pipe.qml" line="1"/>
        <source>Material settings</source>
        <translation>Définition du matériau</translation>
    </message>
    <message>
        <location filename="../ressources/qml/wd_loss_broken_pipe.qml" line="1"/>
        <source>Roughness</source>
        <translation>Rugosité</translation>
    </message>
    <message>
        <location filename="../ressources/qml/wd_loss_broken_pipe.qml" line="1"/>
        <source>Material roughness</source>
        <translation>Rugosité matériau</translation>
    </message>
    <message>
        <location filename="../ressources/qml/wd_loss_broken_pipe.qml" line="1"/>
        <source>Custom roughness</source>
        <translation>Rugosité forcée</translation>
    </message>
    <message>
        <location filename="../ressources/qml/wd_loss_broken_pipe.qml" line="1"/>
        <source>Celerity</source>
        <translation>Célérité</translation>
    </message>
    <message>
        <location filename="../ressources/qml/wd_loss_broken_pipe.qml" line="1"/>
        <source>Calculated celerity</source>
        <translation>Célérité calculée</translation>
    </message>
    <message>
        <location filename="../ressources/qml/wd_loss_broken_pipe.qml" line="1"/>
        <source>Custom celerity</source>
        <translation>Célérité forcée</translation>
    </message>
</context>
<context>
    <name>sensor.qml</name>
    <message>
        <location filename="../ressources/qml/sensor.qml" line="1"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../ressources/qml/sensor.qml" line="1"/>
        <source>Type</source>
        <translation>Type</translation>
    </message>
    <message>
        <location filename="../ressources/qml/sensor.qml" line="1"/>
        <source>Units</source>
        <translation>Unités</translation>
    </message>
    <message>
        <location filename="../ressources/qml/sensor.qml" line="1"/>
        <source>Comment</source>
        <translation>Commentaire</translation>
    </message>
</context>
<context>
    <name>measure.qml</name>
    <message>
        <location filename="../ressources/qml/measure.qml" line="1"/>
        <source>Timestamp</source>
        <translation>Horodatage</translation>
    </message>
    <message>
        <location filename="../ressources/qml/measure.qml" line="1"/>
        <source>Value</source>
        <translation>Valeur</translation>
    </message>
    <message>
        <location filename="../ressources/qml/measure.qml" line="1"/>
        <source>Sensor</source>
        <translation>Capteur</translation>
    </message>
    <message>
        <location filename="../ressources/qml/measure.qml" line="1"/>
        <source>Creation date</source>
        <translation>Date de création</translation>
    </message>
</context>
<context><name>pressure_min.qml</name></context><context><name>pressure_max.qml</name></context><context><name>p_min_fire_hydrant.qml</name></context><context><name>node_without_piezo.qml</name></context><context><name>link_without_piezo.qml</name></context><context><name>flow_at_1bar_fire_hydrant.qml</name></context><context><name>configured.qml</name></context><context><name>warning.qml</name></context><context><name>q_over_qcons.qml</name></context></TS>
