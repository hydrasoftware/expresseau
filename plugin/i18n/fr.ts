<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR" sourcelanguage="en_US">
<context>
    <name>@default</name>
    <message>
        <location filename="../gui/widgets/graph_widget.py" line="133"/>
        <source>Copy image</source>
        <translation>Copier l'image</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="190"/>
        <source>Nodes</source>
        <translation>Noeuds</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="560"/>
        <source>Material</source>
        <translation>Matériau</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="236"/>
        <source>Hourly modulation curve</source>
        <translation>Courbe de modulation horaire</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="72"/>
        <source>S(Z) Curve</source>
        <translation>Courbe S(Z)</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="73"/>
        <source>Z(t) Curve</source>
        <translation>Courbe Z(t)</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="74"/>
        <source>Q(t) Curve</source>
        <translation>Courbe Q(t)</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="76"/>
        <source>P(Q) Curve</source>
        <translation>Courbe P(Q)</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="78"/>
        <source>Alpha(t) Curve</source>
        <translation>Courbe Alpha(t)</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="79"/>
        <source>Hourly coefs.</source>
        <translation>Coef. horaires</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="79"/>
        <source>Weekday
Hourly coefficients</source>
        <translation>Coef. horaires semaine</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="79"/>
        <source>Weekend
Hourly coefficients</source>
        <translation>Coef. horaires weekend</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="676"/>
        <source>Water delivery point</source>
        <translation>Point de consommation</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="559"/>
        <source>Water delivery sector</source>
        <translation>Secteur de consommation</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="181"/>
        <source>Singularities</source>
        <translation>Singularités</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="181"/>
        <source>Imposed piezometry</source>
        <translation>Piezométrie imposée</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="181"/>
        <source>Surge tank</source>
        <translation>Cheminée d'équilibre</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="181"/>
        <source>Flow injection</source>
        <translation>Injection de débit</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="235"/>
        <source>Chlorine injection</source>
        <translation type="obsolete">Injection de chlore</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="181"/>
        <source>Air relief valve</source>
        <translation>Ventouse</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="181"/>
        <source>Pressure accumulator</source>
        <translation>Ballon anti-bélier</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="181"/>
        <source>Model connection</source>
        <translation>Connecteur inter-modèles</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="236"/>
        <source>User node</source>
        <translation>Noeud</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="391"/>
        <source>Reservoir</source>
        <translation>Réservoir</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="195"/>
        <source>Links</source>
        <translation>Liaisons</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="195"/>
        <source>Check valve</source>
        <translation>Clapet</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="195"/>
        <source>Valve</source>
        <translation>Vanne</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="195"/>
        <source>Headloss</source>
        <translation>Perte de charge</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="195"/>
        <source>Flow regulator</source>
        <translation>Régulateur de débit</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="195"/>
        <source>Pressure regulator</source>
        <translation>Régulateur de pression</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="146"/>
        <source>Pump</source>
        <comment>pump_link</comment>
        <translation type="obsolete">Pompe</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="146"/>
        <source>Pipe</source>
        <comment>pipe_link</comment>
        <translation type="obsolete">Canalisation</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="672"/>
        <source>Settings</source>
        <translation>Paramétrages</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="561"/>
        <source>Fluid properties</source>
        <translation>Propriétés du fluide</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="155"/>
        <source>Failure curve</source>
        <comment>failure_curve</comment>
        <translation type="obsolete">Courbe de défaillance</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="205"/>
        <source>Threshold warning</source>
        <translation>Paramétrage des alertes</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="361"/>
        <source>synthetic results</source>
        <translation>résultats synthétiques</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="376"/>
        <source>Results tables</source>
        <translation>Résultats</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="377"/>
        <source>Hydraulic maps results</source>
        <translation>Cartographie hydraulique</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="382"/>
        <source>Water quality maps results</source>
        <translation>Qualité de l'eau</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="391"/>
        <source>Node</source>
        <translation>Noeud</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="391"/>
        <source>Pipe</source>
        <translation>Canalisation</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="416"/>
        <source>Headloss max (mm/m)</source>
        <translation>Perte de charge max (mm/n)</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="477"/>
        <source>Travel time max (h)</source>
        <translation>Temps de parcours max (h)</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="239"/>
        <source>Pump</source>
        <translation>Pompe</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="205"/>
        <source>Failure curve</source>
        <translation>Courbe de défaillance (transitoire rapide)</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="353"/>
        <source>Chlorine injection node</source>
        <translation type="obsolete">Mode injection chlore</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="354"/>
        <source>Air relief valve node</source>
        <translation type="obsolete">Mode soupape</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="355"/>
        <source>Check valve down</source>
        <translation type="obsolete">Clapet aval</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="356"/>
        <source>Check valve up</source>
        <translation type="obsolete">Clapet amont</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="357"/>
        <source>Flow injection node</source>
        <translation type="obsolete">Noeud d'injection</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="358"/>
        <source>Flow regulator down</source>
        <translation type="obsolete">Régulation débit aval</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="359"/>
        <source>Flow regulator up</source>
        <translation type="obsolete">Régulation débit amont</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="360"/>
        <source>Headloss down</source>
        <translation type="obsolete">Perte de charge aval</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="361"/>
        <source>Headloss up</source>
        <translation type="obsolete">Perte de charge amont</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="362"/>
        <source>Impozed piezometry node</source>
        <translation type="obsolete">Noeud peizometrie imposée</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="363"/>
        <source>model connection node</source>
        <translation type="obsolete">Noeud donnexion inter-modèles</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="364"/>
        <source>Pipe down</source>
        <translation type="obsolete">Canalisation aval</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="238"/>
        <source>Pipe material</source>
        <translation>Matériau canalisation</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="366"/>
        <source>Pipe up</source>
        <translation type="obsolete">Canalisation amont</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="367"/>
        <source>Pressure accumulator node</source>
        <translation type="obsolete">Noeud accumulateur de pression</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="368"/>
        <source>Pressure regulator down</source>
        <translation type="obsolete">Noeud régulation pression aval</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="369"/>
        <source>Pressure regulator up</source>
        <translation type="obsolete">Noeud regulation pression amont</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="370"/>
        <source>Pump down</source>
        <translation type="obsolete">Pompe aval</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="239"/>
        <source>Pump target node</source>
        <translation>Pompe noeud cible</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="372"/>
        <source>Pump up</source>
        <translation type="obsolete">Pompe amont</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="373"/>
        <source>Surge tank node</source>
        <translation type="obsolete">Noeud vase expansion</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="374"/>
        <source>User node domestic modulation curve</source>
        <translation type="obsolete">Noeud courbe de modulation domestique</translation>
    </message>
    <message>
        <location filename="../gui/menu.py" line="58"/>
        <source>&amp;Manage projects</source>
        <translation type="obsolete">&amp;Gérer les projets</translation>
    </message>
    <message>
        <location filename="../gui/menu.py" line="59"/>
        <source>&amp;New project</source>
        <translation type="obsolete">&amp;Nouveau projet</translation>
    </message>
    <message>
        <location filename="../gui/menu.py" line="66"/>
        <source>&amp;Models</source>
        <translation type="obsolete">&amp;Modèles</translation>
    </message>
    <message>
        <location filename="../gui/menu.py" line="67"/>
        <source>&amp;Set current</source>
        <translation type="obsolete">&amp;Définir le modèle courant</translation>
    </message>
    <message>
        <location filename="../gui/menu.py" line="76"/>
        <source>&amp;Add model</source>
        <translation type="obsolete">&amp;Ajouter un modèle</translation>
    </message>
    <message>
        <location filename="../gui/menu.py" line="77"/>
        <source>&amp;Delete current model</source>
        <translation type="obsolete">&amp;Supprimer le modèle courant</translation>
    </message>
    <message>
        <location filename="../gui/menu.py" line="80"/>
        <source>&amp;Water delivery</source>
        <translation type="obsolete">&amp;Distribution</translation>
    </message>
    <message>
        <location filename="../gui/menu.py" line="81"/>
        <source>&amp;Water delivery sector settings</source>
        <translation type="obsolete">Secteurs de distribution</translation>
    </message>
    <message>
        <location filename="../gui/menu.py" line="82"/>
        <source>Water delivery &amp;scenario</source>
        <translation type="obsolete">Scénarios de distribution</translation>
    </message>
    <message>
        <location filename="../gui/menu.py" line="85"/>
        <source>&amp;Scenarios</source>
        <translation type="obsolete">&amp;Scénarios</translation>
    </message>
    <message>
        <location filename="../gui/menu.py" line="86"/>
        <source>&amp;Settings</source>
        <translation type="obsolete">&amp;Paramètres</translation>
    </message>
    <message>
        <location filename="../gui/menu.py" line="92"/>
        <source>Licence activation</source>
        <translation type="obsolete">Activer la licence</translation>
    </message>
    <message>
        <location filename="../gui/menu.py" line="94"/>
        <source>&amp;Help</source>
        <translation type="obsolete">&amp;Aide</translation>
    </message>
    <message>
        <location filename="../gui/menu.py" line="95"/>
        <source>About</source>
        <translation type="obsolete">A propos</translation>
    </message>
    <message>
        <location filename="../gui/project_manager.py" line="272"/>
        <source>Delete model</source>
        <translation type="obsolete">Supprimer le modèle</translation>
    </message>
    <message>
        <location filename="../gui/project_manager.py" line="272"/>
        <source>This will delete model </source>
        <translation type="obsolete">Cela supprimera le modèle </translation>
    </message>
    <message>
        <location filename="../gui/project_manager.py" line="272"/>
        <source>. Proceed?</source>
        <translation type="obsolete">. Continuer ?</translation>
    </message>
    <message>
        <location filename="../gui/menu.py" line="151"/>
        <source>Licence already activated</source>
        <translation type="obsolete">La licence est déjà activée</translation>
    </message>
    <message>
        <location filename="../gui/menu.py" line="151"/>
        <source>A licence has already been activated. Do you want to activate it again?</source>
        <translation type="obsolete">La licence a déjà été activée. Voulez vous l'activer de nouveau ?</translation>
    </message>
    <message>
        <location filename="../gui/menu.py" line="156"/>
        <source>Hydra licence activation</source>
        <translation type="obsolete">Activation de la licence</translation>
    </message>
    <message>
        <location filename="../gui/menu.py" line="156"/>
        <source>Enter your licence number:</source>
        <translation type="obsolete">Saisir le numéro de licence:</translation>
    </message>
    <message>
        <location filename="../gui/menu.py" line="161"/>
        <source>Licence activated</source>
        <translation type="obsolete">Licence activée</translation>
    </message>
    <message>
        <location filename="../gui/menu.py" line="162"/>
        <source>Activation successfull</source>
        <translation type="obsolete">Activation réalisée</translation>
    </message>
    <message>
        <location filename="../gui/menu.py" line="165"/>
        <source>Error</source>
        <translation type="obsolete">Erreur</translation>
    </message>
    <message>
        <location filename="../gui/menu.py" line="165"/>
        <source>There has been an error during your licence activation. Please contact technical support.</source>
        <translation type="obsolete">Il y a eu une erreur ors de l'activation de la licence. Merci de contacter le support technique.</translation>
    </message>
    <message>
        <location filename="../gui/menu.py" line="167"/>
        <source>No licence activated</source>
        <translation type="obsolete">La licence n'est pas activée</translation>
    </message>
    <message>
        <location filename="../gui/menu.py" line="185"/>
        <source>About Expresseau</source>
        <translation type="obsolete">A propos d'Expresseau</translation>
    </message>
    <message>
        <location filename="../gui/toolbar.py" line="50"/>
        <source>Expresseau toolbar</source>
        <translation>Barre d'outils Expresseau</translation>
    </message>
    <message>
        <location filename="../gui/toolbar.py" line="64"/>
        <source>Run computation</source>
        <translation type="obsolete">Lancer le calcul</translation>
    </message>
    <message>
        <location filename="../gui/toolbar.py" line="78"/>
        <source>toolbar created</source>
        <translation type="obsolete">barre d'outil créée</translation>
    </message>
    <message>
        <location filename="../gui/menu.py" line="162"/>
        <source>Your licence has successfully been activated!</source>
        <translation type="obsolete">Votre licence a été activée!</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="676"/>
        <source>Sector</source>
        <translation>Secteur</translation>
    </message>
    <message>
        <location filename="../gui/water_delivery_manager.py" line="63"/>
        <source>Domestic
volume (m3/d)</source>
        <translation type="obsolete">Volume domestique (m3/jour)</translation>
    </message>
    <message>
        <location filename="../gui/water_delivery_manager.py" line="63"/>
        <source>Adjustment
coef.</source>
        <translation type="obsolete">Coef. d'ajustement</translation>
    </message>
    <message>
        <location filename="../gui/water_delivery_manager.py" line="63"/>
        <source>Leak efficiency
rate</source>
        <translation type="obsolete">Rendement</translation>
    </message>
    <message>
        <location filename="../gui/water_delivery_manager.py" line="67"/>
        <source>Water delivery
point</source>
        <translation type="obsolete">Point de consommation</translation>
    </message>
    <message>
        <location filename="../gui/water_delivery_manager.py" line="67"/>
        <source>Industrial
volume (m3/d)</source>
        <translation type="obsolete">Volume industriel (m3/jour)</translation>
    </message>
    <message>
        <location filename="../gui/water_delivery_manager.py" line="67"/>
        <source>Target
node</source>
        <translation type="obsolete">Noeud cible</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="205"/>
        <source>Model</source>
        <translation>Modèle</translation>
    </message>
    <message>
        <location filename="../gui/water_delivery_manager.py" line="67"/>
        <source>Industrial
curve</source>
        <translation type="obsolete">Courbe industrielle</translation>
    </message>
    <message>
        <location filename="../gui/menu.py" line="93"/>
        <source>Save custom layer styles</source>
        <translation type="obsolete">Enregistrer les styles de couches personalisés</translation>
    </message>
    <message>
        <location filename="../gui/menu.py" line="101"/>
        <source>Save custom layers styles</source>
        <translation type="obsolete">Enregistrer les styles de couches personalisés</translation>
    </message>
    <message>
        <location filename="../gui/menu.py" line="101"/>
        <source>This will overwrite all your custom layer styles</source>
        <translation type="obsolete">Ceci écrasera tous les styles de couche personalisés</translation>
    </message>
    <message>
        <location filename="../gui/project_manager.py" line="71"/>
        <source>If the project is up to date, database views and function will be reloaded</source>
        <translation type="obsolete">Si le projet est à jour, les vues et les fonctions de la base de données seront rechargés</translation>
    </message>
    <message>
        <location filename="../gui/project_manager.py" line="75"/>
        <source>The directory must contain:</source>
        <translation type="obsolete">Le répertoire doit contenir:</translation>
    </message>
    <message>
        <location filename="../gui/project_manager.py" line="75"/>
        <source>- a .dat file where most information is stored</source>
        <translation type="obsolete">- un fichier .dat où sont stockées la plupart des informations</translation>
    </message>
    <message>
        <location filename="../gui/project_manager.py" line="75"/>
        <source>- a _CrbModulConso.Csv file for modulation curves (curves that are already in the project are ignored)</source>
        <translation type="obsolete">- un fichier _CrbModulConso.Csv pour les courbes de modulation (les courbes déjà présentes dans le projets sont ignorées)</translation>
    </message>
    <message>
        <location filename="../gui/project_manager.py" line="75"/>
        <source>- a caracteristiques_materiaux.xlsx file</source>
        <translation type="obsolete">- un fichier caracteristiques_materiaux.xlsx</translation>
    </message>
    <message>
        <location filename="../gui/project_manager.py" line="75"/>
        <source>- a _Nod.Csv file</source>
        <translation type="obsolete">- un fichier _Node.Csv</translation>
    </message>
    <message>
        <location filename="../gui/project_manager.py" line="75"/>
        <source>- a _CANA.Csv file</source>
        <translation type="obsolete">- un fichier -CANA.Csv</translation>
    </message>
    <message>
        <location filename="../gui/project_manager.py" line="145"/>
        <source>created</source>
        <translation type="obsolete">créé</translation>
    </message>
    <message>
        <location filename="../gui/project_manager.py" line="165"/>
        <source>duplicated into</source>
        <translation type="obsolete"> dupliqué dans </translation>
    </message>
    <message>
        <location filename="../gui/project_manager.py" line="171"/>
        <source>Delete project</source>
        <translation type="obsolete">Supprimer le projet</translation>
    </message>
    <message>
        <location filename="../gui/project_manager.py" line="171"/>
        <source>This will delete project </source>
        <translation type="obsolete">Ceci supprimera le projet </translation>
    </message>
    <message>
        <location filename="../gui/project_manager.py" line="177"/>
        <source> deleted</source>
        <translation type="obsolete"> supprimé</translation>
    </message>
    <message>
        <location filename="../gui/project_manager.py" line="209"/>
        <source>Select a file</source>
        <translation type="obsolete">Sélectionner un fichier</translation>
    </message>
    <message>
        <location filename="../gui/project_manager.py" line="254"/>
        <source> exported to </source>
        <translation type="obsolete"> exporté dans </translation>
    </message>
    <message>
        <location filename="../gui/project_manager.py" line="205"/>
        <source> updated</source>
        <translation type="obsolete"> mis à jour</translation>
    </message>
    <message>
        <location filename="../gui/project_manager.py" line="214"/>
        <source>SRID not found in file </source>
        <translation type="obsolete">SRID introuvable dans le fichier </translation>
    </message>
    <message>
        <location filename="../gui/project_manager.py" line="235"/>
        <source> imported from </source>
        <translation type="obsolete"> importé depuis </translation>
    </message>
    <message>
        <location filename="../gui/project_manager.py" line="242"/>
        <source>Choose scenario (optional)</source>
        <translation type="obsolete">Choisir un scénario (optionnel)</translation>
    </message>
    <message>
        <location filename="../gui/project_manager.py" line="251"/>
        <source>Select a file to export </source>
        <translation type="obsolete">Selectionner un fichier où exporter </translation>
    </message>
    <message>
        <location filename="../gui/project_manager.py" line="272"/>
        <source> in project </source>
        <translation type="obsolete"> dans le projet </translation>
    </message>
    <message>
        <location filename="../gui/project_manager.py" line="282"/>
        <source>Select a directory</source>
        <translation type="obsolete">Sélectionner un répertoire</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="240"/>
        <source>Water delivery point pipe</source>
        <translation>Canalisation du point de consommation</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="676"/>
        <source>All nodes</source>
        <translation>Tous les noeuds</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="232"/>
        <source> up</source>
        <translation> amont</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="233"/>
        <source> down</source>
        <translation> aval</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="235"/>
        <source>User node domestic curve</source>
        <translation>Courbe de modulation domestique des noeuds</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="236"/>
        <source>User node industrial curve</source>
        <translation>Courbe de modulation industrielle des noeuds</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="235"/>
        <source>Chlorine injection target node</source>
        <translation type="obsolete">Noeud cible d'une injection de chlore</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="175"/>
        <source>Configured</source>
        <translation>Configuré</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="380"/>
        <source>Warning</source>
        <translation>Alertes</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="205"/>
        <source>Configuration</source>
        <translation>Configuration</translation>
    </message>
    <message>
        <location filename="../gui/toolbar.py" line="111"/>
        <source>Default</source>
        <translation type="obsolete">Défaut</translation>
    </message>
    <message>
        <location filename="../gui/menu.py" line="89"/>
        <source>&amp;Advanced tools</source>
        <translation type="obsolete">&amp;Outils avancés</translation>
    </message>
    <message>
        <location filename="../gui/menu.py" line="90"/>
        <source>&amp;Extract</source>
        <translation type="obsolete">&amp;Extract</translation>
    </message>
    <message>
        <location filename="../bin/extract.py" line="172"/>
        <source>Extract error</source>
        <translation type="obsolete">Erreur extract</translation>
    </message>
    <message>
        <location filename="../bin/extract.py" line="175"/>
        <source>Extract successful</source>
        <translation type="obsolete">Succès extract</translation>
    </message>
    <message>
        <location filename="../bin/extract.py" line="223"/>
        <source>WExpdess error</source>
        <translation type="obsolete">Erreur wexpdess</translation>
    </message>
    <message>
        <location filename="../bin/extract.py" line="226"/>
        <source>WExpdess successful</source>
        <translation type="obsolete">Succès wexpdess</translation>
    </message>
    <message>
        <location filename="../gui/toolbar.py" line="67"/>
        <source>Postprocess</source>
        <translation type="obsolete">Post-traitement</translation>
    </message>
    <message>
        <location filename="../gui/water_delivery_sector_settings.py" line="55"/>
        <source>Comment</source>
        <translation type="obsolete">Commentaire</translation>
    </message>
    <message>
        <location filename="../gui/water_delivery_sector_settings.py" line="49"/>
        <source>Total domestic
volume (m3/d)</source>
        <translation type="obsolete">Volume domestique total (m3/jour)</translation>
    </message>
    <message>
        <location filename="../gui/water_delivery_sector_settings.py" line="55"/>
        <source>Name</source>
        <translation type="obsolete">Nom</translation>
    </message>
    <message>
        <location filename="../gui/water_delivery_sector_settings.py" line="52"/>
        <source>Domestic
curve</source>
        <translation type="obsolete">Courbe domestique</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="205"/>
        <source>Sectorization</source>
        <translation>Sectorisation</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="241"/>
        <source>Pipe singularity</source>
        <translation>Singularité sur canalisation</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="241"/>
        <source>Pipe singularity pipe</source>
        <translation>Canalisation de la singularité</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="242"/>
        <source>Fire hydrant</source>
        <translation>Poteau incendie</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="242"/>
        <source>Fire hydrant pipe</source>
        <translation>Canalisation du poteau incendie</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="205"/>
        <source>Metadata</source>
        <translation>Métadonnée</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="379"/>
        <source>Exterior fire defense</source>
        <translation>Défense extérieure contre l'incendie</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="72"/>
        <source>Surface (m²)</source>
        <translation>Surface (m²)</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="464"/>
        <source>warning_on_node</source>
        <translation>Alerte sur les noeuds</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="464"/>
        <source>warning_on_pipe</source>
        <translation>Alerte sur les canalisations</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="359"/>
        <source>SYNTHETIC RESULTS</source>
        <translation>RESULTATS SYNTHETIQUES</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="73"/>
        <source>Surface (mÂ²)</source>
        <translation type="obsolete">Surface(m²)</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="464"/>
        <source>slowdown</source>
        <translation>Ralentissement du calcul</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="243"/>
        <source>Sensor</source>
        <translation>Capteur</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="672"/>
        <source>Measure</source>
        <translation>Mesure</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="171"/>
        <source>Measures layers</source>
        <translation>Métrologie</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="175"/>
        <source>Informations</source>
        <translation>Informations</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="175"/>
        <source>Link without piezo reference</source>
        <translation>Liaisons sans référence piezo</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="175"/>
        <source>Node without piezo reference</source>
        <translation>Noeuds sans référence piezo</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="360"/>
        <source>Flow on pipe</source>
        <translation type="obsolete">Débit</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="477"/>
        <source>Contact time max (h)</source>
        <translation>Temps de contact max (h)</translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="331"/>
        <source>ANIMATED RESULTS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qgis_utilities.py" line="321"/>
        <source>results</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Autograph</name>
    <message>
        <location filename="../processing/autograph.py" line="45"/>
        <source>autograph</source>
        <translation>autograph</translation>
    </message>
    <message>
        <location filename="../processing/autograph.py" line="48"/>
        <source>expresseau</source>
        <translation>expresseau</translation>
    </message>
    <message>
        <location filename="../processing/autograph.py" line="54"/>
        <source>Creates measure/simualtion comparison graphs for model calibration</source>
        <translation>Crée des graphes de comparaison mesure/simulation pour le calage</translation>
    </message>
</context>
<context>
    <name>ComputSettingsWidget</name>
    <message>
        <location filename="../gui/scn_widgets/comput_settings.py" line="84"/>
        <source>Add hydrology file</source>
        <translation>Ajouter un fichier hydrologique</translation>
    </message>
</context>
<context>
    <name>ExportInp</name>
    <message>
        <location filename="../processing/export_inp.py" line="38"/>
        <source>export inp</source>
        <translation>export inp</translation>
    </message>
    <message>
        <location filename="../processing/export_inp.py" line="41"/>
        <source>expresseau</source>
        <translation>expresseau</translation>
    </message>
    <message>
        <location filename="../processing/export_inp.py" line="47"/>
        <source>Export model to an epanet .inp file.</source>
        <translation>Exporter le modèle au format epanet .inp.</translation>
    </message>
    <message>
        <location filename="../processing/export_inp.py" line="92"/>
        <source> exported to </source>
        <translation> exporté dans </translation>
    </message>
    <message>
        <location filename="../processing/export_inp.py" line="90"/>
        <source> with </source>
        <translation> avec </translation>
    </message>
    <message>
        <location filename="../processing/export_inp.py" line="88"/>
        <source> errors and </source>
        <translation> erreurs et </translation>
    </message>
    <message>
        <location filename="../processing/export_inp.py" line="90"/>
        <source> warnings</source>
        <translation> alertes</translation>
    </message>
</context>
<context>
    <name>ExpresseauMenu</name>
    <message>
        <location filename="../gui/menu.py" line="57"/>
        <source>&amp;Manage projects</source>
        <translation>&amp;Gérer les projets</translation>
    </message>
    <message>
        <location filename="../gui/menu.py" line="58"/>
        <source>&amp;New project</source>
        <translation>&amp;Nouveau projet</translation>
    </message>
    <message>
        <location filename="../gui/menu.py" line="65"/>
        <source>&amp;Models</source>
        <translation>&amp;Modèles</translation>
    </message>
    <message>
        <location filename="../gui/menu.py" line="66"/>
        <source>&amp;Set current</source>
        <translation>&amp;Définir le modèle courant</translation>
    </message>
    <message>
        <location filename="../gui/menu.py" line="75"/>
        <source>&amp;Add model</source>
        <translation>&amp;Ajouter un modèle</translation>
    </message>
    <message>
        <location filename="../gui/menu.py" line="76"/>
        <source>&amp;Delete current model</source>
        <translation>&amp;Supprimer le modèle courant</translation>
    </message>
    <message>
        <location filename="../gui/menu.py" line="79"/>
        <source>&amp;Water delivery</source>
        <translation>&amp;Distribution</translation>
    </message>
    <message>
        <location filename="../gui/menu.py" line="80"/>
        <source>&amp;Water delivery sector settings</source>
        <translation>Secteurs de distribution</translation>
    </message>
    <message>
        <location filename="../gui/menu.py" line="81"/>
        <source>Water delivery &amp;scenario</source>
        <translation>Scénarios de distribution</translation>
    </message>
    <message>
        <location filename="../gui/menu.py" line="85"/>
        <source>&amp;Scenarios</source>
        <translation>&amp;Scénarios de calcul</translation>
    </message>
    <message>
        <location filename="../gui/menu.py" line="83"/>
        <source>&amp;Settings</source>
        <translation type="obsolete">&amp;Paramètres</translation>
    </message>
    <message>
        <location filename="../gui/menu.py" line="88"/>
        <source>&amp;Advanced tools</source>
        <translation>&amp;Outils avancés</translation>
    </message>
    <message>
        <location filename="../gui/menu.py" line="89"/>
        <source>&amp;Extract</source>
        <translation>&amp;Extract</translation>
    </message>
    <message>
        <location filename="../gui/menu.py" line="92"/>
        <source>Licence activation</source>
        <translation>Activer la licence</translation>
    </message>
    <message>
        <location filename="../gui/menu.py" line="93"/>
        <source>Save custom layer styles</source>
        <translation>Enregistrer les styles de couches personalisés</translation>
    </message>
    <message>
        <location filename="../gui/menu.py" line="94"/>
        <source>&amp;Help</source>
        <translation>&amp;Aide</translation>
    </message>
    <message>
        <location filename="../gui/menu.py" line="95"/>
        <source>About</source>
        <translation>A propos</translation>
    </message>
    <message>
        <location filename="../gui/menu.py" line="101"/>
        <source>Save custom layers styles</source>
        <translation>Enregistrer les styles personalisés des couches </translation>
    </message>
    <message>
        <location filename="../gui/menu.py" line="101"/>
        <source>This will overwrite all your custom layer styles</source>
        <translation>Ceci écrasera tous les styles de couche personalisés</translation>
    </message>
    <message>
        <location filename="../gui/menu.py" line="157"/>
        <source>Delete model</source>
        <translation>Supprimer le modèle</translation>
    </message>
    <message>
        <location filename="../gui/menu.py" line="157"/>
        <source>This will delete model </source>
        <translation>Cela supprimera le modèle </translation>
    </message>
    <message>
        <location filename="../gui/menu.py" line="157"/>
        <source>. Proceed?</source>
        <translation>. Continuer ?</translation>
    </message>
    <message>
        <location filename="../gui/menu.py" line="186"/>
        <source>Licence already activated</source>
        <translation>La licence est déjà activée</translation>
    </message>
    <message>
        <location filename="../gui/menu.py" line="167"/>
        <source>A licence has already been activated. Do you want to activate it again?</source>
        <translation type="obsolete">La licence a déjà été activée. Voulez vous l'activer de nouveau ?</translation>
    </message>
    <message>
        <location filename="../gui/menu.py" line="172"/>
        <source>Hydra licence activation</source>
        <translation type="obsolete">Activation de la licence</translation>
    </message>
    <message>
        <location filename="../gui/menu.py" line="172"/>
        <source>Enter your licence number:</source>
        <translation type="obsolete">Saisir le numéro de licence:</translation>
    </message>
    <message>
        <location filename="../gui/menu.py" line="196"/>
        <source>Licence activated</source>
        <translation>Licence activée</translation>
    </message>
    <message>
        <location filename="../gui/menu.py" line="197"/>
        <source>Activation successfull</source>
        <translation>Activation réalisée</translation>
    </message>
    <message>
        <location filename="../gui/menu.py" line="178"/>
        <source>Your licence has successfully been activated!</source>
        <translation type="obsolete">Votre licence a été activée!</translation>
    </message>
    <message>
        <location filename="../gui/menu.py" line="200"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <location filename="../gui/menu.py" line="181"/>
        <source>There has been an error during your licence activation. Please contact technical support.</source>
        <translation type="obsolete">Il y a eu une erreur ors de l'activation de la licence. Merci de contacter le support technique.</translation>
    </message>
    <message>
        <location filename="../gui/menu.py" line="183"/>
        <source>No licence activated</source>
        <translation type="obsolete">La licence n'est pas activée</translation>
    </message>
    <message>
        <location filename="../gui/menu.py" line="220"/>
        <source>About Expresseau</source>
        <translation>A propos d'Expresseau</translation>
    </message>
    <message>
        <location filename="../gui/menu.py" line="82"/>
        <source>&amp;Regulation file from selected fire hydrants</source>
        <translation>Fichier de &amp;regulation à partir des poteaux sélectionnés</translation>
    </message>
    <message>
        <location filename="../gui/menu.py" line="110"/>
        <source>Select a file</source>
        <translation>Sélectionner un fichier</translation>
    </message>
    <message>
        <location filename="../gui/menu.py" line="186"/>
        <source>A license has already been activated. Do you want to activate it again?</source>
        <translation>La licence a déjà été activée. Voulez-vous l'activer de nouveau ?</translation>
    </message>
    <message>
        <location filename="../gui/menu.py" line="191"/>
        <source>Hydra license activation</source>
        <translation>Activation de la licence</translation>
    </message>
    <message>
        <location filename="../gui/menu.py" line="191"/>
        <source>Enter your license number:</source>
        <translation>Saisir le numéro de licence:</translation>
    </message>
    <message>
        <location filename="../gui/menu.py" line="197"/>
        <source>Your license has successfully been activated!</source>
        <translation>Votre licence a été activée!</translation>
    </message>
    <message>
        <location filename="../gui/menu.py" line="200"/>
        <source>There has been an error during your license activation. Please contact technical support.</source>
        <translation>Il y a eu une erreur lors de l'activation de la licence. Merci de contacter le support technique.</translation>
    </message>
    <message>
        <location filename="../gui/menu.py" line="202"/>
        <source>No license activated</source>
        <translation>La licence n'est pas activée</translation>
    </message>
    <message>
        <location filename="../gui/menu.py" line="90"/>
        <source>&amp;Anim&apos;eau</source>
        <translation>&amp;Anim'eau</translation>
    </message>
</context>
<context>
    <name>ExpresseauToolbar</name>
    <message>
        <location filename="../gui/toolbar.py" line="36"/>
        <source>Expresseau toolbar</source>
        <translation type="obsolete">Barre d'outils Expresseau</translation>
    </message>
    <message>
        <location filename="../gui/toolbar.py" line="137"/>
        <source>Default</source>
        <translation>Défaut</translation>
    </message>
    <message>
        <location filename="../gui/toolbar.py" line="75"/>
        <source>Run computation</source>
        <translation>Lancer le calcul</translation>
    </message>
    <message>
        <location filename="../gui/toolbar.py" line="78"/>
        <source>Postprocess</source>
        <translation>Post-traitement</translation>
    </message>
    <message>
        <location filename="../gui/toolbar.py" line="93"/>
        <source>toolbar created</source>
        <translation>barre d'outil créée</translation>
    </message>
    <message>
        <location filename="../gui/toolbar.py" line="58"/>
        <source>Current model</source>
        <translation>Modèle courant</translation>
    </message>
    <message>
        <location filename="../gui/toolbar.py" line="64"/>
        <source>Current configuration</source>
        <translation>Configuration courante</translation>
    </message>
    <message>
        <location filename="../gui/toolbar.py" line="73"/>
        <source>Current scenario</source>
        <translation>Scénario courant</translation>
    </message>
    <message>
        <location filename="../gui/toolbar.py" line="80"/>
        <source>If checked, post-processing is run automatically after computation, you can also check to launch post-processing for the current scenario.</source>
        <translation>Si coché, le post-traitement est lancé automatiquement après le calcul. En cochant le post-traitement est lancé pour le scénario courant.</translation>
    </message>
    <message>
        <location filename="../gui/toolbar.py" line="83"/>
        <source>Configuration:</source>
        <translation>Configuration:</translation>
    </message>
    <message>
        <location filename="../gui/toolbar.py" line="85"/>
        <source>Current model:</source>
        <translation>Modèle courant:</translation>
    </message>
    <message>
        <location filename="../gui/toolbar.py" line="87"/>
        <source>Current scenario:</source>
        <translation>Scénario courant:</translation>
    </message>
</context>
<context>
    <name>Extract</name>
    <message>
        <location filename="../bin/extract.py" line="168"/>
        <source>Extract error</source>
        <translation>Erreur extract</translation>
    </message>
    <message>
        <location filename="../bin/extract.py" line="171"/>
        <source>Extract successful</source>
        <translation>Succès extract</translation>
    </message>
    <message>
        <location filename="../bin/extract.py" line="219"/>
        <source>WExpdess error</source>
        <translation>Erreur wexpdess</translation>
    </message>
    <message>
        <location filename="../bin/extract.py" line="222"/>
        <source>WExpdess successful</source>
        <translation>Succès wexpdess</translation>
    </message>
</context>
<context>
    <name>Form</name>
    <message>
        <location filename="../gui/water_delivery_manager.ui" line="20"/>
        <source>Water delivery scenario</source>
        <translation type="obsolete">Scénario de distribution</translation>
    </message>
    <message>
        <location filename="../gui/water_delivery_manager.ui" line="26"/>
        <source>Domestic water delivery scenarios</source>
        <translation type="obsolete">Scénarios de distribution domestique</translation>
    </message>
    <message>
        <location filename="../gui/water_delivery_manager.ui" line="32"/>
        <source>Domestic water delivery settings</source>
        <translation type="obsolete">Paramétrage des distributions domestiques</translation>
    </message>
    <message>
        <location filename="../gui/water_delivery_manager.ui" line="38"/>
        <source>Weekdays</source>
        <translation type="obsolete">Jours de semaine</translation>
    </message>
    <message>
        <location filename="../gui/water_delivery_manager.ui" line="48"/>
        <source>Weekend</source>
        <translation type="obsolete">Weekend</translation>
    </message>
    <message>
        <location filename="../gui/water_delivery_manager.ui" line="62"/>
        <source>Sector</source>
        <translation type="obsolete">Secteur</translation>
    </message>
    <message>
        <location filename="../gui/water_delivery_manager.ui" line="67"/>
        <source>Domestic volume (m3/d)</source>
        <translation type="obsolete">Volume domestique (m3/j)</translation>
    </message>
    <message>
        <location filename="../gui/water_delivery_manager.ui" line="72"/>
        <source>Adjustment coef.</source>
        <translation type="obsolete">Coef. ajustement</translation>
    </message>
    <message>
        <location filename="../gui/water_delivery_manager.ui" line="77"/>
        <source>Leak efficiency rate</source>
        <translation type="obsolete">Rendement</translation>
    </message>
    <message>
        <location filename="../gui/water_delivery_manager.ui" line="91"/>
        <source>Industrial water delivery settings</source>
        <translation type="obsolete">Paramétrage des distributions industrielles</translation>
    </message>
    <message>
        <location filename="../gui/water_delivery_manager.ui" line="116"/>
        <source>Water delivery point</source>
        <translation type="obsolete">Point de consommation</translation>
    </message>
    <message>
        <location filename="../gui/water_delivery_manager.ui" line="121"/>
        <source>Volume (m3/d)</source>
        <translation type="obsolete">Volume (m3/jour)</translation>
    </message>
    <message>
        <location filename="../gui/water_delivery_manager.ui" line="126"/>
        <source>Target node</source>
        <translation type="obsolete">Noeud cible</translation>
    </message>
    <message>
        <location filename="../gui/water_delivery_manager.ui" line="131"/>
        <source>Model</source>
        <translation type="obsolete">Modèle</translation>
    </message>
    <message>
        <location filename="../gui/water_delivery_manager.ui" line="136"/>
        <source>Industrial curve</source>
        <translation type="obsolete">Courbe industrielle</translation>
    </message>
    <message>
        <location filename="../gui/widgets/array_widget.ui" line="92"/>
        <source>Add</source>
        <translation type="obsolete">Ajouter</translation>
    </message>
    <message>
        <location filename="../gui/widgets/array_widget.ui" line="53"/>
        <source>Delete</source>
        <translation type="obsolete">Supprimer</translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/regulation_and_configuration.ui" line="30"/>
        <source>Regulation settings</source>
        <translation type="obsolete">Régulation</translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/regulation_and_configuration.ui" line="39"/>
        <source>Add file</source>
        <translation type="obsolete">Ajouter un fichier</translation>
    </message>
    <message>
        <location filename="../bin/extract.ui" line="89"/>
        <source>Edit file</source>
        <translation>Editer</translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/regulation_and_configuration.ui" line="46"/>
        <source>Delete file</source>
        <translation type="obsolete">Supprimer</translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/regulation_and_configuration.ui" line="108"/>
        <source>Configurations</source>
        <translation type="obsolete">Configurations</translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/regulation_and_configuration.ui" line="116"/>
        <source>Remove</source>
        <translation type="obsolete">Supprimer</translation>
    </message>
    <message>
        <location filename="../bin/extract.ui" line="68"/>
        <source>Extract</source>
        <translation>Extract</translation>
    </message>
    <message>
        <location filename="../bin/extract.ui" line="36"/>
        <source>(Extract files from #DIR#)</source>
        <translation>Fichiers extract dans #DIR#</translation>
    </message>
    <message>
        <location filename="../bin/extract.ui" line="75"/>
        <source>Open log file</source>
        <translation>Ouvrir le fichier log</translation>
    </message>
    <message>
        <location filename="../bin/extract.ui" line="82"/>
        <source>Wexpdess</source>
        <translation>Wexpdess</translation>
    </message>
    <message>
        <location filename="../bin/extract.ui" line="96"/>
        <source>Help</source>
        <translation>Aide</translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/regulation_and_configuration.ui" line="120"/>
        <source>Selected for scenario</source>
        <translation type="obsolete">Sélectionés pour le scénario</translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/regulation_and_configuration.ui" line="166"/>
        <source>Available</source>
        <translation type="obsolete">Disponibles</translation>
    </message>
</context>
<context>
    <name>ImportInp</name>
    <message>
        <location filename="../processing/import_inp.py" line="43"/>
        <source>import inp</source>
        <translation>importer un fichier inp</translation>
    </message>
    <message>
        <location filename="../processing/import_inp.py" line="46"/>
        <source>expresseau</source>
        <translation>expresseau</translation>
    </message>
    <message>
        <location filename="../processing/import_inp.py" line="52"/>
        <source>Import epanet model from .inp file.</source>
        <translation>Importer un modèle epanet depuis un fichier .inp.</translation>
    </message>
    <message>
        <location filename="../processing/import_inp.py" line="110"/>
        <source> imported from </source>
        <translation> importé depuis </translation>
    </message>
    <message>
        <location filename="../processing/import_inp.py" line="108"/>
        <source> with </source>
        <translation> avec </translation>
    </message>
    <message>
        <location filename="../processing/import_inp.py" line="106"/>
        <source> errors and </source>
        <translation> erreurs et </translation>
    </message>
    <message>
        <location filename="../processing/import_inp.py" line="108"/>
        <source> warnings</source>
        <translation> alertes</translation>
    </message>
</context>
<context>
    <name>ProjectManager</name>
    <message>
        <location filename="../gui/project_manager.py" line="74"/>
        <source>If the project is up to date, database views and function will be reloaded</source>
        <translation>Si le projet est à jour, les vues et les fonctions de la base de données seront rechargés</translation>
    </message>
    <message>
        <location filename="../gui/project_manager.py" line="72"/>
        <source>The directory must contain:</source>
        <translation type="obsolete">Le répertoire doit contenir:</translation>
    </message>
    <message>
        <location filename="../gui/project_manager.py" line="78"/>
        <source>- a .dat file where most information is stored</source>
        <translation>- un fichier .dat où sont stockées la plupart des informations</translation>
    </message>
    <message>
        <location filename="../gui/project_manager.py" line="78"/>
        <source>- a _CrbModulConso.Csv file for modulation curves (curves that are already in the project are ignored)</source>
        <translation>- un fichier _CrbModulConso.Csv pour les courbes de modulation (les courbes déjà présentes dans le projets sont ignorées)</translation>
    </message>
    <message>
        <location filename="../gui/project_manager.py" line="78"/>
        <source>- a caracteristiques_materiaux.xlsx file</source>
        <translation>- un fichier caracteristiques_materiaux.xlsx</translation>
    </message>
    <message>
        <location filename="../gui/project_manager.py" line="78"/>
        <source>- a _Nod.Csv file</source>
        <translation>- un fichier _Node.Csv</translation>
    </message>
    <message>
        <location filename="../gui/project_manager.py" line="78"/>
        <source>- a _CANA.Csv file</source>
        <translation>- un fichier -CANA.Csv</translation>
    </message>
    <message>
        <location filename="../gui/project_manager.py" line="190"/>
        <source>created</source>
        <translation>créé</translation>
    </message>
    <message>
        <location filename="../gui/project_manager.py" line="216"/>
        <source>duplicated into</source>
        <translation> dupliqué dans </translation>
    </message>
    <message>
        <location filename="../gui/project_manager.py" line="223"/>
        <source>Delete project</source>
        <translation>Supprimer le projet</translation>
    </message>
    <message>
        <location filename="../gui/project_manager.py" line="224"/>
        <source>This will delete project </source>
        <translation>Ceci supprimera le projet </translation>
    </message>
    <message>
        <location filename="../gui/project_manager.py" line="332"/>
        <source>. Proceed?</source>
        <translation>. Continuer ?</translation>
    </message>
    <message>
        <location filename="../gui/project_manager.py" line="236"/>
        <source> deleted</source>
        <translation> supprimé</translation>
    </message>
    <message>
        <location filename="../gui/project_manager.py" line="348"/>
        <source>Select a file</source>
        <translation>Sélectionner un fichier</translation>
    </message>
    <message>
        <location filename="../gui/project_manager.py" line="245"/>
        <source> exported to </source>
        <translation> exporté dans </translation>
    </message>
    <message>
        <location filename="../gui/project_manager.py" line="260"/>
        <source> updated</source>
        <translation> mis à jour</translation>
    </message>
    <message>
        <location filename="../gui/project_manager.py" line="269"/>
        <source>SRID not found in file </source>
        <translation>SRID introuvable dans le fichier </translation>
    </message>
    <message>
        <location filename="../gui/project_manager.py" line="279"/>
        <source> imported from </source>
        <translation> importé depuis </translation>
    </message>
    <message>
        <location filename="../gui/project_manager.py" line="294"/>
        <source>Choose scenario (optional)</source>
        <translation>Choisir un scénario (optionnel)</translation>
    </message>
    <message>
        <location filename="../gui/project_manager.py" line="303"/>
        <source>Select a file to export </source>
        <translation>Selectionner un fichier où exporter </translation>
    </message>
    <message>
        <location filename="../gui/project_manager.py" line="331"/>
        <source>Delete model</source>
        <translation>Supprimer le modèle</translation>
    </message>
    <message>
        <location filename="../gui/project_manager.py" line="332"/>
        <source>This will delete model </source>
        <translation>Cela supprimera le modèle </translation>
    </message>
    <message>
        <location filename="../gui/project_manager.py" line="332"/>
        <source> in project </source>
        <translation> dans le projet </translation>
    </message>
    <message>
        <location filename="../gui/project_manager.py" line="282"/>
        <source>Select a directory</source>
        <translation type="obsolete">Sélectionner un répertoire</translation>
    </message>
    <message>
        <location filename="../gui/project_manager.py" line="78"/>
        <source>Import epanet model (*.inp) or legacy expresseau models (*.dat).</source>
        <translation>Importer un modèle epanet (*.inp) ou un modèle expresseau ancien (*.dat).</translation>
    </message>
    <message>
        <location filename="../gui/project_manager.py" line="78"/>
        <source>Note: in case of legacy import, the directory the .dat file is located in must contain:</source>
        <translation>Note: pour importer un modèle ancien, le répertoire dans lequel se trouve le fichier .dat doit aussi contenir:</translation>
    </message>
    <message>
        <location filename="../gui/project_manager.py" line="346"/>
        <source> with </source>
        <translation type="obsolete"> avec </translation>
    </message>
    <message>
        <location filename="../gui/project_manager.py" line="344"/>
        <source> errors and </source>
        <translation type="obsolete"> erreurs et </translation>
    </message>
    <message>
        <location filename="../gui/project_manager.py" line="346"/>
        <source> warnings</source>
        <translation type="obsolete"> alertes</translation>
    </message>
    <message>
        <location filename="../gui/project_manager.py" line="225"/>
        <source>Also delete project directory</source>
        <translation>Supprimer aussi le répertoire projet</translation>
    </message>
    <message>
        <location filename="../gui/project_manager.py" line="333"/>
        <source>Also delete delivery sectors and nodes</source>
        <translation>Supprimer aussi les secteurs et les points de consommation</translation>
    </message>
    <message>
        <location filename="../gui/project_manager.py" line="169"/>
        <source>Update project</source>
        <translation>Mettre à jour le projet</translation>
    </message>
    <message>
        <location filename="../gui/project_manager.py" line="169"/>
        <source>Do you want to update project {} from version {} to version {} ?</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>RegulationConfigurationWidget</name>
    <message>
        <location filename="../gui/scn_widgets/regulation_and_configuration.py" line="142"/>
        <source>Add regulation file</source>
        <translation>Ajouter un fichier de régulation</translation>
    </message>
</context>
<context>
    <name>ResultWindow</name>
    <message>
        <location filename="../gui/result_dialog.py" line="251"/>
        <source>Select a file</source>
        <translation>Sélectionner un fichier</translation>
    </message>
    <message>
        <location filename="../gui/result_dialog.py" line="266"/>
        <source>Select a file to save graph</source>
        <translation>Selectionner un fichier pour sauver le graphe</translation>
    </message>
</context>
<context>
    <name>ScenarioManager</name>
    <message>
        <location filename="../gui/scenario_manager.py" line="66"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../gui/scenario_manager.py" line="66"/>
        <source>Comment</source>
        <translation>Commentaire</translation>
    </message>
</context>
<context>
    <name>Simualte</name>
    <message>
        <location filename="../processing/simulate.py" line="55"/>
        <source>expresseau</source>
        <translation type="obsolete">expresseau</translation>
    </message>
    <message>
        <location filename="../processing/simulate.py" line="61"/>
        <source>Run expresseau simulation.</source>
        <translation type="obsolete">Lance une simulation expresseau</translation>
    </message>
    <message>
        <location filename="../processing/simulate.py" line="52"/>
        <source>simulate</source>
        <translation type="obsolete">simuler</translation>
    </message>
</context>
<context>
    <name>Simulate</name>
    <message>
        <location filename="../processing/simulate.py" line="578"/>
        <source>simulate</source>
        <translation>Lancement d'une simulation</translation>
    </message>
    <message>
        <location filename="../processing/simulate.py" line="581"/>
        <source>expresseau</source>
        <translation>expresseau</translation>
    </message>
    <message>
        <location filename="../processing/simulate.py" line="587"/>
        <source>Run expresseau simulation.</source>
        <translation>Lancer une simulation expresseau.</translation>
    </message>
    <message>
        <location filename="../processing/simulate.py" line="595"/>
        <source>Project</source>
        <translation>Projet</translation>
    </message>
    <message>
        <location filename="../processing/simulate.py" line="606"/>
        <source>Postprocess</source>
        <translation>Post-traitement</translation>
    </message>
    <message>
        <location filename="../processing/simulate.py" line="609"/>
        <source>Number of process (Exterior fire defense and Break criticality only)</source>
        <translation>Nombre de processeurs utilisés (défense extérieure contre l'incendie et criticité des casses uniquement)</translation>
    </message>
    <message>
        <location filename="../processing/simulate.py" line="603"/>
        <source>Autograph</source>
        <translation>Autographe</translation>
    </message>
</context>
<context>
    <name>W14ToCsv</name>
    <message>
        <location filename="../processing/wfiles_to_csv.py" line="29"/>
        <source>w14 to csv</source>
        <translation>w14 vers csv</translation>
    </message>
    <message>
        <location filename="../processing/wfiles_to_csv.py" line="32"/>
        <source>expresseau</source>
        <translation>expresseau</translation>
    </message>
    <message>
        <location filename="../processing/wfiles_to_csv.py" line="38"/>
        <source>Convert w14 to csv</source>
        <translation>Converti un fichier de résultat w14 en csv</translation>
    </message>
</context>
<context>
    <name>W14ToGpkg</name>
    <message>
        <location filename="../processing/w14_to_gpkg.py" line="31"/>
        <source>w14_to_gpkg</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../processing/w14_to_gpkg.py" line="34"/>
        <source>expresseau</source>
        <translation>expresseau</translation>
    </message>
    <message>
        <location filename="../processing/w14_to_gpkg.py" line="40"/>
        <source>Post-process expresseau scenario</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>W16ToUgrid</name>
    <message>
        <location filename="../processing/w16_to_ugrid.py" line="34"/>
        <source>w16_to_ugrid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/w16_to_ugrid.py" line="37"/>
        <source>expresseau</source>
        <translation type="unfinished">expresseau</translation>
    </message>
    <message>
        <location filename="../processing/w16_to_ugrid.py" line="43"/>
        <source>Post-process expresseau scenario for dynamic cartography</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/w16_to_ugrid.py" line="75"/>
        <source>post-processing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/w16_to_ugrid.py" line="88"/>
        <source>old ugrid file removed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/w16_to_ugrid.py" line="90"/>
        <source>no ugrid file to remove</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/w16_to_ugrid.py" line="93"/>
        <source>no results for scenario </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/w16_to_ugrid.py" line="106"/>
        <source>ugrid mesh loades</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../processing/w16_to_ugrid.py" line="108"/>
        <source>no ugrid mesh to load</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>W17ToCsv</name>
    <message>
        <location filename="../processing/wfiles_to_csv.py" line="78"/>
        <source>w17 to csv</source>
        <translation>w17 vers csv</translation>
    </message>
    <message>
        <location filename="../processing/wfiles_to_csv.py" line="81"/>
        <source>expresseau</source>
        <translation>expresseau</translation>
    </message>
    <message>
        <location filename="../processing/wfiles_to_csv.py" line="87"/>
        <source>Convert w17 to csv</source>
        <translation>Converti un fichier w17 en csv</translation>
    </message>
</context>
<context>
    <name>WaterDeliveryManager</name>
    <message>
        <location filename="../gui/water_delivery_manager.py" line="65"/>
        <source>Sector</source>
        <translation>Secteur</translation>
    </message>
    <message>
        <location filename="../gui/water_delivery_manager.py" line="65"/>
        <source>Domestic
volume (m3/d)</source>
        <translation>Volume domestique (m3/jour)</translation>
    </message>
    <message>
        <location filename="../gui/water_delivery_manager.py" line="65"/>
        <source>Adjustment
coef.</source>
        <translation>Coef. journalier</translation>
    </message>
    <message>
        <location filename="../gui/water_delivery_manager.py" line="65"/>
        <source>Leak efficiency
rate</source>
        <translation>Rendement</translation>
    </message>
    <message>
        <location filename="../gui/water_delivery_manager.py" line="69"/>
        <source>Water delivery
point</source>
        <translation>Point de consommation</translation>
    </message>
    <message>
        <location filename="../gui/water_delivery_manager.py" line="69"/>
        <source>Industrial
volume (m3/d)</source>
        <translation>Volume industriel (m3/jour)</translation>
    </message>
    <message>
        <location filename="../gui/water_delivery_manager.py" line="69"/>
        <source>Target
node</source>
        <translation>Noeud cible</translation>
    </message>
    <message>
        <location filename="../gui/water_delivery_manager.py" line="69"/>
        <source>Model</source>
        <translation>Modèle</translation>
    </message>
    <message>
        <location filename="../gui/water_delivery_manager.py" line="69"/>
        <source>Industrial
curve</source>
        <translation>Courbe industrielle</translation>
    </message>
</context>
<context>
    <name>WaterDeliverySectorSettings</name>
    <message>
        <location filename="../gui/water_delivery_sector_settings.py" line="51"/>
        <source>Sector</source>
        <translation>Secteur</translation>
    </message>
    <message>
        <location filename="../gui/water_delivery_sector_settings.py" line="57"/>
        <source>Comment</source>
        <translation>Commentaire</translation>
    </message>
    <message>
        <location filename="../gui/water_delivery_sector_settings.py" line="51"/>
        <source>Total domestic
volume (m3/d)</source>
        <translation>Volume domestique total (m3/jour)</translation>
    </message>
    <message>
        <location filename="../gui/water_delivery_sector_settings.py" line="54"/>
        <source>Model</source>
        <translation>Modèle</translation>
    </message>
    <message>
        <location filename="../gui/water_delivery_sector_settings.py" line="57"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../gui/water_delivery_sector_settings.py" line="54"/>
        <source>Domestic
volume (m3/d)</source>
        <translation>Volume domestique (m3/jour)</translation>
    </message>
    <message>
        <location filename="../gui/water_delivery_sector_settings.py" line="54"/>
        <source>Domestic
curve</source>
        <translation>Courbe domestique</translation>
    </message>
    <message>
        <location filename="../gui/water_delivery_sector_settings.py" line="66"/>
        <source>Weekday
Hourly coefficients</source>
        <translation>Coef. horaires semaine</translation>
    </message>
    <message>
        <location filename="../gui/water_delivery_sector_settings.py" line="66"/>
        <source>Weekend
Hourly coefficients</source>
        <translation>Coef. horaires weekend</translation>
    </message>
</context>
<context>
    <name>array_and_graph</name>
    <message>
        <location filename="../gui/widgets/array_and_graph.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>array_widget</name>
    <message>
        <location filename="../gui/widgets/array_widget.ui" line="26"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/widgets/array_widget.ui" line="53"/>
        <source>Delete</source>
        <translation>Supprimer</translation>
    </message>
    <message>
        <location filename="../gui/widgets/array_widget.ui" line="92"/>
        <source>Add</source>
        <translation>Ajouter</translation>
    </message>
    <message>
        <location filename="../gui/widgets/array_widget.ui" line="139"/>
        <source>column_1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/widgets/array_widget.ui" line="144"/>
        <source>column_2</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>comput_settings</name>
    <message>
        <location filename="../gui/scn_widgets/comput_settings.ui" line="219"/>
        <source>Computation settings</source>
        <translation>Paramètres généraux</translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/comput_settings.ui" line="91"/>
        <source>Reference scenario</source>
        <translation>Scénario de référence</translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/comput_settings.ui" line="97"/>
        <source>Set reference scenario:</source>
        <translation>Définir le scénario de référence:</translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/comput_settings.ui" line="335"/>
        <source>Computation output time intervals:</source>
        <translation>Sorties:</translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/comput_settings.ui" line="344"/>
        <source>Output time step:</source>
        <translation>Pas de temps de sortie (s):</translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/comput_settings.ui" line="380"/>
        <source>t max:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/comput_settings.ui" line="370"/>
        <source>t min:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/comput_settings.ui" line="26"/>
        <source>Initial conditions settings</source>
        <translation>Paramétrage des conditions initiales</translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/comput_settings.ui" line="42"/>
        <source>Start from rest</source>
        <translation>Départ au repos</translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/comput_settings.ui" line="349"/>
        <source>Duration of initial conditions settings simulation (h:m):</source>
        <translation type="obsolete">Durée de simuation des contions initiales (h:m):</translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/comput_settings.ui" line="58"/>
        <source>Rstart time:</source>
        <translation>Temps de démarrage à chaud:</translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/comput_settings.ui" line="78"/>
        <source>Hot start scenario:</source>
        <translation>Scénario de démarrage à chaud:</translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/comput_settings.ui" line="128"/>
        <source>Computation mode</source>
        <translation>Mode de calcul</translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/comput_settings.ui" line="157"/>
        <source>Fast transient</source>
        <translation>Transitoire rapide</translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/comput_settings.ui" line="137"/>
        <source>Steady state</source>
        <translation>Régime établi</translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/comput_settings.ui" line="150"/>
        <source>Gradually transient</source>
        <translation>Régime graduellement varié (dynamique)</translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/comput_settings.ui" line="237"/>
        <source>Quality only</source>
        <translation type="obsolete">Qualité seulement</translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/comput_settings.ui" line="311"/>
        <source>Output time for hotstart option:</source>
        <translation>Sorties pour démarrage à chaud:</translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/comput_settings.ui" line="255"/>
        <source>Computation time step:</source>
        <translation>Pas de temps de calcul (s):</translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/comput_settings.ui" line="242"/>
        <source>Computation duration:</source>
        <translation>Durée du calcul:</translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/comput_settings.ui" line="284"/>
        <source>Refined discretisation</source>
        <translation>Raffinement de la discrétisation</translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/comput_settings.ui" line="225"/>
        <source>Computation starting date:</source>
        <translation>Date de début du calcul:</translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/comput_settings.ui" line="291"/>
        <source>Vaporization</source>
        <translation>Vaporisation</translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/comput_settings.ui" line="64"/>
        <source>Water distribution scenario:</source>
        <translation type="obsolete">Scénario de distribution:</translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/comput_settings.ui" line="376"/>
        <source>Peak coefficient:</source>
        <translation type="obsolete">Coeff. journalier:</translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/comput_settings.ui" line="390"/>
        <source>Hydrology files</source>
        <translation>Fichiers hydrologiques</translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/comput_settings.ui" line="209"/>
        <source>Remove</source>
        <translation type="obsolete">Supprimer</translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/comput_settings.ui" line="202"/>
        <source>Add</source>
        <translation type="obsolete">Ajouter</translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/comput_settings.ui" line="167"/>
        <source>Exterior fire defense</source>
        <translation type="obsolete">Défense Exterieure Contre les Incendis</translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/comput_settings.ui" line="32"/>
        <source>Duration of initial conditions simulation (h:m):</source>
        <translation>Durée de simulation des conditions initiales (h:m)</translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/comput_settings.ui" line="110"/>
        <source>Water distribution scenario</source>
        <translation>Scénario de distribution</translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/comput_settings.ui" line="200"/>
        <source>Break criticality</source>
        <translation>Criticité des casses</translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/comput_settings.ui" line="167"/>
        <source>Exterior fire defense nominal flow</source>
        <translation>Défense extérieure contre l'incendie - débit nominal</translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/comput_settings.ui" line="190"/>
        <source>Exterior fire defense flow at 1bar</source>
        <translation>Défense extérieure contre l'incendie - débit à 1bar</translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/comput_settings.ui" line="232"/>
        <source>yyyy-MM-dd HH:mm:ss</source>
        <translation>yyyy-MM-dd HH:mm:ss</translation>
    </message>
</context>
<context>
    <name>computation_options</name>
    <message>
        <location filename="../gui/scn_widgets/comput_options.ui" line="14"/>
        <source>Computation options</source>
        <translation>Options de calcul</translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/comput_options.ui" line="297"/>
        <source>Option file</source>
        <translation>Fichier d'option</translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/comput_options.ui" line="332"/>
        <source>...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/comput_options.ui" line="20"/>
        <source>Link failure</source>
        <translation>Défaillance de raccords</translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/comput_options.ui" line="204"/>
        <source>Remove row</source>
        <translation type="obsolete">Supprimer la ligne</translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/comput_options.ui" line="241"/>
        <source>Add</source>
        <translation type="obsolete">Ajouter</translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/comput_options.ui" line="254"/>
        <source>Model</source>
        <translation>Modèle</translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/comput_options.ui" line="47"/>
        <source>Type</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/comput_options.ui" line="259"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/comput_options.ui" line="264"/>
        <source>Alp(t) curve</source>
        <translation>Courbe Alp(t)</translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/comput_options.ui" line="269"/>
        <source>Time</source>
        <translation>Temps</translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/comput_options.ui" line="127"/>
        <source>Pump failure</source>
        <translation>Défaillance de pompes</translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/comput_options.ui" line="274"/>
        <source>Mode</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/comput_options.ui" line="189"/>
        <source>Four quadrant file:</source>
        <translation>Fichier quatre quadrants:</translation>
    </message>
</context>
<context>
    <name>mainform</name>
    <message>
        <location filename="../gui/project_manager.ui" line="14"/>
        <source>Project manager</source>
        <translation>Gestionnaire de projets</translation>
    </message>
    <message>
        <location filename="../gui/project_manager.ui" line="29"/>
        <source>Model</source>
        <translation>Modèle</translation>
    </message>
    <message>
        <location filename="../gui/project_manager.ui" line="35"/>
        <source>Add model</source>
        <translation>Ajouter un modèle</translation>
    </message>
    <message>
        <location filename="../gui/project_manager.ui" line="42"/>
        <source>Delete model</source>
        <translation>Supprimer le modèle</translation>
    </message>
    <message>
        <location filename="../gui/project_manager.ui" line="49"/>
        <source>Import model</source>
        <translation>Importer un modèle</translation>
    </message>
    <message>
        <location filename="../gui/project_manager.ui" line="56"/>
        <source>Export model</source>
        <translation>Exporter le modèle</translation>
    </message>
    <message>
        <location filename="../gui/project_manager.ui" line="66"/>
        <source>Project</source>
        <translation>Projet</translation>
    </message>
    <message>
        <location filename="../gui/project_manager.ui" line="72"/>
        <source>Open project</source>
        <translation>Ouvrir le projet</translation>
    </message>
    <message>
        <location filename="../gui/project_manager.ui" line="79"/>
        <source>New project</source>
        <translation>Nouveau projet</translation>
    </message>
    <message>
        <location filename="../gui/project_manager.ui" line="93"/>
        <source>Delete project</source>
        <translation>Supprimer le projet</translation>
    </message>
    <message>
        <location filename="../gui/project_manager.ui" line="100"/>
        <source>Export project</source>
        <translation>Exporter le projet</translation>
    </message>
    <message>
        <location filename="../gui/project_manager.ui" line="107"/>
        <source>Import project</source>
        <translation>Importer un projet</translation>
    </message>
    <message>
        <location filename="../gui/project_manager.ui" line="114"/>
        <source>Update project</source>
        <translation>Mettre à jour le projet</translation>
    </message>
    <message>
        <location filename="../gui/project_manager.ui" line="153"/>
        <source>Version</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/project_manager.ui" line="158"/>
        <source>SRID</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/project_manager.ui" line="163"/>
        <source>Workspace</source>
        <translation>Espace de travail</translation>
    </message>
    <message>
        <location filename="../gui/scenario_manager.ui" line="14"/>
        <source>Scenario manager</source>
        <translation>Gestionaire de scénario</translation>
    </message>
    <message>
        <location filename="../gui/scenario_manager.ui" line="51"/>
        <source>List of scenarios</source>
        <translation>Liste des scénarios</translation>
    </message>
    <message>
        <location filename="../gui/scenario_manager.ui" line="57"/>
        <source>Clone scenario</source>
        <translation>Cloner le scénario</translation>
    </message>
    <message>
        <location filename="../gui/scenario_manager.ui" line="87"/>
        <source>Scenario settings</source>
        <translation>Paramétrage des scénarios</translation>
    </message>
    <message>
        <location filename="../gui/scenario_manager.ui" line="112"/>
        <source>Computation settings</source>
        <translation>Paramétrage des calculs</translation>
    </message>
    <message>
        <location filename="../gui/scenario_manager.ui" line="132"/>
        <source>Model ordering</source>
        <translation>Ordre des modèles</translation>
    </message>
    <message>
        <location filename="../gui/scenario_manager.ui" line="146"/>
        <source>Regulation and configuration</source>
        <translation>Configurations et régulation</translation>
    </message>
    <message>
        <location filename="../gui/scenario_manager.ui" line="156"/>
        <source>Computation options</source>
        <translation>Options de calcul</translation>
    </message>
    <message>
        <location filename="../gui/scenario_manager.ui" line="170"/>
        <source>Water quality</source>
        <translation>Qualité de l'eau</translation>
    </message>
    <message>
        <location filename="../gui/project_manager.ui" line="86"/>
        <source>Duplicate project</source>
        <translation>Dupliquer le projet</translation>
    </message>
    <message>
        <location filename="../gui/project_manager.ui" line="174"/>
        <source>Service</source>
        <translation>Service</translation>
    </message>
</context>
<context>
    <name>model_ordering</name>
    <message>
        <location filename="../gui/scn_widgets/model_ordering.ui" line="14"/>
        <source>Model ordering</source>
        <translation>Ordre des modèles</translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/model_ordering.ui" line="20"/>
        <source>Model connexion method</source>
        <translation>Méthode de connexion</translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/model_ordering.ui" line="26"/>
        <source>Global</source>
        <translation>Globale</translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/model_ordering.ui" line="39"/>
        <source>Cascade</source>
        <translation>En cascade</translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/model_ordering.ui" line="49"/>
        <source>Mixed</source>
        <translation>Mixte</translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/model_ordering.ui" line="83"/>
        <source>Cascade ordering</source>
        <translation>Ordre</translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/model_ordering.ui" line="171"/>
        <source>Groups</source>
        <translation>Groupes</translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/model_ordering.ui" line="158"/>
        <source>Models</source>
        <translation>Modèles</translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/model_ordering.ui" line="190"/>
        <source>Delete</source>
        <translation type="obsolete">Supprimer</translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/model_ordering.ui" line="292"/>
        <source>Add</source>
        <translation type="obsolete">Ajouter</translation>
    </message>
</context>
<context>
    <name>new_project_dialog</name>
    <message>
        <location filename="../gui/new_project_dialog.ui" line="14"/>
        <source>New project</source>
        <translation>Nouveau projet</translation>
    </message>
    <message>
        <location filename="../gui/new_project_dialog.ui" line="50"/>
        <source>Project name:</source>
        <translation>Nom du projet:</translation>
    </message>
    <message>
        <location filename="../gui/import_epanet.ui" line="68"/>
        <source>Input file encoding</source>
        <translation type="obsolete">Encodage du fichier</translation>
    </message>
    <message>
        <location filename="../gui/import_epanet.ui" line="82"/>
        <source>Input file:</source>
        <translation type="obsolete">Fichier:</translation>
    </message>
    <message>
        <location filename="../gui/new_project_dialog.ui" line="63"/>
        <source>2154</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/new_project_dialog.ui" line="120"/>
        <source>EPSG:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/new_project_dialog.ui" line="43"/>
        <source>Select EPSG...</source>
        <translation>Choisir EPSG...</translation>
    </message>
    <message>
        <location filename="../gui/new_model_dialog.ui" line="20"/>
        <source>Model name:</source>
        <translation>Nom du modèle:</translation>
    </message>
    <message>
        <location filename="../gui/new_project_dialog.ui" line="133"/>
        <source>...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/new_project_dialog.ui" line="26"/>
        <source>Working directory:</source>
        <translation>Répertoire de travail:</translation>
    </message>
    <message>
        <location filename="../gui/new_model_dialog.ui" line="14"/>
        <source>New model</source>
        <translation>Nouveau modèle</translation>
    </message>
    <message>
        <location filename="../gui/import_epanet.ui" line="130"/>
        <source>Project EPSG:</source>
        <translation type="obsolete">EPSG du projet:</translation>
    </message>
    <message>
        <location filename="../gui/import_epanet.ui" line="211"/>
        <source>Input file EPSG:</source>
        <translation type="obsolete">EPSG du fichier d'entrée:</translation>
    </message>
    <message>
        <location filename="../gui/new_project_dialog.ui" line="140"/>
        <source>Service</source>
        <translation>Service</translation>
    </message>
</context>
<context>
    <name>regulation_and_configuration</name>
    <message>
        <location filename="../gui/scn_widgets/regulation_and_configuration.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/regulation_and_configuration.ui" line="30"/>
        <source>Regulation settings</source>
        <translation>Fichiers de régulation</translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/regulation_and_configuration.ui" line="108"/>
        <source>Configurations</source>
        <translation>Configurations</translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/regulation_and_configuration.ui" line="120"/>
        <source>Selected for scenario</source>
        <translation>Sélectionnés pour ce scénario</translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/regulation_and_configuration.ui" line="166"/>
        <source>Available</source>
        <translation>Disponibles</translation>
    </message>
</context>
<context>
    <name>result_dialog</name>
    <message>
        <location filename="../gui/result_dialog.ui" line="20"/>
        <source>Results dialog</source>
        <translation>Résultats</translation>
    </message>
    <message>
        <location filename="../gui/result_dialog.ui" line="376"/>
        <source>X: - / Y: -</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/result_dialog.ui" line="298"/>
        <source>Scenario:</source>
        <translation>Scénario:</translation>
    </message>
    <message>
        <location filename="../gui/result_dialog.ui" line="274"/>
        <source>Model:</source>
        <translation>Modèle:</translation>
    </message>
    <message>
        <location filename="../gui/result_dialog.ui" line="318"/>
        <source>Table:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/result_dialog.ui" line="288"/>
        <source>Item:</source>
        <translation>Entité:</translation>
    </message>
    <message>
        <location filename="../gui/result_dialog.ui" line="311"/>
        <source>Result:</source>
        <translation>Résultat:</translation>
    </message>
    <message>
        <location filename="../gui/result_dialog.ui" line="238"/>
        <source>Add to left axe</source>
        <translation>Ajouter à gauche</translation>
    </message>
    <message>
        <location filename="../gui/result_dialog.ui" line="245"/>
        <source>Add to right axe</source>
        <translation>Ajouter à droite</translation>
    </message>
    <message>
        <location filename="../gui/result_dialog.ui" line="209"/>
        <source>title</source>
        <translation>titre</translation>
    </message>
    <message>
        <location filename="../gui/result_dialog.ui" line="166"/>
        <source>Show legend</source>
        <translation>Afficher la légende</translation>
    </message>
    <message>
        <location filename="../gui/result_dialog.ui" line="177"/>
        <source>hide grid</source>
        <translation>cacher la grille</translation>
    </message>
    <message>
        <location filename="../gui/result_dialog.ui" line="182"/>
        <source>left grid</source>
        <translation>grille de gauche</translation>
    </message>
    <message>
        <location filename="../gui/result_dialog.ui" line="187"/>
        <source>right grid</source>
        <translation>grille de droite</translation>
    </message>
    <message>
        <location filename="../gui/result_dialog.ui" line="192"/>
        <source>both grid</source>
        <translation>les deux grilles</translation>
    </message>
    <message>
        <location filename="../gui/result_dialog.ui" line="74"/>
        <source>Time scale</source>
        <translation>Echelle temps</translation>
    </message>
    <message>
        <location filename="../gui/result_dialog.ui" line="82"/>
        <source>hours</source>
        <translation>heures</translation>
    </message>
    <message>
        <location filename="../gui/result_dialog.ui" line="87"/>
        <source>minutes</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/result_dialog.ui" line="92"/>
        <source>seconds</source>
        <translation>secondes</translation>
    </message>
    <message>
        <location filename="../gui/result_dialog.ui" line="97"/>
        <source>calendar</source>
        <translation>calendaire</translation>
    </message>
    <message>
        <location filename="../gui/result_dialog.ui" line="216"/>
        <source>Clear graph</source>
        <translation>Effacer le graphe</translation>
    </message>
    <message>
        <location filename="../gui/result_dialog.ui" line="202"/>
        <source>Copy data to clipboad</source>
        <translation>Copier les données dans le presse papier</translation>
    </message>
    <message>
        <location filename="../gui/result_dialog.ui" line="107"/>
        <source>Load</source>
        <translation>Ouvrir</translation>
    </message>
    <message>
        <location filename="../gui/result_dialog.ui" line="114"/>
        <source>Save</source>
        <translation>Sauver</translation>
    </message>
    <message>
        <location filename="../gui/result_dialog.ui" line="345"/>
        <source>filter</source>
        <translation>filtre</translation>
    </message>
    <message>
        <location filename="../gui/result_dialog.ui" line="258"/>
        <source>Simulation</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/result_dialog.ui" line="339"/>
        <source>Measure</source>
        <translation>Mesures</translation>
    </message>
    <message>
        <location filename="../gui/result_dialog.ui" line="143"/>
        <source>◰</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/result_dialog.ui" line="148"/>
        <source>◳</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/result_dialog.ui" line="153"/>
        <source>◲</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/result_dialog.ui" line="158"/>
        <source>◱</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>table_widget</name>
    <message>
        <location filename="../gui/widgets/table_widget.ui" line="14"/>
        <source>Table widget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/widgets/table_widget.ui" line="33"/>
        <source>Delete</source>
        <translation>Supprimer</translation>
    </message>
    <message>
        <location filename="../gui/widgets/table_widget.ui" line="59"/>
        <source>...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/widgets/table_widget.ui" line="56"/>
        <source>Add</source>
        <translation>Ajouter</translation>
    </message>
</context>
<context>
    <name>time_input</name>
    <message>
        <location filename="../gui/widgets/time_input_widget.ui" line="14"/>
        <source>Time input</source>
        <translation>Définition du temps</translation>
    </message>
    <message>
        <location filename="../gui/widgets/time_input_widget.ui" line="86"/>
        <source>h :</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/widgets/time_input_widget.ui" line="112"/>
        <source>m :</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/widgets/time_input_widget.ui" line="138"/>
        <source>s</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/widgets/time_input_widget.ui" line="54"/>
        <source>+</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/widgets/time_input_widget.ui" line="59"/>
        <source>-</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>water_delivery_manager</name>
    <message>
        <location filename="../gui/water_delivery_manager.ui" line="20"/>
        <source>Water delivery scenario</source>
        <translation>Scénario de distribution</translation>
    </message>
    <message>
        <location filename="../gui/water_delivery_manager.ui" line="26"/>
        <source>Domestic water delivery scenarios</source>
        <translation>Scénarios de distribution domestique</translation>
    </message>
    <message>
        <location filename="../gui/water_delivery_manager.ui" line="32"/>
        <source>Domestic water delivery settings</source>
        <translation>Paramétrage des distributions domestiques</translation>
    </message>
    <message>
        <location filename="../gui/water_delivery_manager.ui" line="38"/>
        <source>Weekdays</source>
        <translation>Jours de semaine</translation>
    </message>
    <message>
        <location filename="../gui/water_delivery_manager.ui" line="48"/>
        <source>Weekend</source>
        <translation>Weekend</translation>
    </message>
    <message>
        <location filename="../gui/water_delivery_manager.ui" line="62"/>
        <source>Sector</source>
        <translation>Secteur</translation>
    </message>
    <message>
        <location filename="../gui/water_delivery_manager.ui" line="67"/>
        <source>Domestic volume (m3/d)</source>
        <translation>Volume domestique (m3/j)</translation>
    </message>
    <message>
        <location filename="../gui/water_delivery_manager.ui" line="72"/>
        <source>Adjustment coef.</source>
        <translation>Coef. ajustement Kj.</translation>
    </message>
    <message>
        <location filename="../gui/water_delivery_manager.ui" line="77"/>
        <source>Leak efficiency rate</source>
        <translation>Rendement</translation>
    </message>
    <message>
        <location filename="../gui/water_delivery_manager.ui" line="91"/>
        <source>Industrial water delivery settings</source>
        <translation>Paramétrage des distributions industrielles</translation>
    </message>
    <message>
        <location filename="../gui/water_delivery_manager.ui" line="116"/>
        <source>Water delivery point</source>
        <translation>Point de consommation</translation>
    </message>
    <message>
        <location filename="../gui/water_delivery_manager.ui" line="121"/>
        <source>Volume (m3/d)</source>
        <translation>Volume (m3/jour)</translation>
    </message>
    <message>
        <location filename="../gui/water_delivery_manager.ui" line="126"/>
        <source>Target node</source>
        <translation>Noeud cible</translation>
    </message>
    <message>
        <location filename="../gui/water_delivery_manager.ui" line="131"/>
        <source>Model</source>
        <translation>Modèle</translation>
    </message>
    <message>
        <location filename="../gui/water_delivery_manager.ui" line="136"/>
        <source>Industrial curve</source>
        <translation>Courbe industrielle</translation>
    </message>
</context>
<context>
    <name>water_delivery_sectors_settings</name>
    <message>
        <location filename="../gui/water_delivery_sector_settings.ui" line="26"/>
        <source>Water delivery sectors settings</source>
        <translation>Paramétrage des secteurs de distribution</translation>
    </message>
    <message>
        <location filename="../gui/water_delivery_sector_settings.ui" line="39"/>
        <source>Water delivery sectors</source>
        <translation>Secteurs de distribution</translation>
    </message>
    <message>
        <location filename="../gui/water_delivery_sector_settings.ui" line="144"/>
        <source>Water delivery nodes in sector</source>
        <translation>Noeuds des secteurs de distribution</translation>
    </message>
    <message>
        <location filename="../gui/water_delivery_sector_settings.ui" line="187"/>
        <source>Model</source>
        <translation>Modèle</translation>
    </message>
    <message>
        <location filename="../gui/water_delivery_sector_settings.ui" line="192"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../gui/water_delivery_sector_settings.ui" line="197"/>
        <source>Domestic volume (m3/d)</source>
        <translation>Volume domestique (m3/j)</translation>
    </message>
    <message>
        <location filename="../gui/water_delivery_sector_settings.ui" line="202"/>
        <source>Domestic curve</source>
        <translation>Courbe domestique</translation>
    </message>
    <message>
        <location filename="../gui/water_delivery_sector_settings.ui" line="107"/>
        <source>Nouvelle ligne</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/water_delivery_sector_settings.ui" line="112"/>
        <source>Sector name</source>
        <translation>Nom du secteur</translation>
    </message>
    <message>
        <location filename="../gui/water_delivery_sector_settings.ui" line="117"/>
        <source>Comment</source>
        <translation>Commentaire</translation>
    </message>
    <message>
        <location filename="../gui/water_delivery_sector_settings.ui" line="122"/>
        <source>Total domestic volume (m3/d)</source>
        <translation>Volume domestique total (m3/jour)</translation>
    </message>
    <message>
        <location filename="../gui/water_delivery_sector_settings.ui" line="223"/>
        <source>Hourly modulation curves</source>
        <translation>Courbes de modulation horaires</translation>
    </message>
</context>
<context>
    <name>water_quality</name>
    <message>
        <location filename="../gui/scn_widgets/water_quality.ui" line="14"/>
        <source>Water quality</source>
        <translation>Qualité de l'eau</translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/water_quality.ui" line="33"/>
        <source>Water origin</source>
        <translation>Origine de l'eau</translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/water_quality.ui" line="47"/>
        <source>Model:</source>
        <translation>Modèle:</translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/water_quality.ui" line="57"/>
        <source>Impozed piezometry singularity:</source>
        <translation type="obsolete">Singularité de piezométrie imposée:</translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/water_quality.ui" line="77"/>
        <source>Comment:</source>
        <translation>Commentaire:</translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/water_quality.ui" line="90"/>
        <source>Add</source>
        <translation type="obsolete">Ajouter</translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/water_quality.ui" line="137"/>
        <source>Model</source>
        <translation>Modèle</translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/water_quality.ui" line="142"/>
        <source>Source (Imposed piezometry)</source>
        <translation type="obsolete">Source (piezo imposée)</translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/water_quality.ui" line="152"/>
        <source>Comment</source>
        <translation>Commentaire</translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/water_quality.ui" line="145"/>
        <source>Delete</source>
        <translation type="obsolete">Supprimer</translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/water_quality.ui" line="166"/>
        <source>Trihalomethane</source>
        <translation>Trialométhane</translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/water_quality.ui" line="219"/>
        <source>Reaction parameter in water (1/hr)</source>
        <translation>Coef. de réaction dans l'eau (1/hr)</translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/water_quality.ui" line="194"/>
        <source>Concentration limit</source>
        <translation>Concentration limite</translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/water_quality.ui" line="207"/>
        <source>Chlorine</source>
        <translation>Chlore</translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/water_quality.ui" line="232"/>
        <source>Reaction speed at walls</source>
        <translation>Vitesse de réaction aux paroies</translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/water_quality.ui" line="248"/>
        <source>Travel time</source>
        <translation type="obsolete">Temps de séjour</translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/water_quality.ui" line="255"/>
        <source>Passive tracer</source>
        <translation>Traceur passif</translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/water_quality.ui" line="264"/>
        <source>Select quality output:</source>
        <translation>Type de sortie qualité:</translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/water_quality.ui" line="57"/>
        <source>Singularity type:</source>
        <translation>Type de singularité:</translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/water_quality.ui" line="67"/>
        <source>Singularity:</source>
        <translation>Singularité:</translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/water_quality.ui" line="142"/>
        <source>Source type</source>
        <translation>Type de source</translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/water_quality.ui" line="147"/>
        <source>Source</source>
        <translation>Source</translation>
    </message>
    <message>
        <location filename="../gui/scn_widgets/water_quality.ui" line="248"/>
        <source>Travel and contact time</source>
        <translation>Temps de séjour et de contact</translation>
    </message>
</context>
</TS>
