from qgis.PyQt.QtCore import QSettings
import os
from collections import defaultdict

"""
global expresseau service definition
"""

def get_service():
    settings = QSettings()
    if not settings.contains('expresseau/service') or settings.value('expresseau/service') not in services():
        settings.setValue('expresseau/service', 'expresseau') # does not work on test server, maybe because QApplication is not instanciated
    return settings.value('expresseau/service') or 'expresseau' # for tests the value of settings isn't changed, the or 'expresseau' is important here

def set_service(value):
    settings = QSettings()
    settings.setValue('expresseau/service', value)

def services():
    pg_service_file = os.environ['PGSERVICEFILE'] if 'PGSERVICEFILE' in os.environ else (os.path.expanduser('~')+os.sep+".pg_service.conf")
    """default db admin operations is postgres unless a database is specified in the srevice"""
    pg_service_file = os.environ['PGSERVICEFILE'] if 'PGSERVICEFILE' in os.environ else (os.path.expanduser('~')+os.sep+".pg_service.conf")
    srv = defaultdict(dict)
    with open(pg_service_file) as f:
        for ll in f:
            ll = ll.strip()
            if ll.startswith('['):
                current_service = ll.replace('[','').replace(']','')
            elif ll.find('=') and len(ll.split('='))==2:
                k, v = ll.split('=')
                srv[current_service][k.strip()] = v.strip()
            else:
                pass
    return srv

def get_dbname():
    current_service = get_service()
    srv = services()
    if current_service in srv and 'dbname' in srv[current_service]:
        return srv[current_service]['dbname']
    else:
        return 'postgres'


