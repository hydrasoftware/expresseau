################################################################################################
##                                                                                            ##
##     This file is part of EXPRESSEAU, a QGIS plugin for hydraulics                          ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

"""

import controls dans rules of an EPANET inp file to expresseau ctl file

usage : python -m expresseau.database.inp_to_ctl [-e <encoding>] project_name model_name inp_file

"""

import sys
import os
import re
from collections import defaultdict

def tokenize(row, length=None):
    row = row.split(';')[0]
    row = row.strip().split()
    if length is None:
        return row
    else:
        return row + [None]*(length-len(row))

def format_rule(new_set_variables, new_conditions, new_actions, new_actions_alter):

    def two_conditions(conditions, op):
        '''Scinde les conditions en groupes de 2 avec l'opérateur donné (AND ou OR)'''
        morceaux = [conditions[i:i + 2] for i in range(0, len(conditions), 2)]
        return [f" {(' ) ' + op + ' ( ').join(m)} " for m in morceaux]

    def format_cond_linked_by_or(list_or, new_actions, new_actions_alter):
        txt = ""
        or_groups = [two_conditions(list_or, "OR")][0]
        for i, c in enumerate(or_groups):
            txt += f"IF ( {c} )  THEN\n"
            indentation = "  "
            txt += "\n"
            txt += "\n".join([f'    {action}' for action in new_actions]) + "\n"
            txt += "ENDIF\n\n"
        if len(new_actions_alter): # si il y a un ELSE : la condition c'est un "and not" de toutes les conditions donc if imbriqué.
            feeback.PushWarning("translation of rules with several conditions, linked by OR and with an ELSE after, not implemented")
            assert(False) # not implemented

        return txt

    def format_cond_linked_by_and(list_and, new_actions, new_actions_alter):
        txt = ""
        and_groups = [two_conditions(list_and, "AND")][0]
        for i, c in enumerate(and_groups):
            indentation = "  "*(i)
            txt += f"{indentation}IF ( {c} ) THEN\n"
            txt += "\n"
            if i==len(and_groups)-1:
                txt += "\n".join([f'    {action}' for action in new_actions]) + "\n"
                txt += "\n"
            if len(new_actions_alter):
                txt += f"\n{indentation}ELSE\n\n"
                txt += "\n".join([f'    {action}' for action in new_actions_alter]) + "\n"
        for i, c in enumerate(and_groups):
            indentation = "  "*(len(and_groups)-i-1)
            txt += f"{indentation}ENDIF\n\n"

        return txt

    def keep_cond_linked_by_or_and(new_conditions, new_actions, new_actions_alter):
        txt = ""
        actions_alter = []
        new_actions_alter = []
        txt += f'IF ( {" ) AND ( ".join([" ) OR ( ".join(c) for c in new_conditions])} ) THEN' + "\n\n"
        txt += "\n".join([f'    {action}' for action in new_actions]) + "\n\n"
        if len(new_actions_alter):
            txt += "\nELSE\n\n"
            txt += "\n".join([f'    {action}' for action in new_actions_alter]) + "\n\n"
        txt += "ENDIF\n\n"

        return txt

    ctl_txt = ""
    for set_var in new_set_variables:
        if len(set_var):
            ctl_txt += set_var
            ctl_txt += "\n\n"
    nb_cond_and = len(new_conditions)
    nb_cond_if = max(len(c) for c in new_conditions)
    if nb_cond_and > 2 and nb_cond_if >= 2:
        feedback.pushWarning("ATTENTION : Plus de 2 conditions liées par des AND et des OR. Traduction à reformuler dans le fichier ctl.")
        ctl_txt += "!ATTENTION : Plus de 2 conditions liées par des AND et des OR. Traduction à reformuler :\n"
        ctl_txt += keep_cond_linked_by_or_and(new_conditions, new_actions, new_actions_alter)
    elif nb_cond_and > 2:
        l = [c[0] for c in new_conditions]
        ctl_txt += format_cond_linked_by_and(l, new_actions, new_actions_alter)
    else :
        ctl_txt += format_cond_linked_by_or(new_conditions[0], new_actions, new_actions_alter)

    return ctl_txt

trad = {
    '=' : 'EQ',
    'IS' : 'EQ',
    'BELOW' : 'LT',
    '<': 'LT',
    '<=' : 'LT',
    '=<' : 'LT',
    'ABOVE' : 'GT',
    '>': 'GT',
    '>=' : 'GT',
    '=>' : 'GT',
    'OPEN' : '1.',
    'CLOSE' : '0.',
    'CLOSED' : '0.'
}

def read_a_rule(rule, order, project, comments, feedback):
    warnings = 0
    errors = 0
    res = ""
    var_id = 1

    if len(comments):
        res = "\n".join(comments) + "\n\n"
    res += "!" + rule[0] + "\n\n"

    # RULE ID Commande
    # IF condition_1
    # AND condition_2
    # OR condition_3
    # AND condition_4
    # etc.
    # THEN action_1
    # AND action_2
    # etc.
    # ELSE action_3
    # AND action_4
    # etc.
    # PRIORITY Priorité

    cmd =re.sub(r' +', ' ', " ".join(rule[1:]))
    conditions, actions = re.split(' THEN ', cmd, flags=re.I)
    assert(conditions.upper().startswith('IF '))
    conditions = conditions[3:]
    conditions = re.split(' AND ', conditions, flags=re.I)
    splt = re.split(' PRIORITY ', actions, flags=re.I)
    assert(len(splt)<=2)
    if len(splt) ==2:
        actions = splt[0]
        priority = splt[1]
    else:
        priority = 9999

    splt = re.split(' ELSE ', actions, flags=re.I)
    actions = re.split(' AND ', splt[0], flags=re.I)
    if len(splt)>2:
        feedback.reportError(f"Error in epanet file syntax : {actions}")
        assert(len(splt)<=2)
    if len(splt)==2:
        actions_alter = re.split(' AND ', splt[1], flags=re.I)
    else:
        actions_alter = []
    new_conditions = []
    new_set_variables = []

    for cond in conditions:
        new_conditions.append([])

        for condition in re.split(" OR ", cond, flags=re.I):

            obj_type, obj_id = tokenize(condition)[0:2]
            obj_type = obj_type.upper()

            if obj_type=="NODE":
                attribut, op, value = tokenize(condition.upper())[2:]
                if attribut=="DEMAND":
                    feedback.reportError('translation of rules with "DEMAND" on condition not implemented')
                    assert(False) # translation of rules with "DEMAND" on condition not implemented
                elif attribut=="HEAD":
                    new_conditions[-1].append(f"ZAM {obj_id} {trad[op]} {value} ")
                elif attribut=="LEVEL":
                    new_conditions[-1].append(f"HAM {obj_id} {trad[op]} {value} ")
                elif attribut=="PRESSURE":
                    new_conditions[-1].append(f"HAM {obj_id} {trad[op]} {value} ")
                else:
                    feedback.reportError(f"Unknown attibut {attribut} in rule : {cmd}")
                    errors += 1

            if obj_type=="JUNCTION":
                feedback.reportError('translation of rules with "JUNCTION" on condition not implemented')
                assert(False) # translation of rules with "JUNCTION" on condition not implemented

            if obj_type=="RESERVOIR":
                feedback.reportError(f"Translation of rules with RESERVOIR on condition not implemented")
                feedback.reportError(f"Verify synthaxe of your inp file : is it a RESERVOIR or a TANK ?")
                assert(False) # translation of rules with "RESERVOIR" on condition not implemented

            if obj_type=="TANK":
                attribut, op, value = tokenize(condition.upper())[2:]
                sz, = project.fetchone("select sz_array from api.reservoir_node where name=%s", (obj_id,))
                z_fond = sz.replace('{', '').replace('}', '').split(',')[0]
                new_value = float(value) + float(z_fond)

                if attribut=="LEVEL" or attribut=="PRESSURE":
                    new_conditions[-1].append(f"HAM {obj_id} {trad[op]} {value} ")
                elif attribut=="PRESSURE":
                    new_conditions[-1].append(f"HAM {obj_id} {trad[op]} {value} ")
                elif attribut=="FILLTIME":
                    feedback.reportError('translation of rules with "TANK FILLTIME" on condition not implemented')
                    assert(False) # translation of rules with "TANK FILLTIME" on condition not implemented
                elif attribut=="DRAINTIME":
                    feedback.reportError('translation of rules with "TANK DRAINTIME" on condition not implemented')
                    assert(False)  # translation of rules with "TANK DRAINTIME" on condition not implemented
                else:
                    feedback.reportError(f'translation of rules with attribut "{attribut}" for TANK not implemented')
                    assert(False)  # there is a rule with a TANK attribut that is not implemented

            if obj_type=="LINK" or obj_type=="PIPE" or obj_type=="PUMP" or obj_type=="VALVE":
                attribut, op, value = tokenize(condition.upper())[2:]
                #link_type, = project.fetchone("select exportcode from ___.link l join ___.type_link t on t.name=l._type where l.name=%s", (obj_id,))

                if attribut=="FLOW":
                    new_conditions[-1].append(f"QLIAIS {obj_id} {trad[op]} {value} ")
                elif attribut=="STATUS":
                    # ajout d'un SET variable
                    new_set_variables.append(f"SET %var_{var_id} = {link_type} '{obj_id}' ! translation of STATUS")
                    if value=='OPEN' or value=='CLOSED':
                        new_conditions[-1].append(f"%var_{var_id} EQ {trad[value]}")
                    else:
                        feedback.reportError(f'only translation "STATUS" "OPEN" or "CLOSED" for pump. {value} not recognize.')
                        assert(False) # only translation "STATUS" for "OPEN" or "CLOSED" for pump
                    var_id += 1
                elif attribut=="SETTINGS":
                    if obj_type!="PUMP" and obj_type!="VALVE":
                        feedback.reportError(f'only translation "SETTINGS" for PUMP and VALVE. {obj_type} SETTINGS not recognize.')
                        assert(False) # only translation "SETTINGS" for PUMP and VALVE
                    new_set_variables.append(f"SET %var_{var_id} = {link_type} '{obj_id}' ! translation of SETTINGS")
                    new_conditions[-1].append(f"%var_{var_id} {trad[op]} {value}")
                    var_id += 1

            if obj_type=="SYSTEM":
                attribut = tokenize(condition.upper())[1]

                if attribut=="DEMAND":
                    feedback.reportError('translation of rules with "DEMAND" on condition not implemented')
                    assert(False)   # translation of rules with "DEMAND" on condition not implemented

                if attribut=="TIME":
                    op, time = tokenize(condition.upper())[2:]
                    if time.find(":") != -1: #si format en HH:MM, conversion en décimale
                        time = float(time.split(":")[0])+float(time.split(":")[1])/60
                    else:
                        time=float(time)
                    new_conditions[-1].append(f"TIME {trad[op]} {time}")

                if attribut=="CLOCKTIME":
                    op, clocktime = tokenize(condition.upper())[2:4]
                    am_or_pm = tokenize(condition)[4] if len(tokenize(condition)) > 4 else ''
                    splt = clocktime.split(':')
                    h, m = splt if len(splt)==2 else (splt[0], 0)
                    time = float(h) + float(m)/60 + (12 if am_or_pm=='PM' else 0)
                    new_conditions[-1].append(f"TIME {trad[op]} {time}")

    new_actions = []
    new_actions_alter = []

    for act, new_act in ((actions, new_actions), (actions_alter, new_actions_alter)):

        for action in act:

            obj_type, obj_id, param, op, status = tokenize(action)
            obj_type = obj_type.upper()
            param = param.upper()
            op = op.upper()
            status = status.upper()

            if obj_type=="LINK" or obj_type=="PIPE":
                link_type, = project.fetchone("select exportcode from ___.link l join ___.type_link t on t.name=l._type where l.name=%s", (obj_id,))

                if obj_type=="PIPE" or link_type == "CANA":
                    CV_name_if_exists = obj_id+'_CV' if len(obj_id)<21 else obj_id[0:20]+'_CV'
                    CV_exists = project.fetchone("select count(*) from ___.link where name = %s", (CV_name_if_exists,))[0] > 0
                    final_obj_id = CV_name_if_exists if CV_exists else obj_id

                if param=="STATUS":
                    if obj_type=="PIPE" or link_type == "CANA":
                        new_act.append( f"{link_type} '{final_obj_id}'")
                        new_act.append( f"STATUT {trad[status]}\n")
                    else:
                        feedback.pushWarning(f"verify translation of : {action}")
                        new_act.append( f"! Traduction à vérifier : ")
                        new_act.append( f"{link_type} '{obj_id}'")
                        new_act.append( f"STATUT {trad[status]}\n")

                elif param=="FLOW":
                    feedback.pushWarning(f"verify translation of : {action}")
                    new_act.append( f"! Traduction à vérifier : ")
                    new_act.append( f"{link_type} '{obj_id}'")
                    new_act.append( f"FLOW {status}\n")

                elif param=="SETTING":
                    feedback.pushWarning(f"verify translation of : {action}")
                    new_act.append( f"! Traduction à vérifier : ")
                    new_act.append( f"{link_type} '{obj_id}'")
                    new_act.append( f"SETTING {status}\n")

            elif obj_type=="PUMP":

                if param=="STATUS":
                    new_act.append( f"POMP '{obj_id}'")
                    if status in ("OPEN", "CLOSE", "CLOSED"):
                        new_act.append( f"ALPR {trad[status]}\n")
                    else:
                        new_act.append( f"ALPR {status}\n")

                elif param=="FLOW" or param=="SETTING":
                    new_act.append( f"POMP '{obj_id}'")
                    if status in ("OPEN", "CLOSE", "CLOSED"):
                        new_act.append( f"ALPR {trad[status]}\n")
                    else:
                        new_act.append( f"QCONS {status}\n")


            elif obj_type=="VALVE": # une valve est devenue une reg_p, des reg_q ou une valve
                link_type, = project.fetchone("select exportcode from ___.link l join ___.type_link t on t.name=l._type where l.name=%s", (obj_id,))
                if link_type=="REDP":
                    link_type = "REGP"
                if link_type=="VANNE":
                    link_type = "VANN"
                if param=="STATUS":
                    new_act.append( f"{link_type} '{obj_id}'")
                    new_act.append( f"ALPS {trad[status]}\n")  # ok pour les 3 types de vannes
                elif param=="FLOW":
                    if link_type != "REGQ":
                        feedback.reportError("translation of actions with VALVE FLOW possible only with a REGQ")
                        errors += 1
                    else :
                        new_act.append( f"{link_type} '{obj_id}'")
                        new_act.append( f"QCONS {status}\n")
                elif param=="SETTING":
                    new_act.append( f"{link_type} '{obj_id}'")
                    if link_type=="REGP":
                        new_act.append( f"HCONS {status}\n")
                    elif link_type=="REGQ":
                        new_act.append( f"QCONS {status}\n")
                    else:
                        feedback.pushWarning(f"verify translation of : {action}")
                        new_act.append( f"! Traduction à vérifier : ")
                        new_act.append( f"SETTING {status}\n")

            else:
                feedback.reportError(f"Unknown object type {obj_type} in this action : {action}")
                errors += 1

    res += format_rule(new_set_variables, new_conditions, new_actions, new_actions_alter)

    return int(priority), 9999-order, res, warnings, errors


def import_(project, model, path, feedback, enc):
    errors = 0
    warnings = 0
    nb_controls = 0
    nb_rules = 0
    with open(os.path.join(project.directory, 'data', f"{model}.ctl"), "w", encoding='utf-8') as ctl , open(path, encoding=enc, errors='replace') as inp:
        ctl.write(f"$ case ({model})"+ "\n\n\n")
        comments = []
        previous_comments = []
        rule = []
        rules = []
        set_niv_list = []
        for line in inp:
            line = line.strip()

            if not len(line):
                continue
            line = re.sub(r" +", " ", line)

            m = re.search(r'^\[([A-Z]+)\]', line)
            if m:   # si il y a texte dans des crochets
                section = m.group(1).upper()
                if section in ('CONTROLS', 'RULES'):
                    ctl.write("\n" + "! **** Section " + section + " ****" + "\n" + "\n")
                comments = []
                continue

            if line.startswith(';') :
                comments.append(re.sub(r'^;', '!', line))
                continue

            if section=='CONTROLS':

                ctl.write("\n" + "\n".join(comments) + "\n" + "\n")

                if line.upper().startswith('LINK') or line.upper().startswith('PIPE') or line.upper().startswith('PUMP') or line.upper().startswith('VALVE'):
                    nb_controls += 1
                    arc_id, state = tokenize(line)[1:3]
                    state = state.upper()
                    link_type, = project.fetchone("select exportcode from ___.link l join ___.type_link t on t.name=l._type where l.name=%s", (arc_id,))

                    if line.upper().startswith('PIPE') and link_type!='CANA'\
                            or line.upper().startswith('PUMP') and link_type!='POMP'\
                            or line.upper().startswith('VALVE') and link_type!='VANNE':
                        raise Exception(f"Incoherent link type for '{arc_id}' at line : \n {line} \n")

                    if re.search(r"IF NODE", line, re.I):
                        node_id, op, value = tokenize(line)[5:]
                        op = op.upper()
                        value = value.upper()
                        node_type, = project.fetchone("select exportcode from ___.node n join ___.type_node t on t.name=n._type where n.name=%s", (node_id,))

                        if node_type=="USER_NODE":
                            z_nod, = project.fetchone("select z_ground - depth from api.user_node where name=%s", (node_id,))
                            if node_id not in set_niv_list:
                                set_niv_list.append(node_id)
                                ctl.write(f"set %niv = ZAM '{node_id}'" + "\n")
                        elif node_type=="RESERVOIR_NODE":
                            z_nod, = project.fetchone("select (sz_array::real[])[1][1] from api.reservoir_node where name=%s", (node_id,))
                            if node_id not in set_niv_list:
                                set_niv_list.append(node_id)
                                ctl.write(f"set %niv = CHAT '{node_id}'" + "\n"+ "\n")
                        else:
                            raise Exception(f"unknown node type '{node_type}'")

                        if link_type=='VANNE':
                            ctl.write(f"VANN '{arc_id}'" + "\n")
                            ctl.write(f"IF ( %niv {trad[op]} {float(value)+z_nod} ) THEN ( ALPS {trad[state]} )" + "\n")
                        elif link_type=='POMP':
                            ctl.write(f"{link_type} '{arc_id}'" + "\n")
                            ctl.write(f"IF ( %niv {trad[op]} {float(value)+z_nod} ) THEN ( ALPR {trad[state]} )" + "\n")
                        elif link_type=='CANA':
                            ctl.write(f"{link_type} '{arc_id}'" + "\n")
                            ctl.write(f"IF ( %niv {trad[op]} {float(value)+z_nod} ) THEN ( STATUT {trad[state]} )" + "\n")
                        elif link_type=='REDP':
                            ctl.write(f"REGP '{arc_id}'" + "\n")
                            ctl.write(f"IF ( %niv {trad[op]} {float(value)+z_nod} ) THEN ( HCONS {state} )" + "\n")
                        elif link_type=='REGQ':
                            ctl.write(f"{link_type}'{arc_id}'" + "\n")
                            ctl.write(f"IF ( %niv {trad[op]} {float(value)+z_nod} ) THEN ( QCONS {state} )" + "\n")

                    if re.search(r"AT TIME", line, re.I):

                        if re.search(r"AT TIME", line, re.I):
                            time = tokenize(line)[5]
                            if time.find(":") != -1: #si format en HH:MM, conversion en décimale
                                time = float(time.split(":")[0])+float(time.split(":")[1])/60
                            else:
                                time=float(time)

                        if re.search(r"AT CLOCKTIME", line, re.I):
                            clocktime = tokenize(line)[5]
                            am_or_pm = tokenize(line)[6] if len(tokenize(line))>6 else ''
                            #TODO soustraire l'heure de début de simulation + demander un modulo à TLP
                            splt = clocktime.split(':')
                            h, m = splt if len(splt)==2 else (splt[0], 0)
                            time = float(h) + float(m)/60 + (12 if am_or_pm=='PM' else 0)

                        if link_type=='VANNE':
                            ctl.write(f"VANN '{arc_id}'" + "\n")
                            ctl.write(f"IF ( TIME GT {time:.2f} ) THEN ( ALPS {trad[state]} )" + "\n")
                        elif link_type=='POMP':
                            ctl.write(f"{link_type} '{arc_id}'" + "\n")
                            ctl.write(f"IF ( TIME GT {time:.2f} ) THEN ( ALPR {trad[state]} )" + "\n")
                        elif link_type=='CANA':
                            ctl.write(f"{link_type} '{arc_id}'" + "\n")
                            ctl.write(f"IF ( TIME GT {time:.2f} ) THEN ( STATUT {trad[state]} )" + "\n")
                        elif link_type=='REDP':
                            ctl.write(f"REGP '{arc_id}'" + "\n")
                            ctl.write(f"IF ( TIME GT {time:.2f} ) THEN ( HCONS {state} )" + "\n")
                        elif link_type=='REGQ':
                            ctl.write(f"REGQ '{arc_id}'" + "\n")
                            ctl.write(f"IF ( TIME GT {time:.2f} ) THEN ( QCONS {state} )" + "\n")

                    comments = []

                else: # if not line.startswith('LINK')
                    feedback.pushWarning(f"Line not translate in CONTROLS section : \n {line} \n")
                    ctl.write(f"! not translate : {line}" + "\n")

            if section=='RULES':
                if line.upper().startswith('RULE'):
                    nb_rules += 1
                    if len(rule): #la première fois, rule est vide car on a juste lu [RULES]
                        # au 'RULE' suivant on traiet la rule qu'on vient de finir de lire.
                        rules.append(read_a_rule(rule, len(rules), project, previous_comments, feedback))
                        previous_comments = comments
                        comments = []
                        rule = []

                rule.append(line) # on ajoute les lignes une par une

        # quand on sort de la section 'RULES', on traite la dernière rule
        if len(rule):
            rules.append(read_a_rule(rule, len(rules), project, previous_comments, feedback))

        for p, l, r, e, w in reversed(sorted(rules)):
            warnings += w
            errors += e
            ctl.write(r)

        feedback.pushInfo(f"Controls and rules translated :  \n     {nb_controls} control(s) translated \n     {nb_rules} rule(s) translated \n")

        return warnings, errors

if __name__=='__main__':
    import sys
    from expresseau.project import Project
    from expresseau.utility.log import Feedback
    import getopt

    optlist, args = getopt.getopt(sys.argv[1:], 'e:', ["encoding="])
    optlist = dict(optlist)
    project_name, model, path = args[-3:]
    enc = optlist['-e'] if '-e' in optlist else 'utf-8'
    project = Project(project_name, debug=False)
    import_(project, model, path, Feedback(), enc)