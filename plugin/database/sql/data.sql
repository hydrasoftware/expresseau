------------------------------------------------------------------------------------------------
--                                                                                            --
--     This file is part of EXPRESSEAU, a QGIS plugin for hydraulics                          --
--     (see <http://hydra-software.net/>).                                                    --
--                                                                                            --
--     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      --
--     of Setec Hydratec, Paris.                                                              --
--                                                                                            --
--     Contact: <contact@hydra-software.net>                                                  --
--                                                                                            --
--     You can use this program under the terms of the GNU General Public                     --
--     License as published by the Free Software Foundation, version 3 of                     --
--     the License.                                                                           --
--                                                                                            --
--     You should have received a copy of the GNU General Public License                      --
--     along with this program. If not, see <http://www.gnu.org/licenses/>.                   --
--                                                                                            --
------------------------------------------------------------------------------------------------

create extension if not exists postgis;

create schema ___;

comment on schema ___ is 'tables schema for expresseau, admin use only';

create or replace function ___.unique_name(concrete varchar, abstract varchar default null, abbreviation varchar default null)
returns varchar
language plpgsql volatile as
$$
declare
    mx integer;
    nxt integer;
    seq varchar;
    tab varchar;
begin
    if abbreviation is null then
        execute format('(select abbreviation from ___.type_%2$s where name=''%1$s'')', concrete, abstract) into abbreviation;
    end if;
    tab := concrete||coalesce('_'||abstract, '');
    seq := tab||'_name_seq';
    nxt := nextval('___.'||seq);
    execute format('with substrings as (
                        select substring(name from ''%2$s_(.*)'') as s
                        from ___.%1$I
                        where name like ''%2$s_%%''
                    )
                    select coalesce(max(s::integer), 1) as mx
                    from substrings
                    where s ~ ''^\d+$'' ',
                    tab, abbreviation) into mx;

    if nxt < mx then
        nxt := mx + 1;
        perform setval('___.'||seq, mx+1);
    end if;
    return abbreviation||'_'||nxt::varchar;
end;
$$
;

------------------------------------------------------------------------------------------------
-- METADATA                                                                                   --
------------------------------------------------------------------------------------------------

create table ___.metadata(
    id integer primary key default 1 check (id=1), -- only one row
    creation_date timestamp not null default now(),
    srid integer references spatial_ref_sys(srid) default 2154,
    unique_name_per_model boolean default true
);

insert into ___.metadata default values;

------------------------------------------------------------------------------------------------
-- ENUMS                                                                                      --
------------------------------------------------------------------------------------------------

create type ___.return_valve_mode as enum ('Regulated', 'Closed', 'Open');

create type ___.valve_closing_mode as enum ('Instantaneous', 'Progressive');

create type ___.mixing_mode as enum ('Perfect mixing', 'Piston flow LIFO', 'Piston flow FIFO', 'Two chambers mixing');

create type ___.alim_mode as enum ('From bottom', 'From top');

create type ___.pressure_regulation_option as enum ('Constant pressure', 'Curve P(Q)');

create type ___.pressure_regulation_mode as enum ('Imposed differential pressure', 'Imposed upstream pressure', 'Imposed downstream pressure');

create type ___.pump_regulation_option as enum ('No regulation', 'Flow regulation', 'Stop-start regulation', 'Pressure regulation', 'Water level in reservoir');

create type ___.pump_failure_mode as enum ('Automatic alpha computation', 'Alpha(t) curve', 'Four quadrants curve');

create type ___.week_period as enum ('Weekday', 'Weekend', 'Auto');

create type ___.pipe_type_singularity as enum ('Bend', 'Borda headloss');

create type ___.computation_mode as enum (
    'Steady state',
    'Gradual transient',
    'Fast transient',
    'Exterior fire defense nominal flow',
    'Break criticality',
    'Exterior fire defense flow at 1bar'
    );

create type ___.model_connection_settings as enum ('Cascade', 'Global', 'Mixed');

create type ___.model_connect_mode as enum ('Imposed piezometry Z(t)', 'Delivery Q(t)');

create type ___.quality_output as enum ('None', 'Tracer', 'Water origin');


------------------------------------------------------------------------------------------------
-- MODELS                                                                                     --
------------------------------------------------------------------------------------------------

create table ___.model(
    name varchar(16) primary key default ___.unique_name('model', abbreviation=>'model') check(not name~' ' and octet_length(name)<=16),
    creation_date timestamp not null default current_date,
    comment varchar
);


------------------------------------------------------------------------------------------------
-- Settings                                                                                   --
------------------------------------------------------------------------------------------------

create table ___.fluid_properties(
    id varchar primary key default 'Fluid properties' check (id='Fluid properties'), -- only one row
    volumic_mass_kg_m3 real not null default 1000,
    temperature_c real not null default 10,
    kinematic_viscosity_m2_s double precision not null default 1.30e-6,
    volumic_elasticity_module_N_m2 double precision not null default 2.05e9
);

insert into ___.fluid_properties default values;

create table ___.material(
    name varchar primary key default ___.unique_name('material', abbreviation=>'custom_material'),
    roughness_mm real not null default 0.500,
    elasticity_N_m2 double precision not null default 2.00e11
);

insert into ___.material(name, roughness_mm, elasticity_N_m2) values
('Unknown',                     0.500, 2.00e11),
('Ductile iron',                0.050, 1.70e11),
('Grey cast iron',              0.100, 1.70e11),
('Steel',                       0.050, 2.20e11),
('Reinforced concrete',         0.100, 2.50e10),
('Asbestos ciment',             0.100, 2.28e10),
('Polyvinyl chloride',          0.025, 3.00e09),
('High density polyethylene',   0.025, 1.20e09),
('Low density polyethylene',    0.025, 2.00e08);

create or replace function ___._array_sum(arr real[], col integer default 1)
returns real
language sql immutable as
$$
select sum(values)::real from (select unnest(arr[:][col:col]) as values) as arr
$$
;

create table ___.hourly_modulation_curve(
    name varchar(24) primary key default ___.unique_name('hourly_modulation_curve', abbreviation=>'CURV') check(not name~' ' and octet_length(name)<=24),
    comment varchar,
    hourly_modulation_array real[] default array_fill(1, array[24, 2]) check(array_length(hourly_modulation_array, 1)=24 and array_length(hourly_modulation_array, 2)=2),
        check(round(___._array_sum(hourly_modulation_array, 1)::numeric, 4)::real = 24::real),
        check(round(___._array_sum(hourly_modulation_array, 2)::numeric, 4)::real = 24::real)
);

create table ___.failure_curve(
    name varchar(24) primary key default ___.unique_name('failure_curve', abbreviation=>'ALP') check(not name~' ' and octet_length(name)<=24),
    comment varchar,
    alpt_array real[10][2] default '{{0, 0}, {10, 1}}'::real[] check(array_length(alpt_array, 1)>1 and array_length(alpt_array, 1)<=10 and array_length(alpt_array, 2)=2)
);

create table ___.threshold_warning_settings(
    pipe_maximum_speed real not null default 2,
    pipe_minimum_speed real not null default 0.3,
    node_maximum_pressure real not null default 100,
    node_minimum_pressure real not null default -50,
    minimum_service_pressure real not null default 20,
    id varchar primary key default 'Warnings' check (id='Warnings') -- only one row
);

insert into ___.threshold_warning_settings default values;

create table ___.sectorization_settings(
    pipe_link_meter boolean default true,
    valve_link_open boolean default true,
    pressure_regulator_link boolean default false,
    flow_regulator_link boolean default false,
    reservoir_node boolean default false,
    id varchar primary key default 'Sectorization' check (id='Sectorization') -- only one row
);

insert into ___.sectorization_settings default values;


------------------------------------------------------------------------------------------------
-- NODES                                                                                      --
------------------------------------------------------------------------------------------------

create type ___.type_node_ as enum ('user', 'reservoir');

create table ___.type_node(
    name ___.type_node_,
    exportid integer,
    exportcode varchar,
    abbreviation varchar,
    comment varchar,
        primary key (name, exportid, exportcode, abbreviation)
);

insert into ___.type_node(name, exportid, exportcode, abbreviation) values
('user',        1,  'USER_NODE',            'NOD'),
('reservoir',   2,  'RESERVOIR_NODE',       'RES');

create table ___.node(
    id serial primary key,
    name varchar(24) not null check(not name~' ' and octet_length(name)<=24),
    model varchar not null references ___.model(name) on update cascade on delete cascade,
    _type ___.type_node_ not null,
    comment varchar,
    zground real not null default 9999,
    geom geometry('POINTZ', 2154) unique not null,
        unique (name, model),
        unique (id, name, _type),
        check(ST_Z(geom)::real <= zground)
);

create index node_geomidx on ___.node using gist(geom);
create index node_typeidx on ___.node(_type);

create table ___.user_node(
    id integer primary key,
    name varchar default ___.unique_name('user', 'node'),
    _type ___.type_node_ default 'user' check(_type='user'),
    domestic_curve varchar references ___.hourly_modulation_curve(name) on update cascade on delete set null,
    industrial_curve varchar references ___.hourly_modulation_curve(name) on update cascade on delete set null,
    q0 real not null default 0,
        foreign key (id, name, _type) references ___.node(id, name, _type) on update cascade on delete cascade,
        unique(id, name, _type)
);


create table ___.reservoir_node(
    id integer primary key,
    name varchar default ___.unique_name('reservoir', 'node'),
    _type ___.type_node_ not null default 'reservoir' check(_type='reservoir'),
    sz_array real[10][2] not null default '{{0, 0}}'::real[] check(array_length(sz_array, 1)>0 and array_length(sz_array, 1)<=10 and array_length(sz_array, 2)=2),
    alim_mode ___.alim_mode not null default 'From bottom',
    z_ini real not null,
    z_overflow real not null,
    return_in_valve_mode ___.return_valve_mode not null default 'Regulated',
    return_out_valve_mode ___.return_valve_mode not null default 'Regulated',
    mixing_mode ___.mixing_mode not null default 'Perfect mixing',
    reset_travel_time boolean not null default false,
        foreign key (id, name, _type) references ___.node(id, name, _type) on update cascade on delete cascade,
        check(z_ini >= sz_array[1][1]),
        unique(id, name, _type)
);


------------------------------------------------------------------------------------------------
-- SINGULARITIES                                                                              --
------------------------------------------------------------------------------------------------

create type ___.type_singularity_ as enum ('surge_tank', 'pressure_accumulator',
'air_relief_valve', 'imposed_piezometry', 'flow_injection', 'chlorine_injection', 'model_connection');

create table ___.type_singularity(
    name ___.type_singularity_,
    exportid integer,
    exportcode varchar,
    abbreviation varchar,
    comment varchar,
        primary key (name, exportid, exportcode, abbreviation)
);

insert into ___.type_singularity(name, exportid, exportcode, abbreviation) values
('surge_tank',              10, 'TANK',     'TANK'),
('pressure_accumulator',    11, 'BALL',     'BALL'),
('air_relief_valve',        12, 'VNTS',     'AIRV'),
('imposed_piezometry',      13, 'PRES',     'IMP'),
('flow_injection',          14, 'EQAPP',    'QINJ'),
('chlorine_injection',      15, 'ECHLORE',  'CINJ'),
('model_connection',        16, 'RACC',     'RACC');

create table ___.singularity(
    id integer primary key references ___.node(id) on update cascade on delete cascade,
    name varchar(24) not null check(not name~' ' and octet_length(name)<=24),
    _type ___.type_singularity_ not null,
    comment varchar,
        unique (id, name, _type)
);

create index singularity_typeidx on ___.singularity(_type);


create table ___.surge_tank_singularity(
    id integer primary key,
    name varchar default ___.unique_name('surge_tank', 'singularity'),
    _type ___.type_singularity_ default 'surge_tank' check(_type='surge_tank'),
    sz_array real[10][2] not null default '{{0, 0}}'::real[] check(array_length(sz_array, 1)>0 and array_length(sz_array, 1)<=10 and array_length(sz_array, 2)=2),
    zini real not null default 0,
    pressurized_option boolean not null default false,
    orifice_diameter real not null check (orifice_diameter >=0),
    in_headloss_coef real not null default 1.5 check(in_headloss_coef >=0),
    out_headloss_coef real not null default 1.5 check(out_headloss_coef >=0),
        foreign key (id, name, _type) references ___.singularity(id, name, _type) on update cascade on delete cascade,
        check(zini >= sz_array[1][1]),
        unique(id, name, _type)
);

create table ___.pressure_accumulator_singularity(
    id integer primary key,
    name varchar default ___.unique_name('pressure_accumulator', 'singularity'),
    _type ___.type_singularity_ default 'pressure_accumulator' check(_type='pressure_accumulator'),
    v0 real not null default 0 check(v0 >=0),
    z_connection real not null default 0,
    filling_pressure real not null default 0 check(filling_pressure >=0),
        foreign key (id, name, _type) references ___.singularity(id, name, _type) on update cascade on delete cascade,
        unique(id, name, _type)
);

create table ___.air_relief_valve_singularity(
    id integer primary key,
    name varchar default ___.unique_name('air_relief_valve', 'singularity'),
    _type ___.type_singularity_ default 'air_relief_valve' check(_type='air_relief_valve'),
    orifice_diameter real not null check (orifice_diameter >=0),
    contraction_coef real not null default 0.6 check(contraction_coef >=0 and contraction_coef <=1),
        foreign key (id, name, _type) references ___.singularity(id, name, _type) on update cascade on delete cascade,
        unique(id, name, _type)
);

create table ___.imposed_piezometry_singularity(
    id integer primary key,
    name varchar default ___.unique_name('imposed_piezometry', 'singularity'),
    _type ___.type_singularity_ default 'imposed_piezometry' check(_type='imposed_piezometry'),
    z0 real not null default 0,
    chlorine real not null default 0 check(chlorine >=0),
    trihalomethane real not null default 0 check(trihalomethane >=0),
    passive_tracer real not null default 0 check(passive_tracer >=0),
    travel_time real not null default 0 check(travel_time >=0),
    contact_time real not null default 0 check(contact_time >=0),
    external_file boolean not null default false,
    cyclic boolean not null default false,
        foreign key (id, name, _type) references ___.singularity(id, name, _type) on update cascade on delete cascade,
        unique(id, name, _type)
);

create table ___.flow_injection_singularity(
    id integer primary key,
    name varchar default ___.unique_name('flow_injection', 'singularity'),
    _type ___.type_singularity_ default 'flow_injection' check(_type='flow_injection'),
    q0 real not null default 0,
    external_file boolean not null default false,
    cyclic boolean not null default false,
    chlorine real not null default 0 check(chlorine >=0),
    trihalomethane real not null default 0 check(trihalomethane >=0),
    passive_tracer real not null default 0 check(passive_tracer >=0),
    travel_time real not null default 0 check(travel_time >=0),
    contact_time real not null default 0 check(contact_time >=0),
        foreign key (id, name, _type) references ___.singularity(id, name, _type) on update cascade on delete cascade,
        unique(id, name, _type)
);

create table ___.chlorine_injection_singularity(
    id integer primary key,
    name varchar default ___.unique_name('chlorine_injection', 'singularity'),
    _type ___.type_singularity_ default 'chlorine_injection' check(_type='chlorine_injection'),
    target_concentration real not null default 0 check(target_concentration >=0),
    target_node integer references ___.node(id) on update cascade on delete set null,
        foreign key (id, name, _type) references ___.singularity(id, name, _type) on update cascade on delete cascade,
        unique(id, name, _type)
);


create table ___.model_connection_singularity(
    id integer primary key,
    name varchar default ___.unique_name('model_connection', 'singularity'),
    _type ___.type_singularity_ default 'model_connection' check(_type='model_connection'),
    cascade_mode ___.model_connect_mode not null default 'Imposed piezometry Z(t)',
    zt_array real[20][2] not null default '{{0, 0}}'::real[] check(array_length(zt_array, 1)>0 and array_length(zt_array, 1)<=20 and array_length(zt_array, 2)=2),
    qt_array real[20][2] not null default '{{0, 0}}'::real[] check(array_length(qt_array, 1)>0 and array_length(qt_array, 1)<=20 and array_length(qt_array, 2)=2),
    external_file boolean not null default false,
    cyclic boolean not null default false,
    chlorine real not null default 0 check(chlorine >=0),
    trihalomethane real not null default 0 check(trihalomethane >=0),
    passive_tracer real not null default 0 check(passive_tracer >=0),
    travel_time real not null default 0 check(travel_time >=0),
    contact_time real not null default 0 check(contact_time >=0),
        foreign key (id, name, _type) references ___.singularity(id, name, _type) on update cascade on delete cascade,
        unique(id, name, _type)
);


------------------------------------------------------------------------------------------------
-- LINKS                                                                                      --
------------------------------------------------------------------------------------------------

create type ___.type_link_ as enum ('pipe', 'valve', 'flow_regulator', 'pressure_regulator',
'check_valve', 'pump', 'headloss');

create table ___.type_link(
    name ___.type_link_,
    exportid integer,
    exportcode varchar,
    abbreviation varchar,
        primary key (name, exportid, exportcode, abbreviation)
);

insert into ___.type_link(name, exportid, exportcode, abbreviation) values
('pipe',                3, 'CANA',  'CANA'),
('valve',               4, 'VANNE', 'VALV'),
('flow_regulator',      5, 'REGQ',  'REGQ'),
('pressure_regulator',  6, 'REDP',  'REGP'),
('check_valve',         7, 'CLPT',  'CLPT'),
('pump',                8, 'POMP',  'POMP'),
('headloss',            9, 'BORDA', 'BORDA');

create table ___.link(
    id serial primary key,
    name varchar(24) not null check(not name~' ' and octet_length(name)<=24),
    _type ___.type_link_ not null,
    up integer not null references ___.node(id) on update cascade on delete cascade,
    down integer not null references ___.node(id) on update cascade on delete cascade,
    comment varchar,
    geom geometry('LINESTRING', 2154) not null check(ST_IsValid(geom)),
        unique (id, name, _type),
        unique (id, _type)
);

create index link_geomidx on ___.link using gist(geom);
create index link_typeidx on ___.link(_type);
create index link_upidx on ___.link(up);
create index link_downidx on ___.link(down);

create table ___.pipe_link(
    id integer primary key,
    name varchar default ___.unique_name('pipe', 'link'),
    _type ___.type_link_ not null default 'pipe' check(_type='pipe'),
    diameter real check(diameter > 0),
    thickness_mm real not null default 8 check(thickness_mm > 0),
    material varchar references ___.material(name) on update cascade on delete set null default 'Unknown',
    status_open boolean not null default true,
    meter boolean not null default false,
    overload_length boolean not null default false,
    overloaded_length real check(overloaded_length > 0),
    overload_celerity boolean not null default false,
    overloaded_celerity real check(overloaded_celerity > 0),
    overload_roughness_mm boolean not null default false,
    overloaded_roughness_mm real check(overloaded_roughness_mm >= 0),
    is_feeder boolean default false,
    installation_date date,
    compute_contact_time boolean not null default true,
        foreign key (id, name, _type) references ___.link(id, name, _type) on update cascade on delete cascade,
        check(not overload_length or overloaded_length is not null),
        check(not overload_celerity or overloaded_celerity is not null),
        check(not overload_roughness_mm or overloaded_roughness_mm is not null),
        unique(id, name, _type)
);

create table ___.pipe_link_singularity(
    id serial primary key,
    pipe_link integer references ___.pipe_link(id) on update cascade on delete cascade,
    number integer not null default 1 check(number>0),
    type_singularity ___.pipe_type_singularity,
    bend_angle real check(bend_angle>0 and bend_angle<=180),
    bend_radius real check(bend_radius>0),
    bend_is_smooth boolean,
    borda_section real check(borda_section>0),
    borda_coef real check(borda_coef>0),
        unique (pipe_link, type_singularity),
        check(type_singularity is null
            or (type_singularity='Bend' and bend_angle is not null and bend_is_smooth is not null)
            or (type_singularity='Borda headloss' and borda_section is not null and borda_coef is not null)
            )
);

create table ___.valve_link(
    id integer primary key,
    name varchar default ___.unique_name('valve', 'link'),
    _type ___.type_link_ not null default 'valve' check(_type='valve'),
    section real not null check(section >=0),
    headloss_coef real not null default 1.5 check(headloss_coef >=0),
    c_opening real not null default 1 check(c_opening >=0 and c_opening <=1),
        foreign key (id, name, _type) references ___.link(id, name, _type) on update cascade on delete cascade,
        unique(id, name, _type)
);

create table ___.flow_regulator_link(
    id integer primary key,
    name varchar default ___.unique_name('flow_regulator', 'link'),
    _type ___.type_link_ not null default 'flow_regulator' check(_type='flow_regulator'),
    section real not null check(section >=0),
    headloss_coef real not null default 1.5 check(headloss_coef >=0),
    regul_q real not null default 0 check(regul_q >=0),
        foreign key (id, name, _type) references ___.link(id, name, _type) on update cascade on delete cascade,
        unique(id, name, _type)
);

create table ___.pressure_regulator_link(
    id integer primary key,
    name varchar default ___.unique_name('pressure_regulator', 'link'),
    _type ___.type_link_ not null default 'pressure_regulator' check(_type='pressure_regulator'),
    section real null check(section >=0),
    headloss_coef real not null default 1.5 check(headloss_coef >=0),
    pressure_regulation_option ___.pressure_regulation_option not null default 'Constant pressure',
    regul_p real,
    regul_pq_array real[10][2] not null default '{{0, 0}}'::real[]  check(array_length(regul_pq_array, 1)>0 and array_length(regul_pq_array, 1)<=10 and array_length(regul_pq_array, 2)=2),
    pressure_regulation_mode ___.pressure_regulation_mode not null default 'Imposed differential pressure',
    pid_p real not null default 1,
    pid_i real not null default 1,
    pid_d real not null default 1,
        foreign key (id, name, _type) references ___.link(id, name, _type) on update cascade on delete cascade,
        check(pressure_regulation_option != 'Constant pressure' or regul_p is not null),
        check(pressure_regulation_option != 'Constant pressure' or regul_p >=0),
        unique(id, name, _type)
);

create table ___.check_valve_link(
    id integer primary key,
    name varchar default ___.unique_name('check_valve', 'link'),
    _type ___.type_link_ not null default 'check_valve' check(_type='check_valve'),
    section real not null check(section > 0),
    headloss_coef real not null default 1.5 check(headloss_coef >=0),
    opening_pressure real not null default 0 check(opening_pressure >=0),
    closing_mode ___.valve_closing_mode not null default 'Instantaneous',
    cd_coef real default 0.3,
        foreign key (id, name, _type) references ___.link(id, name, _type) on update cascade on delete cascade,
        check(closing_mode!='Instantaneous' or cd_coef is not null),
        check(closing_mode!='Instantaneous' or cd_coef >=0),
        unique(id, name, _type)
);

create table ___.pump_link(
    id integer primary key,
    name varchar default ___.unique_name('pump', 'link'),
    _type ___.type_link_ not null default 'pump' check(_type='pump'),
    pq_array real[10][2] not null default '{{0, 0}}'::real[] check(array_length(pq_array, 1)>0 and array_length(pq_array, 1)<=10 and array_length(pq_array, 2)=2),
    check_valve boolean not null default true,
    rotation_speed real not null check(rotation_speed >=0),
    inertia real not null check(inertia >=0),
    pump_regulation_option ___.pump_regulation_option not null default 'No regulation',
    speed_reduction_coef real not null default 1,
    regul_q real default 0,
    target_node integer references ___.node(id) on update cascade on delete set null,
    regul_z_start real default 0,
    regul_z_stop real default 0,
    regul_p real default 0,
    external_file boolean not null default false,
    cyclic boolean not null default false,
    regul_z real,
    regul_proportional real,
    regul_integral real,
    regul_derivative real,
        foreign key (id, name, _type) references ___.link(id, name, _type) on update cascade on delete cascade,
        check(pump_regulation_option != 'No regulation' or speed_reduction_coef is not null),
        check(pump_regulation_option != 'No regulation' or (speed_reduction_coef >=0 and speed_reduction_coef <=1)),
        check(pump_regulation_option != 'Flow regulation' or regul_q is not null),
        check(pump_regulation_option != 'Flow regulation' or regul_q >=0),
        --check(pump_regulation_option != 'Stop-start regulation' or target_node is not null), -- removed because it forbids deletion of model
        check(pump_regulation_option != 'Stop-start regulation' or regul_z_start is not null),
        check(pump_regulation_option != 'Stop-start regulation' or regul_z_stop is not null),
        check(pump_regulation_option != 'Pressure regulation' or regul_p is not null),
        check(pump_regulation_option != 'Pressure regulation' or regul_p >=0),
        check(pump_regulation_option != 'Water level in reservoir' or (regul_z is not null and regul_proportional is not null and regul_integral is not null and regul_derivative is not null)),
        unique(id, name, _type)
);

create table ___.headloss_link(
    id integer primary key,
    name varchar default ___.unique_name('headloss', 'link'),
    _type ___.type_link_ not null default 'headloss' check(_type='headloss'),
    section real not null check(section >=0),
    rk1 real not null default 1.5 check(rk2 >=0),
    rk2 real not null default 1.5 check(rk1 >=0),
        foreign key (id, name, _type) references ___.link(id, name, _type) on update cascade on delete cascade,
        unique(id, name, _type)
);


------------------------------------------------------------------------------------------------
-- WATER DELIVERY                                                                             --
------------------------------------------------------------------------------------------------

create table ___.water_delivery_sector(
    name varchar(24) primary key default ___.unique_name('water_delivery_sector', abbreviation=>'WD_SECT') check(not name~' ' and octet_length(name)<=24),
    comment varchar,
    ref_leak_efficiency real not null check(ref_leak_efficiency>0 and ref_leak_efficiency<=1) default 1,
    geom geometry('MULTIPOLYGON', 2154) not null check(ST_IsValid(geom))
);

create index water_delivery_sector_geomidx on ___.water_delivery_sector using gist(geom);

create table ___.water_delivery_scenario(
    name varchar(24) primary key check(name not like 'WD_SCN_REF' and not name~' ' and octet_length(name)<=24) default ___.unique_name('water_delivery_scenario', abbreviation=>'WD_SCN'),
    comment varchar,
    period ___.week_period not null default 'Weekday'
);

create table ___.water_delivery_scenario_sector_setting(
    scenario varchar references ___.water_delivery_scenario(name) on update cascade on delete cascade,
    sector varchar references ___.water_delivery_sector(name) on update cascade on delete cascade,
    volume_m3j real,
    adjust_coef real not null check (adjust_coef>=0) default 1,
    leak_efficiency real not null check(leak_efficiency>0 and leak_efficiency<=1) default 1,
        primary key (scenario, sector)
);

create table ___.water_delivery_point(
    id serial primary key,
    name varchar unique default ___.unique_name('water_delivery_point', abbreviation=>'WD'),
    comment varchar,
    geom geometry('POINT', 2154) not null,
    volume real not null default 0 check(volume >= 0),
    industrial boolean not null default false,
    pipe_link integer references ___.pipe_link(id) on update cascade on delete set null,
        unique (name, industrial)
);

create index water_delivery_point_geomidx on ___.water_delivery_point using gist(geom);

create index water_delivery_point_user_nodeidx on ___.water_delivery_point (pipe_link);

create table ___.water_delivery_scenario_industrial_volume(
    water_delivery_scenario varchar references ___.water_delivery_scenario(name) on update cascade on delete cascade,
    water_delivery_point varchar,
    volume real not null default 0 check(volume >= 0),
    industrial boolean not null default true check(industrial),
        primary key (water_delivery_scenario, water_delivery_point),
        foreign key (water_delivery_point, industrial) references ___.water_delivery_point(name, industrial) on update cascade on delete cascade
);

create table ___.fire_hydrant(
    id serial primary key,
    name varchar unique default ___.unique_name('fire_hydrant', abbreviation=>'FH') check(not name~' ' and octet_length(name)<=24),
    comment varchar,
    geom geometry('POINT', 2154) not null,
    diameter real not null check (diameter>=0),
    nominal_flow_m3h real not null check (nominal_flow_m3h >= 0),
    draw_time_h real not null check (draw_time_h >=0),
    pipe_link integer references ___.pipe_link(id) on update cascade on delete set null
);



------------------------------------------------------------------------------------------------
-- SCENARIOS                                                                                  --
------------------------------------------------------------------------------------------------

create table ___.scenario(
    name varchar(24) primary key default ___.unique_name('scenario', abbreviation=>'SCN'),
    comment varchar,
    comput_mode ___.computation_mode not null default 'Steady state',
    starting_date timestamp not null check(extract(seconds from starting_date)=0) default '2000-01-01 00:00:00',
    duration interval default '12:00:00',
    timestep real not null check(timestep>0) default 300,
    refined_discretisation boolean default false,
    water_delivery_scenario varchar references ___.water_delivery_scenario(name) on update cascade on delete set null,
    peak_coefficient real not null check(peak_coefficient>=0) default 1,
    -- initial conditions settings
    tini interval default '12:00:00',
    flag_save boolean not null default false,
    tsave_hr interval default '00:00:00',
    scenario_rstart varchar references ___.scenario(name) on update cascade on delete set null,
    trstart_hr interval default '00:00:01',
    -- output time intervals
    dt_output real not null check(timestep>0) default 300,
    tbegin_output_hr interval default '00:00:00',
    tend_output_hr interval default '12:00:00',
    -- reference scenario
    scenario_ref varchar references ___.scenario(name) on update cascade on delete set null,
    -- model ordering
    model_connect_settings ___.model_connection_settings not null default 'Cascade',
    -- computation_options
    option_file varchar,
    four_quadrant_file varchar,
    vaporization boolean not null default false,
    -- water quality
    quality_output ___.quality_output not null default 'None',
    chlorine boolean not null default false,
    trihalomethane boolean not null default false,
    passive_tracer boolean not null default false,
    travel_time boolean not null default false,
    chlorine_water_reaction_coef real not null default 0.05,
    chlorine_wall_reaction_coef real not null default 5e-7,
    trihalomethane_reaction_parameter real not null default 0.05,
    trihalomethane_concentration_limit real not null default 5e-7,
    skeletonization boolean not null default false,
        check(comput_mode!='Fast transient' or refined_discretisation is true),
        check(comput_mode::text not like 'Exterior fire defense%' or scenario_rstart is not null),
        check(comput_mode!='Break criticality' or scenario_rstart is not null),
        check(scenario_rstart is null or scenario_ref is not null)
);

-- Model ordering

create table ___.groupe(
    name varchar primary key default ___.unique_name('groupe',abbreviation=>'GROUP')
);

create table ___.groupe_model(
    groupe varchar references ___.groupe(name) on update cascade on delete cascade,
    model varchar references ___.model(name) on update cascade on delete cascade,
        primary key (groupe, model)
);

create table ___.mixed(
    priority integer check(priority>0), -- group export order : lower priority written first
    scenario varchar references ___.scenario(name) on update cascade on delete cascade,
    groupe varchar references ___.groupe(name) on update cascade on delete cascade,
        primary key (priority, scenario, groupe),
        unique(scenario, groupe)
);

create table ___.cascade(
    priority integer check(priority>0), -- model export order : lower priority written first
    scenario varchar references ___.scenario(name) on update cascade on delete cascade,
    model varchar references ___.model(name) on update cascade on delete cascade,
        primary key (priority, scenario, model),
        unique(scenario, model)
);

-- Regulation

create table ___.regulation_scenario(
    priority integer not null check(priority>0), -- files export order : lower priority written first
    regulation_file varchar,
    scenario varchar references ___.scenario(name) on update cascade on delete cascade,
        primary key (regulation_file, scenario),
        unique(priority, scenario)
);

-- Hydrology
create table ___.hydrology_scenario(
    hydrology_file varchar,
    scenario varchar references ___.scenario(name) on update cascade on delete cascade,
        primary key (hydrology_file, scenario)
);

-- Failures
create table ___.scenario_link_failure(
    id serial primary key,
    scenario varchar references ___.scenario(name) on update cascade on delete cascade,
    link integer,
    link_type ___.type_link_ check(link_type in ('check_valve', 'flow_regulator', 'pressure_regulator', 'valve')),
    failure_timestamp interval default '00:00:00' check(failure_timestamp<'24 hours'::interval),
    failure_curve varchar not null references ___.failure_curve(name) on update cascade,
        unique (scenario, link),
        foreign key (link, link_type) references ___.link(id, _type) on update cascade on delete cascade
);

create table ___.scenario_pump_failure(
    id serial primary key,
    scenario varchar references ___.scenario(name) on update cascade on delete cascade,
    link integer,
    link_type ___.type_link_ default 'pump' check(link_type = 'pump'),
    failure_timestamp interval default '00:00:00' check(failure_timestamp<'24 hours'::interval),
    failure_curve varchar references ___.failure_curve(name) on update cascade,
    failure_mode ___.pump_failure_mode not null default 'Automatic alpha computation',
        unique (scenario, link),
        foreign key (link, link_type) references ___.link(id, _type) on update cascade on delete cascade,
        check(failure_curve is null or failure_mode = 'Alpha(t) curve')
);

-- Water origin
create table ___.scenario_water_origin(
    id serial primary key,
    scenario varchar references ___.scenario(name) on update cascade on delete cascade,
    imposed_piezometry_singularity integer references ___.imposed_piezometry_singularity(id) on update cascade on delete cascade,
    flow_injection_singularity integer references ___.flow_injection_singularity(id) on update cascade on delete cascade,
    model_connection_singularity integer references ___.model_connection_singularity(id) on update cascade on delete cascade,
    comment varchar,
        unique (scenario, imposed_piezometry_singularity, flow_injection_singularity, model_connection_singularity),
        check(num_nonnulls(imposed_piezometry_singularity, flow_injection_singularity, model_connection_singularity)=1)
);

------------------------------------------------------------------------------------------------
-- USER CONFIG                                                                                --
------------------------------------------------------------------------------------------------

create table ___.configuration(
    name varchar primary key default ___.unique_name('configuration', abbreviation=>'CFG'),
    creation_date timestamp not null default current_date,
    comment varchar
);

create table ___.user_configuration(
    user_ varchar primary key default session_user,
    config varchar references ___.configuration(name)
);

create table ___.scenario_configuration(
    scenario varchar references ___.scenario(name) on delete cascade on update cascade,
    config varchar references ___.configuration(name) on delete cascade on update cascade,
        primary key (scenario, config),
    rank_ integer not null default 1,
        unique (scenario, rank_)
);

create or replace function ___.current_config()
returns varchar
language sql volatile security definer as
$$
    select config from ___.user_configuration where user_=session_user;
$$
;


do $$
declare
   r record;
   c varchar;
begin
    for r in
        select 'create sequence ___.'||replace(replace(regexp_replace(replace(replace(
            col.column_default,
            '___.unique_name(''', ''),
            '::character varying', ''),
            ''', abbreviation =>.*\)', ''),
            ''', ''', '_'),
            ''')', '')||'_name_seq' as query
        from information_schema.columns col
        where col.column_default is not null
              and col.table_schema='___' and col.column_default ~ '^___.unique_name\(.*'
    loop
        raise notice '%',r.query;
        execute r.query;
    end loop;

    for c in select name from ___.type_node loop
        execute format($sql$
            create table ___.%1$s_node_config(
            like ___.%1$s_node,
            config varchar default ___.current_config() references ___.configuration(name) on update cascade on delete cascade,
                foreign key (id, name, _type) references ___.%1$s_node(id, name, _type) on delete cascade on update cascade,
                primary key (id, config)
            )
            $sql$, c);
    end loop;

    for c in select name from ___.type_singularity loop
        execute format($sql$
            create table ___.%1$s_singularity_config(
            like ___.%1$s_singularity,
            config varchar default ___.current_config() references ___.configuration(name) on update cascade on delete cascade,
                foreign key (id, name, _type) references ___.%1$s_singularity(id, name, _type) on delete cascade on update cascade,
                primary key (id, config)
            )
            $sql$, c);
    end loop;

    for c in select name from ___.type_link loop
        execute format($sql$
            create table ___.%1$s_link_config(
            like ___.%1$s_link,
            config varchar default ___.current_config() references ___.configuration(name) on update cascade on delete cascade,
                foreign key (id, name, _type) references ___.%1$s_link(id, name, _type) on delete cascade on update cascade,
                primary key (id, config)
            )
            $sql$, c);
    end loop;


end
$$
;

create or replace function ___.duplicated_names()
returns varchar
language sql as
$$
    with counted as (
        select upper(name) as name, model, count(1) ct
        from (
            select name, model from ___.node
            union all
            select s.name, n.model from ___.singularity s join ___.node n on n.id=s.id
            union all
            select l.name, n.model from ___.link l join ___.node n on n.id=l.up
            ) t
        group by upper(name), model
    )
    select string_agg(name, ', ') from counted where ct > 1;
$$
;

create or replace function ___.name_unicity_check()
returns trigger
language plpgsql as
$$
declare
    duplicates varchar;
begin
    select ___.duplicated_names() into duplicates;
    if duplicates is not null then
        raise 'duplicated name %', duplicates;
    end if;
    return new;
end;
$$
;

create trigger node_after_update_trig
after insert or update on ___.node
for each statement execute procedure ___.name_unicity_check();

create trigger singularity_after_update_trig
after insert or update on ___.singularity
for each statement execute procedure ___.name_unicity_check();

create trigger link_after_update_trig
after insert or update on ___.link
for each statement execute procedure ___.name_unicity_check();

create or replace function ___.metadata_after_srid_update_fct()
returns trigger
language plpgsql volatile as
$$
declare
    table_name varchar;
    geometry_type varchar;
    dimension integer;
    api_exists boolean;
    duplicates varchar;
begin
    if exists (select 1 from information_schema.schemata where schema_name = 'api') then
        drop schema api cascade;
        api_exists = true;
    end if;

    for table_name, geometry_type, dimension in select f_table_name, type, coord_dimension from geometry_columns where f_table_schema = '___' loop
        execute 'alter table ___.'||table_name||' '||
        E'alter column geom '||
        E'type geometry('''||geometry_type||case when dimension=3 then 'Z' else '' end||''', '||new.srid||') '||
        E'using st_transform(geom, '||new.srid||');';
    end loop;

    if api_exists then
        perform create_template();
        perform create_api();
        drop schema template cascade;
    end if;
    return new;
end;
$$
;

create or replace function ___.metadata_after_unique_name_per_model_update_fct()
returns trigger
language plpgsql volatile as
$$
declare
    table_name varchar;
    geometry_type varchar;
    dimension integer;
    api_exists boolean;
    duplicates varchar;
begin
    if new.unique_name_per_model then
        select ___.duplicated_names() into duplicates ;
        if duplicates is not null then
            raise 'duplicated name %', duplicates;
        end if;
        alter table ___.node enable trigger node_after_update_trig;
        alter table ___.singularity enable trigger singularity_after_update_trig;
        alter table ___.link enable trigger link_after_update_trig;
    else
        alter table ___.node disable trigger node_after_update_trig;
        alter table ___.singularity disable trigger singularity_after_update_trig;
        alter table ___.link disable trigger link_after_update_trig;
    end if;
    return new;
end;
$$
;

create trigger metadata_after_srid_update_trig
after update of srid on ___.metadata
for each row execute procedure ___.metadata_after_srid_update_fct();

create trigger metadata_after_unique_name_per_model_update_trig
after update of unique_name_per_model on ___.metadata
for each row execute procedure ___.metadata_after_unique_name_per_model_update_fct();

create type ___.sensor_type as enum (
    'index(m3)',
    'q(m3/h)',
    'q(m3/s)',
    'q(m3/day)',
    'z(m)',
    'p(bar)',
    'p(m)'
);

create table ___.sensor (
    id serial primary key,
    name varchar not null unique,
    type_ ___.sensor_type not null,
    geom public.geometry(POINT, 2154) not null,
    comment varchar
);

create index sensor_geom_idx on ___.sensor using gist(geom);

create table ___.measure (
    t timestamp without time zone not null,
    value float not null,
    sensor integer not null references ___.sensor(id) on delete cascade on update cascade,
    creation_date timestamp without time zone default now() not null,
        unique (t, sensor),
    id serial primary key
);

create index measure_sensor_idx on ___.measure(sensor);

create table ___.comment (
	id serial primary key,
	user_ varchar default session_user,
	creation_date timestamp not null default now(),
	txt varchar,
	node integer references ___.node(id) on delete cascade on update cascade,
	link integer references ___.link(id) on delete cascade on update cascade,
	singularity integer references ___.singularity(id) on delete cascade on update cascade,
	scenario varchar references ___.scenario(name) on delete cascade on update cascade
);
