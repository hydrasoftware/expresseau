-- copy model from cpy schema
-- also copies water_delivery_point and water_delivery_sector

create cast (cpy.type_node_ as ___.type_node_) with inout as implicit;
create cast (cpy.type_link_ as ___.type_link_) with inout as implicit;
create cast (cpy.type_singularity_ as ___.type_singularity_) with inout as implicit;
create cast (cpy.pipe_type_singularity as ___.pipe_type_singularity) with inout as implicit;

insert into ___.model(name) select name from cpy.model;

insert into ___.hourly_modulation_curve(name, comment, hourly_modulation_array)
select name, comment, hourly_modulation_array from cpy.hourly_modulation_curve
on conflict on constraint hourly_modulation_curve_pkey do nothing
;

-- offset ids of:
-- - node,
-- - link,
-- - singularity
-- - pipe_link_singularity
-- in cpy schema
do $do$
declare
    tbl varchar;
    strt integer;
begin
    for tbl in values ('node'), ('link'), ('pipe_link_singularity')
    loop
        -- on se décale pour éviter les doublons, puis on revient en arrière pour démarrer à 1
        execute format( $$ alter sequence cpy.%s_id_seq restart with 9999999; $$, tbl);
        execute format( $$ update cpy.%1$s set id=nextval('cpy.%1$s_id_seq') $$, tbl);
        execute format( $$ select nextval('___.%s_id_seq') $$, tbl) into strt;
        execute format( $$ update cpy.%1$s set id=nextval('___.%1$s_id_seq') $$, tbl);
        if tbl != 'pipe_link_singularity' then
            execute format( $$ insert into ___.%1$s select * from cpy.%1$s $$, tbl);
        end if;
    end loop;
end;
$do$;

insert into ___.singularity select * from cpy.singularity; -- no renumbering needed

create cast (cpy.return_valve_mode as ___.return_valve_mode) with inout as implicit;
create cast (cpy.valve_closing_mode as ___.valve_closing_mode) with inout as implicit;
create cast (cpy.mixing_mode as ___.mixing_mode) with inout as implicit;
create cast (cpy.alim_mode as ___.alim_mode) with inout as implicit;
create cast (cpy.pressure_regulation_option as ___.pressure_regulation_option) with inout as implicit;
create cast (cpy.pressure_regulation_mode as ___.pressure_regulation_mode) with inout as implicit;
create cast (cpy.pump_regulation_option as ___.pump_regulation_option) with inout as implicit;
create cast (cpy.pump_failure_mode as ___.pump_failure_mode) with inout as implicit;
create cast (cpy.week_period as ___.week_period) with inout as implicit;
create cast (cpy.computation_mode as ___.computation_mode) with inout as implicit;
create cast (cpy.model_connection_settings as ___.model_connection_settings) with inout as implicit;
create cast (cpy.model_connect_mode as ___.model_connect_mode) with inout as implicit;
create cast (cpy.quality_output as ___.quality_output) with inout as implicit;

do $$
declare
   r record;
   c varchar;
begin

    for c in select name from ___.type_node loop
        execute format($sql$ insert into ___.%1$s_node select * from cpy.%1$s_node $sql$, c);
    end loop;

    for c in select name from ___.type_singularity loop
        execute format($sql$ insert into ___.%1$s_singularity select * from cpy.%1$s_singularity $sql$, c);
    end loop;

    for c in select name from ___.type_link loop
        execute format($sql$ insert into ___.%1$s_link select * from cpy.%1$s_link $sql$, c);
    end loop;


end
$$
;

insert into ___.pipe_link_singularity select * from cpy.pipe_link_singularity;

insert into ___.water_delivery_point (name, comment, geom, volume, industrial, pipe_link)
select name, comment, geom, volume, industrial, pipe_link from cpy.water_delivery_point
on conflict on constraint water_delivery_point_pkey do nothing
;
insert into ___.water_delivery_sector(name, comment, ref_leak_efficiency, geom)
select name, comment, ref_leak_efficiency, geom from cpy.water_delivery_sector
on conflict on constraint water_delivery_sector_pkey do nothing
;


drop schema cpy cascade
;
