------------------------------------------------------------------------------------------------
--                                                                                            --
--     This file is part of EXPRESSEAU, a QGIS plugin for hydraulics                          --
--     (see <http://hydra-software.net/>).                                                    --
--                                                                                            --
--     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      --
--     of Setec Hydratec, Paris.                                                              --
--                                                                                            --
--     Contact: <contact@hydra-software.net>                                                  --
--                                                                                            --
--     You can use this program under the terms of the GNU General Public                     --
--     License as published by the Free Software Foundation, version 3 of                     --
--     the License.                                                                           --
--                                                                                            --
--     You should have received a copy of the GNU General Public License                      --
--     along with this program. If not, see <http://www.gnu.org/licenses/>.                   --
--                                                                                            --
------------------------------------------------------------------------------------------------

create extension if not exists pgrouting;

create or replace function create_template()
returns varchar
language sql volatile
as
$template$

drop schema if exists api cascade;

drop schema if exists template cascade;

create schema template;

comment on schema template is 'temporary schema for template generating functions. dropped at the end of this script';

------------------------------------------------------------------------------------------------
-- Dynamic queries for inherited objects view / functions and triggers                        --
------------------------------------------------------------------------------------------------

create function template.view_statement(
    concrete varchar,
    abstract varchar default null,
    additional_columns varchar[] default '{}'::varchar[],
    additional_join varchar default null,
    additional_union varchar default null,
    specific_geom varchar default null,
    specific_columns json default '{}'::json
    )
returns varchar
language sql stable as
$$
    with pk as (
        select string_agg('a.'||kcu.column_name||'=c.'||kcu.column_name, ' and ') as whr,
               string_agg('c.'||kcu.column_name, '||''_''||') as _id
        from information_schema.table_constraints tco
        join information_schema.key_column_usage kcu
            on kcu.constraint_name = tco.constraint_name
            and kcu.constraint_schema = tco.constraint_schema
            and kcu.constraint_name = tco.constraint_name
        where tco.constraint_type = 'PRIMARY KEY'
        and kcu.constraint_schema = '___' and kcu.table_name = concrete||coalesce('_'||abstract, '')
        and not kcu.column_name ~ '^_.*'
    ),
    concrete_cols as (
        select column_name, data_type
        from information_schema.columns
        where table_schema = '___'
        and table_name = concrete||coalesce('_'||abstract, '')
        and not column_name ~ '^_.*'
    ),
    abstract_cols as (
        select column_name
        from information_schema.columns
        where table_schema = '___'
        and table_name = abstract
        and not column_name ~ '^_.*'
        except
        select column_name from concrete_cols
    )
    select 'create view api.'||concrete||coalesce('_'||abstract, '')||' as '||
        'select '||array_to_string(array_agg(column_name)||additional_columns::text[], ', ')||' '||
        'from ___.'||concrete||coalesce('_'||abstract, '')||' as c '||
        coalesce('join ___.'||abstract||' as a on '||whr, '')||
        coalesce(' '||additional_join, '')||
        coalesce(' '||additional_union, '')
    from (
        select case
            when data_type = 'ARRAY' then ('c.'||column_name||'::text as '||column_name)
            when column_name in (select key from json_each_text(specific_columns)) then (specific_columns::json->>column_name||' as '||column_name)
            else 'c.'||column_name end
        as column_name from concrete_cols
        union all
        select case
            when column_name = 'geom' then coalesce(specific_geom||' as geom', 'a.geom')
            else 'a.'||column_name end as column_name from abstract_cols ) t
    cross join pk
    group by whr, _id
    ;
$$
;

create function template.view_default_statement(concrete varchar, abstract varchar default null)
returns setof varchar
language sql stable as
$$
    select 'alter view api.'||concrete||coalesce('_'||abstract, '')||' alter column '||column_name||' set default '||column_default
    from information_schema.columns
    where table_schema = '___'
    and table_name in (abstract, concrete||coalesce('_'||abstract, ''))
    and not column_name ~ '^_.*'
    and column_default is not null
    ;
$$
;

create function template.insert_statement(tablename varchar, concrete varchar default null)
returns varchar
language sql stable as
$$
    select 'insert into ___.'||tablename||'('||string_agg(column_name, ', ')||') '||
           'values ('||string_agg(
                case
                    when column_name = '_type' then ''''||concrete||''''
                    when data_type = 'ARRAY' then 'new.'||column_name||'::real[]'
                    when column_name = 'geom' then 'coalesce(overloaded_geom, new.'||column_name||')'
                else 'new.'||column_name end,
                ', ')||') '
    from information_schema.columns
    where table_schema = '___'
    and table_name = tablename
    ;
$$
;

create function template.update_statement(tablename varchar)
returns varchar
language sql stable as
$$
    with pk as (
        select string_agg(kcu.column_name||'=old.'||kcu.column_name, ' and ') as whr
        from information_schema.table_constraints tco
        join information_schema.key_column_usage kcu
            on kcu.constraint_name = tco.constraint_name
            and kcu.constraint_schema = tco.constraint_schema
            and kcu.constraint_name = tco.constraint_name
        where tco.constraint_type = 'PRIMARY KEY'
        and kcu.constraint_schema = '___' and kcu.table_name = tablename
        and not kcu.column_name  ~ '^_.*'
    )
    select 'update ___.'||tablename||' '||
           'set '||string_agg(column_name||'='||
                case
                    when data_type = 'ARRAY' then 'new.'||column_name||'::real[]'
                    when column_name = 'geom' then 'coalesce(overloaded_geom, new.'||column_name||')'
                else 'new.'||column_name end,
                ', ')||' '||
           'where '||whr
    from information_schema.columns
    cross join pk
    where table_schema = '___'
    and table_name = tablename
    and not column_name ~ '^_.*'
    group by whr
    ;
$$
;

create function template.delete_statement(tablename varchar)
returns varchar
language sql stable as
$$
    select 'delete from ___.'||tablename||' where '||string_agg(kcu.column_name||'=old.'||kcu.column_name, ' and ') as whr
    from information_schema.table_constraints tco
    join information_schema.key_column_usage kcu
        on kcu.constraint_name = tco.constraint_name
        and kcu.constraint_schema = tco.constraint_schema
        and kcu.constraint_name = tco.constraint_name
    where tco.constraint_type = 'PRIMARY KEY'
    and kcu.constraint_schema = '___'
    and kcu.table_name = tablename
    and not column_name ~ '^_.*'
    ;
$$
;

create function template.basic_function_statement(
    table_name varchar,
    cannot_delete boolean default False,
    start_section varchar default null
    )
returns varchar
language plpgsql stable as
$$
begin
    return 'create function api.'||table_name||E'_instead_fct()\n'||
    E'returns trigger \n'||
    E'language plpgsql volatile security definer as\n'||
    E'$fct$\n'||
    E'declare\n'||
    E'    overloaded_geom geometry; -- used in start_section to modify geom type\n'||
    E'begin\n'||
    coalesce(start_section, '')||
    E'    if tg_op = ''INSERT'' then\n'||
    E'        '||template.insert_statement(table_name)||E';\n'||
    E'        return new;\n'||
    E'    elsif tg_op = ''UPDATE'' then\n'||
    E'        '||template.update_statement(table_name)||E';\n'||
    E'        return new;\n'||
    E'    elsif tg_op = ''DELETE'' then\n'||
    case when cannot_delete then
    E'        raise notice ''cannot delete from table '||table_name||E''';\n'
    else
    E'        '||template.delete_statement(table_name)||E';\n'
    end||
    E'        return old;\n'||
    E'    end if;\n'||
    E'end;\n'||
    E'$fct$\n';
end;
$$
;

create function template.inherited_function_statement(
    concrete varchar,
    abstract varchar,
    start_section varchar default null,
    after_update_section varchar default null
    )
returns varchar
language plpgsql stable as
$$
begin
    --raise notice '%', template.insert_statement(concrete||'_'||abstract||'_config', concrete);
    --raise notice '%', template.update_statement(concrete||'_'||abstract||'_config');
    return 'create function api.'||concrete||'_'||abstract||E'_instead_fct()\n'||
    E'returns trigger \n'||
    E'language plpgsql volatile security definer as\n'||
    E'$fct$\n'||
    E'declare\n'||
    E'    overloaded_geom geometry; -- used in start_section to modify geom type\n'||
    E'begin\n'||
    coalesce(start_section, '')||
    E'    if tg_op = ''INSERT'' then\n'||
    E'        '||template.insert_statement(abstract, concrete)||E';\n'||
    E'        '||template.insert_statement(concrete||'_'||abstract, concrete)||E';\n'||
    E'        return new;\n'||
    E'    elsif tg_op = ''UPDATE'' then\n'||
    E'        '||template.update_statement(abstract)||E';\n'||
    E'        if ___.current_config() is null then\n'||
    E'            '||template.update_statement(concrete||'_'||abstract)||E';\n'||
    E'        else\n'||
    E'            '||replace(template.insert_statement(concrete||'_'||abstract||'_config', concrete), 'new.config', 'api.current_config()')||
    E'            on conflict on constraint '||concrete||'_'||abstract||E'_config_pkey do\n'||
    E'            '||replace(replace(regexp_replace(template.update_statement(concrete||'_'||abstract), ' where .*', ''), ' ___.'||concrete||'_'||abstract, ''), 'new.config', 'api.current_config()')||E';\n'||
    E'        end if;\n'||
    coalesce(after_update_section, '')||
    E'        return new;\n'||
    E'    elsif tg_op = ''DELETE'' then\n'||
    E'        '||template.delete_statement(abstract)||E';\n'||
    E'        '||template.delete_statement(concrete||'_'||abstract)||E';\n'|| -- inutile, le abrait fait référence au concret avec 'on delete cascade'
    E'        return old;\n'||
    E'    end if;\n'||
    E'end;\n'||
    E'$fct$\n';
end;
$$
;

create function template.trigger_statement(concrete varchar, abstract varchar default null)
returns varchar
language sql stable as
$$
    select
    E'create trigger '||concrete||coalesce('_'||abstract, '')||E'_instead_trig\n'||
    E'instead of insert or update or delete on api.'||concrete||coalesce('_'||abstract, '')||E'\n'||
    E'for each row execute procedure api.'||concrete||coalesce('_'||abstract, '')||E'_instead_fct()\n';
$$
;

create function template.basic_view(
    table_name varchar,
    additional_columns varchar[] default '{}'::varchar[],
    additional_join varchar default null,
    additional_union varchar default null,
    with_trigger boolean default False,
    cannot_delete boolean default False,
    specific_geom varchar default null,
    specific_columns json default '{}'::json,
    start_section varchar default ''
    )
returns varchar
language plpgsql volatile as
$$
declare
    s varchar;
begin
    --raise notice '%', template.view_statement(table_name, additional_columns => additional_columns, additional_join => additional_join, additional_union => additional_union, specific_geom => specific_geom, specific_columns => specific_columns);
    execute template.view_statement(table_name, additional_columns => additional_columns, additional_join => additional_join, additional_union => additional_union, specific_geom => specific_geom, specific_columns => specific_columns);
    for s in select * from template.view_default_statement(table_name) loop
        raise notice '%', s;
        execute s;
    end loop;
    if with_trigger then
        --raise notice '%', template.basic_function_statement(table_name, cannot_delete=>cannot_delete);
        execute template.basic_function_statement(table_name, cannot_delete=>cannot_delete, start_section=>start_section);
        --raise notice '%', template.trigger_statement(table_name);
        execute template.trigger_statement(table_name);
    end if;

    return 'CREATE BASIC VIEW api.'||table_name;
end;
$$
;

create function template.inherited_view(
    concrete varchar,
    abstract varchar,
    additional_columns varchar[] default '{}'::varchar[],
    additional_join varchar default null,
    specific_geom varchar default null,
    specific_columns json default '{}'::json,
    start_section varchar default '',
    after_update_section varchar default ''
    )
returns varchar
language plpgsql volatile as
$$
declare
    s varchar;
begin
    --raise notice '%', template.view_statement(concrete, abstract, additional_columns, additional_join, specific_geom, specific_columns);
    additional_join := coalesce(additional_join, '')||format(' where c.id not in (select g.id from ___.%s_%s_config g where g.config=___.current_config())', concrete, abstract);

    s := template.view_statement(concrete, abstract, additional_columns => additional_columns, additional_join => additional_join, specific_geom => specific_geom, specific_columns => specific_columns);
    -- add configuration

    s := s||
        regexp_replace(
        regexp_replace(
        regexp_replace(
        s, 'create view [^ ]+ as select ', ' union all select '),
        concrete||'_'||abstract, concrete||'_'||abstract||'_config'),
        ' where .*', ' where c.config=___.current_config()');

    --raise notice '%', s;
    execute s;

    for s in select * from template.view_default_statement(concrete, abstract) loop
        --raise notice '%', s;
        execute s;
    end loop;
    --raise notice '%', template.inherited_function_statement(concrete, abstract);
    execute template.inherited_function_statement(concrete, abstract, start_section=>start_section, after_update_section=>after_update_section);
    --raise notice '%', template.trigger_statement(concrete, abstract);
    execute template.trigger_statement(concrete, abstract);

    return 'CREATE INHERITED VIEW api.'||concrete||'_'||abstract;
end;
$$
;

create function template.create_configured_view()
returns void
language plpgsql volatile as
$$
declare
    sql varchar;
    c varchar;
    row_diff_join varchar;
    delete_statements varchar;
    update_statements varchar;
begin
    sql := E'create view api.configured as \n'||
           E'with diff as (\n';
    delete_statements := '';
    update_statements := '';

    row_diff_join = $sql$
            join lateral (
                select
                --json_object_agg(COALESCE(orig.key, config.key), ('{"default": '||orig.value||', "config": '||config.value||'}')::jsonb) as txt
                string_agg(COALESCE(orig.key, config.key)||' '||coalesce(orig.value, 'null')||' -> '||coalesce(config.value, 'null'), ', ') as difference
                from json_each_text(row_to_json(o)) orig
                join json_each_text(row_to_json(c)) config on config.key = orig.key
                where config.value is distinct from orig.value
                ) d on true
    $sql$;

    for c in select name from ___.type_node
    loop
        raise notice '%', c;
        sql := sql || format($sql$
            select '%1$s_node' as type, n.name, c.config, d.difference, n.geom
            from ___.%1$s_node_config c
            join ___.%1$s_node o on o.id=c.id
            join ___.node n on n.id=c.id %2$s union all $sql$, c, row_diff_join);
        delete_statements := delete_statements ||
            'delete from ___.'||c||E'_node_config where name=old.name and config=old.config;\n';
        update_statements := update_statements ||
            'update ___.'||c||E'_node_config set config=new.config where name=old.name and config=old.config;\n';
    end loop;

    for c in select name from ___.type_singularity
    loop
        raise notice '%', c;
        sql := sql || format($sql$
            select '%1$s_singularity' as type, c.name, c.config, d.difference, n.geom
            from ___.%1$s_singularity_config c
            join ___.%1$s_singularity o on o.id=c.id
            join ___.node n on n.id=c.id %2$s union all $sql$, c, row_diff_join);
        delete_statements := delete_statements ||
            'delete from ___.'||c||E'_singularity_config where name=old.name and config=old.config;\n';
        update_statements := update_statements ||
            'update ___.'||c||E'_singularity_config set config=new.config where name=old.name and config=old.config;\n';
    end loop;

    for c in select name from ___.type_link
    loop
        sql := sql || format($sql$
            select '%1$s_link' as type, l.name, c.config, d.difference, st_lineinterpolatepoint(l.geom, .5) geom
            from ___.%1$s_link_config c
            join ___.%1$s_link o on o.id=c.id
            join ___.link l on l.id=c.id %2$s union all $sql$, c, row_diff_join);
        delete_statements := delete_statements ||
            'delete from ___.'||c||E'_link_config where name=old.name and config=old.config;\n';
        update_statements := update_statements ||
            'update ___.'||c||E'_link_config set config=new.config where name=old.name and config=old.config;\n';
    end loop;

    sql := regexp_replace(sql, 'union all $', '');
    sql := sql || format(') select row_number() over(order by name, config) as id, type, name, config, difference, st_force2d(geom)::geometry(POINT, %s) geom from diff', (select srid from ___.metadata));

    execute sql;

    execute format($sql$
        create function api.configured_instead_fct()
        returns trigger
        language plpgsql volatile security definer as
        $fct$
        begin
            if tg_op = 'INSERT' then
                raise 'no insert allowed';
                return new;
            elsif tg_op = 'UPDATE' then
                %s
                return new;
            elsif tg_op = 'DELETE' then
                %s
                return old;
            end if;
        end;
        $fct$
        $sql$, update_statements, delete_statements);

    create trigger configured_instead_trig
    instead of insert or delete or update on api.configured
    for each row execute procedure api.configured_instead_fct();

end;
$$
;

select 'ok'::varchar;
$template$
;

select create_template();

------------------------------------------------------------------------------------------------
-- API                                                                                        --
------------------------------------------------------------------------------------------------

create or replace function create_api()
returns varchar
language sql volatile as
$api$

drop schema if exists api cascade;

create schema api;

comment on schema api is 'views and functions schema for expresseau, for user access';

------------------------------------------------------------------------------------------------
-- METADATA                                                                                   --
------------------------------------------------------------------------------------------------

select template.basic_view('metadata');

------------------------------------------------------------------------------------------------
-- USER CONFIG                                                                                --
------------------------------------------------------------------------------------------------

select template.basic_view('configuration');
select template.basic_view('scenario_configuration');

--select template.basic_view('user_configuration');

create function api.current_config()
returns varchar
language sql stable security definer as
$$
    select ___.current_config();
$$
;

create function api.set_current_config(name varchar)
returns void
language sql volatile security definer as
$$
    insert into ___.user_configuration(user_, config) values (session_user, name)
    on conflict on constraint user_configuration_pkey do
    update set config=name;
$$
;


------------------------------------------------------------------------------------------------
-- Links 3D geom for view from nodes and 2D geom                                              --
------------------------------------------------------------------------------------------------

create function api.link_3d_geom(geom_2d geometry('LINESTRING'), up_node integer, down_node integer)
returns geometry('LINESTRINGZ')
language sql volatile as
$$
with points as (
    select geom, ST_LineLocatePoint(geom_2d, geom) as pk
    from ST_DumpPoints(geom_2d)
    ),
interpolated as (
    select ST_MakePoint(ST_X(p.geom), ST_Y(p.geom), (1-p.pk) * ST_Z(u.geom) + p.pk * ST_Z(d.geom)) as geom, pk
    from points as p
    join ___.node as d on d.id = down_node
    join ___.node as u on u.id = up_node
    )
select ST_SetSRID(ST_Makeline(geom order by pk), ST_SRID(geom_2d))
from interpolated;
$$
;


------------------------------------------------------------------------------------------------
-- MODELS                                                                                     --
------------------------------------------------------------------------------------------------

select template.basic_view('model');

------------------------------------------------------------------------------------------------
-- COMMENTS                                                                                   --
------------------------------------------------------------------------------------------------

select template.basic_view('comment');

------------------------------------------------------------------------------------------------
-- Water delivery                                                                             --
------------------------------------------------------------------------------------------------

select template.basic_view('water_delivery_sector',
    with_trigger => True,
    start_section =>
    $$
        if tg_op = 'INSERT' or tg_op = 'UPDATE' then
            if exists (
                    select 1
                    from ___.node as n
                    join ___.water_delivery_sector as w on ST_Intersects(w.geom, n.geom)
                    where ST_Intersects(new.geom, n.geom)
                    and w.name != new.name
                    and coalesce(w.name != old.name, true)
                    and n._type = 'user')
                then raise exception 'water delivery sector % would contain user nodes already in another water delivery sector', new.name;
            elsif exists (
                    select 1
                    from ___.node as n
                    where ST_DWithin(ST_Boundary(new.geom), n.geom, 0.0001)
                    and n._type = 'user')
                then raise exception 'water delivery sector % would cross user nodes on its boundary', new.name;
            end if;
        end if;
    $$
);

select template.basic_view('water_delivery_scenario',
    additional_union => 'union all select ''WD_SCN_REF'' as name, ''Reference scenario - not editable'' as comment, ''Weekday'' as period',
    with_trigger=>True);

select template.basic_view('water_delivery_point',
    additional_columns => ('{
        case when ST_LinelocatePoint(l.geom, c.geom)<0.5 then coalesce(nup.id, ndown.id) else coalesce(ndown.id, nup.id) end as _node,
        ST_AsText(ST_Force2D(ST_Makeline(
            ARRAY[
                c.geom,
                ST_ClosestPoint(l.geom, c.geom),
                case when ST_LinelocatePoint(l.geom, c.geom)<0.5 then coalesce(nup.geom, ndown.geom) else coalesce(ndown.geom, nup.geom) end
                ]
            ))) as _affectation_geom
        }')::varchar[],
    additional_join => 'left join ___.link as l on l.id=c.pipe_link
                        left join ___.node nup on nup.id=l.up and nup._type=''user''
                        left join ___.node ndown on ndown.id=l.down and ndown._type=''user''
                        ',
    with_trigger => True,
    start_section =>
    $$
        if tg_op = 'INSERT' and new.pipe_link is null then
            select n.id from ___.link as n join ___.pipe_link p on p.id=n.id where not p.is_feeder order by n.geom <-> new.geom limit 1 into new.pipe_link;
        elsif tg_op = 'UPDATE' and new.industrial is False then
            delete from ___.water_delivery_scenario_industrial_volume where water_delivery_point=new.name;
        end if;
    $$
);

select template.basic_view('fire_hydrant',
    additional_columns => ('{
        case when ST_LinelocatePoint(l.geom, c.geom)<0.5 then l.up else l.down end as _node,
        ST_AsText(ST_Force2D(ST_Makeline(
            ARRAY[
                c.geom,
                ST_ClosestPoint(l.geom, c.geom),
                case when ST_LinelocatePoint(l.geom, c.geom)<0.5 then ST_StartPoint(l.geom) else ST_EndPoint(l.geom) end
                ]
            ))) as _affectation_geom
        }')::varchar[],
    additional_join => 'left join ___.link as l on l.id=c.pipe_link',
    with_trigger => True,
    start_section =>
    $$
        if tg_op = 'INSERT' and new.pipe_link is null then
            select id from ___.link as n where _type='pipe' order by n.geom <-> new.geom limit 1 into new.pipe_link;
        end if;
    $$
);

select template.basic_view('water_delivery_scenario_industrial_volume',
    additional_union => 'union all
                            select
                                s.name as water_delivery_scenario,
                                p.name as water_delivery_point,
                                p.volume as volume,
                                p.industrial as industrial
                            from ___.water_delivery_scenario as s, ___.water_delivery_point as p
                            where p.industrial and not exists (
                                select 1 from ___.water_delivery_scenario_industrial_volume
                                where water_delivery_scenario=s.name and water_delivery_point=p.name
                            )
                         union all
                            select
                                ''WD_SCN_REF'' as water_delivery_scenario,
                                p.name as water_delivery_point,
                                p.volume as volume,
                                p.industrial as industrial
                            from ___.water_delivery_point as p
                            where p.industrial'
);

create function api.water_delivery_scenario_industrial_volume_instead_fct()
returns trigger
language plpgsql volatile security definer as
$$
begin
    if tg_op = 'INSERT' or tg_op = 'UPDATE' then
        insert into ___.water_delivery_scenario_industrial_volume(water_delivery_scenario, water_delivery_point, volume)
            values (new.water_delivery_scenario, new.water_delivery_point, new.volume)
        on conflict (water_delivery_scenario, water_delivery_point)
        do update set volume=excluded.volume;
        return new;
    elsif tg_op = 'DELETE' then
        delete from ___.water_delivery_scenario_industrial_volume
        where water_delivery_scenario=old.water_delivery_scenario and water_delivery_point=old.water_delivery_point;
        return old;
    end if;
end;
$$
;

comment on function api.water_delivery_scenario_industrial_volume_instead_fct() is 'custom trigger to facilitate updating properties
of given pair water delivery scenarios / water delivery points parameters just insert and if pair already exists => on conflict switches to update';

create trigger water_delivery_scenario_industrial_volume_instead_trig
instead of insert or delete or update on api.water_delivery_scenario_industrial_volume
for each row execute procedure api.water_delivery_scenario_industrial_volume_instead_fct();

create view api._user_node_affected_volume as
    select n.id as _user_node,
        sum(case when not wd.industrial then wd.volume else 0 end)::numeric(12,3) as _domestic_volume,
        n.geom as geom,
        array_agg(wd.name) as _sources
    from ___.node as n
    join api.water_delivery_point as wd on wd._node=n.id
    where n._type='user'
    group by n.id, n.geom
;

-- water delivery scenarios / sectors settings
select template.basic_view('water_delivery_scenario_sector_setting',
    additional_union =>
    'union all
        select
            ''WD_SCN_REF'' as scenario,
            s.name as sector,
            coalesce(sum(n._domestic_volume), 0)::numeric(12,3) as volume_m3j,
            1 as adjust_coef,
            s.ref_leak_efficiency as leak_efficiency
        from ___.water_delivery_sector as s
        left join api._user_node_affected_volume as n on ST_Contains(s.geom, n.geom)
        group by s.name'
);

create function api.water_delivery_scenario_sector_setting_instead_fct()
returns trigger
language plpgsql volatile security definer as
$$
begin
    if tg_op = 'INSERT' then
        insert into ___.water_delivery_scenario_sector_setting(scenario, sector, volume_m3j, adjust_coef, leak_efficiency)
            values (new.scenario, new.sector, new.volume_m3j, new.adjust_coef, new.leak_efficiency)
        on conflict (scenario, sector)
        do update set volume_m3j=excluded.volume_m3j, adjust_coef=excluded.adjust_coef, leak_efficiency=excluded.leak_efficiency;
        return new;
    elsif tg_op = 'UPDATE' then
        update ___.water_delivery_scenario_sector_setting
        set scenario=new.scenario, sector=new.sector, volume_m3j=new.volume_m3j, adjust_coef=new.adjust_coef, leak_efficiency=new.leak_efficiency
        where scenario=old.scenario and sector=old.sector;
        return new;
    elsif tg_op = 'DELETE' then
        delete from ___.water_delivery_scenario_sector_setting
        where scenario=old.scenario and sector=old.sector;
        return old;
    end if;
end;
$$
;

comment on function api.water_delivery_scenario_sector_setting_instead_fct() is 'custom trigger to facilitate updating properties
of given pair water delivery scenarios / sectors parameters just insert and if pair already exists => on conflict switches to update';

create trigger water_delivery_scenario_sector_setting_instead_trig
instead of insert or delete or update on api.water_delivery_scenario_sector_setting
for each row execute procedure api.water_delivery_scenario_sector_setting_instead_fct();


------------------------------------------------------------------------------------------------
-- Settings                                                                                   --
------------------------------------------------------------------------------------------------

select template.basic_view('fluid_properties', with_trigger=>True, cannot_delete=>True);
select template.basic_view('material');
select template.basic_view('node', additional_columns => '{c._type, c.model as _model,  coalesce(zground, 9999) - coalesce(ST_Z(geom), 0) as depth}');
select template.basic_view('link', additional_columns => '{n.model as _model, c._type}', additional_join => 'join ___.node as n on n.id=c.up');
select template.basic_view('singularity', additional_columns => '{n.geom, n.model as _model, c._type}', additional_join => 'join ___.node as n on n.id=c.id');
select template.basic_view('hourly_modulation_curve', with_trigger=>True);
select template.basic_view('failure_curve', with_trigger=>True);
select template.basic_view('threshold_warning_settings', with_trigger=>True, cannot_delete=>True);
select template.basic_view('sectorization_settings', with_trigger=>True, cannot_delete=>True);



------------------------------------------------------------------------------------------------
-- Pipes celerity                                                                             --
------------------------------------------------------------------------------------------------

create function api.celerity(pipe_elasticity double precision, pipe_diameter real, pipe_thickness real)
returns numeric
language sql stable security definer as
$$
select round((1./sqrt(volumic_mass_kg_m3/volumic_elasticity_module_N_m2 + volumic_mass_kg_m3*pipe_diameter/(pipe_elasticity*pipe_thickness)))::numeric, 2)
from ___.fluid_properties;
$$
;


------------------------------------------------------------------------------------------------
-- NODES                                                                                      --
------------------------------------------------------------------------------------------------

select template.inherited_view('user', 'node',
    additional_columns => '{s.name as _sector, coalesce(a.zground, 9999) - coalesce(ST_Z(a.geom), 0) as depth}',
    additional_join => 'left join ___.water_delivery_sector as s on ST_Contains(s.geom, a.geom)',
    specific_geom => 'ST_Force2D(a.geom)::geometry(POINT,'||(select srid from ___.metadata)||')',
    specific_columns => '{"domestic_curve": "coalesce(c.domestic_curve, ''Constant'')",
                          "industrial_curve": "coalesce(c.industrial_curve, ''Constant'')"}',
    start_section =>
    $$
        if tg_op = 'INSERT' or (tg_op = 'UPDATE' and (not new.geom = old.geom)) then
            if (select count(*) > 1 from ___.water_delivery_sector where ST_Intersects(new.geom, geom))
                then raise exception 'user node % would be in multiple water delivery sectors', new.name;
            elsif (select exists (select 1 from ___.water_delivery_sector where ST_DWithin(new.geom, ST_Boundary(geom), 0.0001)))
                then raise exception 'user node % would be on water delivery sector boundary', new.name;
            end if;
        end if;

        if tg_op = 'INSERT' or tg_op = 'UPDATE' then
            select ST_SetSRID(ST_MakePoint(ST_X(new.geom), ST_Y(new.geom), coalesce(new.zground, 9999)-coalesce(new.depth, 0)), ST_SRID(new.geom)) into overloaded_geom;
            select nullif(new.domestic_curve, 'Constant') into new.domestic_curve;
            select nullif(new.industrial_curve, 'Constant') into new.industrial_curve;
        end if;
    $$,
    after_update_section =>
    $$
        if not new.geom = old.geom then
            update ___.link set geom=st_setpoint(geom, 0, new.geom) where up=new.id;
            update ___.link set geom=st_setpoint(geom, -1, new.geom) where down=new.id;
        end if;
    $$

);

select template.inherited_view('reservoir', 'node',
    additional_columns => '{coalesce(a.zground, 9999) - coalesce(ST_Z(a.geom), 0) as depth}',
    specific_geom => 'ST_Force2D(a.geom)::geometry(POINT,'||(select srid from ___.metadata)||')',
    start_section =>
    $$
        if tg_op = 'INSERT' or tg_op = 'UPDATE' then
            select ST_SetSRID(ST_MakePoint(ST_X(new.geom), ST_Y(new.geom), coalesce(new.zground, 9999)-coalesce(new.depth, 0)), ST_SRID(new.geom)) into overloaded_geom;
        end if;
    $$,
    after_update_section =>
    $$
        --if new.geom != old.geom then
        if not St_equals(old.geom,new.geom) then
            update ___.link set geom=st_setpoint(geom, 0, new.geom) where up=new.id;
            update ___.link set geom=st_setpoint(geom, -1, new.geom) where down=new.id;
        end if;
    $$
);


------------------------------------------------------------------------------------------------
-- SINGULARITIES                                                                              --
------------------------------------------------------------------------------------------------

select template.inherited_view(name::varchar, 'singularity',
    additional_columns => ('{n.model as _model, ST_Force2D(n.geom)::geometry(POINT,'||(select srid from ___.metadata)||') as geom}')::varchar[],
    additional_join => 'join ___.node as n on n.id=a.id',
    start_section =>
    E'if tg_op = ''INSERT'' or tg_op = ''UPDATE'' then\n'||
    E'      select id, model from ___.node where ST_Equals(new.geom, geom) and new.geom && geom into new.id, new._model;\n'||
    E'      if (select exists(select 1 from ___.singularity as s join ___.node as n on n.id=s.id where s.id!=new.id and s.name=new.name and n.model=new._model))\n'||
    E'          then raise exception ''a singularity named % already exists in model %'', new.name, new._model;\n'||
    E'      end if;\n'||
    case when name='chlorine_injection' then
    E'      select coalesce(new.target_node, new.id, old.target_node) into new.target_node;\n'
    else E'' end||
    E'  end if;\n'
)
from ___.type_singularity
;


------------------------------------------------------------------------------------------------
-- LINKS                                                                                      --
------------------------------------------------------------------------------------------------

select template.inherited_view('pipe'::varchar, 'link',
    additional_columns => '{n.model as _model, m.roughness_mm as _roughness_mm, m.elasticity_N_m2 as _elasticity_N_m2, api.celerity(m.elasticity_N_m2, c.diameter, (c.thickness_mm/1000.)::real) as _celerity}',
    additional_join => 'left join ___.material as m on m.name=c.material join ___.node as n on n.id=a.up',
    --specific_geom => 'api.link_3d_geom(a.geom , a.up, a.down)',
    specific_geom => 'ST_Force2D(a.geom)::geometry(LINESTRING,'||(select srid from ___.metadata)||')',
    start_section =>
    $$
        if tg_op = 'INSERT' or tg_op = 'UPDATE' then
            -- nullif(A, nullif(A, B)) returns null if A!=B else return A(=B)
            select u.id, d.id, nullif(u.model, nullif(u.model, d.model))
            from ___.node as u, ___.node as d
            where ST_Equals(ST_StartPoint(new.geom), u.geom) and ST_Equals(ST_EndPoint(new.geom), d.geom) and
            ST_StartPoint(new.geom) && u.geom and ST_EndPoint(new.geom) && d.geom
            into new.up, new.down, new._model;
            if new._model is null
                then raise exception 'upstream and downstream nodes cannot be found or not in the same model for geometry %', new.geom;
            elsif (select exists(select 1 from ___.link as l join ___.node as n on n.id=l.up where l.id!=new.id and l.name=new.name and n.model=new._model))
                then raise exception 'a link named % already exists in model %', new.name, new._model;
            end if;
        end if;
    $$
);

create function api.headloss_bend(angle real, k90 real default 0.15)
returns real
language sql immutable security definer as
$$
select (k90*c)::real
from (values (1, 0.), (10, .2), (20, .38), (30, .5), (40, .62), (50, .73), (60, .81), (70, .89), (80, .95), (90, 1), (100, 1.4), (110, 1.09), (120, 1.12) ) as t(theta, c)
order by abs(angle-theta) asc limit 1;
$$
;

create function api.k90(radius real, diameter real, is_smooth boolean)
returns real
language sql immutable security definer as
$$
select case when radius is null then 0.15::real when is_smooth then smooth_coef::real else not_smooth_coef::real end
from (values (1, .15, .5), (2, .15, .3), (3, .12, .28), (4, .11, .23), (5, .09, .2), (6, .08, .18), (7, .08, .17), (8, .08, .18), (9, .09, .19), (10, .09, .2)) as t(r_over_d, smooth_coef, not_smooth_coef)
order by abs(radius/diameter - r_over_d) asc limit 1;
$$
;

select template.basic_view('pipe_link_singularity',
    with_trigger => True,
    additional_columns => $${c.number*(
        case when c.type_singularity='Bend' then
            api.headloss_bend(c.bend_angle, api.k90(c.bend_radius, p.diameter, c.bend_is_smooth))
        else
            c.borda_coef*((p.diameter^2/4) / c.borda_section)^2
        end) as _k_equivalent}$$,
    additional_join => 'left join ___.pipe_link p on p.id=c.pipe_link'
    );

select template.inherited_view(name::varchar, 'link',
    additional_columns => '{n.model as _model}',
    additional_join => 'join ___.node as n on n.id=a.up',
    --specific_geom => 'api.link_3d_geom(a.geom , a.up, a.down)',
    specific_geom => 'ST_Force2D(a.geom)::geometry(LINESTRING,'||(select srid from ___.metadata)||')',
    start_section =>
    E'if tg_op = ''INSERT'' or tg_op = ''UPDATE'' then\n'||
    E'      select ST_Force2D(new.geom) into new.geom;\n'||
    E'      -- nullif(u.model, nullif(u.model, d.model)) returns null if models do not match else return the value if it match\n'||
    E'      select u.id, d.id, nullif(u.model, nullif(u.model, d.model))\n'||
    E'      from ___.node as u, ___.node as d\n'||
    E'      where ST_Equals(ST_StartPoint(new.geom), u.geom) and ST_Equals(ST_EndPoint(new.geom), d.geom)\n'||
    E'      and ST_StartPoint(new.geom) && u.geom and ST_EndPoint(new.geom) && d.geom\n'||
    E'      into new.up, new.down, new._model;\n'||
    E'      if new._model is null\n'||
    E'          then raise exception ''upstream and downstream nodes are not in the same model'';\n'||
    E'      elsif (select exists(select 1 from ___.link as l join ___.node as n on n.id=l.up where l.id!=new.id and l.name=new.name and n.model=new._model))\n'||
    E'          then raise exception ''a link named % already exists in model %'', new.name, new._model;\n'||
    E'      end if;\n'||
    case when name='pump' then
    E'      select coalesce(new.target_node, new.down, old.target_node) into new.target_node;\n'
    else E'' end||
    E'  end if;\n'
)
from ___.type_link
where name::varchar != 'pipe'
;


create view api.pressure_regulator_downstream_sector as
with
node_volume as (
    select id, sum(volume) as volume from
    (
        select p.up as id, 0.5*(case when p.overload_length then p.overloaded_length else st_length(p.geom) end * pi() * p.diameter ^ 2 / 4.)  as volume
        from api.pipe_link p
        union all
        select p.down as id, 0.5*(case when p.overload_length then p.overloaded_length else st_length(p.geom) end * pi() * p.diameter ^ 2 / 4.) as volume
        from api.pipe_link p
    ) as t
    group by id
),
sector_node as (
    select
        t.node,
        t.component as sector,
        sum(v.volume) over (partition by t.component) as downstream_volume
    from pgr_connectedcomponents('
        with excluded as (
            select id, up, down from api.pipe_link where not status_open
            union all
            select id, up, down from api.valve_link where c_opening = 0
            union all
            select id, up, down from api.pressure_regulator_link
        ),
        filtered as (
            select id, up, down
            from
            (
                select id, up, down from api.link
                except
                select id, up, down from excluded
            ) as t
            where (up not in (select id from api.reservoir_node) and down not in (select id from api.reservoir_node))
        )
        select id, up as source, down as target, 1 as cost, 1 as reverse_cost from filtered
    '::text) t(seq, component, node)
    join node_volume v on t.node=v.id
)
select n.id, n.name, n.geom, v.sector, v.downstream_volume
from api.node n
join sector_node v on v.node=n.id
;


create view api.pressure_regulator_downstream_volume as
with
node_volume as (
    select id, sum(volume) as volume from
    (
        select p.up as id, 0.5*(case when p.overload_length then p.overloaded_length else st_length(p.geom) end * pi() * p.diameter ^ 2 / 4.)  as volume
        from api.pipe_link p
        union all
        select p.down as id, 0.5*(case when p.overload_length then p.overloaded_length else st_length(p.geom) end * pi() * p.diameter ^ 2 / 4.) as volume
        from api.pipe_link p
    ) as t
    group by id
),
sector_node as (
    select
        t.node,
        t.component as sector,
        sum(v.volume) over (partition by t.component) as downstream_volume
    from pgr_connectedcomponents($query$
        with excluded as (
            select id, up, down from api.pipe_link where not status_open
            union all
            select id, up, down from api.valve_link where c_opening = 0
            union all
            select id, up, down from api.pressure_regulator_link
        ),
        filtered as (
            select id, up, down
            from
            (
                select id, up, down from api.link
                except
                select id, up, down from excluded
            ) as t
            where (up not in (select id from api.reservoir_node) and down not in (select id from api.reservoir_node))
        )
        select id, up as source, down as target, 1 as cost, 1 as reverse_cost from filtered
    $query$) t(seq, component, node)
    join node_volume v on t.node=v.id
)
select l.id, l.name, n.downstream_volume, st_endpoint(l.geom) as geom
from api.pressure_regulator_link l
join sector_node n on l.down = n.node
;


------------------------------------------------------------------------------------------------
-- SCENARIOS                                                                                  --
------------------------------------------------------------------------------------------------

select template.basic_view('scenario',
    with_trigger => True,
    specific_columns => '{"water_delivery_scenario": "coalesce(c.water_delivery_scenario, ''WD_SCN_REF'')"}',
    start_section =>
    $$
        if tg_op = 'INSERT' or tg_op = 'UPDATE' then
            select nullif(new.water_delivery_scenario, 'WD_SCN_REF') into new.water_delivery_scenario;
        end if;
    $$
);

select template.basic_view('groupe');
select template.basic_view('groupe_model');
select template.basic_view('mixed');
select template.basic_view('cascade');

select template.basic_view('regulation_scenario');
select template.basic_view('hydrology_scenario');

select template.basic_view('scenario_link_failure', with_trigger=>True);
select template.basic_view('scenario_pump_failure', with_trigger=>True);

select template.basic_view('scenario_water_origin',
    with_trigger => True,
    additional_columns => '{coalesce(si.name, sf.name, sc.name) as name, (case when si.name is not null then ''imposed_piezometry'' when sf.name is not null then ''flow_injection'' else ''model_connection'' end)::___.type_singularity_ as _type, n.model as model}',
    additional_join => 'join ___.node as n on n.id=coalesce(c.imposed_piezometry_singularity, c.flow_injection_singularity, c.model_connection_singularity) left join ___.imposed_piezometry_singularity as si on si.id=c.imposed_piezometry_singularity left join ___.flow_injection_singularity as sf on sf.id=c.flow_injection_singularity left join ___.model_connection_singularity as sc on sc.id=c.model_connection_singularity ',
    start_section =>
    $$
        if tg_op = 'INSERT' and (select count(1) from ___.scenario_water_origin where scenario=new.scenario) >= 8 then
            raise 'cannot specify more than 8 water origin sources';
        end if;

        if tg_op = 'INSERT' or tg_op = 'UPDATE' then
            if new.name is not null and new.model is not null then
                select id from api.imposed_piezometry_singularity where name=new.name and _model=new.model
                into new.imposed_piezometry_singularity;
                select id from api.flow_injection_singularity where name=new.name and _model=new.model
                into new.flow_injection_singularity;
                select id from api.model_connection_singularity where name=new.name and _model=new.model
                into new.model_connection_singularity;
            end if;
        end if;
    $$
);


------------------------------------------------------------------------------------------------
-- Sectorization                                                                              --
------------------------------------------------------------------------------------------------

create view api.sector as
with sector_node as (
    select id, component as sector, node, geom from
    pgr_connectedComponents($query$
    with excluded as (
        select id, up, down from api.pipe_link where not status_open or (meter and (select pipe_link_meter from api.sectorization_settings))
        union all
        select id, up, down from api.valve_link where c_opening = 0 or (select valve_link_open from api.sectorization_settings)
        union all
        select id, up, down from api.pressure_regulator_link where (select pressure_regulator_link from api.sectorization_settings)
        union all
        select id, up, down from api.flow_regulator_link where (select flow_regulator_link from api.sectorization_settings)
    ),
    filtered as (
        select id, up, down
        from
        (
            select id, up, down from api.link
            except
            select id, up, down from excluded
        ) as t
        where not (select reservoir_node from api.sectorization_settings)
        or (up not in (select id from api.reservoir_node) and down not in (select id from api.reservoir_node))
    )
    select id, up as source, down as target, 1 as cost, 1 as reverse_cost from filtered
    $query$
    ) as t
    join api.node on id=node
),
sector_map as (
    select row_number() over() as to_, sector as from_
    from (select distinct sector from sector_node) t
)
select l.id, m.to_ as sector, l.geom, l._type, coalesce(p.diameter, 0) as diameter
from ___.link l
join sector_node u on u.id = l.up
join sector_node d on d.id = l.down
join sector_map m on m.from_=u.sector
left join ___.pipe_link p on p.id = l.id
where u.sector = d.sector
;

comment on view api.sector is 'given a set of properties, certain links and nodes split the network into sectors separated from each other
a sector "0" is created for the links that split so the end result includes all links';

------------------------------------------------------------------------------------------------
-- Skeletonization
------------------------------------------------------------------------------------------------

do $$
begin
    execute $script$
    drop schema if exists skl cascade;
    create schema skl;

    create materialized view skl.skl_network as
    with excluded as (
        select l.id
        from api.pipe_link l
        join api.reservoir_node n on n.id in (l.up, l.down)
        union all
        select l.id
        from api.pipe_link l
        join api.singularity s on s.id in (l.up, l.down)-- and s._type != 'model_connection'
    )
    select id, up, down, geom, st_length(geom)/(diameter^5) as cost
    from api.pipe_link
    where status_open and not meter and id not in (select id from excluded)
    with no data
    ;

    create index network_geom_idx on skl.skl_network using gist(geom);
    create index network_up_idx on skl.skl_network(up);
    create index network_down_idx on skl.skl_network(down);

    create materialized view skl.skl_sector_ as
    with node as (
        select id, component as sector, node, geom from
        pgr_connectedComponents($query$
        select id, up as source, down as target, cost, cost as reverse_cost from skl.skl_network
        $query$
        ) as t
        join api.node on id=node
    ),
    map as (
        select row_number() over() as to_, sector as from_
        from (select distinct sector from node) t
    )
    select n.node as id, m.to_ as sector, n.geom, nn.name, nn._sector as delivery_sector, nn.domestic_curve, nn.industrial_curve, sum(coalesce(wdd.volume,0)) as domestic_volume, sum(coalesce(wdi.volume,0)) industrial_volume, nn.q0, nn.model, nn.depth, nn.zground
    from node n
    join api.user_node nn on nn.id=n.id
    join map m on m.from_=n.sector
    left join api.water_delivery_point wdd on wdd._node=n.id and not wdd.industrial
    left join api.water_delivery_point wdi on wdi._node=n.id and wdi.industrial
    group by n.node, m.to_, n.geom, nn._sector, nn.domestic_curve, nn.industrial_curve, nn.q0, nn.name, nn.model, nn.depth, nn.zground
    with no data
    ;

    create index sector_node_id_idx on skl.skl_sector_(id);
    create index sector_node_sector_idx on skl.skl_sector_(sector);

    create materialized view skl.skl_sector as
    select l.id, u.sector, u.name, l.geom, l.cost, l.up, l.down
    from skl.skl_network l
    join skl.skl_sector_ u on u.id = l.up
    join skl.skl_sector_ d on d.id = l.down
    where u.sector = d.sector
    with no data
    ;

    create index sector_geom_idx on skl.skl_sector using gist(geom);

    -- un point par courbe de conso et par secteur de conso, le plus gros diametre
    create materialized view skl.skl_delivery_node as
    with v as (
        select domestic_curve, industrial_curve, sector, delivery_sector, sum(domestic_volume) as domestic_volume, sum(industrial_volume) as industrial_volume, sum(q0) as q0, model --, avg(zground) as zground, avg(depth) as depth
        from skl.skl_sector_
        group by domestic_curve, industrial_curve, sector, delivery_sector, model
    )
    select distinct t.node, t.sector, t.name, t.geom, t.domestic_curve, t.industrial_curve, t.delivery_sector, t.model, t.zground, t.depth, v.domestic_volume, v.industrial_volume, v.q0
    from (
        select n.id as node, n.sector, n.name, n.geom, n.domestic_curve, n.industrial_curve, n.delivery_sector, n.model, n.zground, n.depth,
            row_number() over(
                partition by n.domestic_curve, n.industrial_curve, n.sector, n.delivery_sector, n.model
                order by diameter desc) as rk
        from skl.skl_sector_ n
        join api.pipe_link l on n.id in (l.up, l.down)
        ) t
    join v on (v.domestic_curve is not distinct from t.domestic_curve) and (v.industrial_curve is not distinct from t.industrial_curve) and v.sector=t.sector and v.delivery_sector=t.delivery_sector and v.model=t.model
    where rk = 1
    with no data
    ;

    create materialized view skl.skl_node_sector as
    select distinct * from (
    select l.up as node, s.sector, n.name, st_startpoint(l.geom) as geom, n.model as model, n.depth, n.zground
    from api.pipe_link l
    left join skl.skl_sector s on l.up in (s.up, s.down)
    join api.node n on n.id=l.up and n._type='user'
    where meter
    union all
    select l.down as node, s.sector, n.name, st_endpoint(l.geom) as geom, n.model as model, n.depth, n.zground
    from api.pipe_link l
    left join skl.skl_sector s on l.down in (s.up, s.down)
    join api.node n on n.id=l.down and n._type='user'
    where meter
    union all
    select l.up as node, s.sector, n.name, st_startpoint(l.geom) as geom, n.model as model, n.depth, n.zground
    from api.link l
    left join skl.skl_sector s on l.up in (s.up, s.down)
    join api.node n on n.id=l.up
    where l._type != 'pipe' and n._type='user'
    union all
    select l.down as node, s.sector, n.name, st_endpoint(l.geom) as geom, n.model as model, n.depth, n.zground
    from api.link l
    left join skl.skl_sector s on l.down in (s.up, s.down)
    join api.node n on n.id=l.down
    where l._type != 'pipe' and n._type='user'
    union all
    select u.up as node, s.sector, n.name, st_startpoint(u.geom) as geom, n.model as model, n.depth, n.zground
    from api.singularity r
    join api.pipe_link u on u.down=r.id
    left join skl.skl_sector s on u.up in (s.up, s.down)
    join api.node n on n.id=u.up and n._type='user'
    --where r._type != 'model_connection'
    union all
    select d.down as node, s.sector, n.name, st_endpoint(d.geom) as geom, n.model as model, n.depth, n.zground
    from api.singularity r
    join api.pipe_link d on d.up=r.id
    left join skl.skl_sector s on d.down in (s.up, s.down)
    join api.node n on n.id=d.down and n._type='user'
    --where r._type != 'model_connection'
    union all
    select d.down as node, s.sector, n.name, st_endpoint(d.geom) as geom, n.model, n.depth, n.zground
    from api.reservoir_node r
    join api.pipe_link d on d.up=r.id
    left join skl.skl_sector s on d.down in (s.up, s.down)
    join api.node n on n.id=d.down and n._type='user'
    union all
    select u.up as node, s.sector, n.name, st_startpoint(u.geom) as geom, n.model, n.depth, n.zground
    from api.reservoir_node r
    join api.pipe_link u on u.down=r.id
    left join skl.skl_sector s on u.up in (s.up, s.down)
    join api.node n on n.id=u.up and n._type='user'
    union all
    select node, sector, name, st_force2d(geom) as geom, model, depth, zground
    from skl.skl_delivery_node
    union all
    select n.id as node, s.sector, n.name, st_force2d(n.geom) as geom, n.model as model, n.depth, n.zground
    from api.singularity r
    join api.node n on n.id=r.id
    left join skl.skl_sector s on n.id in (s.up, s.down)
    where r._type = 'model_connection'
    ) t
    with no data
    ;

    create index node_sector_sector_idx on skl.skl_node_sector(sector);
    create index node_sector_node_idx on skl.skl_node_sector(node);

    create materialized view skl.skl_pipe as
    with skl as (
        select distinct a.sector, s.geom, a.model
        from skl.skl_node_sector as a
        join skl.skl_node_sector as b on b.sector = a.sector and b.node > a.node
        cross join lateral pgr_dijkstra(format(
            $query$
            select id, up as source, down as target, cost, cost as reverse_cost from skl.skl_sector where sector=%1$s
            $query$, a.sector), a.node, b.node) as d
        join skl.skl_sector s on s.id = d.edge and s.sector=a.sector
    )
    select row_number() over() as id, t.*, up.id as up, down.id as down
    from (
        select 'SKL_'||row_number() over() as name, s.sector, d.geom, sum(st_length(l.geom)*l.diameter)/sum(st_length(l.geom)) as diameter, sum(st_length(l.geom)*l._roughness_mm)/sum(st_length(l.geom)) as roughness_mm, sum(st_length(l.geom)*l._celerity)/sum(st_length(l.geom)) as celerity, model
        from (select sector, st_linemerge(st_collect(geom)) as geom, model from skl group by sector, model) s
        cross join st_dump(s.geom) as d
        join api.pipe_link l on st_covers(d.geom, l.geom)
        group by s.sector, d.geom, s.model
        union -- pipe connectés aux singularités, aux réservoirs et compteurs
        select p.name, null as sector, p.geom, p.diameter, _roughness_mm as roughness_mm, _celerity as celerity, p._model as model
        from api.pipe_link p where  meter
        union
        select p.name, null as sector, p.geom, p.diameter, _roughness_mm as roughness_mm, _celerity as celerity, p._model as model
        from api.pipe_link p
        join api.reservoir_node n on n.id in (p.up, p.down)
        union all
        select p.name, null as sector, p.geom, p.diameter, _roughness_mm as roughness_mm, _celerity as celerity, p._model as model
        from api.pipe_link p
        join api.singularity s on s.id in (p.up, p.down) and s._type != 'model_connection'
    ) t
    join api.node up on up.geom && t.geom and st_intersects(up.geom, st_startpoint(t.geom))
    join api.node down on down.geom && t.geom and st_intersects(down.geom, st_endpoint(t.geom))
    with no data
    ;

    create index skeleton_geom_idx on skl.skl_pipe using gist(geom);
    create index skeleton_up_idx on skl.skl_pipe(up);
    create index skeleton_down_idx on skl.skl_pipe(down);

    create materialized view skl.skl_node as
    select node, sector, name, geom, model, depth, zground
    from skl.skl_node_sector s
    union
    select distinct n.id as node, null::bigint as sector, n.name, st_force2d(n.geom) as geon, n.model, n.depth, n.zground
    from api.node n
    join skl.skl_pipe l on n.id in (l.up, l.down)
    where  n._type='user' and not exists (select 1 from skl.skl_node_sector ns where ns.node=n.id)
    with no data
    ;

    $script$;

end;
$$
;


create function api.skeletonize()
returns void
language sql as
$$
    refresh materialized view skl.skl_network;
    refresh materialized view skl.skl_sector_;
    refresh materialized view skl.skl_sector;
    refresh materialized view skl.skl_delivery_node;
    refresh materialized view skl.skl_node_sector;
    refresh materialized view skl.skl_pipe;
    refresh materialized view skl.skl_node;
$$
;

------------------------------------------------------------------------------------------------
-- Export utility                                                                             --
------------------------------------------------------------------------------------------------
create function api._bool_to_text(flag boolean)
returns text
language sql immutable security definer as
$$
    select case when flag then '.true.' else '.false.' end;
$$
;

comment on function api._bool_to_text(boolean) is 'converts an boolean to text for export (either .true. or .false.)';

create function api._array_to_text(anyarray, integer default 0)
returns text
language sql immutable security definer as
$$
with arrays as (
    select d1, array_agg($1[d1][d2] order by d1)as a
    from generate_subscripts($1,1) d1, generate_subscripts($1,2) d2
    group by d1
        union all
    select d1, array_fill(0, array[array_length($1, 2)]) as a
    from generate_series(array_length($1, 1) + 1, $2) as d1
    order by d1
)
select string_agg(array_to_string(a, ' '), E'\n') from arrays
$$
;

comment on function api._array_to_text(anyarray, integer) is 'converts an array to text for export:
x1 y1
x2 y2
x3 y3
...';

create function api._export_header(concrete varchar, abstract varchar, model_name varchar, excluded_name varchar[] default '{}'::varchar[])
returns text
language plpgsql stable security definer as
$$
declare
    _code varchar;
    _id varchar;
    _count integer;
    _res varchar;
    _query varchar;
begin
    execute 'select exportcode, exportid  from ___.type_'||abstract||' where name='''||concrete||'''' into _code, _id;

    _query := format($query$
        select count(*)
        from api.%s_%s
        where %smodel='%s' and name != all('%s'::varchar[])
        $query$, concrete, abstract, case when  abstract='node' then '' else '_' end, model_name, excluded_name::varchar);
    -- raise notice '%', _query;
    execute _query into _count;

    return format(
$res$
$%s (%s_%s)
%s %s
$res$,
_code, concrete, abstract,
_id, _count
    );
end;
$$
;

comment on function api._export_header(concrete varchar, abstract varchar, model_name varchar, excluded_name varchar[]) is 'creates a custom header for hydra export:
$export_code (table_name)
export_id object_count';

create function api._interval_to_hms(_interval interval)
returns text
language sql immutable security definer as
$$
select floor(extract('epoch' from _interval)/3600)||' '||to_char(_interval, 'MI SS')
$$
;

comment on function api._interval_to_hms(_interval interval) is 'write an interval as "HH MM SS". HH (hours) can exceed 24.';

------------------------------------------------------------------------------------------------
-- Export functions                                                                           --
------------------------------------------------------------------------------------------------

create function api.model_grouping(scenario_name varchar)
returns table(priority integer, models varchar)
language sql stable security definer as
$$
with model_global as (
    select 1 as priority, name as model
    from api.model
    order by name
    ),
model_cascade as (
    select priority, model
    from (
        select priority, model from api.cascade where scenario=scenario_name
            union all
        select 999 as priority, name as model
        from api.model
        where not exists (select 1 from api.cascade where scenario=scenario_name and name=model)
        ) as patched_cascadey
    ),
model_mixed as (
    select priority, model
    from api.groupe_model as gp
    join api.mixed as m on gp.groupe=m.groupe
    where m.scenario=scenario_name
    )
select priority, string_agg(model, E' ' order by model) as models from model_global join api.scenario on model_connect_settings='Global' where name=scenario_name group by priority
    union all
select priority, model as models from model_cascade join api.scenario on model_connect_settings='Cascade' where name=scenario_name
    union all
select priority, string_agg(model, E' ' order by model) as models from model_mixed join api.scenario on model_connect_settings='Mixed' where name=scenario_name group by priority;
$$
;

create function api.file_scnnom(scenario_name varchar)
returns text
language sql stable security definer as
$$
with scenario_settings as (
    select
        case when comput_mode='Steady state' then 1
             when comput_mode='Gradual transient' then 2
             when comput_mode='Fast transient' then 3
             when comput_mode='Exterior fire defense nominal flow' then 2
             when comput_mode='Exterior fire defense flow at 1bar' then 2
             when comput_mode='Break criticality' then 2
        end as computation_mode,
        scenario_ref
    from api.scenario
    where name=scenario_name
    ),
modeles as (
    select string_agg(name, E' ') as list from api.model
    ),
grouping as (
    select string_agg(models, E'\n' order by priority) as list from api.model_grouping(scenario_name)
    )
select format(
$scnnom$
*scenario
%s

*modeles
%s

*computation_mode
%s

*groupage
%s

%s

$scnnom$,
scenario_name,
modeles.list,
scenario_settings.computation_mode,
grouping.list,
coalesce(E'*scnref\n'||scenario_settings.scenario_ref, '')
)
from scenario_settings, modeles, grouping;
$$
;

create function api.file_cmd(scenario_name varchar)
returns text
language sql stable security definer as
$$
with water_origin as (
    select string_agg(format($pol_origin$%s %s$pol_origin$, model, name), E'\n' order by name) as pol_origin
    from api.scenario_water_origin
    where scenario=scenario_name
    ),
data_dactv as (
    select row_number() over() as n, t.exportid, l.name,
        api._interval_to_hms(f.failure_timestamp) as failure_timestamp, f.failure_curve
    from api.scenario_link_failure as f
    join ___.type_link as t on t.name=f.link_type
    join ___.link as l on l.id=f.link
    where f.scenario = scenario_name
    ),
bloc_dactv as (
    select count(*) as numel, string_agg(format(
        E'%s %s %s\n%s %s',
        n, exportid, name,
        failure_timestamp, failure_curve
        ), E'\n') as text
    from data_dactv
    ),
data_dactp as (
    select row_number() over() as n, exportid, l.name,
        api._interval_to_hms(f.failure_timestamp) as failure_timestamp, lower(replace(f.failure_mode::text, ' ', '_')) as failure_mode,
        case when f.failure_mode='Automatic alpha computation' then '    '
             when f.failure_mode='Alpha(t) curve' then f.failure_curve
             when f.failure_mode='Four quadrants curve' then (select four_quadrant_file from api.scenario where name=scenario_name)
        end as additional_data
    from api.scenario_pump_failure as f
    join ___.type_link as t on t.name=f.link_type
    join ___.link as l on l.id=f.link
    where f.scenario = scenario_name
    ),
bloc_dactp as (
    select count(*) as numel, string_agg(format(
        E'%s %s %s\n%s %s\n%s',
        n, exportid, name,
        failure_timestamp, failure_mode,
        additional_data
        ), E'\n') as text
    from data_dactp
    ),
bloc_regul as (
    select count(*) as n, string_agg(regulation_file, E'\n' order by priority) as list
    from api.regulation_scenario where scenario=scenario_name
    ),
bloc_hydro as (
    select count(*) as n, string_agg(hydrology_file, E'\n' order by hydrology_file) as list
    from api.hydrology_scenario where scenario=scenario_name
    )
select format(
$cmd$
*time
%s%s

%s

%s

%s

%s

%s

%s

%s

%s

%s

%s

$cmd$,
to_char(starting_date, 'YYYY MM DD HH24 MI'),
case when comput_mode!='Steady state' then format(
$time_details$
%s
%s
%s
$time_details$,
    api._interval_to_hms(duration),
    timestep,
    api._interval_to_hms(tini)) else '' end,
case when comput_mode!='Steady state' then format(
$sor$
*sor
%s
%s
%s
$sor$,
    dt_output,
    api._interval_to_hms(tbegin_output_hr),
    api._interval_to_hms(tend_output_hr)) else '' end,
case when flag_save then format(
$save$
*save
%s
$save$,
    api._interval_to_hms(tsave_hr)) else '' end,
case when scenario_rstart is not null then format(
$rstart$
*rstart
%s
%s
$rstart$,
    scenario_rstart, api._interval_to_hms(trstart_hr)) else '' end,
case when refined_discretisation then '*refine_dx' else '' end,
case when vaporization then '*vaporization' else '' end,
case when quality_output='Tracer' then format(
$tracer$
*pol_tracer
%s
$tracer$,
    case when chlorine then 'chlorine '||coalesce(chlorine_water_reaction_coef, 0)||' '||coalesce(chlorine_wall_reaction_coef, 0)||E'\n' else '' end ||
    case when trihalomethane then 'trihalomethane '||coalesce(trihalomethane_reaction_parameter, 0)||' '||coalesce(trihalomethane_concentration_limit, 0)||E'\n' else '' end ||
    case when passive_tracer then E'passive_tracer 0 0\n' else '' end ||
    case when travel_time then E'travel_time 0 0\ncontact_time 0 0\n' else '' end
) when quality_output='Water origin' then format(
$water_origin$
*pol_origin
%s
$water_origin$,
    pol_origin) else '' end,
case when bloc_dactv.numel>0 then format(
$dactv$
*dactv
2 %s
%s
$dactv$,
    bloc_dactv.numel, bloc_dactv.text) else '' end,
case when bloc_dactp.numel>0 then format(
$dactp$
*dactp
3 %s
%s
$dactp$,
    bloc_dactp.numel, bloc_dactp.text) else '' end,
case when bloc_regul.n>0 then format(
$regul$
*f_regul
%s
$regul$,
    bloc_regul.list) else '' end,
case when bloc_hydro.n>0 then format(
$regul$
*f_hy_externe
%s
$regul$,
    bloc_hydro.list) else '' end
)
from api.scenario, water_origin, bloc_dactv, bloc_dactp, bloc_regul, bloc_hydro
where name=scenario_name;
$$
;

create function api.file_qts(water_delivery_scenario_name varchar)
returns text
language sql stable security definer as
$$
with sectors as (
    select string_agg(format($wdsector$%s %s %s %s$wdsector$,
                             sector.name, coalesce(setting.volume_m3j, 0), coalesce(setting.leak_efficiency, 0), coalesce(setting.adjust_coef, 0)
                            ), E'\n' order by name) as params
    from api.water_delivery_sector as sector
    left join api.water_delivery_scenario_sector_setting as setting on sector.name=setting.sector and setting.scenario=water_delivery_scenario_name
)
select format(
$qts$
%s
%s %s
%s
$qts$,
scenario.name,
(select count(*) from api.water_delivery_sector), case when scenario.period='Weekday' then 1 when scenario.period='Weekend' then 2 else 3 end,
sectors.params)
from api.water_delivery_scenario as scenario, sectors
where scenario.name=water_delivery_scenario_name;
$$
;

create function api.file_cts()
returns text
language sql stable security definer as
$$
with hourly_modulation_curves as (
    select name, string_agg(format($curve$%s %s %s$curve$,
                                   u.i, u.weekdays, u.weekends
                                  ), E'\n') as curve
    from api.hourly_modulation_curve, unnest(
                                            array(select generate_series(1,24)),
                                            (hourly_modulation_array::real[])[:][1:1],
                                            (hourly_modulation_array::real[])[:][2:2]
                                        ) as u(i, weekdays, weekends)
    group by name
)
select string_agg(format(
$cts$
%s
%s
$cts$,
name, curve), '')
from hourly_modulation_curves;
$$
;

create function api.file_rac()
returns text
language plpgsql volatile security definer as
$$
declare
    contend varchar;
    initial_config varchar;
begin
    initial_config := api.current_config();
    perform api.set_current_config(null);

    with header as (
        select E'$'||exportcode||E' (model connection)\n'||(select count(*) from api.model_connection_singularity s where not exists (select 1 from api.node_without_piezo n where n.id=s.id))||E'\n' as text
        from ___.type_singularity as s
        where s.name='model_connection'
    ),
    bloc as (
        select string_agg(format(
        E'\n'||
        E'%s %s %s %s\n'||
        E'%s %s %s %s %s %s %s\n'||
        E'%s\n',
        s._model, s.name, s.id, lower(replace(cascade_mode::text, ' ', '_')),
        chlorine, trihalomethane, passive_tracer, api._bool_to_text(external_file), api._bool_to_text(cyclic), travel_time, contact_time,
        case when external_file
        then
            ''
        else
            api._array_to_text(
                case when cascade_mode = 'Imposed piezometry Z(t)' then zt_array::real[]
                     when cascade_mode = 'Delivery Q(t)'           then qt_array::real[]
                end , 20)
        end
        ), '') as text
        from api.model_connection_singularity as s
        where not exists (select 1 from api.node_without_piezo n where n.id=s.id)
    )
    select header.text||coalesce(E'\n'||bloc.text, '') from header, bloc into contend;

    perform api.set_current_config(initial_config);

    return contend;
end;
$$
;

create function api._bloc_user_node(scenario_name varchar, model_name varchar, excluded_name varchar[] default '{}'::varchar[])
returns text
language sql stable security definer as
$$
with sectors as (
    select s.name, sum(_domestic_volume)::numeric(12,3) as total_volume_dom
    from api.water_delivery_sector as s
    join api.user_node as n on s.name=n._sector
    join api._user_node_affected_volume as v on v._user_node=n.id
    group by s.name
),
header as (
    select api._export_header('user', 'node', model_name, excluded_name) as text
),
wd_scn as (
    select water_delivery_scenario as name
    from api.scenario
    where name=scenario_name
),
user_node_industrial_volume as (
    select wd._node, sum(wdi.volume * wdi.industrial::integer) as vol
    from api.water_delivery_scenario_industrial_volume as wdi
    join api.water_delivery_point as wd on wd.name=wdi.water_delivery_point
    join wd_scn on wdi.water_delivery_scenario=wd_scn.name
    group by wd._node
),
data as (
    select row_number() over(order by n.name collate "C") as nid, n.id, n.name,
    round(ST_X(n.geom)::numeric, 3) as x, round(ST_Y(n.geom)::numeric, 3) as y, n.zground, n.depth,
    n.q0,
    coalesce(n._sector, 'null') as sector,
    coalesce(n.domestic_curve, 'CONSTANT') as domestic_curve,
    round(coalesce(case when s.total_volume_dom != 0 then coalesce(v._domestic_volume, 0)/s.total_volume_dom else 0 end, 0)::numeric, 12) as contrib_dom,
    coalesce(n.industrial_curve, 'CONSTANT') as industrial_curve,
    coalesce(uiv.vol, 0) as industrial_volume
    from wd_scn, api.user_node as n
    left join user_node_industrial_volume uiv on uiv._node=n.id
    left join sectors as s on s.name=n._sector
    left join api._user_node_affected_volume as v on v._user_node=n.id
    where n.model=model_name and n.name != all(excluded_name)
),
bloc as (
    select string_agg(format(
$node$
%s %s %s
%s %s %s %s
%s
%s %s %s %s %s
$node$,
    nid, id, name,
    x, y, zground, depth,
    q0,
    sector, domestic_curve, contrib_dom, industrial_curve, industrial_volume
    ), '' order by nid) as text
    from data
)
select header.text||coalesce(bloc.text, '') from header, bloc;
$$
;

create function api._bloc_reservoir_node(model_name varchar, excluded_name varchar[] default '{}'::varchar[])
returns text
language sql stable security definer as
$$
with header as (
    select api._export_header('reservoir', 'node', model_name, excluded_name) as text
),
data as (
    select row_number() over(order by n.name collate "C") as nid, n.id, n.name,
    round(ST_X(geom)::numeric, 3) as x, round(ST_Y(geom)::numeric, 3) as y, zground, depth,
    api._array_to_text(sz_array::real[], 10) as sz_array,
    z_ini, z_overflow,
    lower(replace(alim_mode::text, ' ', '_')) as alim_mode,
    lower(replace(return_in_valve_mode::text, ' ', '_')) as return_in_valve_mode,
    lower(replace(return_out_valve_mode::text, ' ', '_')) as return_out_valve_mode,
    lower(replace(mixing_mode::text, ' ', '_')) as mixing_mode,
    api._bool_to_text(reset_travel_time) as reset_travel_time
    from api.reservoir_node as n
    where n.model=model_name and n.name != all(excluded_name)
),
bloc as (
    select string_agg(format(
$node$
%s %s %s
%s %s %s %s
%s
%s %s %s %s %s %s %s
$node$,
    nid, id, name,
    x, y, zground, depth,
    sz_array,
    z_ini, z_overflow, alim_mode, return_in_valve_mode, return_out_valve_mode, mixing_mode, reset_travel_time
    ), '' order by nid) as text
    from data
)
select header.text||coalesce(bloc.text, '') from header, bloc;
$$
;

create function api._bloc_pipe_link(model_name varchar, excluded_name varchar[] default '{}'::varchar[])
returns text
language sql stable security definer as
$$
with header as (
    select api._export_header('pipe', 'link', model_name, excluded_name) as text
),
data as (
    select row_number() over(order by l.name collate "C") as nid, l.up as nodam, l.down as nodav, l.name,
    diameter,
    case when overload_roughness_mm then overloaded_roughness_mm else _roughness_mm end as roughness_mm,
    case when overload_celerity then overloaded_celerity else _celerity end as celerity,
    status_open::integer,
    case when overload_length then overloaded_length else ST_Length(l.geom) end as length,
    coalesce(sum(s._k_equivalent), 0) as k_equivalent,
    api._bool_to_text(l.compute_contact_time) compute_contact_time
    from api.pipe_link as l
    left join api.pipe_link_singularity s on s.pipe_link=l.id
    where l._model=model_name and l.name != all(excluded_name)
    group by l.name, l.up, l.down, diameter, geom, overload_roughness_mm, overloaded_roughness_mm, _roughness_mm, overload_celerity, overloaded_celerity, _celerity, status_open, overload_length, overloaded_length, l.compute_contact_time
),
bloc as (
    select string_agg(format(
$link$
%s %s %s %s
%s %s %s %s %s %s %s
$link$,
    nid, nodam, nodav, name,
    diameter, roughness_mm, coalesce(celerity::varchar, 'NULL'), status_open, length, k_equivalent, compute_contact_time
    ), '' order by nid) as text
    from data
)
select header.text||coalesce(bloc.text, '') from header, bloc;
$$
;

create function api._bloc_valve_link(model_name varchar, excluded_name varchar[] default '{}'::varchar[])
returns text
language sql stable security definer as
$$
with header as (
    select api._export_header('valve', 'link', model_name, excluded_name) as text
),
data as (
    select row_number() over(order by l.name collate "C") as nid, l.up as nodam, l.down as nodav, l.name,
    section, headloss_coef, c_opening
    from api.valve_link as l
    where l._model=model_name and l.name != all(excluded_name)
),
bloc as (
    select string_agg(format(
$link$
%s %s %s %s
%s %s %s
$link$,
    nid, nodam, nodav, name,
    section, headloss_coef, c_opening
    ), '' order by nid) as text
    from data
)
select header.text||coalesce(bloc.text, '') from header, bloc;
$$
;

create function api._bloc_flow_regulator_link(model_name varchar, excluded_name varchar[] default '{}'::varchar[])
returns text
language sql stable security definer as
$$
with header as (
    select api._export_header('flow_regulator', 'link', model_name, excluded_name) as text
),
data as (
    select row_number() over(order by l.name collate "C") as nid, l.up as nodam, l.down as nodav, l.name,
    section, headloss_coef, regul_q
    from api.flow_regulator_link as l
    where l._model=model_name and l.name != all(excluded_name)
),
bloc as (
    select string_agg(format(
$link$
%s %s %s %s
%s %s %s
$link$,
    nid, nodam, nodav, name,
    section, headloss_coef, regul_q
    ), '' order by nid) as text
    from data
)
select header.text||coalesce(bloc.text, '') from header, bloc;
$$
;

create function api._bloc_pressure_regulator_link(model_name varchar, excluded_name varchar[] default '{}'::varchar[])
returns text
language sql stable security definer as
$$
with header as (
    select api._export_header('pressure_regulator', 'link', model_name, excluded_name) as text
),
data as (
    select row_number() over(order by l.name collate "C") as nid, l.up as nodam, l.down as nodav, l.name,
    section, headloss_coef,
    lower(replace(pressure_regulation_mode::text, ' ', '_')) as pressure_regulation_mode,
    lower(replace(pressure_regulation_option::text, ' ', '_')) as pressure_regulation_option,
    coalesce(regul_p, 0) as regul_p, coalesce(v.downstream_volume, 0) as downstream_volume, pid_p, pid_i, pid_d,
    api._array_to_text(regul_pq_array::real[], 10) as regul_pq_array
    from api.pressure_regulator_link as l
    left join api.pressure_regulator_downstream_volume as v on v.id=l.id
    where l._model=model_name and l.name != all(excluded_name)
),
bloc as (
    select string_agg(format(
$link$
%s %s %s %s
%s %s %s %s
%s %s %s %s %s
%s
$link$,
    nid, nodam, nodav, name,
    section, headloss_coef, pressure_regulation_mode, pressure_regulation_option,
    regul_p, downstream_volume, pid_p, pid_i, pid_d,
    regul_pq_array
    ), '' order by nid) as text
    from data
)
select header.text||coalesce(bloc.text, '') from header, bloc;
$$
;

create function api._bloc_check_valve_link(model_name varchar, excluded_name varchar[] default '{}'::varchar[])
returns text
language sql stable security definer as
$$
with header as (
    select api._export_header('check_valve', 'link', model_name, excluded_name) as text
),
data as (
    select row_number() over(order by l.name collate "C") as nid, l.up as nodam, l.down as nodav, l.name,
    section, headloss_coef, opening_pressure, lower(replace(closing_mode::text, ' ', '_')) as closing_mode, cd_coef
    from api.check_valve_link as l
    where l._model=model_name and l.name != all(excluded_name)
),
bloc as (
    select string_agg(format(
$link$
%s %s %s %s
%s %s %s %s %s
$link$,
    nid, nodam, nodav, name,
    section, headloss_coef, opening_pressure, closing_mode, cd_coef
    ), '' order by nid) as text
    from data
)
select header.text||coalesce(bloc.text, '') from header, bloc;
$$
;

create function api._bloc_pump_link(model_name varchar, excluded_name varchar[] default '{}'::varchar[])
returns text
language sql stable security definer as
$$
with header as (
    select api._export_header('pump', 'link', model_name, excluded_name) as text
),
data as (
    select row_number() over(order by l.name collate "C") as nid, l.up as nodam, l.down as nodav, l.name,
    api._array_to_text(pq_array::real[], 10) as pq_array,
    speed_reduction_coef, check_valve, rotation_speed, inertia, lower(replace(pump_regulation_option::text, ' ', '_')) as pump_regulation_option,
    case when pump_regulation_option='No regulation' then speed_reduction_coef::varchar
         when pump_regulation_option='Flow regulation' then regul_q::varchar
         when pump_regulation_option='Stop-start regulation' then target_node||' '||regul_z_start::varchar||' '||regul_z_stop::varchar
         when pump_regulation_option='Pressure regulation' then target_node||' '||regul_p::varchar||' '||api._bool_to_text(external_file)||' '||api._bool_to_text(cyclic)
         when pump_regulation_option='Water level in reservoir' then target_node||' '||regul_z::varchar||' '||api._bool_to_text(external_file)||' '||api._bool_to_text(cyclic)||' '||regul_proportional::varchar||' '||regul_integral::varchar||' '||regul_derivative
         end as param
    from api.pump_link as l
    where l._model=model_name and l.name != all(excluded_name)
),
bloc as (
    select string_agg(format(
$link$
%s %s %s %s
%s
%s %s %s %s %s
%s
$link$,
    nid, nodam, nodav, name,
    pq_array,
    speed_reduction_coef, api._bool_to_text(check_valve), rotation_speed, inertia, pump_regulation_option,
    param
    ), '' order by nid) as text
    from data
)
select header.text||coalesce(bloc.text, '') from header, bloc;
$$
;

create function api._bloc_headloss_link(model_name varchar, excluded_name varchar[] default '{}'::varchar[])
returns text
language sql stable security definer as
$$
with header as (
    select api._export_header('headloss', 'link', model_name, excluded_name) as text
),
data as (
    select row_number() over(order by l.name collate "C") as nid, l.up as nodam, l.down as nodav, l.name,
    section, rk1, rk2
    from api.headloss_link as l
    where l._model=model_name and l.name != all(excluded_name)
),
bloc as (
    select string_agg(format(
$link$
%s %s %s %s
%s %s %s
$link$,
    nid, nodam, nodav, name,
    section, rk1, rk2
    ), '' order by nid) as text
    from data
)
select header.text||coalesce(bloc.text, '') from header, bloc;
$$
;

create function api._bloc_imposed_piezometry_singularity(model_name varchar, excluded_name varchar[] default '{}'::varchar[])
returns text
language sql stable security definer as
$$
with header as (
    select api._export_header('imposed_piezometry', 'singularity', model_name, excluded_name) as text
),
data as (
    select row_number() over(order by s.name collate "C") as nid, s.id, s.name,
    z0, chlorine, trihalomethane, passive_tracer, external_file, cyclic, travel_time, contact_time
    from api.imposed_piezometry_singularity as s
    where s._model=model_name and s.name != all(excluded_name)
),
bloc as (
    select string_agg(format(
$singularity$
%s %s %s
%s %s %s %s %s %s %s %s
$singularity$,
    nid, id, name,
    z0, chlorine, trihalomethane, passive_tracer, api._bool_to_text(external_file), api._bool_to_text(cyclic), travel_time, contact_time
    ), '' order by nid) as text
    from data
)
select header.text||coalesce(bloc.text, '') from header, bloc;
$$
;

create function api._bloc_flow_injection_singularity(model_name varchar, excluded_name varchar[] default '{}'::varchar[])
returns text
language sql stable security definer as
$$
with header as (
    select api._export_header('flow_injection', 'singularity', model_name, excluded_name) as text
),
data as (
    select row_number() over(order by s.name collate "C") as nid, s.id, s.name,
    q0, chlorine, trihalomethane, passive_tracer, external_file, cyclic, travel_time, contact_time
    from api.flow_injection_singularity as s
    where s._model=model_name and s.name != all(excluded_name)
),
bloc as (
    select string_agg(format(
$singularity$
%s %s %s
%s %s %s %s %s %s %s %s
$singularity$,
    nid, id, name,
    q0, chlorine, trihalomethane, passive_tracer, api._bool_to_text(external_file), api._bool_to_text(cyclic), travel_time, contact_time
    ), '' order by nid) as text
    from data
)
select header.text||coalesce(bloc.text, '') from header, bloc;
$$
;

create function api._bloc_chlorine_injection_singularity(model_name varchar, excluded_name varchar[] default '{}'::varchar[])
returns text
language sql stable security definer as
$$
with header as (
    select api._export_header('chlorine_injection', 'singularity', model_name, excluded_name) as text
),
data as (
    select row_number() over(order by s.name collate "C") as nid, s.id, s.name,
    target_node, target_concentration
    from api.chlorine_injection_singularity as s
    where s._model=model_name and s.name != all(excluded_name)
),
bloc as (
    select string_agg(format(
$singularity$
%s %s %s
%s %s
$singularity$,
    nid, id, name,
    target_node, target_concentration
    ), '' order by nid) as text
    from data
)
select header.text||coalesce(bloc.text, '') from header, bloc;
$$
;

create function api._bloc_surge_tank_singularity(model_name varchar, excluded_name varchar[] default '{}'::varchar[])
returns text
language sql stable security definer as
$$
with header as (
    select api._export_header('surge_tank', 'singularity', model_name, excluded_name) as text
),
data as (
    select row_number() over(order by s.name collate "C") as nid, s.id, s.name,
    api._array_to_text(sz_array::real[], 10) as sz_array,
    zini, api._bool_to_text(pressurized_option) as pressurized_option, orifice_diameter, in_headloss_coef, out_headloss_coef
    from api.surge_tank_singularity as s
    where s._model=model_name and s.name != all(excluded_name)
),
bloc as (
    select string_agg(format(
$singularity$
%s %s %s
%s
%s %s %s %s %s
$singularity$,
    nid, id, name,
    sz_array,
    zini, pressurized_option, orifice_diameter, in_headloss_coef, out_headloss_coef
    ), '' order by nid) as text
    from data
)
select header.text||coalesce(bloc.text, '') from header, bloc;
$$
;

create function api._bloc_pressure_accumulator_singularity(model_name varchar, excluded_name varchar[] default '{}'::varchar[])
returns text
language sql stable security definer as
$$
with header as (
    select api._export_header('pressure_accumulator', 'singularity', model_name, excluded_name) as text
),
data as (
    select row_number() over(order by s.name collate "C") as nid, s.id, s.name,
    v0, z_connection, filling_pressure
    from api.pressure_accumulator_singularity as s
    where s._model=model_name and s.name != all(excluded_name)
),
bloc as (
    select string_agg(format(
$singularity$
%s %s %s
%s %s %s
$singularity$,
    nid, id, name,
    v0, z_connection, filling_pressure
    ), '' order by nid) as text
    from data
)
select header.text||coalesce(bloc.text, '') from header, bloc;
$$
;

create function api._bloc_air_relief_valve_singularity(model_name varchar, excluded_name varchar[] default '{}'::varchar[])
returns text
language sql stable security definer as
$$
with header as (
    select api._export_header('air_relief_valve', 'singularity', model_name, excluded_name) as text
),
data as (
    select row_number() over(order by s.name collate "C") as nid, s.id, s.name,
    orifice_diameter, contraction_coef
    from api.air_relief_valve_singularity as s
    where s._model=model_name and s.name != all(excluded_name)
),
bloc as (
    select string_agg(format(
$singularity$
%s %s %s
%s %s
$singularity$,
    nid, id, name,
    orifice_diameter, contraction_coef
    ), '' order by nid) as text
    from data
)
select header.text||coalesce(bloc.text, '') from header, bloc;
$$
;

create or replace function api.create_scenario_config(config_name varchar, scenario_name varchar)
returns text
language plpgsql volatile security definer as
$$
declare
   r record;
   c varchar;
   cols_n varchar;
   cols_v varchar;
   query varchar;
begin
    insert into api.configuration(name) values (config_name);
    for c in
        select name||'_node' from ___.type_node
        union all
        select name||'_link' from ___.type_link
        union all
        select name||'_singularity' from ___.type_singularity
    loop


        query := format($sql$
            select
                string_agg(column_name, ', ')||', config',
                string_agg('c.'||column_name, ', ')||', ''%2$s'''
            from information_schema.columns
            where table_schema = '___'
            and table_name = '%1$s_config'
            and column_name != 'config'
            $sql$, c, config_name);

        --raise notice '%', query;
        execute query into cols_n, cols_v;

        query := format($sql$
            with configured_with_max_rank as (
                select distinct first_value(c.id) over w as id, first_value(c.config) over w as config
                from  ___.%1$s_config c
                join ___.scenario_configuration sc on sc.config=c.config
                where sc.scenario='%2$s'
                window w as (partition by c.id order by sc.rank_)
            )
            insert into ___.%1$s_config(%3$s)
            select %4$s
            from ___.%1$s_config c
            join configured_with_max_rank as cmr on cmr.config=c.config and cmr.id=c.id
            $sql$, c, scenario_name, cols_n, cols_v);
        --raise notice '%', query;
        execute query;
    end loop;

    return 'ok';

end
$$
;

create or replace function api._bloc_user_node_skl(scenario_name varchar, model_name varchar, excluded_name varchar[] default '{}'::varchar[])
returns text
language sql stable security definer as
$$
with sectors as (
    select delivery_sector, sum(domestic_volume)::numeric(12,3) as total_volume_dom
    from skl.skl_delivery_node
    group by delivery_sector
),
header as (
    select format(
$res$
$%s (%s_%s)
%s %s
$res$,
    (select exportcode from ___.type_node where name='user'),
    'user', 'node',
    (select exportid  from ___.type_node where name='user'),
    (select count(1) from skl.skl_node where model=model_name and name != all(excluded_name))
    ) as text
),
wd_scn as (
    select water_delivery_scenario as name
    from api.scenario
    where name=scenario_name
),
user_node_industrial_volume as (
    select wd._node, sum(wdi.volume * wdi.industrial::integer) as vol
    from api.water_delivery_scenario_industrial_volume as wdi
    join api.water_delivery_point as wd on wd.name=wdi.water_delivery_point
    join wd_scn on wdi.water_delivery_scenario=wd_scn.name
    group by wd._node
),
data as (
    select row_number() over(order by n.name collate "C") as nid, n.node as id, n.name,
    round(ST_X(n.geom)::numeric, 3) as x, round(ST_Y(n.geom)::numeric, 3) as y, n.zground, n.depth,
    coalesce(dn.q0, 0) as q0,
    coalesce(dn.delivery_sector, 'null') as sector,
    coalesce(dn.domestic_curve, 'CONSTANT') as domestic_curve,
    round(coalesce(case when s.total_volume_dom != 0 then coalesce(dn.domestic_volume, 0)/s.total_volume_dom else 0 end, 0)::numeric, 12) as contrib_dom,
    coalesce(dn.industrial_curve, 'CONSTANT') as industrial_curve,
    coalesce(uiv.vol, 0) as industrial_volume
    from skl.skl_node n
    left join skl.skl_delivery_node dn on dn.node=n.node
    left join sectors as s on s.delivery_sector=dn.delivery_sector
    left join user_node_industrial_volume uiv on uiv._node=dn.node
    where n.model=model_name and n.name != all(excluded_name)
),
bloc as (
    select string_agg(format(
$node$
%s %s %s
%s %s %s %s
%s
%s %s %s %s %s
$node$,
    nid, id, name,
    x, y, zground, depth,
    q0,
    sector, domestic_curve, contrib_dom, industrial_curve, industrial_volume
    ), '' order by nid) as text
    from data
)
select header.text||coalesce(bloc.text, '') from header, bloc;
$$
;

create or replace function api._bloc_pipe_link_skl(model_name varchar, excluded_name varchar[] default '{}'::varchar[])
returns text
language sql stable security definer as
$$
with header as (
    select format(
$res$
$%s (%s_%s)
%s %s
$res$,
    (select exportcode  from ___.type_link where name='pipe'),
    'pipe', 'link',
    (select exportid  from ___.type_link where name='pipe'),
    (select count(1) from skl.skl_pipe where model=model_name and name != all(excluded_name))
    ) as text
),
data as (
    select row_number() over() as nid, l.up as nodam, l.down as nodav, l.name,
    l.diameter::numeric(8,3),
    l.roughness_mm::numeric(12,5),
    l.celerity::numeric(8,3),
    1 as status_open,
    ST_Length(l.geom)::numeric(15,1) as length,
    0 as k_equivalent,
    '.false.' as compute_contact_time
    from skl.skl_pipe as l
    where l.model=model_name and l.name != all(excluded_name)
),
bloc as (
    select string_agg(format(
$link$
%s %s %s %s
%s %s %s %s %s %s %s
$link$,
    nid, nodam, nodav, name,
    diameter, roughness_mm, coalesce(celerity::varchar, 'NULL'), status_open, length, k_equivalent, compute_contact_time
    ), '' order by nid) as text
    from data
)
select header.text||coalesce(bloc.text, '') from header, bloc;
$$
;


create function api.file_dat(scenario_name varchar, model_name varchar)
returns text
language plpgsql volatile security definer as
$$
declare
    initial_config varchar;
    config_ varchar;
    res varchar;
    excluded_node varchar[];
    excluded_link varchar[];
    excluded_singularity varchar[];
    skl boolean;
begin
    initial_config := api.current_config();
    config_ := '_'||scenario_name;
    perform api.create_scenario_config(config_, scenario_name);
    perform api.set_current_config(config_);
    select skeletonization from api.scenario where name ilike scenario_name into skl;

    select coalesce(array_agg(name), '{}'::varchar[]) from api.node_without_piezo where model=model_name into excluded_node;
    if skl then
        select coalesce(array_agg(name), '{}'::varchar[])
        from (
            select p.name
            from skl.skl_pipe p
            join api.node_without_piezo n on st_intersects(n.geom, p.geom)
            union
            select l.name
            from api.link_without_piezo l
            ) e
        into excluded_link;
    else
        select coalesce(array_agg(name), '{}'::varchar[]) from api.link_without_piezo where _model=model_name into excluded_link;
    end if;
    select coalesce(array_agg(s.name), '{}'::varchar[]) from api.singularity s join api.node_without_piezo n on n.id=s.id where model=model_name into excluded_singularity;

    select concat(
    case when skl then api._bloc_user_node_skl(scenario_name, model_name, excluded_node) else api._bloc_user_node(scenario_name, model_name, excluded_node) end,
    api._bloc_reservoir_node(model_name, excluded_node),
    case when skl then api._bloc_pipe_link_skl(model_name, excluded_link) else api._bloc_pipe_link(model_name, excluded_link) end,
    api._bloc_valve_link(model_name, excluded_link),
    api._bloc_flow_regulator_link(model_name, excluded_link),
    api._bloc_pressure_regulator_link(model_name, excluded_link),
    api._bloc_check_valve_link(model_name, excluded_link),
    api._bloc_pump_link(model_name, excluded_link),
    api._bloc_headloss_link(model_name, excluded_link),
    api._bloc_imposed_piezometry_singularity(model_name, excluded_singularity),
    api._bloc_flow_injection_singularity(model_name, excluded_singularity),
    api._bloc_chlorine_injection_singularity(model_name, excluded_singularity),
    api._bloc_surge_tank_singularity(model_name, excluded_singularity),
    api._bloc_pressure_accumulator_singularity(model_name, excluded_singularity),
    api._bloc_air_relief_valve_singularity(model_name, excluded_singularity)) into res;
    perform api.set_current_config(initial_config);
    delete from api.configuration where name=config_;
    return res;
end
$$
;

create function api.file_config_dat(scenario_name varchar, model_name varchar)
returns text
language plpgsql volatile security definer as
$$
declare
    initial_config varchar;
    config_ varchar;
    res varchar;
    configured varchar[];
    exportid_ integer;
    already_configured varchar[];
    excluded varchar[];
    excl_no_piezo varchar[];
    a varchar;
    c varchar;
    bloc varchar;
    skip_next_line_if_node varchar;
    excluded_node varchar[];
    excluded_link varchar[];
    excluded_singularity varchar[];
    skl boolean;
begin
    initial_config := api.current_config();
    config_ := '_'||scenario_name;
    perform api.create_scenario_config(config_, scenario_name);
    perform api.set_current_config(config_);

    select skeletonization from api.scenario where name ilike scenario_name into skl;

    select coalesce(array_agg(name), '{}'::varchar[]) from api.node_without_piezo where model=model_name into excluded_node;
    if skl then
        select coalesce(array_agg(name), '{}'::varchar[])
        from (
            select p.name
            from skl.skl_pipe p
            join api.node_without_piezo n on st_intersects(n.geom, p.geom)
            union
            select l.name
            from api.link_without_piezo l
            ) e
        into excluded_link;
    else
        select coalesce(array_agg(name), '{}'::varchar[]) from api.link_without_piezo where _model=model_name into excluded_link;
    end if;
    select coalesce(array_agg(s.name), '{}'::varchar[]) from api.singularity s join api.node_without_piezo n on n.id=s.id where model=model_name into excluded_singularity;

    res := E'\n\n';
    for a in values ('node'), ('singularity'), ('link')
    loop
        for c in execute 'select name from ___.type_'||a
        loop
            continue when c||'_'||a = 'model_connection_singularity'; -- this one is not exported
            -- raise notice '%',c||'_'||a;

            raise notice '%', format('select exportid from ___.type_%s where name=''%s''', a, c);
            execute format('select exportid from ___.type_%s where name=''%s''', a, c) into exportid_;
            already_configured := '{}'::varchar[];

            if a = 'node' then
                excl_no_piezo := excluded_node;
            elsif a = 'link' then
                excl_no_piezo := excluded_link;
            elsif a = 'singularity' then
                excl_no_piezo := excluded_singularity;
            end if;

            execute format('select array_agg(name) from ___.%s_%s_config where config=''%s''', c, a, config_) into configured;
            configured := coalesce(configured, '{}'::varchar[]);
            execute format('select array_agg(name) from ___.%s_%s where name != all(''%s''::varchar[]) or name = any(''%s''::varchar[])', c, a, configured, already_configured) into excluded;
            excluded := coalesce(excluded, '{}'::varchar[]);
            --raise notice 'configured % excluded %', configured, excluded;
            if c='user' then
                bloc := case when skl then
                    api._bloc_user_node_skl(scenario_name, model_name, excluded||excl_no_piezo)
                    else
                    api._bloc_user_node(scenario_name, model_name, excluded||excl_no_piezo)
                    end;
                skip_next_line_if_node := '[^\n]+\n';
            elsif c='pipe' then
                bloc := case when skl then
                    api._bloc_pipe_link_skl(model_name, excluded||excl_no_piezo)
                    else
                    api._bloc_pipe_link(model_name, excluded||excl_no_piezo)
                    end;
                skip_next_line_if_node := '';
            elsif c='reservoir' then
                bloc := api._bloc_reservoir_node(model_name, excluded||excl_no_piezo);
                skip_next_line_if_node := '[^\n]+\n';
            else
                execute format('select api._bloc_%s_%s(''%s'', ''%s''::varchar[])', c, a, model_name, excluded||excl_no_piezo) into bloc;
                skip_next_line_if_node := '';
            end if;
            res := res || regexp_replace(
                regexp_replace(bloc,
                '\$[^\n]*\n[^\n]*\n', ''), -- get rid of headers
                '\n\n([^ \n]+ )*([^ \n]+)\n'||skip_next_line_if_node, E'\n\n* '||exportid_||E' \\2\n', 'g'); -- adds config * header for each element
            already_configured := already_configured || configured;
        end loop;
    end loop;

    res := res || (
        with bloc as (
            select string_agg(format(
            E'\n'||
            E'* %s %s\n'||
            E'%s %s %s %s %s %s %s\n'||
            E'%s %s\n'||
            E'%s\n',
            99, a.name,
            c.chlorine, c.trihalomethane, c.passive_tracer, api._bool_to_text(c.external_file), api._bool_to_text(c.cyclic), travel_time, contact_time,
            case
            when c.cascade_mode = 'Delivery Q(t)' then 1
            when c.cascade_mode = 'Imposed piezometry Z(t)' then 2
            else 0
            end,
            case
            when c.cascade_mode = 'Delivery Q(t)' and not c.external_file then array_length(c.qt_array,1)
            when c.cascade_mode = 'Imposed piezometry Z(t)' and not c.external_file then array_length(c.zt_array,1)
            else 0
            end,
            case when c.external_file
            then
                ''
            else
                api._array_to_text(
                    case when c.cascade_mode = 'Imposed piezometry Z(t)' then c.zt_array::real[]
                         when c.cascade_mode = 'Delivery Q(t)'           then c.qt_array::real[]
                    end , 20)
            end
            ), '') as text
            from ___.model_connection_singularity_config as c
            join ___.singularity as a on a.id=c.id
            join ___.node n on n.id=c.id
            where config=config_ and n.model=model_name
        ) select coalesce(E'\n'||bloc.text, '') from bloc);

    perform api.set_current_config(initial_config);
    delete from api.configuration where name=config_;
    return res;
end
$$
;


create function api.file_phys_dat()
returns text
language sql stable security definer as
$$
select format(
$phys$
*EAU (caracteristiques du fluide)
%s %s %s %s

*HCRIT (valeur des alarmes)
%s
%s %s
%s %s
$phys$,
volumic_mass_kg_m3, temperature_c, kinematic_viscosity_m2_s, volumic_elasticity_module_N_m2,
minimum_service_pressure,
pipe_maximum_speed, pipe_minimum_speed,
node_maximum_pressure, node_minimum_pressure
)
from api.fluid_properties, api.threshold_warning_settings;
$$
;

create function api.file_act_dat()
returns text
language sql stable security definer as
$$
select string_agg(format(
$alp$
%s
%s
$alp$,
name, api._array_to_text(alpt_array::real[])), '' order by name)
from api.failure_curve;
$$
;

create function api.file_optsor_dat()
returns text
language sql stable security definer as
$$
--with bloc as (
--    select string_agg(format(
--$ind$%s %s$ind$,
--    n.model, n.id--, n.industrial_volume + n.domestic_volume
--    ), E'\n') as text
--    from api.user_node as n
--    --where n.industrial_volume > 0
--)
--select E'*CONSO_IND\n'||coalesce(bloc.text, '') from bloc; VMO
select '';
$$
;

select template.create_configured_view();

create view api.warning as
select row_number() over() as id, a.geom, a.name||' is too close to node '||b.name as reason, st_distance(a.geom, b.geom)::numeric(12,3) as distance
from api.node a
join api.node b on st_dwithin(a.geom, b.geom, .01)
where a.id > b.id
;

create view api.opened_link as
with closed as (
    select id, up, down from api.pipe_link where not status_open
    union all
    select id, up, down from api.valve_link where c_opening = 0
    union all
    select id, up, down from api.flow_regulator_link where regul_q = 0
    union all
    select id, up, down from api.pressure_regulator_link where pressure_regulation_mode = 'Imposed downstream pressure' and regul_p = 0
)
select id, up, down from api.link
except
select id, up, down from closed
;


create view api.node_without_piezo as
with sector_node as (
    select n.id, t.component AS zone, t.node, n.geom, n._type, n.name, n.model
    from pgr_connectedcomponents($query$
        with filtered as (
            select id, up, down
            from api.opened_link
        )
        select id, up as source, down as target, 1 as cost, 1 as reverse_cost from filtered
        $query$
::text) t(seq, component, node)
    join api.node n on n.id = t.node
),
sector_map as (
    select row_number() over () as to_, t.zone as from_
    from ( select distinct sector_node.zone from sector_node) t
),
piezzo as (
    select id, name, 'connector z(t)' as type from api.model_connection_singularity where cascade_mode='Imposed piezometry Z(t)'
    union all
    select id, name, 'piezzo' as type from api.imposed_piezometry_singularity
    union all
    select id, name, 'reservoir' from api.reservoir_node
),
sectorized as (
    select n.id, m.to_ AS zone, n.geom, n._type, n.name, n.model
    from sector_node n
    join sector_map m on m.from_ = n.zone
),
without_piezo as (
    select distinct to_ as zone
    from sector_map
    except
    select distinct s.zone
    from sectorized s
    join piezzo p on p.id=s.id
)
select * from sectorized where zone in (select zone from without_piezo)
;

create view api.link_without_piezo as
select distinct l.*
from api.node_without_piezo n
join api.link l on n.id in (l.up, l.down)
;

------------------------------------------------------------------------------------------------
-- Measures                                                                                   --
------------------------------------------------------------------------------------------------

select template.basic_view('sensor', with_trigger=>true);

create view api.measure as
select m.id, m.t, m.value, s.name as sensor, m.creation_date
from ___.measure m
left join ___.sensor s on s.id=m.sensor
;

alter view api.measure alter column creation_date set default now() ;
alter view api.measure alter column id set default nextval('___.measure_id_seq'::regclass) ;

create function api.measure_instead_fct()
returns trigger
language plpgsql volatile security definer as
$fct$
begin
    if tg_op = 'INSERT' then
        insert into ___.measure(id, t, value, sensor, creation_date)
        select new.id, new.t, new.value, s.id, new.creation_date
        from ___.sensor s where s.name=new.sensor returning id into new.id;
        return new;
    elsif tg_op = 'UPDATE' then
        update ___.measure m set id=new.id, t=new.t, value=new.value, sensor=s.id, creation_date=new.creation_date
        from ___.sensor s
        where s.name=new.name and m.id=old.id ;
        return new;
    elsif tg_op = 'DELETE' then
        delete from ___.measure where id=old.id;
        return old;
    end if;
end;
$fct$
;

create trigger measure_instead_trig
instead of insert or update or delete on api.measure
for each row execute procedure api.measure_instead_fct()
;

create function api.measure_conversion(sensor varchar, target_type ___.sensor_type)
returns table(scale float, "offset" float)
language plpgsql stable as
$$
declare
    source_type ___.sensor_type;
    sensor_geom geometry;
    z0 float;
begin
    if sensor is null or target_type is null then
        return query select 1::float as scale, 0::float as offset;
        return;
    else
        select s.type_, s.geom from ___.sensor s where name=sensor into source_type, sensor_geom;

        if source_type::varchar like 'q(%)' and target_type::varchar like 'q(%)' then
            if     source_type = 'q(m3/h)'   and target_type = 'q(m3/h)' then
                return query select       1::float as scale, 0::float as offset;
                return;
            elsif  source_type = 'q(m3/s)'   and target_type = 'q(m3/h)' then
                return query select    3600::float as scale, 0::float as offset;
                return;
            elsif  source_type = 'q(m3/day)' and target_type = 'q(m3/h)' then
                return query select (1./24)::float as scale, 0::float as offset;
                return;
            end if;

        elsif target_type::varchar like 'p(%)' or target_type::varchar like 'z(%)' then
            select coalesce(r.sz_array[1][1], st_z(n.geom))
            from ___.node n
            left join ___.reservoir_node r on r.id = n.id
            where st_dwithin(geom, sensor_geom, .01) into z0;

            if     source_type = 'p(m)'   and target_type = 'z(m)' then
                return query select        1::float as scale,          z0::float as offset;
                return;
            elsif  source_type = 'p(bar)' and target_type = 'z(m)' then
                return query select     10.2::float as scale,          z0::float as offset;
                return;
            elsif  source_type = 'p(bar)' and target_type = 'p(m)' then
                return query select     10.2::float as scale,           0::float as offset;
                return;
            elsif  source_type = 'p(m)'   and target_type = 'p(m)' then
                return query select        1::float as scale,           0::float as offset;
                return;
            elsif  source_type = 'z(m)'   and target_type = 'z(m)' then
                return query select        1::float as scale,           0::float as offset;
                return;
            elsif  source_type = 'z(m)'   and target_type = 'p(m)' then
                return query select        1::float as scale,         -z0::float as offset;
                return;
            elsif  source_type = 'z(m)'   and target_type = 'p(bar)' then
                return query select (1./10.2)::float as scale, -(z0/10.2)::float as offset;
                return;
            end if;
        end if;
    end if;
    raise 'cannot convert % from % to %', sensor, source_type, target_type;
end;
$$
;

select 'ok'::varchar;

$api$
;

select create_api();

drop schema template cascade;