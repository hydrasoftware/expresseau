--
-- PostgreSQL database dump
--

-- Dumped from database version 12.11 (Ubuntu 12.11-0ubuntu0.20.04.1)
-- Dumped by pg_dump version 12.11 (Ubuntu 12.11-0ubuntu0.20.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: ___; Type: SCHEMA; Schema: -; Owner: hydra
--

CREATE SCHEMA ___;


ALTER SCHEMA ___ OWNER TO hydra;

--
-- Name: SCHEMA ___; Type: COMMENT; Schema: -; Owner: hydra
--

COMMENT ON SCHEMA ___ IS 'tables schema for expresseau, admin use only';


--
-- Name: api; Type: SCHEMA; Schema: -; Owner: hydra
--

CREATE SCHEMA api;


ALTER SCHEMA api OWNER TO hydra;

--
-- Name: SCHEMA api; Type: COMMENT; Schema: -; Owner: hydra
--

COMMENT ON SCHEMA api IS 'views and functions schema for expresseau, for user access';


--
-- Name: postgis; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS postgis WITH SCHEMA public;


--
-- Name: EXTENSION postgis; Type: COMMENT; Schema: -; Owner:
--

COMMENT ON EXTENSION postgis IS 'PostGIS geometry, geography, and raster spatial types and functions';


--
-- Name: alim_mode; Type: TYPE; Schema: ___; Owner: hydra
--

CREATE TYPE ___.alim_mode AS ENUM (
    'From bottom',
    'Overflow'
);


ALTER TYPE ___.alim_mode OWNER TO hydra;

--
-- Name: computation_mode; Type: TYPE; Schema: ___; Owner: hydra
--

CREATE TYPE ___.computation_mode AS ENUM (
    'Steady state',
    'Gradual transient',
    'Fast transient',
    'Quality only'
);


ALTER TYPE ___.computation_mode OWNER TO hydra;

--
-- Name: mixing_mode; Type: TYPE; Schema: ___; Owner: hydra
--

CREATE TYPE ___.mixing_mode AS ENUM (
    'Perfect mixing',
    'Piston flow LIFO',
    'Piston flow FIFO',
    'Two chambers mixing'
);


ALTER TYPE ___.mixing_mode OWNER TO hydra;

--
-- Name: model_connect_mode; Type: TYPE; Schema: ___; Owner: hydra
--

CREATE TYPE ___.model_connect_mode AS ENUM (
    'Imposed piezometry Z(t)',
    'Delivery Q(t)'
);


ALTER TYPE ___.model_connect_mode OWNER TO hydra;

--
-- Name: model_connection_settings; Type: TYPE; Schema: ___; Owner: hydra
--

CREATE TYPE ___.model_connection_settings AS ENUM (
    'Cascade',
    'Global',
    'Mixed'
);


ALTER TYPE ___.model_connection_settings OWNER TO hydra;

--
-- Name: pipe_type_singularity; Type: TYPE; Schema: ___; Owner: hydra
--

CREATE TYPE ___.pipe_type_singularity AS ENUM (
    'Bend',
    'Borda headloss'
);


ALTER TYPE ___.pipe_type_singularity OWNER TO hydra;

--
-- Name: pressure_regulation_mode; Type: TYPE; Schema: ___; Owner: hydra
--

CREATE TYPE ___.pressure_regulation_mode AS ENUM (
    'Imposed differential pressure',
    'Imposed upstream pressure',
    'Imposed downstream pressure'
);


ALTER TYPE ___.pressure_regulation_mode OWNER TO hydra;

--
-- Name: pressure_regulation_option; Type: TYPE; Schema: ___; Owner: hydra
--

CREATE TYPE ___.pressure_regulation_option AS ENUM (
    'Constant pressure',
    'Curve P(Q)'
);


ALTER TYPE ___.pressure_regulation_option OWNER TO hydra;

--
-- Name: pump_failure_mode; Type: TYPE; Schema: ___; Owner: hydra
--

CREATE TYPE ___.pump_failure_mode AS ENUM (
    'Automatic alpha computation',
    'Alpha(t) curve',
    'Four quadrants curve'
);


ALTER TYPE ___.pump_failure_mode OWNER TO hydra;

--
-- Name: pump_regulation_option; Type: TYPE; Schema: ___; Owner: hydra
--

CREATE TYPE ___.pump_regulation_option AS ENUM (
    'No regulation',
    'Flow regulation',
    'Stop-start regulation',
    'Pressure regulation'
);


ALTER TYPE ___.pump_regulation_option OWNER TO hydra;

--
-- Name: quality_output; Type: TYPE; Schema: ___; Owner: hydra
--

CREATE TYPE ___.quality_output AS ENUM (
    'None',
    'Tracer',
    'Water origin'
);


ALTER TYPE ___.quality_output OWNER TO hydra;

--
-- Name: return_valve_mode; Type: TYPE; Schema: ___; Owner: hydra
--

CREATE TYPE ___.return_valve_mode AS ENUM (
    'Regulated',
    'Closed',
    'Open'
);


ALTER TYPE ___.return_valve_mode OWNER TO hydra;

--
-- Name: type_link_; Type: TYPE; Schema: ___; Owner: hydra
--

CREATE TYPE ___.type_link_ AS ENUM (
    'pipe',
    'valve',
    'flow_regulator',
    'pressure_regulator',
    'check_valve',
    'pump',
    'headloss'
);


ALTER TYPE ___.type_link_ OWNER TO hydra;

--
-- Name: type_node_; Type: TYPE; Schema: ___; Owner: hydra
--

CREATE TYPE ___.type_node_ AS ENUM (
    'user',
    'reservoir'
);


ALTER TYPE ___.type_node_ OWNER TO hydra;

--
-- Name: type_singularity_; Type: TYPE; Schema: ___; Owner: hydra
--

CREATE TYPE ___.type_singularity_ AS ENUM (
    'surge_tank',
    'pressure_accumulator',
    'air_relief_valve',
    'imposed_piezometry',
    'flow_injection',
    'chlorine_injection',
    'model_connection'
);


ALTER TYPE ___.type_singularity_ OWNER TO hydra;

--
-- Name: valve_closing_mode; Type: TYPE; Schema: ___; Owner: hydra
--

CREATE TYPE ___.valve_closing_mode AS ENUM (
    'Instantaneous',
    'Progressive'
);


ALTER TYPE ___.valve_closing_mode OWNER TO hydra;

--
-- Name: week_period; Type: TYPE; Schema: ___; Owner: hydra
--

CREATE TYPE ___.week_period AS ENUM (
    'Weekday',
    'Weekend'
);


ALTER TYPE ___.week_period OWNER TO hydra;

--
-- Name: _array_sum(real[], integer); Type: FUNCTION; Schema: ___; Owner: hydra
--

CREATE FUNCTION ___._array_sum(arr real[], col integer DEFAULT 1) RETURNS real
    LANGUAGE sql IMMUTABLE
    AS $$
select sum(values)::real from (select unnest(arr[:][col:col]) as values) as arr
$$;


ALTER FUNCTION ___._array_sum(arr real[], col integer) OWNER TO hydra;

--
-- Name: metadata_after_srid_update_fct(); Type: FUNCTION; Schema: ___; Owner: hydra
--

CREATE FUNCTION ___.metadata_after_srid_update_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
declare
    table_name varchar;
    geometry_type varchar;
    dimension integer;
    api_exists boolean;
begin
    if exists (select 1 from information_schema.schemata where schema_name = 'api') then
        drop schema api cascade;
        api_exists = true;
    end if;

    for table_name, geometry_type, dimension in select f_table_name, type, coord_dimension from geometry_columns where f_table_schema = '___' loop
        execute 'alter table ___.'||table_name||' '||
        E'alter column geom '||
        E'type geometry('''||geometry_type||case when dimension=3 then 'Z' else '' end||''', '||new.srid||') '||
        E'using st_transform(geom, '||new.srid||');';
    end loop;

    if api_exists then
        perform create_template();
        perform create_api();
        drop schema template cascade;
    end if;
    return new;
end;
$$;


ALTER FUNCTION ___.metadata_after_srid_update_fct() OWNER TO hydra;

--
-- Name: unique_name(character varying, character varying, character varying); Type: FUNCTION; Schema: ___; Owner: hydra
--

CREATE FUNCTION ___.unique_name(concrete character varying, abstract character varying DEFAULT NULL::character varying, abbreviation character varying DEFAULT NULL::character varying) RETURNS character varying
    LANGUAGE plpgsql
    AS $_$
declare
    mx integer;
    nxt integer;
    seq varchar;
    tab varchar;
begin
    if abbreviation is null then
        execute format('(select abbreviation from ___.type_%2$s where name=''%1$s'')', concrete, abstract) into abbreviation;
    end if;
    tab := concrete||coalesce('_'||abstract, '');
    seq := tab||'_name_seq';
    nxt := nextval('___.'||seq);
    execute format('with substrings as (
                        select substring(name from ''%2$s_(.*)'') as s
                        from ___.%1$I
                        where name like ''%2$s_%%''
                    )
                    select coalesce(max(s::integer), 1) as mx
                    from substrings
                    where s ~ ''^\d+$'' ',
                    tab, abbreviation) into mx;

    if nxt < mx then
        nxt := mx + 1;
        perform setval('___.'||seq, mx+1);
    end if;
    return abbreviation||'_'||nxt::varchar;
end;
$_$;


ALTER FUNCTION ___.unique_name(concrete character varying, abstract character varying, abbreviation character varying) OWNER TO hydra;

--
-- Name: _array_to_text(anyarray, integer); Type: FUNCTION; Schema: api; Owner: hydra
--

CREATE FUNCTION api._array_to_text(anyarray, integer DEFAULT 0) RETURNS text
    LANGUAGE sql IMMUTABLE
    AS $_$
with arrays as (
	select d1, array_agg($1[d1][d2])as a
	from generate_subscripts($1,1) d1, generate_subscripts($1,2) d2
	group by d1
		union all
	select d1, array_fill(0, array[array_length($1, 2)]) as a
	from generate_series(array_length($1, 1) + 1, $2) as d1
	order by d1
)
select string_agg(array_to_string(a, ' '), E'\n') from arrays
$_$;


ALTER FUNCTION api._array_to_text(anyarray, integer) OWNER TO hydra;

--
-- Name: FUNCTION _array_to_text(anyarray, integer); Type: COMMENT; Schema: api; Owner: hydra
--

COMMENT ON FUNCTION api._array_to_text(anyarray, integer) IS 'converts an array to text for export:
x1 y1
x2 y2
x3 y3
...';


--
-- Name: _bloc_air_relief_valve_singularity(character varying); Type: FUNCTION; Schema: api; Owner: hydra
--

CREATE FUNCTION api._bloc_air_relief_valve_singularity(model_name character varying) RETURNS text
    LANGUAGE sql STABLE
    AS $_$
with header as (
    select api._export_header('air_relief_valve', 'singularity', model_name) as text
),
data as (
    select row_number() over() as nid, s.id, s.name,
    orifice_diameter, contraction_coef
    from api.air_relief_valve_singularity as s
    where s._model=model_name
),
bloc as (
    select string_agg(format(
$singularity$
%s %s %s
%s %s
$singularity$,
    nid, id, name,
    orifice_diameter, contraction_coef
    ), '') as text
    from data
)
select header.text||coalesce(bloc.text, '') from header, bloc;
$_$;


ALTER FUNCTION api._bloc_air_relief_valve_singularity(model_name character varying) OWNER TO hydra;

--
-- Name: _bloc_check_valve_link(character varying); Type: FUNCTION; Schema: api; Owner: hydra
--

CREATE FUNCTION api._bloc_check_valve_link(model_name character varying) RETURNS text
    LANGUAGE sql STABLE
    AS $_$
with header as (
    select api._export_header('check_valve', 'link', model_name) as text
),
data as (
    select row_number() over() as nid, l.up as nodam, l.down as nodav, l.name,
    section, headloss_coef, opening_pressure, lower(replace(closing_mode::text, ' ', '_')) as closing_mode, cd_coef
    from api.check_valve_link as l
    where l._model=model_name
),
bloc as (
    select string_agg(format(
$link$
%s %s %s %s
%s %s %s %s %s
$link$,
    nid, nodam, nodav, name,
    section, headloss_coef, opening_pressure, closing_mode, cd_coef
    ), '') as text
    from data
)
select header.text||coalesce(bloc.text, '') from header, bloc;
$_$;


ALTER FUNCTION api._bloc_check_valve_link(model_name character varying) OWNER TO hydra;

--
-- Name: _bloc_chlorine_injection_singularity(character varying); Type: FUNCTION; Schema: api; Owner: hydra
--

CREATE FUNCTION api._bloc_chlorine_injection_singularity(model_name character varying) RETURNS text
    LANGUAGE sql STABLE
    AS $_$
with header as (
    select api._export_header('chlorine_injection', 'singularity', model_name) as text
),
data as (
    select row_number() over() as nid, s.id, s.name,
    target_node, target_concentration
    from api.chlorine_injection_singularity as s
    where s._model=model_name
),
bloc as (
    select string_agg(format(
$singularity$
%s %s %s
%s %s
$singularity$,
    nid, id, name,
    target_node, target_concentration
    ), '') as text
    from data
)
select header.text||coalesce(bloc.text, '') from header, bloc;
$_$;


ALTER FUNCTION api._bloc_chlorine_injection_singularity(model_name character varying) OWNER TO hydra;

--
-- Name: _bloc_flow_injection_singularity(character varying); Type: FUNCTION; Schema: api; Owner: hydra
--

CREATE FUNCTION api._bloc_flow_injection_singularity(model_name character varying) RETURNS text
    LANGUAGE sql STABLE
    AS $_$
with header as (
    select api._export_header('flow_injection', 'singularity', model_name) as text
),
data as (
    select row_number() over() as nid, s.id, s.name,
    q0, external_file, chlorine, trihalomethane, passive_tracer
    from api.flow_injection_singularity as s
    where s._model=model_name
),
bloc as (
    select string_agg(format(
$singularity$
%s %s %s
%s %s %s %s %s
$singularity$,
    nid, id, name,
    q0, external_file, chlorine, trihalomethane, passive_tracer
    ), '') as text
    from data
)
select header.text||coalesce(bloc.text, '') from header, bloc;
$_$;


ALTER FUNCTION api._bloc_flow_injection_singularity(model_name character varying) OWNER TO hydra;

--
-- Name: _bloc_flow_regulator_link(character varying); Type: FUNCTION; Schema: api; Owner: hydra
--

CREATE FUNCTION api._bloc_flow_regulator_link(model_name character varying) RETURNS text
    LANGUAGE sql STABLE
    AS $_$
with header as (
    select api._export_header('flow_regulator', 'link', model_name) as text
),
data as (
    select row_number() over() as nid, l.up as nodam, l.down as nodav, l.name,
    section, headloss_coef, regul_q
    from api.flow_regulator_link as l
    where l._model=model_name
),
bloc as (
    select string_agg(format(
$link$
%s %s %s %s
%s %s %s
$link$,
    nid, nodam, nodav, name,
    section, headloss_coef, regul_q
    ), '') as text
    from data
)
select header.text||coalesce(bloc.text, '') from header, bloc;
$_$;


ALTER FUNCTION api._bloc_flow_regulator_link(model_name character varying) OWNER TO hydra;

--
-- Name: _bloc_headloss_link(character varying); Type: FUNCTION; Schema: api; Owner: hydra
--

CREATE FUNCTION api._bloc_headloss_link(model_name character varying) RETURNS text
    LANGUAGE sql STABLE
    AS $_$
with header as (
    select api._export_header('headloss', 'link', model_name) as text
),
data as (
    select row_number() over() as nid, l.up as nodam, l.down as nodav, l.name,
    section, rk1, rk2
    from api.headloss_link as l
    where l._model=model_name
),
bloc as (
    select string_agg(format(
$link$
%s %s %s %s
%s %s %s
$link$,
    nid, nodam, nodav, name,
    section, rk1, rk2
    ), '') as text
    from data
)
select header.text||coalesce(bloc.text, '') from header, bloc;
$_$;


ALTER FUNCTION api._bloc_headloss_link(model_name character varying) OWNER TO hydra;

--
-- Name: _bloc_imposed_piezometry_singularity(character varying); Type: FUNCTION; Schema: api; Owner: hydra
--

CREATE FUNCTION api._bloc_imposed_piezometry_singularity(model_name character varying) RETURNS text
    LANGUAGE sql STABLE
    AS $_$
with header as (
    select api._export_header('imposed_piezometry', 'singularity', model_name) as text
),
data as (
    select row_number() over() as nid, s.id, s.name,
    z0, chlorine, trihalomethane, passive_tracer
    from api.imposed_piezometry_singularity as s
    where s._model=model_name
),
bloc as (
    select string_agg(format(
$singularity$
%s %s %s
%s %s %s %s
$singularity$,
    nid, id, name,
    z0, chlorine, trihalomethane, passive_tracer
    ), '') as text
    from data
)
select header.text||coalesce(bloc.text, '') from header, bloc;
$_$;


ALTER FUNCTION api._bloc_imposed_piezometry_singularity(model_name character varying) OWNER TO hydra;

--
-- Name: _bloc_pipe_link(character varying); Type: FUNCTION; Schema: api; Owner: hydra
--

CREATE FUNCTION api._bloc_pipe_link(model_name character varying) RETURNS text
    LANGUAGE sql STABLE
    AS $_$
with header as (
    select api._export_header('pipe', 'link', model_name) as text
),
data as (
    select row_number() over() as nid, l.up as nodam, l.down as nodav, l.name,
    diameter,
    case when overload_roughness_mm then overloaded_roughness_mm else _roughness_mm end as roughness_mm,
    case when overload_celerity then overloaded_celerity else _celerity end as celerity,
    status_open::integer,
    case when overload_length then overloaded_length else ST_Length(l.geom) end as length
    from api.pipe_link as l
    where l._model=model_name
),
bloc as (
    select string_agg(format(
$link$
%s %s %s %s
%s %s %s %s %s %s
$link$,
    nid, nodam, nodav, name,
    diameter, roughness_mm, coalesce(celerity::varchar, 'NULL'), status_open, length, 0
    ), '') as text
    from data
)
select header.text||coalesce(bloc.text, '') from header, bloc;
$_$;


ALTER FUNCTION api._bloc_pipe_link(model_name character varying) OWNER TO hydra;

--
-- Name: _bloc_pressure_accumulator_singularity(character varying); Type: FUNCTION; Schema: api; Owner: hydra
--

CREATE FUNCTION api._bloc_pressure_accumulator_singularity(model_name character varying) RETURNS text
    LANGUAGE sql STABLE
    AS $_$
with header as (
    select api._export_header('pressure_accumulator', 'singularity', model_name) as text
),
data as (
    select row_number() over() as nid, s.id, s.name,
    v0, z_connection, filling_pressure
    from api.pressure_accumulator_singularity as s
    where s._model=model_name
),
bloc as (
    select string_agg(format(
$singularity$
%s %s %s
%s %s %s
$singularity$,
    nid, id, name,
    v0, z_connection, filling_pressure
    ), '') as text
    from data
)
select header.text||coalesce(bloc.text, '') from header, bloc;
$_$;


ALTER FUNCTION api._bloc_pressure_accumulator_singularity(model_name character varying) OWNER TO hydra;

--
-- Name: _bloc_pressure_regulator_link(character varying); Type: FUNCTION; Schema: api; Owner: hydra
--

CREATE FUNCTION api._bloc_pressure_regulator_link(model_name character varying) RETURNS text
    LANGUAGE sql STABLE
    AS $_$
with header as (
    select api._export_header('pressure_regulator', 'link', model_name) as text
),
data as (
    select row_number() over() as nid, l.up as nodam, l.down as nodav, l.name,
    section, headloss_coef,
    lower(replace(pressure_regulation_mode::text, ' ', '_')) as pressure_regulation_mode,
    lower(replace(pressure_regulation_option::text, ' ', '_')) as pressure_regulation_option,
    regul_p,
    api._array_to_text(regul_pq_array::real[], 10) as regul_pq_array
    from api.pressure_regulator_link as l
    where l._model=model_name
),
bloc as (
    select string_agg(format(
$link$
%s %s %s %s
%s %s %s %s
%s
%s
$link$,
    nid, nodam, nodav, name,
    section, headloss_coef, pressure_regulation_mode, pressure_regulation_option,
    regul_p,
    regul_pq_array
    ), '') as text
    from data
)
select header.text||coalesce(bloc.text, '') from header, bloc;
$_$;


ALTER FUNCTION api._bloc_pressure_regulator_link(model_name character varying) OWNER TO hydra;

--
-- Name: _bloc_pump_link(character varying); Type: FUNCTION; Schema: api; Owner: hydra
--

CREATE FUNCTION api._bloc_pump_link(model_name character varying) RETURNS text
    LANGUAGE sql STABLE
    AS $_$
with header as (
    select api._export_header('pump', 'link', model_name)as text
),
data as (
    select row_number() over() as nid, l.up as nodam, l.down as nodav, l.name,
    api._array_to_text(pq_array::real[], 10) as pq_array,
    speed_reduction_coef, check_valve, rotation_speed, inertia, lower(replace(pump_regulation_option::text, ' ', '_')) as pump_regulation_option,
    case when pump_regulation_option='No regulation' then speed_reduction_coef::varchar
         when pump_regulation_option='Flow regulation' then regul_q::varchar
         when pump_regulation_option='Stop-start regulation' then target_node||' '||regul_z_stop::varchar||' '||regul_z_start::varchar
         when pump_regulation_option='Pressure regulation' then l.down||' '||regul_p::varchar
         end as param
    from api.pump_link as l
    where l._model=model_name
),
bloc as (
    select string_agg(format(
$link$
%s %s %s %s
%s
%s %s %s %s %s
%s
$link$,
    nid, nodam, nodav, name,
    pq_array,
    speed_reduction_coef, check_valve, rotation_speed, inertia, pump_regulation_option,
    param
    ), '') as text
    from data
)
select header.text||coalesce(bloc.text, '') from header, bloc;
$_$;


ALTER FUNCTION api._bloc_pump_link(model_name character varying) OWNER TO hydra;

--
-- Name: _bloc_reservoir_node(character varying); Type: FUNCTION; Schema: api; Owner: hydra
--

CREATE FUNCTION api._bloc_reservoir_node(model_name character varying) RETURNS text
    LANGUAGE sql STABLE
    AS $_$
with header as (
    select api._export_header('reservoir', 'node', model_name) as text
),
data as (
    select row_number() over() as nid, n.id, n.name,
    round(ST_X(geom)::numeric, 3) as x, round(ST_Y(geom)::numeric, 3) as y, zground, depth,
    api._array_to_text(sz_array::real[], 10) as sz_array,
    z_ini, z_overflow,
    lower(replace(alim_mode::text, ' ', '_')) as alim_mode,
    lower(replace(return_in_valve_mode::text, ' ', '_')) as return_in_valve_mode,
    lower(replace(return_out_valve_mode::text, ' ', '_')) as return_out_valve_mode,
    lower(replace(mixing_mode::text, ' ', '_')) as mixing_mode
    from api.reservoir_node as n
    where n.model=model_name
),
bloc as (
    select string_agg(format(
$node$
%s %s %s
%s %s %s %s
%s
%s %s %s %s %s %s
$node$,
    nid, id, name,
    x, y, zground, depth,
    sz_array,
    z_ini, z_overflow, alim_mode, return_in_valve_mode, return_out_valve_mode, mixing_mode
    ), '') as text
    from data
)
select header.text||coalesce(bloc.text, '') from header, bloc;
$_$;


ALTER FUNCTION api._bloc_reservoir_node(model_name character varying) OWNER TO hydra;

--
-- Name: _bloc_surge_tank_singularity(character varying); Type: FUNCTION; Schema: api; Owner: hydra
--

CREATE FUNCTION api._bloc_surge_tank_singularity(model_name character varying) RETURNS text
    LANGUAGE sql STABLE
    AS $_$
with header as (
    select api._export_header('surge_tank', 'singularity', model_name) as text
),
data as (
    select row_number() over() as nid, s.id, s.name,
    api._array_to_text(sz_array::real[], 10) as sz_array,
    zini, replace(pressurized_option::text, ' ', '_') as pressurized_option, orifice_diameter, in_headloss_coef, out_headloss_coef
    from api.surge_tank_singularity as s
    where s._model=model_name
),
bloc as (
    select string_agg(format(
$singularity$
%s %s %s
%s
%s %s %s %s %s
$singularity$,
    nid, id, name,
    sz_array,
    zini, pressurized_option, orifice_diameter, in_headloss_coef, out_headloss_coef
    ), '') as text
    from data
)
select header.text||coalesce(bloc.text, '') from header, bloc;
$_$;


ALTER FUNCTION api._bloc_surge_tank_singularity(model_name character varying) OWNER TO hydra;

--
-- Name: _bloc_user_node(character varying, character varying); Type: FUNCTION; Schema: api; Owner: hydra
--

CREATE FUNCTION api._bloc_user_node(scenario_name character varying, model_name character varying) RETURNS text
    LANGUAGE sql STABLE
    AS $_$
with sectors as (
    select s.name, n.model, sum(_domestic_volume) as total_volume_dom
    from api.water_delivery_sector as s
    join api.user_node as n on s.name=n._sector
    left join api._user_node_affected_volume as v on v._user_node=n.id
    group by s.name, n.model
),
header as (
    select api._export_header('user', 'node', model_name) as text
),
wd_scn as (
    select water_delivery_scenario as name
    from api.scenario
    where name=scenario_name
),
data as (
    select row_number() over() as nid, n.id, n.name,
    round(ST_X(n.geom)::numeric, 3) as x, round(ST_Y(n.geom)::numeric, 3) as y, n.zground, n.depth,
    n.fire_hydrant::integer, n.q0,
    coalesce(n._sector, 'null') as sector,
    coalesce(n.domestic_curve, 'CONSTANT') as domestic_curve,
    round(coalesce(case when s.total_volume_dom != 0 then coalesce(v._domestic_volume, 0)/s.total_volume_dom else 0 end, 0)::numeric, 3) as contrib_dom,
    coalesce(n.industrial_curve, 'CONSTANT') as industrial_curve,
    api.user_node_industrial_volume(wd_scn.name, n.id) as industrial_volume
    from wd_scn, api.user_node as n
    left join sectors as s on s.name=n._sector and s.model=n.model
    left join api._user_node_affected_volume as v on v._user_node=n.id
    where n.model=model_name
),
bloc as (
    select string_agg(format(
$node$
%s %s %s
%s %s %s %s
%s %s
%s %s %s %s %s
$node$,
    nid, id, name,
    x, y, zground, depth,
    fire_hydrant, q0,
    sector, domestic_curve, contrib_dom, industrial_curve, industrial_volume
    ), '') as text
    from data
)
select header.text||coalesce(bloc.text, '') from header, bloc;
$_$;


ALTER FUNCTION api._bloc_user_node(scenario_name character varying, model_name character varying) OWNER TO hydra;

--
-- Name: _bloc_valve_link(character varying); Type: FUNCTION; Schema: api; Owner: hydra
--

CREATE FUNCTION api._bloc_valve_link(model_name character varying) RETURNS text
    LANGUAGE sql STABLE
    AS $_$
with header as (
    select api._export_header('valve', 'link', model_name) as text
),
data as (
    select row_number() over() as nid, l.up as nodam, l.down as nodav, l.name,
    section, headloss_coef, c_opening
    from api.valve_link as l
    where l._model=model_name
),
bloc as (
    select string_agg(format(
$link$
%s %s %s %s
%s %s %s
$link$,
    nid, nodam, nodav, name,
    section, headloss_coef, c_opening
    ), '') as text
    from data
)
select header.text||coalesce(bloc.text, '') from header, bloc;
$_$;


ALTER FUNCTION api._bloc_valve_link(model_name character varying) OWNER TO hydra;

--
-- Name: _export_header(character varying, character varying, character varying); Type: FUNCTION; Schema: api; Owner: hydra
--

CREATE FUNCTION api._export_header(concrete character varying, abstract character varying, model_name character varying) RETURNS text
    LANGUAGE plpgsql STABLE
    AS $_$
declare
    _code varchar;
    _id varchar;
    _count integer;
    _res varchar;
begin
    execute 'select exportcode, exportid  from ___.type_'||abstract||' where name='''||concrete||'''' into _code, _id;
    execute 'select count(*) from api.'||concrete||'_'||abstract||' where '||case when  abstract='node' then '' else '_' end||'model='''||model_name||'''' into _count;
    return format(
$res$
$%s (%s_%s)
%s %s
$res$,
_code, concrete, abstract,
_id, _count
    );
end;
$_$;


ALTER FUNCTION api._export_header(concrete character varying, abstract character varying, model_name character varying) OWNER TO hydra;

--
-- Name: FUNCTION _export_header(concrete character varying, abstract character varying, model_name character varying); Type: COMMENT; Schema: api; Owner: hydra
--

COMMENT ON FUNCTION api._export_header(concrete character varying, abstract character varying, model_name character varying) IS 'creates a custom header for hydra export:
$export_code (table_name)
export_id object_count';


--
-- Name: _interval_to_hms(interval); Type: FUNCTION; Schema: api; Owner: hydra
--

CREATE FUNCTION api._interval_to_hms(_interval interval) RETURNS text
    LANGUAGE sql IMMUTABLE
    AS $$
select floor(extract('epoch' from _interval)/3600)||' '||to_char(_interval, 'MI SS')
$$;


ALTER FUNCTION api._interval_to_hms(_interval interval) OWNER TO hydra;

--
-- Name: FUNCTION _interval_to_hms(_interval interval); Type: COMMENT; Schema: api; Owner: hydra
--

COMMENT ON FUNCTION api._interval_to_hms(_interval interval) IS 'write an interval as "HH MM SS". HH (hours) can exceed 24.';


--
-- Name: air_relief_valve_singularity_instead_fct(); Type: FUNCTION; Schema: api; Owner: hydra
--

CREATE FUNCTION api.air_relief_valve_singularity_instead_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
declare
    overloaded_geom geometry; -- used in start_section to modify geom type
begin
if tg_op = 'INSERT' or tg_op = 'UPDATE' then
      select id, model from ___.node where ST_Equals(new.geom, geom) into new.id, new._model;
      if (select exists(select 1 from ___.singularity as s join ___.node as n on n.id=s.id where s.id!=new.id and s.name=new.name and n.model=new._model))
          then raise exception 'a singularity named % already exists in model %', new.name, new._model;
      end if;
  end if;
    if tg_op = 'INSERT' then
        insert into ___.singularity(id, name, _type, comment) values (new.id, new.name, 'air_relief_valve', new.comment) ;
        insert into ___.air_relief_valve_singularity(id, name, _type, orifice_diameter, contraction_coef) values (new.id, new.name, 'air_relief_valve', new.orifice_diameter, new.contraction_coef) ;
        return new;
    elsif tg_op = 'UPDATE' then
        update ___.singularity set id=new.id, name=new.name, comment=new.comment where id=old.id;
        update ___.air_relief_valve_singularity set id=new.id, name=new.name, orifice_diameter=new.orifice_diameter, contraction_coef=new.contraction_coef where id=old.id;
        return new;
    elsif tg_op = 'DELETE' then
        delete from ___.singularity where id=old.id;
        delete from ___.air_relief_valve_singularity where id=old.id;
        return old;
    end if;
end;
$$;


ALTER FUNCTION api.air_relief_valve_singularity_instead_fct() OWNER TO hydra;

--
-- Name: celerity(double precision, real, real); Type: FUNCTION; Schema: api; Owner: hydra
--

CREATE FUNCTION api.celerity(pipe_elasticity double precision, pipe_diameter real, pipe_thickness real) RETURNS numeric
    LANGUAGE sql STABLE
    AS $$
select round((1./sqrt(volumic_mass_kg_m3/volumic_elasticity_module_N_m2 + volumic_mass_kg_m3*pipe_diameter/(pipe_elasticity*pipe_thickness)))::numeric, 2)
from ___.fluid_properties;
$$;


ALTER FUNCTION api.celerity(pipe_elasticity double precision, pipe_diameter real, pipe_thickness real) OWNER TO hydra;

--
-- Name: check_valve_link_instead_fct(); Type: FUNCTION; Schema: api; Owner: hydra
--

CREATE FUNCTION api.check_valve_link_instead_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
declare
    overloaded_geom geometry; -- used in start_section to modify geom type
begin
if tg_op = 'INSERT' or tg_op = 'UPDATE' then
      select ST_Force2D(new.geom) into new.geom;
      -- nullif(u.model, nullif(u.model, d.model)) returns null if models do not match else return the value if it match
      select u.id, d.id, nullif(u.model, nullif(u.model, d.model))
      from ___.node as u, ___.node as d
      where ST_Equals(ST_StartPoint(new.geom), u.geom) and ST_Equals(ST_EndPoint(new.geom), d.geom)
      into new.up, new.down, new._model;
      if new._model is null
          then raise exception 'upstream and downstream nodes are not in the same model';
      elsif (select exists(select 1 from ___.link as l join ___.node as n on n.id=l.up where l.id!=new.id and l.name=new.name and n.model=new._model))
          then raise exception 'a link named % already exists in model %', new.name, new._model;
      end if;
  end if;
    if tg_op = 'INSERT' then
        insert into ___.link(id, name, _type, up, down, comment, geom) values (new.id, new.name, 'check_valve', new.up, new.down, new.comment, coalesce(overloaded_geom, new.geom)) ;
        insert into ___.check_valve_link(id, name, _type, section, headloss_coef, opening_pressure, closing_mode, cd_coef) values (new.id, new.name, 'check_valve', new.section, new.headloss_coef, new.opening_pressure, new.closing_mode, new.cd_coef) ;
        return new;
    elsif tg_op = 'UPDATE' then
        update ___.link set id=new.id, name=new.name, up=new.up, down=new.down, comment=new.comment, geom=coalesce(overloaded_geom, new.geom) where id=old.id;
        update ___.check_valve_link set id=new.id, name=new.name, section=new.section, headloss_coef=new.headloss_coef, opening_pressure=new.opening_pressure, closing_mode=new.closing_mode, cd_coef=new.cd_coef where id=old.id;
        return new;
    elsif tg_op = 'DELETE' then
        delete from ___.link where id=old.id;
        delete from ___.check_valve_link where id=old.id;
        return old;
    end if;
end;
$$;


ALTER FUNCTION api.check_valve_link_instead_fct() OWNER TO hydra;

--
-- Name: chlorine_injection_singularity_instead_fct(); Type: FUNCTION; Schema: api; Owner: hydra
--

CREATE FUNCTION api.chlorine_injection_singularity_instead_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
declare
    overloaded_geom geometry; -- used in start_section to modify geom type
begin
if tg_op = 'INSERT' or tg_op = 'UPDATE' then
      select id, model from ___.node where ST_Equals(new.geom, geom) into new.id, new._model;
      if (select exists(select 1 from ___.singularity as s join ___.node as n on n.id=s.id where s.id!=new.id and s.name=new.name and n.model=new._model))
          then raise exception 'a singularity named % already exists in model %', new.name, new._model;
      end if;
      select coalesce(new.target_node, new.id, old.target_node) into new.target_node;
  end if;
    if tg_op = 'INSERT' then
        insert into ___.singularity(id, name, _type, comment) values (new.id, new.name, 'chlorine_injection', new.comment) ;
        insert into ___.chlorine_injection_singularity(id, name, _type, target_concentration, target_node) values (new.id, new.name, 'chlorine_injection', new.target_concentration, new.target_node) ;
        return new;
    elsif tg_op = 'UPDATE' then
        update ___.singularity set id=new.id, name=new.name, comment=new.comment where id=old.id;
        update ___.chlorine_injection_singularity set id=new.id, name=new.name, target_concentration=new.target_concentration, target_node=new.target_node where id=old.id;
        return new;
    elsif tg_op = 'DELETE' then
        delete from ___.singularity where id=old.id;
        delete from ___.chlorine_injection_singularity where id=old.id;
        return old;
    end if;
end;
$$;


ALTER FUNCTION api.chlorine_injection_singularity_instead_fct() OWNER TO hydra;

--
-- Name: failure_curve_instead_fct(); Type: FUNCTION; Schema: api; Owner: hydra
--

CREATE FUNCTION api.failure_curve_instead_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
declare
    overloaded_geom geometry; -- used in start_section to modify geom type
begin
    if tg_op = 'INSERT' then
        insert into ___.failure_curve(name, comment, alpt_array) values (new.name, new.comment, new.alpt_array::real[]) ;
        return new;
    elsif tg_op = 'UPDATE' then
        update ___.failure_curve set name=new.name, comment=new.comment, alpt_array=new.alpt_array::real[] where name=old.name;
        return new;
    elsif tg_op = 'DELETE' then
        delete from ___.failure_curve where name=old.name;
        return old;
    end if;
end;
$$;


ALTER FUNCTION api.failure_curve_instead_fct() OWNER TO hydra;

--
-- Name: file_act_dat(); Type: FUNCTION; Schema: api; Owner: hydra
--

CREATE FUNCTION api.file_act_dat() RETURNS text
    LANGUAGE sql STABLE
    AS $_$
select string_agg(format(
$alp$
%s
%s
$alp$,
name, api._array_to_text(alpt_array::real[])), '' order by name)
from api.failure_curve;
$_$;


ALTER FUNCTION api.file_act_dat() OWNER TO hydra;

--
-- Name: file_cmd(character varying); Type: FUNCTION; Schema: api; Owner: hydra
--

CREATE FUNCTION api.file_cmd(scenario_name character varying) RETURNS text
    LANGUAGE sql STABLE
    AS $_$
with water_origin as (
    select string_agg(format($pol_origin$%s %s$pol_origin$, ip._model, ip.name), E'\n') as pol_origin
    from api.scenario_water_origin as wo
    join api.imposed_piezometry_singularity as ip on wo.imposed_piezometry_singularity= ip.id
    where wo.scenario=scenario_name
    ),
data_dactv as (
    select row_number() over() as n, t.exportid, l.name,
        api._interval_to_hms(f.failure_timestamp) as failure_timestamp, f.failure_curve
    from api.scenario_link_failure as f
    join ___.type_link as t on t.name=f.link_type
    join ___.link as l on l.id=f.link
    where f.scenario = scenario_name
    ),
bloc_dactv as (
    select count(*) as numel, string_agg(format(
        E'%s %s %s\n%s %s',
        n, exportid, name,
        failure_timestamp, failure_curve
        ), E'\n') as text
    from data_dactv
    ),
data_dactp as (
    select row_number() over() as n, exportid, l.name,
        api._interval_to_hms(f.failure_timestamp) as failure_timestamp, lower(replace(f.failure_mode::text, ' ', '_')) as failure_mode,
        case when f.failure_mode='Automatic alpha computation' then '    '
             when f.failure_mode='Alpha(t) curve' then f.failure_curve
             when f.failure_mode='Four quadrants curve' then (select four_quadrant_file from api.scenario where name=scenario_name)
        end as additional_data
    from api.scenario_pump_failure as f
    join ___.type_link as t on t.name=f.link_type
    join ___.link as l on l.id=f.link
    where f.scenario = scenario_name
    ),
bloc_dactp as (
    select count(*) as numel, string_agg(format(
        E'%s %s %s\n%s %s\n%s',
        n, exportid, name,
        failure_timestamp, failure_mode,
        additional_data
        ), E'\n') as text
    from data_dactp
    ),
bloc_regul as (
    select count(*) as n, string_agg(regulation_file, E'\n' order by priority) as list
    from api.regulation_scenario where scenario=scenario_name
    )
select format(
$cmd$
*time
%s%s

%s

%s

%s

%s

%s

%s

%s

%s

%s

$cmd$,
to_char(starting_date, 'YYYY MM DD HH24 MI'),
case when comput_mode!='Steady state' then format(
$time_details$
%s
%s
%s
$time_details$,
    api._interval_to_hms(duration),
    timestep,
    api._interval_to_hms(tini)) else '' end,
case when comput_mode!='Steady state' then format(
$sor$
*sor
%s
%s
%s
$sor$,
    dt_output_hr,
    api._interval_to_hms(tbegin_output_hr),
    api._interval_to_hms(tend_output_hr)) else '' end,
case when flag_save then format(
$save$
*save
%s
$save$,
    api._interval_to_hms(tsave_hr)) else '' end,
case when flag_rstart then format(
$rstart$
*rstart
%s
%s
$rstart$,
    scenario_rstart, api._interval_to_hms(trstart_hr)) else '' end,
case when refined_discretisation then '*refine_dx' else '' end,
case when vaporization then '*vaporization' else '' end,
case when quality_output='Tracer' then format(
$tracer$
*pol_tracer
%s
$tracer$,
    case when chlorine then 'chlorine '||coalesce(chlorine_water_reaction_coef, 0)||' '||coalesce(chlorine_wall_reaction_coef, 0)||E'\n' else '' end ||
    case when trihalomethane then 'trihalomethane '||coalesce(trihalomethane_reaction_parameter, 0)||' '||coalesce(trihalomethane_concentration_limit, 0)||E'\n' else '' end ||
    case when passive_tracer then E'passive_tracer 0 0\n' else '' end ||
    case when travel_time then E'travel_time 0 0\n' else '' end
) when quality_output='Water origin' then format(
$water_origin$
*pol_origin
%s
$water_origin$,
    pol_origin) else '' end,
case when bloc_dactv.numel>0 then format(
$dactv$
*dactv
2 %s
%s
$dactv$,
    bloc_dactv.numel, bloc_dactv.text) else '' end,
case when bloc_dactp.numel>0 then format(
$dactp$
*dactp
3 %s
%s
$dactp$,
    bloc_dactp.numel, bloc_dactp.text) else '' end,
case when bloc_regul.n>0 then format(
$regul$
*f_regul
%s
$regul$,
    bloc_regul.list) else '' end
)
from api.scenario, water_origin, bloc_dactv, bloc_dactp, bloc_regul
where name=scenario_name;
$_$;


ALTER FUNCTION api.file_cmd(scenario_name character varying) OWNER TO hydra;

--
-- Name: file_cts(); Type: FUNCTION; Schema: api; Owner: hydra
--

CREATE FUNCTION api.file_cts() RETURNS text
    LANGUAGE sql STABLE
    AS $_$
with hourly_modulation_curves as (
    select name, string_agg(format($curve$%s %s %s$curve$,
                                   u.i, u.weekdays, u.weekends
                                  ), E'\n') as curve
    from api.hourly_modulation_curve, unnest(
                                            array(select generate_series(1,24)),
                                            (hourly_modulation_array::real[])[:][1:1],
                                            (hourly_modulation_array::real[])[:][2:2]
                                        ) as u(i, weekdays, weekends)
    group by name
)
select string_agg(format(
$cts$
%s
%s
$cts$,
name, curve), '')
from hourly_modulation_curves;
$_$;


ALTER FUNCTION api.file_cts() OWNER TO hydra;

--
-- Name: file_dat(character varying, character varying); Type: FUNCTION; Schema: api; Owner: hydra
--

CREATE FUNCTION api.file_dat(scenario_name character varying, model_name character varying) RETURNS text
    LANGUAGE sql STABLE
    AS $$
select concat(api._bloc_user_node(scenario_name, model_name), api._bloc_reservoir_node(model_name),
api._bloc_pipe_link(model_name), api._bloc_valve_link(model_name), api._bloc_flow_regulator_link(model_name), api._bloc_pressure_regulator_link(model_name),
api._bloc_check_valve_link(model_name), api._bloc_pump_link(model_name), api._bloc_headloss_link(model_name), api._bloc_imposed_piezometry_singularity(model_name),
api._bloc_flow_injection_singularity(model_name), api._bloc_chlorine_injection_singularity(model_name), api._bloc_surge_tank_singularity(model_name),
api._bloc_pressure_accumulator_singularity(model_name), api._bloc_air_relief_valve_singularity(model_name));
$$;


ALTER FUNCTION api.file_dat(scenario_name character varying, model_name character varying) OWNER TO hydra;

--
-- Name: file_optsor_dat(); Type: FUNCTION; Schema: api; Owner: hydra
--

CREATE FUNCTION api.file_optsor_dat() RETURNS text
    LANGUAGE sql STABLE
    AS $_$
with bloc as (
    select string_agg(format(
$ind$%s %s$ind$,
    n.model, n.id--, n.industrial_volume + n.domestic_volume
    ), E'\n') as text
    from api.user_node as n
    --where n.industrial_volume > 0
)
select E'*CONSO_IND\n'||coalesce(bloc.text, '') from bloc;
$_$;


ALTER FUNCTION api.file_optsor_dat() OWNER TO hydra;

--
-- Name: file_phys_dat(); Type: FUNCTION; Schema: api; Owner: hydra
--

CREATE FUNCTION api.file_phys_dat() RETURNS text
    LANGUAGE sql STABLE
    AS $_$
select format(
$phys$
*EAU (caracteristiques du fluide)
%s %s %s %s

*HCRIT (valeur des alarmes)
%s
%s %s
%s %s
$phys$,
volumic_mass_kg_m3, temperature_c, kinematic_viscosity_m2_s, volumic_elasticity_module_N_m2,
minimum_service_pressure,
pipe_maximum_speed, pipe_minimum_speed,
node_maximum_pressure, node_minimum_pressure
)
from api.fluid_properties, api.threshold_warning_settings;
$_$;


ALTER FUNCTION api.file_phys_dat() OWNER TO hydra;

--
-- Name: file_qts(character varying); Type: FUNCTION; Schema: api; Owner: hydra
--

CREATE FUNCTION api.file_qts(water_delivery_scenario_name character varying) RETURNS text
    LANGUAGE sql STABLE
    AS $_$
with sectors as (
    select string_agg(format($wdsector$%s %s %s %s$wdsector$,
                             sector.name, setting.volume_m3j, setting.leak_efficiency, setting.adjust_coef
                            ), E'\n') as params
    from api.water_delivery_sector as sector
    join api.water_delivery_scenario_sector_setting as setting on sector.name=setting.sector and setting.scenario=water_delivery_scenario_name
)
select format(
$qts$
%s
%s %s
%s
$qts$,
scenario.comment,
(select count(*) from api.water_delivery_sector), case when scenario.period='Weekday' then 1 else 2 end,
sectors.params)
from api.water_delivery_scenario as scenario, sectors
where scenario.name=water_delivery_scenario_name;
$_$;


ALTER FUNCTION api.file_qts(water_delivery_scenario_name character varying) OWNER TO hydra;

--
-- Name: file_rac(); Type: FUNCTION; Schema: api; Owner: hydra
--

CREATE FUNCTION api.file_rac() RETURNS text
    LANGUAGE sql STABLE
    AS $_$
with header as (
    select E'$'||exportcode||E' (model connection)\n'||(select count(*) from api.model_connection_singularity)||E'\n' as text
    from ___.type_singularity as s
    where s.name='model_connection'
),
bloc as (
    select string_agg(format(
$rac$
%s %s %s %s
%s
$rac$,
    s._model, s.name, s.id, lower(replace(cascade_mode::text, ' ', '_')),
    api._array_to_text(
        case when cascade_mode = 'Imposed piezometry Z(t)' then zt_array::real[]
             when cascade_mode = 'Delivery Q(t)'           then qt_array::real[]
        end , 20)
    ), '') as text
    from api.model_connection_singularity as s
)
select header.text||coalesce(E'\n'||bloc.text, '') from header, bloc;
$_$;


ALTER FUNCTION api.file_rac() OWNER TO hydra;

--
-- Name: file_scnnom(character varying); Type: FUNCTION; Schema: api; Owner: hydra
--

CREATE FUNCTION api.file_scnnom(scenario_name character varying) RETURNS text
    LANGUAGE sql STABLE
    AS $_$
with scenario_settings as (
    select
        case when comput_mode='Steady state' then 1
             when comput_mode='Gradual transient' then 2
             when comput_mode='Fast transient' then 3
             when comput_mode='Quality only' then 4
        end as computation_mode,
        scenario_ref
    from api.scenario
    where name=scenario_name
    ),
modeles as (
    select string_agg(name, E' ') as list from api.model
    ),
grouping as (
    select string_agg(models, E'\n' order by priority) as list from api.model_grouping(scenario_name)
    )
select format(
$scnnom$
*scenario
%s

*modeles
%s

*computation_mode
%s

*groupage
%s

%s

$scnnom$,
scenario_name,
modeles.list,
scenario_settings.computation_mode,
grouping.list,
coalesce(E'*scnref\n'||scenario_settings.scenario_ref, '')
)
from scenario_settings, modeles, grouping;
$_$;


ALTER FUNCTION api.file_scnnom(scenario_name character varying) OWNER TO hydra;

--
-- Name: flow_injection_singularity_instead_fct(); Type: FUNCTION; Schema: api; Owner: hydra
--

CREATE FUNCTION api.flow_injection_singularity_instead_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
declare
    overloaded_geom geometry; -- used in start_section to modify geom type
begin
if tg_op = 'INSERT' or tg_op = 'UPDATE' then
      select id, model from ___.node where ST_Equals(new.geom, geom) into new.id, new._model;
      if (select exists(select 1 from ___.singularity as s join ___.node as n on n.id=s.id where s.id!=new.id and s.name=new.name and n.model=new._model))
          then raise exception 'a singularity named % already exists in model %', new.name, new._model;
      end if;
  end if;
    if tg_op = 'INSERT' then
        insert into ___.singularity(id, name, _type, comment) values (new.id, new.name, 'flow_injection', new.comment) ;
        insert into ___.flow_injection_singularity(id, name, _type, q0, external_file, chlorine, trihalomethane, passive_tracer) values (new.id, new.name, 'flow_injection', new.q0, new.external_file, new.chlorine, new.trihalomethane, new.passive_tracer) ;
        return new;
    elsif tg_op = 'UPDATE' then
        update ___.singularity set id=new.id, name=new.name, comment=new.comment where id=old.id;
        update ___.flow_injection_singularity set id=new.id, name=new.name, q0=new.q0, external_file=new.external_file, chlorine=new.chlorine, trihalomethane=new.trihalomethane, passive_tracer=new.passive_tracer where id=old.id;
        return new;
    elsif tg_op = 'DELETE' then
        delete from ___.singularity where id=old.id;
        delete from ___.flow_injection_singularity where id=old.id;
        return old;
    end if;
end;
$$;


ALTER FUNCTION api.flow_injection_singularity_instead_fct() OWNER TO hydra;

--
-- Name: flow_regulator_link_instead_fct(); Type: FUNCTION; Schema: api; Owner: hydra
--

CREATE FUNCTION api.flow_regulator_link_instead_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
declare
    overloaded_geom geometry; -- used in start_section to modify geom type
begin
if tg_op = 'INSERT' or tg_op = 'UPDATE' then
      select ST_Force2D(new.geom) into new.geom;
      -- nullif(u.model, nullif(u.model, d.model)) returns null if models do not match else return the value if it match
      select u.id, d.id, nullif(u.model, nullif(u.model, d.model))
      from ___.node as u, ___.node as d
      where ST_Equals(ST_StartPoint(new.geom), u.geom) and ST_Equals(ST_EndPoint(new.geom), d.geom)
      into new.up, new.down, new._model;
      if new._model is null
          then raise exception 'upstream and downstream nodes are not in the same model';
      elsif (select exists(select 1 from ___.link as l join ___.node as n on n.id=l.up where l.id!=new.id and l.name=new.name and n.model=new._model))
          then raise exception 'a link named % already exists in model %', new.name, new._model;
      end if;
  end if;
    if tg_op = 'INSERT' then
        insert into ___.link(id, name, _type, up, down, comment, geom) values (new.id, new.name, 'flow_regulator', new.up, new.down, new.comment, coalesce(overloaded_geom, new.geom)) ;
        insert into ___.flow_regulator_link(id, name, _type, section, headloss_coef, regul_q) values (new.id, new.name, 'flow_regulator', new.section, new.headloss_coef, new.regul_q) ;
        return new;
    elsif tg_op = 'UPDATE' then
        update ___.link set id=new.id, name=new.name, up=new.up, down=new.down, comment=new.comment, geom=coalesce(overloaded_geom, new.geom) where id=old.id;
        update ___.flow_regulator_link set id=new.id, name=new.name, section=new.section, headloss_coef=new.headloss_coef, regul_q=new.regul_q where id=old.id;
        return new;
    elsif tg_op = 'DELETE' then
        delete from ___.link where id=old.id;
        delete from ___.flow_regulator_link where id=old.id;
        return old;
    end if;
end;
$$;


ALTER FUNCTION api.flow_regulator_link_instead_fct() OWNER TO hydra;

--
-- Name: fluid_properties_instead_fct(); Type: FUNCTION; Schema: api; Owner: hydra
--

CREATE FUNCTION api.fluid_properties_instead_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
declare
    overloaded_geom geometry; -- used in start_section to modify geom type
begin
    if tg_op = 'INSERT' then
        insert into ___.fluid_properties(id, volumic_mass_kg_m3, temperature_c, kinematic_viscosity_m2_s, volumic_elasticity_module_n_m2) values (new.id, new.volumic_mass_kg_m3, new.temperature_c, new.kinematic_viscosity_m2_s, new.volumic_elasticity_module_n_m2) ;
        return new;
    elsif tg_op = 'UPDATE' then
        update ___.fluid_properties set id=new.id, volumic_mass_kg_m3=new.volumic_mass_kg_m3, temperature_c=new.temperature_c, kinematic_viscosity_m2_s=new.kinematic_viscosity_m2_s, volumic_elasticity_module_n_m2=new.volumic_elasticity_module_n_m2 where id=old.id;
        return new;
    elsif tg_op = 'DELETE' then
        raise notice 'cannot delete from table fluid_properties';
        return old;
    end if;
end;
$$;


ALTER FUNCTION api.fluid_properties_instead_fct() OWNER TO hydra;

--
-- Name: headloss_link_instead_fct(); Type: FUNCTION; Schema: api; Owner: hydra
--

CREATE FUNCTION api.headloss_link_instead_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
declare
    overloaded_geom geometry; -- used in start_section to modify geom type
begin
if tg_op = 'INSERT' or tg_op = 'UPDATE' then
      select ST_Force2D(new.geom) into new.geom;
      -- nullif(u.model, nullif(u.model, d.model)) returns null if models do not match else return the value if it match
      select u.id, d.id, nullif(u.model, nullif(u.model, d.model))
      from ___.node as u, ___.node as d
      where ST_Equals(ST_StartPoint(new.geom), u.geom) and ST_Equals(ST_EndPoint(new.geom), d.geom)
      into new.up, new.down, new._model;
      if new._model is null
          then raise exception 'upstream and downstream nodes are not in the same model';
      elsif (select exists(select 1 from ___.link as l join ___.node as n on n.id=l.up where l.id!=new.id and l.name=new.name and n.model=new._model))
          then raise exception 'a link named % already exists in model %', new.name, new._model;
      end if;
  end if;
    if tg_op = 'INSERT' then
        insert into ___.link(id, name, _type, up, down, comment, geom) values (new.id, new.name, 'headloss', new.up, new.down, new.comment, coalesce(overloaded_geom, new.geom)) ;
        insert into ___.headloss_link(id, name, _type, section, rk1, rk2) values (new.id, new.name, 'headloss', new.section, new.rk1, new.rk2) ;
        return new;
    elsif tg_op = 'UPDATE' then
        update ___.link set id=new.id, name=new.name, up=new.up, down=new.down, comment=new.comment, geom=coalesce(overloaded_geom, new.geom) where id=old.id;
        update ___.headloss_link set id=new.id, name=new.name, section=new.section, rk1=new.rk1, rk2=new.rk2 where id=old.id;
        return new;
    elsif tg_op = 'DELETE' then
        delete from ___.link where id=old.id;
        delete from ___.headloss_link where id=old.id;
        return old;
    end if;
end;
$$;


ALTER FUNCTION api.headloss_link_instead_fct() OWNER TO hydra;

--
-- Name: hourly_modulation_curve_instead_fct(); Type: FUNCTION; Schema: api; Owner: hydra
--

CREATE FUNCTION api.hourly_modulation_curve_instead_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
declare
    overloaded_geom geometry; -- used in start_section to modify geom type
begin
    if tg_op = 'INSERT' then
        insert into ___.hourly_modulation_curve(name, comment, hourly_modulation_array) values (new.name, new.comment, new.hourly_modulation_array::real[]) ;
        return new;
    elsif tg_op = 'UPDATE' then
        update ___.hourly_modulation_curve set name=new.name, comment=new.comment, hourly_modulation_array=new.hourly_modulation_array::real[] where name=old.name;
        return new;
    elsif tg_op = 'DELETE' then
        delete from ___.hourly_modulation_curve where name=old.name;
        return old;
    end if;
end;
$$;


ALTER FUNCTION api.hourly_modulation_curve_instead_fct() OWNER TO hydra;

--
-- Name: imposed_piezometry_singularity_instead_fct(); Type: FUNCTION; Schema: api; Owner: hydra
--

CREATE FUNCTION api.imposed_piezometry_singularity_instead_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
declare
    overloaded_geom geometry; -- used in start_section to modify geom type
begin
if tg_op = 'INSERT' or tg_op = 'UPDATE' then
      select id, model from ___.node where ST_Equals(new.geom, geom) into new.id, new._model;
      if (select exists(select 1 from ___.singularity as s join ___.node as n on n.id=s.id where s.id!=new.id and s.name=new.name and n.model=new._model))
          then raise exception 'a singularity named % already exists in model %', new.name, new._model;
      end if;
  end if;
    if tg_op = 'INSERT' then
        insert into ___.singularity(id, name, _type, comment) values (new.id, new.name, 'imposed_piezometry', new.comment) ;
        insert into ___.imposed_piezometry_singularity(id, name, _type, z0, chlorine, trihalomethane, passive_tracer) values (new.id, new.name, 'imposed_piezometry', new.z0, new.chlorine, new.trihalomethane, new.passive_tracer) ;
        return new;
    elsif tg_op = 'UPDATE' then
        update ___.singularity set id=new.id, name=new.name, comment=new.comment where id=old.id;
        update ___.imposed_piezometry_singularity set id=new.id, name=new.name, z0=new.z0, chlorine=new.chlorine, trihalomethane=new.trihalomethane, passive_tracer=new.passive_tracer where id=old.id;
        return new;
    elsif tg_op = 'DELETE' then
        delete from ___.singularity where id=old.id;
        delete from ___.imposed_piezometry_singularity where id=old.id;
        return old;
    end if;
end;
$$;


ALTER FUNCTION api.imposed_piezometry_singularity_instead_fct() OWNER TO hydra;

--
-- Name: link_3d_geom(public.geometry, integer, integer); Type: FUNCTION; Schema: api; Owner: hydra
--

CREATE FUNCTION api.link_3d_geom(geom_2d public.geometry, up_node integer, down_node integer) RETURNS public.geometry
    LANGUAGE sql
    AS $$
with points as (
    select geom, ST_LineLocatePoint(geom_2d, geom) as pk
    from ST_DumpPoints(geom_2d)
    ),
interpolated as (
    select ST_MakePoint(ST_X(p.geom), ST_Y(p.geom), (1-p.pk) * ST_Z(u.geom) + p.pk * ST_Z(d.geom)) as geom, pk
    from points as p
    join ___.node as d on d.id = down_node
    join ___.node as u on u.id = up_node
    )
select ST_SetSRID(ST_Makeline(geom order by pk), ST_SRID(geom_2d))
from interpolated;
$$;


ALTER FUNCTION api.link_3d_geom(geom_2d public.geometry, up_node integer, down_node integer) OWNER TO hydra;

--
-- Name: model_connection_singularity_instead_fct(); Type: FUNCTION; Schema: api; Owner: hydra
--

CREATE FUNCTION api.model_connection_singularity_instead_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
declare
    overloaded_geom geometry; -- used in start_section to modify geom type
begin
if tg_op = 'INSERT' or tg_op = 'UPDATE' then
      select id, model from ___.node where ST_Equals(new.geom, geom) into new.id, new._model;
      if (select exists(select 1 from ___.singularity as s join ___.node as n on n.id=s.id where s.id!=new.id and s.name=new.name and n.model=new._model))
          then raise exception 'a singularity named % already exists in model %', new.name, new._model;
      end if;
  end if;
    if tg_op = 'INSERT' then
        insert into ___.singularity(id, name, _type, comment) values (new.id, new.name, 'model_connection', new.comment) ;
        insert into ___.model_connection_singularity(id, name, _type, cascade_mode, zt_array, qt_array) values (new.id, new.name, 'model_connection', new.cascade_mode, new.zt_array::real[], new.qt_array::real[]) ;
        return new;
    elsif tg_op = 'UPDATE' then
        update ___.singularity set id=new.id, name=new.name, comment=new.comment where id=old.id;
        update ___.model_connection_singularity set id=new.id, name=new.name, cascade_mode=new.cascade_mode, zt_array=new.zt_array::real[], qt_array=new.qt_array::real[] where id=old.id;
        return new;
    elsif tg_op = 'DELETE' then
        delete from ___.singularity where id=old.id;
        delete from ___.model_connection_singularity where id=old.id;
        return old;
    end if;
end;
$$;


ALTER FUNCTION api.model_connection_singularity_instead_fct() OWNER TO hydra;

--
-- Name: model_grouping(character varying); Type: FUNCTION; Schema: api; Owner: hydra
--

CREATE FUNCTION api.model_grouping(scenario_name character varying) RETURNS TABLE(priority integer, models character varying)
    LANGUAGE sql STABLE
    AS $$
with model_global as (
    select 1 as priority, name as model
    from api.model
    order by name
    ),
model_cascade as (
    select priority, model
    from (
        select priority, model from api.cascade where scenario=scenario_name
            union all
        select 999 as priority, name as model
        from api.model
        where not exists (select 1 from api.cascade where scenario=scenario_name and name=model)
        ) as patched_cascadey
    ),
model_mixed as (
    select priority, model
    from api.groupe_model as gp
    join api.mixed as m on gp.groupe=m.groupe
    where m.scenario=scenario_name
    )
select priority, string_agg(model, E' ' order by model) as models from model_global join api.scenario on model_connect_settings='Global' where name=scenario_name group by priority
    union all
select priority, model as models from model_cascade join api.scenario on model_connect_settings='Cascade' where name=scenario_name
    union all
select priority, string_agg(model, E' ' order by model) as models from model_mixed join api.scenario on model_connect_settings='Mixed' where name=scenario_name group by priority;
$$;


ALTER FUNCTION api.model_grouping(scenario_name character varying) OWNER TO hydra;

--
-- Name: pipe_link_instead_fct(); Type: FUNCTION; Schema: api; Owner: hydra
--

CREATE FUNCTION api.pipe_link_instead_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
declare
    overloaded_geom geometry; -- used in start_section to modify geom type
begin

        if tg_op = 'INSERT' or tg_op = 'UPDATE' then
            select ST_Force2D(new.geom) into new.geom;
            -- nullif(A, nullif(A, B)) returns null if A!=B else return A(=B)
            select u.id, d.id, nullif(u.model, nullif(u.model, d.model))
            from ___.node as u, ___.node as d
            where ST_Equals(ST_StartPoint(new.geom), u.geom) and ST_Equals(ST_EndPoint(new.geom), d.geom)
            into new.up, new.down, new._model;
            if new._model is null
                then raise exception 'upstream and downstream nodes are not in the same model';
            elsif (select exists(select 1 from ___.link as l join ___.node as n on n.id=l.up where l.id!=new.id and l.name=new.name and n.model=new._model))
                then raise exception 'a link named % already exists in model %', new.name, new._model;
            end if;
        end if;
        if tg_op = 'INSERT' then
        insert into ___.link(id, name, _type, up, down, comment, geom) values (new.id, new.name, 'pipe', new.up, new.down, new.comment, coalesce(overloaded_geom, new.geom)) ;
        insert into ___.pipe_link(id, name, _type, diameter, thickness_mm, material, status_open, meter, overload_length, overloaded_length, overload_celerity, overloaded_celerity, overload_roughness_mm, overloaded_roughness_mm) values (new.id, new.name, 'pipe', new.diameter, new.thickness_mm, new.material, new.status_open, new.meter, new.overload_length, new.overloaded_length, new.overload_celerity, new.overloaded_celerity, new.overload_roughness_mm, new.overloaded_roughness_mm) ;
        return new;
    elsif tg_op = 'UPDATE' then
        update ___.link set id=new.id, name=new.name, up=new.up, down=new.down, comment=new.comment, geom=coalesce(overloaded_geom, new.geom) where id=old.id;
        update ___.pipe_link set id=new.id, name=new.name, diameter=new.diameter, thickness_mm=new.thickness_mm, material=new.material, status_open=new.status_open, meter=new.meter, overload_length=new.overload_length, overloaded_length=new.overloaded_length, overload_celerity=new.overload_celerity, overloaded_celerity=new.overloaded_celerity, overload_roughness_mm=new.overload_roughness_mm, overloaded_roughness_mm=new.overloaded_roughness_mm where id=old.id;
        return new;
    elsif tg_op = 'DELETE' then
        delete from ___.link where id=old.id;
        delete from ___.pipe_link where id=old.id;
        return old;
    end if;
end;
$$;


ALTER FUNCTION api.pipe_link_instead_fct() OWNER TO hydra;

--
-- Name: pressure_accumulator_singularity_instead_fct(); Type: FUNCTION; Schema: api; Owner: hydra
--

CREATE FUNCTION api.pressure_accumulator_singularity_instead_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
declare
    overloaded_geom geometry; -- used in start_section to modify geom type
begin
if tg_op = 'INSERT' or tg_op = 'UPDATE' then
      select id, model from ___.node where ST_Equals(new.geom, geom) into new.id, new._model;
      if (select exists(select 1 from ___.singularity as s join ___.node as n on n.id=s.id where s.id!=new.id and s.name=new.name and n.model=new._model))
          then raise exception 'a singularity named % already exists in model %', new.name, new._model;
      end if;
  end if;
    if tg_op = 'INSERT' then
        insert into ___.singularity(id, name, _type, comment) values (new.id, new.name, 'pressure_accumulator', new.comment) ;
        insert into ___.pressure_accumulator_singularity(id, name, _type, v0, z_connection, filling_pressure) values (new.id, new.name, 'pressure_accumulator', new.v0, new.z_connection, new.filling_pressure) ;
        return new;
    elsif tg_op = 'UPDATE' then
        update ___.singularity set id=new.id, name=new.name, comment=new.comment where id=old.id;
        update ___.pressure_accumulator_singularity set id=new.id, name=new.name, v0=new.v0, z_connection=new.z_connection, filling_pressure=new.filling_pressure where id=old.id;
        return new;
    elsif tg_op = 'DELETE' then
        delete from ___.singularity where id=old.id;
        delete from ___.pressure_accumulator_singularity where id=old.id;
        return old;
    end if;
end;
$$;


ALTER FUNCTION api.pressure_accumulator_singularity_instead_fct() OWNER TO hydra;

--
-- Name: pressure_regulator_link_instead_fct(); Type: FUNCTION; Schema: api; Owner: hydra
--

CREATE FUNCTION api.pressure_regulator_link_instead_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
declare
    overloaded_geom geometry; -- used in start_section to modify geom type
begin
if tg_op = 'INSERT' or tg_op = 'UPDATE' then
      select ST_Force2D(new.geom) into new.geom;
      -- nullif(u.model, nullif(u.model, d.model)) returns null if models do not match else return the value if it match
      select u.id, d.id, nullif(u.model, nullif(u.model, d.model))
      from ___.node as u, ___.node as d
      where ST_Equals(ST_StartPoint(new.geom), u.geom) and ST_Equals(ST_EndPoint(new.geom), d.geom)
      into new.up, new.down, new._model;
      if new._model is null
          then raise exception 'upstream and downstream nodes are not in the same model';
      elsif (select exists(select 1 from ___.link as l join ___.node as n on n.id=l.up where l.id!=new.id and l.name=new.name and n.model=new._model))
          then raise exception 'a link named % already exists in model %', new.name, new._model;
      end if;
  end if;
    if tg_op = 'INSERT' then
        insert into ___.link(id, name, _type, up, down, comment, geom) values (new.id, new.name, 'pressure_regulator', new.up, new.down, new.comment, coalesce(overloaded_geom, new.geom)) ;
        insert into ___.pressure_regulator_link(id, name, _type, section, headloss_coef, pressure_regulation_option, regul_p, regul_pq_array, pressure_regulation_mode) values (new.id, new.name, 'pressure_regulator', new.section, new.headloss_coef, new.pressure_regulation_option, new.regul_p, new.regul_pq_array::real[], new.pressure_regulation_mode) ;
        return new;
    elsif tg_op = 'UPDATE' then
        update ___.link set id=new.id, name=new.name, up=new.up, down=new.down, comment=new.comment, geom=coalesce(overloaded_geom, new.geom) where id=old.id;
        update ___.pressure_regulator_link set id=new.id, name=new.name, section=new.section, headloss_coef=new.headloss_coef, pressure_regulation_option=new.pressure_regulation_option, regul_p=new.regul_p, regul_pq_array=new.regul_pq_array::real[], pressure_regulation_mode=new.pressure_regulation_mode where id=old.id;
        return new;
    elsif tg_op = 'DELETE' then
        delete from ___.link where id=old.id;
        delete from ___.pressure_regulator_link where id=old.id;
        return old;
    end if;
end;
$$;


ALTER FUNCTION api.pressure_regulator_link_instead_fct() OWNER TO hydra;

--
-- Name: pump_link_instead_fct(); Type: FUNCTION; Schema: api; Owner: hydra
--

CREATE FUNCTION api.pump_link_instead_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
declare
    overloaded_geom geometry; -- used in start_section to modify geom type
begin
if tg_op = 'INSERT' or tg_op = 'UPDATE' then
      select ST_Force2D(new.geom) into new.geom;
      -- nullif(u.model, nullif(u.model, d.model)) returns null if models do not match else return the value if it match
      select u.id, d.id, nullif(u.model, nullif(u.model, d.model))
      from ___.node as u, ___.node as d
      where ST_Equals(ST_StartPoint(new.geom), u.geom) and ST_Equals(ST_EndPoint(new.geom), d.geom)
      into new.up, new.down, new._model;
      if new._model is null
          then raise exception 'upstream and downstream nodes are not in the same model';
      elsif (select exists(select 1 from ___.link as l join ___.node as n on n.id=l.up where l.id!=new.id and l.name=new.name and n.model=new._model))
          then raise exception 'a link named % already exists in model %', new.name, new._model;
      end if;
      select coalesce(new.target_node, new.down, old.target_node) into new.target_node;
  end if;
    if tg_op = 'INSERT' then
        insert into ___.link(id, name, _type, up, down, comment, geom) values (new.id, new.name, 'pump', new.up, new.down, new.comment, coalesce(overloaded_geom, new.geom)) ;
        insert into ___.pump_link(id, name, _type, pq_array, check_valve, rotation_speed, inertia, pump_regulation_option, speed_reduction_coef, regul_q, target_node, regul_z_start, regul_z_stop, regul_p) values (new.id, new.name, 'pump', new.pq_array::real[], new.check_valve, new.rotation_speed, new.inertia, new.pump_regulation_option, new.speed_reduction_coef, new.regul_q, new.target_node, new.regul_z_start, new.regul_z_stop, new.regul_p) ;
        return new;
    elsif tg_op = 'UPDATE' then
        update ___.link set id=new.id, name=new.name, up=new.up, down=new.down, comment=new.comment, geom=coalesce(overloaded_geom, new.geom) where id=old.id;
        update ___.pump_link set id=new.id, name=new.name, pq_array=new.pq_array::real[], check_valve=new.check_valve, rotation_speed=new.rotation_speed, inertia=new.inertia, pump_regulation_option=new.pump_regulation_option, speed_reduction_coef=new.speed_reduction_coef, regul_q=new.regul_q, target_node=new.target_node, regul_z_start=new.regul_z_start, regul_z_stop=new.regul_z_stop, regul_p=new.regul_p where id=old.id;
        return new;
    elsif tg_op = 'DELETE' then
        delete from ___.link where id=old.id;
        delete from ___.pump_link where id=old.id;
        return old;
    end if;
end;
$$;


ALTER FUNCTION api.pump_link_instead_fct() OWNER TO hydra;

--
-- Name: reservoir_node_instead_fct(); Type: FUNCTION; Schema: api; Owner: hydra
--

CREATE FUNCTION api.reservoir_node_instead_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
declare
    overloaded_geom geometry; -- used in start_section to modify geom type
begin

        if tg_op = 'INSERT' or tg_op = 'UPDATE' then
            select ST_SetSRID(ST_MakePoint(ST_X(new.geom), ST_Y(new.geom), coalesce(new.zground, 9999)-coalesce(new.depth, 0)), ST_SRID(new.geom)) into overloaded_geom;
        end if;
        if tg_op = 'INSERT' then
        insert into ___.node(id, name, model, _type, comment, zground, geom) values (new.id, new.name, new.model, 'reservoir', new.comment, new.zground, coalesce(overloaded_geom, new.geom)) ;
        insert into ___.reservoir_node(id, name, _type, sz_array, alim_mode, z_ini, z_overflow, return_in_valve_mode, return_out_valve_mode, mixing_mode) values (new.id, new.name, 'reservoir', new.sz_array::real[], new.alim_mode, new.z_ini, new.z_overflow, new.return_in_valve_mode, new.return_out_valve_mode, new.mixing_mode) ;
        return new;
    elsif tg_op = 'UPDATE' then
        update ___.node set id=new.id, name=new.name, model=new.model, comment=new.comment, zground=new.zground, geom=coalesce(overloaded_geom, new.geom) where id=old.id;
        update ___.reservoir_node set id=new.id, name=new.name, sz_array=new.sz_array::real[], alim_mode=new.alim_mode, z_ini=new.z_ini, z_overflow=new.z_overflow, return_in_valve_mode=new.return_in_valve_mode, return_out_valve_mode=new.return_out_valve_mode, mixing_mode=new.mixing_mode where id=old.id;
        return new;
    elsif tg_op = 'DELETE' then
        delete from ___.node where id=old.id;
        delete from ___.reservoir_node where id=old.id;
        return old;
    end if;
end;
$$;


ALTER FUNCTION api.reservoir_node_instead_fct() OWNER TO hydra;

--
-- Name: scenario_instead_fct(); Type: FUNCTION; Schema: api; Owner: hydra
--

CREATE FUNCTION api.scenario_instead_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
declare
    overloaded_geom geometry; -- used in start_section to modify geom type
begin

        if tg_op = 'INSERT' or tg_op = 'UPDATE' then
            select nullif(new.water_delivery_scenario, 'WD_SCN_REF') into new.water_delivery_scenario;
        end if;
        if tg_op = 'INSERT' then
        insert into ___.scenario(name, comment, comput_mode, starting_date, duration, timestep, refined_discretisation, water_delivery_scenario, peak_coefficient, tini, flag_save, tsave_hr, flag_rstart, scenario_rstart, trstart_hr, dt_output_hr, tbegin_output_hr, tend_output_hr, scenario_ref, model_connect_settings, option_file, four_quadrant_file, vaporization, quality_output, chlorine, trihalomethane, passive_tracer, travel_time, chlorine_water_reaction_coef, chlorine_wall_reaction_coef, trihalomethane_reaction_parameter, trihalomethane_concentration_limit) values (new.name, new.comment, new.comput_mode, new.starting_date, new.duration, new.timestep, new.refined_discretisation, new.water_delivery_scenario, new.peak_coefficient, new.tini, new.flag_save, new.tsave_hr, new.flag_rstart, new.scenario_rstart, new.trstart_hr, new.dt_output_hr, new.tbegin_output_hr, new.tend_output_hr, new.scenario_ref, new.model_connect_settings, new.option_file, new.four_quadrant_file, new.vaporization, new.quality_output, new.chlorine, new.trihalomethane, new.passive_tracer, new.travel_time, new.chlorine_water_reaction_coef, new.chlorine_wall_reaction_coef, new.trihalomethane_reaction_parameter, new.trihalomethane_concentration_limit) ;
        return new;
    elsif tg_op = 'UPDATE' then
        update ___.scenario set name=new.name, comment=new.comment, comput_mode=new.comput_mode, starting_date=new.starting_date, duration=new.duration, timestep=new.timestep, refined_discretisation=new.refined_discretisation, water_delivery_scenario=new.water_delivery_scenario, peak_coefficient=new.peak_coefficient, tini=new.tini, flag_save=new.flag_save, tsave_hr=new.tsave_hr, flag_rstart=new.flag_rstart, scenario_rstart=new.scenario_rstart, trstart_hr=new.trstart_hr, dt_output_hr=new.dt_output_hr, tbegin_output_hr=new.tbegin_output_hr, tend_output_hr=new.tend_output_hr, scenario_ref=new.scenario_ref, model_connect_settings=new.model_connect_settings, option_file=new.option_file, four_quadrant_file=new.four_quadrant_file, vaporization=new.vaporization, quality_output=new.quality_output, chlorine=new.chlorine, trihalomethane=new.trihalomethane, passive_tracer=new.passive_tracer, travel_time=new.travel_time, chlorine_water_reaction_coef=new.chlorine_water_reaction_coef, chlorine_wall_reaction_coef=new.chlorine_wall_reaction_coef, trihalomethane_reaction_parameter=new.trihalomethane_reaction_parameter, trihalomethane_concentration_limit=new.trihalomethane_concentration_limit where name=old.name;
        return new;
    elsif tg_op = 'DELETE' then
        delete from ___.scenario where name=old.name;
        return old;
    end if;
end;
$$;


ALTER FUNCTION api.scenario_instead_fct() OWNER TO hydra;

--
-- Name: scenario_link_failure_instead_fct(); Type: FUNCTION; Schema: api; Owner: hydra
--

CREATE FUNCTION api.scenario_link_failure_instead_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
declare
    overloaded_geom geometry; -- used in start_section to modify geom type
begin
    if tg_op = 'INSERT' then
        insert into ___.scenario_link_failure(id, scenario, link, link_type, failure_timestamp, failure_curve) values (new.id, new.scenario, new.link, new.link_type, new.failure_timestamp, new.failure_curve) ;
        return new;
    elsif tg_op = 'UPDATE' then
        update ___.scenario_link_failure set id=new.id, scenario=new.scenario, link=new.link, link_type=new.link_type, failure_timestamp=new.failure_timestamp, failure_curve=new.failure_curve where id=old.id;
        return new;
    elsif tg_op = 'DELETE' then
        delete from ___.scenario_link_failure where id=old.id;
        return old;
    end if;
end;
$$;


ALTER FUNCTION api.scenario_link_failure_instead_fct() OWNER TO hydra;

--
-- Name: scenario_pump_failure_instead_fct(); Type: FUNCTION; Schema: api; Owner: hydra
--

CREATE FUNCTION api.scenario_pump_failure_instead_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
declare
    overloaded_geom geometry; -- used in start_section to modify geom type
begin
    if tg_op = 'INSERT' then
        insert into ___.scenario_pump_failure(id, scenario, link, link_type, failure_timestamp, failure_curve, failure_mode) values (new.id, new.scenario, new.link, new.link_type, new.failure_timestamp, new.failure_curve, new.failure_mode) ;
        return new;
    elsif tg_op = 'UPDATE' then
        update ___.scenario_pump_failure set id=new.id, scenario=new.scenario, link=new.link, link_type=new.link_type, failure_timestamp=new.failure_timestamp, failure_curve=new.failure_curve, failure_mode=new.failure_mode where id=old.id;
        return new;
    elsif tg_op = 'DELETE' then
        delete from ___.scenario_pump_failure where id=old.id;
        return old;
    end if;
end;
$$;


ALTER FUNCTION api.scenario_pump_failure_instead_fct() OWNER TO hydra;

--
-- Name: scenario_water_origin_instead_fct(); Type: FUNCTION; Schema: api; Owner: hydra
--

CREATE FUNCTION api.scenario_water_origin_instead_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
declare
    overloaded_geom geometry; -- used in start_section to modify geom type
begin

        if tg_op = 'INSERT' or tg_op = 'UPDATE' then
            if new.name is not null and new.model is not null then
                select id from api.imposed_piezometry_singularity where name=new.name and _model=new.model into new.imposed_piezometry_singularity;
            end if;
        end if;
        if tg_op = 'INSERT' then
        insert into ___.scenario_water_origin(id, scenario, imposed_piezometry_singularity, comment) values (new.id, new.scenario, new.imposed_piezometry_singularity, new.comment) ;
        return new;
    elsif tg_op = 'UPDATE' then
        update ___.scenario_water_origin set id=new.id, scenario=new.scenario, imposed_piezometry_singularity=new.imposed_piezometry_singularity, comment=new.comment where id=old.id;
        return new;
    elsif tg_op = 'DELETE' then
        delete from ___.scenario_water_origin where id=old.id;
        return old;
    end if;
end;
$$;


ALTER FUNCTION api.scenario_water_origin_instead_fct() OWNER TO hydra;

--
-- Name: sectorization_settings_instead_fct(); Type: FUNCTION; Schema: api; Owner: hydra
--

CREATE FUNCTION api.sectorization_settings_instead_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
declare
    overloaded_geom geometry; -- used in start_section to modify geom type
begin
    if tg_op = 'INSERT' then
        insert into ___.sectorization_settings(pipe_link_meter, valve_link_open, pressure_regulator_link, flow_regulator_link, reservoir_node, id) values (new.pipe_link_meter, new.valve_link_open, new.pressure_regulator_link, new.flow_regulator_link, new.reservoir_node, new.id) ;
        return new;
    elsif tg_op = 'UPDATE' then
        update ___.sectorization_settings set pipe_link_meter=new.pipe_link_meter, valve_link_open=new.valve_link_open, pressure_regulator_link=new.pressure_regulator_link, flow_regulator_link=new.flow_regulator_link, reservoir_node=new.reservoir_node, id=new.id where id=old.id;
        return new;
    elsif tg_op = 'DELETE' then
        raise notice 'cannot delete from table sectorization_settings';
        return old;
    end if;
end;
$$;


ALTER FUNCTION api.sectorization_settings_instead_fct() OWNER TO hydra;

--
-- Name: surge_tank_singularity_instead_fct(); Type: FUNCTION; Schema: api; Owner: hydra
--

CREATE FUNCTION api.surge_tank_singularity_instead_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
declare
    overloaded_geom geometry; -- used in start_section to modify geom type
begin
if tg_op = 'INSERT' or tg_op = 'UPDATE' then
      select id, model from ___.node where ST_Equals(new.geom, geom) into new.id, new._model;
      if (select exists(select 1 from ___.singularity as s join ___.node as n on n.id=s.id where s.id!=new.id and s.name=new.name and n.model=new._model))
          then raise exception 'a singularity named % already exists in model %', new.name, new._model;
      end if;
  end if;
    if tg_op = 'INSERT' then
        insert into ___.singularity(id, name, _type, comment) values (new.id, new.name, 'surge_tank', new.comment) ;
        insert into ___.surge_tank_singularity(id, name, _type, sz_array, zini, pressurized_option, orifice_diameter, in_headloss_coef, out_headloss_coef) values (new.id, new.name, 'surge_tank', new.sz_array::real[], new.zini, new.pressurized_option, new.orifice_diameter, new.in_headloss_coef, new.out_headloss_coef) ;
        return new;
    elsif tg_op = 'UPDATE' then
        update ___.singularity set id=new.id, name=new.name, comment=new.comment where id=old.id;
        update ___.surge_tank_singularity set id=new.id, name=new.name, sz_array=new.sz_array::real[], zini=new.zini, pressurized_option=new.pressurized_option, orifice_diameter=new.orifice_diameter, in_headloss_coef=new.in_headloss_coef, out_headloss_coef=new.out_headloss_coef where id=old.id;
        return new;
    elsif tg_op = 'DELETE' then
        delete from ___.singularity where id=old.id;
        delete from ___.surge_tank_singularity where id=old.id;
        return old;
    end if;
end;
$$;


ALTER FUNCTION api.surge_tank_singularity_instead_fct() OWNER TO hydra;

--
-- Name: threshold_warning_settings_instead_fct(); Type: FUNCTION; Schema: api; Owner: hydra
--

CREATE FUNCTION api.threshold_warning_settings_instead_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
declare
    overloaded_geom geometry; -- used in start_section to modify geom type
begin
    if tg_op = 'INSERT' then
        insert into ___.threshold_warning_settings(pipe_maximum_speed, pipe_minimum_speed, node_maximum_pressure, node_minimum_pressure, minimum_service_pressure, id) values (new.pipe_maximum_speed, new.pipe_minimum_speed, new.node_maximum_pressure, new.node_minimum_pressure, new.minimum_service_pressure, new.id) ;
        return new;
    elsif tg_op = 'UPDATE' then
        update ___.threshold_warning_settings set pipe_maximum_speed=new.pipe_maximum_speed, pipe_minimum_speed=new.pipe_minimum_speed, node_maximum_pressure=new.node_maximum_pressure, node_minimum_pressure=new.node_minimum_pressure, minimum_service_pressure=new.minimum_service_pressure, id=new.id where id=old.id;
        return new;
    elsif tg_op = 'DELETE' then
        raise notice 'cannot delete from table threshold_warning_settings';
        return old;
    end if;
end;
$$;


ALTER FUNCTION api.threshold_warning_settings_instead_fct() OWNER TO hydra;

--
-- Name: user_node_industrial_volume(character varying, integer); Type: FUNCTION; Schema: api; Owner: hydra
--

CREATE FUNCTION api.user_node_industrial_volume(wd_scn_name character varying, user_node_id integer) RETURNS real
    LANGUAGE sql STABLE
    AS $$
select coalesce(sum(wdi.volume * wdi.industrial::integer), 0)::real
from api.water_delivery_scenario_industrial_volume as wdi
join api.water_delivery_point as wd on wd.name=wdi.water_delivery_point
where wd._node=user_node_id and wdi.water_delivery_scenario=wd_scn_name
$$;


ALTER FUNCTION api.user_node_industrial_volume(wd_scn_name character varying, user_node_id integer) OWNER TO hydra;

--
-- Name: user_node_instead_fct(); Type: FUNCTION; Schema: api; Owner: hydra
--

CREATE FUNCTION api.user_node_instead_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
declare
    overloaded_geom geometry; -- used in start_section to modify geom type
begin

        if tg_op = 'INSERT' or tg_op = 'UPDATE' then
            if (select count(*) > 1 from ___.water_delivery_sector where ST_Intersects(new.geom, geom))
                then raise exception 'user node % would be in multiple water delivery sectors', new.name;
            elsif (select exists (select 1 from ___.water_delivery_sector where ST_DWithin(new.geom, ST_Boundary(geom), 0.0001)))
                then raise exception 'user node % would be on water delivery sector boundary', new.name;
            end if;
            select ST_SetSRID(ST_MakePoint(ST_X(new.geom), ST_Y(new.geom), coalesce(new.zground, 9999)-coalesce(new.depth, 0)), ST_SRID(new.geom)) into overloaded_geom;
            select nullif(new.domestic_curve, 'Constant') into new.domestic_curve;
            select nullif(new.industrial_curve, 'Constant') into new.industrial_curve;
        end if;
        if tg_op = 'INSERT' then
        insert into ___.node(id, name, model, _type, comment, zground, geom) values (new.id, new.name, new.model, 'user', new.comment, new.zground, coalesce(overloaded_geom, new.geom)) ;
        insert into ___.user_node(id, name, _type, domestic_curve, industrial_curve, q0, fire_hydrant) values (new.id, new.name, 'user', new.domestic_curve, new.industrial_curve, new.q0, new.fire_hydrant) ;
        return new;
    elsif tg_op = 'UPDATE' then
        update ___.node set id=new.id, name=new.name, model=new.model, comment=new.comment, zground=new.zground, geom=coalesce(overloaded_geom, new.geom) where id=old.id;
        update ___.user_node set id=new.id, name=new.name, domestic_curve=new.domestic_curve, industrial_curve=new.industrial_curve, q0=new.q0, fire_hydrant=new.fire_hydrant where id=old.id;
        return new;
    elsif tg_op = 'DELETE' then
        delete from ___.node where id=old.id;
        delete from ___.user_node where id=old.id;
        return old;
    end if;
end;
$$;


ALTER FUNCTION api.user_node_instead_fct() OWNER TO hydra;

--
-- Name: valve_link_instead_fct(); Type: FUNCTION; Schema: api; Owner: hydra
--

CREATE FUNCTION api.valve_link_instead_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
declare
    overloaded_geom geometry; -- used in start_section to modify geom type
begin
if tg_op = 'INSERT' or tg_op = 'UPDATE' then
      select ST_Force2D(new.geom) into new.geom;
      -- nullif(u.model, nullif(u.model, d.model)) returns null if models do not match else return the value if it match
      select u.id, d.id, nullif(u.model, nullif(u.model, d.model))
      from ___.node as u, ___.node as d
      where ST_Equals(ST_StartPoint(new.geom), u.geom) and ST_Equals(ST_EndPoint(new.geom), d.geom)
      into new.up, new.down, new._model;
      if new._model is null
          then raise exception 'upstream and downstream nodes are not in the same model';
      elsif (select exists(select 1 from ___.link as l join ___.node as n on n.id=l.up where l.id!=new.id and l.name=new.name and n.model=new._model))
          then raise exception 'a link named % already exists in model %', new.name, new._model;
      end if;
  end if;
    if tg_op = 'INSERT' then
        insert into ___.link(id, name, _type, up, down, comment, geom) values (new.id, new.name, 'valve', new.up, new.down, new.comment, coalesce(overloaded_geom, new.geom)) ;
        insert into ___.valve_link(id, name, _type, section, headloss_coef, c_opening) values (new.id, new.name, 'valve', new.section, new.headloss_coef, new.c_opening) ;
        return new;
    elsif tg_op = 'UPDATE' then
        update ___.link set id=new.id, name=new.name, up=new.up, down=new.down, comment=new.comment, geom=coalesce(overloaded_geom, new.geom) where id=old.id;
        update ___.valve_link set id=new.id, name=new.name, section=new.section, headloss_coef=new.headloss_coef, c_opening=new.c_opening where id=old.id;
        return new;
    elsif tg_op = 'DELETE' then
        delete from ___.link where id=old.id;
        delete from ___.valve_link where id=old.id;
        return old;
    end if;
end;
$$;


ALTER FUNCTION api.valve_link_instead_fct() OWNER TO hydra;

--
-- Name: water_delivery_point_instead_fct(); Type: FUNCTION; Schema: api; Owner: hydra
--

CREATE FUNCTION api.water_delivery_point_instead_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
declare
    overloaded_geom geometry; -- used in start_section to modify geom type
begin

        if tg_op = 'INSERT' and new.pipe_link is null then
            select id from ___.link as n where _type='pipe' order by n.geom <-> new.geom limit 1 into new.pipe_link;
        elsif tg_op = 'UPDATE' and new.industrial is False then
            delete from ___.water_delivery_scenario_industrial_volume where water_delivery_point=new.name;
        end if;
        if tg_op = 'INSERT' then
        insert into ___.water_delivery_point(name, comment, geom, volume, industrial, pipe_link) values (new.name, new.comment, coalesce(overloaded_geom, new.geom), new.volume, new.industrial, new.pipe_link) ;
        return new;
    elsif tg_op = 'UPDATE' then
        update ___.water_delivery_point set name=new.name, comment=new.comment, geom=coalesce(overloaded_geom, new.geom), volume=new.volume, industrial=new.industrial, pipe_link=new.pipe_link where name=old.name;
        return new;
    elsif tg_op = 'DELETE' then
        delete from ___.water_delivery_point where name=old.name;
        return old;
    end if;
end;
$$;


ALTER FUNCTION api.water_delivery_point_instead_fct() OWNER TO hydra;

--
-- Name: water_delivery_scenario_industrial_volume_instead_fct(); Type: FUNCTION; Schema: api; Owner: hydra
--

CREATE FUNCTION api.water_delivery_scenario_industrial_volume_instead_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
begin
    if tg_op = 'INSERT' or tg_op = 'UPDATE' then
        insert into ___.water_delivery_scenario_industrial_volume(water_delivery_scenario, water_delivery_point, volume)
            values (new.water_delivery_scenario, new.water_delivery_point, new.volume)
        on conflict (water_delivery_scenario, water_delivery_point)
        do update set volume=excluded.volume;
        return new;
    elsif tg_op = 'DELETE' then
        delete from ___.water_delivery_scenario_industrial_volume
        where water_delivery_scenario=old.water_delivery_scenario and water_delivery_point=old.water_delivery_point;
        return old;
    end if;
end;
$$;


ALTER FUNCTION api.water_delivery_scenario_industrial_volume_instead_fct() OWNER TO hydra;

--
-- Name: FUNCTION water_delivery_scenario_industrial_volume_instead_fct(); Type: COMMENT; Schema: api; Owner: hydra
--

COMMENT ON FUNCTION api.water_delivery_scenario_industrial_volume_instead_fct() IS 'custom trigger to facilitate updating properties
of given pair water delivery scenarios / water delivery points parameters just insert and if pair already exists => on conflict switches to update';


--
-- Name: water_delivery_scenario_instead_fct(); Type: FUNCTION; Schema: api; Owner: hydra
--

CREATE FUNCTION api.water_delivery_scenario_instead_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
declare
    overloaded_geom geometry; -- used in start_section to modify geom type
begin
    if tg_op = 'INSERT' then
        insert into ___.water_delivery_scenario(name, comment, period) values (new.name, new.comment, new.period) ;
        return new;
    elsif tg_op = 'UPDATE' then
        update ___.water_delivery_scenario set name=new.name, comment=new.comment, period=new.period where name=old.name;
        return new;
    elsif tg_op = 'DELETE' then
        delete from ___.water_delivery_scenario where name=old.name;
        return old;
    end if;
end;
$$;


ALTER FUNCTION api.water_delivery_scenario_instead_fct() OWNER TO hydra;

--
-- Name: water_delivery_scenario_sector_setting_instead_fct(); Type: FUNCTION; Schema: api; Owner: hydra
--

CREATE FUNCTION api.water_delivery_scenario_sector_setting_instead_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
begin
    if tg_op = 'INSERT' then
        insert into ___.water_delivery_scenario_sector_setting(scenario, sector, volume_m3j, adjust_coef, leak_efficiency)
            values (new.scenario, new.sector, new.volume_m3j, new.adjust_coef, new.leak_efficiency)
        on conflict (scenario, sector)
        do update set volume_m3j=excluded.volume_m3j, adjust_coef=excluded.adjust_coef, leak_efficiency=excluded.leak_efficiency;
        return new;
    elsif tg_op = 'UPDATE' then
        update ___.water_delivery_scenario_sector_setting
        set scenario=new.scenario, sector=new.sector, volume_m3j=new.volume_m3j, adjust_coef=new.adjust_coef, leak_efficiency=new.leak_efficiency
        where scenario=old.scenario and sector=old.sector;
        return new;
    elsif tg_op = 'DELETE' then
        delete from ___.water_delivery_scenario_sector_setting
        where scenario=old.scenario and sector=old.sector;
        return old;
    end if;
end;
$$;


ALTER FUNCTION api.water_delivery_scenario_sector_setting_instead_fct() OWNER TO hydra;

--
-- Name: FUNCTION water_delivery_scenario_sector_setting_instead_fct(); Type: COMMENT; Schema: api; Owner: hydra
--

COMMENT ON FUNCTION api.water_delivery_scenario_sector_setting_instead_fct() IS 'custom trigger to facilitate updating properties
of given pair water delivery scenarios / sectors parameters just insert and if pair already exists => on conflict switches to update';


--
-- Name: water_delivery_sector_instead_fct(); Type: FUNCTION; Schema: api; Owner: hydra
--

CREATE FUNCTION api.water_delivery_sector_instead_fct() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
declare
    overloaded_geom geometry; -- used in start_section to modify geom type
begin

        if tg_op = 'INSERT' or tg_op = 'UPDATE' then
            if (select exists (select 1 from ___.node as n
                    join ___.water_delivery_sector as w on ST_Intersects(w.geom, n.geom) and w.name != new.name and w.name != old.name
                    where ST_Intersects(new.geom, n.geom) and n._type = 'user')
                    )
                then raise exception 'water delivery sector % would contain user nodes already in another water delivery sector', new.name;
            elsif (select exists (select 1 from ___.node as n where ST_DWithin(ST_Boundary(new.geom), n.geom, 0.0001) and n._type = 'user'))
                then raise exception 'water delivery sector % would cross user nodes on its boundary', new.name;
            end if;
        end if;
        if tg_op = 'INSERT' then
        insert into ___.water_delivery_sector(name, comment, geom) values (new.name, new.comment, coalesce(overloaded_geom, new.geom)) ;
        return new;
    elsif tg_op = 'UPDATE' then
        update ___.water_delivery_sector set name=new.name, comment=new.comment, geom=coalesce(overloaded_geom, new.geom) where name=old.name;
        return new;
    elsif tg_op = 'DELETE' then
        delete from ___.water_delivery_sector where name=old.name;
        return old;
    end if;
end;
$$;


ALTER FUNCTION api.water_delivery_sector_instead_fct() OWNER TO hydra;

--
-- Name: create_api(); Type: FUNCTION; Schema: public; Owner: hydra
--

CREATE FUNCTION public.create_api() RETURNS character varying
    LANGUAGE sql
    AS $_$

drop schema if exists api cascade;

create schema api;

comment on schema api is 'views and functions schema for expresseau, for user access';


------------------------------------------------------------------------------------------------
-- METADATA                                                                                   --
------------------------------------------------------------------------------------------------

select template.basic_view('metadata');

------------------------------------------------------------------------------------------------
-- Links 3D geom for view from nodes and 2D geom                                                                          --
------------------------------------------------------------------------------------------------

create or replace function api.link_3d_geom(geom_2d geometry('LINESTRING'), up_node integer, down_node integer)
returns geometry('LINESTRINGZ')
language sql volatile as
$$
with points as (
    select geom, ST_LineLocatePoint(geom_2d, geom) as pk
    from ST_DumpPoints(geom_2d)
    ),
interpolated as (
    select ST_MakePoint(ST_X(p.geom), ST_Y(p.geom), (1-p.pk) * ST_Z(u.geom) + p.pk * ST_Z(d.geom)) as geom, pk
    from points as p
    join ___.node as d on d.id = down_node
    join ___.node as u on u.id = up_node
    )
select ST_SetSRID(ST_Makeline(geom order by pk), ST_SRID(geom_2d))
from interpolated;
$$
;


------------------------------------------------------------------------------------------------
-- MODELS                                                                                     --
------------------------------------------------------------------------------------------------

select template.basic_view('model');


------------------------------------------------------------------------------------------------
-- Water delivery                                                                             --
------------------------------------------------------------------------------------------------

select template.basic_view('water_delivery_sector',
    with_trigger => True,
    start_section =>
    $$
        if tg_op = 'INSERT' or tg_op = 'UPDATE' then
            if (select exists (select 1 from ___.node as n
                    join ___.water_delivery_sector as w on ST_Intersects(w.geom, n.geom) and w.name != new.name and w.name != old.name
                    where ST_Intersects(new.geom, n.geom) and n._type = 'user')
                    )
                then raise exception 'water delivery sector % would contain user nodes already in another water delivery sector', new.name;
            elsif (select exists (select 1 from ___.node as n where ST_DWithin(ST_Boundary(new.geom), n.geom, 0.0001) and n._type = 'user'))
                then raise exception 'water delivery sector % would cross user nodes on its boundary', new.name;
            end if;
        end if;
    $$
);

select template.basic_view('water_delivery_scenario',
    additional_union => 'union all select ''WD_SCN_REF'' as name, ''Reference scenario - not editable'' as comment, ''Weekday'' as period',
    with_trigger=>True);

select template.basic_view('water_delivery_point',
    additional_columns => ('{
        case when ST_LinelocatePoint(l.geom, c.geom)<0.5 then l.up else l.down end as _node,
        ST_Force2D(ST_Makeline(
            ARRAY[
                c.geom,
                ST_ClosestPoint(l.geom, c.geom),
                case when ST_LinelocatePoint(l.geom, c.geom)<0.5 then ST_StartPoint(l.geom) else ST_EndPoint(l.geom) end
                ]
            ))::geometry(LINESTRING,'||(select srid from ___.metadata)||') as _affectation_geom
        }')::varchar[],
    additional_join => 'left join ___.link as l on l.id=c.pipe_link',
    with_trigger => True,
    start_section =>
    $$
        if tg_op = 'INSERT' and new.pipe_link is null then
            select id from ___.link as n where _type='pipe' order by n.geom <-> new.geom limit 1 into new.pipe_link;
        elsif tg_op = 'UPDATE' and new.industrial is False then
            delete from ___.water_delivery_scenario_industrial_volume where water_delivery_point=new.name;
        end if;
    $$
);

select template.basic_view('water_delivery_scenario_industrial_volume',
    additional_union => 'union all
                            select
                                s.name as water_delivery_scenario,
                                p.name as water_delivery_point,
                                p.volume as volume,
                                p.industrial as industrial
                            from ___.water_delivery_scenario as s, ___.water_delivery_point as p
                            where p.industrial and not exists (
                                select 1 from ___.water_delivery_scenario_industrial_volume
                                where water_delivery_scenario=s.name and water_delivery_point=p.name
                            )
                         union all
                            select
                                ''WD_SCN_REF'' as water_delivery_scenario,
                                p.name as water_delivery_point,
                                p.volume as volume,
                                p.industrial as industrial
                            from ___.water_delivery_point as p
                            where p.industrial'
);

create function api.water_delivery_scenario_industrial_volume_instead_fct()
returns trigger
language plpgsql volatile as
$$
begin
    if tg_op = 'INSERT' or tg_op = 'UPDATE' then
        insert into ___.water_delivery_scenario_industrial_volume(water_delivery_scenario, water_delivery_point, volume)
            values (new.water_delivery_scenario, new.water_delivery_point, new.volume)
        on conflict (water_delivery_scenario, water_delivery_point)
        do update set volume=excluded.volume;
        return new;
    elsif tg_op = 'DELETE' then
        delete from ___.water_delivery_scenario_industrial_volume
        where water_delivery_scenario=old.water_delivery_scenario and water_delivery_point=old.water_delivery_point;
        return old;
    end if;
end;
$$
;

comment on function api.water_delivery_scenario_industrial_volume_instead_fct() is 'custom trigger to facilitate updating properties
of given pair water delivery scenarios / water delivery points parameters just insert and if pair already exists => on conflict switches to update';

create trigger water_delivery_scenario_industrial_volume_instead_trig
instead of insert or delete or update on api.water_delivery_scenario_industrial_volume
for each row execute procedure api.water_delivery_scenario_industrial_volume_instead_fct();

create or replace view api._user_node_affected_volume as
    select n.id as _user_node,
        sum(case when not wd.industrial then wd.volume else 0 end) as _domestic_volume,
        n.geom as geom,
        array_agg(wd.name) as _sources
    from ___.node as n
    join api.water_delivery_point as wd on wd._node=n.id
    where n._type='user'
    group by n.id, n.geom
;

-- water delivery scenarios / sectors settings
select template.basic_view('water_delivery_scenario_sector_setting',
    additional_union =>
    'union all
        select
            ''WD_SCN_REF'' as scenario,
            s.name as sector,
            coalesce(sum(n._domestic_volume), 0) as volume_m3j,
            1 as adjust_coef,
            0.8 as leak_efficiency
        from ___.water_delivery_sector as s
        left join api._user_node_affected_volume as n on ST_Contains(s.geom, n.geom)
        group by s.name'
);

create function api.water_delivery_scenario_sector_setting_instead_fct()
returns trigger
language plpgsql volatile as
$$
begin
    if tg_op = 'INSERT' then
        insert into ___.water_delivery_scenario_sector_setting(scenario, sector, volume_m3j, adjust_coef, leak_efficiency)
            values (new.scenario, new.sector, new.volume_m3j, new.adjust_coef, new.leak_efficiency)
        on conflict (scenario, sector)
        do update set volume_m3j=excluded.volume_m3j, adjust_coef=excluded.adjust_coef, leak_efficiency=excluded.leak_efficiency;
        return new;
    elsif tg_op = 'UPDATE' then
        update ___.water_delivery_scenario_sector_setting
        set scenario=new.scenario, sector=new.sector, volume_m3j=new.volume_m3j, adjust_coef=new.adjust_coef, leak_efficiency=new.leak_efficiency
        where scenario=old.scenario and sector=old.sector;
        return new;
    elsif tg_op = 'DELETE' then
        delete from ___.water_delivery_scenario_sector_setting
        where scenario=old.scenario and sector=old.sector;
        return old;
    end if;
end;
$$
;

comment on function api.water_delivery_scenario_sector_setting_instead_fct() is 'custom trigger to facilitate updating properties
of given pair water delivery scenarios / sectors parameters just insert and if pair already exists => on conflict switches to update';

create trigger water_delivery_scenario_sector_setting_instead_trig
instead of insert or delete or update on api.water_delivery_scenario_sector_setting
for each row execute procedure api.water_delivery_scenario_sector_setting_instead_fct();


------------------------------------------------------------------------------------------------
-- Settings                                                                                   --
------------------------------------------------------------------------------------------------

select template.basic_view('fluid_properties', with_trigger=>True, cannot_delete=>True);
select template.basic_view('material');
select template.basic_view('node');
select template.basic_view('hourly_modulation_curve', with_trigger=>True);
select template.basic_view('failure_curve', with_trigger=>True);
select template.basic_view('threshold_warning_settings', with_trigger=>True, cannot_delete=>True);
select template.basic_view('sectorization_settings', with_trigger=>True, cannot_delete=>True);



------------------------------------------------------------------------------------------------
-- Pipes celerity                                                                             --
------------------------------------------------------------------------------------------------

create or replace function api.celerity(pipe_elasticity double precision, pipe_diameter real, pipe_thickness real)
returns numeric
language sql stable as
$$
select round((1./sqrt(volumic_mass_kg_m3/volumic_elasticity_module_N_m2 + volumic_mass_kg_m3*pipe_diameter/(pipe_elasticity*pipe_thickness)))::numeric, 2)
from ___.fluid_properties;
$$
;


------------------------------------------------------------------------------------------------
-- NODES                                                                                      --
------------------------------------------------------------------------------------------------

select template.inherited_view('user', 'node',
    additional_columns => '{s.name as _sector, coalesce(a.zground, 9999) - coalesce(ST_Z(a.geom), 0) as depth}',
    additional_join => 'left join ___.water_delivery_sector as s on ST_Contains(s.geom, a.geom)',
    specific_geom => 'ST_Force2D(a.geom)::geometry(POINT,'||(select srid from ___.metadata)||')',
    specific_columns => '{"domestic_curve": "coalesce(c.domestic_curve, ''Constant'')",
                          "industrial_curve": "coalesce(c.industrial_curve, ''Constant'')"}',
    start_section =>
    $$
        if tg_op = 'INSERT' or tg_op = 'UPDATE' then
            if (select count(*) > 1 from ___.water_delivery_sector where ST_Intersects(new.geom, geom))
                then raise exception 'user node % would be in multiple water delivery sectors', new.name;
            elsif (select exists (select 1 from ___.water_delivery_sector where ST_DWithin(new.geom, ST_Boundary(geom), 0.0001)))
                then raise exception 'user node % would be on water delivery sector boundary', new.name;
            end if;
            select ST_SetSRID(ST_MakePoint(ST_X(new.geom), ST_Y(new.geom), coalesce(new.zground, 9999)-coalesce(new.depth, 0)), ST_SRID(new.geom)) into overloaded_geom;
            select nullif(new.domestic_curve, 'Constant') into new.domestic_curve;
            select nullif(new.industrial_curve, 'Constant') into new.industrial_curve;
        end if;
    $$
);

select template.inherited_view('reservoir', 'node',
    additional_columns => '{coalesce(a.zground, 9999) - coalesce(ST_Z(a.geom), 0) as depth}',
    specific_geom => 'ST_Force2D(a.geom)::geometry(POINT,'||(select srid from ___.metadata)||')',
    start_section =>
    $$
        if tg_op = 'INSERT' or tg_op = 'UPDATE' then
            select ST_SetSRID(ST_MakePoint(ST_X(new.geom), ST_Y(new.geom), coalesce(new.zground, 9999)-coalesce(new.depth, 0)), ST_SRID(new.geom)) into overloaded_geom;
        end if;
    $$
);


------------------------------------------------------------------------------------------------
-- SINGULARITIES                                                                              --
------------------------------------------------------------------------------------------------

select template.inherited_view(name::varchar, 'singularity',
    additional_columns => ('{n.model as _model, ST_Force2D(n.geom)::geometry(POINT,'||(select srid from ___.metadata)||') as geom}')::varchar[],
    additional_join => 'join ___.node as n on n.id=a.id',
    start_section =>
    E'if tg_op = ''INSERT'' or tg_op = ''UPDATE'' then\n'||
    E'      select id, model from ___.node where ST_Equals(new.geom, geom) into new.id, new._model;\n'||
    E'      if (select exists(select 1 from ___.singularity as s join ___.node as n on n.id=s.id where s.id!=new.id and s.name=new.name and n.model=new._model))\n'||
    E'          then raise exception ''a singularity named % already exists in model %'', new.name, new._model;\n'||
    E'      end if;\n'||
    case when name='chlorine_injection' then
    E'      select coalesce(new.target_node, new.id, old.target_node) into new.target_node;\n'
    else E'' end||
    E'  end if;\n'
)
from ___.type_singularity
;


------------------------------------------------------------------------------------------------
-- LINKS                                                                                      --
------------------------------------------------------------------------------------------------

select template.inherited_view('pipe'::varchar, 'link',
    additional_columns => '{n.model as _model, m.roughness_mm as _roughness_mm, m.elasticity_N_m2 as _elasticity_N_m2, api.celerity(m.elasticity_N_m2, c.diameter, (c.thickness_mm/1000.)::real) as _celerity}',
    additional_join => 'left join ___.material as m on m.name=c.material join ___.node as n on n.id=a.up',
    --specific_geom => 'api.link_3d_geom(a.geom , a.up, a.down)',
    specific_geom => 'ST_Force2D(a.geom)::geometry(LINESTRING,'||(select srid from ___.metadata)||')',
    start_section =>
    $$
        if tg_op = 'INSERT' or tg_op = 'UPDATE' then
            select ST_Force2D(new.geom) into new.geom;
            -- nullif(A, nullif(A, B)) returns null if A!=B else return A(=B)
            select u.id, d.id, nullif(u.model, nullif(u.model, d.model))
            from ___.node as u, ___.node as d
            where ST_Equals(ST_StartPoint(new.geom), u.geom) and ST_Equals(ST_EndPoint(new.geom), d.geom)
            into new.up, new.down, new._model;
            if new._model is null
                then raise exception 'upstream and downstream nodes are not in the same model';
            elsif (select exists(select 1 from ___.link as l join ___.node as n on n.id=l.up where l.id!=new.id and l.name=new.name and n.model=new._model))
                then raise exception 'a link named % already exists in model %', new.name, new._model;
            end if;
        end if;
    $$
);

select template.basic_view('pipe_link_singularity');

select template.inherited_view(name::varchar, 'link',
    additional_columns => '{n.model as _model}',
    additional_join => 'join ___.node as n on n.id=a.up',
    --specific_geom => 'api.link_3d_geom(a.geom , a.up, a.down)',
    specific_geom => 'ST_Force2D(a.geom)::geometry(LINESTRING,'||(select srid from ___.metadata)||')',
    start_section =>
    E'if tg_op = ''INSERT'' or tg_op = ''UPDATE'' then\n'||
    E'      select ST_Force2D(new.geom) into new.geom;\n'||
    E'      -- nullif(u.model, nullif(u.model, d.model)) returns null if models do not match else return the value if it match\n'||
    E'      select u.id, d.id, nullif(u.model, nullif(u.model, d.model))\n'||
    E'      from ___.node as u, ___.node as d\n'||
    E'      where ST_Equals(ST_StartPoint(new.geom), u.geom) and ST_Equals(ST_EndPoint(new.geom), d.geom)\n'||
    E'      into new.up, new.down, new._model;\n'||
    E'      if new._model is null\n'||
    E'          then raise exception ''upstream and downstream nodes are not in the same model'';\n'||
    E'      elsif (select exists(select 1 from ___.link as l join ___.node as n on n.id=l.up where l.id!=new.id and l.name=new.name and n.model=new._model))\n'||
    E'          then raise exception ''a link named % already exists in model %'', new.name, new._model;\n'||
    E'      end if;\n'||
    case when name='pump' then
    E'      select coalesce(new.target_node, new.down, old.target_node) into new.target_node;\n'
    else E'' end||
    E'  end if;\n'
)
from ___.type_link
where name::varchar != 'pipe'
;


------------------------------------------------------------------------------------------------
-- SCENARIOS                                                                                  --
------------------------------------------------------------------------------------------------

select template.basic_view('scenario',
    with_trigger => True,
    specific_columns => '{"water_delivery_scenario": "coalesce(c.water_delivery_scenario, ''WD_SCN_REF'')"}',
    start_section =>
    $$
        if tg_op = 'INSERT' or tg_op = 'UPDATE' then
            select nullif(new.water_delivery_scenario, 'WD_SCN_REF') into new.water_delivery_scenario;
        end if;
    $$
);

select template.basic_view('groupe');
select template.basic_view('groupe_model');
select template.basic_view('mixed');
select template.basic_view('cascade');

select template.basic_view('regulation_scenario');

select template.basic_view('scenario_link_failure', with_trigger=>True);
select template.basic_view('scenario_pump_failure', with_trigger=>True);

select template.basic_view('scenario_water_origin',
    with_trigger => True,
    additional_columns => '{s.name as name, n.model as model}',
    additional_join => 'join ___.node as n on n.id=c.imposed_piezometry_singularity join ___.imposed_piezometry_singularity as s on s.id=c.imposed_piezometry_singularity',
    start_section =>
    $$
        if tg_op = 'INSERT' or tg_op = 'UPDATE' then
            if new.name is not null and new.model is not null then
                select id from api.imposed_piezometry_singularity where name=new.name and _model=new.model into new.imposed_piezometry_singularity;
            end if;
        end if;
    $$
);


------------------------------------------------------------------------------------------------
-- Sectorization                                                                              --
------------------------------------------------------------------------------------------------

select template.execute_with_srid(
    $$
        create or replace view api.sector as
            with splitting_lines as (
                select id, ST_Force2D(geom) as geom from api.pipe_link where meter and (select pipe_link_meter from ___.sectorization_settings)
                    union all
                select id, ST_Force2D(geom) as geom from api.valve_link where c_opening = 0 and (select valve_link_open from ___.sectorization_settings)
                    union all
                select id, ST_Force2D(geom) as geom from api.pressure_regulator_link where (select pressure_regulator_link from ___.sectorization_settings)
                    union all
                select id, ST_Force2D(geom) as geom from api.flow_regulator_link where (select flow_regulator_link from ___.sectorization_settings)
            ),
            lines as (
                select id, ST_Force2D(geom) as geom from ___.link where id not in (select id from splitting_lines)
            ),
            splitting_points as (
                select ST_Buffer(geom, 0.01) as geom from api.reservoir_node where (select reservoir_node from ___.sectorization_settings)
                           union
                select ST_Buffer(ST_Intersection(l1.geom, l2.geom), 0.01) as geom
                    from api.pipe_link as l1,api.pipe_link as l2
                    where l1.id<l2.id and ST_Crosses(l1.geom, l2.geom)
            ),
            diff as (
                select l.id, coalesce(ST_Difference(l.geom, b.geom), l.geom) as geom
                from lines as l
                left join splitting_points as b on ST_Intersects(l.geom, b.geom)
            ),
            cluster as (
                select id, ST_ClusterDBScan(geom, 0, 1) over () AS cid
                from diff
            ),
            sector as (
                select ST_Union(l.geom) as geom
                from lines as l
                join cluster as c on l.id=c.id
                group by c.cid
            )
            select 0 as id, ST_Union(geom)::geometry('MULTILINESTRING', $SRID) as geom from splitting_lines
                union all
            select row_number() over () AS id, ST_Multi(geom)::geometry('MULTILINESTRING', $SRID) as geom from sector;
    $$,
    (select srid from ___.metadata)
);

comment on view api.sector is 'given a set of properties, certain links and nodes split the network into sectors separated from each other
a sector "0" is created for the links that split so the end result includes all links';


------------------------------------------------------------------------------------------------
-- Export utility                                                                             --
------------------------------------------------------------------------------------------------

create or replace function api._array_to_text(anyarray, integer default 0)
returns text
language sql immutable as
$$
with arrays as (
	select d1, array_agg($1[d1][d2])as a
	from generate_subscripts($1,1) d1, generate_subscripts($1,2) d2
	group by d1
		union all
	select d1, array_fill(0, array[array_length($1, 2)]) as a
	from generate_series(array_length($1, 1) + 1, $2) as d1
	order by d1
)
select string_agg(array_to_string(a, ' '), E'\n') from arrays
$$
;

comment on function api._array_to_text(anyarray, integer) is 'converts an array to text for export:
x1 y1
x2 y2
x3 y3
...';

create or replace function api._export_header(concrete varchar, abstract varchar, model_name varchar)
returns text
language plpgsql stable as
$$
declare
    _code varchar;
    _id varchar;
    _count integer;
    _res varchar;
begin
    execute 'select exportcode, exportid  from ___.type_'||abstract||' where name='''||concrete||'''' into _code, _id;
    execute 'select count(*) from api.'||concrete||'_'||abstract||' where '||case when  abstract='node' then '' else '_' end||'model='''||model_name||'''' into _count;
    return format(
$res$
$%s (%s_%s)
%s %s
$res$,
_code, concrete, abstract,
_id, _count
    );
end;
$$
;

comment on function api._export_header(concrete varchar, abstract varchar, model_name varchar) is 'creates a custom header for hydra export:
$export_code (table_name)
export_id object_count';

create or replace function api._interval_to_hms(_interval interval)
returns text
language sql immutable as
$$
select floor(extract('epoch' from _interval)/3600)||' '||to_char(_interval, 'MI SS')
$$
;

comment on function api._interval_to_hms(_interval interval) is 'write an interval as "HH MM SS". HH (hours) can exceed 24.';

create or replace function api.user_node_industrial_volume(wd_scn_name varchar, user_node_id integer)
returns real
language sql stable as
$$
select coalesce(sum(wdi.volume * wdi.industrial::integer), 0)::real
from api.water_delivery_scenario_industrial_volume as wdi
join api.water_delivery_point as wd on wd.name=wdi.water_delivery_point
where wd._node=user_node_id and wdi.water_delivery_scenario=wd_scn_name
$$
;


------------------------------------------------------------------------------------------------
-- Export functions                                                                           --
------------------------------------------------------------------------------------------------

create or replace function api.model_grouping(scenario_name varchar)
returns table(priority integer, models varchar)
language sql stable as
$$
with model_global as (
    select 1 as priority, name as model
    from api.model
    order by name
    ),
model_cascade as (
    select priority, model
    from (
        select priority, model from api.cascade where scenario=scenario_name
            union all
        select 999 as priority, name as model
        from api.model
        where not exists (select 1 from api.cascade where scenario=scenario_name and name=model)
        ) as patched_cascadey
    ),
model_mixed as (
    select priority, model
    from api.groupe_model as gp
    join api.mixed as m on gp.groupe=m.groupe
    where m.scenario=scenario_name
    )
select priority, string_agg(model, E' ' order by model) as models from model_global join api.scenario on model_connect_settings='Global' where name=scenario_name group by priority
    union all
select priority, model as models from model_cascade join api.scenario on model_connect_settings='Cascade' where name=scenario_name
    union all
select priority, string_agg(model, E' ' order by model) as models from model_mixed join api.scenario on model_connect_settings='Mixed' where name=scenario_name group by priority;
$$
;

create or replace function api.file_scnnom(scenario_name varchar)
returns text
language sql stable as
$$
with scenario_settings as (
    select
        case when comput_mode='Steady state' then 1
             when comput_mode='Gradual transient' then 2
             when comput_mode='Fast transient' then 3
             when comput_mode='Quality only' then 4
        end as computation_mode,
        scenario_ref
    from api.scenario
    where name=scenario_name
    ),
modeles as (
    select string_agg(name, E' ') as list from api.model
    ),
grouping as (
    select string_agg(models, E'\n' order by priority) as list from api.model_grouping(scenario_name)
    )
select format(
$scnnom$
*scenario
%s

*modeles
%s

*computation_mode
%s

*groupage
%s

%s

$scnnom$,
scenario_name,
modeles.list,
scenario_settings.computation_mode,
grouping.list,
coalesce(E'*scnref\n'||scenario_settings.scenario_ref, '')
)
from scenario_settings, modeles, grouping;
$$
;

create or replace function api.file_cmd(scenario_name varchar)
returns text
language sql stable as
$$
with water_origin as (
    select string_agg(format($pol_origin$%s %s$pol_origin$, ip._model, ip.name), E'\n') as pol_origin
    from api.scenario_water_origin as wo
    join api.imposed_piezometry_singularity as ip on wo.imposed_piezometry_singularity= ip.id
    where wo.scenario=scenario_name
    ),
data_dactv as (
    select row_number() over() as n, t.exportid, l.name,
        api._interval_to_hms(f.failure_timestamp) as failure_timestamp, f.failure_curve
    from api.scenario_link_failure as f
    join ___.type_link as t on t.name=f.link_type
    join ___.link as l on l.id=f.link
    where f.scenario = scenario_name
    ),
bloc_dactv as (
    select count(*) as numel, string_agg(format(
        E'%s %s %s\n%s %s',
        n, exportid, name,
        failure_timestamp, failure_curve
        ), E'\n') as text
    from data_dactv
    ),
data_dactp as (
    select row_number() over() as n, exportid, l.name,
        api._interval_to_hms(f.failure_timestamp) as failure_timestamp, lower(replace(f.failure_mode::text, ' ', '_')) as failure_mode,
        case when f.failure_mode='Automatic alpha computation' then '    '
             when f.failure_mode='Alpha(t) curve' then f.failure_curve
             when f.failure_mode='Four quadrants curve' then (select four_quadrant_file from api.scenario where name=scenario_name)
        end as additional_data
    from api.scenario_pump_failure as f
    join ___.type_link as t on t.name=f.link_type
    join ___.link as l on l.id=f.link
    where f.scenario = scenario_name
    ),
bloc_dactp as (
    select count(*) as numel, string_agg(format(
        E'%s %s %s\n%s %s\n%s',
        n, exportid, name,
        failure_timestamp, failure_mode,
        additional_data
        ), E'\n') as text
    from data_dactp
    ),
bloc_regul as (
    select count(*) as n, string_agg(regulation_file, E'\n' order by priority) as list
    from api.regulation_scenario where scenario=scenario_name
    )
select format(
$cmd$
*time
%s%s

%s

%s

%s

%s

%s

%s

%s

%s

%s

$cmd$,
to_char(starting_date, 'YYYY MM DD HH24 MI'),
case when comput_mode!='Steady state' then format(
$time_details$
%s
%s
%s
$time_details$,
    api._interval_to_hms(duration),
    timestep,
    api._interval_to_hms(tini)) else '' end,
case when comput_mode!='Steady state' then format(
$sor$
*sor
%s
%s
%s
$sor$,
    dt_output_hr,
    api._interval_to_hms(tbegin_output_hr),
    api._interval_to_hms(tend_output_hr)) else '' end,
case when flag_save then format(
$save$
*save
%s
$save$,
    api._interval_to_hms(tsave_hr)) else '' end,
case when flag_rstart then format(
$rstart$
*rstart
%s
%s
$rstart$,
    scenario_rstart, api._interval_to_hms(trstart_hr)) else '' end,
case when refined_discretisation then '*refine_dx' else '' end,
case when vaporization then '*vaporization' else '' end,
case when quality_output='Tracer' then format(
$tracer$
*pol_tracer
%s
$tracer$,
    case when chlorine then 'chlorine '||coalesce(chlorine_water_reaction_coef, 0)||' '||coalesce(chlorine_wall_reaction_coef, 0)||E'\n' else '' end ||
    case when trihalomethane then 'trihalomethane '||coalesce(trihalomethane_reaction_parameter, 0)||' '||coalesce(trihalomethane_concentration_limit, 0)||E'\n' else '' end ||
    case when passive_tracer then E'passive_tracer 0 0\n' else '' end ||
    case when travel_time then E'travel_time 0 0\n' else '' end
) when quality_output='Water origin' then format(
$water_origin$
*pol_origin
%s
$water_origin$,
    pol_origin) else '' end,
case when bloc_dactv.numel>0 then format(
$dactv$
*dactv
2 %s
%s
$dactv$,
    bloc_dactv.numel, bloc_dactv.text) else '' end,
case when bloc_dactp.numel>0 then format(
$dactp$
*dactp
3 %s
%s
$dactp$,
    bloc_dactp.numel, bloc_dactp.text) else '' end,
case when bloc_regul.n>0 then format(
$regul$
*f_regul
%s
$regul$,
    bloc_regul.list) else '' end
)
from api.scenario, water_origin, bloc_dactv, bloc_dactp, bloc_regul
where name=scenario_name;
$$
;

create or replace function api.file_qts(water_delivery_scenario_name varchar)
returns text
language sql stable as
$$
with sectors as (
    select string_agg(format($wdsector$%s %s %s %s$wdsector$,
                             sector.name, setting.volume_m3j, setting.leak_efficiency, setting.adjust_coef
                            ), E'\n') as params
    from api.water_delivery_sector as sector
    join api.water_delivery_scenario_sector_setting as setting on sector.name=setting.sector and setting.scenario=water_delivery_scenario_name
)
select format(
$qts$
%s
%s %s
%s
$qts$,
scenario.comment,
(select count(*) from api.water_delivery_sector), case when scenario.period='Weekday' then 1 else 2 end,
sectors.params)
from api.water_delivery_scenario as scenario, sectors
where scenario.name=water_delivery_scenario_name;
$$
;

create function api.file_cts()
returns text
language sql stable as
$$
with hourly_modulation_curves as (
    select name, string_agg(format($curve$%s %s %s$curve$,
                                   u.i, u.weekdays, u.weekends
                                  ), E'\n') as curve
    from api.hourly_modulation_curve, unnest(
                                            array(select generate_series(1,24)),
                                            (hourly_modulation_array::real[])[:][1:1],
                                            (hourly_modulation_array::real[])[:][2:2]
                                        ) as u(i, weekdays, weekends)
    group by name
)
select string_agg(format(
$cts$
%s
%s
$cts$,
name, curve), '')
from hourly_modulation_curves;
$$
;

create or replace function api.file_rac()
returns text
language sql stable as
$$
with header as (
    select E'$'||exportcode||E' (model connection)\n'||(select count(*) from api.model_connection_singularity)||E'\n' as text
    from ___.type_singularity as s
    where s.name='model_connection'
),
bloc as (
    select string_agg(format(
$rac$
%s %s %s %s
%s
$rac$,
    s._model, s.name, s.id, lower(replace(cascade_mode::text, ' ', '_')),
    api._array_to_text(
        case when cascade_mode = 'Imposed piezometry Z(t)' then zt_array::real[]
             when cascade_mode = 'Delivery Q(t)'           then qt_array::real[]
        end , 20)
    ), '') as text
    from api.model_connection_singularity as s
)
select header.text||coalesce(E'\n'||bloc.text, '') from header, bloc;
$$
;

create or replace function api._bloc_user_node(scenario_name varchar, model_name varchar)
returns text
language sql stable as
$$
with sectors as (
    select s.name, n.model, sum(_domestic_volume) as total_volume_dom
    from api.water_delivery_sector as s
    join api.user_node as n on s.name=n._sector
    left join api._user_node_affected_volume as v on v._user_node=n.id
    group by s.name, n.model
),
header as (
    select api._export_header('user', 'node', model_name) as text
),
wd_scn as (
    select water_delivery_scenario as name
    from api.scenario
    where name=scenario_name
),
data as (
    select row_number() over() as nid, n.id, n.name,
    round(ST_X(n.geom)::numeric, 3) as x, round(ST_Y(n.geom)::numeric, 3) as y, n.zground, n.depth,
    n.fire_hydrant::integer, n.q0,
    coalesce(n._sector, 'null') as sector,
    coalesce(n.domestic_curve, 'CONSTANT') as domestic_curve,
    round(coalesce(case when s.total_volume_dom != 0 then coalesce(v._domestic_volume, 0)/s.total_volume_dom else 0 end, 0)::numeric, 3) as contrib_dom,
    coalesce(n.industrial_curve, 'CONSTANT') as industrial_curve,
    api.user_node_industrial_volume(wd_scn.name, n.id) as industrial_volume
    from wd_scn, api.user_node as n
    left join sectors as s on s.name=n._sector and s.model=n.model
    left join api._user_node_affected_volume as v on v._user_node=n.id
    where n.model=model_name
),
bloc as (
    select string_agg(format(
$node$
%s %s %s
%s %s %s %s
%s %s
%s %s %s %s %s
$node$,
    nid, id, name,
    x, y, zground, depth,
    fire_hydrant, q0,
    sector, domestic_curve, contrib_dom, industrial_curve, industrial_volume
    ), '') as text
    from data
)
select header.text||coalesce(bloc.text, '') from header, bloc;
$$
;

create or replace function api._bloc_reservoir_node(model_name varchar)
returns text
language sql stable as
$$
with header as (
    select api._export_header('reservoir', 'node', model_name) as text
),
data as (
    select row_number() over() as nid, n.id, n.name,
    round(ST_X(geom)::numeric, 3) as x, round(ST_Y(geom)::numeric, 3) as y, zground, depth,
    api._array_to_text(sz_array::real[], 10) as sz_array,
    z_ini, z_overflow,
    lower(replace(alim_mode::text, ' ', '_')) as alim_mode,
    lower(replace(return_in_valve_mode::text, ' ', '_')) as return_in_valve_mode,
    lower(replace(return_out_valve_mode::text, ' ', '_')) as return_out_valve_mode,
    lower(replace(mixing_mode::text, ' ', '_')) as mixing_mode
    from api.reservoir_node as n
    where n.model=model_name
),
bloc as (
    select string_agg(format(
$node$
%s %s %s
%s %s %s %s
%s
%s %s %s %s %s %s
$node$,
    nid, id, name,
    x, y, zground, depth,
    sz_array,
    z_ini, z_overflow, alim_mode, return_in_valve_mode, return_out_valve_mode, mixing_mode
    ), '') as text
    from data
)
select header.text||coalesce(bloc.text, '') from header, bloc;
$$
;

create or replace function api._bloc_pipe_link(model_name varchar)
returns text
language sql stable as
$$
with header as (
    select api._export_header('pipe', 'link', model_name) as text
),
data as (
    select row_number() over() as nid, l.up as nodam, l.down as nodav, l.name,
    diameter,
    case when overload_roughness_mm then overloaded_roughness_mm else _roughness_mm end as roughness_mm,
    case when overload_celerity then overloaded_celerity else _celerity end as celerity,
    status_open::integer,
    case when overload_length then overloaded_length else ST_Length(l.geom) end as length
    from api.pipe_link as l
    where l._model=model_name
),
bloc as (
    select string_agg(format(
$link$
%s %s %s %s
%s %s %s %s %s %s
$link$,
    nid, nodam, nodav, name,
    diameter, roughness_mm, coalesce(celerity::varchar, 'NULL'), status_open, length, 0
    ), '') as text
    from data
)
select header.text||coalesce(bloc.text, '') from header, bloc;
$$
;

create or replace function api._bloc_valve_link(model_name varchar)
returns text
language sql stable as
$$
with header as (
    select api._export_header('valve', 'link', model_name) as text
),
data as (
    select row_number() over() as nid, l.up as nodam, l.down as nodav, l.name,
    section, headloss_coef, c_opening
    from api.valve_link as l
    where l._model=model_name
),
bloc as (
    select string_agg(format(
$link$
%s %s %s %s
%s %s %s
$link$,
    nid, nodam, nodav, name,
    section, headloss_coef, c_opening
    ), '') as text
    from data
)
select header.text||coalesce(bloc.text, '') from header, bloc;
$$
;

create or replace function api._bloc_flow_regulator_link(model_name varchar)
returns text
language sql stable as
$$
with header as (
    select api._export_header('flow_regulator', 'link', model_name) as text
),
data as (
    select row_number() over() as nid, l.up as nodam, l.down as nodav, l.name,
    section, headloss_coef, regul_q
    from api.flow_regulator_link as l
    where l._model=model_name
),
bloc as (
    select string_agg(format(
$link$
%s %s %s %s
%s %s %s
$link$,
    nid, nodam, nodav, name,
    section, headloss_coef, regul_q
    ), '') as text
    from data
)
select header.text||coalesce(bloc.text, '') from header, bloc;
$$
;

create or replace function api._bloc_pressure_regulator_link(model_name varchar)
returns text
language sql stable as
$$
with header as (
    select api._export_header('pressure_regulator', 'link', model_name) as text
),
data as (
    select row_number() over() as nid, l.up as nodam, l.down as nodav, l.name,
    section, headloss_coef,
    lower(replace(pressure_regulation_mode::text, ' ', '_')) as pressure_regulation_mode,
    lower(replace(pressure_regulation_option::text, ' ', '_')) as pressure_regulation_option,
    regul_p,
    api._array_to_text(regul_pq_array::real[], 10) as regul_pq_array
    from api.pressure_regulator_link as l
    where l._model=model_name
),
bloc as (
    select string_agg(format(
$link$
%s %s %s %s
%s %s %s %s
%s
%s
$link$,
    nid, nodam, nodav, name,
    section, headloss_coef, pressure_regulation_mode, pressure_regulation_option,
    regul_p,
    regul_pq_array
    ), '') as text
    from data
)
select header.text||coalesce(bloc.text, '') from header, bloc;
$$
;

create or replace function api._bloc_check_valve_link(model_name varchar)
returns text
language sql stable as
$$
with header as (
    select api._export_header('check_valve', 'link', model_name) as text
),
data as (
    select row_number() over() as nid, l.up as nodam, l.down as nodav, l.name,
    section, headloss_coef, opening_pressure, lower(replace(closing_mode::text, ' ', '_')) as closing_mode, cd_coef
    from api.check_valve_link as l
    where l._model=model_name
),
bloc as (
    select string_agg(format(
$link$
%s %s %s %s
%s %s %s %s %s
$link$,
    nid, nodam, nodav, name,
    section, headloss_coef, opening_pressure, closing_mode, cd_coef
    ), '') as text
    from data
)
select header.text||coalesce(bloc.text, '') from header, bloc;
$$
;

create or replace function api._bloc_pump_link(model_name varchar)
returns text
language sql stable as
$$
with header as (
    select api._export_header('pump', 'link', model_name)as text
),
data as (
    select row_number() over() as nid, l.up as nodam, l.down as nodav, l.name,
    api._array_to_text(pq_array::real[], 10) as pq_array,
    speed_reduction_coef, check_valve, rotation_speed, inertia, lower(replace(pump_regulation_option::text, ' ', '_')) as pump_regulation_option,
    case when pump_regulation_option='No regulation' then speed_reduction_coef::varchar
         when pump_regulation_option='Flow regulation' then regul_q::varchar
         when pump_regulation_option='Stop-start regulation' then target_node||' '||regul_z_stop::varchar||' '||regul_z_start::varchar
         when pump_regulation_option='Pressure regulation' then l.down||' '||regul_p::varchar
         end as param
    from api.pump_link as l
    where l._model=model_name
),
bloc as (
    select string_agg(format(
$link$
%s %s %s %s
%s
%s %s %s %s %s
%s
$link$,
    nid, nodam, nodav, name,
    pq_array,
    speed_reduction_coef, check_valve, rotation_speed, inertia, pump_regulation_option,
    param
    ), '') as text
    from data
)
select header.text||coalesce(bloc.text, '') from header, bloc;
$$
;

create or replace function api._bloc_headloss_link(model_name varchar)
returns text
language sql stable as
$$
with header as (
    select api._export_header('headloss', 'link', model_name) as text
),
data as (
    select row_number() over() as nid, l.up as nodam, l.down as nodav, l.name,
    section, rk1, rk2
    from api.headloss_link as l
    where l._model=model_name
),
bloc as (
    select string_agg(format(
$link$
%s %s %s %s
%s %s %s
$link$,
    nid, nodam, nodav, name,
    section, rk1, rk2
    ), '') as text
    from data
)
select header.text||coalesce(bloc.text, '') from header, bloc;
$$
;

create or replace function api._bloc_imposed_piezometry_singularity(model_name varchar)
returns text
language sql stable as
$$
with header as (
    select api._export_header('imposed_piezometry', 'singularity', model_name) as text
),
data as (
    select row_number() over() as nid, s.id, s.name,
    z0, chlorine, trihalomethane, passive_tracer
    from api.imposed_piezometry_singularity as s
    where s._model=model_name
),
bloc as (
    select string_agg(format(
$singularity$
%s %s %s
%s %s %s %s
$singularity$,
    nid, id, name,
    z0, chlorine, trihalomethane, passive_tracer
    ), '') as text
    from data
)
select header.text||coalesce(bloc.text, '') from header, bloc;
$$
;

create or replace function api._bloc_flow_injection_singularity(model_name varchar)
returns text
language sql stable as
$$
with header as (
    select api._export_header('flow_injection', 'singularity', model_name) as text
),
data as (
    select row_number() over() as nid, s.id, s.name,
    q0, external_file, chlorine, trihalomethane, passive_tracer
    from api.flow_injection_singularity as s
    where s._model=model_name
),
bloc as (
    select string_agg(format(
$singularity$
%s %s %s
%s %s %s %s %s
$singularity$,
    nid, id, name,
    q0, external_file, chlorine, trihalomethane, passive_tracer
    ), '') as text
    from data
)
select header.text||coalesce(bloc.text, '') from header, bloc;
$$
;

create or replace function api._bloc_chlorine_injection_singularity(model_name varchar)
returns text
language sql stable as
$$
with header as (
    select api._export_header('chlorine_injection', 'singularity', model_name) as text
),
data as (
    select row_number() over() as nid, s.id, s.name,
    target_node, target_concentration
    from api.chlorine_injection_singularity as s
    where s._model=model_name
),
bloc as (
    select string_agg(format(
$singularity$
%s %s %s
%s %s
$singularity$,
    nid, id, name,
    target_node, target_concentration
    ), '') as text
    from data
)
select header.text||coalesce(bloc.text, '') from header, bloc;
$$
;

create or replace function api._bloc_surge_tank_singularity(model_name varchar)
returns text
language sql stable as
$$
with header as (
    select api._export_header('surge_tank', 'singularity', model_name) as text
),
data as (
    select row_number() over() as nid, s.id, s.name,
    api._array_to_text(sz_array::real[], 10) as sz_array,
    zini, replace(pressurized_option::text, ' ', '_') as pressurized_option, orifice_diameter, in_headloss_coef, out_headloss_coef
    from api.surge_tank_singularity as s
    where s._model=model_name
),
bloc as (
    select string_agg(format(
$singularity$
%s %s %s
%s
%s %s %s %s %s
$singularity$,
    nid, id, name,
    sz_array,
    zini, pressurized_option, orifice_diameter, in_headloss_coef, out_headloss_coef
    ), '') as text
    from data
)
select header.text||coalesce(bloc.text, '') from header, bloc;
$$
;

create or replace function api._bloc_pressure_accumulator_singularity(model_name varchar)
returns text
language sql stable as
$$
with header as (
    select api._export_header('pressure_accumulator', 'singularity', model_name) as text
),
data as (
    select row_number() over() as nid, s.id, s.name,
    v0, z_connection, filling_pressure
    from api.pressure_accumulator_singularity as s
    where s._model=model_name
),
bloc as (
    select string_agg(format(
$singularity$
%s %s %s
%s %s %s
$singularity$,
    nid, id, name,
    v0, z_connection, filling_pressure
    ), '') as text
    from data
)
select header.text||coalesce(bloc.text, '') from header, bloc;
$$
;

create or replace function api._bloc_air_relief_valve_singularity(model_name varchar)
returns text
language sql stable as
$$
with header as (
    select api._export_header('air_relief_valve', 'singularity', model_name) as text
),
data as (
    select row_number() over() as nid, s.id, s.name,
    orifice_diameter, contraction_coef
    from api.air_relief_valve_singularity as s
    where s._model=model_name
),
bloc as (
    select string_agg(format(
$singularity$
%s %s %s
%s %s
$singularity$,
    nid, id, name,
    orifice_diameter, contraction_coef
    ), '') as text
    from data
)
select header.text||coalesce(bloc.text, '') from header, bloc;
$$
;

create or replace function api.file_dat(scenario_name varchar, model_name varchar)
returns text
language sql stable as
$$
select concat(api._bloc_user_node(scenario_name, model_name), api._bloc_reservoir_node(model_name),
api._bloc_pipe_link(model_name), api._bloc_valve_link(model_name), api._bloc_flow_regulator_link(model_name), api._bloc_pressure_regulator_link(model_name),
api._bloc_check_valve_link(model_name), api._bloc_pump_link(model_name), api._bloc_headloss_link(model_name), api._bloc_imposed_piezometry_singularity(model_name),
api._bloc_flow_injection_singularity(model_name), api._bloc_chlorine_injection_singularity(model_name), api._bloc_surge_tank_singularity(model_name),
api._bloc_pressure_accumulator_singularity(model_name), api._bloc_air_relief_valve_singularity(model_name));
$$
;

create or replace function api.file_phys_dat()
returns text
language sql stable as
$$
select format(
$phys$
*EAU (caracteristiques du fluide)
%s %s %s %s

*HCRIT (valeur des alarmes)
%s
%s %s
%s %s
$phys$,
volumic_mass_kg_m3, temperature_c, kinematic_viscosity_m2_s, volumic_elasticity_module_N_m2,
minimum_service_pressure,
pipe_maximum_speed, pipe_minimum_speed,
node_maximum_pressure, node_minimum_pressure
)
from api.fluid_properties, api.threshold_warning_settings;
$$
;

create function api.file_act_dat()
returns text
language sql stable as
$$
select string_agg(format(
$alp$
%s
%s
$alp$,
name, api._array_to_text(alpt_array::real[])), '' order by name)
from api.failure_curve;
$$
;

create or replace function api.file_optsor_dat()
returns text
language sql stable as
$$
with bloc as (
    select string_agg(format(
$ind$%s %s$ind$,
    n.model, n.id--, n.industrial_volume + n.domestic_volume
    ), E'\n') as text
    from api.user_node as n
    --where n.industrial_volume > 0
)
select E'*CONSO_IND\n'||coalesce(bloc.text, '') from bloc;
$$
;

select 'ok'::varchar;

$_$;


ALTER FUNCTION public.create_api() OWNER TO hydra;

--
-- Name: create_template(); Type: FUNCTION; Schema: public; Owner: hydra
--

CREATE FUNCTION public.create_template() RETURNS character varying
    LANGUAGE sql
    AS $_$

drop schema if exists template cascade;

create schema template;

comment on schema template is 'temporary schema for template generating functions. dropped at the end of this script';

------------------------------------------------------------------------------------------------
-- Dynamic queries for inherited objects view / functions and triggers                        --
------------------------------------------------------------------------------------------------

create function template.view_statement(
    concrete varchar,
    abstract varchar default null,
    additional_columns varchar[] default '{}'::varchar[],
    additional_join varchar default null,
    additional_union varchar default null,
    specific_geom varchar default null,
    specific_columns json default '{}'::json
    )
returns varchar
language sql stable as
$$
    with pk as (
        select string_agg('a.'||kcu.column_name||'=c.'||kcu.column_name, ' and ') as whr,
               string_agg('c.'||kcu.column_name, '||''_''||') as _id
        from information_schema.table_constraints tco
        join information_schema.key_column_usage kcu
            on kcu.constraint_name = tco.constraint_name
            and kcu.constraint_schema = tco.constraint_schema
            and kcu.constraint_name = tco.constraint_name
        where tco.constraint_type = 'PRIMARY KEY'
        and kcu.constraint_schema = '___' and kcu.table_name = concrete||coalesce('_'||abstract, '')
        and not kcu.column_name ~ '^_.*'
    ),
    concrete_cols as (
        select column_name, data_type
        from information_schema.columns
        where table_schema = '___'
        and table_name = concrete||coalesce('_'||abstract, '')
        and not column_name ~ '^_.*'
    ),
    abstract_cols as (
        select column_name
        from information_schema.columns
        where table_schema = '___'
        and table_name = abstract
        and not column_name ~ '^_.*'
        except
        select column_name from concrete_cols
    )
    select 'create view api.'||concrete||coalesce('_'||abstract, '')||' as '||
        'select '||array_to_string(array_agg(column_name)||additional_columns::text[], ', ')||' '||
        'from ___.'||concrete||coalesce('_'||abstract, '')||' as c '||
        coalesce('join ___.'||abstract||' as a on '||whr, '')||
        coalesce(' '||additional_join, '')||
        coalesce(' '||additional_union, '')
    from (
        select case
            when data_type = 'ARRAY' then ('c.'||column_name||'::text as '||column_name)
            when column_name in (select key from json_each_text(specific_columns)) then (specific_columns::json->>column_name||' as '||column_name)
            else 'c.'||column_name end
        as column_name from concrete_cols
        union all
        select case
            when column_name = 'geom' then coalesce(specific_geom||' as geom', 'a.geom')
            else 'a.'||column_name end as column_name from abstract_cols ) t
    cross join pk
    group by whr, _id
    ;
$$
;

create function template.view_default_statement(concrete varchar, abstract varchar default null)
returns setof varchar
language sql stable as
$$
    select 'alter view api.'||concrete||coalesce('_'||abstract, '')||' alter column '||column_name||' set default '||column_default
    from information_schema.columns
    where table_schema = '___'
    and table_name in (abstract, concrete||coalesce('_'||abstract, ''))
    and not column_name ~ '^_.*'
    and column_default is not null
    ;
$$
;

create function template.insert_statement(tablename varchar, concrete varchar default null)
returns varchar
language sql stable as
$$
    select 'insert into ___.'||tablename||'('||string_agg(column_name, ', ')||') '||
           'values ('||string_agg(
                case
                    when column_name = '_type' then ''''||concrete||''''
                    when data_type = 'ARRAY' then 'new.'||column_name||'::real[]'
                    when column_name = 'geom' then 'coalesce(overloaded_geom, new.'||column_name||')'
                else 'new.'||column_name end,
                ', ')||') '
    from information_schema.columns
    where table_schema = '___'
    and table_name = tablename
    ;
$$
;

create function template.update_statement(tablename varchar)
returns varchar
language sql stable as
$$
    with pk as (
        select string_agg(kcu.column_name||'=old.'||kcu.column_name, ' and ') as whr
        from information_schema.table_constraints tco
        join information_schema.key_column_usage kcu
            on kcu.constraint_name = tco.constraint_name
            and kcu.constraint_schema = tco.constraint_schema
            and kcu.constraint_name = tco.constraint_name
        where tco.constraint_type = 'PRIMARY KEY'
        and kcu.constraint_schema = '___' and kcu.table_name = tablename
        and not kcu.column_name  ~ '^_.*'
    )
    select 'update ___.'||tablename||' '||
           'set '||string_agg(column_name||'='||
                case
                    when data_type = 'ARRAY' then 'new.'||column_name||'::real[]'
                    when column_name = 'geom' then 'coalesce(overloaded_geom, new.'||column_name||')'
                else 'new.'||column_name end,
                ', ')||' '||
           'where '||whr
    from information_schema.columns
    cross join pk
    where table_schema = '___'
    and table_name = tablename
    and not column_name ~ '^_.*'
    group by whr
    ;
$$
;

create function template.delete_statement(tablename varchar)
returns varchar
language sql stable as
$$
    select 'delete from ___.'||tablename||' where '||string_agg(kcu.column_name||'=old.'||kcu.column_name, ' and ') as whr
    from information_schema.table_constraints tco
    join information_schema.key_column_usage kcu
        on kcu.constraint_name = tco.constraint_name
        and kcu.constraint_schema = tco.constraint_schema
        and kcu.constraint_name = tco.constraint_name
    where tco.constraint_type = 'PRIMARY KEY'
    and kcu.constraint_schema = '___'
    and kcu.table_name = tablename
    and not column_name ~ '^_.*'
    ;
$$
;

create function template.basic_function_statement(
    table_name varchar,
    cannot_delete boolean default False,
    start_section varchar default null
    )
returns varchar
language plpgsql stable as
$$
begin
    return 'create function api.'||table_name||E'_instead_fct()\n'||
    E'returns trigger \n'||
    E'language plpgsql volatile as\n'||
    E'$fct$\n'||
    E'declare\n'||
    E'    overloaded_geom geometry; -- used in start_section to modify geom type\n'||
    E'begin\n'||
    coalesce(start_section, '')||
    E'    if tg_op = ''INSERT'' then\n'||
    E'        '||template.insert_statement(table_name)||E';\n'||
    E'        return new;\n'||
    E'    elsif tg_op = ''UPDATE'' then\n'||
    E'        '||template.update_statement(table_name)||E';\n'||
    E'        return new;\n'||
    E'    elsif tg_op = ''DELETE'' then\n'||
    case when cannot_delete then
    E'        raise notice ''cannot delete from table '||table_name||E''';\n'
    else
    E'        '||template.delete_statement(table_name)||E';\n'
    end||
    E'        return old;\n'||
    E'    end if;\n'||
    E'end;\n'||
    E'$fct$\n';
end;
$$
;

create function template.inherited_function_statement(
    concrete varchar,
    abstract varchar,
    start_section varchar default null
    )
returns varchar
language plpgsql stable as
$$
begin
    return 'create function api.'||concrete||'_'||abstract||E'_instead_fct()\n'||
    E'returns trigger \n'||
    E'language plpgsql volatile as\n'||
    E'$fct$\n'||
    E'declare\n'||
    E'    overloaded_geom geometry; -- used in start_section to modify geom type\n'||
    E'begin\n'||
    coalesce(start_section, '')||
    E'    if tg_op = ''INSERT'' then\n'||
    E'        '||template.insert_statement(abstract, concrete)||E';\n'||
    E'        '||template.insert_statement(concrete||'_'||abstract, concrete)||E';\n'||
    E'        return new;\n'||
    E'    elsif tg_op = ''UPDATE'' then\n'||
    E'        '||template.update_statement(abstract)||E';\n'||
    E'        '||template.update_statement(concrete||'_'||abstract)||E';\n'||
    E'        return new;\n'||
    E'    elsif tg_op = ''DELETE'' then\n'||
    E'        '||template.delete_statement(abstract)||E';\n'||
    E'        '||template.delete_statement(concrete||'_'||abstract)||E';\n'||
    E'        return old;\n'||
    E'    end if;\n'||
    E'end;\n'||
    E'$fct$\n';
end;
$$
;

create function template.trigger_statement(concrete varchar, abstract varchar default null)
returns varchar
language sql stable as
$$
    select
    E'create trigger '||concrete||coalesce('_'||abstract, '')||E'_instead_trig\n'||
    E'instead of insert or update or delete on api.'||concrete||coalesce('_'||abstract, '')||E'\n'||
    E'for each row execute procedure api.'||concrete||coalesce('_'||abstract, '')||E'_instead_fct()\n';
$$
;

create function template.basic_view(
    table_name varchar,
    additional_columns varchar[] default '{}'::varchar[],
    additional_join varchar default null,
    additional_union varchar default null,
    with_trigger boolean default False,
    cannot_delete boolean default False,
    specific_geom varchar default null,
    specific_columns json default '{}'::json,
    start_section varchar default ''
    )
returns varchar
language plpgsql volatile as
$$
declare
    s varchar;
begin
    --raise notice '%', template.view_statement(table_name, additional_columns => additional_columns, additional_join => additional_join, additional_union => additional_union, specific_geom => specific_geom, specific_columns => specific_columns);
    execute template.view_statement(table_name, additional_columns => additional_columns, additional_join => additional_join, additional_union => additional_union, specific_geom => specific_geom, specific_columns => specific_columns);
    for s in select * from template.view_default_statement(table_name) loop
        --raise notice '%', s;
        execute s;
    end loop;
    if with_trigger then
        --raise notice '%', template.basic_function_statement(table_name, cannot_delete=>cannot_delete);
        execute template.basic_function_statement(table_name, cannot_delete=>cannot_delete, start_section=>start_section);
        --raise notice '%', template.trigger_statement(table_name);
        execute template.trigger_statement(table_name);
    end if;

    return 'CREATE BASIC VIEW api.'||table_name;
end;
$$
;

create function template.inherited_view(
    concrete varchar,
    abstract varchar,
    additional_columns varchar[] default '{}'::varchar[],
    additional_join varchar default null,
    specific_geom varchar default null,
    specific_columns json default '{}'::json,
    start_section varchar default ''
    )
returns varchar
language plpgsql volatile as
$$
declare
    s varchar;
begin
    --raise notice '%', template.view_statement(concrete, abstract, additional_columns, additional_join, specific_geom, specific_columns);
    execute template.view_statement(concrete, abstract, additional_columns => additional_columns, additional_join => additional_join, specific_geom => specific_geom, specific_columns => specific_columns);
    for s in select * from template.view_default_statement(concrete, abstract) loop
        --raise notice '%', s;
        execute s;
    end loop;
    --raise notice '%', template.inherited_function_statement(concrete, abstract);
    execute template.inherited_function_statement(concrete, abstract, start_section=>start_section);
    --raise notice '%', template.trigger_statement(concrete, abstract);
    execute template.trigger_statement(concrete, abstract);

    return 'CREATE INHERITED VIEW api.'||concrete||'_'||abstract;
end;
$$
;

create function template.execute_with_srid(query varchar, srid integer)
returns varchar
language plpgsql volatile as
$$
begin
    --raise notice '%', replace(query, '$SRID', srid::varchar);
    execute replace(query, '$SRID', srid::varchar);
    return replace(query, '$SRID', srid::varchar);
end;
$$
;

select 'ok'::varchar;
$_$;


ALTER FUNCTION public.create_template() OWNER TO hydra;

--
-- Name: expresseau_version(); Type: FUNCTION; Schema: public; Owner: hydra
--

CREATE FUNCTION public.expresseau_version() RETURNS character varying
    LANGUAGE sql IMMUTABLE
    AS $$
    select '0.1'::varchar;
$$;


ALTER FUNCTION public.expresseau_version() OWNER TO hydra;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: air_relief_valve_singularity; Type: TABLE; Schema: ___; Owner: hydra
--

CREATE TABLE ___.air_relief_valve_singularity (
    id integer NOT NULL,
    name character varying DEFAULT ___.unique_name('air_relief_valve'::character varying, 'singularity'::character varying),
    _type ___.type_singularity_ DEFAULT 'air_relief_valve'::___.type_singularity_,
    orifice_diameter real NOT NULL,
    contraction_coef real DEFAULT 0.6 NOT NULL,
    CONSTRAINT air_relief_valve_singularity__type_check CHECK ((_type = 'air_relief_valve'::___.type_singularity_)),
    CONSTRAINT air_relief_valve_singularity_contraction_coef_check CHECK (((contraction_coef >= (0)::double precision) AND (contraction_coef <= (1)::double precision))),
    CONSTRAINT air_relief_valve_singularity_orifice_diameter_check CHECK ((orifice_diameter >= (0)::double precision))
);


ALTER TABLE ___.air_relief_valve_singularity OWNER TO hydra;

--
-- Name: air_relief_valve_singularity_name_seq; Type: SEQUENCE; Schema: ___; Owner: hydra
--

CREATE SEQUENCE ___.air_relief_valve_singularity_name_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ___.air_relief_valve_singularity_name_seq OWNER TO hydra;

--
-- Name: cascade; Type: TABLE; Schema: ___; Owner: hydra
--

CREATE TABLE ___.cascade (
    priority integer NOT NULL,
    scenario character varying NOT NULL,
    model character varying NOT NULL,
    CONSTRAINT cascade_priority_check CHECK ((priority > 0))
);


ALTER TABLE ___.cascade OWNER TO hydra;

--
-- Name: check_valve_link; Type: TABLE; Schema: ___; Owner: hydra
--

CREATE TABLE ___.check_valve_link (
    id integer NOT NULL,
    name character varying DEFAULT ___.unique_name('check_valve'::character varying, 'link'::character varying),
    _type ___.type_link_ DEFAULT 'check_valve'::___.type_link_ NOT NULL,
    section real NOT NULL,
    headloss_coef real DEFAULT 1.5 NOT NULL,
    opening_pressure real DEFAULT 0 NOT NULL,
    closing_mode ___.valve_closing_mode DEFAULT 'Instantaneous'::___.valve_closing_mode NOT NULL,
    cd_coef real DEFAULT 0.3,
    CONSTRAINT check_valve_link__type_check CHECK ((_type = 'check_valve'::___.type_link_)),
    CONSTRAINT check_valve_link_check CHECK (((closing_mode <> 'Instantaneous'::___.valve_closing_mode) OR (cd_coef IS NOT NULL))),
    CONSTRAINT check_valve_link_check1 CHECK (((closing_mode <> 'Instantaneous'::___.valve_closing_mode) OR (cd_coef >= (0)::double precision))),
    CONSTRAINT check_valve_link_headloss_coef_check CHECK ((headloss_coef >= (0)::double precision)),
    CONSTRAINT check_valve_link_opening_pressure_check CHECK ((opening_pressure >= (0)::double precision)),
    CONSTRAINT check_valve_link_section_check CHECK ((section > (0)::double precision))
);


ALTER TABLE ___.check_valve_link OWNER TO hydra;

--
-- Name: check_valve_link_name_seq; Type: SEQUENCE; Schema: ___; Owner: hydra
--

CREATE SEQUENCE ___.check_valve_link_name_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ___.check_valve_link_name_seq OWNER TO hydra;

--
-- Name: chlorine_injection_singularity; Type: TABLE; Schema: ___; Owner: hydra
--

CREATE TABLE ___.chlorine_injection_singularity (
    id integer NOT NULL,
    name character varying DEFAULT ___.unique_name('chlorine_injection'::character varying, 'singularity'::character varying),
    _type ___.type_singularity_ DEFAULT 'chlorine_injection'::___.type_singularity_,
    target_concentration real DEFAULT 0 NOT NULL,
    target_node integer,
    CONSTRAINT chlorine_injection_singularity__type_check CHECK ((_type = 'chlorine_injection'::___.type_singularity_)),
    CONSTRAINT chlorine_injection_singularity_target_concentration_check CHECK ((target_concentration >= (0)::double precision))
);


ALTER TABLE ___.chlorine_injection_singularity OWNER TO hydra;

--
-- Name: chlorine_injection_singularity_name_seq; Type: SEQUENCE; Schema: ___; Owner: hydra
--

CREATE SEQUENCE ___.chlorine_injection_singularity_name_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ___.chlorine_injection_singularity_name_seq OWNER TO hydra;

--
-- Name: failure_curve; Type: TABLE; Schema: ___; Owner: hydra
--

CREATE TABLE ___.failure_curve (
    name character varying(24) DEFAULT ___.unique_name('failure_curve'::character varying, abbreviation => 'ALP'::character varying) NOT NULL,
    comment character varying,
    alpt_array real[] DEFAULT '{{0,0},{10,1}}'::real[],
    CONSTRAINT failure_curve_alpt_array_check CHECK (((array_length(alpt_array, 1) > 1) AND (array_length(alpt_array, 1) <= 10) AND (array_length(alpt_array, 2) = 2)))
);


ALTER TABLE ___.failure_curve OWNER TO hydra;

--
-- Name: failure_curve_name_seq; Type: SEQUENCE; Schema: ___; Owner: hydra
--

CREATE SEQUENCE ___.failure_curve_name_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ___.failure_curve_name_seq OWNER TO hydra;

--
-- Name: flow_injection_singularity; Type: TABLE; Schema: ___; Owner: hydra
--

CREATE TABLE ___.flow_injection_singularity (
    id integer NOT NULL,
    name character varying DEFAULT ___.unique_name('flow_injection'::character varying, 'singularity'::character varying),
    _type ___.type_singularity_ DEFAULT 'flow_injection'::___.type_singularity_,
    q0 real DEFAULT 0 NOT NULL,
    external_file boolean DEFAULT false NOT NULL,
    chlorine real DEFAULT 0 NOT NULL,
    trihalomethane real DEFAULT 0 NOT NULL,
    passive_tracer real DEFAULT 0 NOT NULL,
    CONSTRAINT flow_injection_singularity__type_check CHECK ((_type = 'flow_injection'::___.type_singularity_)),
    CONSTRAINT flow_injection_singularity_chlorine_check CHECK ((chlorine >= (0)::double precision)),
    CONSTRAINT flow_injection_singularity_passive_tracer_check CHECK ((passive_tracer >= (0)::double precision)),
    CONSTRAINT flow_injection_singularity_trihalomethane_check CHECK ((trihalomethane >= (0)::double precision))
);


ALTER TABLE ___.flow_injection_singularity OWNER TO hydra;

--
-- Name: flow_injection_singularity_name_seq; Type: SEQUENCE; Schema: ___; Owner: hydra
--

CREATE SEQUENCE ___.flow_injection_singularity_name_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ___.flow_injection_singularity_name_seq OWNER TO hydra;

--
-- Name: flow_regulator_link; Type: TABLE; Schema: ___; Owner: hydra
--

CREATE TABLE ___.flow_regulator_link (
    id integer NOT NULL,
    name character varying DEFAULT ___.unique_name('flow_regulator'::character varying, 'link'::character varying),
    _type ___.type_link_ DEFAULT 'flow_regulator'::___.type_link_ NOT NULL,
    section real NOT NULL,
    headloss_coef real DEFAULT 1.5 NOT NULL,
    regul_q real DEFAULT 0 NOT NULL,
    CONSTRAINT flow_regulator_link__type_check CHECK ((_type = 'flow_regulator'::___.type_link_)),
    CONSTRAINT flow_regulator_link_headloss_coef_check CHECK ((headloss_coef >= (0)::double precision)),
    CONSTRAINT flow_regulator_link_regul_q_check CHECK ((regul_q >= (0)::double precision)),
    CONSTRAINT flow_regulator_link_section_check CHECK ((section >= (0)::double precision))
);


ALTER TABLE ___.flow_regulator_link OWNER TO hydra;

--
-- Name: flow_regulator_link_name_seq; Type: SEQUENCE; Schema: ___; Owner: hydra
--

CREATE SEQUENCE ___.flow_regulator_link_name_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ___.flow_regulator_link_name_seq OWNER TO hydra;

--
-- Name: fluid_properties; Type: TABLE; Schema: ___; Owner: hydra
--

CREATE TABLE ___.fluid_properties (
    id character varying DEFAULT 'Fluid properties'::character varying NOT NULL,
    volumic_mass_kg_m3 real DEFAULT 1000 NOT NULL,
    temperature_c real DEFAULT 10 NOT NULL,
    kinematic_viscosity_m2_s double precision DEFAULT 0.00000130 NOT NULL,
    volumic_elasticity_module_n_m2 double precision DEFAULT '2050000000'::numeric NOT NULL,
    CONSTRAINT fluid_properties_id_check CHECK (((id)::text = 'Fluid properties'::text))
);


ALTER TABLE ___.fluid_properties OWNER TO hydra;

--
-- Name: groupe; Type: TABLE; Schema: ___; Owner: hydra
--

CREATE TABLE ___.groupe (
    name character varying DEFAULT ___.unique_name('groupe'::character varying, abbreviation => 'GROUP'::character varying) NOT NULL
);


ALTER TABLE ___.groupe OWNER TO hydra;

--
-- Name: groupe_model; Type: TABLE; Schema: ___; Owner: hydra
--

CREATE TABLE ___.groupe_model (
    groupe character varying NOT NULL,
    model character varying NOT NULL
);


ALTER TABLE ___.groupe_model OWNER TO hydra;

--
-- Name: groupe_name_seq; Type: SEQUENCE; Schema: ___; Owner: hydra
--

CREATE SEQUENCE ___.groupe_name_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ___.groupe_name_seq OWNER TO hydra;

--
-- Name: headloss_link; Type: TABLE; Schema: ___; Owner: hydra
--

CREATE TABLE ___.headloss_link (
    id integer NOT NULL,
    name character varying DEFAULT ___.unique_name('headloss'::character varying, 'link'::character varying),
    _type ___.type_link_ DEFAULT 'headloss'::___.type_link_ NOT NULL,
    section real NOT NULL,
    rk1 real DEFAULT 1.5 NOT NULL,
    rk2 real DEFAULT 1.5 NOT NULL,
    CONSTRAINT headloss_link__type_check CHECK ((_type = 'headloss'::___.type_link_)),
    CONSTRAINT headloss_link_rk1_check CHECK ((rk1 >= (0)::double precision)),
    CONSTRAINT headloss_link_rk2_check CHECK ((rk2 >= (0)::double precision)),
    CONSTRAINT headloss_link_section_check CHECK ((section >= (0)::double precision))
);


ALTER TABLE ___.headloss_link OWNER TO hydra;

--
-- Name: headloss_link_name_seq; Type: SEQUENCE; Schema: ___; Owner: hydra
--

CREATE SEQUENCE ___.headloss_link_name_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ___.headloss_link_name_seq OWNER TO hydra;

--
-- Name: hourly_modulation_curve; Type: TABLE; Schema: ___; Owner: hydra
--

CREATE TABLE ___.hourly_modulation_curve (
    name character varying(24) DEFAULT ___.unique_name('hourly_modulation_curve'::character varying, abbreviation => 'CURV'::character varying) NOT NULL,
    comment character varying,
    hourly_modulation_array real[] DEFAULT array_fill(1, ARRAY[24, 2]),
    CONSTRAINT hourly_modulation_curve_hourly_modulation_array_check CHECK (((array_length(hourly_modulation_array, 1) = 24) AND (array_length(hourly_modulation_array, 2) = 2))),
    CONSTRAINT hourly_modulation_curve_hourly_modulation_array_check1 CHECK (((round((___._array_sum(hourly_modulation_array, 1))::numeric, 4))::real = (24)::real)),
    CONSTRAINT hourly_modulation_curve_hourly_modulation_array_check2 CHECK (((round((___._array_sum(hourly_modulation_array, 2))::numeric, 4))::real = (24)::real))
);


ALTER TABLE ___.hourly_modulation_curve OWNER TO hydra;

--
-- Name: hourly_modulation_curve_name_seq; Type: SEQUENCE; Schema: ___; Owner: hydra
--

CREATE SEQUENCE ___.hourly_modulation_curve_name_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ___.hourly_modulation_curve_name_seq OWNER TO hydra;

--
-- Name: imposed_piezometry_singularity; Type: TABLE; Schema: ___; Owner: hydra
--

CREATE TABLE ___.imposed_piezometry_singularity (
    id integer NOT NULL,
    name character varying DEFAULT ___.unique_name('imposed_piezometry'::character varying, 'singularity'::character varying),
    _type ___.type_singularity_ DEFAULT 'imposed_piezometry'::___.type_singularity_,
    z0 real DEFAULT 0 NOT NULL,
    chlorine real DEFAULT 0 NOT NULL,
    trihalomethane real DEFAULT 0 NOT NULL,
    passive_tracer real DEFAULT 0 NOT NULL,
    CONSTRAINT imposed_piezometry_singularity__type_check CHECK ((_type = 'imposed_piezometry'::___.type_singularity_)),
    CONSTRAINT imposed_piezometry_singularity_chlorine_check CHECK ((chlorine >= (0)::double precision)),
    CONSTRAINT imposed_piezometry_singularity_passive_tracer_check CHECK ((passive_tracer >= (0)::double precision)),
    CONSTRAINT imposed_piezometry_singularity_trihalomethane_check CHECK ((trihalomethane >= (0)::double precision))
);


ALTER TABLE ___.imposed_piezometry_singularity OWNER TO hydra;

--
-- Name: imposed_piezometry_singularity_name_seq; Type: SEQUENCE; Schema: ___; Owner: hydra
--

CREATE SEQUENCE ___.imposed_piezometry_singularity_name_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ___.imposed_piezometry_singularity_name_seq OWNER TO hydra;

--
-- Name: link; Type: TABLE; Schema: ___; Owner: hydra
--

CREATE TABLE ___.link (
    id integer NOT NULL,
    name character varying(24) NOT NULL,
    _type ___.type_link_ NOT NULL,
    up integer NOT NULL,
    down integer NOT NULL,
    comment character varying,
    geom public.geometry(LineString,2154) NOT NULL,
    CONSTRAINT link_geom_check CHECK (public.st_isvalid(geom))
);


ALTER TABLE ___.link OWNER TO hydra;

--
-- Name: link_id_seq; Type: SEQUENCE; Schema: ___; Owner: hydra
--

CREATE SEQUENCE ___.link_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ___.link_id_seq OWNER TO hydra;

--
-- Name: link_id_seq; Type: SEQUENCE OWNED BY; Schema: ___; Owner: hydra
--

ALTER SEQUENCE ___.link_id_seq OWNED BY ___.link.id;


--
-- Name: material; Type: TABLE; Schema: ___; Owner: hydra
--

CREATE TABLE ___.material (
    name character varying DEFAULT ___.unique_name('material'::character varying, abbreviation => 'custom_material'::character varying) NOT NULL,
    roughness_mm real DEFAULT 0.500 NOT NULL,
    elasticity_n_m2 double precision DEFAULT '200000000000'::numeric NOT NULL
);


ALTER TABLE ___.material OWNER TO hydra;

--
-- Name: material_name_seq; Type: SEQUENCE; Schema: ___; Owner: hydra
--

CREATE SEQUENCE ___.material_name_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ___.material_name_seq OWNER TO hydra;

--
-- Name: metadata; Type: TABLE; Schema: ___; Owner: hydra
--

CREATE TABLE ___.metadata (
    id integer DEFAULT 1 NOT NULL,
    creation_date timestamp without time zone DEFAULT now() NOT NULL,
    srid integer DEFAULT 2154,
    CONSTRAINT metadata_id_check CHECK ((id = 1))
);


ALTER TABLE ___.metadata OWNER TO hydra;

--
-- Name: mixed; Type: TABLE; Schema: ___; Owner: hydra
--

CREATE TABLE ___.mixed (
    priority integer NOT NULL,
    scenario character varying NOT NULL,
    groupe character varying NOT NULL,
    CONSTRAINT mixed_priority_check CHECK ((priority > 0))
);


ALTER TABLE ___.mixed OWNER TO hydra;

--
-- Name: model; Type: TABLE; Schema: ___; Owner: hydra
--

CREATE TABLE ___.model (
    name character varying DEFAULT ___.unique_name('model'::character varying, abbreviation => 'model'::character varying) NOT NULL,
    creation_date timestamp without time zone DEFAULT CURRENT_DATE NOT NULL,
    comment character varying
);


ALTER TABLE ___.model OWNER TO hydra;

--
-- Name: model_connection_singularity; Type: TABLE; Schema: ___; Owner: hydra
--

CREATE TABLE ___.model_connection_singularity (
    id integer NOT NULL,
    name character varying DEFAULT ___.unique_name('model_connection'::character varying, 'singularity'::character varying),
    _type ___.type_singularity_ DEFAULT 'model_connection'::___.type_singularity_,
    cascade_mode ___.model_connect_mode DEFAULT 'Imposed piezometry Z(t)'::___.model_connect_mode NOT NULL,
    zt_array real[] DEFAULT '{{0,0}}'::real[] NOT NULL,
    qt_array real[] DEFAULT '{{0,0}}'::real[] NOT NULL,
    CONSTRAINT model_connection_singularity__type_check CHECK ((_type = 'model_connection'::___.type_singularity_)),
    CONSTRAINT model_connection_singularity_qt_array_check CHECK (((array_length(qt_array, 1) > 0) AND (array_length(qt_array, 1) <= 20) AND (array_length(qt_array, 2) = 2))),
    CONSTRAINT model_connection_singularity_zt_array_check CHECK (((array_length(zt_array, 1) > 0) AND (array_length(zt_array, 1) <= 20) AND (array_length(zt_array, 2) = 2)))
);


ALTER TABLE ___.model_connection_singularity OWNER TO hydra;

--
-- Name: model_connection_singularity_name_seq; Type: SEQUENCE; Schema: ___; Owner: hydra
--

CREATE SEQUENCE ___.model_connection_singularity_name_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ___.model_connection_singularity_name_seq OWNER TO hydra;

--
-- Name: model_name_seq; Type: SEQUENCE; Schema: ___; Owner: hydra
--

CREATE SEQUENCE ___.model_name_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ___.model_name_seq OWNER TO hydra;

--
-- Name: node; Type: TABLE; Schema: ___; Owner: hydra
--

CREATE TABLE ___.node (
    id integer NOT NULL,
    name character varying(24) NOT NULL,
    model character varying NOT NULL,
    _type ___.type_node_ NOT NULL,
    comment character varying,
    zground real DEFAULT 9999 NOT NULL,
    geom public.geometry(PointZ,2154) NOT NULL,
    CONSTRAINT node_check CHECK (((public.st_z(geom))::real <= zground))
);


ALTER TABLE ___.node OWNER TO hydra;

--
-- Name: node_id_seq; Type: SEQUENCE; Schema: ___; Owner: hydra
--

CREATE SEQUENCE ___.node_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ___.node_id_seq OWNER TO hydra;

--
-- Name: node_id_seq; Type: SEQUENCE OWNED BY; Schema: ___; Owner: hydra
--

ALTER SEQUENCE ___.node_id_seq OWNED BY ___.node.id;


--
-- Name: pipe_link; Type: TABLE; Schema: ___; Owner: hydra
--

CREATE TABLE ___.pipe_link (
    id integer NOT NULL,
    name character varying DEFAULT ___.unique_name('pipe'::character varying, 'link'::character varying),
    _type ___.type_link_ DEFAULT 'pipe'::___.type_link_ NOT NULL,
    diameter real,
    thickness_mm real DEFAULT 8 NOT NULL,
    material character varying DEFAULT 'Unknown'::character varying,
    status_open boolean DEFAULT true NOT NULL,
    meter boolean DEFAULT false NOT NULL,
    overload_length boolean DEFAULT false NOT NULL,
    overloaded_length real,
    overload_celerity boolean DEFAULT false NOT NULL,
    overloaded_celerity real,
    overload_roughness_mm boolean DEFAULT false NOT NULL,
    overloaded_roughness_mm real,
    CONSTRAINT pipe_link__type_check CHECK ((_type = 'pipe'::___.type_link_)),
    CONSTRAINT pipe_link_check CHECK (((NOT overload_length) OR (overloaded_length IS NOT NULL))),
    CONSTRAINT pipe_link_check1 CHECK (((NOT overload_celerity) OR (overloaded_celerity IS NOT NULL))),
    CONSTRAINT pipe_link_check2 CHECK (((NOT overload_roughness_mm) OR (overloaded_roughness_mm IS NOT NULL))),
    CONSTRAINT pipe_link_diameter_check CHECK ((diameter > (0)::double precision)),
    CONSTRAINT pipe_link_overloaded_celerity_check CHECK ((overloaded_celerity > (0)::double precision)),
    CONSTRAINT pipe_link_overloaded_length_check CHECK ((overloaded_length > (0)::double precision)),
    CONSTRAINT pipe_link_overloaded_roughness_mm_check CHECK ((overloaded_roughness_mm >= (0)::double precision)),
    CONSTRAINT pipe_link_thickness_mm_check CHECK ((thickness_mm > (0)::double precision))
);


ALTER TABLE ___.pipe_link OWNER TO hydra;

--
-- Name: pipe_link_name_seq; Type: SEQUENCE; Schema: ___; Owner: hydra
--

CREATE SEQUENCE ___.pipe_link_name_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ___.pipe_link_name_seq OWNER TO hydra;

--
-- Name: pipe_link_singularity; Type: TABLE; Schema: ___; Owner: hydra
--

CREATE TABLE ___.pipe_link_singularity (
    id integer NOT NULL,
    pipe_link integer,
    number integer DEFAULT 1 NOT NULL,
    type_singularity ___.pipe_type_singularity DEFAULT 'Bend'::___.pipe_type_singularity NOT NULL,
    bend_angle real,
    borda_section real,
    borda_coef real,
    CONSTRAINT pipe_link_singularity_bend_angle_check CHECK (((bend_angle > (0)::double precision) AND (bend_angle <= (180)::double precision))),
    CONSTRAINT pipe_link_singularity_borda_coef_check CHECK ((borda_coef > (0)::double precision)),
    CONSTRAINT pipe_link_singularity_borda_section_check CHECK ((borda_section > (0)::double precision)),
    CONSTRAINT pipe_link_singularity_check CHECK (((type_singularity = 'Bend'::___.pipe_type_singularity) OR ((borda_section IS NOT NULL) AND (borda_coef IS NOT NULL)))),
    CONSTRAINT pipe_link_singularity_check1 CHECK (((type_singularity = 'Borda headloss'::___.pipe_type_singularity) OR (bend_angle IS NOT NULL))),
    CONSTRAINT pipe_link_singularity_number_check CHECK ((number > 0))
);


ALTER TABLE ___.pipe_link_singularity OWNER TO hydra;

--
-- Name: pressure_accumulator_singularity; Type: TABLE; Schema: ___; Owner: hydra
--

CREATE TABLE ___.pressure_accumulator_singularity (
    id integer NOT NULL,
    name character varying DEFAULT ___.unique_name('pressure_accumulator'::character varying, 'singularity'::character varying),
    _type ___.type_singularity_ DEFAULT 'pressure_accumulator'::___.type_singularity_,
    v0 real DEFAULT 0 NOT NULL,
    z_connection real DEFAULT 0 NOT NULL,
    filling_pressure real DEFAULT 0 NOT NULL,
    CONSTRAINT pressure_accumulator_singularity__type_check CHECK ((_type = 'pressure_accumulator'::___.type_singularity_)),
    CONSTRAINT pressure_accumulator_singularity_filling_pressure_check CHECK ((filling_pressure >= (0)::double precision)),
    CONSTRAINT pressure_accumulator_singularity_v0_check CHECK ((v0 >= (0)::double precision))
);


ALTER TABLE ___.pressure_accumulator_singularity OWNER TO hydra;

--
-- Name: pressure_accumulator_singularity_name_seq; Type: SEQUENCE; Schema: ___; Owner: hydra
--

CREATE SEQUENCE ___.pressure_accumulator_singularity_name_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ___.pressure_accumulator_singularity_name_seq OWNER TO hydra;

--
-- Name: pressure_regulator_link; Type: TABLE; Schema: ___; Owner: hydra
--

CREATE TABLE ___.pressure_regulator_link (
    id integer NOT NULL,
    name character varying DEFAULT ___.unique_name('pressure_regulator'::character varying, 'link'::character varying),
    _type ___.type_link_ DEFAULT 'pressure_regulator'::___.type_link_ NOT NULL,
    section real,
    headloss_coef real DEFAULT 1.5 NOT NULL,
    pressure_regulation_option ___.pressure_regulation_option DEFAULT 'Constant pressure'::___.pressure_regulation_option NOT NULL,
    regul_p real,
    regul_pq_array real[] DEFAULT '{{0,0}}'::real[] NOT NULL,
    pressure_regulation_mode ___.pressure_regulation_mode DEFAULT 'Imposed differential pressure'::___.pressure_regulation_mode NOT NULL,
    CONSTRAINT pressure_regulator_link__type_check CHECK ((_type = 'pressure_regulator'::___.type_link_)),
    CONSTRAINT pressure_regulator_link_check CHECK (((pressure_regulation_option <> 'Constant pressure'::___.pressure_regulation_option) OR (regul_p IS NOT NULL))),
    CONSTRAINT pressure_regulator_link_check1 CHECK (((pressure_regulation_option <> 'Constant pressure'::___.pressure_regulation_option) OR (regul_p >= (0)::double precision))),
    CONSTRAINT pressure_regulator_link_headloss_coef_check CHECK ((headloss_coef >= (0)::double precision)),
    CONSTRAINT pressure_regulator_link_regul_pq_array_check CHECK (((array_length(regul_pq_array, 1) > 0) AND (array_length(regul_pq_array, 1) <= 10) AND (array_length(regul_pq_array, 2) = 2))),
    CONSTRAINT pressure_regulator_link_section_check CHECK ((section >= (0)::double precision))
);


ALTER TABLE ___.pressure_regulator_link OWNER TO hydra;

--
-- Name: pressure_regulator_link_name_seq; Type: SEQUENCE; Schema: ___; Owner: hydra
--

CREATE SEQUENCE ___.pressure_regulator_link_name_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ___.pressure_regulator_link_name_seq OWNER TO hydra;

--
-- Name: pump_link; Type: TABLE; Schema: ___; Owner: hydra
--

CREATE TABLE ___.pump_link (
    id integer NOT NULL,
    name character varying DEFAULT ___.unique_name('pump'::character varying, 'link'::character varying),
    _type ___.type_link_ DEFAULT 'pump'::___.type_link_ NOT NULL,
    pq_array real[] DEFAULT '{{0,0}}'::real[] NOT NULL,
    check_valve boolean DEFAULT true NOT NULL,
    rotation_speed real NOT NULL,
    inertia real NOT NULL,
    pump_regulation_option ___.pump_regulation_option DEFAULT 'No regulation'::___.pump_regulation_option NOT NULL,
    speed_reduction_coef real DEFAULT 1,
    regul_q real DEFAULT 0,
    target_node integer,
    regul_z_start real DEFAULT 0,
    regul_z_stop real DEFAULT 0,
    regul_p real DEFAULT 0,
    CONSTRAINT pump_link__type_check CHECK ((_type = 'pump'::___.type_link_)),
    CONSTRAINT pump_link_check CHECK (((pump_regulation_option <> 'No regulation'::___.pump_regulation_option) OR (speed_reduction_coef IS NOT NULL))),
    CONSTRAINT pump_link_check1 CHECK (((pump_regulation_option <> 'No regulation'::___.pump_regulation_option) OR ((speed_reduction_coef >= (0)::double precision) AND (speed_reduction_coef <= (1)::double precision)))),
    CONSTRAINT pump_link_check2 CHECK (((pump_regulation_option <> 'Flow regulation'::___.pump_regulation_option) OR (regul_q IS NOT NULL))),
    CONSTRAINT pump_link_check3 CHECK (((pump_regulation_option <> 'Flow regulation'::___.pump_regulation_option) OR (regul_q >= (0)::double precision))),
    CONSTRAINT pump_link_check4 CHECK (((pump_regulation_option <> 'Stop-start regulation'::___.pump_regulation_option) OR (target_node IS NOT NULL))),
    CONSTRAINT pump_link_check5 CHECK (((pump_regulation_option <> 'Stop-start regulation'::___.pump_regulation_option) OR (regul_z_start IS NOT NULL))),
    CONSTRAINT pump_link_check6 CHECK (((pump_regulation_option <> 'Stop-start regulation'::___.pump_regulation_option) OR (regul_z_stop IS NOT NULL))),
    CONSTRAINT pump_link_check7 CHECK (((pump_regulation_option <> 'Pressure regulation'::___.pump_regulation_option) OR (regul_p IS NOT NULL))),
    CONSTRAINT pump_link_check8 CHECK (((pump_regulation_option <> 'Pressure regulation'::___.pump_regulation_option) OR (regul_p >= (0)::double precision))),
    CONSTRAINT pump_link_inertia_check CHECK ((inertia >= (0)::double precision)),
    CONSTRAINT pump_link_pq_array_check CHECK (((array_length(pq_array, 1) > 0) AND (array_length(pq_array, 1) <= 10) AND (array_length(pq_array, 2) = 2))),
    CONSTRAINT pump_link_rotation_speed_check CHECK ((rotation_speed >= (0)::double precision))
);


ALTER TABLE ___.pump_link OWNER TO hydra;

--
-- Name: pump_link_name_seq; Type: SEQUENCE; Schema: ___; Owner: hydra
--

CREATE SEQUENCE ___.pump_link_name_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ___.pump_link_name_seq OWNER TO hydra;

--
-- Name: regulation_scenario; Type: TABLE; Schema: ___; Owner: hydra
--

CREATE TABLE ___.regulation_scenario (
    priority integer NOT NULL,
    regulation_file character varying NOT NULL,
    scenario character varying NOT NULL,
    CONSTRAINT regulation_scenario_priority_check CHECK ((priority > 0))
);


ALTER TABLE ___.regulation_scenario OWNER TO hydra;

--
-- Name: reservoir_node; Type: TABLE; Schema: ___; Owner: hydra
--

CREATE TABLE ___.reservoir_node (
    id integer NOT NULL,
    name character varying DEFAULT ___.unique_name('reservoir'::character varying, 'node'::character varying),
    _type ___.type_node_ DEFAULT 'reservoir'::___.type_node_ NOT NULL,
    sz_array real[] DEFAULT '{{0,0}}'::real[] NOT NULL,
    alim_mode ___.alim_mode DEFAULT 'From bottom'::___.alim_mode NOT NULL,
    z_ini real NOT NULL,
    z_overflow real NOT NULL,
    return_in_valve_mode ___.return_valve_mode DEFAULT 'Regulated'::___.return_valve_mode NOT NULL,
    return_out_valve_mode ___.return_valve_mode DEFAULT 'Regulated'::___.return_valve_mode NOT NULL,
    mixing_mode ___.mixing_mode DEFAULT 'Perfect mixing'::___.mixing_mode NOT NULL,
    CONSTRAINT reservoir_node__type_check CHECK ((_type = 'reservoir'::___.type_node_)),
    CONSTRAINT reservoir_node_check CHECK ((z_ini >= sz_array[1][1])),
    CONSTRAINT reservoir_node_sz_array_check CHECK (((array_length(sz_array, 1) > 0) AND (array_length(sz_array, 1) <= 10) AND (array_length(sz_array, 2) = 2)))
);


ALTER TABLE ___.reservoir_node OWNER TO hydra;

--
-- Name: reservoir_node_name_seq; Type: SEQUENCE; Schema: ___; Owner: hydra
--

CREATE SEQUENCE ___.reservoir_node_name_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ___.reservoir_node_name_seq OWNER TO hydra;

--
-- Name: scenario; Type: TABLE; Schema: ___; Owner: hydra
--

CREATE TABLE ___.scenario (
    name character varying(24) DEFAULT ___.unique_name('scenario'::character varying, abbreviation => 'SCN'::character varying) NOT NULL,
    comment character varying,
    comput_mode ___.computation_mode DEFAULT 'Steady state'::___.computation_mode NOT NULL,
    starting_date timestamp without time zone DEFAULT '2000-01-01 00:00:00'::timestamp without time zone NOT NULL,
    duration interval DEFAULT '12:00:00'::interval,
    timestep real DEFAULT 300 NOT NULL,
    refined_discretisation boolean DEFAULT false,
    water_delivery_scenario character varying,
    peak_coefficient real DEFAULT 1 NOT NULL,
    tini interval DEFAULT '12:00:00'::interval,
    flag_save boolean DEFAULT false NOT NULL,
    tsave_hr interval DEFAULT '00:00:00'::interval,
    flag_rstart boolean DEFAULT false NOT NULL,
    scenario_rstart character varying,
    trstart_hr interval DEFAULT '00:00:01'::interval,
    dt_output_hr real DEFAULT 300 NOT NULL,
    tbegin_output_hr interval DEFAULT '00:00:00'::interval,
    tend_output_hr interval DEFAULT '12:00:00'::interval,
    scenario_ref character varying,
    model_connect_settings ___.model_connection_settings DEFAULT 'Cascade'::___.model_connection_settings NOT NULL,
    option_file character varying,
    four_quadrant_file character varying,
    vaporization boolean DEFAULT false NOT NULL,
    quality_output ___.quality_output DEFAULT 'None'::___.quality_output NOT NULL,
    chlorine boolean DEFAULT false NOT NULL,
    trihalomethane boolean DEFAULT false NOT NULL,
    passive_tracer boolean DEFAULT false NOT NULL,
    travel_time boolean DEFAULT false NOT NULL,
    chlorine_water_reaction_coef real DEFAULT 0.05 NOT NULL,
    chlorine_wall_reaction_coef real DEFAULT 0.0000005 NOT NULL,
    trihalomethane_reaction_parameter real DEFAULT 0.05 NOT NULL,
    trihalomethane_concentration_limit real DEFAULT 0.0000005 NOT NULL,
    CONSTRAINT scenario_peak_coefficient_check CHECK ((peak_coefficient >= (0)::double precision)),
    CONSTRAINT scenario_starting_date_check CHECK ((date_part('seconds'::text, starting_date) = (0)::double precision)),
    CONSTRAINT scenario_timestep_check CHECK ((timestep > (0)::double precision)),
    CONSTRAINT scenario_timestep_check1 CHECK ((timestep > (0)::double precision))
);


ALTER TABLE ___.scenario OWNER TO hydra;

--
-- Name: scenario_link_failure; Type: TABLE; Schema: ___; Owner: hydra
--

CREATE TABLE ___.scenario_link_failure (
    id integer NOT NULL,
    scenario character varying,
    link integer,
    link_type ___.type_link_,
    failure_timestamp interval DEFAULT '00:00:00'::interval,
    failure_curve character varying NOT NULL,
    CONSTRAINT scenario_link_failure_failure_timestamp_check CHECK ((failure_timestamp < '24:00:00'::interval)),
    CONSTRAINT scenario_link_failure_link_type_check CHECK ((link_type = ANY (ARRAY['check_valve'::___.type_link_, 'flow_regulator'::___.type_link_, 'pressure_regulator'::___.type_link_, 'valve'::___.type_link_])))
);


ALTER TABLE ___.scenario_link_failure OWNER TO hydra;

--
-- Name: scenario_link_failure_id_seq; Type: SEQUENCE; Schema: ___; Owner: hydra
--

CREATE SEQUENCE ___.scenario_link_failure_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ___.scenario_link_failure_id_seq OWNER TO hydra;

--
-- Name: scenario_link_failure_id_seq; Type: SEQUENCE OWNED BY; Schema: ___; Owner: hydra
--

ALTER SEQUENCE ___.scenario_link_failure_id_seq OWNED BY ___.scenario_link_failure.id;


--
-- Name: scenario_name_seq; Type: SEQUENCE; Schema: ___; Owner: hydra
--

CREATE SEQUENCE ___.scenario_name_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ___.scenario_name_seq OWNER TO hydra;

--
-- Name: scenario_pump_failure; Type: TABLE; Schema: ___; Owner: hydra
--

CREATE TABLE ___.scenario_pump_failure (
    id integer NOT NULL,
    scenario character varying,
    link integer,
    link_type ___.type_link_ DEFAULT 'pump'::___.type_link_,
    failure_timestamp interval DEFAULT '00:00:00'::interval,
    failure_curve character varying,
    failure_mode ___.pump_failure_mode DEFAULT 'Automatic alpha computation'::___.pump_failure_mode NOT NULL,
    CONSTRAINT scenario_pump_failure_check CHECK (((failure_curve IS NULL) OR (failure_mode = 'Alpha(t) curve'::___.pump_failure_mode))),
    CONSTRAINT scenario_pump_failure_failure_timestamp_check CHECK ((failure_timestamp < '24:00:00'::interval)),
    CONSTRAINT scenario_pump_failure_link_type_check CHECK ((link_type = 'pump'::___.type_link_))
);


ALTER TABLE ___.scenario_pump_failure OWNER TO hydra;

--
-- Name: scenario_pump_failure_id_seq; Type: SEQUENCE; Schema: ___; Owner: hydra
--

CREATE SEQUENCE ___.scenario_pump_failure_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ___.scenario_pump_failure_id_seq OWNER TO hydra;

--
-- Name: scenario_pump_failure_id_seq; Type: SEQUENCE OWNED BY; Schema: ___; Owner: hydra
--

ALTER SEQUENCE ___.scenario_pump_failure_id_seq OWNED BY ___.scenario_pump_failure.id;


--
-- Name: scenario_water_origin; Type: TABLE; Schema: ___; Owner: hydra
--

CREATE TABLE ___.scenario_water_origin (
    id integer NOT NULL,
    scenario character varying,
    imposed_piezometry_singularity integer,
    comment character varying
);


ALTER TABLE ___.scenario_water_origin OWNER TO hydra;

--
-- Name: scenario_water_origin_id_seq; Type: SEQUENCE; Schema: ___; Owner: hydra
--

CREATE SEQUENCE ___.scenario_water_origin_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ___.scenario_water_origin_id_seq OWNER TO hydra;

--
-- Name: scenario_water_origin_id_seq; Type: SEQUENCE OWNED BY; Schema: ___; Owner: hydra
--

ALTER SEQUENCE ___.scenario_water_origin_id_seq OWNED BY ___.scenario_water_origin.id;


--
-- Name: sectorization_settings; Type: TABLE; Schema: ___; Owner: hydra
--

CREATE TABLE ___.sectorization_settings (
    pipe_link_meter boolean DEFAULT true,
    valve_link_open boolean DEFAULT true,
    pressure_regulator_link boolean DEFAULT false,
    flow_regulator_link boolean DEFAULT false,
    reservoir_node boolean DEFAULT false,
    id character varying DEFAULT 'Sectorization'::character varying NOT NULL,
    CONSTRAINT sectorization_settings_id_check CHECK (((id)::text = 'Sectorization'::text))
);


ALTER TABLE ___.sectorization_settings OWNER TO hydra;

--
-- Name: singularity; Type: TABLE; Schema: ___; Owner: hydra
--

CREATE TABLE ___.singularity (
    id integer NOT NULL,
    name character varying(24) NOT NULL,
    _type ___.type_singularity_ NOT NULL,
    comment character varying
);


ALTER TABLE ___.singularity OWNER TO hydra;

--
-- Name: singularity_id_seq; Type: SEQUENCE; Schema: ___; Owner: hydra
--

CREATE SEQUENCE ___.singularity_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ___.singularity_id_seq OWNER TO hydra;

--
-- Name: singularity_id_seq; Type: SEQUENCE OWNED BY; Schema: ___; Owner: hydra
--

ALTER SEQUENCE ___.singularity_id_seq OWNED BY ___.singularity.id;


--
-- Name: surge_tank_singularity; Type: TABLE; Schema: ___; Owner: hydra
--

CREATE TABLE ___.surge_tank_singularity (
    id integer NOT NULL,
    name character varying DEFAULT ___.unique_name('surge_tank'::character varying, 'singularity'::character varying),
    _type ___.type_singularity_ DEFAULT 'surge_tank'::___.type_singularity_,
    sz_array real[] DEFAULT '{{0,0}}'::real[] NOT NULL,
    zini real DEFAULT 0 NOT NULL,
    pressurized_option boolean DEFAULT false NOT NULL,
    orifice_diameter real NOT NULL,
    in_headloss_coef real DEFAULT 1.5 NOT NULL,
    out_headloss_coef real DEFAULT 1.5 NOT NULL,
    CONSTRAINT surge_tank_singularity__type_check CHECK ((_type = 'surge_tank'::___.type_singularity_)),
    CONSTRAINT surge_tank_singularity_check CHECK ((zini >= sz_array[1][1])),
    CONSTRAINT surge_tank_singularity_in_headloss_coef_check CHECK ((in_headloss_coef >= (0)::double precision)),
    CONSTRAINT surge_tank_singularity_orifice_diameter_check CHECK ((orifice_diameter >= (0)::double precision)),
    CONSTRAINT surge_tank_singularity_out_headloss_coef_check CHECK ((out_headloss_coef >= (0)::double precision)),
    CONSTRAINT surge_tank_singularity_sz_array_check CHECK (((array_length(sz_array, 1) > 0) AND (array_length(sz_array, 1) <= 10) AND (array_length(sz_array, 2) = 2)))
);


ALTER TABLE ___.surge_tank_singularity OWNER TO hydra;

--
-- Name: surge_tank_singularity_name_seq; Type: SEQUENCE; Schema: ___; Owner: hydra
--

CREATE SEQUENCE ___.surge_tank_singularity_name_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ___.surge_tank_singularity_name_seq OWNER TO hydra;

--
-- Name: threshold_warning_settings; Type: TABLE; Schema: ___; Owner: hydra
--

CREATE TABLE ___.threshold_warning_settings (
    pipe_maximum_speed real DEFAULT 2 NOT NULL,
    pipe_minimum_speed real DEFAULT 0.3 NOT NULL,
    node_maximum_pressure real DEFAULT 100 NOT NULL,
    node_minimum_pressure real DEFAULT '-50'::integer NOT NULL,
    minimum_service_pressure real DEFAULT 20 NOT NULL,
    id character varying DEFAULT 'Warnings'::character varying NOT NULL,
    CONSTRAINT threshold_warning_settings_id_check CHECK (((id)::text = 'Warnings'::text))
);


ALTER TABLE ___.threshold_warning_settings OWNER TO hydra;

--
-- Name: type_link; Type: TABLE; Schema: ___; Owner: hydra
--

CREATE TABLE ___.type_link (
    name ___.type_link_ NOT NULL,
    exportid integer NOT NULL,
    exportcode character varying NOT NULL,
    abbreviation character varying NOT NULL
);


ALTER TABLE ___.type_link OWNER TO hydra;

--
-- Name: type_node; Type: TABLE; Schema: ___; Owner: hydra
--

CREATE TABLE ___.type_node (
    name ___.type_node_ NOT NULL,
    exportid integer NOT NULL,
    exportcode character varying NOT NULL,
    abbreviation character varying NOT NULL,
    comment character varying
);


ALTER TABLE ___.type_node OWNER TO hydra;

--
-- Name: type_singularity; Type: TABLE; Schema: ___; Owner: hydra
--

CREATE TABLE ___.type_singularity (
    name ___.type_singularity_ NOT NULL,
    exportid integer NOT NULL,
    exportcode character varying NOT NULL,
    abbreviation character varying NOT NULL,
    comment character varying
);


ALTER TABLE ___.type_singularity OWNER TO hydra;

--
-- Name: user_node; Type: TABLE; Schema: ___; Owner: hydra
--

CREATE TABLE ___.user_node (
    id integer NOT NULL,
    name character varying DEFAULT ___.unique_name('user'::character varying, 'node'::character varying),
    _type ___.type_node_ DEFAULT 'user'::___.type_node_,
    domestic_curve character varying,
    industrial_curve character varying,
    q0 real DEFAULT 0 NOT NULL,
    fire_hydrant boolean DEFAULT false NOT NULL,
    CONSTRAINT user_node__type_check CHECK ((_type = 'user'::___.type_node_))
);


ALTER TABLE ___.user_node OWNER TO hydra;

--
-- Name: user_node_name_seq; Type: SEQUENCE; Schema: ___; Owner: hydra
--

CREATE SEQUENCE ___.user_node_name_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ___.user_node_name_seq OWNER TO hydra;

--
-- Name: valve_link; Type: TABLE; Schema: ___; Owner: hydra
--

CREATE TABLE ___.valve_link (
    id integer NOT NULL,
    name character varying DEFAULT ___.unique_name('valve'::character varying, 'link'::character varying),
    _type ___.type_link_ DEFAULT 'valve'::___.type_link_ NOT NULL,
    section real NOT NULL,
    headloss_coef real DEFAULT 1.5 NOT NULL,
    c_opening real DEFAULT 1 NOT NULL,
    CONSTRAINT valve_link__type_check CHECK ((_type = 'valve'::___.type_link_)),
    CONSTRAINT valve_link_c_opening_check CHECK (((c_opening >= (0)::double precision) AND (c_opening <= (1)::double precision))),
    CONSTRAINT valve_link_headloss_coef_check CHECK ((headloss_coef >= (0)::double precision)),
    CONSTRAINT valve_link_section_check CHECK ((section >= (0)::double precision))
);


ALTER TABLE ___.valve_link OWNER TO hydra;

--
-- Name: valve_link_name_seq; Type: SEQUENCE; Schema: ___; Owner: hydra
--

CREATE SEQUENCE ___.valve_link_name_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ___.valve_link_name_seq OWNER TO hydra;

--
-- Name: water_delivery_point; Type: TABLE; Schema: ___; Owner: hydra
--

CREATE TABLE ___.water_delivery_point (
    name character varying DEFAULT ___.unique_name('water_delivery_point'::character varying, abbreviation => 'WD'::character varying) NOT NULL,
    comment character varying,
    geom public.geometry(Point,2154) NOT NULL,
    volume real DEFAULT 0 NOT NULL,
    industrial boolean DEFAULT false NOT NULL,
    pipe_link integer,
    CONSTRAINT water_delivery_point_volume_check CHECK ((volume >= (0)::double precision))
);


ALTER TABLE ___.water_delivery_point OWNER TO hydra;

--
-- Name: water_delivery_point_name_seq; Type: SEQUENCE; Schema: ___; Owner: hydra
--

CREATE SEQUENCE ___.water_delivery_point_name_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ___.water_delivery_point_name_seq OWNER TO hydra;

--
-- Name: water_delivery_scenario; Type: TABLE; Schema: ___; Owner: hydra
--

CREATE TABLE ___.water_delivery_scenario (
    name character varying(24) DEFAULT ___.unique_name('water_delivery_scenario'::character varying, abbreviation => 'WD_SCN'::character varying) NOT NULL,
    comment character varying,
    period ___.week_period DEFAULT 'Weekday'::___.week_period NOT NULL,
    CONSTRAINT water_delivery_scenario_name_check CHECK (((name)::text !~~ 'WD_SCN_REF'::text))
);


ALTER TABLE ___.water_delivery_scenario OWNER TO hydra;

--
-- Name: water_delivery_scenario_industrial_volume; Type: TABLE; Schema: ___; Owner: hydra
--

CREATE TABLE ___.water_delivery_scenario_industrial_volume (
    water_delivery_scenario character varying NOT NULL,
    water_delivery_point character varying NOT NULL,
    volume real DEFAULT 0 NOT NULL,
    industrial boolean,
    CONSTRAINT water_delivery_scenario_industrial_volume_industrial_check CHECK (industrial),
    CONSTRAINT water_delivery_scenario_industrial_volume_volume_check CHECK ((volume >= (0)::double precision))
);


ALTER TABLE ___.water_delivery_scenario_industrial_volume OWNER TO hydra;

--
-- Name: water_delivery_scenario_name_seq; Type: SEQUENCE; Schema: ___; Owner: hydra
--

CREATE SEQUENCE ___.water_delivery_scenario_name_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ___.water_delivery_scenario_name_seq OWNER TO hydra;

--
-- Name: water_delivery_scenario_sector_setting; Type: TABLE; Schema: ___; Owner: hydra
--

CREATE TABLE ___.water_delivery_scenario_sector_setting (
    scenario character varying NOT NULL,
    sector character varying NOT NULL,
    volume_m3j real,
    adjust_coef real DEFAULT 1 NOT NULL,
    leak_efficiency real DEFAULT 1 NOT NULL,
    CONSTRAINT water_delivery_scenario_sector_setting_adjust_coef_check CHECK ((adjust_coef >= (0)::double precision)),
    CONSTRAINT water_delivery_scenario_sector_setting_leak_efficiency_check CHECK (((leak_efficiency > (0)::double precision) AND (leak_efficiency <= (1)::double precision)))
);


ALTER TABLE ___.water_delivery_scenario_sector_setting OWNER TO hydra;

--
-- Name: water_delivery_sector; Type: TABLE; Schema: ___; Owner: hydra
--

CREATE TABLE ___.water_delivery_sector (
    name character varying(24) DEFAULT ___.unique_name('water_delivery_sector'::character varying, abbreviation => 'WD_SECT'::character varying) NOT NULL,
    comment character varying,
    geom public.geometry(MultiPolygon,2154) NOT NULL,
    CONSTRAINT water_delivery_sector_geom_check CHECK (public.st_isvalid(geom))
);


ALTER TABLE ___.water_delivery_sector OWNER TO hydra;

--
-- Name: water_delivery_sector_name_seq; Type: SEQUENCE; Schema: ___; Owner: hydra
--

CREATE SEQUENCE ___.water_delivery_sector_name_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ___.water_delivery_sector_name_seq OWNER TO hydra;

--
-- Name: water_delivery_point; Type: VIEW; Schema: api; Owner: hydra
--

CREATE VIEW api.water_delivery_point AS
 SELECT c.name,
    c.comment,
    c.geom,
    c.volume,
    c.industrial,
    c.pipe_link,
        CASE
            WHEN (public.st_linelocatepoint(l.geom, c.geom) < (0.5)::double precision) THEN l.up
            ELSE l.down
        END AS _node,
    (public.st_force2d(public.st_makeline(ARRAY[c.geom, public.st_closestpoint(l.geom, c.geom),
        CASE
            WHEN (public.st_linelocatepoint(l.geom, c.geom) < (0.5)::double precision) THEN public.st_startpoint(l.geom)
            ELSE public.st_endpoint(l.geom)
        END])))::public.geometry(LineString,2154) AS _affectation_geom
   FROM (___.water_delivery_point c
     LEFT JOIN ___.link l ON ((l.id = c.pipe_link)));


ALTER TABLE api.water_delivery_point OWNER TO hydra;

--
-- Name: _user_node_affected_volume; Type: VIEW; Schema: api; Owner: hydra
--

CREATE VIEW api._user_node_affected_volume AS
 SELECT n.id AS _user_node,
    sum(
        CASE
            WHEN (NOT wd.industrial) THEN wd.volume
            ELSE (0)::real
        END) AS _domestic_volume,
    n.geom,
    array_agg(wd.name) AS _sources
   FROM (___.node n
     JOIN api.water_delivery_point wd ON ((wd._node = n.id)))
  WHERE (n._type = 'user'::___.type_node_)
  GROUP BY n.id, n.geom;


ALTER TABLE api._user_node_affected_volume OWNER TO hydra;

--
-- Name: air_relief_valve_singularity; Type: VIEW; Schema: api; Owner: hydra
--

CREATE VIEW api.air_relief_valve_singularity AS
 SELECT c.id,
    c.name,
    c.orifice_diameter,
    c.contraction_coef,
    a.comment,
    n.model AS _model,
    (public.st_force2d(n.geom))::public.geometry(Point,2154) AS geom
   FROM ((___.air_relief_valve_singularity c
     JOIN ___.singularity a ON ((a.id = c.id)))
     JOIN ___.node n ON ((n.id = a.id)));


ALTER TABLE api.air_relief_valve_singularity OWNER TO hydra;

--
-- Name: cascade; Type: VIEW; Schema: api; Owner: hydra
--

CREATE VIEW api.cascade AS
 SELECT c.priority,
    c.scenario,
    c.model
   FROM ___.cascade c;


ALTER TABLE api.cascade OWNER TO hydra;

--
-- Name: check_valve_link; Type: VIEW; Schema: api; Owner: hydra
--

CREATE VIEW api.check_valve_link AS
 SELECT c.id,
    c.name,
    c.section,
    c.headloss_coef,
    c.opening_pressure,
    c.closing_mode,
    c.cd_coef,
    (public.st_force2d(a.geom))::public.geometry(LineString,2154) AS geom,
    a.comment,
    a.down,
    a.up,
    n.model AS _model
   FROM ((___.check_valve_link c
     JOIN ___.link a ON ((a.id = c.id)))
     JOIN ___.node n ON ((n.id = a.up)));


ALTER TABLE api.check_valve_link OWNER TO hydra;

--
-- Name: chlorine_injection_singularity; Type: VIEW; Schema: api; Owner: hydra
--

CREATE VIEW api.chlorine_injection_singularity AS
 SELECT c.id,
    c.name,
    c.target_concentration,
    c.target_node,
    a.comment,
    n.model AS _model,
    (public.st_force2d(n.geom))::public.geometry(Point,2154) AS geom
   FROM ((___.chlorine_injection_singularity c
     JOIN ___.singularity a ON ((a.id = c.id)))
     JOIN ___.node n ON ((n.id = a.id)));


ALTER TABLE api.chlorine_injection_singularity OWNER TO hydra;

--
-- Name: failure_curve; Type: VIEW; Schema: api; Owner: hydra
--

CREATE VIEW api.failure_curve AS
 SELECT c.name,
    c.comment,
    (c.alpt_array)::text AS alpt_array
   FROM ___.failure_curve c;


ALTER TABLE api.failure_curve OWNER TO hydra;

--
-- Name: flow_injection_singularity; Type: VIEW; Schema: api; Owner: hydra
--

CREATE VIEW api.flow_injection_singularity AS
 SELECT c.id,
    c.name,
    c.q0,
    c.external_file,
    c.chlorine,
    c.trihalomethane,
    c.passive_tracer,
    a.comment,
    n.model AS _model,
    (public.st_force2d(n.geom))::public.geometry(Point,2154) AS geom
   FROM ((___.flow_injection_singularity c
     JOIN ___.singularity a ON ((a.id = c.id)))
     JOIN ___.node n ON ((n.id = a.id)));


ALTER TABLE api.flow_injection_singularity OWNER TO hydra;

--
-- Name: flow_regulator_link; Type: VIEW; Schema: api; Owner: hydra
--

CREATE VIEW api.flow_regulator_link AS
 SELECT c.id,
    c.name,
    c.section,
    c.headloss_coef,
    c.regul_q,
    (public.st_force2d(a.geom))::public.geometry(LineString,2154) AS geom,
    a.comment,
    a.down,
    a.up,
    n.model AS _model
   FROM ((___.flow_regulator_link c
     JOIN ___.link a ON ((a.id = c.id)))
     JOIN ___.node n ON ((n.id = a.up)));


ALTER TABLE api.flow_regulator_link OWNER TO hydra;

--
-- Name: fluid_properties; Type: VIEW; Schema: api; Owner: hydra
--

CREATE VIEW api.fluid_properties AS
 SELECT c.id,
    c.volumic_mass_kg_m3,
    c.temperature_c,
    c.kinematic_viscosity_m2_s,
    c.volumic_elasticity_module_n_m2
   FROM ___.fluid_properties c;


ALTER TABLE api.fluid_properties OWNER TO hydra;

--
-- Name: groupe; Type: VIEW; Schema: api; Owner: hydra
--

CREATE VIEW api.groupe AS
 SELECT c.name
   FROM ___.groupe c;


ALTER TABLE api.groupe OWNER TO hydra;

--
-- Name: groupe_model; Type: VIEW; Schema: api; Owner: hydra
--

CREATE VIEW api.groupe_model AS
 SELECT c.groupe,
    c.model
   FROM ___.groupe_model c;


ALTER TABLE api.groupe_model OWNER TO hydra;

--
-- Name: headloss_link; Type: VIEW; Schema: api; Owner: hydra
--

CREATE VIEW api.headloss_link AS
 SELECT c.id,
    c.name,
    c.section,
    c.rk1,
    c.rk2,
    (public.st_force2d(a.geom))::public.geometry(LineString,2154) AS geom,
    a.comment,
    a.down,
    a.up,
    n.model AS _model
   FROM ((___.headloss_link c
     JOIN ___.link a ON ((a.id = c.id)))
     JOIN ___.node n ON ((n.id = a.up)));


ALTER TABLE api.headloss_link OWNER TO hydra;

--
-- Name: hourly_modulation_curve; Type: VIEW; Schema: api; Owner: hydra
--

CREATE VIEW api.hourly_modulation_curve AS
 SELECT c.name,
    c.comment,
    (c.hourly_modulation_array)::text AS hourly_modulation_array
   FROM ___.hourly_modulation_curve c;


ALTER TABLE api.hourly_modulation_curve OWNER TO hydra;

--
-- Name: imposed_piezometry_singularity; Type: VIEW; Schema: api; Owner: hydra
--

CREATE VIEW api.imposed_piezometry_singularity AS
 SELECT c.id,
    c.name,
    c.z0,
    c.chlorine,
    c.trihalomethane,
    c.passive_tracer,
    a.comment,
    n.model AS _model,
    (public.st_force2d(n.geom))::public.geometry(Point,2154) AS geom
   FROM ((___.imposed_piezometry_singularity c
     JOIN ___.singularity a ON ((a.id = c.id)))
     JOIN ___.node n ON ((n.id = a.id)));


ALTER TABLE api.imposed_piezometry_singularity OWNER TO hydra;

--
-- Name: material; Type: VIEW; Schema: api; Owner: hydra
--

CREATE VIEW api.material AS
 SELECT c.name,
    c.roughness_mm,
    c.elasticity_n_m2
   FROM ___.material c;


ALTER TABLE api.material OWNER TO hydra;

--
-- Name: metadata; Type: VIEW; Schema: api; Owner: hydra
--

CREATE VIEW api.metadata AS
 SELECT c.id,
    c.creation_date,
    c.srid
   FROM ___.metadata c;


ALTER TABLE api.metadata OWNER TO hydra;

--
-- Name: mixed; Type: VIEW; Schema: api; Owner: hydra
--

CREATE VIEW api.mixed AS
 SELECT c.priority,
    c.scenario,
    c.groupe
   FROM ___.mixed c;


ALTER TABLE api.mixed OWNER TO hydra;

--
-- Name: model; Type: VIEW; Schema: api; Owner: hydra
--

CREATE VIEW api.model AS
 SELECT c.name,
    c.creation_date,
    c.comment
   FROM ___.model c;


ALTER TABLE api.model OWNER TO hydra;

--
-- Name: model_connection_singularity; Type: VIEW; Schema: api; Owner: hydra
--

CREATE VIEW api.model_connection_singularity AS
 SELECT c.id,
    c.name,
    c.cascade_mode,
    (c.zt_array)::text AS zt_array,
    (c.qt_array)::text AS qt_array,
    a.comment,
    n.model AS _model,
    (public.st_force2d(n.geom))::public.geometry(Point,2154) AS geom
   FROM ((___.model_connection_singularity c
     JOIN ___.singularity a ON ((a.id = c.id)))
     JOIN ___.node n ON ((n.id = a.id)));


ALTER TABLE api.model_connection_singularity OWNER TO hydra;

--
-- Name: node; Type: VIEW; Schema: api; Owner: hydra
--

CREATE VIEW api.node AS
 SELECT c.id,
    c.name,
    c.model,
    c.comment,
    c.zground,
    c.geom
   FROM ___.node c;


ALTER TABLE api.node OWNER TO hydra;

--
-- Name: pipe_link; Type: VIEW; Schema: api; Owner: hydra
--

CREATE VIEW api.pipe_link AS
 SELECT c.id,
    c.name,
    c.diameter,
    c.thickness_mm,
    c.material,
    c.status_open,
    c.meter,
    c.overload_length,
    c.overloaded_length,
    c.overload_celerity,
    c.overloaded_celerity,
    c.overload_roughness_mm,
    c.overloaded_roughness_mm,
    (public.st_force2d(a.geom))::public.geometry(LineString,2154) AS geom,
    a.comment,
    a.down,
    a.up,
    n.model AS _model,
    m.roughness_mm AS _roughness_mm,
    m.elasticity_n_m2 AS _elasticity_n_m2,
    api.celerity(m.elasticity_n_m2, c.diameter, ((c.thickness_mm / ('1000'::numeric)::double precision))::real) AS _celerity
   FROM (((___.pipe_link c
     JOIN ___.link a ON ((a.id = c.id)))
     LEFT JOIN ___.material m ON (((m.name)::text = (c.material)::text)))
     JOIN ___.node n ON ((n.id = a.up)));


ALTER TABLE api.pipe_link OWNER TO hydra;

--
-- Name: pipe_link_singularity; Type: VIEW; Schema: api; Owner: hydra
--

CREATE VIEW api.pipe_link_singularity AS
 SELECT c.id,
    c.pipe_link,
    c.number,
    c.type_singularity,
    c.bend_angle,
    c.borda_section,
    c.borda_coef
   FROM ___.pipe_link_singularity c;


ALTER TABLE api.pipe_link_singularity OWNER TO hydra;

--
-- Name: pressure_accumulator_singularity; Type: VIEW; Schema: api; Owner: hydra
--

CREATE VIEW api.pressure_accumulator_singularity AS
 SELECT c.id,
    c.name,
    c.v0,
    c.z_connection,
    c.filling_pressure,
    a.comment,
    n.model AS _model,
    (public.st_force2d(n.geom))::public.geometry(Point,2154) AS geom
   FROM ((___.pressure_accumulator_singularity c
     JOIN ___.singularity a ON ((a.id = c.id)))
     JOIN ___.node n ON ((n.id = a.id)));


ALTER TABLE api.pressure_accumulator_singularity OWNER TO hydra;

--
-- Name: pressure_regulator_link; Type: VIEW; Schema: api; Owner: hydra
--

CREATE VIEW api.pressure_regulator_link AS
 SELECT c.id,
    c.name,
    c.section,
    c.headloss_coef,
    c.pressure_regulation_option,
    c.regul_p,
    (c.regul_pq_array)::text AS regul_pq_array,
    c.pressure_regulation_mode,
    (public.st_force2d(a.geom))::public.geometry(LineString,2154) AS geom,
    a.comment,
    a.down,
    a.up,
    n.model AS _model
   FROM ((___.pressure_regulator_link c
     JOIN ___.link a ON ((a.id = c.id)))
     JOIN ___.node n ON ((n.id = a.up)));


ALTER TABLE api.pressure_regulator_link OWNER TO hydra;

--
-- Name: pump_link; Type: VIEW; Schema: api; Owner: hydra
--

CREATE VIEW api.pump_link AS
 SELECT c.id,
    c.name,
    (c.pq_array)::text AS pq_array,
    c.check_valve,
    c.rotation_speed,
    c.inertia,
    c.pump_regulation_option,
    c.speed_reduction_coef,
    c.regul_q,
    c.target_node,
    c.regul_z_start,
    c.regul_z_stop,
    c.regul_p,
    (public.st_force2d(a.geom))::public.geometry(LineString,2154) AS geom,
    a.comment,
    a.down,
    a.up,
    n.model AS _model
   FROM ((___.pump_link c
     JOIN ___.link a ON ((a.id = c.id)))
     JOIN ___.node n ON ((n.id = a.up)));


ALTER TABLE api.pump_link OWNER TO hydra;

--
-- Name: regulation_scenario; Type: VIEW; Schema: api; Owner: hydra
--

CREATE VIEW api.regulation_scenario AS
 SELECT c.priority,
    c.regulation_file,
    c.scenario
   FROM ___.regulation_scenario c;


ALTER TABLE api.regulation_scenario OWNER TO hydra;

--
-- Name: reservoir_node; Type: VIEW; Schema: api; Owner: hydra
--

CREATE VIEW api.reservoir_node AS
 SELECT c.id,
    c.name,
    (c.sz_array)::text AS sz_array,
    c.alim_mode,
    c.z_ini,
    c.z_overflow,
    c.return_in_valve_mode,
    c.return_out_valve_mode,
    c.mixing_mode,
    (public.st_force2d(a.geom))::public.geometry(Point,2154) AS geom,
    a.model,
    a.zground,
    a.comment,
    (COALESCE(a.zground, (9999)::real) - COALESCE(public.st_z(a.geom), (0)::double precision)) AS depth
   FROM (___.reservoir_node c
     JOIN ___.node a ON ((a.id = c.id)));


ALTER TABLE api.reservoir_node OWNER TO hydra;

--
-- Name: scenario; Type: VIEW; Schema: api; Owner: hydra
--

CREATE VIEW api.scenario AS
 SELECT c.name,
    c.comment,
    c.comput_mode,
    c.starting_date,
    c.duration,
    c.timestep,
    c.refined_discretisation,
    COALESCE(c.water_delivery_scenario, 'WD_SCN_REF'::character varying) AS water_delivery_scenario,
    c.peak_coefficient,
    c.tini,
    c.flag_save,
    c.tsave_hr,
    c.flag_rstart,
    c.scenario_rstart,
    c.trstart_hr,
    c.dt_output_hr,
    c.tbegin_output_hr,
    c.tend_output_hr,
    c.scenario_ref,
    c.model_connect_settings,
    c.option_file,
    c.four_quadrant_file,
    c.vaporization,
    c.quality_output,
    c.chlorine,
    c.trihalomethane,
    c.passive_tracer,
    c.travel_time,
    c.chlorine_water_reaction_coef,
    c.chlorine_wall_reaction_coef,
    c.trihalomethane_reaction_parameter,
    c.trihalomethane_concentration_limit
   FROM ___.scenario c;


ALTER TABLE api.scenario OWNER TO hydra;

--
-- Name: scenario_link_failure; Type: VIEW; Schema: api; Owner: hydra
--

CREATE VIEW api.scenario_link_failure AS
 SELECT c.id,
    c.scenario,
    c.link,
    c.link_type,
    c.failure_timestamp,
    c.failure_curve
   FROM ___.scenario_link_failure c;


ALTER TABLE api.scenario_link_failure OWNER TO hydra;

--
-- Name: scenario_pump_failure; Type: VIEW; Schema: api; Owner: hydra
--

CREATE VIEW api.scenario_pump_failure AS
 SELECT c.id,
    c.scenario,
    c.link,
    c.link_type,
    c.failure_timestamp,
    c.failure_curve,
    c.failure_mode
   FROM ___.scenario_pump_failure c;


ALTER TABLE api.scenario_pump_failure OWNER TO hydra;

--
-- Name: scenario_water_origin; Type: VIEW; Schema: api; Owner: hydra
--

CREATE VIEW api.scenario_water_origin AS
 SELECT c.id,
    c.scenario,
    c.imposed_piezometry_singularity,
    c.comment,
    s.name,
    n.model
   FROM ((___.scenario_water_origin c
     JOIN ___.node n ON ((n.id = c.imposed_piezometry_singularity)))
     JOIN ___.imposed_piezometry_singularity s ON ((s.id = c.imposed_piezometry_singularity)));


ALTER TABLE api.scenario_water_origin OWNER TO hydra;

--
-- Name: valve_link; Type: VIEW; Schema: api; Owner: hydra
--

CREATE VIEW api.valve_link AS
 SELECT c.id,
    c.name,
    c.section,
    c.headloss_coef,
    c.c_opening,
    (public.st_force2d(a.geom))::public.geometry(LineString,2154) AS geom,
    a.comment,
    a.down,
    a.up,
    n.model AS _model
   FROM ((___.valve_link c
     JOIN ___.link a ON ((a.id = c.id)))
     JOIN ___.node n ON ((n.id = a.up)));


ALTER TABLE api.valve_link OWNER TO hydra;

--
-- Name: sector; Type: VIEW; Schema: api; Owner: hydra
--

CREATE VIEW api.sector AS
 WITH splitting_lines AS (
         SELECT pipe_link.id,
            public.st_force2d(pipe_link.geom) AS geom
           FROM api.pipe_link
          WHERE (pipe_link.meter AND ( SELECT sectorization_settings.pipe_link_meter
                   FROM ___.sectorization_settings))
        UNION ALL
         SELECT valve_link.id,
            public.st_force2d(valve_link.geom) AS geom
           FROM api.valve_link
          WHERE ((valve_link.c_opening = (0)::double precision) AND ( SELECT sectorization_settings.valve_link_open
                   FROM ___.sectorization_settings))
        UNION ALL
         SELECT pressure_regulator_link.id,
            public.st_force2d(pressure_regulator_link.geom) AS geom
           FROM api.pressure_regulator_link
          WHERE ( SELECT sectorization_settings.pressure_regulator_link
                   FROM ___.sectorization_settings)
        UNION ALL
         SELECT flow_regulator_link.id,
            public.st_force2d(flow_regulator_link.geom) AS geom
           FROM api.flow_regulator_link
          WHERE ( SELECT sectorization_settings.flow_regulator_link
                   FROM ___.sectorization_settings)
        ), lines AS (
         SELECT link.id,
            public.st_force2d(link.geom) AS geom
           FROM ___.link
          WHERE (NOT (link.id IN ( SELECT splitting_lines.id
                   FROM splitting_lines)))
        ), splitting_points AS (
         SELECT public.st_buffer(reservoir_node.geom, (0.01)::double precision) AS geom
           FROM api.reservoir_node
          WHERE ( SELECT sectorization_settings.reservoir_node
                   FROM ___.sectorization_settings)
        UNION
         SELECT public.st_buffer(public.st_intersection(l1.geom, l2.geom), (0.01)::double precision) AS geom
           FROM api.pipe_link l1,
            api.pipe_link l2
          WHERE ((l1.id < l2.id) AND public.st_crosses(l1.geom, l2.geom))
        ), diff AS (
         SELECT l.id,
            COALESCE(public.st_difference(l.geom, b.geom), l.geom) AS geom
           FROM (lines l
             LEFT JOIN splitting_points b ON (public.st_intersects(l.geom, b.geom)))
        ), cluster AS (
         SELECT diff.id,
            public.st_clusterdbscan(diff.geom, (0)::double precision, 1) OVER () AS cid
           FROM diff
        ), sector AS (
         SELECT public.st_union(l.geom) AS geom
           FROM (lines l
             JOIN cluster c ON ((l.id = c.id)))
          GROUP BY c.cid
        )
 SELECT 0 AS id,
    (public.st_union(splitting_lines.geom))::public.geometry(MultiLineString,2154) AS geom
   FROM splitting_lines
UNION ALL
 SELECT row_number() OVER () AS id,
    (public.st_multi(sector.geom))::public.geometry(MultiLineString,2154) AS geom
   FROM sector;


ALTER TABLE api.sector OWNER TO hydra;

--
-- Name: VIEW sector; Type: COMMENT; Schema: api; Owner: hydra
--

COMMENT ON VIEW api.sector IS 'given a set of properties, certain links and nodes split the network into sectors separated from each other
a sector "0" is created for the links that split so the end result includes all links';


--
-- Name: sectorization_settings; Type: VIEW; Schema: api; Owner: hydra
--

CREATE VIEW api.sectorization_settings AS
 SELECT c.pipe_link_meter,
    c.valve_link_open,
    c.pressure_regulator_link,
    c.flow_regulator_link,
    c.reservoir_node,
    c.id
   FROM ___.sectorization_settings c;


ALTER TABLE api.sectorization_settings OWNER TO hydra;

--
-- Name: surge_tank_singularity; Type: VIEW; Schema: api; Owner: hydra
--

CREATE VIEW api.surge_tank_singularity AS
 SELECT c.id,
    c.name,
    (c.sz_array)::text AS sz_array,
    c.zini,
    c.pressurized_option,
    c.orifice_diameter,
    c.in_headloss_coef,
    c.out_headloss_coef,
    a.comment,
    n.model AS _model,
    (public.st_force2d(n.geom))::public.geometry(Point,2154) AS geom
   FROM ((___.surge_tank_singularity c
     JOIN ___.singularity a ON ((a.id = c.id)))
     JOIN ___.node n ON ((n.id = a.id)));


ALTER TABLE api.surge_tank_singularity OWNER TO hydra;

--
-- Name: threshold_warning_settings; Type: VIEW; Schema: api; Owner: hydra
--

CREATE VIEW api.threshold_warning_settings AS
 SELECT c.pipe_maximum_speed,
    c.pipe_minimum_speed,
    c.node_maximum_pressure,
    c.node_minimum_pressure,
    c.minimum_service_pressure,
    c.id
   FROM ___.threshold_warning_settings c;


ALTER TABLE api.threshold_warning_settings OWNER TO hydra;

--
-- Name: user_node; Type: VIEW; Schema: api; Owner: hydra
--

CREATE VIEW api.user_node AS
 SELECT c.id,
    c.name,
    COALESCE(c.domestic_curve, 'Constant'::character varying) AS domestic_curve,
    COALESCE(c.industrial_curve, 'Constant'::character varying) AS industrial_curve,
    c.q0,
    c.fire_hydrant,
    (public.st_force2d(a.geom))::public.geometry(Point,2154) AS geom,
    a.model,
    a.zground,
    a.comment,
    s.name AS _sector,
    (COALESCE(a.zground, (9999)::real) - COALESCE(public.st_z(a.geom), (0)::double precision)) AS depth
   FROM ((___.user_node c
     JOIN ___.node a ON ((a.id = c.id)))
     LEFT JOIN ___.water_delivery_sector s ON (public.st_contains(s.geom, a.geom)));


ALTER TABLE api.user_node OWNER TO hydra;

--
-- Name: water_delivery_scenario; Type: VIEW; Schema: api; Owner: hydra
--

CREATE VIEW api.water_delivery_scenario AS
 SELECT c.name,
    c.comment,
    c.period
   FROM ___.water_delivery_scenario c
UNION ALL
 SELECT 'WD_SCN_REF'::character varying AS name,
    'Reference scenario - not editable'::character varying AS comment,
    'Weekday'::___.week_period AS period;


ALTER TABLE api.water_delivery_scenario OWNER TO hydra;

--
-- Name: water_delivery_scenario_industrial_volume; Type: VIEW; Schema: api; Owner: hydra
--

CREATE VIEW api.water_delivery_scenario_industrial_volume AS
 SELECT c.water_delivery_scenario,
    c.water_delivery_point,
    c.volume,
    c.industrial
   FROM ___.water_delivery_scenario_industrial_volume c
UNION ALL
 SELECT s.name AS water_delivery_scenario,
    p.name AS water_delivery_point,
    p.volume,
    p.industrial
   FROM ___.water_delivery_scenario s,
    ___.water_delivery_point p
  WHERE (p.industrial AND (NOT (EXISTS ( SELECT 1
           FROM ___.water_delivery_scenario_industrial_volume
          WHERE (((water_delivery_scenario_industrial_volume.water_delivery_scenario)::text = (s.name)::text) AND ((water_delivery_scenario_industrial_volume.water_delivery_point)::text = (p.name)::text))))))
UNION ALL
 SELECT 'WD_SCN_REF'::character varying AS water_delivery_scenario,
    p.name AS water_delivery_point,
    p.volume,
    p.industrial
   FROM ___.water_delivery_point p
  WHERE p.industrial;


ALTER TABLE api.water_delivery_scenario_industrial_volume OWNER TO hydra;

--
-- Name: water_delivery_scenario_sector_setting; Type: VIEW; Schema: api; Owner: hydra
--

CREATE VIEW api.water_delivery_scenario_sector_setting AS
 SELECT c.scenario,
    c.sector,
    c.volume_m3j,
    c.adjust_coef,
    c.leak_efficiency
   FROM ___.water_delivery_scenario_sector_setting c
UNION ALL
 SELECT 'WD_SCN_REF'::character varying AS scenario,
    s.name AS sector,
    COALESCE(sum(n._domestic_volume), (0)::real) AS volume_m3j,
    1 AS adjust_coef,
    0.8 AS leak_efficiency
   FROM (___.water_delivery_sector s
     LEFT JOIN api._user_node_affected_volume n ON (public.st_contains(s.geom, n.geom)))
  GROUP BY s.name;


ALTER TABLE api.water_delivery_scenario_sector_setting OWNER TO hydra;

--
-- Name: water_delivery_sector; Type: VIEW; Schema: api; Owner: hydra
--

CREATE VIEW api.water_delivery_sector AS
 SELECT c.name,
    c.comment,
    c.geom
   FROM ___.water_delivery_sector c;


ALTER TABLE api.water_delivery_sector OWNER TO hydra;

--
-- Name: link id; Type: DEFAULT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.link ALTER COLUMN id SET DEFAULT nextval('___.link_id_seq'::regclass);


--
-- Name: node id; Type: DEFAULT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.node ALTER COLUMN id SET DEFAULT nextval('___.node_id_seq'::regclass);


--
-- Name: scenario_link_failure id; Type: DEFAULT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.scenario_link_failure ALTER COLUMN id SET DEFAULT nextval('___.scenario_link_failure_id_seq'::regclass);


--
-- Name: scenario_pump_failure id; Type: DEFAULT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.scenario_pump_failure ALTER COLUMN id SET DEFAULT nextval('___.scenario_pump_failure_id_seq'::regclass);


--
-- Name: scenario_water_origin id; Type: DEFAULT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.scenario_water_origin ALTER COLUMN id SET DEFAULT nextval('___.scenario_water_origin_id_seq'::regclass);


--
-- Name: singularity id; Type: DEFAULT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.singularity ALTER COLUMN id SET DEFAULT nextval('___.singularity_id_seq'::regclass);


--
-- Name: air_relief_valve_singularity id; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.air_relief_valve_singularity ALTER COLUMN id SET DEFAULT nextval('___.singularity_id_seq'::regclass);


--
-- Name: air_relief_valve_singularity name; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.air_relief_valve_singularity ALTER COLUMN name SET DEFAULT ___.unique_name('air_relief_valve'::character varying, 'singularity'::character varying);


--
-- Name: air_relief_valve_singularity contraction_coef; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.air_relief_valve_singularity ALTER COLUMN contraction_coef SET DEFAULT 0.6;


--
-- Name: check_valve_link id; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.check_valve_link ALTER COLUMN id SET DEFAULT nextval('___.link_id_seq'::regclass);


--
-- Name: check_valve_link name; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.check_valve_link ALTER COLUMN name SET DEFAULT ___.unique_name('check_valve'::character varying, 'link'::character varying);


--
-- Name: check_valve_link headloss_coef; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.check_valve_link ALTER COLUMN headloss_coef SET DEFAULT 1.5;


--
-- Name: check_valve_link opening_pressure; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.check_valve_link ALTER COLUMN opening_pressure SET DEFAULT 0;


--
-- Name: check_valve_link closing_mode; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.check_valve_link ALTER COLUMN closing_mode SET DEFAULT 'Instantaneous'::___.valve_closing_mode;


--
-- Name: check_valve_link cd_coef; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.check_valve_link ALTER COLUMN cd_coef SET DEFAULT 0.3;


--
-- Name: chlorine_injection_singularity id; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.chlorine_injection_singularity ALTER COLUMN id SET DEFAULT nextval('___.singularity_id_seq'::regclass);


--
-- Name: chlorine_injection_singularity name; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.chlorine_injection_singularity ALTER COLUMN name SET DEFAULT ___.unique_name('chlorine_injection'::character varying, 'singularity'::character varying);


--
-- Name: chlorine_injection_singularity target_concentration; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.chlorine_injection_singularity ALTER COLUMN target_concentration SET DEFAULT 0;


--
-- Name: failure_curve name; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.failure_curve ALTER COLUMN name SET DEFAULT ___.unique_name('failure_curve'::character varying, abbreviation => 'ALP'::character varying);


--
-- Name: failure_curve alpt_array; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.failure_curve ALTER COLUMN alpt_array SET DEFAULT '{{0,0},{10,1}}'::real[];


--
-- Name: flow_injection_singularity id; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.flow_injection_singularity ALTER COLUMN id SET DEFAULT nextval('___.singularity_id_seq'::regclass);


--
-- Name: flow_injection_singularity name; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.flow_injection_singularity ALTER COLUMN name SET DEFAULT ___.unique_name('flow_injection'::character varying, 'singularity'::character varying);


--
-- Name: flow_injection_singularity q0; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.flow_injection_singularity ALTER COLUMN q0 SET DEFAULT 0;


--
-- Name: flow_injection_singularity external_file; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.flow_injection_singularity ALTER COLUMN external_file SET DEFAULT false;


--
-- Name: flow_injection_singularity chlorine; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.flow_injection_singularity ALTER COLUMN chlorine SET DEFAULT 0;


--
-- Name: flow_injection_singularity trihalomethane; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.flow_injection_singularity ALTER COLUMN trihalomethane SET DEFAULT 0;


--
-- Name: flow_injection_singularity passive_tracer; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.flow_injection_singularity ALTER COLUMN passive_tracer SET DEFAULT 0;


--
-- Name: flow_regulator_link id; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.flow_regulator_link ALTER COLUMN id SET DEFAULT nextval('___.link_id_seq'::regclass);


--
-- Name: flow_regulator_link name; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.flow_regulator_link ALTER COLUMN name SET DEFAULT ___.unique_name('flow_regulator'::character varying, 'link'::character varying);


--
-- Name: flow_regulator_link headloss_coef; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.flow_regulator_link ALTER COLUMN headloss_coef SET DEFAULT 1.5;


--
-- Name: flow_regulator_link regul_q; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.flow_regulator_link ALTER COLUMN regul_q SET DEFAULT 0;


--
-- Name: fluid_properties id; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.fluid_properties ALTER COLUMN id SET DEFAULT 'Fluid properties'::character varying;


--
-- Name: fluid_properties volumic_mass_kg_m3; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.fluid_properties ALTER COLUMN volumic_mass_kg_m3 SET DEFAULT 1000;


--
-- Name: fluid_properties temperature_c; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.fluid_properties ALTER COLUMN temperature_c SET DEFAULT 10;


--
-- Name: fluid_properties kinematic_viscosity_m2_s; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.fluid_properties ALTER COLUMN kinematic_viscosity_m2_s SET DEFAULT 0.00000130;


--
-- Name: fluid_properties volumic_elasticity_module_n_m2; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.fluid_properties ALTER COLUMN volumic_elasticity_module_n_m2 SET DEFAULT '2050000000'::numeric;


--
-- Name: groupe name; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.groupe ALTER COLUMN name SET DEFAULT ___.unique_name('groupe'::character varying, abbreviation => 'GROUP'::character varying);


--
-- Name: headloss_link id; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.headloss_link ALTER COLUMN id SET DEFAULT nextval('___.link_id_seq'::regclass);


--
-- Name: headloss_link name; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.headloss_link ALTER COLUMN name SET DEFAULT ___.unique_name('headloss'::character varying, 'link'::character varying);


--
-- Name: headloss_link rk1; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.headloss_link ALTER COLUMN rk1 SET DEFAULT 1.5;


--
-- Name: headloss_link rk2; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.headloss_link ALTER COLUMN rk2 SET DEFAULT 1.5;


--
-- Name: hourly_modulation_curve name; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.hourly_modulation_curve ALTER COLUMN name SET DEFAULT ___.unique_name('hourly_modulation_curve'::character varying, abbreviation => 'CURV'::character varying);


--
-- Name: hourly_modulation_curve hourly_modulation_array; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.hourly_modulation_curve ALTER COLUMN hourly_modulation_array SET DEFAULT array_fill(1, ARRAY[24, 2]);


--
-- Name: imposed_piezometry_singularity id; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.imposed_piezometry_singularity ALTER COLUMN id SET DEFAULT nextval('___.singularity_id_seq'::regclass);


--
-- Name: imposed_piezometry_singularity name; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.imposed_piezometry_singularity ALTER COLUMN name SET DEFAULT ___.unique_name('imposed_piezometry'::character varying, 'singularity'::character varying);


--
-- Name: imposed_piezometry_singularity z0; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.imposed_piezometry_singularity ALTER COLUMN z0 SET DEFAULT 0;


--
-- Name: imposed_piezometry_singularity chlorine; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.imposed_piezometry_singularity ALTER COLUMN chlorine SET DEFAULT 0;


--
-- Name: imposed_piezometry_singularity trihalomethane; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.imposed_piezometry_singularity ALTER COLUMN trihalomethane SET DEFAULT 0;


--
-- Name: imposed_piezometry_singularity passive_tracer; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.imposed_piezometry_singularity ALTER COLUMN passive_tracer SET DEFAULT 0;


--
-- Name: material name; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.material ALTER COLUMN name SET DEFAULT ___.unique_name('material'::character varying, abbreviation => 'custom_material'::character varying);


--
-- Name: material roughness_mm; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.material ALTER COLUMN roughness_mm SET DEFAULT 0.500;


--
-- Name: material elasticity_n_m2; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.material ALTER COLUMN elasticity_n_m2 SET DEFAULT '200000000000'::numeric;


--
-- Name: metadata id; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.metadata ALTER COLUMN id SET DEFAULT 1;


--
-- Name: metadata creation_date; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.metadata ALTER COLUMN creation_date SET DEFAULT now();


--
-- Name: metadata srid; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.metadata ALTER COLUMN srid SET DEFAULT 2154;


--
-- Name: model name; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.model ALTER COLUMN name SET DEFAULT ___.unique_name('model'::character varying, abbreviation => 'model'::character varying);


--
-- Name: model creation_date; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.model ALTER COLUMN creation_date SET DEFAULT CURRENT_DATE;


--
-- Name: model_connection_singularity id; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.model_connection_singularity ALTER COLUMN id SET DEFAULT nextval('___.singularity_id_seq'::regclass);


--
-- Name: model_connection_singularity name; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.model_connection_singularity ALTER COLUMN name SET DEFAULT ___.unique_name('model_connection'::character varying, 'singularity'::character varying);


--
-- Name: model_connection_singularity cascade_mode; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.model_connection_singularity ALTER COLUMN cascade_mode SET DEFAULT 'Imposed piezometry Z(t)'::___.model_connect_mode;


--
-- Name: model_connection_singularity zt_array; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.model_connection_singularity ALTER COLUMN zt_array SET DEFAULT '{{0,0}}'::real[];


--
-- Name: model_connection_singularity qt_array; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.model_connection_singularity ALTER COLUMN qt_array SET DEFAULT '{{0,0}}'::real[];


--
-- Name: node id; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.node ALTER COLUMN id SET DEFAULT nextval('___.node_id_seq'::regclass);


--
-- Name: node zground; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.node ALTER COLUMN zground SET DEFAULT 9999;


--
-- Name: pipe_link id; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.pipe_link ALTER COLUMN id SET DEFAULT nextval('___.link_id_seq'::regclass);


--
-- Name: pipe_link name; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.pipe_link ALTER COLUMN name SET DEFAULT ___.unique_name('pipe'::character varying, 'link'::character varying);


--
-- Name: pipe_link thickness_mm; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.pipe_link ALTER COLUMN thickness_mm SET DEFAULT 8;


--
-- Name: pipe_link material; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.pipe_link ALTER COLUMN material SET DEFAULT 'Unknown'::character varying;


--
-- Name: pipe_link status_open; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.pipe_link ALTER COLUMN status_open SET DEFAULT true;


--
-- Name: pipe_link meter; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.pipe_link ALTER COLUMN meter SET DEFAULT false;


--
-- Name: pipe_link overload_length; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.pipe_link ALTER COLUMN overload_length SET DEFAULT false;


--
-- Name: pipe_link overload_celerity; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.pipe_link ALTER COLUMN overload_celerity SET DEFAULT false;


--
-- Name: pipe_link overload_roughness_mm; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.pipe_link ALTER COLUMN overload_roughness_mm SET DEFAULT false;


--
-- Name: pipe_link_singularity number; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.pipe_link_singularity ALTER COLUMN number SET DEFAULT 1;


--
-- Name: pipe_link_singularity type_singularity; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.pipe_link_singularity ALTER COLUMN type_singularity SET DEFAULT 'Bend'::___.pipe_type_singularity;


--
-- Name: pressure_accumulator_singularity id; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.pressure_accumulator_singularity ALTER COLUMN id SET DEFAULT nextval('___.singularity_id_seq'::regclass);


--
-- Name: pressure_accumulator_singularity name; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.pressure_accumulator_singularity ALTER COLUMN name SET DEFAULT ___.unique_name('pressure_accumulator'::character varying, 'singularity'::character varying);


--
-- Name: pressure_accumulator_singularity v0; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.pressure_accumulator_singularity ALTER COLUMN v0 SET DEFAULT 0;


--
-- Name: pressure_accumulator_singularity z_connection; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.pressure_accumulator_singularity ALTER COLUMN z_connection SET DEFAULT 0;


--
-- Name: pressure_accumulator_singularity filling_pressure; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.pressure_accumulator_singularity ALTER COLUMN filling_pressure SET DEFAULT 0;


--
-- Name: pressure_regulator_link id; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.pressure_regulator_link ALTER COLUMN id SET DEFAULT nextval('___.link_id_seq'::regclass);


--
-- Name: pressure_regulator_link name; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.pressure_regulator_link ALTER COLUMN name SET DEFAULT ___.unique_name('pressure_regulator'::character varying, 'link'::character varying);


--
-- Name: pressure_regulator_link headloss_coef; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.pressure_regulator_link ALTER COLUMN headloss_coef SET DEFAULT 1.5;


--
-- Name: pressure_regulator_link pressure_regulation_option; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.pressure_regulator_link ALTER COLUMN pressure_regulation_option SET DEFAULT 'Constant pressure'::___.pressure_regulation_option;


--
-- Name: pressure_regulator_link regul_pq_array; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.pressure_regulator_link ALTER COLUMN regul_pq_array SET DEFAULT '{{0,0}}'::real[];


--
-- Name: pressure_regulator_link pressure_regulation_mode; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.pressure_regulator_link ALTER COLUMN pressure_regulation_mode SET DEFAULT 'Imposed differential pressure'::___.pressure_regulation_mode;


--
-- Name: pump_link id; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.pump_link ALTER COLUMN id SET DEFAULT nextval('___.link_id_seq'::regclass);


--
-- Name: pump_link name; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.pump_link ALTER COLUMN name SET DEFAULT ___.unique_name('pump'::character varying, 'link'::character varying);


--
-- Name: pump_link pq_array; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.pump_link ALTER COLUMN pq_array SET DEFAULT '{{0,0}}'::real[];


--
-- Name: pump_link check_valve; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.pump_link ALTER COLUMN check_valve SET DEFAULT true;


--
-- Name: pump_link pump_regulation_option; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.pump_link ALTER COLUMN pump_regulation_option SET DEFAULT 'No regulation'::___.pump_regulation_option;


--
-- Name: pump_link speed_reduction_coef; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.pump_link ALTER COLUMN speed_reduction_coef SET DEFAULT 1;


--
-- Name: pump_link regul_q; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.pump_link ALTER COLUMN regul_q SET DEFAULT 0;


--
-- Name: pump_link regul_z_start; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.pump_link ALTER COLUMN regul_z_start SET DEFAULT 0;


--
-- Name: pump_link regul_z_stop; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.pump_link ALTER COLUMN regul_z_stop SET DEFAULT 0;


--
-- Name: pump_link regul_p; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.pump_link ALTER COLUMN regul_p SET DEFAULT 0;


--
-- Name: reservoir_node id; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.reservoir_node ALTER COLUMN id SET DEFAULT nextval('___.node_id_seq'::regclass);


--
-- Name: reservoir_node name; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.reservoir_node ALTER COLUMN name SET DEFAULT ___.unique_name('reservoir'::character varying, 'node'::character varying);


--
-- Name: reservoir_node sz_array; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.reservoir_node ALTER COLUMN sz_array SET DEFAULT '{{0,0}}'::real[];


--
-- Name: reservoir_node alim_mode; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.reservoir_node ALTER COLUMN alim_mode SET DEFAULT 'From bottom'::___.alim_mode;


--
-- Name: reservoir_node return_in_valve_mode; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.reservoir_node ALTER COLUMN return_in_valve_mode SET DEFAULT 'Regulated'::___.return_valve_mode;


--
-- Name: reservoir_node return_out_valve_mode; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.reservoir_node ALTER COLUMN return_out_valve_mode SET DEFAULT 'Regulated'::___.return_valve_mode;


--
-- Name: reservoir_node mixing_mode; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.reservoir_node ALTER COLUMN mixing_mode SET DEFAULT 'Perfect mixing'::___.mixing_mode;


--
-- Name: reservoir_node zground; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.reservoir_node ALTER COLUMN zground SET DEFAULT 9999;


--
-- Name: scenario name; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.scenario ALTER COLUMN name SET DEFAULT ___.unique_name('scenario'::character varying, abbreviation => 'SCN'::character varying);


--
-- Name: scenario comput_mode; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.scenario ALTER COLUMN comput_mode SET DEFAULT 'Steady state'::___.computation_mode;


--
-- Name: scenario starting_date; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.scenario ALTER COLUMN starting_date SET DEFAULT '2000-01-01 00:00:00'::timestamp without time zone;


--
-- Name: scenario duration; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.scenario ALTER COLUMN duration SET DEFAULT '12:00:00'::interval;


--
-- Name: scenario timestep; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.scenario ALTER COLUMN timestep SET DEFAULT 300;


--
-- Name: scenario refined_discretisation; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.scenario ALTER COLUMN refined_discretisation SET DEFAULT false;


--
-- Name: scenario peak_coefficient; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.scenario ALTER COLUMN peak_coefficient SET DEFAULT 1;


--
-- Name: scenario tini; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.scenario ALTER COLUMN tini SET DEFAULT '12:00:00'::interval;


--
-- Name: scenario flag_save; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.scenario ALTER COLUMN flag_save SET DEFAULT false;


--
-- Name: scenario tsave_hr; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.scenario ALTER COLUMN tsave_hr SET DEFAULT '00:00:00'::interval;


--
-- Name: scenario flag_rstart; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.scenario ALTER COLUMN flag_rstart SET DEFAULT false;


--
-- Name: scenario trstart_hr; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.scenario ALTER COLUMN trstart_hr SET DEFAULT '00:00:01'::interval;


--
-- Name: scenario dt_output_hr; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.scenario ALTER COLUMN dt_output_hr SET DEFAULT 300;


--
-- Name: scenario tbegin_output_hr; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.scenario ALTER COLUMN tbegin_output_hr SET DEFAULT '00:00:00'::interval;


--
-- Name: scenario tend_output_hr; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.scenario ALTER COLUMN tend_output_hr SET DEFAULT '12:00:00'::interval;


--
-- Name: scenario model_connect_settings; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.scenario ALTER COLUMN model_connect_settings SET DEFAULT 'Cascade'::___.model_connection_settings;


--
-- Name: scenario vaporization; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.scenario ALTER COLUMN vaporization SET DEFAULT false;


--
-- Name: scenario quality_output; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.scenario ALTER COLUMN quality_output SET DEFAULT 'None'::___.quality_output;


--
-- Name: scenario chlorine; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.scenario ALTER COLUMN chlorine SET DEFAULT false;


--
-- Name: scenario trihalomethane; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.scenario ALTER COLUMN trihalomethane SET DEFAULT false;


--
-- Name: scenario passive_tracer; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.scenario ALTER COLUMN passive_tracer SET DEFAULT false;


--
-- Name: scenario travel_time; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.scenario ALTER COLUMN travel_time SET DEFAULT false;


--
-- Name: scenario chlorine_water_reaction_coef; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.scenario ALTER COLUMN chlorine_water_reaction_coef SET DEFAULT 0.05;


--
-- Name: scenario chlorine_wall_reaction_coef; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.scenario ALTER COLUMN chlorine_wall_reaction_coef SET DEFAULT 0.0000005;


--
-- Name: scenario trihalomethane_reaction_parameter; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.scenario ALTER COLUMN trihalomethane_reaction_parameter SET DEFAULT 0.05;


--
-- Name: scenario trihalomethane_concentration_limit; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.scenario ALTER COLUMN trihalomethane_concentration_limit SET DEFAULT 0.0000005;


--
-- Name: scenario_link_failure id; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.scenario_link_failure ALTER COLUMN id SET DEFAULT nextval('___.scenario_link_failure_id_seq'::regclass);


--
-- Name: scenario_link_failure failure_timestamp; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.scenario_link_failure ALTER COLUMN failure_timestamp SET DEFAULT '00:00:00'::interval;


--
-- Name: scenario_pump_failure id; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.scenario_pump_failure ALTER COLUMN id SET DEFAULT nextval('___.scenario_pump_failure_id_seq'::regclass);


--
-- Name: scenario_pump_failure link_type; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.scenario_pump_failure ALTER COLUMN link_type SET DEFAULT 'pump'::___.type_link_;


--
-- Name: scenario_pump_failure failure_timestamp; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.scenario_pump_failure ALTER COLUMN failure_timestamp SET DEFAULT '00:00:00'::interval;


--
-- Name: scenario_pump_failure failure_mode; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.scenario_pump_failure ALTER COLUMN failure_mode SET DEFAULT 'Automatic alpha computation'::___.pump_failure_mode;


--
-- Name: scenario_water_origin id; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.scenario_water_origin ALTER COLUMN id SET DEFAULT nextval('___.scenario_water_origin_id_seq'::regclass);


--
-- Name: sectorization_settings pipe_link_meter; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.sectorization_settings ALTER COLUMN pipe_link_meter SET DEFAULT true;


--
-- Name: sectorization_settings valve_link_open; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.sectorization_settings ALTER COLUMN valve_link_open SET DEFAULT true;


--
-- Name: sectorization_settings pressure_regulator_link; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.sectorization_settings ALTER COLUMN pressure_regulator_link SET DEFAULT false;


--
-- Name: sectorization_settings flow_regulator_link; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.sectorization_settings ALTER COLUMN flow_regulator_link SET DEFAULT false;


--
-- Name: sectorization_settings reservoir_node; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.sectorization_settings ALTER COLUMN reservoir_node SET DEFAULT false;


--
-- Name: sectorization_settings id; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.sectorization_settings ALTER COLUMN id SET DEFAULT 'Sectorization'::character varying;


--
-- Name: surge_tank_singularity id; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.surge_tank_singularity ALTER COLUMN id SET DEFAULT nextval('___.singularity_id_seq'::regclass);


--
-- Name: surge_tank_singularity name; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.surge_tank_singularity ALTER COLUMN name SET DEFAULT ___.unique_name('surge_tank'::character varying, 'singularity'::character varying);


--
-- Name: surge_tank_singularity sz_array; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.surge_tank_singularity ALTER COLUMN sz_array SET DEFAULT '{{0,0}}'::real[];


--
-- Name: surge_tank_singularity zini; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.surge_tank_singularity ALTER COLUMN zini SET DEFAULT 0;


--
-- Name: surge_tank_singularity pressurized_option; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.surge_tank_singularity ALTER COLUMN pressurized_option SET DEFAULT false;


--
-- Name: surge_tank_singularity in_headloss_coef; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.surge_tank_singularity ALTER COLUMN in_headloss_coef SET DEFAULT 1.5;


--
-- Name: surge_tank_singularity out_headloss_coef; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.surge_tank_singularity ALTER COLUMN out_headloss_coef SET DEFAULT 1.5;


--
-- Name: threshold_warning_settings pipe_maximum_speed; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.threshold_warning_settings ALTER COLUMN pipe_maximum_speed SET DEFAULT 2;


--
-- Name: threshold_warning_settings pipe_minimum_speed; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.threshold_warning_settings ALTER COLUMN pipe_minimum_speed SET DEFAULT 0.3;


--
-- Name: threshold_warning_settings node_maximum_pressure; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.threshold_warning_settings ALTER COLUMN node_maximum_pressure SET DEFAULT 100;


--
-- Name: threshold_warning_settings node_minimum_pressure; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.threshold_warning_settings ALTER COLUMN node_minimum_pressure SET DEFAULT '-50'::integer;


--
-- Name: threshold_warning_settings minimum_service_pressure; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.threshold_warning_settings ALTER COLUMN minimum_service_pressure SET DEFAULT 20;


--
-- Name: threshold_warning_settings id; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.threshold_warning_settings ALTER COLUMN id SET DEFAULT 'Warnings'::character varying;


--
-- Name: user_node id; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.user_node ALTER COLUMN id SET DEFAULT nextval('___.node_id_seq'::regclass);


--
-- Name: user_node name; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.user_node ALTER COLUMN name SET DEFAULT ___.unique_name('user'::character varying, 'node'::character varying);


--
-- Name: user_node q0; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.user_node ALTER COLUMN q0 SET DEFAULT 0;


--
-- Name: user_node fire_hydrant; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.user_node ALTER COLUMN fire_hydrant SET DEFAULT false;


--
-- Name: user_node zground; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.user_node ALTER COLUMN zground SET DEFAULT 9999;


--
-- Name: valve_link id; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.valve_link ALTER COLUMN id SET DEFAULT nextval('___.link_id_seq'::regclass);


--
-- Name: valve_link name; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.valve_link ALTER COLUMN name SET DEFAULT ___.unique_name('valve'::character varying, 'link'::character varying);


--
-- Name: valve_link headloss_coef; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.valve_link ALTER COLUMN headloss_coef SET DEFAULT 1.5;


--
-- Name: valve_link c_opening; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.valve_link ALTER COLUMN c_opening SET DEFAULT 1;


--
-- Name: water_delivery_point name; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.water_delivery_point ALTER COLUMN name SET DEFAULT ___.unique_name('water_delivery_point'::character varying, abbreviation => 'WD'::character varying);


--
-- Name: water_delivery_point volume; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.water_delivery_point ALTER COLUMN volume SET DEFAULT 0;


--
-- Name: water_delivery_point industrial; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.water_delivery_point ALTER COLUMN industrial SET DEFAULT false;


--
-- Name: water_delivery_scenario name; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.water_delivery_scenario ALTER COLUMN name SET DEFAULT ___.unique_name('water_delivery_scenario'::character varying, abbreviation => 'WD_SCN'::character varying);


--
-- Name: water_delivery_scenario period; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.water_delivery_scenario ALTER COLUMN period SET DEFAULT 'Weekday'::___.week_period;


--
-- Name: water_delivery_scenario_industrial_volume volume; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.water_delivery_scenario_industrial_volume ALTER COLUMN volume SET DEFAULT 0;


--
-- Name: water_delivery_scenario_sector_setting adjust_coef; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.water_delivery_scenario_sector_setting ALTER COLUMN adjust_coef SET DEFAULT 1;


--
-- Name: water_delivery_scenario_sector_setting leak_efficiency; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.water_delivery_scenario_sector_setting ALTER COLUMN leak_efficiency SET DEFAULT 1;


--
-- Name: water_delivery_sector name; Type: DEFAULT; Schema: api; Owner: hydra
--

ALTER TABLE ONLY api.water_delivery_sector ALTER COLUMN name SET DEFAULT ___.unique_name('water_delivery_sector'::character varying, abbreviation => 'WD_SECT'::character varying);


--
-- Data for Name: air_relief_valve_singularity; Type: TABLE DATA; Schema: ___; Owner: hydra
--

COPY ___.air_relief_valve_singularity (id, name, _type, orifice_diameter, contraction_coef) FROM stdin;
\.


--
-- Data for Name: cascade; Type: TABLE DATA; Schema: ___; Owner: hydra
--

COPY ___.cascade (priority, scenario, model) FROM stdin;
\.


--
-- Data for Name: check_valve_link; Type: TABLE DATA; Schema: ___; Owner: hydra
--

COPY ___.check_valve_link (id, name, _type, section, headloss_coef, opening_pressure, closing_mode, cd_coef) FROM stdin;
\.


--
-- Data for Name: chlorine_injection_singularity; Type: TABLE DATA; Schema: ___; Owner: hydra
--

COPY ___.chlorine_injection_singularity (id, name, _type, target_concentration, target_node) FROM stdin;
\.


--
-- Data for Name: failure_curve; Type: TABLE DATA; Schema: ___; Owner: hydra
--

COPY ___.failure_curve (name, comment, alpt_array) FROM stdin;
\.


--
-- Data for Name: flow_injection_singularity; Type: TABLE DATA; Schema: ___; Owner: hydra
--

COPY ___.flow_injection_singularity (id, name, _type, q0, external_file, chlorine, trihalomethane, passive_tracer) FROM stdin;
\.


--
-- Data for Name: flow_regulator_link; Type: TABLE DATA; Schema: ___; Owner: hydra
--

COPY ___.flow_regulator_link (id, name, _type, section, headloss_coef, regul_q) FROM stdin;
\.


--
-- Data for Name: fluid_properties; Type: TABLE DATA; Schema: ___; Owner: hydra
--

COPY ___.fluid_properties (id, volumic_mass_kg_m3, temperature_c, kinematic_viscosity_m2_s, volumic_elasticity_module_n_m2) FROM stdin;
Fluid properties	1000	10	1.3e-06	2050000000
\.


--
-- Data for Name: groupe; Type: TABLE DATA; Schema: ___; Owner: hydra
--

COPY ___.groupe (name) FROM stdin;
\.


--
-- Data for Name: groupe_model; Type: TABLE DATA; Schema: ___; Owner: hydra
--

COPY ___.groupe_model (groupe, model) FROM stdin;
\.


--
-- Data for Name: headloss_link; Type: TABLE DATA; Schema: ___; Owner: hydra
--

COPY ___.headloss_link (id, name, _type, section, rk1, rk2) FROM stdin;
\.


--
-- Data for Name: hourly_modulation_curve; Type: TABLE DATA; Schema: ___; Owner: hydra
--

COPY ___.hourly_modulation_curve (name, comment, hourly_modulation_array) FROM stdin;
\.


--
-- Data for Name: imposed_piezometry_singularity; Type: TABLE DATA; Schema: ___; Owner: hydra
--

COPY ___.imposed_piezometry_singularity (id, name, _type, z0, chlorine, trihalomethane, passive_tracer) FROM stdin;
\.


--
-- Data for Name: link; Type: TABLE DATA; Schema: ___; Owner: hydra
--

COPY ___.link (id, name, _type, up, down, comment, geom) FROM stdin;
\.


--
-- Data for Name: material; Type: TABLE DATA; Schema: ___; Owner: hydra
--

COPY ___.material (name, roughness_mm, elasticity_n_m2) FROM stdin;
Unknown	0.5	200000000000
Ductile iron	0.05	170000000000
Grey cast iron	0.1	170000000000
Steel	0.05	220000000000
Reinforced concrete	0.1	25000000000
Asbestos ciment	0.1	22800000000
Polyvinyl chloride	0.025	3000000000
High density polyethylene	0.025	1200000000
Low density polyethylene	0.025	200000000
\.


--
-- Data for Name: metadata; Type: TABLE DATA; Schema: ___; Owner: hydra
--

COPY ___.metadata (id, creation_date, srid) FROM stdin;
1	2022-08-23 21:12:37.083364	2154
\.


--
-- Data for Name: mixed; Type: TABLE DATA; Schema: ___; Owner: hydra
--

COPY ___.mixed (priority, scenario, groupe) FROM stdin;
\.


--
-- Data for Name: model; Type: TABLE DATA; Schema: ___; Owner: hydra
--

COPY ___.model (name, creation_date, comment) FROM stdin;
\.


--
-- Data for Name: model_connection_singularity; Type: TABLE DATA; Schema: ___; Owner: hydra
--

COPY ___.model_connection_singularity (id, name, _type, cascade_mode, zt_array, qt_array) FROM stdin;
\.


--
-- Data for Name: node; Type: TABLE DATA; Schema: ___; Owner: hydra
--

COPY ___.node (id, name, model, _type, comment, zground, geom) FROM stdin;
\.


--
-- Data for Name: pipe_link; Type: TABLE DATA; Schema: ___; Owner: hydra
--

COPY ___.pipe_link (id, name, _type, diameter, thickness_mm, material, status_open, meter, overload_length, overloaded_length, overload_celerity, overloaded_celerity, overload_roughness_mm, overloaded_roughness_mm) FROM stdin;
\.


--
-- Data for Name: pipe_link_singularity; Type: TABLE DATA; Schema: ___; Owner: hydra
--

COPY ___.pipe_link_singularity (id, pipe_link, number, type_singularity, bend_angle, borda_section, borda_coef) FROM stdin;
\.


--
-- Data for Name: pressure_accumulator_singularity; Type: TABLE DATA; Schema: ___; Owner: hydra
--

COPY ___.pressure_accumulator_singularity (id, name, _type, v0, z_connection, filling_pressure) FROM stdin;
\.


--
-- Data for Name: pressure_regulator_link; Type: TABLE DATA; Schema: ___; Owner: hydra
--

COPY ___.pressure_regulator_link (id, name, _type, section, headloss_coef, pressure_regulation_option, regul_p, regul_pq_array, pressure_regulation_mode) FROM stdin;
\.


--
-- Data for Name: pump_link; Type: TABLE DATA; Schema: ___; Owner: hydra
--

COPY ___.pump_link (id, name, _type, pq_array, check_valve, rotation_speed, inertia, pump_regulation_option, speed_reduction_coef, regul_q, target_node, regul_z_start, regul_z_stop, regul_p) FROM stdin;
\.


--
-- Data for Name: regulation_scenario; Type: TABLE DATA; Schema: ___; Owner: hydra
--

COPY ___.regulation_scenario (priority, regulation_file, scenario) FROM stdin;
\.


--
-- Data for Name: reservoir_node; Type: TABLE DATA; Schema: ___; Owner: hydra
--

COPY ___.reservoir_node (id, name, _type, sz_array, alim_mode, z_ini, z_overflow, return_in_valve_mode, return_out_valve_mode, mixing_mode) FROM stdin;
\.


--
-- Data for Name: scenario; Type: TABLE DATA; Schema: ___; Owner: hydra
--

COPY ___.scenario (name, comment, comput_mode, starting_date, duration, timestep, refined_discretisation, water_delivery_scenario, peak_coefficient, tini, flag_save, tsave_hr, flag_rstart, scenario_rstart, trstart_hr, dt_output_hr, tbegin_output_hr, tend_output_hr, scenario_ref, model_connect_settings, option_file, four_quadrant_file, vaporization, quality_output, chlorine, trihalomethane, passive_tracer, travel_time, chlorine_water_reaction_coef, chlorine_wall_reaction_coef, trihalomethane_reaction_parameter, trihalomethane_concentration_limit) FROM stdin;
\.


--
-- Data for Name: scenario_link_failure; Type: TABLE DATA; Schema: ___; Owner: hydra
--

COPY ___.scenario_link_failure (id, scenario, link, link_type, failure_timestamp, failure_curve) FROM stdin;
\.


--
-- Data for Name: scenario_pump_failure; Type: TABLE DATA; Schema: ___; Owner: hydra
--

COPY ___.scenario_pump_failure (id, scenario, link, link_type, failure_timestamp, failure_curve, failure_mode) FROM stdin;
\.


--
-- Data for Name: scenario_water_origin; Type: TABLE DATA; Schema: ___; Owner: hydra
--

COPY ___.scenario_water_origin (id, scenario, imposed_piezometry_singularity, comment) FROM stdin;
\.


--
-- Data for Name: sectorization_settings; Type: TABLE DATA; Schema: ___; Owner: hydra
--

COPY ___.sectorization_settings (pipe_link_meter, valve_link_open, pressure_regulator_link, flow_regulator_link, reservoir_node, id) FROM stdin;
t	t	f	f	f	Sectorization
\.


--
-- Data for Name: singularity; Type: TABLE DATA; Schema: ___; Owner: hydra
--

COPY ___.singularity (id, name, _type, comment) FROM stdin;
\.


--
-- Data for Name: surge_tank_singularity; Type: TABLE DATA; Schema: ___; Owner: hydra
--

COPY ___.surge_tank_singularity (id, name, _type, sz_array, zini, pressurized_option, orifice_diameter, in_headloss_coef, out_headloss_coef) FROM stdin;
\.


--
-- Data for Name: threshold_warning_settings; Type: TABLE DATA; Schema: ___; Owner: hydra
--

COPY ___.threshold_warning_settings (pipe_maximum_speed, pipe_minimum_speed, node_maximum_pressure, node_minimum_pressure, minimum_service_pressure, id) FROM stdin;
2	0.3	100	-50	20	Warnings
\.


--
-- Data for Name: type_link; Type: TABLE DATA; Schema: ___; Owner: hydra
--

COPY ___.type_link (name, exportid, exportcode, abbreviation) FROM stdin;
pipe	3	CANA	CANA
valve	4	VANNE	VALV
flow_regulator	5	REGQ	REGQ
pressure_regulator	6	REDP	REGP
check_valve	7	CLPT	CLPT
pump	8	POMP	POMP
headloss	9	BORDA	BORDA
\.


--
-- Data for Name: type_node; Type: TABLE DATA; Schema: ___; Owner: hydra
--

COPY ___.type_node (name, exportid, exportcode, abbreviation, comment) FROM stdin;
user	1	USER_NODE	NOD	\N
reservoir	2	RESERVOIR_NODE	RES	\N
\.


--
-- Data for Name: type_singularity; Type: TABLE DATA; Schema: ___; Owner: hydra
--

COPY ___.type_singularity (name, exportid, exportcode, abbreviation, comment) FROM stdin;
surge_tank	10	TANK	TANK	\N
pressure_accumulator	11	BALL	BALL	\N
air_relief_valve	12	VNTS	AIRV	\N
imposed_piezometry	13	PRES	IMP	\N
flow_injection	14	EQAPP	QINJ	\N
chlorine_injection	15	ECHLORE	CINJ	\N
model_connection	16	RACC	RACC	\N
\.


--
-- Data for Name: user_node; Type: TABLE DATA; Schema: ___; Owner: hydra
--

COPY ___.user_node (id, name, _type, domestic_curve, industrial_curve, q0, fire_hydrant) FROM stdin;
\.


--
-- Data for Name: valve_link; Type: TABLE DATA; Schema: ___; Owner: hydra
--

COPY ___.valve_link (id, name, _type, section, headloss_coef, c_opening) FROM stdin;
\.


--
-- Data for Name: water_delivery_point; Type: TABLE DATA; Schema: ___; Owner: hydra
--

COPY ___.water_delivery_point (name, comment, geom, volume, industrial, pipe_link) FROM stdin;
\.


--
-- Data for Name: water_delivery_scenario; Type: TABLE DATA; Schema: ___; Owner: hydra
--

COPY ___.water_delivery_scenario (name, comment, period) FROM stdin;
\.


--
-- Data for Name: water_delivery_scenario_industrial_volume; Type: TABLE DATA; Schema: ___; Owner: hydra
--

COPY ___.water_delivery_scenario_industrial_volume (water_delivery_scenario, water_delivery_point, volume, industrial) FROM stdin;
\.


--
-- Data for Name: water_delivery_scenario_sector_setting; Type: TABLE DATA; Schema: ___; Owner: hydra
--

COPY ___.water_delivery_scenario_sector_setting (scenario, sector, volume_m3j, adjust_coef, leak_efficiency) FROM stdin;
\.


--
-- Data for Name: water_delivery_sector; Type: TABLE DATA; Schema: ___; Owner: hydra
--

COPY ___.water_delivery_sector (name, comment, geom) FROM stdin;
\.


--
-- Data for Name: spatial_ref_sys; Type: TABLE DATA; Schema: public; Owner: hydra
--

COPY public.spatial_ref_sys (srid, auth_name, auth_srid, srtext, proj4text) FROM stdin;
\.


--
-- Name: air_relief_valve_singularity_name_seq; Type: SEQUENCE SET; Schema: ___; Owner: hydra
--

SELECT pg_catalog.setval('___.air_relief_valve_singularity_name_seq', 1, false);


--
-- Name: check_valve_link_name_seq; Type: SEQUENCE SET; Schema: ___; Owner: hydra
--

SELECT pg_catalog.setval('___.check_valve_link_name_seq', 1, false);


--
-- Name: chlorine_injection_singularity_name_seq; Type: SEQUENCE SET; Schema: ___; Owner: hydra
--

SELECT pg_catalog.setval('___.chlorine_injection_singularity_name_seq', 1, false);


--
-- Name: failure_curve_name_seq; Type: SEQUENCE SET; Schema: ___; Owner: hydra
--

SELECT pg_catalog.setval('___.failure_curve_name_seq', 1, false);


--
-- Name: flow_injection_singularity_name_seq; Type: SEQUENCE SET; Schema: ___; Owner: hydra
--

SELECT pg_catalog.setval('___.flow_injection_singularity_name_seq', 1, false);


--
-- Name: flow_regulator_link_name_seq; Type: SEQUENCE SET; Schema: ___; Owner: hydra
--

SELECT pg_catalog.setval('___.flow_regulator_link_name_seq', 1, false);


--
-- Name: groupe_name_seq; Type: SEQUENCE SET; Schema: ___; Owner: hydra
--

SELECT pg_catalog.setval('___.groupe_name_seq', 1, false);


--
-- Name: headloss_link_name_seq; Type: SEQUENCE SET; Schema: ___; Owner: hydra
--

SELECT pg_catalog.setval('___.headloss_link_name_seq', 1, false);


--
-- Name: hourly_modulation_curve_name_seq; Type: SEQUENCE SET; Schema: ___; Owner: hydra
--

SELECT pg_catalog.setval('___.hourly_modulation_curve_name_seq', 1, false);


--
-- Name: imposed_piezometry_singularity_name_seq; Type: SEQUENCE SET; Schema: ___; Owner: hydra
--

SELECT pg_catalog.setval('___.imposed_piezometry_singularity_name_seq', 1, false);


--
-- Name: link_id_seq; Type: SEQUENCE SET; Schema: ___; Owner: hydra
--

SELECT pg_catalog.setval('___.link_id_seq', 1, false);


--
-- Name: material_name_seq; Type: SEQUENCE SET; Schema: ___; Owner: hydra
--

SELECT pg_catalog.setval('___.material_name_seq', 1, false);


--
-- Name: model_connection_singularity_name_seq; Type: SEQUENCE SET; Schema: ___; Owner: hydra
--

SELECT pg_catalog.setval('___.model_connection_singularity_name_seq', 1, false);


--
-- Name: model_name_seq; Type: SEQUENCE SET; Schema: ___; Owner: hydra
--

SELECT pg_catalog.setval('___.model_name_seq', 1, false);


--
-- Name: node_id_seq; Type: SEQUENCE SET; Schema: ___; Owner: hydra
--

SELECT pg_catalog.setval('___.node_id_seq', 1, false);


--
-- Name: pipe_link_name_seq; Type: SEQUENCE SET; Schema: ___; Owner: hydra
--

SELECT pg_catalog.setval('___.pipe_link_name_seq', 1, false);


--
-- Name: pressure_accumulator_singularity_name_seq; Type: SEQUENCE SET; Schema: ___; Owner: hydra
--

SELECT pg_catalog.setval('___.pressure_accumulator_singularity_name_seq', 1, false);


--
-- Name: pressure_regulator_link_name_seq; Type: SEQUENCE SET; Schema: ___; Owner: hydra
--

SELECT pg_catalog.setval('___.pressure_regulator_link_name_seq', 1, false);


--
-- Name: pump_link_name_seq; Type: SEQUENCE SET; Schema: ___; Owner: hydra
--

SELECT pg_catalog.setval('___.pump_link_name_seq', 1, false);


--
-- Name: reservoir_node_name_seq; Type: SEQUENCE SET; Schema: ___; Owner: hydra
--

SELECT pg_catalog.setval('___.reservoir_node_name_seq', 1, false);


--
-- Name: scenario_link_failure_id_seq; Type: SEQUENCE SET; Schema: ___; Owner: hydra
--

SELECT pg_catalog.setval('___.scenario_link_failure_id_seq', 1, false);


--
-- Name: scenario_name_seq; Type: SEQUENCE SET; Schema: ___; Owner: hydra
--

SELECT pg_catalog.setval('___.scenario_name_seq', 1, false);


--
-- Name: scenario_pump_failure_id_seq; Type: SEQUENCE SET; Schema: ___; Owner: hydra
--

SELECT pg_catalog.setval('___.scenario_pump_failure_id_seq', 1, false);


--
-- Name: scenario_water_origin_id_seq; Type: SEQUENCE SET; Schema: ___; Owner: hydra
--

SELECT pg_catalog.setval('___.scenario_water_origin_id_seq', 1, false);


--
-- Name: singularity_id_seq; Type: SEQUENCE SET; Schema: ___; Owner: hydra
--

SELECT pg_catalog.setval('___.singularity_id_seq', 1, false);


--
-- Name: surge_tank_singularity_name_seq; Type: SEQUENCE SET; Schema: ___; Owner: hydra
--

SELECT pg_catalog.setval('___.surge_tank_singularity_name_seq', 1, false);


--
-- Name: user_node_name_seq; Type: SEQUENCE SET; Schema: ___; Owner: hydra
--

SELECT pg_catalog.setval('___.user_node_name_seq', 1, false);


--
-- Name: valve_link_name_seq; Type: SEQUENCE SET; Schema: ___; Owner: hydra
--

SELECT pg_catalog.setval('___.valve_link_name_seq', 1, false);


--
-- Name: water_delivery_point_name_seq; Type: SEQUENCE SET; Schema: ___; Owner: hydra
--

SELECT pg_catalog.setval('___.water_delivery_point_name_seq', 1, false);


--
-- Name: water_delivery_scenario_name_seq; Type: SEQUENCE SET; Schema: ___; Owner: hydra
--

SELECT pg_catalog.setval('___.water_delivery_scenario_name_seq', 1, false);


--
-- Name: water_delivery_sector_name_seq; Type: SEQUENCE SET; Schema: ___; Owner: hydra
--

SELECT pg_catalog.setval('___.water_delivery_sector_name_seq', 1, false);


--
-- Name: air_relief_valve_singularity air_relief_valve_singularity_pkey; Type: CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.air_relief_valve_singularity
    ADD CONSTRAINT air_relief_valve_singularity_pkey PRIMARY KEY (id);


--
-- Name: cascade cascade_pkey; Type: CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.cascade
    ADD CONSTRAINT cascade_pkey PRIMARY KEY (priority, scenario, model);


--
-- Name: cascade cascade_scenario_model_key; Type: CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.cascade
    ADD CONSTRAINT cascade_scenario_model_key UNIQUE (scenario, model);


--
-- Name: check_valve_link check_valve_link_pkey; Type: CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.check_valve_link
    ADD CONSTRAINT check_valve_link_pkey PRIMARY KEY (id);


--
-- Name: chlorine_injection_singularity chlorine_injection_singularity_pkey; Type: CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.chlorine_injection_singularity
    ADD CONSTRAINT chlorine_injection_singularity_pkey PRIMARY KEY (id);


--
-- Name: failure_curve failure_curve_pkey; Type: CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.failure_curve
    ADD CONSTRAINT failure_curve_pkey PRIMARY KEY (name);


--
-- Name: flow_injection_singularity flow_injection_singularity_pkey; Type: CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.flow_injection_singularity
    ADD CONSTRAINT flow_injection_singularity_pkey PRIMARY KEY (id);


--
-- Name: flow_regulator_link flow_regulator_link_pkey; Type: CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.flow_regulator_link
    ADD CONSTRAINT flow_regulator_link_pkey PRIMARY KEY (id);


--
-- Name: fluid_properties fluid_properties_pkey; Type: CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.fluid_properties
    ADD CONSTRAINT fluid_properties_pkey PRIMARY KEY (id);


--
-- Name: groupe_model groupe_model_pkey; Type: CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.groupe_model
    ADD CONSTRAINT groupe_model_pkey PRIMARY KEY (groupe, model);


--
-- Name: groupe groupe_pkey; Type: CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.groupe
    ADD CONSTRAINT groupe_pkey PRIMARY KEY (name);


--
-- Name: headloss_link headloss_link_pkey; Type: CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.headloss_link
    ADD CONSTRAINT headloss_link_pkey PRIMARY KEY (id);


--
-- Name: hourly_modulation_curve hourly_modulation_curve_pkey; Type: CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.hourly_modulation_curve
    ADD CONSTRAINT hourly_modulation_curve_pkey PRIMARY KEY (name);


--
-- Name: imposed_piezometry_singularity imposed_piezometry_singularity_pkey; Type: CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.imposed_piezometry_singularity
    ADD CONSTRAINT imposed_piezometry_singularity_pkey PRIMARY KEY (id);


--
-- Name: link link_id__type_key; Type: CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.link
    ADD CONSTRAINT link_id__type_key UNIQUE (id, _type);


--
-- Name: link link_id_name__type_key; Type: CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.link
    ADD CONSTRAINT link_id_name__type_key UNIQUE (id, name, _type);


--
-- Name: link link_pkey; Type: CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.link
    ADD CONSTRAINT link_pkey PRIMARY KEY (id);


--
-- Name: material material_pkey; Type: CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.material
    ADD CONSTRAINT material_pkey PRIMARY KEY (name);


--
-- Name: metadata metadata_pkey; Type: CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.metadata
    ADD CONSTRAINT metadata_pkey PRIMARY KEY (id);


--
-- Name: mixed mixed_pkey; Type: CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.mixed
    ADD CONSTRAINT mixed_pkey PRIMARY KEY (priority, scenario, groupe);


--
-- Name: mixed mixed_scenario_groupe_key; Type: CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.mixed
    ADD CONSTRAINT mixed_scenario_groupe_key UNIQUE (scenario, groupe);


--
-- Name: model_connection_singularity model_connection_singularity_pkey; Type: CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.model_connection_singularity
    ADD CONSTRAINT model_connection_singularity_pkey PRIMARY KEY (id);


--
-- Name: model model_pkey; Type: CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.model
    ADD CONSTRAINT model_pkey PRIMARY KEY (name);


--
-- Name: node node_geom_key; Type: CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.node
    ADD CONSTRAINT node_geom_key UNIQUE (geom);


--
-- Name: node node_id_name__type_key; Type: CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.node
    ADD CONSTRAINT node_id_name__type_key UNIQUE (id, name, _type);


--
-- Name: node node_name_model_key; Type: CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.node
    ADD CONSTRAINT node_name_model_key UNIQUE (name, model);


--
-- Name: node node_pkey; Type: CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.node
    ADD CONSTRAINT node_pkey PRIMARY KEY (id);


--
-- Name: pipe_link pipe_link_pkey; Type: CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.pipe_link
    ADD CONSTRAINT pipe_link_pkey PRIMARY KEY (id);


--
-- Name: pipe_link_singularity pipe_link_singularity_pipe_link_type_singularity_key; Type: CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.pipe_link_singularity
    ADD CONSTRAINT pipe_link_singularity_pipe_link_type_singularity_key UNIQUE (pipe_link, type_singularity);


--
-- Name: pipe_link_singularity pipe_link_singularity_pkey; Type: CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.pipe_link_singularity
    ADD CONSTRAINT pipe_link_singularity_pkey PRIMARY KEY (id);


--
-- Name: pressure_accumulator_singularity pressure_accumulator_singularity_pkey; Type: CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.pressure_accumulator_singularity
    ADD CONSTRAINT pressure_accumulator_singularity_pkey PRIMARY KEY (id);


--
-- Name: pressure_regulator_link pressure_regulator_link_pkey; Type: CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.pressure_regulator_link
    ADD CONSTRAINT pressure_regulator_link_pkey PRIMARY KEY (id);


--
-- Name: pump_link pump_link_pkey; Type: CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.pump_link
    ADD CONSTRAINT pump_link_pkey PRIMARY KEY (id);


--
-- Name: regulation_scenario regulation_scenario_pkey; Type: CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.regulation_scenario
    ADD CONSTRAINT regulation_scenario_pkey PRIMARY KEY (regulation_file, scenario);


--
-- Name: regulation_scenario regulation_scenario_priority_scenario_key; Type: CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.regulation_scenario
    ADD CONSTRAINT regulation_scenario_priority_scenario_key UNIQUE (priority, scenario);


--
-- Name: reservoir_node reservoir_node_pkey; Type: CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.reservoir_node
    ADD CONSTRAINT reservoir_node_pkey PRIMARY KEY (id);


--
-- Name: scenario_link_failure scenario_link_failure_pkey; Type: CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.scenario_link_failure
    ADD CONSTRAINT scenario_link_failure_pkey PRIMARY KEY (id);


--
-- Name: scenario_link_failure scenario_link_failure_scenario_link_key; Type: CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.scenario_link_failure
    ADD CONSTRAINT scenario_link_failure_scenario_link_key UNIQUE (scenario, link);


--
-- Name: scenario scenario_pkey; Type: CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.scenario
    ADD CONSTRAINT scenario_pkey PRIMARY KEY (name);


--
-- Name: scenario_pump_failure scenario_pump_failure_pkey; Type: CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.scenario_pump_failure
    ADD CONSTRAINT scenario_pump_failure_pkey PRIMARY KEY (id);


--
-- Name: scenario_pump_failure scenario_pump_failure_scenario_link_key; Type: CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.scenario_pump_failure
    ADD CONSTRAINT scenario_pump_failure_scenario_link_key UNIQUE (scenario, link);


--
-- Name: scenario_water_origin scenario_water_origin_pkey; Type: CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.scenario_water_origin
    ADD CONSTRAINT scenario_water_origin_pkey PRIMARY KEY (id);


--
-- Name: scenario_water_origin scenario_water_origin_scenario_imposed_piezometry_singulari_key; Type: CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.scenario_water_origin
    ADD CONSTRAINT scenario_water_origin_scenario_imposed_piezometry_singulari_key UNIQUE (scenario, imposed_piezometry_singularity);


--
-- Name: sectorization_settings sectorization_settings_pkey; Type: CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.sectorization_settings
    ADD CONSTRAINT sectorization_settings_pkey PRIMARY KEY (id);


--
-- Name: singularity singularity_id_name__type_key; Type: CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.singularity
    ADD CONSTRAINT singularity_id_name__type_key UNIQUE (id, name, _type);


--
-- Name: singularity singularity_pkey; Type: CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.singularity
    ADD CONSTRAINT singularity_pkey PRIMARY KEY (id);


--
-- Name: surge_tank_singularity surge_tank_singularity_pkey; Type: CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.surge_tank_singularity
    ADD CONSTRAINT surge_tank_singularity_pkey PRIMARY KEY (id);


--
-- Name: threshold_warning_settings threshold_warning_settings_pkey; Type: CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.threshold_warning_settings
    ADD CONSTRAINT threshold_warning_settings_pkey PRIMARY KEY (id);


--
-- Name: type_link type_link_pkey; Type: CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.type_link
    ADD CONSTRAINT type_link_pkey PRIMARY KEY (name, exportid, exportcode, abbreviation);


--
-- Name: type_node type_node_pkey; Type: CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.type_node
    ADD CONSTRAINT type_node_pkey PRIMARY KEY (name, exportid, exportcode, abbreviation);


--
-- Name: type_singularity type_singularity_pkey; Type: CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.type_singularity
    ADD CONSTRAINT type_singularity_pkey PRIMARY KEY (name, exportid, exportcode, abbreviation);


--
-- Name: user_node user_node_pkey; Type: CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.user_node
    ADD CONSTRAINT user_node_pkey PRIMARY KEY (id);


--
-- Name: valve_link valve_link_pkey; Type: CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.valve_link
    ADD CONSTRAINT valve_link_pkey PRIMARY KEY (id);


--
-- Name: water_delivery_point water_delivery_point_name_industrial_key; Type: CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.water_delivery_point
    ADD CONSTRAINT water_delivery_point_name_industrial_key UNIQUE (name, industrial);


--
-- Name: water_delivery_point water_delivery_point_pkey; Type: CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.water_delivery_point
    ADD CONSTRAINT water_delivery_point_pkey PRIMARY KEY (name);


--
-- Name: water_delivery_scenario_industrial_volume water_delivery_scenario_industrial_volume_pkey; Type: CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.water_delivery_scenario_industrial_volume
    ADD CONSTRAINT water_delivery_scenario_industrial_volume_pkey PRIMARY KEY (water_delivery_scenario, water_delivery_point);


--
-- Name: water_delivery_scenario water_delivery_scenario_pkey; Type: CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.water_delivery_scenario
    ADD CONSTRAINT water_delivery_scenario_pkey PRIMARY KEY (name);


--
-- Name: water_delivery_scenario_sector_setting water_delivery_scenario_sector_setting_pkey; Type: CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.water_delivery_scenario_sector_setting
    ADD CONSTRAINT water_delivery_scenario_sector_setting_pkey PRIMARY KEY (scenario, sector);


--
-- Name: water_delivery_sector water_delivery_sector_pkey; Type: CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.water_delivery_sector
    ADD CONSTRAINT water_delivery_sector_pkey PRIMARY KEY (name);


--
-- Name: link_geomidx; Type: INDEX; Schema: ___; Owner: hydra
--

CREATE INDEX link_geomidx ON ___.link USING gist (geom);


--
-- Name: node_geomidx; Type: INDEX; Schema: ___; Owner: hydra
--

CREATE INDEX node_geomidx ON ___.node USING gist (geom);


--
-- Name: water_delivery_point_geomidx; Type: INDEX; Schema: ___; Owner: hydra
--

CREATE INDEX water_delivery_point_geomidx ON ___.water_delivery_point USING gist (geom);


--
-- Name: water_delivery_point_user_nodeidx; Type: INDEX; Schema: ___; Owner: hydra
--

CREATE INDEX water_delivery_point_user_nodeidx ON ___.water_delivery_point USING btree (pipe_link);


--
-- Name: water_delivery_sector_geomidx; Type: INDEX; Schema: ___; Owner: hydra
--

CREATE INDEX water_delivery_sector_geomidx ON ___.water_delivery_sector USING gist (geom);


--
-- Name: metadata metadata_after_srid_update_trig; Type: TRIGGER; Schema: ___; Owner: hydra
--

CREATE TRIGGER metadata_after_srid_update_trig AFTER UPDATE OF srid ON ___.metadata FOR EACH ROW EXECUTE FUNCTION ___.metadata_after_srid_update_fct();


--
-- Name: air_relief_valve_singularity air_relief_valve_singularity_instead_trig; Type: TRIGGER; Schema: api; Owner: hydra
--

CREATE TRIGGER air_relief_valve_singularity_instead_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON api.air_relief_valve_singularity FOR EACH ROW EXECUTE FUNCTION api.air_relief_valve_singularity_instead_fct();


--
-- Name: check_valve_link check_valve_link_instead_trig; Type: TRIGGER; Schema: api; Owner: hydra
--

CREATE TRIGGER check_valve_link_instead_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON api.check_valve_link FOR EACH ROW EXECUTE FUNCTION api.check_valve_link_instead_fct();


--
-- Name: chlorine_injection_singularity chlorine_injection_singularity_instead_trig; Type: TRIGGER; Schema: api; Owner: hydra
--

CREATE TRIGGER chlorine_injection_singularity_instead_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON api.chlorine_injection_singularity FOR EACH ROW EXECUTE FUNCTION api.chlorine_injection_singularity_instead_fct();


--
-- Name: failure_curve failure_curve_instead_trig; Type: TRIGGER; Schema: api; Owner: hydra
--

CREATE TRIGGER failure_curve_instead_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON api.failure_curve FOR EACH ROW EXECUTE FUNCTION api.failure_curve_instead_fct();


--
-- Name: flow_injection_singularity flow_injection_singularity_instead_trig; Type: TRIGGER; Schema: api; Owner: hydra
--

CREATE TRIGGER flow_injection_singularity_instead_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON api.flow_injection_singularity FOR EACH ROW EXECUTE FUNCTION api.flow_injection_singularity_instead_fct();


--
-- Name: flow_regulator_link flow_regulator_link_instead_trig; Type: TRIGGER; Schema: api; Owner: hydra
--

CREATE TRIGGER flow_regulator_link_instead_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON api.flow_regulator_link FOR EACH ROW EXECUTE FUNCTION api.flow_regulator_link_instead_fct();


--
-- Name: fluid_properties fluid_properties_instead_trig; Type: TRIGGER; Schema: api; Owner: hydra
--

CREATE TRIGGER fluid_properties_instead_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON api.fluid_properties FOR EACH ROW EXECUTE FUNCTION api.fluid_properties_instead_fct();


--
-- Name: headloss_link headloss_link_instead_trig; Type: TRIGGER; Schema: api; Owner: hydra
--

CREATE TRIGGER headloss_link_instead_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON api.headloss_link FOR EACH ROW EXECUTE FUNCTION api.headloss_link_instead_fct();


--
-- Name: hourly_modulation_curve hourly_modulation_curve_instead_trig; Type: TRIGGER; Schema: api; Owner: hydra
--

CREATE TRIGGER hourly_modulation_curve_instead_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON api.hourly_modulation_curve FOR EACH ROW EXECUTE FUNCTION api.hourly_modulation_curve_instead_fct();


--
-- Name: imposed_piezometry_singularity imposed_piezometry_singularity_instead_trig; Type: TRIGGER; Schema: api; Owner: hydra
--

CREATE TRIGGER imposed_piezometry_singularity_instead_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON api.imposed_piezometry_singularity FOR EACH ROW EXECUTE FUNCTION api.imposed_piezometry_singularity_instead_fct();


--
-- Name: model_connection_singularity model_connection_singularity_instead_trig; Type: TRIGGER; Schema: api; Owner: hydra
--

CREATE TRIGGER model_connection_singularity_instead_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON api.model_connection_singularity FOR EACH ROW EXECUTE FUNCTION api.model_connection_singularity_instead_fct();


--
-- Name: pipe_link pipe_link_instead_trig; Type: TRIGGER; Schema: api; Owner: hydra
--

CREATE TRIGGER pipe_link_instead_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON api.pipe_link FOR EACH ROW EXECUTE FUNCTION api.pipe_link_instead_fct();


--
-- Name: pressure_accumulator_singularity pressure_accumulator_singularity_instead_trig; Type: TRIGGER; Schema: api; Owner: hydra
--

CREATE TRIGGER pressure_accumulator_singularity_instead_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON api.pressure_accumulator_singularity FOR EACH ROW EXECUTE FUNCTION api.pressure_accumulator_singularity_instead_fct();


--
-- Name: pressure_regulator_link pressure_regulator_link_instead_trig; Type: TRIGGER; Schema: api; Owner: hydra
--

CREATE TRIGGER pressure_regulator_link_instead_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON api.pressure_regulator_link FOR EACH ROW EXECUTE FUNCTION api.pressure_regulator_link_instead_fct();


--
-- Name: pump_link pump_link_instead_trig; Type: TRIGGER; Schema: api; Owner: hydra
--

CREATE TRIGGER pump_link_instead_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON api.pump_link FOR EACH ROW EXECUTE FUNCTION api.pump_link_instead_fct();


--
-- Name: reservoir_node reservoir_node_instead_trig; Type: TRIGGER; Schema: api; Owner: hydra
--

CREATE TRIGGER reservoir_node_instead_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON api.reservoir_node FOR EACH ROW EXECUTE FUNCTION api.reservoir_node_instead_fct();


--
-- Name: scenario scenario_instead_trig; Type: TRIGGER; Schema: api; Owner: hydra
--

CREATE TRIGGER scenario_instead_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON api.scenario FOR EACH ROW EXECUTE FUNCTION api.scenario_instead_fct();


--
-- Name: scenario_link_failure scenario_link_failure_instead_trig; Type: TRIGGER; Schema: api; Owner: hydra
--

CREATE TRIGGER scenario_link_failure_instead_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON api.scenario_link_failure FOR EACH ROW EXECUTE FUNCTION api.scenario_link_failure_instead_fct();


--
-- Name: scenario_pump_failure scenario_pump_failure_instead_trig; Type: TRIGGER; Schema: api; Owner: hydra
--

CREATE TRIGGER scenario_pump_failure_instead_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON api.scenario_pump_failure FOR EACH ROW EXECUTE FUNCTION api.scenario_pump_failure_instead_fct();


--
-- Name: scenario_water_origin scenario_water_origin_instead_trig; Type: TRIGGER; Schema: api; Owner: hydra
--

CREATE TRIGGER scenario_water_origin_instead_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON api.scenario_water_origin FOR EACH ROW EXECUTE FUNCTION api.scenario_water_origin_instead_fct();


--
-- Name: sectorization_settings sectorization_settings_instead_trig; Type: TRIGGER; Schema: api; Owner: hydra
--

CREATE TRIGGER sectorization_settings_instead_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON api.sectorization_settings FOR EACH ROW EXECUTE FUNCTION api.sectorization_settings_instead_fct();


--
-- Name: surge_tank_singularity surge_tank_singularity_instead_trig; Type: TRIGGER; Schema: api; Owner: hydra
--

CREATE TRIGGER surge_tank_singularity_instead_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON api.surge_tank_singularity FOR EACH ROW EXECUTE FUNCTION api.surge_tank_singularity_instead_fct();


--
-- Name: threshold_warning_settings threshold_warning_settings_instead_trig; Type: TRIGGER; Schema: api; Owner: hydra
--

CREATE TRIGGER threshold_warning_settings_instead_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON api.threshold_warning_settings FOR EACH ROW EXECUTE FUNCTION api.threshold_warning_settings_instead_fct();


--
-- Name: user_node user_node_instead_trig; Type: TRIGGER; Schema: api; Owner: hydra
--

CREATE TRIGGER user_node_instead_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON api.user_node FOR EACH ROW EXECUTE FUNCTION api.user_node_instead_fct();


--
-- Name: valve_link valve_link_instead_trig; Type: TRIGGER; Schema: api; Owner: hydra
--

CREATE TRIGGER valve_link_instead_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON api.valve_link FOR EACH ROW EXECUTE FUNCTION api.valve_link_instead_fct();


--
-- Name: water_delivery_point water_delivery_point_instead_trig; Type: TRIGGER; Schema: api; Owner: hydra
--

CREATE TRIGGER water_delivery_point_instead_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON api.water_delivery_point FOR EACH ROW EXECUTE FUNCTION api.water_delivery_point_instead_fct();


--
-- Name: water_delivery_scenario_industrial_volume water_delivery_scenario_industrial_volume_instead_trig; Type: TRIGGER; Schema: api; Owner: hydra
--

CREATE TRIGGER water_delivery_scenario_industrial_volume_instead_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON api.water_delivery_scenario_industrial_volume FOR EACH ROW EXECUTE FUNCTION api.water_delivery_scenario_industrial_volume_instead_fct();


--
-- Name: water_delivery_scenario water_delivery_scenario_instead_trig; Type: TRIGGER; Schema: api; Owner: hydra
--

CREATE TRIGGER water_delivery_scenario_instead_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON api.water_delivery_scenario FOR EACH ROW EXECUTE FUNCTION api.water_delivery_scenario_instead_fct();


--
-- Name: water_delivery_scenario_sector_setting water_delivery_scenario_sector_setting_instead_trig; Type: TRIGGER; Schema: api; Owner: hydra
--

CREATE TRIGGER water_delivery_scenario_sector_setting_instead_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON api.water_delivery_scenario_sector_setting FOR EACH ROW EXECUTE FUNCTION api.water_delivery_scenario_sector_setting_instead_fct();


--
-- Name: water_delivery_sector water_delivery_sector_instead_trig; Type: TRIGGER; Schema: api; Owner: hydra
--

CREATE TRIGGER water_delivery_sector_instead_trig INSTEAD OF INSERT OR DELETE OR UPDATE ON api.water_delivery_sector FOR EACH ROW EXECUTE FUNCTION api.water_delivery_sector_instead_fct();


--
-- Name: air_relief_valve_singularity air_relief_valve_singularity_id_name__type_fkey; Type: FK CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.air_relief_valve_singularity
    ADD CONSTRAINT air_relief_valve_singularity_id_name__type_fkey FOREIGN KEY (id, name, _type) REFERENCES ___.singularity(id, name, _type) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: cascade cascade_model_fkey; Type: FK CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.cascade
    ADD CONSTRAINT cascade_model_fkey FOREIGN KEY (model) REFERENCES ___.model(name) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: cascade cascade_scenario_fkey; Type: FK CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.cascade
    ADD CONSTRAINT cascade_scenario_fkey FOREIGN KEY (scenario) REFERENCES ___.scenario(name) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: check_valve_link check_valve_link_id_name__type_fkey; Type: FK CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.check_valve_link
    ADD CONSTRAINT check_valve_link_id_name__type_fkey FOREIGN KEY (id, name, _type) REFERENCES ___.link(id, name, _type) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: chlorine_injection_singularity chlorine_injection_singularity_id_name__type_fkey; Type: FK CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.chlorine_injection_singularity
    ADD CONSTRAINT chlorine_injection_singularity_id_name__type_fkey FOREIGN KEY (id, name, _type) REFERENCES ___.singularity(id, name, _type) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: chlorine_injection_singularity chlorine_injection_singularity_target_node_fkey; Type: FK CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.chlorine_injection_singularity
    ADD CONSTRAINT chlorine_injection_singularity_target_node_fkey FOREIGN KEY (target_node) REFERENCES ___.node(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: flow_injection_singularity flow_injection_singularity_id_name__type_fkey; Type: FK CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.flow_injection_singularity
    ADD CONSTRAINT flow_injection_singularity_id_name__type_fkey FOREIGN KEY (id, name, _type) REFERENCES ___.singularity(id, name, _type) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: flow_regulator_link flow_regulator_link_id_name__type_fkey; Type: FK CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.flow_regulator_link
    ADD CONSTRAINT flow_regulator_link_id_name__type_fkey FOREIGN KEY (id, name, _type) REFERENCES ___.link(id, name, _type) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: groupe_model groupe_model_groupe_fkey; Type: FK CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.groupe_model
    ADD CONSTRAINT groupe_model_groupe_fkey FOREIGN KEY (groupe) REFERENCES ___.groupe(name) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: groupe_model groupe_model_model_fkey; Type: FK CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.groupe_model
    ADD CONSTRAINT groupe_model_model_fkey FOREIGN KEY (model) REFERENCES ___.model(name) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: headloss_link headloss_link_id_name__type_fkey; Type: FK CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.headloss_link
    ADD CONSTRAINT headloss_link_id_name__type_fkey FOREIGN KEY (id, name, _type) REFERENCES ___.link(id, name, _type) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: imposed_piezometry_singularity imposed_piezometry_singularity_id_name__type_fkey; Type: FK CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.imposed_piezometry_singularity
    ADD CONSTRAINT imposed_piezometry_singularity_id_name__type_fkey FOREIGN KEY (id, name, _type) REFERENCES ___.singularity(id, name, _type) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: link link_down_fkey; Type: FK CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.link
    ADD CONSTRAINT link_down_fkey FOREIGN KEY (down) REFERENCES ___.node(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: link link_up_fkey; Type: FK CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.link
    ADD CONSTRAINT link_up_fkey FOREIGN KEY (up) REFERENCES ___.node(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: metadata metadata_srid_fkey; Type: FK CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.metadata
    ADD CONSTRAINT metadata_srid_fkey FOREIGN KEY (srid) REFERENCES public.spatial_ref_sys(srid);


--
-- Name: mixed mixed_groupe_fkey; Type: FK CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.mixed
    ADD CONSTRAINT mixed_groupe_fkey FOREIGN KEY (groupe) REFERENCES ___.groupe(name) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: mixed mixed_scenario_fkey; Type: FK CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.mixed
    ADD CONSTRAINT mixed_scenario_fkey FOREIGN KEY (scenario) REFERENCES ___.scenario(name) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: model_connection_singularity model_connection_singularity_id_name__type_fkey; Type: FK CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.model_connection_singularity
    ADD CONSTRAINT model_connection_singularity_id_name__type_fkey FOREIGN KEY (id, name, _type) REFERENCES ___.singularity(id, name, _type) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: node node_model_fkey; Type: FK CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.node
    ADD CONSTRAINT node_model_fkey FOREIGN KEY (model) REFERENCES ___.model(name) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: pipe_link pipe_link_id_name__type_fkey; Type: FK CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.pipe_link
    ADD CONSTRAINT pipe_link_id_name__type_fkey FOREIGN KEY (id, name, _type) REFERENCES ___.link(id, name, _type) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: pipe_link pipe_link_material_fkey; Type: FK CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.pipe_link
    ADD CONSTRAINT pipe_link_material_fkey FOREIGN KEY (material) REFERENCES ___.material(name) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: pipe_link_singularity pipe_link_singularity_pipe_link_fkey; Type: FK CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.pipe_link_singularity
    ADD CONSTRAINT pipe_link_singularity_pipe_link_fkey FOREIGN KEY (pipe_link) REFERENCES ___.pipe_link(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: pressure_accumulator_singularity pressure_accumulator_singularity_id_name__type_fkey; Type: FK CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.pressure_accumulator_singularity
    ADD CONSTRAINT pressure_accumulator_singularity_id_name__type_fkey FOREIGN KEY (id, name, _type) REFERENCES ___.singularity(id, name, _type) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: pressure_regulator_link pressure_regulator_link_id_name__type_fkey; Type: FK CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.pressure_regulator_link
    ADD CONSTRAINT pressure_regulator_link_id_name__type_fkey FOREIGN KEY (id, name, _type) REFERENCES ___.link(id, name, _type) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: pump_link pump_link_id_name__type_fkey; Type: FK CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.pump_link
    ADD CONSTRAINT pump_link_id_name__type_fkey FOREIGN KEY (id, name, _type) REFERENCES ___.link(id, name, _type) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: pump_link pump_link_target_node_fkey; Type: FK CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.pump_link
    ADD CONSTRAINT pump_link_target_node_fkey FOREIGN KEY (target_node) REFERENCES ___.node(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: regulation_scenario regulation_scenario_scenario_fkey; Type: FK CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.regulation_scenario
    ADD CONSTRAINT regulation_scenario_scenario_fkey FOREIGN KEY (scenario) REFERENCES ___.scenario(name) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: reservoir_node reservoir_node_id_name__type_fkey; Type: FK CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.reservoir_node
    ADD CONSTRAINT reservoir_node_id_name__type_fkey FOREIGN KEY (id, name, _type) REFERENCES ___.node(id, name, _type) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: scenario_link_failure scenario_link_failure_failure_curve_fkey; Type: FK CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.scenario_link_failure
    ADD CONSTRAINT scenario_link_failure_failure_curve_fkey FOREIGN KEY (failure_curve) REFERENCES ___.failure_curve(name) ON UPDATE CASCADE;


--
-- Name: scenario_link_failure scenario_link_failure_link_link_type_fkey; Type: FK CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.scenario_link_failure
    ADD CONSTRAINT scenario_link_failure_link_link_type_fkey FOREIGN KEY (link, link_type) REFERENCES ___.link(id, _type) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: scenario_link_failure scenario_link_failure_scenario_fkey; Type: FK CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.scenario_link_failure
    ADD CONSTRAINT scenario_link_failure_scenario_fkey FOREIGN KEY (scenario) REFERENCES ___.scenario(name) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: scenario_pump_failure scenario_pump_failure_failure_curve_fkey; Type: FK CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.scenario_pump_failure
    ADD CONSTRAINT scenario_pump_failure_failure_curve_fkey FOREIGN KEY (failure_curve) REFERENCES ___.failure_curve(name) ON UPDATE CASCADE;


--
-- Name: scenario_pump_failure scenario_pump_failure_link_link_type_fkey; Type: FK CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.scenario_pump_failure
    ADD CONSTRAINT scenario_pump_failure_link_link_type_fkey FOREIGN KEY (link, link_type) REFERENCES ___.link(id, _type) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: scenario_pump_failure scenario_pump_failure_scenario_fkey; Type: FK CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.scenario_pump_failure
    ADD CONSTRAINT scenario_pump_failure_scenario_fkey FOREIGN KEY (scenario) REFERENCES ___.scenario(name) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: scenario scenario_scenario_ref_fkey; Type: FK CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.scenario
    ADD CONSTRAINT scenario_scenario_ref_fkey FOREIGN KEY (scenario_ref) REFERENCES ___.scenario(name) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: scenario scenario_scenario_rstart_fkey; Type: FK CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.scenario
    ADD CONSTRAINT scenario_scenario_rstart_fkey FOREIGN KEY (scenario_rstart) REFERENCES ___.scenario(name) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: scenario scenario_water_delivery_scenario_fkey; Type: FK CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.scenario
    ADD CONSTRAINT scenario_water_delivery_scenario_fkey FOREIGN KEY (water_delivery_scenario) REFERENCES ___.water_delivery_scenario(name) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: scenario_water_origin scenario_water_origin_imposed_piezometry_singularity_fkey; Type: FK CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.scenario_water_origin
    ADD CONSTRAINT scenario_water_origin_imposed_piezometry_singularity_fkey FOREIGN KEY (imposed_piezometry_singularity) REFERENCES ___.imposed_piezometry_singularity(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: scenario_water_origin scenario_water_origin_scenario_fkey; Type: FK CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.scenario_water_origin
    ADD CONSTRAINT scenario_water_origin_scenario_fkey FOREIGN KEY (scenario) REFERENCES ___.scenario(name) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: singularity singularity_id_fkey; Type: FK CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.singularity
    ADD CONSTRAINT singularity_id_fkey FOREIGN KEY (id) REFERENCES ___.node(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: surge_tank_singularity surge_tank_singularity_id_name__type_fkey; Type: FK CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.surge_tank_singularity
    ADD CONSTRAINT surge_tank_singularity_id_name__type_fkey FOREIGN KEY (id, name, _type) REFERENCES ___.singularity(id, name, _type) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: user_node user_node_domestic_curve_fkey; Type: FK CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.user_node
    ADD CONSTRAINT user_node_domestic_curve_fkey FOREIGN KEY (domestic_curve) REFERENCES ___.hourly_modulation_curve(name) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: user_node user_node_id_name__type_fkey; Type: FK CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.user_node
    ADD CONSTRAINT user_node_id_name__type_fkey FOREIGN KEY (id, name, _type) REFERENCES ___.node(id, name, _type) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: user_node user_node_industrial_curve_fkey; Type: FK CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.user_node
    ADD CONSTRAINT user_node_industrial_curve_fkey FOREIGN KEY (industrial_curve) REFERENCES ___.hourly_modulation_curve(name) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: valve_link valve_link_id_name__type_fkey; Type: FK CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.valve_link
    ADD CONSTRAINT valve_link_id_name__type_fkey FOREIGN KEY (id, name, _type) REFERENCES ___.link(id, name, _type) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: water_delivery_point water_delivery_point_pipe_link_fkey; Type: FK CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.water_delivery_point
    ADD CONSTRAINT water_delivery_point_pipe_link_fkey FOREIGN KEY (pipe_link) REFERENCES ___.pipe_link(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: water_delivery_scenario_industrial_volume water_delivery_scenario_indus_water_delivery_point_industr_fkey; Type: FK CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.water_delivery_scenario_industrial_volume
    ADD CONSTRAINT water_delivery_scenario_indus_water_delivery_point_industr_fkey FOREIGN KEY (water_delivery_point, industrial) REFERENCES ___.water_delivery_point(name, industrial) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: water_delivery_scenario_industrial_volume water_delivery_scenario_industrial_water_delivery_scenario_fkey; Type: FK CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.water_delivery_scenario_industrial_volume
    ADD CONSTRAINT water_delivery_scenario_industrial_water_delivery_scenario_fkey FOREIGN KEY (water_delivery_scenario) REFERENCES ___.water_delivery_scenario(name) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: water_delivery_scenario_sector_setting water_delivery_scenario_sector_setting_scenario_fkey; Type: FK CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.water_delivery_scenario_sector_setting
    ADD CONSTRAINT water_delivery_scenario_sector_setting_scenario_fkey FOREIGN KEY (scenario) REFERENCES ___.water_delivery_scenario(name) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: water_delivery_scenario_sector_setting water_delivery_scenario_sector_setting_sector_fkey; Type: FK CONSTRAINT; Schema: ___; Owner: hydra
--

ALTER TABLE ONLY ___.water_delivery_scenario_sector_setting
    ADD CONSTRAINT water_delivery_scenario_sector_setting_sector_fkey FOREIGN KEY (sector) REFERENCES ___.water_delivery_sector(name) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--
