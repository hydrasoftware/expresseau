import sys
import os
import re

def is_float(s):
    try:
        float(s)
        return True
    except ValueError:
        return False

trad = {
    ' LT ' : ' BELOW ',
    ' GT ' : ' ABOVE ',
    ' THEN ' : '\nTHEN ',
    '#TMOD24 ( TIME )' : 'SYSTEM CLOCKTIME',
    "'" : "",
    'CANA' : 'PIPE',
    'VANN' : 'VALVE',
    'REGP' : 'VALVE',
    'REGQ' : 'VALVE',
    'POMP' : 'PUMP',
    'CHAT' : 'TANK',
    'CLPT' : 'PIPE',
    }

def invert(c):
    "invert a ctl condition BELOW <-> ABOVE"
    return re.sub(r'\b(BELOW|ABOVE)\b', lambda x: 'ABOVE' if x.group()=='BELOW' else 'BELOW', c)


def unnest(block, variables, rule_id, feedback, project, conditions):
    "tranform nested if/elsif/else nested statements to single condition with and/not"
    assert block[0].startswith('IF')
    assert block[-1] == 'ENDIF'

    # extract condition from if/elsif line
    condition = lambda x: x.split('(')[1].split(')')[0].strip()

    for k, v in list(reversed(variables)) + list(trad.items()):
        block[0] = block[0].replace(k, v)

    conds = [condition(block[0])]

    txt = ';'+block[0]+'\n'

    blk = []
    nesting = 0
    for l in block[1:-1]:

        if l=='' and nesting == 0:
            if len(blk):
                t, rule_id = translate_block(blk, variables, rule_id, feedback, project, conditions + tuple(conds))
                txt += t
            blk = []
        elif l.startswith('ELSE') and nesting==0:
            if len(blk):
                t, rule_id = translate_block(blk, variables, rule_id, feedback, project, conditions + tuple(conds))
                txt += t
            txt += ';'+l+'\n'
            blk = []
            conds[-1] = invert(conds[-1])
            if l.startswith('ELSEIF'):
                for k, v in list(reversed(variables)) + list(trad.items()):
                    l = l.replace(k, v)
                conds.append(condition(l))
        elif l.startswith('ENDIF'):
            nesting -= 1
            blk.append(l)
        elif l.startswith('IF') and l.endswith('THEN'):
            nesting +=1
            blk.append(l)
        else:
            blk.append(l)

    if len(blk):
        t, rule_id = translate_block(blk, variables, rule_id, feedback, project, conditions + tuple(conds))
        txt += t

    txt += ';'+block[-1]+'\n'

    return txt, rule_id


def translate_block(block, variables, rule_id, feedback, project, conditions=tuple()):

    if block[0].startswith('IF'):
        #feedback.pushWarning(f"Attention, les conditions imbriquées ne sont pas traduite: section commançant par: {block[0]}")
        return unnest(block, variables, rule_id, feedback, project, conditions)
    else:
        assert isinstance(rule_id, int)
        elm_name = block[0].split()[-1].replace("'", "")
        elm_type = trad[block[0].split()[-2]]
        skip = []
        for i in range(1, len(block)):
            for k, v in list(reversed(variables)) + list(trad.items()):
                block[i] = block[i].replace(k, v)
            block[i] = re.sub(r'\b(0|1)\.( |$)', r'\1 ', block[i])

            block[i] = block[i].replace('( ', ' ').replace(' )', ' ').replace('  ', ' ')

            if (block[i].find('ABOVE') != -1 or block[i].find('BELOW') != -1) and not is_float(re.split('ABOVE|BELOW', block[i].split('\n')[0])[1]):
                feedback.pushWarning("Attention, la comparaison de deux grandeurs n'est pas gérée par EPANET :")
                feedback.pushWarning(block[i]+'\n')
                #block[i] = ';' + block[i] #TODO écrire quand-même mais en commentaires
                skip.append(i)
                continue

            if block[i].find('TANK') != -1:
                block[i] = block[i].replace('BELOW', 'LEVEL BELOW').replace('ABOVE', 'LEVEL ABOVE')

            for k in ('QCONS', 'HCONS', 'ALPR', 'ALPS') :
                for v, s in (('1 ', 'OPEN'), ('0 ', 'CLOSED')):
                    block[i] = block[i].replace(f'{k} {v}', f'{elm_type} {elm_name}_reg STATUS IS {s}')
                if k in ('QCONS', 'HCONS'):
                    block[i] = block[i].replace(f'{k} ', f'VALVE {elm_name}_reg SETTING IS ')

                if block[i].find(f'{k}') != -1: #s'il en reste encore c'est qu'il y a une valeur derrière différente de 1 ou 0
                    feedback.pushWarning("Attention, la consigne suivante n'est pas gérée par EPANET : ")
                    feedback.pushWarning(f'-> {k} différent de 0 ou 1')
                    feedback.pushWarning(block[i]+'\n')
                    skip.append(i)
            if block[i].find('CLOCKTIME ABOVE -') != -1:
                skip.append(i)
            if block[i].find('DEFAUT') != -1:
                feedback.pushWarning('Attention, les consignes DEFAUT ne sont pas gérées par EPANET')
                feedback.pushWarning(block[i]+'\n')
                skip.append(i)

            # remplacement des côtes de chateau par des hauteurs depuis zground
            m = re.search(r'IF +TANK +([^ ]+) +LEVEL (ABOVE|BELOW) +([^ ]+)', block[i])
            if m:
                zground, = project.fetchone("select zground from api.reservoir_node where name=%s", (m.group(1),))
                block[i] = re.sub(r'(IF +TANK +[^ ]+ +LEVEL (ABOVE|BELOW)) +([^ ]+)', f"\\1 {float(m.group(3)) - zground:.2f}", block[i])

            block[i] = block[i].replace('THEN ', ''.join([f'AND {c}\n' for c in conditions])+'THEN ')
            if block[i].find('IF')==-1:
                if len(conditions):
                    block[i] = 'IF '+''.join([f'AND {c}\n' for c in conditions])+'THEN '+block[i]
                else:
                    block[i] = 'IF CLOCKTIME > 0 AM\nTHEN '+block[i]

        for i in reversed(skip):
            del block[i]

        return ''.join([f'RULE {i + rule_id}\n{r}\nPRIORITY {i + rule_id}\n\n' for i, r in enumerate(block[1:])]), rule_id + len(block)-1



def export_(project, model, ctl_file, inp_file, feedback, enc='utf8'):
    with open(inp_file, 'w', encoding='utf8') as inp:
        inp.write(export_inp_rules(project, model, ctl_file, feedback, enc))

def export_inp_rules(project, model, ctl_file, feedback, enc='utf8'):
    inp = ''
    if not os.path.isfile(ctl_file):
        pass
    else:
        with open(ctl_file, 'r', encoding=enc) as ctl:
            in_section = False
            block = []
            variables = []
            nesting = 0
            last_comment=''
            rule_no = 0
            for line in ctl:
                line = re.sub(r' +', ' ', line).strip()

                line = re.sub(r'\b(IF|THEN|ELSE|ELESIF|SET|CASE|CALCUL|LT|GT|CANA|VANN|REGP|REGQ|POMP|CHAT|CLPT|DEBUG)\b', lambda x: x.group().upper(), line, flags=re.I)

                m = re.search(r'\$ CASE \((.*)\)', line, re.I)
                if m:
                    if m.group(1)==model:
                        in_section = True
                    else:
                        in_section = False
                elif in_section:
                    if line.startswith('!'):
                        line = line.replace('!', ';')
                        last_comment = line
                        inp += line + '\n'
                    elif line.startswith('SET'):
                        n, v = line[3:].split('=')
                        if v.find('%') != -1:
                            feedback.pushWarning("Attention, les variables établies sur une autre variable ne sont pas gérées dans les RULES d'EPANET")
                            feedback.pushWarning(line[3:])
                            assert False #translation of a set with another variable not implementad (not possible in epanet)
                        variables.append((n.strip(), v.strip()))
                    elif line.startswith('CALCUL'):
                        n,  v = line[6:].split('=')
                        for k, r in reversed(variables):
                            v = v.replace(k, r)
                        if not v.strip()=='#TMOD24 ( TIME )':
                            feedback.pushWarning("Attention, les variables calculées ne sont pas gérées dans les RULES d'EPANET")
                            feedback.pushWarning('La seule fonction traduite est #TMOD24 appliquée à TIME')
                            feedback.pushWarning(line)
                            assert False #translation of CALCUL not implementad (not possible in epanet)
                        variables.append((n.strip(), v.strip()))
                    elif line.startswith('DEBUG'):
                        pass
                    elif line.startswith('IF') and line.endswith('THEN'):
                        nesting += 1
                        block.append(line)
                    elif line.startswith('ENDIF'):
                        nesting -= 1
                        block.append(line)
                    elif line=='' and nesting==0:
                        in_block = False
                        if len(block):
                            if last_comment.startswith('!RULE'):
                                rule_id = last_comment[6:]
                                txt, rule_id = translate_block(block, variables, rule_id, feedback, project)
                            else:
                                rule_no += 1
                                txt, rule_no = translate_block(block, variables, rule_no, feedback, project)
                            inp += txt
                            block = []
                    else:
                        in_block = True
                        block.append(line)
    return inp


if __name__=='__main__':
    import sys
    from expresseau.project import Project
    from expresseau.utility.log import Feedback
    import getopt

    optlist, args = getopt.getopt(sys.argv[1:], 'e:', ["encoding="])
    optlist = dict(optlist)
    project_name, model_name, ctl_file, inp_file = args[-4:]
    enc = optlist['-e'] if '-e' in optlist else 'utf-8'
    export_(Project(project_name), model_name, ctl_file, inp_file, Feedback(), enc)

    #python -m ctl_to_inp projet_name model_name ***.ctl ***.inp
    #python -m ctl_to_inp -e ANSI projet_name model_name ***.ctl ***.inp