"""
export epanet inp file from expresso models

export_inp  project_name model output.inp
"""

import re
from math import pi, log, pow, sqrt
from collections import defaultdict
from  .ctl_to_inp import export_inp_rules
from string import Template
import os

inp_template = """[TITLE]
$title

[JUNCTIONS]
;ID                        Elev         Demand          Pattern
$junctions

[RESERVOIRS]
;ID                        Head         Pattern
$reservoirs

[TANKS]
;ID                        Elevation    InitLevel    MinLevel     MaxLevel     Diameter     MinVol       VolCurve
$tanks

[PIPES]
;ID                        Node1                     Node2                     Length       Diameter     Roughness    MinorLoss    Status
$pipes

[PUMPS]
;ID                        Node1                     Node2                     Parameters
$pumps

[VALVES]
;ID                        Node1                     Node2                     Diameter  Type Setting      MinorLoss
$valves

[DEMANDS]
;Junction                  Demand       Pattern          Category
$demands

[STATUS]
;ID                        Status/Setting
$status

[EMITTERS]
;Junction                  Coefficient

[CURVES]
;ID                        X-Value      Y-Value
$curves

[PATTERNS]
;ID                        Multipliers
$patterns

[ENERGY]

[CONTROLS]

[RULES]
$rules

[QUALITY]
;Node                      InitQual

[REACTIONS]
;Type      Pipe/Tank        Coefficient

[SOURCES]
;Node             Type         Quality      Pattern

[MIXING]
;Tank             Model

[OPTIONS]
;Option    Value
$options

[TIMES]
$times

[REPORT]

[COORDINATES]
;Node                      X-Coord          Y-Coord
$coordinates

[VERTICES]
;Link                      X-Coord          Y-Coord
$vertices

[LABELS]
;X-Coord           Y-Coord          Label & Anchor Node

[BACKDROP]

[TAGS]

[END]
"""

def export_(path, project, model, feedback, scenario=None):
    feedback.pushWarning(f"Démarrage de l'export en INP")
    warnings = 0
    errors = 0
    issues = project.fetchall("""
        select name from api.hourly_modulation_curve where length(name) > 15
        """)
    if len(issues):
        feedback.reportError("some entity names are too long: {', '.join([n for n, in issues])}")

    assert(len(issues) == 0)

    title = project.name+' '+model+' '+(scenario or '') #project.fetchone("select comment from api.model where name=%s", (model,))

    feedback.pushInfo(f'''{title} \n''')

    if scenario is not None:
        #vérif accessibilité du/des ctl
        for ctl, in project.fetchall(f"select regulation_file from api.regulation_scenario where scenario='{scenario}' order by priority desc"):
            if not os.path.isfile(ctl):
                feedback.pushWarning(f"No such file '{ctl}'. Export model without controls.")
                warnings += 1

    junctions, = project.fetchone(f"""
        select string_agg(blk, '\n') from (
        select string_agg(format(' %-25s %-12s %-12s                   ;%s',
                n.name,
                (zground-depth)::numeric(12,2),
                0,
                n.comment),
                '\n' order by id) as blk
        from api.user_node n
        left join api._user_node_affected_volume aff on aff._user_node=n.id
        where model='{model}'
        and id not in (
            select id from api.imposed_piezometry_singularity
            union all
            select id from api.model_connection_singularity where cascade_mode='Imposed piezometry Z(t)'
            )
        union all
        -- intermediate node for regulated pumps
        select string_agg(format(' %-25s %-12s %-12s                   ;%s',
                p.id||'_midle',
                case when p.pump_regulation_option = 'Flow regulation' then
                    (u.zground-u.depth)::numeric(12,2)
                else
                    (d.zground-d.depth)::numeric(12,2)
                end,
                0,
                'intermediate node for pump '||p.name||' with '||p.pump_regulation_option),
                '\n') as blk
        from api.pump_link p
        join api.user_node u on u.id=p.up
        join api.user_node d on d.id=p.down
        where u.model='{model}' and p.pump_regulation_option in ('Flow regulation', 'Pressure regulation')
        ) t
        """)

    if scenario is not None:
        times, = project.fetchone(f"""
            select
            format(' %-25s %4s:%2s\n', 'Duration', EXTRACT(epoch FROM duration)/3600, LPAD(EXTRACT(MIN FROM duration)::varchar, 2, '0'))||
            format(' %-25s %4s:%2s\n', 'Hydraulic Timestep', LPAD((timestep::integer/3600)::varchar, 2, '0'), LPAD(((timestep::integer - 3600*(timestep::integer/3600))/60)::varchar, 2, '0'))||
            format(' %-25s %4s:%2s\n', 'Pattern Timestep', '01', '00')||
            format(' %-25s %4s:%2s\n', 'Report Timestep', LPAD((dt_output::integer/3600)::varchar, 2, '0'), LPAD(((dt_output::integer - 3600*(dt_output::integer/3600))/60)::varchar, 2, '0'))||
            format(' %-25s %4s:%2s\n', 'Report Start', LPAD((EXTRACT(epoch FROM tbegin_output_hr)/3600)::varchar, 2, '0'), LPAD(EXTRACT(MIN FROM tbegin_output_hr)::varchar, 2, '0'))||
            format(' %-25s %4s:%2s\n', 'Start ClockTime', LPAD(EXTRACT(HOUR FROM starting_date)::varchar, 2, '0'), LPAD(EXTRACT(MIN FROM starting_date)::varchar, 2, '0'))
            from api.scenario
            where name='{scenario}'
            """)

        times = times.replace(' Report Timestep             00:00', 'Report Timestep             00:01')

        demands, needed_pattern = project.fetchone(f"""
            with demand as (
                with wd_scn as (
                    select water_delivery_scenario as name
                    from api.scenario
                    where name='{scenario}'
                ),
                sectors as (
                    select s.name, n.model, sum(_domestic_volume)::numeric(12,3) as total_volume_dom --, wds.volume_m3j*wds.adjust_coef/wds.leak_efficiency volume_m3j
                    from api.water_delivery_sector as s
                    join api.user_node as n on s.name=n._sector
                    left join api._user_node_affected_volume as v on v._user_node=n.id
                    group by s.name, n.model--, wds.volume_m3j, wds.adjust_coef, wds.leak_efficiency
                )
                select id,
                round(coalesce(case when s.total_volume_dom != 0 then coalesce(v._domestic_volume, 0)/s.total_volume_dom else 0 end, 0)::numeric, 12)*wds.volume_m3j*wds.adjust_coef/(wds.leak_efficiency*24) as volume,
                coalesce(n.domestic_curve, '      ') as pattern, n.name
                from wd_scn
                join api.user_node as n on true
                left join sectors as s on s.name=n._sector and s.model=n.model
                left join api._user_node_affected_volume as v on v._user_node=n.id
                left join api.water_delivery_scenario_sector_setting wds on wds.scenario=wd_scn.name and wds.sector=s.name
                where n.model='{model}'

                union all

                select n.id, sum(wdi.volume * wdi.industrial::integer) as vol, industrial_curve as patter, n.name
                from api.scenario s
                join api.water_delivery_scenario wds on wds.name=s.water_delivery_scenario
                join api.water_delivery_scenario_industrial_volume as wdi on wdi.water_delivery_scenario=wds.name
                join api.water_delivery_point as wdp on wdp.name=wdi.water_delivery_point
                join api.user_node n on n.id=wdp._node
                where n.model='{model}' and s.name='{scenario}'
                and n.id not in (
                    select id from api.imposed_piezometry_singularity
                    union all
                    select id from api.model_connection_singularity where cascade_mode='Imposed piezometry Z(t)'
                    )
                group by n.id, n.name, n.industrial_curve

                union all

                select id, q0 as volume, null as pattern, name
                from api.user_node
                where model='{model}'
                and id not in (
                    select id from api.imposed_piezometry_singularity
                    union all
                    select id from api.model_connection_singularity where cascade_mode='Imposed piezometry Z(t)'
                    )

                union all

                select n.id, 1 as volume, 'Q'||s.id||'_curve' as pattern, n.name
                from ___.model_connection_singularity s
                join ___.node n on n.id = s.id
                where n.model='{model}'
                and s.cascade_mode='Delivery Q(t)'

                union all

                select n.id, case when external_file then -1 else -s.q0 end as volume, case when external_file then 'Q'||s.id||'_curve' end as pattern, n.name
                from ___.flow_injection_singularity s
                join ___.node n on n.id = s.id
                where n.model='{model}'
            )
            select string_agg(format(' %-25s %-12s %-17s',
                    name,
                    volume::numeric(12,3),
                    coalesce(pattern, '                 ')),
                    '\n' order by id), array_agg(pattern)
            from demand where volume != 0
            """)

    else:
        demands, needed_pattern = '', None
        times = ''

    needed_pattern = set(needed_pattern if needed_pattern is not None else [])


    reservoirs, = project.fetchone(f"""
        select string_agg(format(' %-25s %-12s %-12s            ; %s', name, z0, pattern, 'node '||name||' '||coalesce(comment, '')),
            '\n' order by name)
        from (
            select
                s.name,
                case when s.external_file then 1 else s.z0 end as z0,
                case when s.external_file then 'P'||s.id||'_curve' else '' end as pattern,
                s.comment
            from api.imposed_piezometry_singularity s
            join ___.node n on n.id=s.id
            where n.model='{model}'
            union all
            select s.name, 1 as z0, 'P'||s.id||'_curve' as pattern, s.comment
            from api.model_connection_singularity s
            where _model='{model}' and cascade_mode='Imposed piezometry Z(t)'
            ) as t

        """)

    tanks, = project.fetchone(f"""
        select string_agg(format(' %-25s %-12s %-12s %-12s %-12s %-12s %-12s %-17s; %s',
            name,
            zground::numeric(12,2),
            (z_ini-zground)::numeric(12,2),
            ((sz_array::real[])[1][1] - zground)::numeric(12,2),
            ((sz_array::real[])[array_length(sz_array::real[], 1)][1] - zground)::numeric(12,2),
            case when array_length(sz_array::real[], 1)=2 and (sz_array::real[])[1][2]=(sz_array::real[])[2][2]
                then (2*sqrt((sz_array::real[])[1][2]/pi()))::numeric(12,2)
                else 0.01
                end,
            0.0,
            case when array_length(sz_array::real[], 1)=2 and (sz_array::real[])[1][2]=(sz_array::real[])[2][2]
                then ''
                else 'N'||id||'_curve'
                end,
            comment),
            '\n' order by id)
        from api.reservoir_node
        where model='{model}'
        """)

    pipes, = project.fetchone(f"""
        with node as (
            select id, name from ___.node where model='{model}'
        )
        select string_agg(blk, '\n') from (
        select string_agg(format(' %-25s %-25s %-25s %-12s %-12s %-12s %-12s %-17s ; %s',
            p.name,
            coalesce(us.name, uss.name, u.name),
            coalesce(ds.name, dss.name, d.name),
            (case when overload_length then overloaded_length else st_length(geom) end)::numeric(16,2),
            round(p.diameter*1000),
            case when overload_roughness_mm then overloaded_roughness_mm else _roughness_mm end,
            0.0,
            case when status_open then 'Open' else 'Closed' end,
            comment),
            '\n' order by p.id) as blk
        from api.pipe_link p
        join node u on u.id = p.up
        join node d on d.id = p.down
        left join ___.imposed_piezometry_singularity us on us.id=u.id
        left join ___.imposed_piezometry_singularity ds on ds.id=d.id
        left join ___.model_connection_singularity uss on uss.id=u.id and uss.cascade_mode='Imposed piezometry Z(t)'
        left join ___.model_connection_singularity dss on dss.id=d.id and dss.cascade_mode='Imposed piezometry Z(t)'
        union all
        select string_agg(format(' %-25s %-25s %-25s %-12s %-12s %-12s %-12s %-17s ; %s',
            p.name,
            coalesce(us.name, u.name),
            coalesce(ds.name, d.name),
            1e-6,
            round(2*sqrt(p.section/pi())*1000),
            1e-6,
            headloss_coef,
            'CV',
            comment),
            '\n' order by p.id) as blk
        from api.check_valve_link p
        join node u on u.id = p.up
        join node d on d.id = p.down
        left join ___.imposed_piezometry_singularity us on us.id=u.id
        left join ___.imposed_piezometry_singularity ds on ds.id=d.id
        left join ___.model_connection_singularity uss on uss.id=u.id and uss.cascade_mode='Imposed piezometry Z(t)'
        left join ___.model_connection_singularity dss on dss.id=d.id and dss.cascade_mode='Imposed piezometry Z(t)'
        union all
        select string_agg(format(' %-25s %-25s %-25s %-12s %-12s %-12s %-12s %-17s ; %s',
            p.name,
            coalesce(us.name, u.name),
            coalesce(ds.name, d.name),
            1e-6,
            round(2*sqrt(p.section/pi())*1000),
            1e-6,
            rk1,
            'CV',
            comment),
            '\n' order by p.id) as blk
        from api.headloss_link p
        join node u on u.id = p.up
        join node d on d.id = p.down
        left join ___.imposed_piezometry_singularity us on us.id=u.id
        left join ___.imposed_piezometry_singularity ds on ds.id=d.id
        left join ___.model_connection_singularity uss on uss.id=u.id and uss.cascade_mode='Imposed piezometry Z(t)'
        left join ___.model_connection_singularity dss on dss.id=d.id and dss.cascade_mode='Imposed piezometry Z(t)'
        union all
        select string_agg(format(' %-25s %-25s %-25s %-12s %-12s %-12s %-12s %-17s ; %s',
            p.name||'_rev',
            coalesce(ds.name, d.name),
            coalesce(us.name, u.name),
            1e-6,
            round(2*sqrt(p.section/pi())*1000),
            1e-6,
            rk2,
            'CV',
            comment),
            '\n' order by p.id) as blk
        from api.headloss_link p
        join node u on u.id = p.up
        join node d on d.id = p.down
        left join ___.imposed_piezometry_singularity us on us.id=u.id
        left join ___.imposed_piezometry_singularity ds on ds.id=d.id
        left join ___.model_connection_singularity uss on uss.id=u.id and uss.cascade_mode='Imposed piezometry Z(t)'
        left join ___.model_connection_singularity dss on dss.id=d.id and dss.cascade_mode='Imposed piezometry Z(t)'
        ) as t
        """)


    pumps, = project.fetchone(f"""
        with node as (
            select id, name from ___.node where model='{model}'
        )
        select string_agg(format(' %-25s %-25s %-25s HEAD %-17s ;%s',
            p.name,
            case when p.pump_regulation_option = 'Flow regulation' then
                p.id||'_midle'
            else
                coalesce(us.name, uss.name, u.name)
            end,
            case when p.pump_regulation_option = 'Pressure regulation' then
                p.id||'_midle'
            else
                coalesce(ds.name, dss.name, d.name)
            end,
            'L'||p.id||'_curve',
            p.comment),
            '\n' order by p.name)
        from api.pump_link p
        join node u on u.id = p.up
        join node d on d.id = p.down
        left join ___.imposed_piezometry_singularity us on us.id=u.id
        left join ___.imposed_piezometry_singularity ds on ds.id=d.id
        left join ___.model_connection_singularity uss on uss.id=u.id and uss.cascade_mode='Imposed piezometry Z(t)'
        left join ___.model_connection_singularity dss on dss.id=d.id and dss.cascade_mode='Imposed piezometry Z(t)'
        """)

    status, = project.fetchone(f"select string_agg(format(' %-25s CLOSED', p.name), '\n' order by p.name) from api.pump_link p join ___.node u on u.id = p.up where u.model='{model}'")

    rules = ''
    # TODO adds rules for pumps
    for pump, z_start, z_stop, target, target_type in project.fetchall(f"""
                select p.name, p.regul_z_start, p.regul_z_stop, t.name, case when t._type='reservoir' then 'TANK' else 'JUNCTIONS' end
                from api.pump_link p
                join ___.node n on n.id=p.up
                join ___.node t on t.id = p.target_node
                where n.model='{model}' and p.pump_regulation_option='Stop-start regulation' """):
        if target_type=='TANK':
            target_zg, = project.fetchone(f"""
                    select zground from api.reservoir_node where name='{target}' """)
            z_start = z_start - target_zg
            z_stop = z_stop - target_zg
        rules += (
            f'RULE START {pump}\n'
            f'IF {target_type} {target} LEVEL BELOW {z_start}\n'
            f'THEN PUMP {pump} STATUS IS OPEN\n\n'
            f'RULE STOP {pump}\n'
            f'IF {target_type} {target} LEVEL ABOVE {z_stop}\n'
            f'THEN PUMP {pump} STATUS IS CLOSED\n\n')

    if scenario is not None:
        for ctl, in project.fetchall(f"select regulation_file from api.regulation_scenario where scenario='{scenario}' order by priority desc"):
            rules += export_inp_rules(project, model, ctl, feedback)

    # TODO determine is we are on weekeday or weekend from scenario and output the appropriate modulation curve
    patterns = ''
    for name, array, comment in project.fetchall(
            """select name, hourly_modulation_array, comment from api.hourly_modulation_curve
            union all
            select 'Constant', array_fill(1, ARRAY[24, 2])::varchar, '' where exists (select 1 from api.user_node where domestic_curve='Constant' or industrial_curve='Constant')
            """):
        if name not in needed_pattern:
            continue
        if comment is not None:
            patterns += f";{comment}\n"
        for i, (weekday, weekend) in enumerate(eval(array.replace('{','[').replace('}',']'))):
            if i%6 == 0:
                patterns += f" {name:<25}"
            patterns += f" {weekday:<12}"
            if (i+1)%6 == 0:
                patterns += "\n"
        if patterns[-1] != "\n":
            patterns += "\n"

    if scenario is not None:
        for sname, pname, external_file, zqt in project.fetchall(f"""
                select s.name, 'P'||s.id||'_curve', True, null
                from api.imposed_piezometry_singularity s
                join ___.node n on n.id=s.id
                where n.model='{model}' and s.external_file
                union all
                select s.name, 'Q'||s.id||'_curve', True, null
                from api.flow_injection_singularity s
                join ___.node n on n.id=s.id
                where n.model='{model}' and s.external_file
                union all
                select
                    s.name,
                    case when cascade_mode='Imposed piezometry Z(t)' then 'P'||s.id||'_curve' else 'Q'||s.id||'_curve' end,
                    external_file,
                    case when cascade_mode='Imposed piezometry Z(t)' then zt_array else qt_array end
                from api.model_connection_singularity s
                where _model='{model}'
                """):
            zqt = [] if external_file else eval(zqt.replace('{','[').replace('}',']'))
            if external_file:
                for hydrology_file, in project.fetchall(f"""select hydrology_file from api.hydrology_scenario where scenario='{scenario}'"""):
                    if not os.path.isfile(hydrology_file):
                        feedback.pushWarning(f"No such file '{hydrology_file}'. Export model without curve for {sname}.")
                        warnings += 1
                    else:
                        with open(hydrology_file) as f:
                            is_in = False
                            for l in f:
                                if is_in:
                                    if l.strip() == '':
                                        break
                                    zqt.append([float(v) for v in l.strip().split()])
                                if not is_in and l.strip().replace("'", "").upper() == sname.upper():
                                    is_in = True
            t_prev = None
            w_ts = 0
            w_ft = 0
            for i, (t, v) in enumerate(zqt[:-1]): # dans expresseau c'est une courbe, dans epanet c'est un diagramme à batons, une histoire de poteau et d'intervale, on supprime la dernière valeur
                if t_prev is not None and t - t_prev != 1:
                    w_ts += 1
                    warnings += 1
                if i==0 and t!=0:
                    w_ft += 1
                    warnings += 1
                if i%6 == 0:
                    patterns += f" {pname:<25}"
                patterns += f" {v:<12}"
                if (i+1)%6 == 0:
                    patterns += "\n"
                t_prev = t

            if w_ts != 0:
                    feedback.pushWarning(f"Incomplete or erroneous convertion of curve of '{sname}' to pattern because {w_ts} timesteps is/are not one hour.")
            if w_ft != 0:
                    feedback.pushWarning(f"Incomplete or erroneous convertion of curve of '{sname}' to pattern because first time is not zero")

    if patterns.endswith('\n'): patterns = patterns[:-1]

    curves = ''
    for id_, name, array, comment, alpr in project.fetchall(
            f"""select 'L'||l.id, l.name, l.pq_array, l.comment, l.speed_reduction_coef
            from api.pump_link l join api.node u on u.id=l.up where u.model='{model}'"""):
        curves += f";PUMP: {name} ; {comment or ''}\n"
        if alpr == 0:
            alpr = 1
        for q, p in reversed(eval(array.replace('{','[').replace('}',']'))):
            curves += f" {id_+'_curve':<25} {q*alpr:<12.3f} {p*alpr**2:<12.3f}\n"

    for id_, name, zground, array, z_overflow, comment in project.fetchall(
            f"select 'N'||id, name, zground, sz_array, z_overflow, comment from api.reservoir_node where model='{model}'"):
        curves += f";VOLUME: {name} ; {comment or ''}\n"
        volume = 0
        r0 = None
        z0 = zground
        s0 = eval(array.replace('{','[').replace('}',']'))[0][1] #ajout Marjo 30/05/23
        for z1, s1 in eval(array.replace('{','[').replace('}',']')):
            # The last reservoir curve point that determines overflow,
            # we interpolate last point according to z_overflow
            if z1 >= z_overflow:
                alpha = (z_overflow-z0)/(z1-z0)
                r1 = sqrt((s0*(1-alpha) + s1*alpha)/pi)
                h = z_overflow - z0
                volume += pi*h*(r1**2 + r0**2 + r1*r0)/3 if r0 is not None else 0
                curves += f" {id_+'_curve':<25} {z_overflow-zground:<12} {volume:<12}\n"
                break
            r1 = sqrt(s1/pi)
            h = z1 - z0
            volume += pi*h*(r1**2 + r0**2 + r1*r0)/3 if r0 is not None else 0
            curves += f" {id_+'_curve':<25} {z1-zground:<12} {volume:<12}\n"
            r0, z0 = r1, z1

    for id_, name, array, comment in project.fetchall(
            f"select 'L'||l.id, l.name, l.regul_pq_array, l.comment from api.pressure_regulator_link l join api.node u on u.id=l.up where u.model='{model}' and pressure_regulation_option='Curve P(Q)'"):
        curves += f";HEADLOSS: {name} ; {comment or ''}\n"
        for q, p in eval(array.replace('{','[').replace('}',']')):
            curves += f" {id_+'_curve':<25} {q:<12} {p:<12}\n"


    if curves.endswith('\n'): curves = curves[:-1]

    options = "\n".join([f"{k:<20} {v:20}" for k, v in [
        ("UNITS", 'CMH'),
        ("Headloss", "D-W")
        ]])

    coordinates, = project.fetchone(f"""
        select string_agg(blk, '\n') from (
        select string_agg(format(' %-25s %-12s %-12s', coalesce(ss.name, s.name, n.name), st_x(geom)::numeric(12,3), st_y(geom)::numeric(12,3)),
            '\n' order by coalesce(s.name, n.name)) as blk
        from ___.node n
        left join ___.imposed_piezometry_singularity s on s.id=n.id
        left join ___.model_connection_singularity ss on ss.id=n.id and ss.cascade_mode='Imposed piezometry Z(t)'
        where model='{model}'
        union all
        -- intermediate node for regulated pumps
        select string_agg(format(' %-25s %-12s %-12s',
                p.id||'_midle', 0.5*(st_x(u.geom)+st_x(d.geom)), 0.5*(st_y(u.geom)+st_y(d.geom))),
                '\n') as blk
        from api.pump_link p
        join api.user_node u on u.id=p.up
        join api.user_node d on d.id=p.down
        where u.model='{model}' and p.pump_regulation_option in ('Flow regulation', 'Pressure regulation')
        ) t
        """)

    vertices, = project.fetchone(f"""
        select string_agg(format(' %-25s %-12s %-12s', l.name, st_x(d.geom)::numeric(12,3), st_y(d.geom)::numeric(12,3)),
            '\n' order by l.name, d.path)
        from ___.link l
        join ___.node u on u.id = l.up
        join lateral st_dumppoints(l.geom) d on st_numpoints(l.geom)>2
        where d.path!=array[1] and d.path!=array[st_numpoints(l.geom)]
        and u.model='{model}'
        """)


    valves, = project.fetchone(f"""
        select string_agg(blk, '\n') from (
        select string_agg(format(' %-25s %-25s %-25s %-12s %-17s %-17s %-12s ; %s',
            l.name,
            coalesce(us.name, uss.name, u.name),
            coalesce(ds.name, uss.name, d.name),
            (2*sqrt(section/pi())*1000)::numeric(12,3),
            case
            when l.pressure_regulation_mode = 'Imposed downstream pressure' then 'PRV'
            when l.pressure_regulation_mode = 'Imposed upstream pressure' then 'PSV'
            when l.pressure_regulation_mode = 'Imposed differential pressure' then 'PBV'
            end,
            case
            when l.pressure_regulation_option = 'Constant pressure' then regul_p::numeric(12,2)::varchar
            when l.pressure_regulation_option = 'Curve P(Q)' then 'L'||l.id||'_curve'
            end,
            headloss_coef,
            l.comment),
            '\n' order by l.name) as blk
        from api.pressure_regulator_link l
        join ___.node u on u.id = l.up
        join ___.node d on d.id = l.down
        left join ___.imposed_piezometry_singularity us on us.id=u.id
        left join ___.imposed_piezometry_singularity ds on ds.id=d.id
        left join ___.model_connection_singularity uss on uss.id=u.id and uss.cascade_mode='Imposed piezometry Z(t)'
        left join ___.model_connection_singularity dss on dss.id=d.id and dss.cascade_mode='Imposed piezometry Z(t)'
        where u.model='{model}'
        union all
        select string_agg(format(' %-25s %-25s %-25s %-12s %-17s %-17s %-12s ;%s',
            l.name,
            coalesce(us.name, uss.name, u.name),
            coalesce(ds.name, uss.name, d.name),
            (2*sqrt(section/pi())*1000)::numeric(12,3),
            'FCV',
            regul_q,
            headloss_coef,
            l.name),
            '\n' order by l.name) as blk
        from api.flow_regulator_link l
        join ___.node u on u.id = l.up
        join ___.node d on d.id = l.down
        left join ___.imposed_piezometry_singularity us on us.id=u.id
        left join ___.imposed_piezometry_singularity ds on ds.id=d.id
        left join ___.model_connection_singularity uss on uss.id=u.id and uss.cascade_mode='Imposed piezometry Z(t)'
        left join ___.model_connection_singularity dss on dss.id=d.id and dss.cascade_mode='Imposed piezometry Z(t)'
        where u.model='{model}'
        union all
        select string_agg(format(' %-25s %-25s %-25s %-12s %-17s %-17s %-12s ; %s',
            l.name,
            coalesce(us.name, uss.name, u.name),
            coalesce(ds.name, dss.name, d.name),
            (2*sqrt(section/pi())*1000)::numeric(12,3),
            'TCV',
            c_opening,
            headloss_coef,
            l.comment),
            '\n' order by l.name) as blk
        from api.valve_link l
        join ___.node u on u.id = l.up
        join ___.node d on d.id = l.down
        left join ___.imposed_piezometry_singularity us on us.id=u.id
        left join ___.imposed_piezometry_singularity ds on ds.id=d.id
        left join ___.model_connection_singularity uss on uss.id=u.id and uss.cascade_mode='Imposed piezometry Z(t)'
        left join ___.model_connection_singularity dss on dss.id=d.id and dss.cascade_mode='Imposed piezometry Z(t)'
        where u.model='{model}'
        union all
        -- pump with regulation
        select string_agg(format(' %-25s %-25s %-25s %-12s %-17s %-17s %-12s ; %s',
            p.name||'_reg',
            case when p.pump_regulation_option = 'Flow regulation' then
                coalesce(us.name, uss.name, u.name)
            else
                p.id||'_midle'
            end,
            case when p.pump_regulation_option = 'Pressure regulation' then
                coalesce(ds.name, dss.name, d.name)
            else
                p.id||'_midle'
            end,
            1,
            case when p.pump_regulation_option = 'Flow regulation' then 'FCV' else 'PRV' end,
            case when p.pump_regulation_option = 'Flow regulation' then p.regul_q else p.regul_p end,
            0,
            'regulator for pump '||p.name||' with '||p.pump_regulation_option),
            '\n' order by p.name)
        from api.pump_link p
        join ___.node u on u.id = p.up
        join ___.node d on d.id = p.down
        left join ___.imposed_piezometry_singularity us on us.id=u.id
        left join ___.imposed_piezometry_singularity ds on ds.id=d.id
        left join ___.model_connection_singularity uss on uss.id=u.id and uss.cascade_mode='Imposed piezometry Z(t)'
        left join ___.model_connection_singularity dss on dss.id=d.id and dss.cascade_mode='Imposed piezometry Z(t)'
        where u.model='{model}' and p.pump_regulation_option in ('Flow regulation', 'Pressure regulation')
        ) as tbl
        """)


    # handles model_connection_links

    with open(path, 'w', encoding='utf-8') as out:
        out.write(Template(inp_template).substitute({
            'title': title or '',
            'junctions': junctions or '',
            'reservoirs': reservoirs or '',
            'tanks': tanks or '',
            'pipes': pipes or '',
            'patterns': patterns or '',
            'demands': demands or '',
            'times': times or '',
            'pumps': pumps or '',
            'curves': curves or '',
            'options': options or '',
            'coordinates': coordinates or '',
            'vertices': vertices or '',
            'valves': valves or '',
            'rules': rules or '',
            'status': status or '',
            }))

    return warnings, errors


if __name__=='__main__':
    import sys
    from qgis.core import QgsApplication
    from expresseau.project import Project
    from expresseau.utility.log import Feedback
    qgs = QgsApplication([], False)
    qgs.initQgis()

    path, project_name, model_name, scenario = sys.argv[1:] + ([None] if len(sys.argv) !=5 else [])
    project = Project(project_name, debug=False)
    export_(path, project, model_name, Feedback(), scenario)