# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of expresseau, a QGIS plugin for hydraulics                             ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

"""
executes database module functions with provided arguments

USAGE
    database [-h, -t, -d] <function> <args>

OPTIONS
    -d, --debug
        ouputs sql statements

    -t, --timing
        ouputs timing

    -h, --help
        ouputs this help

"""

import sys
import time
import getopt
from . import *

try:
    optlist, args = getopt.getopt(sys.argv[1:],
            "htd",
            ["help", "timing", "debug"])
except Exception as e:
    sys.stderr.write(str(e)+"\n")
    exit(1)

optlist = dict(optlist)

if "-h" in optlist or "--help" in optlist:
    help(sys.modules[__name__])
    exit(0)

timing = "-t" in optlist or "--timing" in optlist
debug = "-d" in optlist or "--debug" in optlist

start = time.time()

globals()[args[0]](*args[1:])

if timing:
    print(f"{args[0]} {time.time()-start:.2f} sec")

