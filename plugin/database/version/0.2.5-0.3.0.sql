create or replace function expresseau_version()
returns varchar
language sql immutable as
$$
    select '0.3'::varchar;
$$
;

alter table ___.imposed_piezometry_singularity add column external_file boolean not null default false;
alter table ___.model_connection_singularity add column external_file boolean not null default false;
alter table ___.model_connection_singularity add column chlorine real not null default 0 check(chlorine >=0);
alter table ___.model_connection_singularity add column trihalomethane real not null default 0 check(trihalomethane >=0);
alter table ___.model_connection_singularity add column passive_tracer real not null default 0 check(passive_tracer >=0);

create table ___.configuration(
    name varchar primary key default ___.unique_name('configuration', abbreviation=>'CFG'),
    creation_date timestamp not null default current_date,
    comment varchar
);

create table ___.user_configuration(
    user_ varchar primary key default session_user,
    config varchar references ___.configuration(name)
);

create table ___.scenario_configuration(
    scenario varchar references ___.scenario(name) on delete cascade on update cascade,
    config varchar references ___.configuration(name) on delete cascade on update cascade,
        primary key (scenario, config),
    rank_ integer not null default 1,
        unique (scenario, rank_)
);

create or replace function ___.current_config()
returns varchar
language sql volatile as
$$
    select config from ___.user_configuration where user_=session_user;
$$
;

do $$
declare
   c varchar;
begin
    for c in select name from ___.type_node loop
        execute format($sql$
            alter table ___.%1$s_node add constraint %1$s_node_id_name__type_key unique (id, name, _type);
            $sql$, c);

        execute format($sql$
            create table ___.%1$s_node_config(
            like ___.%1$s_node,
            config varchar default ___.current_config() references ___.configuration(name) on update cascade on delete cascade,
                foreign key (id, name, _type) references ___.%1$s_node(id, name, _type) on delete cascade on update cascade,
                primary key (id, config)
            )
            $sql$, c);
    end loop;

    for c in select name from ___.type_singularity loop
        execute format($sql$
            alter table ___.%1$s_singularity add constraint %1$s_singularity_id_name__type_key unique (id, name, _type);
            $sql$, c);

        execute format($sql$
            create table ___.%1$s_singularity_config(
            like ___.%1$s_singularity,
            config varchar default ___.current_config() references ___.configuration(name) on update cascade on delete cascade,
                foreign key (id, name, _type) references ___.%1$s_singularity(id, name, _type) on delete cascade on update cascade,
                primary key (id, config)
            )
            $sql$, c);
    end loop;

    for c in select name from ___.type_link loop
        execute format($sql$
            alter table ___.%1$s_link add constraint %1$s_link_id_name__type_key unique (id, name, _type);
            $sql$, c);

        execute format($sql$
            create table ___.%1$s_link_config(
            like ___.%1$s_link,
            config varchar default ___.current_config() references ___.configuration(name) on update cascade on delete cascade,
                foreign key (id, name, _type) references ___.%1$s_link(id, name, _type) on delete cascade on update cascade,
                primary key (id, config)
            )
            $sql$, c);
    end loop;
end
$$
;

create or replace function ___.duplicated_names()
returns varchar
language sql as
$$
    with counted as (
        select name, model, count(1) ct
        from (
            select name, model from ___.node
            union all
            select s.name, n.model from ___.singularity s join ___.node n on n.id=s.id
            union all
            select l.name, n.model from ___.link l join ___.node n on n.id=l.up
            ) t
        group by name, model
    )
    select string_agg(name, ', ') from counted where ct > 1;
$$
;

create or replace function ___.name_unicity_check()
returns trigger
language plpgsql as
$$
declare
    duplicates varchar;
begin
    select ___.duplicated_names() into duplicates;
    if duplicates is not null then
        raise 'duplicated name %', duplicates;
    end if;
    return new;
end;
$$
;

create trigger node_after_update_trig
after insert or update on ___.node
for each statement execute procedure ___.name_unicity_check();

create trigger singularity_after_update_trig
after insert or update on ___.singularity
for each statement execute procedure ___.name_unicity_check();

create trigger link_after_update_trig
after insert or update on ___.link
for each statement execute procedure ___.name_unicity_check();

alter table ___.metadata add column unique_name_per_model boolean default true;

create or replace function ___.metadata_after_srid_update_fct()
returns trigger
language plpgsql volatile as
$$
declare
    table_name varchar;
    geometry_type varchar;
    dimension integer;
    api_exists boolean;
    duplicates varchar;
begin
    if exists (select 1 from information_schema.schemata where schema_name = 'api') then
        drop schema api cascade;
        api_exists = true;
    end if;

    for table_name, geometry_type, dimension in select f_table_name, type, coord_dimension from geometry_columns where f_table_schema = '___' loop
        execute 'alter table ___.'||table_name||' '||
        E'alter column geom '||
        E'type geometry('''||geometry_type||case when dimension=3 then 'Z' else '' end||''', '||new.srid||') '||
        E'using st_transform(geom, '||new.srid||');';
    end loop;

    if new.unique_name_per_model then
        select ___.duplicated_names() into duplicates ;
        if duplicates is not null then
            raise 'duplicated name %', duplicates;
        end if;
        alter table ___.node enable trigger node_after_update_trig;
        alter table ___.singularity enable trigger singularity_after_update_trig;
        alter table ___.link enable trigger link_after_update_trig;
    else
        alter table ___.node disable trigger node_after_update_trig;
        alter table ___.singularity disable trigger singularity_after_update_trig;
        alter table ___.link disable trigger link_after_update_trig;
    end if;

    if api_exists then
        perform create_template();
        perform create_api();
        drop schema template cascade;
    end if;
    return new;
end;
$$
;
