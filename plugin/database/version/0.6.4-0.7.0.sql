create or replace function expresseau_version()
returns varchar
language sql immutable as
$$
    select '0.7'::varchar;
$$
;

alter table ___.scenario_water_origin
add column flow_injection_singularity integer references ___.flow_injection_singularity(id) on update cascade on delete cascade,
add column model_connection_singularity integer references ___.model_connection_singularity(id) on update cascade on delete cascade,
drop constraint scenario_water_origin_scenario_imposed_piezometry_singulari_key,
add constraint scenario_water_origin_check check (num_nonnulls(imposed_piezometry_singularity, flow_injection_singularity, model_connection_singularity) = 1),
add constraint scenario_water_origin_scenario_imposed_piezometry_singulari_key unique(scenario, imposed_piezometry_singularity, flow_injection_singularity, model_connection_singularity)
;


