
-- fix inconsistency in 0.9.0 betewwen new modeles and updated models
do
$$
begin
    if 'flow(m3/h)' = any(enum_range(null::___.sensor_type)::varchar[]) then
        alter type ___.sensor_type rename value 'flow(m3/h)' to 'q(m3/h)';
        alter type ___.sensor_type rename value 'flow(m3/s)' to 'q(m3/s)';
        alter type ___.sensor_type rename value 'flow(m3/day)' to 'q(m3/day)';
        alter type ___.sensor_type rename value 'pressure(bar)' to 'p(bar)';
        alter type ___.sensor_type rename value 'pressure(m)' to 'p(m)';
    end if;
end;
$$
;

create index node_typeidx on ___.node(_type);
create index link_typeidx on ___.link(_type);
create index link_upidx on ___.link(up);
create index link_downidx on ___.link(down);
create index singularity_typeidx on ___.singularity(_type);

alter type ___.alim_mode rename value 'Overflow' to 'From top';

alter table ___.measure drop constraint measure_pkey;
alter table ___.measure add column id serial primary key;
create index measure_sensor_idx on ___.measure(sensor);

create table ___.comment (
	id serial primary key,
	user_ varchar default session_user,
	creation_date timestamp not null default now(),
	txt varchar,
	node integer references ___.node(id) on delete cascade on update cascade,
	link integer references ___.link(id) on delete cascade on update cascade,
	singularity integer references ___.singularity(id) on delete cascade on update cascade,
	scenario varchar references ___.scenario(name) on delete cascade on update cascade
);

