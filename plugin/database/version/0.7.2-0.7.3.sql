create or replace function expresseau_version()
returns varchar
language sql immutable as
$$
    select '0.8'::varchar;
$$
;

alter table ___.user_node
drop constraint user_node_domestic_curve_fkey,
add  constraint user_node_domestic_curve_fkey foreign key (domestic_curve) references ___.hourly_modulation_curve(name) on update cascade on delete set null,
drop constraint user_node_industrial_curve_fkey,
add  constraint user_node_industrial_curve_fkey foreign key (industrial_curve) references ___.hourly_modulation_curve(name) on update cascade on delete set null
;


