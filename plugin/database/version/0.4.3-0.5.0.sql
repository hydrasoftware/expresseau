create or replace function expresseau_version()
returns varchar
language sql immutable as
$$
    select '0.5'::varchar;
$$
;

create sequence ___.pipe_link_singularity_id_seq;
alter table ___.pipe_link_singularity alter column id set default nextval('___.pipe_link_singularity_id_seq');
alter table ___.pipe_link_singularity add column bend_radius real check (bend_radius>0);
alter table ___.pipe_link_singularity add column bend_is_smooth boolean;
alter table ___.pipe_link_singularity alter column type_singularity drop not null;
alter table ___.pipe_link_singularity alter column type_singularity drop default;
alter table ___.pipe_link_singularity drop constraint pipe_link_singularity_check;
alter table ___.pipe_link_singularity drop constraint pipe_link_singularity_check1;
alter table ___.pipe_link_singularity add constraint pipe_link_singularity_check check (
    type_singularity is null
    or (type_singularity='Bend' and bend_angle is not null and bend_is_smooth is not null)
    or (type_singularity='Borda headloss' and borda_section is not null and borda_coef is not null)
    )
;

alter table ___.singularity alter column id drop default;
drop sequence ___.singularity_id_seq;

alter table ___.pump_link drop constraint pump_link_check4;

alter table ___.model alter column name type varchar(16);

update ___.model set name=replace(name, ' ', '_');
update ___.hourly_modulation_curve set name=replace(name, ' ', '_');
update ___.failure_curve set name=replace(name, ' ', '_');
update ___.node set name=replace(name, ' ', '_');
update ___.singularity set name=replace(name, ' ', '_');
update ___.link set name=replace(name, ' ', '_');
update ___.water_delivery_sector set name=replace(name, ' ', '_');
update ___.water_delivery_scenario set name=replace(name, ' ', '_');

alter table ___.model add constraint model_name_check check(not name~' ' and octet_length(name)<=16);
alter table ___.hourly_modulation_curve add constraint hourly_modulation_curve_name_check check(not name~' ' and octet_length(name)<=24);
alter table ___.failure_curve add constraint failure_curve_name_check check(not name~' ' and octet_length(name)<=24);
alter table ___.node add constraint node_name_check check(not name~' ' and octet_length(name)<=24);
alter table ___.singularity add constraint singularity_name_check check(not name~' ' and octet_length(name)<=24);
alter table ___.link add constraint link_name_check check(not name~' ' and octet_length(name)<=24);
alter table ___.water_delivery_sector add constraint water_delivery_sector_name_check check(not name~' ' and octet_length(name)<=24);
alter table ___.water_delivery_scenario drop constraint water_delivery_scenario_name_check;
alter table ___.water_delivery_scenario add constraint water_delivery_scenario_name_check check(name not like 'WD_SCN_REF' and not name~' ' and octet_length(name)<=24);

-- update pumps, flow injection and imposed_piezometry to allow cyclic external files
alter table ___.pump_link alter column speed_reduction_coef set not null;
alter table ___.pump_link add column external_file boolean not null default false;
alter table ___.pump_link add column cyclic boolean not null default false;
alter table ___.flow_injection_singularity add column cyclic boolean not null default false;
alter table ___.model_connection_singularity add column cyclic boolean not null default false;
alter table ___.imposed_piezometry_singularity add column cyclic boolean not null default false;

alter table ___.pump_link_config alter column speed_reduction_coef set not null;
alter table ___.pump_link_config add column external_file boolean not null default false;
alter table ___.pump_link_config add column cyclic boolean not null default false;
alter table ___.flow_injection_singularity_config add column cyclic boolean not null default false;
alter table ___.model_connection_singularity_config add column cyclic boolean not null default false;
alter table ___.imposed_piezometry_singularity_config add column cyclic boolean not null default false;

-- rename index logically
drop index if exists water_delivery_point_user_nodeidx;
create index water_delivery_point_pipe_link_idx on ___.water_delivery_point (pipe_link);

create table ___.fire_hydrant(
    name varchar primary key default ___.unique_name('fire_hydrant', abbreviation=>'FH'),
    comment varchar,
    geom geometry('POINT', 2154) not null,
    diameter real not null check (diameter>=0),
    theoritical_flow_m3h real not null check (theoritical_flow_m3h >= 0),
    draw_time_h real not null check (draw_time_h >=0),
    pipe_link integer references ___.pipe_link(id) on update cascade on delete set null
);


-- fix

do $$
declare
   r record;
   c varchar;
begin
    for r in
        select 'create sequence if not exists ___.'||replace(replace(regexp_replace(replace(replace(
            col.column_default,
            '___.unique_name(''', ''),
            '::character varying', ''),
            ''', abbreviation =>.*\)', ''),
            ''', ''', '_'),
            ''')', '')||'_name_seq' as query
        from information_schema.columns col
        where col.column_default is not null
              and col.table_schema='___' and col.column_default ~ '^___.unique_name\(.*'
    loop
        raise notice '%',r.query;
        execute r.query;
    end loop;
end;
$$;


