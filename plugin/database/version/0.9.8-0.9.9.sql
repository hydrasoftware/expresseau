
alter type ___.week_period add value 'Auto';

alter table ___.scenario add column skeletonization boolean not null default false ;

update ___.water_delivery_scenario_industrial_volume set industrial=true;
alter table ___.water_delivery_scenario_industrial_volume alter column industrial set not null;
alter table ___.water_delivery_scenario_industrial_volume alter column industrial set default true;






