alter type ___.computation_mode rename value 'Exterior fire defense' to 'Exterior fire defense nominal flow'
;
alter type ___.computation_mode add value 'Exterior fire defense flow at 1bar'
;

alter table ___.pipe_link add column is_feeder boolean default false
;
alter table ___.pipe_link_config add column is_feeder boolean default false
;
alter table ___.user_node drop column fire_hydrant
;
alter table ___.user_node_config drop column fire_hydrant
;
alter table ___.fire_hydrant rename column theoritical_flow_m3h to nominal_flow_m3h
;
alter table ___.scenario
drop constraint if exists scenario_check,
drop constraint if exists scenario_check1,
drop constraint if exists scenario_check2,
drop constraint if exists scenario_check3,
add constraint  scenario_check check(comput_mode::text !='Fast transient' or refined_discretisation is true),
add constraint  scenario_check1 check(comput_mode::text not like 'Exterior fire defense%' or scenario_rstart is not null),
add constraint  scenario_check2 check(comput_mode::text !='Break criticality' or scenario_rstart is not null),
add constraint  scenario_check3 check(scenario_rstart is null or scenario_ref is not null)
;