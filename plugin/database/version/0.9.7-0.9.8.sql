alter table ___.imposed_piezometry_singularity
    add column travel_time real not null default 0 check(travel_time >=0),
    add column contact_time real not null default 0 check(contact_time >=0);
alter table ___.imposed_piezometry_singularity_config
    add column travel_time real not null default 0 check(travel_time >=0),
    add column contact_time real not null default 0 check(contact_time >=0);
alter table ___.flow_injection_singularity
    add column travel_time real not null default 0 check(travel_time >=0),
    add column contact_time real not null default 0 check(contact_time >=0);
alter table ___.flow_injection_singularity_config
    add column travel_time real not null default 0 check(travel_time >=0),
    add column contact_time real not null default 0 check(contact_time >=0);
alter table ___.model_connection_singularity
    add column travel_time real not null default 0 check(travel_time >=0),
    add column contact_time real not null default 0 check(contact_time >=0);
alter table ___.model_connection_singularity_config
    add column travel_time real not null default 0 check(travel_time >=0),
    add column contact_time real not null default 0 check(contact_time >=0);


