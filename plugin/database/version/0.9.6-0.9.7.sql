alter table ___.reservoir_node add column reset_travel_time boolean not null default false;
alter table ___.reservoir_node_config add column reset_travel_time boolean not null default false;
