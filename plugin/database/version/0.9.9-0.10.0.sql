-- Ajoute la colonne id avec serial
alter table ___.fire_hydrant add column id serial;
-- Supprime la clé primaire existante sur name
alter table ___.fire_hydrant drop constraint fire_hydrant_pkey;
-- Défini id comme nouvelle clé primaire
alter table ___.fire_hydrant add constraint fire_hydrant_pkey primary key (id);
-- Ajoute une contrainte d'unicité sur name
alter table ___.fire_hydrant add constraint fire_hydrant_name_unique unique (name);


--Ajoute la colonne id avec serial
alter table ___.water_delivery_point add column id serial;
--Supprime la clé primaire existante sur name
alter table ___.water_delivery_point drop constraint water_delivery_point_pkey;
--Défini id comme nouvelle clé primaire
alter table ___.water_delivery_point add constraint water_delivery_point_pkey primary key (id);
--Ajoute une contrainte d'unicité sur name
alter table ___.water_delivery_point add constraint water_delivery_point_name_unique unique (name);
