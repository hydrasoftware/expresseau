create or replace function expresseau_version()
returns varchar
language sql immutable as
$$
    select '0.1'::varchar;
$$
;

alter table ___.regulation_scenario add column priority integer not null check(priority>0)
;

alter table ___.scenario rename column chlore to chlorine
;
alter table ___.scenario rename column chlore_water_reaction_coef to chlorine_water_reaction_coef
;
alter table ___.scenario alter column chlorine_water_reaction_coef set default 0.05
;
update ___.scenario set chlorine_water_reaction_coef=0.05 where chlorine_water_reaction_coef is null
;
alter table ___.scenario alter column chlorine_water_reaction_coef set not null
;
alter table ___.scenario rename column chlore_wall_reaction_coef to chlorine_wall_reaction_coef
;
alter table ___.scenario alter column chlorine_wall_reaction_coef set default 0.0000005
;
update ___.scenario set chlorine_wall_reaction_coef=0.0000005 where chlorine_wall_reaction_coef is null
;
alter table ___.scenario alter column chlorine_wall_reaction_coef set not null
;
alter table ___.scenario rename column trihalomethane_water_reaction_coef to trihalomethane_reaction_parameter
;
alter table ___.scenario alter column trihalomethane_reaction_parameter set default 0.05
;
update ___.scenario set trihalomethane_reaction_parameter=0.05 where trihalomethane_reaction_parameter is null
;
alter table ___.scenario alter column trihalomethane_reaction_parameter set not null
;
alter table ___.scenario rename column trihalomethane_wall_reaction_coef to trihalomethane_concentration_limit
;
alter table ___.scenario alter column trihalomethane_concentration_limit set default 0.0000005
;
update ___.scenario set trihalomethane_concentration_limit=0.0000005 where trihalomethane_concentration_limit is null
;
alter table ___.scenario alter column trihalomethane_concentration_limit set not null
;


alter table ___.imposed_piezometry_singularity rename column chlore to chlorine
;
alter table ___.flow_injection_singularity rename column chlore to chlorine
;
