create or replace function expresseau_version()
returns varchar
language sql immutable as
$$
    select '0.6'::varchar;
$$
;

alter type ___.computation_mode add value if not exists 'Exterior fire defense';
alter table ___.scenario drop column flag_rstart;
alter table ___.scenario rename column dt_output_hr to dt_output;

alter type ___.pump_regulation_option add value if not exists 'Water level in reservoir';
alter table ___.pump_link add column regul_z real;
alter table ___.pump_link add column regul_proportional real;
alter table ___.pump_link add column regul_integral real;
alter table ___.pump_link add column regul_derivative real;
alter table ___.pump_link_config add column regul_z real;
alter table ___.pump_link_config add column regul_proportional real;
alter table ___.pump_link_config add column regul_integral real;
alter table ___.pump_link_config add column regul_derivative real;

-- fix

do $$
declare
   r record;
   c varchar;
begin
    for r in
        select 'create sequence if not exists ___.'||replace(replace(regexp_replace(replace(replace(
            col.column_default,
            '___.unique_name(''', ''),
            '::character varying', ''),
            ''', abbreviation =>.*\)', ''),
            ''', ''', '_'),
            ''')', '')||'_name_seq' as query
        from information_schema.columns col
        where col.column_default is not null
              and col.table_schema='___' and col.column_default ~ '^___.unique_name\(.*'
    loop
        raise notice '%',r.query;
        execute r.query;
    end loop;
end;
$$;

