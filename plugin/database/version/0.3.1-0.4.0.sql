create or replace function expresseau_version()
returns varchar
language sql immutable as
$$
    select '0.4'::varchar;
$$
;

create table ___.hydrology_scenario(
    hydrology_file varchar,
    scenario varchar references ___.scenario(name) on update cascade on delete cascade,
        primary key (hydrology_file, scenario)
);

create or replace function ___.metadata_after_srid_update_fct()
returns trigger
language plpgsql volatile as
$$
declare
    table_name varchar;
    geometry_type varchar;
    dimension integer;
    api_exists boolean;
    duplicates varchar;
begin
    if exists (select 1 from information_schema.schemata where schema_name = 'api') then
        drop schema api cascade;
        api_exists = true;
    end if;

    for table_name, geometry_type, dimension in select f_table_name, type, coord_dimension from geometry_columns where f_table_schema = '___' loop
        execute 'alter table ___.'||table_name||' '||
        E'alter column geom '||
        E'type geometry('''||geometry_type||case when dimension=3 then 'Z' else '' end||''', '||new.srid||') '||
        E'using st_transform(geom, '||new.srid||');';
    end loop;

    if api_exists then
        perform create_template();
        perform create_api();
        drop schema template cascade;
    end if;
    return new;
end;
$$
;

create or replace function ___.metadata_after_unique_name_per_model_update_fct()
returns trigger
language plpgsql volatile as
$$
declare
    table_name varchar;
    geometry_type varchar;
    dimension integer;
    api_exists boolean;
    duplicates varchar;
begin
    if new.unique_name_per_model then
        select ___.duplicated_names() into duplicates ;
        if duplicates is not null then
            raise 'duplicated name %', duplicates;
        end if;
        alter table ___.node enable trigger node_after_update_trig;
        alter table ___.singularity enable trigger singularity_after_update_trig;
        alter table ___.link enable trigger link_after_update_trig;
    else
        alter table ___.node disable trigger node_after_update_trig;
        alter table ___.singularity disable trigger singularity_after_update_trig;
        alter table ___.link disable trigger link_after_update_trig;
    end if;
    return new;
end;
$$
;

create trigger metadata_after_unique_name_per_model_update_trig
after update of unique_name_per_model on ___.metadata
for each row execute procedure ___.metadata_after_unique_name_per_model_update_fct();


