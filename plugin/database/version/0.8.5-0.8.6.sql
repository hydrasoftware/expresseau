alter table ___.pipe_link add column installation_date date;
alter table ___.pipe_link_config add column installation_date date;

create or replace function ___.current_config()
returns varchar
language sql volatile security definer as
$$
    select config from ___.user_configuration where user_=session_user;
$$
;

