create type ___.sensor_type as enum (
    'index(m3)',
    'flow(m3/h)',
    'flow(m3/s)',
    'flow(m3/day)',
    'z(m)',
    'pressure(bar)',
    'pressure(m)'
);

create table ___.sensor (
    id serial primary key,
    name varchar not null unique,
    type_ ___.sensor_type not null,
    geom public.geometry(POINT, 2154) not null,
    comment varchar
);

create index sensor_geom_idx on ___.sensor using gist(geom);

create table ___.measure (
    t timestamp without time zone not null,
    value float not null,
    sensor integer not null references ___.sensor(id) on delete cascade on update cascade,
    creation_date timestamp without time zone default now() not null,
        primary key (t, sensor)
);

alter table ___.pressure_regulator_link add column pid_p real not null default 1.;
alter table ___.pressure_regulator_link add column pid_i real not null default 1.;
alter table ___.pressure_regulator_link add column pid_d real not null default 1.;
alter table ___.pressure_regulator_link_config add column pid_p real not null default 1.;
alter table ___.pressure_regulator_link_config add column pid_i real not null default 1.;
alter table ___.pressure_regulator_link_config add column pid_d real not null default 1.;

