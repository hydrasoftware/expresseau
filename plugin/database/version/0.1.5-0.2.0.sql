create or replace function expresseau_version()
returns varchar
language sql immutable as
$$
    select '0.2'::varchar;
$$
;

alter table ___.water_delivery_sector add column if not exists ref_leak_efficiency real not null check(ref_leak_efficiency>0 and ref_leak_efficiency<=1) default 1
;

alter table ___.water_delivery_scenario_sector_setting drop constraint if exists water_delivery_scenario_sector_setting_leak_efficiency_check
;

alter table ___.water_delivery_scenario_sector_setting add constraint water_delivery_scenario_sector_setting_leak_efficiency_check check(leak_efficiency>0 and leak_efficiency<=1)
;

