

# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of EXPRESSEAU, a QGIS plugin for hydraulics                          ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


"""
import old expresseau models

usage: python -m import_legacy <project_name> <model_name> <directory>

the directory must contain:
    - a .dat file where most information is stored
    - a _CrbModulConso.Csv file for modulation curves (curves that are already in model are ignored)
    - a caracteristiques_materiaux.xlsx
    - a _Nod.Csv file
    - a _CANA.Csv file

"""

import pandas
import os
import glob

def remove_model_name(name):
    return ('_'.join(name.split('_')[1:])).upper()

def import_(cursor, model, path):

    #cursor.execute("set session_replication_role=replica")

    cursor.execute("update api.metadata set unique_name_per_model = false returning srid")
    srid, = cursor.fetchone()

    dat_sections = {}
    fichiers = [f for f in glob.glob(os.path.join(path, '*.dat')) if not f.endswith('Phys.dat')]
    for fichier in fichiers:
        with open(fichier, encoding='iso-8859-1') as f:
            dat = f.read().split('$')
            for d in dat[1:]:
                lines = d.split('\n')
                section_name = lines[0].strip()
                if not section_name.startswith('sortie'):
                    # resolution bug plusieurs section $ chateau d'eau
                    if section_name in dat_sections:
                        dat_sections[section_name].append([ll.strip().replace("'", " ").split() for ll in lines[1:]])
                    else:
                        dat_sections[section_name] = [ll.strip().replace("'", " ").split() for ll in lines[1:]]
            del dat
    hourly_modulation_array = []

    file_crb = glob.glob(os.path.join(path, '*_CrbModulConso.Csv'))
    file_phys = glob.glob(os.path.join(path, 'Phys.dat'))

    if file_crb: # hourly_modulation_curve avec des fichiers CrbModulConso.Csv
        with open(file_crb[0], encoding='iso-8859-1') as f:
            data = f.readlines()
            for i in range(0, len(data), 25):
                name = data[i].strip().replace("'","")
                hourly_modulation_array = [[float(r.split(';')[1]), float(r.split(';')[1])] for r in data[i+1:i+25]]
                cursor.execute("""
                    insert into ___.hourly_modulation_curve(name, hourly_modulation_array)
                    select %s, %s where not exists (select 1 from ___.hourly_modulation_curve where name=%s)
                    """, (name, str(hourly_modulation_array).replace('[','{').replace(']','}'), name))
    elif file_phys:
        with open(file_phys[0], encoding='iso-8859-1') as f:
            lignes = f.readlines()
            for i, ligne in enumerate(lignes):
                if ligne.strip() == '*CONSO':
                    nb_courbes = int(lignes[i + 1].strip())
                    for j in range(i + 2, i + 2 + nb_courbes):
                        valeurs = lignes[j].strip().split()[1:]
                        hourly_modulation_array.append([[float(v), float(v)] for v in valeurs])
                    break
    else:
        assert(False) # hourly_modulation_curve not found

    # material

    df = pandas.read_excel(os.path.join(path, 'caracteristiques_materiaux.xlsx'))
    data = [list(r[2:5]) for i, r in df.iterrows()][1:]
    cursor.executemany(f"""
        insert into ___.material(name, roughness_mm, elasticity_n_m2)
        select * from (values {', '.join([f"('{m}', {r}, {e})" for m, r, e in data])}) t
        except
        select name, roughness_mm, elasticity_n_m2 from ___.material
        """, data)

    # node

    cursor.execute("select st_x(geom), st_y(geom) from ___.node")
    node_set = set(cursor.fetchall())
    df = pandas.read_csv(glob.glob(path+'/*_Nod.Csv')[0], delimiter =';', encoding='iso-8859-1')
    df['Id'] = df['Id'].str.upper()
    data = [list(r) + [r[-1]] for i, r in df.iterrows()]

    for i in range(len((data))):
        # move duplicated node 10cm to the right
        while (data[i][1], data[i][2]) in node_set:
            data[i][1] += .1
        node_set.add((data[i][1], data[i][2]))

    cursor.executemany(f"""
        insert into ___.node(name, model, geom, zground, _type)
        values(%s, '{model}', st_setsrid(st_makepoint(%s, %s, %s), {srid}), %s, 'user')
        """, data)

    # node_id_map maps name to node ids and also numeric ids in .dat to node ids
    cursor.execute(f"select name, id from ___.node where model='{model}'")
    node_id_map = dict(cursor.fetchall())
    node_id_map.update(
        {int(ll[0]) : node_id_map[remove_model_name(ll[1])] for ll in dat_sections['noeuds de calcul'] if len(ll) == 2})
    cursor.execute(f"select name, geom from ___.node where model='{model}'")
    node_geom_map = dict(cursor.fetchall())
    node_geom_map.update(
        {int(ll[0]) : node_geom_map[remove_model_name(ll[1])] for ll in dat_sections['noeuds de calcul'] if len(ll) == 2})

    # user_node
    cursor.execute(f"""
        insert into ___.user_node(id, name, _type)
        select id, name, _type from ___.node where _type='user' and model='{model}'
        """)

    del dat_sections['coordonnees des noeuds']
    del dat_sections['cotes du terrain naturel']
    del dat_sections['noeuds de calcul']
    del dat_sections['consommation imposee']

    # reservoir_node
    binodal_reservoir = []
    #print("import chateau")
    for section in ["20 chateau d'eau", "21 chateau d'eau", "chateau d'eau"]:
        if section in dat_sections:
            cats = dat_sections[section]
            for i, ll in enumerate(cats):
                if len(ll)==9:
                    id, id_amont, id_aval, alim_mode, return_in_valve_mode, return_out_valve_mode, z_overflow, z_ini, name = ll
                    alim_mode = {1:'From bottom', 2:'From top'}[int(alim_mode)]
                    return_in_valve_mode = {2:'Regulated', 0:'Closed', 1:'Open'}[float(return_in_valve_mode)]
                    return_out_valve_mode = {2:'Regulated', 0:'Closed', 1:'Open'}[float(return_out_valve_mode)]
                    name = remove_model_name(name)
                    #print("réservoir", name, id, id_amont, id_aval)
                    sz_array = str([[float(z), float(s)] for z, s in cats[i+2: i+2+int(cats[i+1][0])]]).replace('[','{').replace(']','}')

                    cursor.execute(f"""
                            delete from ___.user_node where id={node_id_map[int(id_amont)]}
                        """)
                    cursor.execute(f"""
                            update ___.node set _type='reservoir', name='{name}' where id={node_id_map[int(id_amont)]}
                        """)
                    cursor.execute(f"""
                        insert into ___.reservoir_node(id, name, _type, alim_mode, return_in_valve_mode, return_out_valve_mode, z_overflow, z_ini, sz_array)
                        values ('{node_id_map[int(id_amont)]}', '{name}', 'reservoir', '{alim_mode}', '{return_in_valve_mode}',
                            '{return_out_valve_mode}', {float(z_overflow)}, {float(z_ini)}, '{sz_array}'::real[])
                        """)
                    if int(id_aval) != 0:
                        binodal_reservoir.append((int(id_amont), int(id_aval)))

                    #assert(int(id_aval) == 0) # TODO insert pipe on bi-nodal reservoirs

            del dat_sections[section]

    # pipe_link
    data = []
    cana_or_canc_file = False

    if len(glob.glob(path+'/*_CANA.Csv')):
        #print("import cana")
        cana_or_canc_file = True
        try:
            df = pandas.read_csv(glob.glob(path+'/*_CANA.Csv')[0], delimiter =';', on_bad_lines='skip', encoding='iso-8859-1')
        except TypeError: # on_bad_lines used to be error_bad_lines=False
            df = pandas.read_csv(glob.glob(path+'/*_CANA.Csv')[0], delimiter =';', error_bad_lines=False, encoding='iso-8859-1')
        df['Id'] = df['Id'].str.upper()
        df['Nodam'] = df['Nodam'].str.upper()
        df['Nodav'] = df['Nodav'].str.upper()
        data = [[r[0], node_id_map[r[1]], node_id_map[r[2]], node_geom_map[r[1]], node_geom_map[r[2]]] for i, r in df.iterrows()]
        cursor.executemany("""
            insert into ___.link(name, _type, up, down, geom)
            values(%s, 'pipe', %s, %s, st_makeline(st_force2d(%s), st_force2d(%s)))
            """, data)

        cursor.execute(f"select l.id, l.name, l._type from ___.link l join ___.node n on n.id=l.up where n.model='{model}'")
        link_map = {r[1]: list(r) for r in cursor.fetchall()}
        data = [link_map[r[0]] + [r[3], r[4]/1000, r[5], r[6], r[7], bool(r[8])] for i, r in df.iterrows()]

        cursor.executemany("""
            insert into ___.pipe_link(id, name, _type, overloaded_length, diameter, thickness_mm, overloaded_roughness_mm, material, status_open, overload_length, overload_roughness_mm)
            values(%s, %s, %s, %s, %s, %s, %s, %s, %s, true, true)
            """, data)

    if len(glob.glob(path+'/*_CANC.Csv')):
        #print("import canc")
        cana_or_canc_file = True
        with open(glob.glob(path+'/*_CANC.Csv')[0], encoding='iso-8859-1') as csv:
            buf = []
            for line in csv.readlines()[1:]:
                line = line.strip()
                if line == ';;;;;;;;;;':
                    name, up, down, length, diameter_mm, thickness_mm, roughness_mm, material, status = buf[0].replace('"', '').split(';')
                    name = name.upper()
                    up = up.upper() # attention : parfois faux avec certains exports
                    down = down.upper() # attention : parfois faux avec certains exports
                    coords = ','.join([p.replace(';', ' ') for p in buf[2:]])
                    geom = f'SRID={srid}; LINESTRING({coords})'
                    tol_snap = 0.1
                    cursor.execute("""
                        insert into ___.link(name, _type, up, down, geom)
                        values (%s, 'pipe', %s, %s, st_snap(st_snap(%s, st_force2d(%s), %s), st_force2d(%s), %s))
                        returning id""", (name, node_id_map[up], node_id_map[down], geom, node_geom_map[up], tol_snap, node_geom_map[down], tol_snap))
                    id_, = cursor.fetchone()
                    cursor.execute("""
                        insert into ___.pipe_link(id, name, _type, overloaded_length, diameter, thickness_mm, overloaded_roughness_mm, material, status_open, overload_length, overload_roughness_mm)
                        values(%s, %s, 'pipe', %s, %s, %s, %s, %s, %s, true, true)
                        """, (id_, name, float(length), float(diameter_mm)/1000, float(thickness_mm), float(roughness_mm), material, status=='1'))
                    buf = []
                else:
                    buf.append(line)

    if not cana_or_canc_file:
        assert(False) # Cana not found

    #print("import chateau binodaux")
    for up, down in binodal_reservoir:
        #print(f"binodal_reservoir between {up} and {down}")
        cursor.execute("""
            select overloaded_length, diameter, thickness_mm, overloaded_roughness_mm, material, status_open, overload_length, overload_roughness_mm
            from api.pipe_link where up=%s
            limit 1
            """, (node_id_map[down],))

        result = cursor.fetchone()

        if result:  # Si une ligne est trouvée
            #print("cana trouvée :", result)
            cursor.execute("""
                insert into api.pipe_link(overloaded_length, diameter, thickness_mm, overloaded_roughness_mm, material, status_open, overload_length, overload_roughness_mm, geom)
                values (%s, %s, %s, %s, %s, %s, %s, %s, st_makeline(st_force2d(%s), st_force2d(%s)))
                """, (*result, node_geom_map[up], node_geom_map[down]))
            #print("insertion cana ok")
        else:
            #print("pas de cana aval, création avec DN=1")
            cursor.execute("""
                insert into api.pipe_link(diameter, geom, comment)
                values (%s, st_makeline(st_force2d(%s), st_force2d(%s)), %s)
                """, (1.0, node_geom_map[up], node_geom_map[down], "replace chat2"))
            #print("insertion cana ok")

    del dat_sections['canalisation']

    cursor.execute("set session_replication_role=origin")

    # imposed_piezometry_singularity
    if 'pression imposee' in dat_sections:
        #print("import pression imposee")
        cats = dat_sections['pression imposee']
        for i in range(1, int(cats[0][1])+1):
            cursor.execute("""
                insert into api.imposed_piezometry_singularity(name, z0, geom)
                values (%s, %s, ST_Force2D(%s))
                """, (
                    remove_model_name(cats[i][2]),
                    float(cats[i][1]),
                    node_geom_map[int(cats[i][0])]))

        del dat_sections['pression imposee']

    # check_valve_link
    if 'clapet' in dat_sections:
        #print("import clapet")
        cats = dat_sections['clapet']
        n = int(cats[0][1])
        for i in range(1, n+1):
            cursor.execute("""
                insert into api.check_valve_link(name, section, headloss_coef, opening_pressure, closing_mode, geom)
                values (%s, %s, %s, %s, %s, st_makeline(st_force2d(%s), st_force2d(%s)))
                """, (
                    remove_model_name(cats[i][4]),
                    float(cats[i+n+1][1]),
                    float(cats[i+n+1][2]),
                    float(cats[i+n+1][3]),
                    {'1': 'Instantaneous', '0':'Progressive'}[cats[i+n+1][4]],
                    node_geom_map[int(cats[i][2])], node_geom_map[int(cats[i][3])]))
        del dat_sections['clapet']

    # pressure_regulator_link

    if 'reducteur de pression' in dat_sections:
        #print("import reducteur de pression")
        cats = dat_sections['reducteur de pression']
        n = int(cats[0][1])
        for i, ll in enumerate(cats):
            if i > n and len(ll) == 5:
                h = int(ll[0])
                regul_pq_array = str([[float(q), float(p)] for q, p in cats[i+1: i+1+int(ll[4])]]).replace('[','{').replace(']','}')
                cursor.execute("""
                    insert into api.pressure_regulator_link(name, section, headloss_coef, regul_pq_array, pressure_regulation_mode, pressure_regulation_option, geom)
                    values (%s, %s, %s, %s::real[], %s, 'Curve P(Q)', st_makeline(st_force2d(%s), st_force2d(%s)))
                    """, (
                        remove_model_name(cats[h][4]),
                        float(cats[i][1]),
                        float(cats[i][2]),
                        regul_pq_array,
                        {'0':'Imposed differential pressure', '1':'Imposed upstream pressure', '2':'Imposed downstream pressure'}[cats[i][3]],
                        node_geom_map[int(cats[h][2])], node_geom_map[int(cats[h][3])]))

        del dat_sections['reducteur de pression']

    # valve_link

    if 'vanne' in dat_sections:
        #print("import vanne")
        cats = dat_sections['vanne']
        n = int(cats[0][1])
        for i in range(1, n+1):
            cursor.execute("""
                insert into api.valve_link(name, section, headloss_coef, c_opening, geom)
                values (%s, %s, %s, %s, st_makeline(st_force2d(%s), st_force2d(%s)))
                """, (
                    remove_model_name(cats[i][4]),
                    float(cats[i+n+1][1]),
                    float(cats[i+n+1][2]),
                    float(cats[i+n+1][3]),
                    node_geom_map[int(cats[i][2])], node_geom_map[int(cats[i][3])]))

        del dat_sections['vanne']

    # flow_regulator_link

    if 'regulation de debit' in dat_sections:
        #print("import regulation de debit")
        cats = dat_sections['regulation de debit']
        n = int(cats[0][1])
        for i in range(1, n+1):
            cursor.execute("""
                insert into api.flow_regulator_link(name, section, headloss_coef, regul_q, geom)
                values (%s, %s, %s, %s, st_makeline(st_force2d(%s), st_force2d(%s)))
                """, (
                    remove_model_name(cats[i][4]),
                    float(cats[i+n+1][1]),
                    float(cats[i+n+1][2]),
                    float(cats[i+n+1][3]),
                    node_geom_map[int(cats[i][2])], node_geom_map[int(cats[i][3])]))

        del dat_sections['regulation de debit']

    # pump_link

    if 'pompe/turbine' in dat_sections:
        #print("import pompe/turbine")
        cats = dat_sections['pompe/turbine']
        for i, ll in enumerate(cats):
            if len(ll)==5:
                id_, id_amont, id_aval, unknown, name = ll
                npt, rotation_speed, inertia, unknown, unknown, unknown, regul_z_start, regul_z_stop, unknown, regul_q = cats[i+1]
                pq = sorted([[float(q), float(p)] for p, q in cats[i+2: i+2+int(cats[i+1][0])]], key=lambda x: x[1])
                if pq[0][1] != 0:
                    pq[0][1] = 0
                if pq[-1][0] != 0:
                    pq[-1][0] = 0

                cursor.execute("""
                    insert into api.pump_link(name, inertia, rotation_speed, regul_q, regul_z_start, regul_z_stop, pq_array, geom)
                    values (%s, %s, %s, %s, %s, %s, %s::real[], st_makeline(st_force2d(%s), st_force2d(%s)))
                    """, (
                        remove_model_name(name),
                        float(inertia),
                        float(rotation_speed),
                        float(regul_q),
                        float(regul_z_start),
                        float(regul_z_stop),
                        str(pq).replace('[','{').replace(']','}'),
                        node_geom_map[int(id_amont)], node_geom_map[int(id_aval)]))

        del dat_sections['pompe/turbine']

    # water_delivery_point

    if len(glob.glob(path+'/*_CONSV.Csv')):
        #print("import conso")
        df = pandas.read_csv(glob.glob(path+'/*_CONSV.Csv')[0], delimiter =';', encoding='iso-8859-1', index_col=False)
        df['Id'] = df['Id'].str.upper()
        df['IdNoeud'] = df['IdNoeud'].str.upper()
        data = [[r[0], node_geom_map[r[1]], r[3], r[4] or ''] for i, r in df.iterrows()]

        cursor.executemany("""
            insert into api.water_delivery_point(name, geom, volume, comment)
            values (%s, st_force2d(%s), %s, %s)
            """, data)
    else:
        #print("pas de conso")
        pass

    #print("unhandled sections:", ', '.join(dat_sections.keys()))
    cursor.execute("update api.metadata set unique_name_per_model = true")

if __name__=="__main__":
    import sys
    from expresseau.project import Project
    from expresseau.database import project_exists, create_project

    project_name = sys.argv[1]
    model_name = sys.argv[2]
    path = sys.argv[3]

    if not project_exists(project_name):
        srid = 2154 if len(sys.argv) <= 4 else int(sys.argv[4])
        create_project(project_name, srid)

    project = Project(project_name, debug=False)
    project.delete_model(model_name)
    project.add_new_model(model_name)

    with project.connect() as conn, conn.cursor() as cur:
        import_(cur, model_name, path)
        conn.commit()

    project.vacuum_analyse()