# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


'''
run tests on database structure and functions

USAGE

   server_test.py [-dh] [-l location]

OPTIONS

   -h, --help
        print this help

   -d, --debug
        run in debug mode (lots of output)

   -l location
        uses location as directory for .dat files

    -k keep
        don't delete sql_xtest db afterwards

'''

import sys
import os
import getopt
import psycopg2
import tempfile
from subprocess import Popen, PIPE
from . import export_db, import_db, remove_project, autoconnection
from ..utility import read_file
from .version import __version__

assert(__name__ == "__main__")


try:
    optlist, args = getopt.getopt(sys.argv[1:],
            "hdl:k",
            ["help", "debug", "location=", "keep"])
except Exception as e:
    sys.stderr.write(str(e)+"\n")
    exit(1)

optlist = dict(optlist)

if "-h" in optlist or "--help" in optlist:
    help(sys.modules[__name__])
    exit(0)

debug = "-d" in optlist or "--debug" in optlist

if "-l" in optlist:
    test_dir = optlist['-l']
elif "--location" in optlist:
    test_dir = optlist['--location']
else:
    test_dir = os.path.join(os.path.dirname(__file__), 'test_data')

keep = "-k" in optlist or "--keep" in optlist

print("creating database sql_xtest")
with autoconnection(debug=debug) as con, con.cursor() as cur:
    cur.execute("drop database if exists sql_xtest;")
    cur.execute("create database sql_xtest;")

print("loading test data and creating model")
with autoconnection("sql_xtest", debug=debug) as con, con.cursor() as cur:
    cur.execute("create extension postgis;")

    print("load test shapefiles")
    cur.execute("create schema brut;")

    for table in [f[:-4] for f in os.listdir(test_dir) if f.endswith(".shp")]:

        cmd = ['ogr2ogr', 'PG:service=expresseau dbname=sql_xtest', '-lco', 'GEOMETRY_NAME=geom', '-a_srs', 'EPSG:2154', '-nln', 'brut.'+table, os.path.join(test_dir, table+'.shp')]
        out, err = Popen(cmd, stdout=PIPE, stdin=PIPE, stderr=PIPE).communicate()
        if err :
            print(" ".join(cmd)+'\n')
            print(err)
            exit(1)

    cur.execute(read_file(os.path.join(os.path.dirname(__file__), 'sql', 'data.sql')))
    cur.execute(read_file(os.path.join(os.path.dirname(__file__), 'sql', 'api.sql')))
    cur.execute(f"""
        create or replace function expresseau_version()
        returns varchar
        language sql immutable as
        $$
            select '{__version__}'::varchar;
        $$
        ;""")

    print("creating model")
    cur.execute("insert into api.model(name) values ('model1');")
    cur.execute("insert into api.model(name) values ('model2');")

    print("creating scenario")
    cur.execute("insert into api.scenario default values;")
    cur.execute("insert into api.scenario(name) values('scenario_default');")
    cur.execute("insert into api.scenario(name) values('scenario_export');")
    cur.execute("insert into api.scenario(name) values('REF');")
    cur.execute("insert into api.scenario(name) values('RSTART');")
    cur.execute("""update api.scenario
                   set model_connect_settings='Cascade'
                   where name='scenario_default';""")
    cur.execute("""update api.scenario
                   set refined_discretisation=True, vaporization=True,
                       flag_save=True, tsave_hr='14:26:49',
                       scenario_rstart='RSTART', scenario_ref='RSTART', trstart_hr='02:56:01'
                   where name='scenario_export';""")
    cur.execute("insert into api.regulation_scenario(priority, regulation_file, scenario) values(2, 'regul_file_2', 'scenario_export');")
    cur.execute("insert into api.regulation_scenario(priority, regulation_file, scenario) values(3, 'regul_file_3', 'scenario_export');")
    cur.execute("insert into api.regulation_scenario(priority, regulation_file, scenario) values(1, 'regul_file_1', 'scenario_export');")
    cur.execute("insert into api.failure_curve default values;")
    cur.execute("insert into api.failure_curve default values;")
    cur.execute("insert into api.failure_curve default values;")

    print("setting default values for water delivery")
    cur.execute("insert into api.hourly_modulation_curve default values;")
    cur.execute("""insert into api.hourly_modulation_curve(hourly_modulation_array)
                    values('{{1,1.5}, {1,0.5}, {1,1.5}, {1,0.5}, {1,1.5}, {1,0.5}, {1,1.5},
                             {1,0.5}, {1,1.5}, {1,0.5}, {1,1.5}, {1,0.5}, {1,1.5}, {1,0.5},
                             {1,1.5}, {1,0.5}, {1,1.5}, {1,0.5}, {1,1.5}, {1,0.5}, {1,1.5},
                             {1,0.5}, {1,1.5}, {1,0.5}}'::real[]);""")
    cur.execute("insert into api.water_delivery_scenario default values;")
    cur.execute("insert into api.water_delivery_scenario(name) values('WD_scenario');")
    cur.execute("""update api.scenario set water_delivery_scenario='WD_scenario' where name='scenario_export';""")

    print("transfering data into expresseau data model")
    cur.execute("insert into api.water_delivery_sector(geom) select st_multi(geom) from brut.water_delivery_sector;")
    cur.execute("""insert into api.water_delivery_scenario_sector_setting(scenario, sector, volume_m3j, adjust_coef, leak_efficiency)
                   select 'WD_scenario', name, 24, 0.8, 0.2 from api.water_delivery_sector;""")

    cur.execute("insert into api.user_node(geom, name, model) select ST_Force2D(geom), name, 'model1' from brut.user_node;")

    print("check two water_delivery_sector cannot be created over a node")
    try:
        cur.execute("insert into api.water_delivery_sector(geom, name) select geom, 'WD_SECT_1_bis' from api.water_delivery_sector where name='WD_SECT_1'")
        assert(False)
    except psycopg2.errors.RaiseException:
        pass



    cur.execute("""update api.user_node set domestic_curve=(select name from api.hourly_modulation_curve order by name limit 1),
                        industrial_curve=(select name from api.hourly_modulation_curve order by name desc limit 1);""")
    cur.execute("insert into api.reservoir_node(geom, name, model, z_ini, z_overflow) select ST_Force2D(geom), name, 'model1', 100, 101 from brut.reservoir_node;")

    cur.execute("insert into api.imposed_piezometry_singularity(geom, name) select ST_Force2D(geom), name from brut.imposed_piezometry_singularity;")
    cur.execute("insert into api.chlorine_injection_singularity(geom, name) select ST_Force2D(geom), name from brut.chlorine_injection_singularity;")
    cur.execute("insert into api.surge_tank_singularity(geom, name) select ST_Force2D(geom), name from brut.surge_tank_singularity;")
    cur.execute("insert into api.pressure_accumulator_singularity(geom, name) select ST_Force2D(geom), name from brut.pressure_accumulator_singularity;")
    cur.execute("insert into api.air_relief_valve_singularity(geom, name) select ST_Force2D(geom), name from brut.air_relief_valve_singularity;")
    # cur.execute("insert into api.model_connection_singularity(geom, name) select ST_Force2D(geom), name from brut.model_connection_singularity;")

    cur.execute("insert into api.pipe_link(geom, name) select ST_Force2D(geom), name from brut.pipe_link;")
    cur.execute("insert into api.valve_link(geom, name, section) select ST_Force2D(geom), name, 1 from brut.valve_link;")
    cur.execute("insert into api.flow_regulator_link(geom, name, section) select ST_Force2D(geom), name, 1 from brut.flow_regulator_link;")
    cur.execute("insert into api.pressure_regulator_link(geom, name, section, regul_p) select ST_Force2D(geom), name, 1, 10 from brut.pressure_regulator_link;")
    cur.execute("insert into api.check_valve_link(geom, name, section) select ST_Force2D(geom), name, 1 from brut.check_valve_link;")
    cur.execute("insert into api.pump_link(geom, name, rotation_speed, inertia) select ST_Force2D(geom), name, 25, 10000 from brut.pump_link;")
    cur.execute("insert into api.headloss_link(geom, name, section) select ST_Force2D(geom), name, 1 from brut.headloss_link;")

    print("testing default name generation")
    cur.execute("""insert into api.user_node(geom, name, model)
                   select ST_SetSRID(ST_Makepoint(12,13), 2154), abbreviation||'_test_23', 'model1'
                   from ___.type_node
                   where name='user'""")
    cur.execute("""insert into api.user_node(geom, model)
                   select ST_SetSRID(ST_Makepoint(13,14), 2154), 'model1'""")

    print("testing basic select queries")
    cur.execute("select * from api.metadata;")
    cur.execute("select * from api.material;")
    cur.execute("select * from api.user_node;")
    cur.execute("select * from api.reservoir_node;")
    cur.execute("select * from api.imposed_piezometry_singularity;")
    cur.execute("select * from api.flow_injection_singularity;")
    cur.execute("select * from api.chlorine_injection_singularity;")
    cur.execute("select * from api.surge_tank_singularity;")
    cur.execute("select * from api.pressure_accumulator_singularity;")
    cur.execute("select * from api.air_relief_valve_singularity;")
    cur.execute("select * from api.model_connection_singularity;")
    cur.execute("select * from api.pipe_link;")
    cur.execute("select * from api.valve_link;")
    cur.execute("select * from api.flow_regulator_link;")
    cur.execute("select * from api.pressure_regulator_link;")
    cur.execute("select * from api.check_valve_link;")
    cur.execute("select * from api.pump_link;")
    cur.execute("select * from api.headloss_link;")

    print("testing sectors")
    cur.execute("select distinct sector from api.sector;")
    assert(len(cur.fetchall()) == 6)
    cur.execute("update api.sectorization_settings set pipe_link_meter='f', valve_link_open='f', pressure_regulator_link='t', flow_regulator_link='t', reservoir_node='t';")
    cur.execute("select distinct sector from api.sector;")
    assert(len(cur.fetchall()) == 36)

    print("testing SRID update")
    cur.execute("insert into api.user_node(geom, name, model) values(ST_SetSRID(ST_Makepoint(0,0), 2154), 'SRID_TEST', 'model1');")
    cur.execute("update api.metadata set srid=3944;")
    cur.execute("select st_dwithin(geom, 'SRID=3944; POINT(1020427.76685365 -3019713.26243572)'::geometry, 1e-6) from api.user_node where name='SRID_TEST' and model='model1';")
    assert(cur.fetchone()[0])
    cur.execute("update api.metadata set srid=2154;")
    cur.execute("select ST_Distance(geom, ST_SetSRID(ST_Makepoint(0,0), 2154)) from api.user_node where name='SRID_TEST' and model='model1';")
    assert(cur.fetchone()[0] < 0.01)

    print("testing settings")
    cur.execute("select * from api.fluid_properties;")
    assert(len(cur.fetchall()) == 1)
    cur.execute("delete from api.fluid_properties;")
    cur.execute("select * from api.fluid_properties;")
    assert(len(cur.fetchall()) == 1)
    cur.execute("select * from api.threshold_warning_settings;")
    assert(len(cur.fetchall()) == 1)
    cur.execute("delete from api.threshold_warning_settings;")
    cur.execute("select * from api.threshold_warning_settings;")
    assert(len(cur.fetchall()) == 1)
    cur.execute("select * from api.sectorization_settings;")
    assert(len(cur.fetchall()) == 1)
    cur.execute("delete from api.sectorization_settings;")
    cur.execute("select * from api.sectorization_settings;")
    assert(len(cur.fetchall()) == 1)

    print("testing scenario export")
    cur.execute("select api.file_scnnom(name) from api.scenario;")
    export_scn_nom = cur.fetchone()
    cur.execute("select api.file_cmd(name) from api.scenario;")
    export_cmd = cur.fetchone()
    cur.execute("select api.file_qts(name) from api.water_delivery_scenario;")
    export_qts = cur.fetchone()
    cur.execute("select api.file_cts();")
    export_cts = cur.fetchone()
    cur.execute("select api.file_rac();")
    export_rac = cur.fetchone()

    print("testing model export")
    cur.execute("select api._bloc_user_node(s.name, m.name) from api.scenario as s, api.model as m;")
    cur.execute("select api._bloc_reservoir_node(name) from api.model;")

    cur.execute("select api._bloc_pipe_link(name) from api.model;")
    cur.execute("select api._bloc_valve_link(name) from api.model;")
    cur.execute("select api._bloc_flow_regulator_link(name) from api.model;")
    cur.execute("select api._bloc_pressure_regulator_link(name) from api.model;")
    cur.execute("select api._bloc_check_valve_link(name) from api.model;")
    cur.execute("select api._bloc_pump_link(name) from api.model;")
    cur.execute("select api._bloc_headloss_link(name) from api.model;")

    cur.execute("select api._bloc_imposed_piezometry_singularity(name) from api.model;")
    cur.execute("select api._bloc_flow_injection_singularity(name) from api.model;")
    cur.execute("select api._bloc_chlorine_injection_singularity(name) from api.model;")
    cur.execute("select api._bloc_surge_tank_singularity(name) from api.model;")
    cur.execute("select api._bloc_pressure_accumulator_singularity(name) from api.model;")
    cur.execute("select api._bloc_air_relief_valve_singularity(name) from api.model;")

    cur.execute("select api.file_dat(s.name, m.name) from api.scenario as s, api.model as m;")
    export_dat = cur.fetchone()
    cur.execute("select api.file_phys_dat();")
    export_phys = cur.fetchone()
    cur.execute("select api.file_act_dat();")
    export_act = cur.fetchone()

    print("check API recreation")
    cur.execute(read_file(os.path.join(os.path.dirname(__file__), 'sql', 'api.sql')))

    cur.execute("select api.file_scnnom(name) from api.scenario;")
    assert(export_scn_nom == cur.fetchone())
    cur.execute("select api.file_cmd(name) from api.scenario;")
    assert(export_cmd == cur.fetchone())
    cur.execute("select api.file_qts(name) from api.water_delivery_scenario;")
    assert(export_qts == cur.fetchone())
    cur.execute("select api.file_cts();")
    assert(export_cts == cur.fetchone())
    cur.execute("select api.file_rac();")
    assert(export_rac == cur.fetchone())
    cur.execute("select api.file_dat(s.name, m.name) from api.scenario as s, api.model as m;")
    assert(export_dat == cur.fetchone())
    cur.execute("select api.file_phys_dat();")
    assert(export_phys == cur.fetchone())
    cur.execute("select api.file_act_dat();")
    assert(export_act == cur.fetchone())

    print("export db")
    export_db("sql_xtest", os.path.join(tempfile.gettempdir(), "sql_xtest.sql"))

    print("drop data schema and re-create it")
    cur.execute("drop schema api cascade")
    cur.execute("drop schema ___ cascade")
    cur.execute(read_file(os.path.join(os.path.dirname(__file__), 'sql', 'data.sql')))
    cur.execute(read_file(os.path.join(os.path.dirname(__file__), 'sql', 'api.sql')))

print("remove project")
remove_project('sql_xtest')

print("re-create project from exported db")
import_db("sql_xtest", os.path.join(tempfile.gettempdir(), "sql_xtest.sql"))

if not keep:
    print("deleting database sql_xtest")
    with autoconnection("postgres", debug=debug) as con, con.cursor() as cur:
        cur.execute("drop database if exists sql_xtest;")

sys.stdout.write("ok\n")
