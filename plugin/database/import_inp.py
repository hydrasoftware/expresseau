################################################################################################
##                                                                                            ##
##     This file is part of EXPRESSEAU, a QGIS plugin for hydraulics                          ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

"""

import EPANET inp file to expresseau projet with the model

usage : python -m expresseau.database.import_inp [-s <srid>] [-d <srid>] [-e <encoding>] project_name model_name inp_file

"""

import re
import os
import sys
import shutil
from math import pi, log, pow, sqrt
from collections import defaultdict
from shapely.geometry import LineString, Point
import numpy as np
from itertools import chain
from scipy.optimize import minimize
from datetime import timedelta
from .inp_to_ctl import import_ as inp_to_ctl

def parseTITLE():
    pass

#[JUNCTIONS]
#[RESERVOIRS]
#[TANKS]
#[PIPES]
#[PUMPS]
#[VALVES]
#[EMITTERS]
#[CURVES]
#[PATTERNS]
#[ENERGY]
#[STATUS]
#[CONTROLS]
#[RULES]
#[DEMANDS]
#[QUALITY]
#[REACTIONS]
#[SOURCES]
#[MIXING]
#[OPTIONS]
#[TIMES]
#[REPORT]
#[COORDINATES]
#[VERTICES]
#[LABELS]
#[BACKDROP]
#[TAGS]

# TODO we need to keep unused data from epanet to avoid loosing by import/export
# those data are relative to a particular model

def inverse_douglass_peuker(xy, n):
    "simplify line to n points, points must have ascending x"
    #print('inverse_douglass_peuker', xy)
    if len(xy) <= n or n < 2:
        return xy
    l = LineString([xy[0], xy[-1]])
    for i in range(n-2):
        d = np.array([l.distance(Point(p)) for p in xy])
        l = LineString(sorted(tuple(l.coords) + (tuple(xy[d.argmax()]),), key=lambda p: p[0]))
    #print('inverse_douglass_peuker', [[x,y] for x, y in l.coords])
    return [[x,y] for x, y in l.coords]

class Residual:
    def __init__(self, vh):
        self.v = [v for h, v in vh]
        self.h = [h for h, v in vh]

    def volume(self, r):
        h = self.h
        v = [0]
        for i in range(len(r) - 1):
            v.append(v[-1] +  pi*(h[i+1]-h[i])*(r[i+1]**2 + r[i]**2 + r[i+1]*r[i]))
        return v

    def f(self, r):
        return sum(((v_ref - v)**2 for v_ref, v in zip(self.v, self.volume(r))))

def powercurve(pq):
    """epanet pump power curve from 3 points"""

    """	**  Input:   h0 = shutoff head
        **           h1 = design head
        **           h2 = head at max. flow
        **           q1 = design flow
        **           q2 = max. flow"""

    #h0, h1, h2, q1, q2 = pq[0][1], pq[1][1], pq[2][1], pq[1][0], pq[2][0] #dans la logique où on a [p, q] c'était pas logique ?
    h0, h1, h2, q1, q2 = pq[0][1], pq[1][1], pq[2][1], pq[1][0], pq[2][0] #dans la logique où on a [q, p] c'est ok
    a = h0
    h4 = h0 - h1
    h5 = h0 - h2
    c = log(h5 / h4) / log(q2 / q1)
    assert(c > 0 and c <= 20)
    b = -h4 / (q1**c)
    assert(b < 0)
    return a, b, c


def tokenize(row, length=None):
    row = row.split(';')[0]
    row = row.strip().split()
    if length is None:
        return row
    else:
        return row + [None]*(length-len(row))


def pump_curve_from_power(power, Hshutoff=300, Qmax=5000):
    '''
    Génère une courbe de pompe sous forme de liste de paires (Q, H)
    à partir de la puissance donnée, de Hshutoff et de Qmax choisis
    arbitrairement ici.
    - power (float): Puissance de la pompe en watts.
    - Hshutoff (float): Hauteur de charge maximale (shutoff), en mètres.
    - Qmax (float): Débit maximal (Qmax), en m³/h.
    Retourne :
    - pq_array (list of lists): Une liste de paires (H, Q).
    '''
    import numpy as np

    rho = 1000
    g = 9.81

    Q_vals = np.logspace(np.log10(0.01), np.log10(Qmax), 45)

    # Calcul des charges en utilisant la relation P = rho * g * H * Q
    H_vals = power / (rho * g * Q_vals/3600)
    # Filtre valeurs pour que H ne dépasse pas Hshutoff
    Q_vals = Q_vals[H_vals <= Hshutoff]
    H_vals = H_vals[H_vals <= Hshutoff]

    # Ajouter les points aux extrémités : (Q=0, H=Hshutoff + epsilon) et (Q=Qmax + epsilon, H=0)
    epsilon = 0.1
    Q_vals = np.insert(Q_vals, 0, 0)
    H_vals = np.insert(H_vals, 0, Hshutoff + epsilon)
    Q_vals = np.append(Q_vals, Qmax + epsilon)
    H_vals = np.append(H_vals, 0)

    # Créer la liste de paires (Q, H)
    pq_array = [[Q_vals[i], H_vals[i]] for i in range(len(Q_vals))] #du coup c'est pas pq c'est qp mais idem partout elle est reversée ensuite

    return pq_array


def comment(row):
    return ';'.join(row.split(';')[1:]) or None

def import_(project, model, path, feedback, enc, src_srid):
    warnings = 0
    errors = 0
    sections = defaultdict(list)

    #copy file in data directory
    data_path = os.path.join(project.directory, 'data')
    if not os.path.exists(data_path):
        os.mkdir(data_path)
    shutil.copyfile(path, os.path.join(data_path, "source_epanet.inp"))

    with open(path, encoding=enc, errors='replace') as inp:
        section = ''
        last_comment = None
        for line in inp:
            line = line.strip().replace("\x00", "\uFFFD") #pour fix les vides... VMO avis ?
            #m = re.search(r'\[([A-Z]+)\]', line)
            m = re.search(r'\[([A-Z_]+)\]', line) #fix to read VSD_PUMPS section
            if m:
                section = m.group(1)
                last_comment = None
            elif section:
                if re.match(r'^ *;.*$', line):
                    last_comment = line
                elif len(line):
                    if section in ('CURVES', 'PATTERNS') and last_comment is not None:
                        if comment(line) is not None:
                            feedback.pushInfo(f"Verify synthaxe in your inp file, section {section}\n")
                            feedback.pushInfo(f"Unexpected comment : {comment(line)}\n")
                        assert(comment(line) is None) # Unexpected comment in line defining a curve
                        line += last_comment
                        last_comment = None

                    sections[section].append(line)

    dst_srid = project.srid
    pg_array = lambda x: str(list(x)).replace('[','{').replace(']','}')

    project.execute("update api.metadata set unique_name_per_model=false")

    project.execute(f"""
    update api.model set comment = %s where name = %s
    """,('\n'.join(sections['TITLE']), model))

    options = {}
    demand_multiplier = 1
    for row in sections['OPTIONS']:
        # 2 lines add by marjo to fix demand_multiplier TODO VM check
        if row.upper().startswith("DEMAND MULTIPLIER"):
            demand_multiplier = float(tokenize(row.upper().replace("DEMAND MULTIPLIER", ""))[0])
        row = tokenize(row)
        options[row[0].upper()] = row[1:]

    feedback.pushInfo(f"demand multiplier is {demand_multiplier}\n")

    flow_units_conversion = { # TODO define conversions to m3/h
       #'CFS': 1, # cubic feet per second
       #'GPM': 1, # gallons per minute
       #'MGD': 1, # million gallons per day
       #'IMGD':1, #  Imperial MGD
       #'AFD': 1, # acre-feet per day
       'LPS': 3600/1000, # liters per second
       'LPM': 60/1000, # liters per minute
       #'MLD': 1, # million liters per day
       'CMH': 1, # cubic meters per hour
       'CMD': 1/24, # cubic meters per day
       'CMS': 3600, # cubic meters per second
       }[options['UNITS'][0]] if 'UNITS' in options else 1

    head_unit_convertion = { # TODO define conversions
       #'CFS': 1, #
       #'GPM': 1, #
       #'MGD': 1, #
       #'IMGD':1, #
       #'AFD': 1, #
       'LPS': 1, #
       'LPM': 1, #
       #'MLD': 1, #
       'CMH': 1, #
       'CMD': 1, #
       'CMS': 1, #
       }[options['UNITS'][0]] if 'UNITS' in options else 1

    coordinates = {}
    reverse_coordinates = {}
    for row in sections['COORDINATES']:
        id_, x, y = tokenize(row)
        x, y = float(x), float(y)
        while (x, y) in reverse_coordinates:
            feedback.pushWarning(f"coordinates {id_} is the same as {reverse_coordinates[(x,y)]}, offsetting")
            warnings += 1
            y += 1
        coordinates[id_] = (x, y)
        reverse_coordinates[(x, y)] = id_

    vertices = defaultdict(list)
    for row in sections['VERTICES']:
        id_, x, y = tokenize(row)
        vertices[id_].append((float(x), float(y)))

    patterns = defaultdict(list)
    patterns_comment = {}
    for row in sections['PATTERNS']:
        r = tokenize(row)
        if r[0] not in patterns_comment:
            patterns_comment[r[0]] = comment(row)
        patterns[r[0]] += [float(x) for x in r[1:]]

    curves = defaultdict(list)
    curves_comment = {}
    for row in sections['CURVES']:
        r = tokenize(row)
        if r[0] not in curves_comment:
            curves_comment[r[0]] = comment(row)
        curves[r[0]].append([float(x) for x in r[1:]])

    time_units = {
        'SECONDS': 1,
        'SEC': 1,
        'MINUTES': 60,
        'MIN': 60,
        'HOURS': 3600,
        'DAYS': 24*3600
        }
    times = {}

    for row in sections['TIMES']:
        row = tokenize(row)
        if row[-1].upper() in time_units:
            times[' '.join(row[:-2]).upper()] = float(row[-2])*time_units[row[-1].upper()]
        elif row[-1].find(':') != -1:
            hms = row[-1].split(':')
            h, m, s =  hms + [0]*(3-len(hms))
            times[' '.join(row[:-1]).upper()] = float(h)*3600 + float(m)*60 + float(s)
        elif row[-1].upper() in ('AM', 'PM'):
            times[' '.join(row[:-2]).upper()] = ' '.join(row[-2:])
        else:
            if (row[0]).upper() != "STATISTIC":
                feedback.pushWarning(f"{row[0].upper()} without any unit. Supposed to be hours.")
                warnings += 1
                if len(row) != 2:
                    feedback.pushWarning(f"{row} in wrong format")
                    warnings += 1
                else:
                    times[' '.join(row[:-1]).upper()] = float(row[-1])*3600

    if 'PATTERN TIMESTEP' in times and times['PATTERN TIMESTEP'] != 3600:
        if times['PATTERN TIMESTEP'] < 3600 and 3600%times['PATTERN TIMESTEP'] == 0:
            feedback.pushWarning(f"PATTERN TIMESTEP is {times['PATTERN TIMESTEP']/3600:.2f}h, averaging {3600//times['PATTERN TIMESTEP']} values to convert to 1h")
            warnings += 1
        elif times['PATTERN TIMESTEP'] > 3600 and times['PATTERN TIMESTEP']%3600 == 0:
            feedback.pushWarning(f"PATTERN TIMESTEP is {times['PATTERN TIMESTEP']/3600:.2f}h, duplicating values {times['PATTERN TIMESTEP']//3600}x to convert to 1h")
            warnings += 1
        else:
            feedback.reportError(f"PATTERN TIMESTEP is {times['PATTERN TIMESTEP']/3600:.2f}h, cannot be converted to 1h")
            errors += 1

    timestep = 3600 if 'PATTERN TIMESTEP' not in times else int(times['PATTERN TIMESTEP'])

    orig_pattern = dict(patterns)
    pattern_warnings = {}
    pattern_errors = {}
    pattern_for_demand = defaultdict(list)
    for k, pattern in patterns.items():

        # first handle the timestep by averaging or duplicating

        if timestep == 3600:
            pass
        elif timestep > 3600:
            if timestep%3600 == 0:
                pattern = list(chain.from_iterable([[v]*(timestep//3600) for v in pattern]))
            else:
                pattern = None # we already warned
        elif timestep < 3600:
            if 3600%timestep == 0:
                stride = 3600//timestep
                pattern = [sum(pattern[i*stride:i*stride+stride])/stride for i in range(len(pattern)//stride)]
            else:
                pattern = None # we already signaled error
                pass # we already warned

        # then either truncate or duplicate pattern to have a 24h duration

        if pattern is not None:
            if len(set(pattern)) == 1: # constant pattern
                # it will be converted to q0
                pass
            elif len(pattern) == 24:
                pass
            elif len(pattern) > 24:
                pattern_warnings[k] = f"pattern '{k}' is {len(pattern)}h, truncating"
                warnings += 1
                pattern = pattern[:24]
            elif len(pattern) < 24:
                if 24%len(pattern) == 0:
                    pattern_warnings[k] = f"pattern '{k}' is {len(pattern)}h, duplicating"
                    warnings += 1
                    pattern = pattern*(24//len(pattern))
                else:
                    pattern_errors[k] = f"pattern '{k}' is {len(pattern)}h and cannot be converted"
                    errors += 1
                    pattern = None

        patterns[k] = pattern


    with project.connect() as con, con.cursor() as cur:
        # create user_node
        junction_no = 0
        water_delivery_point_queries = []
        feedback.setProgressText('junctions')
        for row in sections['JUNCTIONS']:
            junction_no += 1
            feedback.setProgress(100*junction_no/len(sections['JUNCTIONS']))
            if feedback.isCanceled():
                feedback.reportError('CANCELLED')
                return warnings, errors
            id_, elev, demand, pattern = tokenize(row, 4)
            # Ajout Marjo 22/11/23
            if len(id_) > 24:
                feedback.reportError(f"Error : '{id_}' has more than 24 char !")
                errors += 1
                assert(False)
            ##
            demand = float(demand)*flow_units_conversion*24 if demand is not None else 0 #fix 09/10/24
            q0 = 0
            if pattern is not None:
                pattern_for_demand[pattern].append(id_)
                if patterns[pattern] is None:
                    pattern = None
                    demand = 0
                else:
                    tot = sum(patterns[pattern])
                    demand *= tot/24
                    if len(set(patterns[pattern])) == 1: # constant pattern, converts to q0
                        q0 = demand/24
                        demand = 0
                        pattern = None
                    else:
                        if tot == 0:
                            hourly_modulation_array = pg_array([[1, 1] for x in range(24)])
                            demand = 0
                        else:
                            hourly_modulation_array = pg_array([[24*x/tot, 24*x/tot] for x in patterns[pattern]])
                        cur.execute("""
                            insert into api.hourly_modulation_curve(name, hourly_modulation_array, comment)
                            select %s, %s::real[], %s
                            where not exists (select 1 from api.hourly_modulation_curve where name=%s)
                            """, (pattern, hourly_modulation_array, patterns_comment[pattern], pattern))

            if demand < 0:
                feedback.reportError(f"demand is negative ({demand}) for node '{id_}'")
                errors +=1
            elif demand > 0:
                water_delivery_point_queries.append(("""
                    with wdp as (select n.id, n.name, %s volume, st_transform(%s::geometry, %s) as geom from api.user_node n where name=%s and model=%s)
                    insert into api.water_delivery_point(name, volume, geom, pipe_link)
                    select name, volume, geom, (select l.id from api.pipe_link l where l.down=wdp.id or l.up=wdp.id limit 1)
                    from wdp
                    """, (demand*demand_multiplier,
                    f"SRID={src_srid}; POINT({coordinates[id_][0]} {coordinates[id_][1]})", dst_srid, id_, model)))

            cur.execute("""
                insert into api.user_node(name, model, domestic_curve, zground, depth, geom, comment, q0)
                values (%s, %s, %s, %s, 0, st_transform(%s::geometry, %s), %s, %s)
                """, (id_, model, pattern, elev,
                f"SRID={src_srid}; POINT({coordinates[id_][0]} {coordinates[id_][1]})", dst_srid, comment(row), q0*demand_multiplier))

            if junction_no%1000 == 0:
                con.commit()

        ext_file = os.path.join(project.directory, 'data', f'{model}_imposed_piezometry.dat')
        if os.path.exists(ext_file):
            os.remove(ext_file)

        # create imposed_piezometry_singularity
        for row in sections['RESERVOIRS']:
            id_, head, pattern = tokenize(row, 3)
            # Ajout Marjo 22/11/23
            if len(id_) > 24:
                feedback.reportError(f"Error : '{id_}' has more than 24 char !")
                errors += 1
                assert(False)
            ##
            head = float(head)
            external_file = False
            if pattern is not None:
                external_file = True
                with open(ext_file, 'a') as e:
                    e.write("'" + id_+ "'" + "\n")
                    t = 0
                    for v in orig_pattern[pattern]:
                        #t += timestep
                        t += timestep /3600 # A VERIFIER : passer en heure
                        e.write(f"{t} {v}"+"\n")

            geom = f"SRID={src_srid}; POINT({coordinates[id_][0]} {coordinates[id_][1]})"
            cur.execute("""
                insert into api.user_node(name, model, zground, depth, geom, comment)
                values (%s, %s, %s, 0, st_transform(%s::geometry, %s), %s)
                """, (id_, model, head, geom, dst_srid, comment(row)))
            cur.execute("""
                insert into api.imposed_piezometry_singularity(name, z0, geom, external_file)
                values (%s, %s, st_transform(%s::geometry, %s), %s)
                """, (id_, head, geom, dst_srid, external_file))

        # create reservoir_node
        for row in sections['TANKS']:
            #id_, elevation, initlevel, minlevel, maxlevel, diameter, minvol, volcurve = tokenize(row, 8)
            # modif Marjo 09/10/24
            # en EPANET 2.2, il y a 9 colonnes avec un overflow Yes/No
            # par ailleurs, volcurve peut être égal à *
            id_, elevation, initlevel, minlevel, maxlevel, diameter, minvol, volcurve, overflow = tokenize(row, 9)
            # Ajout Marjo 22/11/23
            if len(id_) > 24:
                feedback.reportError(f"Error : '{id_}' has more than 24 char !")
                errors += 1
                assert(False)
            ##
            elevation = float(elevation)
            z_ini = elevation + float(initlevel)
            z_bottom = elevation + float(minlevel)
            z_top = elevation + float(maxlevel)
            # Modif Marjo 09/10/24 : overflow compatible 2.0 et 2.2
            if overflow is None or overflow=='Yes':
                z_overflow = z_top
            elif overflow=='No':
                z_overflow = 9999
                feedback.reportError(f"Warning : tank '{id_}' has 'No' overflow in epanet model. Fixed at {z_overflow} in expresseau.")
            if volcurve is None or volcurve=="*":
                s = pi*float(diameter)**2/4
                sz_array = [[z_bottom, s], [z_top, s]]
            else:
                assert(curves[volcurve][0][0] == 0)
                assert(curves[volcurve][0][1] == 0)
                sz_array = []
                debug_arr = []

                vh = inverse_douglass_peuker(curves[volcurve], 10)
                R = Residual(vh)
                res = minimize(R.f, [1]*len(vh))
                for (h, v), r, vr in zip(vh, res.x, R.volume(res.x)):
                    debug_arr += [[h, r, v, vr]]
                    sz_array.append([elevation + h, pi*r**2])

            for i in range(len(sz_array)):
                if sz_array[i][1] > 99999:
                    feedback.pushWarning(f"reservoir {id_} has surface exceeding 99999m² at z {sz_array[i][0]}m, reducing")
                    warnings += 1
                    sz_array[i][1] = 99999


            cur.execute("""
                insert into api.reservoir_node(name, model, sz_array, z_ini, z_overflow, zground, depth, geom, comment)
                values (%s, %s, %s::real[], %s, %s, %s, 0, st_transform(%s::geometry, %s), %s)
                """, (id_, model, pg_array(sz_array), z_ini, z_overflow, elevation,
                f"SRID={src_srid}; POINT({coordinates[id_][0]} {coordinates[id_][1]})", dst_srid, comment(row)))

        con.commit()
        project.vacuum_analyse()

        # create pipe
        pipe_no = 0
        feedback.setProgressText('pipe')
        for row in sections['PIPES']:
            id_, node1, node2, length, diameter, roughness, minorloss, status = tokenize(row, 8)
            # Ajout Marjo 22/11/23
            if len(id_) > 24:
                feedback.reportError(f"Error : '{id_}' has more than 24 char !")
                errors += 1
                assert(False)
            ##
            pipe_no +=1
            feedback.setProgress(100*pipe_no/len(sections['PIPES']))
            if feedback.isCanceled():
                feedback.reportError('CANCELLED')
                return warning, errors
            vtx = ''.join([f"{x} {y}," for x,y in vertices[id_]]) if id_ in vertices else ''
            geom = f"SRID={src_srid}; LINESTRING({coordinates[node1][0]} {coordinates[node1][1]}, {vtx} {coordinates[node2][0]} {coordinates[node2][1]})"
            # TODO handle minorloss
            minorloss = float(minorloss)
            section = pi*(float(diameter)/1000)**2/4

            cur.execute("""
                select count(1) from api.reservoir_node
                where st_intersects(st_transform(st_startpoint(%s::geometry), %s), geom) or st_intersects(st_transform(st_endpoint(%s::geometry), %s), geom)
                """, (geom, dst_srid, geom, dst_srid))
            has_tank, = cur.fetchone()

            status = status if status is not None else 'Open' #fix 09/10/24 status peut être vide (ie Open) en 2.2

            if status.upper() == 'CV':
                cur.execute("select zground from api.node where model=%s and name=%s", (model, node1))
                z1, = cur.fetchone()
                cur.execute("select zground from api.node where model=%s and name=%s", (model, node2))
                z2, = cur.fetchone()
                zground = (z1+z2)/2

                CV_name = id_+'_CV' if len(id_)<21 else id_[0:20]+'_CV'
                snd_pipe_name = id_+'bis' if len(id_)<21 else id_[0:20]+'bis'

                cur.execute("""
                    insert into api.user_node(name, model, zground, depth, geom)
                    values(%s, %s, %s, 0, st_lineinterpolatepoint(st_transform(%s::geometry, %s),
                    greatest(.2, 0.33 - 1/st_length(st_transform(%s::geometry, %s)))))
                    """, ('u'+id_, model, zground ,geom, dst_srid, geom, dst_srid))
                cur.execute("""
                    insert into api.user_node(name, model, zground, depth, geom)
                    values(%s, %s, %s, 0, st_lineinterpolatepoint(st_transform(%s::geometry, %s),
                    least(.4, 0.33 + 1/st_length(st_transform(%s::geometry, %s)))))
                    """, ('d'+id_, model, zground ,geom, dst_srid, geom, dst_srid))
                cur.execute("""
                    insert into api.check_valve_link(name, section, cd_coef, geom)
                    values (%s, %s, %s, st_linesubstring(st_transform(%s::geometry, %s),
                    greatest(.2, 0.33 - 1/st_length(st_transform(%s::geometry, %s))),
                    least(.4, 0.33 + 1/st_length(st_transform(%s::geometry, %s)))))
                    """, (CV_name, section, 0.001, geom, dst_srid, geom, dst_srid, geom, dst_srid))
                cur.execute("""
                    insert into api.pipe_link(name, diameter, overload_length, overloaded_length, overload_roughness_mm, overloaded_roughness_mm, geom, comment)
                    values (%s, %s, true, %s, true, %s, st_linesubstring(st_transform(%s::geometry, %s),
                    0,
                    greatest(.2, 0.33 - 1/st_length(st_transform(%s::geometry, %s)))), %s)
                    returning id
                    """, (id_, float(diameter)/1000, float(length)/2, float(roughness), geom, dst_srid, geom, dst_srid, comment(row)))
                cur.execute("""
                    insert into api.pipe_link(name, diameter, overload_length, overloaded_length, overload_roughness_mm, overloaded_roughness_mm, geom, comment)
                    values (%s, %s, true, %s, true, %s, st_linesubstring(st_transform(%s::geometry, %s),
                    least(.4, 0.33 + 1/st_length(st_transform(%s::geometry, %s))),
                    1), %s)
                    returning id
                    """, (snd_pipe_name, float(diameter)/1000, float(length)/2, float(roughness), geom, dst_srid, geom, dst_srid, comment(row)))

            else:
                cur.execute("""
                    insert into api.pipe_link(name, diameter, overload_length, overloaded_length, overload_roughness_mm, overloaded_roughness_mm, status_open, geom, comment)
                    values (%s, %s, true, %s, true, %s, %s, st_transform(%s::geometry, %s), %s)
                    returning id
                    """, (id_, float(diameter)/1000, float(length), float(roughness), status.lower()=='open', geom, dst_srid, comment(row)))

            pipe_id, = cur.fetchone()

            if minorloss > 0:
                cur.execute("""
                    insert into api.pipe_link_singularity(pipe_link, borda_section, borda_coef, type_singularity)
                    values(%s, %s, %s, 'Borda headloss')
                    """, (pipe_id, section, minorloss))

            if pipe_no%1000 == 0:
                con.commit()

        # create valve_link, pressure_regulator_link, flow_regulator_link
        valve_no = 0
        feedback.setProgressText('valves')
        for row in sections['VALVES']:
            id_, node1, node2, diameter, type_, setting, minorloss = tokenize(row, 7)
            # Ajout Marjo 22/11/23
            if len(id_) > 24:
                feedback.reportError(f"Error : '{id_}' has more than 24 char !")
                errors += 1
                assert(False)
            ##
            valve_no += 1
            feedback.setProgress(100*valve_no/len(sections['VALVES']))
            if feedback.isCanceled():
                feedback.reportError('CANCELLED')
                return warnings, errors
            vtx = ''.join([f"{x} {y}," for x,y in vertices[id_]]) if id_ in vertices else ''
            geom = f"SRID={src_srid}; LINESTRING({coordinates[node1][0]} {coordinates[node1][1]}, {vtx} {coordinates[node2][0]} {coordinates[node2][1]})"
            section = pi*(float(diameter)/1000)**2/4
            minorloss = float(minorloss)
            headloss = minorloss if minorloss!=0 else 0.001
            type_ = type_.upper()
            pressure_regulation_mode = {
                'PRV': 'Imposed downstream pressure',
                'PSV': 'Imposed upstream pressure',
                'PBV': 'Imposed differential pressure',
                'GPV': 'Imposed differential pressure'}
            if type_ in pressure_regulation_mode:
                ad_comment = ''
                if type_ == 'GPV':
                   regul_pq_array = pg_array(curves[setting])
                   pressure = None
                   mode = 'Curve P(Q)'
                else:
                   regul_pq_array = '{{0,0}}'
                   pressure = float(setting)
                   if pressure < 0:
                       pressure = 0
                       ad_comment = 'attention : consigne de pression négative passée à 0.\n '
                   mode = 'Constant pressure'
                   if type_ == 'PBV':
                       ad_comment = "attention : vérifier si cet élément ne serait pas un artifice de modélisation pour un surpresseur.\n "
                cur.execute("""
                    insert into api.pressure_regulator_link(name, section, headloss_coef, pressure_regulation_mode, regul_p, geom, pressure_regulation_option, regul_pq_array, comment)
                    values (%s, %s, %s, %s, %s, st_transform(%s::geometry, %s), %s, %s, %s)
                    """, (id_, section, headloss, pressure_regulation_mode[type_], pressure, geom, dst_srid, mode, regul_pq_array, ((comment(row) or '') + ad_comment) or None))
            elif type_ == 'FCV':
                flow = float(setting)
                cur.execute("""
                    insert into api.flow_regulator_link(name, section, headloss_coef, regul_q, geom, comment)
                    values (%s, %s, %s, %s, st_transform(%s::geometry, %s), %s)
                    """, (id_, section, headloss, flow, geom, dst_srid, comment(row)))
            elif type_ == 'TCV':
                cur.execute("""
                    insert into api.valve_link(name, section, headloss_coef, geom, comment)
                    values (%s, %s, %s, st_transform(%s::geometry, %s), %s)
                    """, (id_, section, headloss, geom, dst_srid, comment(row)))
            else:
                feedback.reportError(f"unknown valve type '{type_}'")
                errors += 1
                assert(False)

        con.commit()
        project.vacuum_analyse()

        # create pump
        pump_no = 0
        feedback.setProgressText('pumps')
        for row in sections['PUMPS']:
            pump_no += 1
            feedback.setProgress(100*pump_no/len(sections['PUMPS']))
            if feedback.isCanceled():
                feedback.reportError('CANCELLED')
                return warnings, errors
            r = tokenize(row)
            id_, node1, node2= r[:3]
            # Ajout Marjo 22/11/23
            if len(id_) > 24:
                feedback.reportError(f"Error : '{id_}' has more than 24 char !")
                errors += 1
                assert(False)
            ##
            # TODO handle pumps parameters, not sure every behavior can be converted to expresseau
            parameters = {k: v for k, v in zip(r[3:-1:2], r[4::2])}
            pq_array = None
            rotation_speed = 1
            inertia = 1
            if 'POWER' in parameters: # power value for constant energy pump, hp (kW)
                power = float(parameters['POWER'])*1000
                #pq_array = [[0, 100.01]] + [[power/(100*0.1**i), 100*0.1**i] for i in range(45)] + [[power/0.01, 0]]
                pq_array = pump_curve_from_power(power) # Modif Marjo 10/10/24
                #print('pq_array après création:', pq_array)

            elif 'HEAD' in parameters: # ID of curve that describes head versus flow for the pump
                pq_array = [[q*flow_units_conversion, p*head_unit_convertion] for q, p in curves[parameters['HEAD']]] #fix marjo 30/06/23
                if len(pq_array) == 1:
                    pq_array = [[0, 1.33*pq_array[0][1]]] + pq_array + [[2*pq_array[0][0], 0]]
                if len(pq_array) == 3: #bug si la courbe a 3 points au départ (et pas 1 qui a été traité au dessus et en a 3 ensuite)
                    # fits p = A - B q^C
                    A, B, C = powercurve(pq_array)
                    qmax = (-A/B)**(1/C)
                    pq_array = [[(i/9)*qmax, A + B*((i/9)*qmax)**C] for i in range(10)]
                    pq_array[-1][1] = 0
                else:
                    #fix marjo 30/06/23
                    if pq_array[0][0] != 0:
                        pq_array = [[0, pq_array[0][1]*1.01]] + pq_array
                    if pq_array[-1][1] != 0:
                        pq_array += [[pq_array[-1][0]*1.01, 0]]

            if 'SPEED' in parameters: # relative speed setting (normal speed is 1.0, 0 means pump is off)
                fct = float(parameters['SPEED'])
                #fix marjo 30/06/23
                if fct==0:
                    rotation_speed = 0
                else:
                    pq_array = [[q*fct, p*fct**2] for q, p in pq_array]

            if 'PATTERN' in parameters: # ID of ti
                feedback.pushWarning(f"time variation for pump '{id_}' not supported")
                warnings += 1

            if len(pq_array) > 10:
                pq_array = list(reversed(inverse_douglass_peuker(list(reversed(pq_array)), 10)))

            #print('pq_array avant mes corrections', pq_array)
            if pq_array[0][0]==0 and pq_array[-1][-1]==0:
                #print("courbe à l'envers, on inverse")
                pq_array = pq_array[::-1]
            elif pq_array[0][1]==0 and pq_array[-1][0]==0:
                #print("courbe à l'endroit")
                pass
            #print('pq_array après mes corrections', pq_array)

            vtx = ''.join([f"{x} {y}," for x,y in vertices[id_]]) if id_ in vertices else ''
            geom = f"SRID={src_srid}; LINESTRING({coordinates[node1][0]} {coordinates[node1][1]}, {vtx} {coordinates[node2][0]} {coordinates[node2][1]})"

            cur.execute("""
                select count(1) from api.reservoir_node
                where st_intersects(st_transform(st_startpoint(%s::geometry), %s), geom)
                or st_intersects(st_transform(st_endpoint(%s::geometry), %s), geom)
                """, (geom, dst_srid, geom, dst_srid))
            has_tank, = cur.fetchone()

            if has_tank:

                cur.execute("""
                select zground from api.node where model=%s and name=%s
                """, (model, node1))
                zground, = cur.fetchone()

                cur.execute("""
                    select count(1) from api.user_node
                    where st_intersects(st_lineinterpolatepoint(st_transform(%s::geometry, %s),
                    greatest(.2, 0.33 - 1/st_length(st_transform(%s::geometry, %s)))), geom)
                    """, (geom, dst_srid, geom, dst_srid))
                is_superimposed, = cur.fetchone()

                comment_pipe = 'pipe needed to connect pump to reservoir node'

                if not is_superimposed:

                    cur.execute("""
                            insert into api.user_node(name, model, zground, depth, geom)
                            values(%s, %s, %s, 0, st_lineinterpolatepoint(st_transform(%s::geometry, %s),
                            greatest(.2, 0.33 - 1/st_length(st_transform(%s::geometry, %s)))))
                            """, (id_+'_NOD1', model, zground ,geom, dst_srid, geom, dst_srid))
                    cur.execute("""
                            insert into api.user_node(name, model, zground, depth, geom)
                            values(%s, %s, %s, 0, st_lineinterpolatepoint(st_transform(%s::geometry, %s),
                            least(.4, 0.33 + 1/st_length(st_transform(%s::geometry, %s)))))
                            """, (id_+'_NOD2', model, zground ,geom, dst_srid, geom, dst_srid))
                    cur.execute("""
                            insert into api.pipe_link(name, diameter, geom, comment)
                            values (%s, %s, st_linesubstring(st_transform(%s::geometry, %s),
                            0,
                            greatest(.2, 0.33 - 1/st_length(st_transform(%s::geometry, %s)))),
                            %s)
                            """, (id_+'_U', 1, geom, dst_srid, geom, dst_srid, ', '.join([str(comment(row)), comment_pipe])))
                    cur.execute("""
                            insert into api.pipe_link(name, diameter, geom, comment)
                            values (%s, %s, st_linesubstring(st_transform(%s::geometry, %s),
                            least(.4, 0.33 + 1/st_length(st_transform(%s::geometry, %s))),
                            1),%s)
                            """, (id_+'_D', 1, geom, dst_srid, geom, dst_srid, ', '.join([str(comment(row)), comment_pipe])))


                cur.execute("""
                        insert into api.pump_link(name, check_valve, rotation_speed, inertia, pq_array, geom, comment)
                        values (%s, False, %s, %s, %s, st_linesubstring(st_transform(%s::geometry, %s),
                        greatest(.2, 0.33 - 1/st_length(st_transform(%s::geometry, %s))),
                        least(.4, 0.33 + 1/st_length(st_transform(%s::geometry, %s)))), %s)
                        """, (id_, rotation_speed, inertia, pg_array(pq_array), geom, dst_srid, geom, dst_srid, geom, dst_srid, comment(row)))

            else:
                cur.execute("""
                    insert into api.pump_link(name, check_valve, rotation_speed, inertia, pq_array, geom, comment)
                    values (%s, False, %s, %s, %s, st_transform(%s::geometry, %s), %s)
                    """, (id_, rotation_speed, inertia, pg_array(pq_array), geom, dst_srid, comment(row)))


        # create water_delivery_point
        wd_no = 0
        feedback.setProgressText('water delivery points')
        for query, param in water_delivery_point_queries:
            wd_no += 1
            feedback.setProgress(100*wd_no/len(water_delivery_point_queries))
            if feedback.isCanceled():
                feedback.reportError('CANCELLED')
                return warnings, errors
            cur.execute(query, param)


        # update nodes according to demands
        demand_for_node = {}
        for row in sorted(sections['DEMANDS']):
            id_, demand, pattern = tokenize(row, 3)
            demand = float(demand)*flow_units_conversion*24
            q0 = 0
            if pattern is not None:
                pattern_for_demand[pattern].append(id_)
                if patterns[pattern] is None:
                    pattern = None
                    demand = 0
                else:
                    tot = sum(patterns[pattern])
                    demand *= tot/24
                    if len(set(patterns[pattern])) == 1: # constant pattern, converts to q0
                        q0 = demand/24
                        demand = 0
                        pattern = None
                    else:
                        if tot == 0:
                            hourly_modulation_array = pg_array([[1, 1] for x in range(24)])
                            demand = 0
                        else:
                            hourly_modulation_array = pg_array([[24*x/tot, 24*x/tot] for x in patterns[pattern]])
                        cur.execute("""
                            insert into api.hourly_modulation_curve(name, hourly_modulation_array, comment)
                            select %s, %s::real[], %s
                            where not exists (select 1 from api.hourly_modulation_curve where name=%s)
                            """, (pattern, hourly_modulation_array, patterns_comment[pattern], pattern))

            if id_ in demand_for_node:
                demand_for_node[id_] = {
                    'demand': demand_for_node[id_]['demand'] + ([demand] if demand > 0 else []),
                    'pattern': demand_for_node[id_]['pattern'] + ([pattern] if pattern is not None and demand > 0 else []),
                    'q0': demand_for_node[id_]['q0'] + q0
                    }
            else:
                demand_for_node[id_] = {
                    'demand': [demand] if demand > 0 else [],
                    'pattern': [pattern] if pattern is not None and demand > 0 else [],
                    'q0': q0
                    }

        demand_no = 0
        feedback.setProgressText('demands')
        for id_, demand in demand_for_node.items():
            demand_no +=1
            feedback.setProgress(100*demand_no/len(demand_for_node))
            if feedback.isCanceled():
                feedback.reportError('CANCELLED')
                return warnings, errors

            cur.execute("delete from api.water_delivery_point where name=%s", (id_,))

            # modif Marjo 23/01/25 : on peut avoir plein de pattern mais qu'en fait ce soit le même nom donc il ne faut pas limiter à 2
            unique_pattern_dico = {}
            for i in range(len(demand['pattern'])):
                pattern_name = demand['pattern'][i]
                pattern_volume = demand['demand'][i]
                if pattern_name in unique_pattern_dico.keys():
                    unique_pattern_dico[pattern_name] += pattern_volume
                else :
                    unique_pattern_dico[pattern_name] = pattern_volume

            if len(unique_pattern_dico) > 2:
                feedback.reportError(f"node {id_} has more than two demand-patterns ({len(unique_patterns)})")
                feedback.reportError("(the 3rd and subsequent ones are ignored)")
                errors += 1
            else:
                cur.execute("""
                    update api.user_node set domestic_curve=%s, industrial_curve=%s, q0=%s where name=%s and model=%s
                    """, (
                    list(unique_pattern_dico.keys())[0] if len(unique_pattern_dico) > 0 else None,
                    list(unique_pattern_dico.keys())[1] if len(unique_pattern_dico) > 1 else None,
                    demand['q0'] * demand_multiplier,
                    id_,
                    model))

            if len(unique_pattern_dico) > 0:
                demand_volume = unique_pattern_dico[list(unique_pattern_dico.keys())[0]]
                cur.execute("""
                    with wdp as (select n.id, n.name, %s volume, st_transform(%s::geometry, %s) as geom from api.user_node n where name=%s and model=%s)
                    insert into api.water_delivery_point(name, volume, geom, pipe_link)
                    select name||'_1', volume, geom, (select l.id from api.pipe_link l where l.down=wdp.id or l.up=wdp.id limit 1)
                    from wdp
                    """, (
                    demand_volume*demand_multiplier,
                    f"SRID={src_srid}; POINT({coordinates[id_][0]} {coordinates[id_][1]})", dst_srid,
                    id_, model))

            if len(unique_pattern_dico) > 1:
                demand_volume = unique_pattern_dico[list(unique_pattern_dico.keys())[1]]
                cur.execute("""
                    with wdp as (select n.id, n.name, %s volume, st_transform(%s::geometry, %s) as geom from api.user_node n where name=%s and model=%s)
                    insert into api.water_delivery_point(name, volume, geom, pipe_link)
                    select name||'_2', volume, geom, (select l.id from api.pipe_link l where l.down=wdp.id or l.up=wdp.id limit 1)
                    from wdp
                    """, (
                    demand_volume*demand_multiplier,
                    f"SRID={src_srid}; POINT({coordinates[id_][0]} {coordinates[id_][1]})", dst_srid,
                    id_, model))

            if demand_no%1000 == 0:
                con.commit()

        for row in sorted(sections['STATUS']):
            id_, status = tokenize(row)
            opened = status.upper()=='OPEN'
            cur.execute("update api.pipe_link set status_open=%s where name=%s", (opened ,id_))
            cur.execute("update api.pump_link set speed_reduction_coef=%s where name=%s", (1 if opened else 0, id_))
            cur.execute("update api.valve_link set c_opening=%s where name=%s", (1 if opened else 0, id_))
            cur.execute("update api.pressure_regulator_link set regul_p=0, pressure_regulation_mode='Imposed upstream pressure', comment='forced to stay OPEN' where name=%s", (id_,))
            cur.execute("update api.flow_regulator_link set regul_q=9999, comment='forced to stay OPEN' where name=%s", (id_,))
            if not opened:
                cur.execute("update api.pressure_regulator_link set regul_p=0, pressure_regulation_mode='Imposed downstream pressure', comment='forced to stay CLOSED' where name=%s", (id_,))
                cur.execute("update api.flow_regulator_link set regul_q=0, comment='forced to stay CLOSED' where name=%s", (id_,))

        for k, v in pattern_for_demand.items():
            if k in pattern_warnings:
                feedback.pushWarning(pattern_warnings[k])
                warnings += 1
            if k in pattern_errors:
                feedback.reportError(pattern_errors[k])
                errors += 1

        # offset water delivery points
        #cur.execute("""
        #    update api.water_delivery_point set geom=st_translate(geom, 0, 1)
        #    """)


        con.commit()
        project.vacuum_analyse()

        feedback.pushInfo("controls and rules translation...")
        w, e = inp_to_ctl(project, model, path, feedback, enc)

        #ajout du/des scn de calcul

        if "DURATION" in times:
            sim_duration = times['DURATION']
        else:
            sim_duration = 24

        sim_duration_hms = str(timedelta(seconds = sim_duration))
        feedback.pushInfo(f" Durée de simulation : {sim_duration_hms}")

        if "HYDRAULIC TIMESTEP" in times:
            sim_timestep = times['HYDRAULIC TIMESTEP']
        else:
            sim_timestep = 300
        sim_ini = 3

        cur.execute(f'''
        insert into api.scenario(name, comput_mode, duration, timestep, tini, dt_output, tend_output_hr)
         values
        ('{model}_noctl', 'Gradual transient', '{sim_duration_hms}', {sim_timestep}, '{sim_ini}:00:00', {sim_timestep}, '{sim_duration_hms}')
        ''')

        ext_file = os.path.join(project.directory, 'data', f'{model}_imposed_piezometry.dat')
        if os.path.exists(ext_file):
            cur.execute(f'''
            insert into api.hydrology_scenario(hydrology_file, scenario)
            values
            ('{ext_file}', '{model}_noctl')
            ''')

        ctl_file = os.path.join(project.directory, 'data', f"{model}.ctl")
        if os.path.exists(ctl_file):
            cur.execute(f'''
            insert into api.scenario(name, comput_mode, duration, timestep, tini, dt_output, tend_output_hr)
            values
            ('{model}_ctl', 'Gradual transient', '{sim_duration_hms}', {sim_timestep}, '{sim_ini}:00:00',{sim_timestep}, '{sim_duration_hms}')
            ''')
            cur.execute(f'''
            insert into api.regulation_scenario(priority, regulation_file, scenario)
            values
            ('1', '{ctl_file}', '{model}_ctl')
            ''')
            if os.path.exists(ext_file):
                cur.execute(f'''
                insert into api.hydrology_scenario(hydrology_file, scenario)
                values
                ('{ext_file}', '{model}_ctl')
                ''')
        else:
            pass


        feedback.pushInfo(f" Computation scenario created \n")

        # A REACTIVER !!!
#         cur.execute(f'''
#         insert into api.water_delivery_sector(geom)
#         select ST_multi(ST_Buffer(ST_ConcaveHull(ST_Union(u.geom), 0.9), 1)) from api.user_node u''')

        con.commit()
    project.vacuum_analyse()

    return warnings + w, errors + e



if __name__=='__main__':
    import sys
    from expresseau.project import Project
    from expresseau.database import project_exists, create_project
    from expresseau.utility.log import Feedback
    import getopt

    optlist, args = getopt.getopt(sys.argv[1:], 's:e:d:', ["source_srid=", "encoding=", "project_srid"])
    optlist = dict(optlist)
    project_name, model_name, path = args[-3:]

    srid = int(optlist['-d']) if '-d' in optlist else None
    enc = optlist['-e'] if '-e' in optlist else 'utf-8'
    src_srid = optlist['-s']

    is_new = not project_exists(project_name)
    if is_new:
        create_project(project_name, srid)

    project = Project(project_name, debug=False)

    print("model_name=", model_name)
    project.add_new_model(model_name)

    import_(project, model_name, path, Feedback(), enc, src_srid)