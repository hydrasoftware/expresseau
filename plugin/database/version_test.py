# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of EXPRESSEAU, a QGIS plugin for hydraulics                          ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


"""
run database tests

USAGE

   database_test.py [-dkh]

OPTIONS

   -h, --help
        print this help

   -d, --debug
        run in debug mode (lots of output)

   -k, --keep
        keeps database after test

"""

assert(__name__ == "__main__")

import sys
import getopt
import psycopg2
import logging
import os
from plugin.database import is_expresseau_db, project_exists, remove_project, import_db, update_db

__currendir = os.path.dirname(__file__)

try:
    optlist, args = getopt.getopt(sys.argv[1:],
            "hdk",
            ["help", "debug", "keep"])
except Exception as e:
    sys.stderr.write(str(e)+"\n")
    exit(1)

optlist = dict(optlist)

if "-h" in optlist or "--help" in optlist:
    help(sys.modules[__name__])
    exit(0)

debug = "-d" in optlist or "--debug" in optlist
keep = "-k" in optlist or "--keep" in optlist

# test database creation
dbname = "version_xtest"

if project_exists(dbname):
    remove_project(dbname)

import_db(dbname, os.path.join(__currendir, 'test_data', 'template_expresseau_v0.1.sql'))

assert(is_expresseau_db(dbname))

update_db(dbname)

if not keep:
    remove_project(dbname)

sys.stdout.write("ok\n")
