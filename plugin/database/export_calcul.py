# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of EXPRESSEAU, a QGIS plugin for hydraulics                          ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

"""
export database for computation

USAGE

    python -m plugin.database.export_calcul <project> <scenario>
"""

import os
import re
import time
import shutil
from qgis.PyQt.QtCore import QObject, pyqtSignal

_expresseau_dir = os.path.join(os.path.expanduser('~'), ".expresseau")

def replace_files_in_cmd(content, work_dir):
    """
       if external files are not found, try to change the path up to .expresseau/
       raise error if the replacement does not exist
    """
    old_content = content
    section = None
    content = ''
    for line in old_content.split('\n'):
        if line.strip() == '':
            section = None
        if section in ('*f_regul', '*f_hy_externe'):
            if not os.path.exists(line) and not os.path.exists(os.path.join(work_dir, line)):
                fil = re.sub(r'.*\.expresseau', _expresseau_dir.replace('\\', '/'), line.replace('\\', '/'))
                if not os.path.exists(fil):
                    raise ExportError(f'file {line} not found' + (f' and replacement file {fil} not found either' if line != fil else ''))
                line = fil

        if line.strip().startswith('*'):
            section = line.strip().lower()
        content += line+'\n'

    return content

class ExportError(Exception):
    pass

class ExportCalcul(QObject):
    log = pyqtSignal(str)

    def __init__(self, project, directory=None, parent=None):
        QObject.__init__(self, parent)

        self.project = project
        self.directory = directory or self.project.directory

    def __init_folder(self, folder):
        if os.path.isdir(folder):
            shutil.rmtree(folder)
        os.mkdir(folder)

    def export(self, scenario):
        assert scenario in self.project.scenarios

        scn_dir = os.path.join(self.directory, scenario.upper())
        work_dir = os.path.join(scn_dir, "travail")
        hydro_dir = os.path.join(scn_dir, "hydraulique")

        self.__init_folder(scn_dir)
        self.__init_folder(work_dir)
        self.__init_folder(hydro_dir)

        if self.project.engine == 'epanet':
            from .export_inp import export_
            models = self.project.fetchall(f"select regexp_split_to_table(models, ' ') from api.model_grouping('{scenario}') order by priority;")
            assert(len(models)==1)
            assert(len(models[0])==1)
            model = models[0][0]
            export_(os.path.join(hydro_dir, scenario+'.inp'), self.project, model, scenario)
            return

        ref_scn, comput_mode, model_connect_settings, start_time_h, skeletonization = self.project.fetchone(f"""
            select scenario_ref is not null, comput_mode, model_connect_settings, extract(epoch from trstart_hr)::float/3600., skeletonization
            from api.scenario where name='{scenario}'
            """)

        if comput_mode.startswith('Exterior fire defense') or comput_mode == 'Break criticality':
            data_dir = os.path.join(self.directory, "data", comput_mode.lower().replace(' ', '_'))
            if not os.path.isdir(data_dir):
                os.makedirs(data_dir, exist_ok=True)

        if comput_mode.startswith('Exterior fire defense'):
            # create a regulation file for each hydrant in the scenario directory

            ### fix marjo 25/05/2023

            if model_connect_settings == "Mixed":
                res = self.project.fetchall(f"""
                    with m as (
                        select g.model
                        from api.groupe_model as g, api.mixed as m, api.scenario as s
                        where m.groupe = g.groupe and m.scenario = s.name and s.name = '{scenario}'
                    )
                    select h.name, h.nominal_flow_m3h, h.draw_time_h, n.name, n.model
                    from api.fire_hydrant h
                    join api.node n on n.id = h._node
                    join m on m.model = n.model
                    where n.id not in (select id from api.node_without_piezo)
                    """)
            else:
                res = self.project.fetchall("""
                        select h.name, h.nominal_flow_m3h, h.draw_time_h, n.name, n.model
                        from api.fire_hydrant h
                        join api.node n on n.id=h._node
                        where n.id not in (select id from api.node_without_piezo)
                        """)

            for name, nominal_flow_m3h, draw_time_h, node_name, model in res:
                for dir_ in (scn_dir, data_dir):
                    if comput_mode == 'Exterior fire defense nominal flow':
                        with open(os.path.join(dir_, name.replace(' ', '_')+'.ctl'), 'w') as ctl:
                            ctl.write(f"$case ({model})" + "\n\n"
                            + f"CONS '{node_name}'" + "\n"
                            + f"IF ( TIME GE {start_time_h:.2f} ) THEN ( QINC {nominal_flow_m3h} ) ENDIF" + "\n"
                            + f"IF ( TIME GT {start_time_h + draw_time_h:.2f} ) THEN ( QINC 0 ) ENDIF" + "\n\n"
                            )
                    elif comput_mode == 'Exterior fire defense flow at 1bar':
                        with open(os.path.join(dir_, name.replace(' ', '_')+'.ctl'), 'w') as ctl:
                            ctl.write(f"$case ({model})" + "\n\n"
                            + f"CONS '{node_name}'\n"
                            + "QINC SEQUENCE\n"
                            + f"{start_time_h} 0\n"
                            + f"{start_time_h + 1} {10*nominal_flow_m3h}\n\n"
                            )
                    else:
                        assert False

        elif comput_mode == 'Break criticality':

            if model_connect_settings == "Mixed":
                res = self.project.fetchall(f"""
                    with m as (
                        select g.model
                        from api.groupe_model as g, api.mixed as m, api.scenario as s
                        where m.groupe = g.groupe and m.scenario = s.name and s.name = '{scenario}'
                    )
                    select p.name, p._model
                    from api.pipe_link p
                    join m on m.model = p._model
                    where p.id not in (select id from api.link_without_piezo)
                    """)
            else:
                res = self.project.fetchall("""
                        select p.name, p._model
                        from api.pipe_link p
                        where p.id not in (select id from api.link_without_piezo)
                        """)

            for pipe_name, model in res:
                for dir_ in (scn_dir, data_dir):
                    with open(os.path.join(dir_, pipe_name.replace(' ', '_')+'.ctl'), 'w') as ctl:
                        ctl.write(f"$case ({model})" + "\n\n"
                        + f"CANA '{pipe_name}'" + "\n"
                        + f"IF ( TIME GE {start_time_h:.2f} ) THEN ( STATUT 0. ) ENDIF" + "\n\n"
                        )

        for dir, file, fct, condition in [
            (scn_dir, "scenario.nom",   f"api.file_scnnom('{scenario}')", True),
            (work_dir, scenario.upper()+".cmd", f"api.file_cmd('{scenario}')", True),
            (work_dir, scenario.upper()+".qts", f"coalesce(api.file_qts(water_delivery_scenario), '') from api.scenario where name='{scenario}'", True),
            (work_dir, scenario.upper()+".cts",  "coalesce(api.file_cts(), '')", True),
            (work_dir, scenario.upper()+".rac",  "api.file_rac()", True),
            (work_dir, "phys.dat",        "api.file_phys_dat()", True),
            (work_dir, "act.dat",         "coalesce(api.file_act_dat(), '')", True),
            (work_dir, "optsor.dat",      "api.file_optsor_dat()", True)
            ] + [
            (work_dir, f"{model.upper()}.dat", f"api.file_dat('{scenario}', '{model}')", not ref_scn)
                for model, in self.project.fetchall(f"select regexp_split_to_table(models, ' ') from api.model_grouping('{scenario}') order by priority;")
            ] + [
            (work_dir, f"{scenario.upper()}_{model.upper()}_config.dat", f"api.file_config_dat('{scenario}', '{model}')", True)
                for model, in self.project.fetchall(f"select regexp_split_to_table(models, ' ') from api.model_grouping('{scenario}') order by priority;")
            ] :
            if condition:
                try:
                    strt = time.time()
                    self.__log(f"Writing file: {file}")
                    content = self.project.fetchone(f"select {fct};")[0].strip()
                    if file.endswith('_config.dat') and re.match(r'^\n*$', content): # empty config, don't write the file
                        self.__log(f"               done in {time.time() - strt :.1f}s")
                        continue
                    if file.endswith('.cmd'): # replace missing files if found in .expresseau
                        content = replace_files_in_cmd(content, work_dir)

                    with open(os.path.join(dir, file), "w", newline='') as f:
                        # Removes multiple whitelines & whitespaces at the end of files
                        # Adds 2 linebreaks at end of files
                        f.write(re.sub(r'\n\s*\n', '\n\n',  content + "\n\n"))
                    self.__log(f"               done in {time.time() - strt :.1f}s")
                except Exception as error:
                    self.__log(f"Error writing file: {file} ({str(error)})")
                    raise


    def __log(self, text):
        self.log.emit(text)
        if 'Error' in text:
            self.project.log.error(text)
            raise ExportError(text)
        else:
            self.project.log.notice(text)


if __name__=="__main__":
    import sys
    from ..project import Project

    project_name = sys.argv[1]
    scenario_name = sys.argv[2]

    project = Project(project_name)
    exporter = ExportCalcul(project)
    exporter.export(scenario_name)
