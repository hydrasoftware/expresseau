# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

import os
import re
import signal
from shutil import which
from subprocess import Popen, PIPE, STDOUT
if os.name == 'nt':
    from subprocess import STARTUPINFO, STARTF_USESHOWWINDOW
from ..project import Project
from ..qgis_utilities import QGisProjectManager
from qgis.core import QgsDataSourceUri, QgsVectorLayer, QgsVectorFileWriter
from ..service import get_service

class Animo():

    #def __init__(self, directory, project, scenario, model, parent=None):
    def __init__(self, project, scenario, model):

        self.directory = os.path.dirname(__file__)

        self.__project = project
        self.__scenario = scenario
        self.__scenario = scenario
        self.__model = model
        self.__hyd_dir = os.path.join(self.__project.directory, self.__scenario.upper(), "hydraulique")
        self.__w16_file = os.path.join(self.__hyd_dir, f'''{self.__scenario.upper()}_{self.__model.upper()}.w16''')

        # export de la table pipe_link en shapefile
        self.__pipe_link_shp = os.path.join(self.__project.directory, "data", f"api_pipe_link_{model}.shp")

        if not os.path.exists(self.__pipe_link_shp):

            source = QgsDataSourceUri()
            source.setConnection("localhost", "5454", project.name, "hydra", "hydra") #("localhost", "5432", "database_name", "username", "password")
            source.setDataSource("api", "pipe_link", "geom")

            layer = QgsVectorLayer(f"""
                service='{get_service()}'
                dbname='{project.name}'
                key='id'
                checkPrimaryKeyUnicity='0'
                table="(select id, geom, name from api.pipe_link where _model='{model}')"
                (geom)
                """, f"api_pipe_link_{model}.shp", 'postgres')

            err = QgsVectorFileWriter.writeAsVectorFormat(layer, self.__pipe_link_shp, "utf-8", driverName="ESRI Shapefile")


    def animo(self):
        '''callable function thats runs animo'''
        # Find path to program
        animo = which('animo') # WTF
        if animo is None:
            animo_exe = os.path.join(os.path.dirname(__file__), "wanimolt.exe")
            if os.path.isfile(animo_exe):
                animo = animo_exe
            else:
                self.log.error(f"Animo error: wanimolt.exe not found in {os.path.dirname(__file__)}")
                raise ExtractError(f"wanimolt.exe not found in {os.path.dirname(__file__)}")

        if os.name == 'nt':
            si = STARTUPINFO()
            si.dwFlags |= STARTF_USESHOWWINDOW
        else : # linux
            si = None

        try:
            self.__process = Popen([animo, self.__w16_file, f"/COORD:{self.__pipe_link_shp}"], stdin=PIPE, stdout=PIPE, stderr=STDOUT, encoding="mbcs", cwd=self.directory, startupinfo=si)
            for stdout_line in iter(self.__process.stdout.readline, ""):
                self.__output_log(stdout_line)
            self.__process.wait()
        except:
            os.kill(self.__process.pid, signal.CTRL_C_EVENT)

