# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of EXPRESSEAU, a QGIS plugin for hydraulics                          ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


"""
run expresseau calculation

USAGE

   python -m kernel <project name>, <scenario name>

OPTIONS

   -h, --help
        outputs this help
"""

import os
import sys
import datetime
from subprocess import Popen, PIPE, STDOUT
if os.name == 'nt':
    from subprocess import STARTUPINFO, STARTF_USESHOWWINDOW
from qgis.PyQt.QtWidgets import QApplication, QDialog
from qgis.PyQt.QtCore import QObject, pyqtSignal
from ..project import Project
from ..utility.license import license_file, program_path

ENCODING = sys.getdefaultencoding()

class KernelError(Exception):
    pass

class Kernel(QObject):
    log = pyqtSignal(str)
    output = pyqtSignal(str)
    kernel_dir = os.path.abspath(os.path.dirname(__file__))

    @staticmethod
    def get_error(fname):
        if not os.path.isfile(fname):
            return -1
        else:
            err = 0
            with open(fname) as f:
                content = f.readlines()
                err = int(content[0])
            return err

    @staticmethod
    def readfile(fname):
        if not os.path.isfile(fname):
            return ""
        else:
            content=""
            with open(fname, 'r', encoding=ENCODING) as f:
                content = f.readlines()
            return content

    def __init__(self, project, scenario, parent=None, output=None):
        '''creates an object to start computations of a given project's scenario
            - project: object project
            - scenario: scenario's id'''
        QObject.__init__(self, parent)

        self.__project = project
        self.__scenario = scenario
        self.__process = None

        self.__cur_dir = os.getcwd()
        self.__calc_dir = os.path.join(self.__project.directory, self.__scenario.upper(), "travail")
        self.__hyd_dir = os.path.join(self.__project.directory, self.__scenario.upper(), "hydraulique")
        if output is not None:
            self.output.connect(output)

    def run(self):
        '''runs the Expresseau kernel processes'''

        self.__log("Calculation starting: {}".format(self.__scenario))

        if self.__project.engine == 'epanet':
            self.execute('epanet', self.__hyd_dir, [
                os.path.join(self.__hyd_dir, self.__scenario+'.inp'),
                os.path.join(self.__hyd_dir, self.__scenario+'.log'),
                os.path.join(self.__hyd_dir, self.__scenario+'.bin')
                ])
            return


        err_gen=self.execute("xeaugen", self.__calc_dir)
        if err_gen==0:
            self.__log("Xeaugen.exe ok")
            err = self.execute("xeau", self.__hyd_dir)
            if err==0:
                self.__log("Xeau.exe ok")
            else:
                self.__log(f"Xeau.exe error: {err}")
        else:
            self.__log(f"Xeaugen.exe error:{err_gen}")

    def execute(self, program, cwd, args=[]):
        '''executes xeaugen.exe & xeau.exe
           emits output and logs
           returns error code'''
        assert(program in ['xeaugen', 'xeau', 'epanet'])

        # Find path to program
        prog_path = program_path(program)
        if prog_path is None:
            self.__log(f"{program} error: {program}.exe not found in {Kernel.kernel_dir}")

        # Licence
        environment = os.environ.copy()
        if program == 'xeau':
            environment['EXPRESSEAU_LICENSE'] = license_file

        # Hide shell
        if os.name == 'nt':
            si = STARTUPINFO()
            si.dwFlags |= STARTF_USESHOWWINDOW # default : si.wShowWindow = subprocess.SW_HIDE
        else:
            si = None

        self.__process = Popen([prog_path]+args, stdin=PIPE, stdout=PIPE, stderr=STDOUT, env=environment, startupinfo=si, encoding=ENCODING, errors='replace', cwd=cwd)

        # Emitting STDOUT of execution through "output" signal
        self.output.emit(f"{program}.exe output ({ENCODING}):")
        for stdout_line in iter(self.__process.stdout.readline, ""):
            self.output.emit(stdout_line)

        self.__process.stdin.close()
        self.__process.stdout.close()
        self.__process.wait()

        # Emitting logs of execution through "log" signal
        log_file = os.path.join(cwd, program + ".log")
        logs = Kernel.readfile(log_file)
        self.__log('\n'.join([line.strip() for line in logs]))

        # Returning error code
        err_file = os.path.join(cwd, program + ".err")
        return Kernel.get_error(err_file)

    def __log(self, text):
        self.log.emit(text)
        if 'error' in text:
            self.__project.log.error(text)
            raise KernelError(text)
        else:
            self.__project.log.notice(text)

    def kill_process(self):
        '''terminates process if any is running'''
        if self.__process:
            self.__process.terminate()

if __name__ == "__main__":
    import sys
    import getopt
    from qgis.PyQt.QtCore import QCoreApplication
    from ..utility.log import LogManager, ConsoleLogger

    try:
        optlist, args = getopt.getopt(sys.argv[1:],
                "h",
                ["help"])
    except Exception as e:
        sys.stderr.write(str(e)+"\n")
        exit(1)

    optlist = dict(optlist)

    if "-h" in optlist or "--help" in optlist:
        help(sys.modules[__name__])
        exit(0)

    app = QApplication(sys.argv)

    project = Project(args[0], LogManager(ConsoleLogger(), "Expresseau"))

    runner = Kernel(project, args[1])
    runner.run()
