# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


import os
import re
import signal
import webbrowser
from shutil import which
from subprocess import Popen, PIPE, STDOUT
if os.name == 'nt':
    from subprocess import STARTUPINFO, STARTF_USESHOWWINDOW
from qgis.gui import QgsMessageBar
from qgis.PyQt import uic
from qgis.PyQt.QtWidgets import QDialog, QFileDialog, QMessageBox
from ..gui.widgets.scroll_log_widget import ScrollLogger
from ..utility.log import LogManager
from ..qgis_utilities import MessageBarLogger, QGisProjectManager

class ExtractError(Exception):
    pass

class Extract(QDialog):
    def __init__(self, directory, parent=None):
        QDialog.__init__(self, parent)
        self.current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(self.current_dir, "extract.ui"), self)

        self.message_bar = QgsMessageBar(self)
        self.message_bar.setMaximumHeight(30)
        self.message_bar_layout.addWidget(self.message_bar)

        self.log = LogManager(MessageBarLogger(self.message_bar), "Express'eau - Extract")
        self.directory = directory
        self.label.setText(f"(Extract files from {self.directory})")

        self.cbox_control_files.currentIndexChanged.connect(self.__refresh_status)

        self.go_extract.clicked.connect(self.__run_extract)
        self.edit_btn.clicked.connect(self.__edit_control_file)
        self.help_btn.clicked.connect(self.__help_control_file)
        self.log_btn.clicked.connect(self.__open_log_file)
        self.go_wexpdess.clicked.connect(self.__run_wexpdess)
        self.buttonBox.accepted.connect(self.close)

        self.logger = ScrollLogger(self.logger_placeholder, night_mode=True)
        self.logger.setFixedSize(700, 600)

        self.__refresh_status()

    def __refresh_status(self):
        selected_control_file = self.cbox_control_files.currentText()

        self.cbox_control_files.blockSignals(True)
        self.cbox_control_files.clear()
        for file in [f for f in os.listdir(self.directory) if os.path.isfile(os.path.join(self.directory, f))]:
            if file.split('.')[-1] not in ['log', 'err', 'crb', 'mst', 'sup'] and file[-7:] != 'res.csv':
                try:
                    with open(os.path.join(self.directory, file)) as f:
                        if '*intervalle' in f.read().lower():
                            self.cbox_control_files.addItem(file)
                except UnicodeDecodeError:
                    # Not text file : ignored
                    self.log.notice(f"File {file} ignored (not a unicode text format)")
            else:
                self.log.notice(f"File {file} ignored (not a valid extension)")
        self.cbox_control_files.setCurrentText(selected_control_file)
        self.cbox_control_files.blockSignals(False)

        self.log_btn.setEnabled(False)
        self.go_wexpdess.setEnabled(False)

        control_file = os.path.join(self.directory, self.cbox_control_files.currentText())
        log_file = os.path.abspath(os.path.join(self.directory, 'extract.log'))
        extract_res_file = os.path.splitext(control_file)[0]+'.mst'

        self.log_btn.setEnabled(os.path.isfile(log_file))
        self.go_wexpdess.setEnabled(os.path.isfile(extract_res_file))

    def __edit_control_file(self):
        '''edits selected control file in default system editor'''
        os.startfile(os.path.join(self.directory, self.cbox_control_files.currentText()))

    def __help_control_file(self):
        '''opens doc'''
        webbrowser.open('http://hydra-software.net/docs/notes_techniques/NT31.pdf', new=0)

    def __output_log(self, text):
        ''' add line for extract output logging'''
        text = '\n'.join(re.split('\n+', text.strip()))
        keep_words = ['extract.exe','*', '*fin']
        if text:
            if "output:" in text:
                self.logger.new_label(20)
                self.logger.add_line(text, None, color='#ffffff')
            elif any(words in text for words in keep_words):
                self.logger.reset_current_label()
                self.logger.new_label()
                self.logger.add_line(text, 'bold', color='#ffffff')
            else:
                self.logger.add_line(text, None, color='#ffffff')

    def __run_extract(self):
        '''for connecting to extract button purposes'''
        control_file = os.path.join(self.directory, self.cbox_control_files.currentText())
        self.extract(control_file)
        self.__refresh_status()

    def extract(self, control_file):
        '''callable function thats runs the extract command on a parameter control file'''
        # Find path to program
        extract = which('extract')
        if extract is None:
            extract_exe = os.path.join(os.path.dirname(__file__), "extract.exe")
            if os.path.isfile(extract_exe):
                extract = extract_exe
            else:
                self.log.error(f"Extract error: extract.exe not found in {os.path.dirname(__file__)}")
                raise ExtractError(f"Extract.exe not found in {os.path.dirname(__file__)}")

        self.logger.clear()
        log_file = os.path.join(self.directory, 'extract.log')
        error_file = os.path.join(self.directory, 'extract.err')

        for file in [log_file, error_file]:
            if os.path.isfile(file):
                os.remove(file)
        if os.name == 'nt':
            si = STARTUPINFO()
            si.dwFlags |= STARTF_USESHOWWINDOW
        else : # linux
            si = None

        try:
            self.__process = Popen([extract, control_file], stdin=PIPE, stdout=PIPE, stderr=STDOUT, encoding="mbcs", cwd=self.directory, startupinfo=si)
            self.__output_log("Extract output:")
            for stdout_line in iter(self.__process.stdout.readline, ""):
                self.__output_log(stdout_line)
            self.__process.wait()
        except:
            # make sure the subprocess does not hang when an exception is raised
            os.kill(self.__process.pid, signal.CTRL_C_EVENT)
            raise

        with open(log_file, encoding="mbcs") as flog, open(error_file, encoding="mbcs") as ferr:
            msg = flog.readlines()
            exit_code = int(ferr.readlines()[0])
            if exit_code != 0:
                self.log.error(f"Command: {' '.join([extract, control_file])}\nError message:\n{'    '.join(msg)}")
                QMessageBox.critical(self, self.tr('Extract error'), f"An error occured during extract with file {control_file}.\nSee hydra.log file for extract logs.\n", QMessageBox.Ok)
            else:
                self.log.notice(f"Command successful: {' '.join([extract, control_file])}")
                QMessageBox.information(self, self.tr('Extract successful'), f"Extract run successfully with control file {control_file}.", QMessageBox.Ok)

    def __run_wexpdess(self):
        '''for connecting to wexpdess button purposes'''
        control_file = os.path.join(self.directory, self.cbox_control_files.currentText())
        self.wexpdess(control_file)

    def wexpdess(self, control_file):
        '''callable function thats runs the wexpdess command on a parameter control file'''
        # Find path to program
        wexpdess = which('wexpdess')
        if wexpdess is None:
            wexpdess_exe = os.path.join(os.path.dirname(__file__), "wexpdess.exe")
            if os.path.isfile(wexpdess_exe):
                wexpdess = wexpdess_exe
            else:
                self.log.error(f"WExpdess error: wexpdess.exe not found in {os.path.dirname(__file__)}")
                raise ExtractError(f"WExpdess.exe not found in {os.path.dirname(__file__)}")

        self.logger.clear()
        log_file = os.path.join(self.directory, 'wexpdess.log')
        error_file = os.path.join(self.directory, 'expdess.err') # MD 02/10/23 pas de "w" dans le nom du fichier err

        for file in [log_file, error_file]:
            if os.path.isfile(file):
                os.remove(file)
        if os.name == 'nt':
            si = STARTUPINFO()
            si.dwFlags |= STARTF_USESHOWWINDOW
        else : # linux
            si = None

        try:
            self.__process = Popen([wexpdess, f"{os.path.splitext(control_file)[0]}.mst", control_file], stdin=PIPE, stdout=PIPE, stderr=STDOUT, encoding="mbcs", cwd=self.directory, startupinfo=si)
            self.__output_log("WExpdess output:")
            for stdout_line in iter(self.__process.stdout.readline, ""):
                self.__output_log(stdout_line)
            self.__process.wait()
        except:
            # make sure the subprocess does not hang when an exception is raised
            os.kill(self.__process.pid, signal.CTRL_C_EVENT)
            raise

        with open(log_file, encoding="mbcs") as flog, open(error_file, encoding="mbcs") as ferr:
            msg = flog.readlines()
            exit_code = int(ferr.readlines()[0])
            if exit_code != 0:
                self.log.error(f"Command: {' '.join([wexpdess, f'{os.path.splitext(control_file)[0]}.mst', control_file])}\nError message:\n{'    '.join(msg)}")
                QMessageBox.critical(self, self.tr('WExpdess error'), f"An error occured during wexpdess with file {control_file}.\nSee hydra.log file for wexpdess logs.\n", QMessageBox.Ok)
            else:
                self.log.notice(f"Command successful: {' '.join([wexpdess, f'{os.path.splitext(control_file)[0]}.mst', control_file])}")
                QMessageBox.information(self, self.tr('WExpdess successful'), f"WExpdess run successfully with control file {control_file}.", QMessageBox.Ok)

    def __open_log_file(self):
        '''opens output log file in default system editor'''
        log_file = os.path.abspath(os.path.join(self.directory, 'extract.log'))
        if os.path.isfile(log_file):
            os.startfile(log_file)

if __name__ == "__main__":
    import sys
    import getopt
    from qgis.core import QgsApplication, QgsProject
    from ..project import Project
    from ..utility.log import LogManager, ConsoleLogger

    try:
        optlist, args = getopt.getopt(sys.argv[1:],
                "h",
                ["help"])
    except Exception as e:
        sys.stderr.write(str(e)+"\n")
        exit(1)

    optlist = dict(optlist)

    if "-h" in optlist or "--help" in optlist:
        help(sys.modules[__name__])
        exit(0)

    qgs = QgsApplication([], False)
    qgs.initQgis()

    project = Project(args[0], LogManager(ConsoleLogger()))

    dialog = Extract(project.extract_directory)
    dialog.exec_()
