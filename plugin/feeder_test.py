from .database import TestProject, project_exists, remove_project
from .project import Project

import shutil
import sys

keep = '-k' in sys.argv
debug = '-d' in sys.argv

dbname = 'feeder_xtest'

if project_exists(dbname):
    remove_project(dbname)
test_project = TestProject(dbname, keep=keep)
project = Project(dbname, debug=debug)


L = 100

# créer un réseau et les éléments du réseau
project.add_new_model('model')
project.execute(f'''
    insert into api.user_node(model, name, zground, depth, geom)
    values
    ('model', 'nod_east',   10, 1,'SRID=2154; POINT({L} 0)'::geometry),
    ('model', 'nod_north', 10, 1,'SRID=2154; POINT(0 {L})'::geometry)
    ;
    insert into api.reservoir_node(model, name, sz_array, zground, depth, z_ini, z_overflow, geom)
    values
    ('model', 'chateau', '{{{{50,100}}, {{60,100}}}}', 10, 0, 58, 60, 'SRID=2154; POINT(0 0)'::geometry)
    ;
    insert into api.material(name, roughness_mm, elasticity_n_m2)
    values ('lisse', .001, 2e+11)
    ;
    insert into api.pipe_link(diameter, material, thickness_mm, geom, is_feeder)
    values
    (.2 , 'lisse', 0.02, 'SRID=2154; LINESTRING(0 0, {L} 0)'::geometry, true),
    (.2 , 'lisse', 0.02, 'SRID=2154; LINESTRING(0 0, 0 {L})'::geometry, false)
    ;
    insert into api.water_delivery_point(geom)
    values
    ('SRID=2154; POINT({L/2} {L/10})'::geometry)
    ;
    ''')



