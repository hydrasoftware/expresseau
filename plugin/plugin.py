# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of XPRESSO, a QGIS plugin for hydraulics                             ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

"""
Expresseau plugin main file
"""

import os
from qgis.core import QgsProject, QgsApplication, QgsSettings
from qgis.gui import QgsGui

from qgis.PyQt.QtCore import QObject, QTranslator, QCoreApplication
from .gui.menu import ExpresseauMenu
from .gui.toolbar import ExpresseauToolbar
from .qgis_utilities import QGisLogger, ArrayWidgetFactory, QGisProjectManager
from .utility.log import LogManager
from .utility.license import check_license
from .processing.provider import Provider as ProcessingProvider
from .service import set_service
from .http_server import HttpServer
import ssl

_plugin_dir = os.path.dirname(__file__)

class Expresseau(QObject):
    '''QGIS Plugin Implementation.'''

    def __init__(self, iface):
        QObject.__init__(self)
        self.__iface = iface

        # initialize menus
        self.__log_manager = LogManager(QGisLogger(self.__iface), "Expresseau")
        self.__toolbar = None
        self.__menu = None

        self.__iface.projectRead.connect(self.__project_loaded)
        self.__iface.newProjectCreated.connect(self.__project_loaded)

        # initialize locale
        locale = QgsSettings().value('locale/userLocale')[0:2]
        locale_path = os.path.join(
            os.path.dirname(__file__),
            'i18n',
            '{}.qm'.format(locale))

        if os.path.exists(locale_path):
            self.__translator = QTranslator()
            self.__translator.load(locale_path)
            QCoreApplication.installTranslator(self.__translator)

        self.__processing_provider = ProcessingProvider()
        QgsApplication.processingRegistry().addProvider(self.__processing_provider)

        # create notepad++ syntax file
        notepad_user_defines_lang = os.path.join(os.path.expanduser('~'), 'AppData', 'Roaming', 'Notepad++', 'userDefineLangs')
        if os.path.exists(notepad_user_defines_lang):
            with open(os.path.join(notepad_user_defines_lang, 'expresseau_ctl.xml'), 'w') as x, open(os.path.join(_plugin_dir, 'expresseau_ctl.xml')) as s:
                x.write(s.read())

        try:
            self.http_server = HttpServer(iface=self.__iface)
        except ssl.SSLError:
            pass

    def initGui(self):
        '''create the menu entries and icons inside the QGIS GUI'''
        self.__menu = ExpresseauMenu(self.__log_manager)
        self.__iface.mainWindow().menuBar().addMenu(self.__menu)
        QgsGui.editorWidgetRegistry().registerWidget("Array", ArrayWidgetFactory())
        self.__project_loaded()

    def unload(self):
        '''Removes the plugin menu item and icon from QGIS GUI'''

        QgsApplication.processingRegistry().removeProvider(self.__processing_provider)

        if self.__menu is not None:
            self.__menu.setParent(None)
            self.__menu = None
        if self.__toolbar is not None:
            self.__toolbar.setParent(None)
            self.__toolbar = None

        self.__iface.newProjectCreated.disconnect(self.__project_loaded)
        self.__iface.projectRead.disconnect(self.__project_loaded)

    def __project_loaded(self, dom=None):
        '''Loads plugin concepts and UI if an expresseau project is opened via a QGIS process'''
        if self.__toolbar is not None:
            self.__toolbar.setParent(None)
            self.__toolbar = None

        if QGisProjectManager.is_expresseau_project():
            set_service(QgsProject.instance().readEntry('expresseau', 'service', 'expresseau')[0] )
            self.__toolbar = ExpresseauToolbar(self.__log_manager)
            self.__iface.addToolBar(self.__toolbar)
            QgsProject.instance().customVariablesChanged.connect(self.__toolbar.variables_changed)
            if not check_license():
                self.__menu.user_activate_license()
