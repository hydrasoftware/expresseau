# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


"""
run test to check if tables used exist on the database

USAGE

   python -m plugin.gui.scenario_manager_test [-dhk]

OPTIONS

   -h, --help
        print this help

   -d, --debug
        run in debug mode (lots of output)

   -k, --keep
        keeps database after test
"""

assert(__name__ == "__main__")

import sys
import getopt
from qgis.PyQt.QtWidgets import QApplication
from plugin.project import Project
from plugin.gui.scenario_manager import ScenarioManager
from plugin.database import TestProject, project_exists, create_project

try:
    optlist, args = getopt.getopt(sys.argv[1:],
            "hdk",
            ["help", "debug", "keep"])
except Exception as e:
    sys.stderr.write(str(e)+"\n")
    exit(1)

optlist = dict(optlist)

if "-h" in optlist or "--help" in optlist:
    help(sys.modules[__name__])
    exit(0)

if not project_exists(TestProject.NAME):
    sys.stdout.write("creating template database... ")
    TestProject.reset_template()
    sys.stdout.write("ok\n")

debug = "-d" in optlist or "--debug" in optlist
keep = "-k" in optlist or "--keep" in optlist

dbname = "scenario_manager_xtest"
test_project = TestProject(dbname, keep, with_model=True)
project_test = Project(dbname, debug=debug)

project_test.execute("insert into api.scenario(name) values('scenario_default');")
project_test.execute("insert into api.scenario(name) values('scenario_export');")
project_test.execute("insert into api.scenario(name) values('RSTART');")
project_test.execute("insert into api.scenario(name) values('REF');")
project_test.execute("""update api.scenario set model_connect_settings='Cascade' where name='scenario_default';""")
project_test.execute("""update api.scenario set refined_discretisation=True, flag_save=True, vaporization=True where name='scenario_export';""")
project_test.execute("insert into api.water_delivery_scenario(name) values('WD_scenario');")
project_test.execute("""update api.scenario set water_delivery_scenario='WD_scenario' where name='scenario_export';""")

app = QApplication(sys.argv)
dialog = ScenarioManager(project_test)
dialog.select_scenario('scenario_default')
dialog.set_active_scenario()
dialog.select_scenario('scenario_export')
dialog.set_active_scenario()
dialog.comput_settings.flag_rstart.setChecked(True)
dialog.comput_settings.scenario_rstart.setCurrentText('RSTART')
dialog.comput_settings.scenario_ref_btn.setChecked(True)
dialog.comput_settings.scenario_ref.setCurrentText('REF')
scn = dialog.clone_scenario()
assert(dialog.active_scn==scn)

dialog.save()

sys.stdout.write("ok\n")
