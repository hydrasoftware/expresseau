# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of EXPRESSEAU, a QGIS plugin for hydraulics                          ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


"""
displays expresseau results as a graph

USAGE

   python -m result_dialog <object name> <table name> <scenario name> <project name>

OPTIONS

   -h, --help
        outputs this help
"""

#from qgis.PyQt.QtCore import Qt
import os
import re
import json
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas, NavigationToolbar2QT as NavigationToolbar
from matplotlib.figure import Figure
import matplotlib.dates as mdates
from matplotlib import ticker
from qgis.gui import QgsMessageBar
from qgis.PyQt import uic
from qgis.PyQt.QtCore import QPoint, Qt, QStringListModel, QSortFilterProxyModel
from qgis.PyQt.QtWidgets import QDialog, QApplication, QMenu, QFileDialog
from ..project import Project
from ..utility.results import Results
from ..utility.log import LogManager
from ..qgis_utilities import MessageBarLogger, QGisProjectManager
from wstar.expresseau_result import expresseau_result_json as _result_json_

COLORS = ["#0000ff", "#aa00ff", "#00aaff", "#00ffff", "#55007f", "#00557f", "#00ff00", "#005500", "#aaaa00", "#aaff00", "#aaaa7f", "#ffaa00"]

TIME_UNITS = ['h', 'min', 's', '']
TIME_LEGENDS = ['time (h)', 'time (min)', 'time (s)', 'time']


class CustomFigureCanvas(FigureCanvas):

    def __init__(self, figure):
        super().__init__(figure)

    def enterEvent(self, event):
        if hasattr(event, 'pos'):
            return FigureCanvas.enterEvent(self, event)


def time_series(project, model, scenario, table, model_item, value_name, time_scale_units='h', date0=None):
    """ time_scale_units are either 'h', 'min', 'sec' or '' for time and dates
    returns x, y lists
    """

    time_scale = {'h':1, 'min':60, 's':3600, '':1/24}[time_scale_units]
    time_offset = 0

    date_start, date_end = project.fetchone(f"select starting_date, starting_date+duration from api.scenario where name='{scenario}'")

    ### modif marjo 28/01/25 : inversion des deux if pour ne pas que l'offset relatif à date_start soit écrasé
    if date0:
        if time_scale_units == '':
            time_offset = mdates.date2num(date0)*24
        else:
            time_offset = (mdates.date2num(date_start) - mdates.date2num(date0))*24

    if time_scale_units == '':
        time_offset = mdates.date2num(date_start)*24
    ###

    if table == 'water_delivery_sector' and value_name in ('v_tot(m3)', 'v_cons_tot(m3)', 'q_tot(m3/h)', 'q_cons_tot(m3/h)'):
       return [], [], 'not really a file', date0 or date_start
    elif value_name in _result_json_[table]['result']:
        index = _result_json_[table]['result'].index(value_name)
        ext = _result_json_[table]['file_hydrau']
    elif value_name in _result_json_[table]['quality']:
        index = _result_json_[table]['quality'].index(value_name)
        ext = _result_json_[table]['file_quality']
    else:
        assert(False)

    file_ = os.path.join(project.directory, scenario.upper(), 'hydraulique', f"{scenario.upper()}_{model.upper()}.{ext}")
    if not os.path.isfile(file_):
        return None, None, file_, date0 or date_start, date_end

    res = Results(file_)
    if model_item not in res.names:
        return None, None, file_, date0 or date_start, date_end

    values = res[:][model_item]
    return [(t+ time_offset)*time_scale for t, v in values], [v[index] for t, v in values], file_, date0 or date_start, date_end

def measures(project, sensor, date0, time_scale_units='h', date_start=None, date_end=None, value_name=None):

    time_scale = {'h':24, 'min':24*60, 's':24*3600, '':1}[time_scale_units]
    scale = 1

    res = project.fetchone("""
        select s.type_::varchar, c.scale, c.offset
        from api.sensor s
        cross join lateral api.measure_conversion(%s, %s) c
        where name=%s
        """, (sensor, value_name, sensor,))
    if res:
        value_name_, scale, offset = res
        filt = ((f" and t>='{date_start}'" if date_start else "")
               +(f" and t<='{date_end}'" if date_end else ""))
        res = project.fetchall(f"""
            select t, value
            from api.measure
            where sensor=%s {filt}
            order by t
            """, (sensor, ))
        if res:
            if time_scale_units == '':
                t0 = 0
            else:
                t0 = mdates.date2num(date0) if date0 is not None else mdates.date2num(res[0][0])

            x = [(mdates.date2num(t) - t0)*time_scale for t, v in res]
            y = [v*scale + offset for t, v in res]
            return x, y, value_name or value_name_

    return None, None, None

class SimulationLine:
    def __init__(self, model, scenario, table, model_item, value_name, side):
        self.model = model
        self.scenario = scenario
        self.table = table
        self.model_item = model_item
        self.value_name = value_name
        self.side = side
        self.line = None

class MeasureLine:
    def __init__(self, sensor, side, value_name=None):
        self.sensor = sensor
        self.side = side
        self.line = None
        self.value_name = value_name

class ResultWindow(QDialog):

    def set_result(self, item_id=None, table_name=None, scenario_name=None, sensor_name=None):

        item_name, model_name = None, self.project.models[0]

        if sensor_name:
            self.tabWidget.setCurrentIndex(1)
        else:
            self.tabWidget.setCurrentIndex(0)
            res = self.project.fetchone(f"select upper(name), {'' if 'node' in table_name else '_'}model from api.{table_name} where id={item_id}")
            if res:
                item_name, model_name = res

        self.combo_model.blockSignals(True)
        self.combo_table.blockSignals(True)

        self.__init_comboboxes(scenario_name, model_name, table_name, item_name, sensor_name)

        self.combo_model.blockSignals(False)
        self.combo_table.blockSignals(False)


    def __init__(self, item_id=None, table_name=None, scenario_name=None, project_name=None, parent=None, sensor_name=None, with_measure=True):
        QDialog.__init__(self, parent)
        self.setWindowFlags(Qt.Window)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "result_dialog.ui"), self)

        # item filtering
        self.filter_item = QSortFilterProxyModel(QStringListModel([], self))
        self.combo_item.setModel(self.filter_item)
        #self.combo_item.setCompleter(None)
        self.item_filter.textChanged.connect(self.__set_model_filter)

        self.with_measure = with_measure
        self.__selected_line = None
        self.__color_index = 0

        self.message_bar = QgsMessageBar(self)
        self.message_bar.setMaximumHeight(30)
        self.message_bar_layout.addWidget(self.message_bar)

        self.project = Project(project_name or QGisProjectManager.project_name(), LogManager(MessageBarLogger(self.message_bar)))

        self.set_result(item_id, table_name, scenario_name, sensor_name)

        self.combo_model.currentTextChanged.connect(self.__refresh_item_combo)
        self.combo_table.currentTextChanged.connect(self.__refresh_item_combo)
        self.combo_table.currentTextChanged.connect(self.__refresh_result_combo)

        self.load_btn.clicked.connect(self.__load)
        self.save_btn.clicked.connect(self.__save)

        self.figure = Figure(tight_layout=True)
        self.ax1 = self.figure.add_subplot()
        self.ax2 = self.ax1.twinx()
        #self.ax1.margins(0.05)
        self.lines = [] # Stores all lines currently in the graph
        self.measures = [] # Stores all measures currently in the graph
        self.canvas = CustomFigureCanvas(self.figure)
        self.graph_layout.addWidget(self.canvas)

        self.toolbar = NavigationToolbar(self.canvas, self)
        self.toolbar.setMaximumWidth(self.groupBox.maximumWidth())
        self.tb.addWidget(self.toolbar)

        self.canvas.mpl_connect('motion_notify_event', self.__hover_canvas)
        self.canvas.mpl_connect('button_press_event', self.__clicked)
        self.__hover_dot = None

        self.button_copy_data.clicked.connect(self.__copy_data)
        self.button_add_to_axe_1.clicked.connect(self.__add_to_axe_1)
        self.button_add_to_axe_2.clicked.connect(self.__add_to_axe_2)
        self.button_clear.clicked.connect(self.__clear)
        self.title.textChanged.connect(self.__title_changed)
        self.legend.toggled.connect(self.__draw)
        self.legend_position.currentIndexChanged.connect(self.__draw)
        self.grid.currentIndexChanged.connect(self.__draw)
        self.time_scale.currentIndexChanged.connect(self.__draw)
        self.ax1.set_xlabel('time (h)')
        self.ax1.format_coord = lambda x, y: ""
        self.ax2.format_coord = lambda x, y: ""
        self.__add_to_axe_1()



    def __load(self):
        file_, __ = QFileDialog.getOpenFileName(self, self.tr("Select a file"), filter="JSON (*.json)")
        if file_:
            with open(file_) as j:
                _json = json.load(j)
            self.__clear()
            self.title.setText(_json['title'])
            self.legend.setChecked(_json['legend'])
            self.legend_position.setCurrentIndex(_json['legend_position'])
            self.grid.setCurrentIndex(_json['grid'])
            self.time_scale.setCurrentIndex(_json['time_scale'])
            self.lines = [SimulationLine(ll['model'], ll['scenario'], ll['table'], ll['model_item'], ll['value_name'], ll['side']) for ll in _json['lines']]
            self.measures = [MeasureLine(ll['sensor'], ll['side'], ll['value_name'] if 'value_name' in ll else None) for ll in _json['measures']]
            self.__draw()

    def __save(self):
        file_, __ = QFileDialog.getSaveFileName(self, self.tr("Select a file to save graph"), filter="JSON (*.json)")
        if file_:
            if not file_.endswith('.json'):
                file_ += '.json'
            with open(file_, 'w') as j:
                json.dump({
                "title": self.title.text(),
                "legend": self.legend.isChecked(),
                "legend_position": self.legend_position.currentIndex(),
                "grid": self.grid.currentIndex(),
                "time_scale": self.time_scale.currentIndex(),
                "lines": [{"model": ll.model, "scenario": ll.scenario, "table": ll.table, "model_item": ll.model_item, "value_name": ll.value_name, "side": ll.side} for ll in self.lines],
                "measures": [{"sensor": ll.sensor, "side": ll.side, "value_name": ll.value_name} for ll in self.measures]
                }, j)



    def __init_comboboxes(self, scenario_name, model_name, table_name, item_name, sensor_name):
        self.combo_scenario.clear()
        for scenario, in self.project.fetchall("""select name from api.scenario order by name;"""):
            self.combo_scenario.addItem(scenario)
        scenario_name and self.combo_scenario.setCurrentText(scenario_name)

        self.combo_model.clear()
        for model, in self.project.fetchall("""select name from api.model order by name;"""):
            self.combo_model.addItem(model)
        model_name and self.combo_model.setCurrentText(model_name)

        self.combo_table.clear()
        self.combo_table.addItem('water_delivery_sector')
        for table in sorted(_result_json_.keys()):
            self.combo_table.addItem(table)
        table_name and self.combo_table.setCurrentText(table_name)

        self.combo_sensor.clear()
        for name, in self.project.fetchall("""select name from api.sensor order by name;"""):
            self.combo_sensor.addItem(name)
        sensor_name and self.combo_sensor.setCurrentText(sensor_name)

        self.__refresh_result_combo()
        item_name and self.__refresh_item_combo(item_name)

    def __refresh_result_combo(self):
        self.combo_result.clear()
        if self.combo_table.currentText() != 'water_delivery_sector':
            for result in _result_json_[self.combo_table.currentText()]['result']:
                self.combo_result.addItem(result)

            quality_output, = self.project.fetchone(f"select quality_output from api.scenario where name='{self.combo_scenario.currentText()}';")

            if quality_output != 'None':
                for result in _result_json_[self.combo_table.currentText()]['quality']:
                    self.combo_result.addItem(result)

    def __refresh_item_combo(self, item_name=None):
        #self.combo_item.clear()
        items = [item for item, in self.project.fetchall(
            f"""select upper(name)
            from api.{self.combo_table.currentText()}
            where {'' if 'node' in self.combo_table.currentText() else '_'}model='{self.combo_model.currentText()}'
            order by name;""")]

        self.filter_item.setSourceModel(QStringListModel(items))

        if item_name:
            self.combo_item.setCurrentText(item_name)

        self.item_filter.setText('')

    def __set_model_filter(self, filt):
        self.filter_item.setFilterRegExp(filt)

    def __draw(self):
        self.ax1.clear()
        self.ax2.clear()
        date0 = None
        date1 = None


        if self.time_scale.currentIndex() != 3:
            self.ax1.xaxis.set_major_formatter(ticker.ScalarFormatter(useMathText=True))
            self.ax1.tick_params(axis="x", rotation=0)
            self.ax1.ticklabel_format(style='plain', useOffset=False)
            self.ax2.ticklabel_format(style='plain', useOffset=False)
        else:
            self.ax1.xaxis.set_major_formatter(mdates.DateFormatter('%d/%m/%y %H:%M'))
            self.ax1.tick_params(axis="x", rotation=50)

        for color, line in enumerate(self.lines):
            ax = self.ax1 if line.side=='left' else self.ax2
            x, y, file_, date0, date1 = time_series(
                self.project,
                line.model,
                line.scenario,
                line.table,
                line.model_item,
                line.value_name,
                TIME_UNITS[self.time_scale.currentIndex()],
                date0
                )
            if x is not None:
                line.line, = ax.plot(x, y,
                    linestyle='-',
                    marker='.',
                    label=f"{line.value_name} - {line.model_item} (line.model) - {line.scenario}",
                    color=COLORS[color%len(COLORS)])
                ax.set_ylabel(line.value_name)
            else:
                self.project.log.warning(f"no result or no result file '{file_}'")

        for color, line in enumerate(self.measures):
            ax = self.ax1 if line.side=='left' else self.ax2
            x, y, value_name = measures(
                self.project,
                line.sensor,
                date0,
                TIME_UNITS[self.time_scale.currentIndex()],
                date0,
                date1,
                line.value_name
                )
            if x is not None:
                line.line, = ax.plot(x, y,
                    linestyle='-',
                    marker='+',#None,
                    label=f"{value_name} - {line.sensor}",
                    color='red', # COLORS[color%len(COLORS)],
                    linewidth=0.5
                    )
                ax.set_ylabel(value_name)
            else:
                self.project.log.warning(f"no measure for sensor '{line.sensor}' between start and end date")

        if self.legend.isChecked():
            lines1, labels1 = self.ax1.get_legend_handles_labels()
            lines2, labels2 = self.ax2.get_legend_handles_labels()
            self.ax1.legend(lines1+lines2, labels1+labels2, loc=['upper left', 'upper right', 'lower right', 'lower left'][self.legend_position.currentIndex()])
        else:
            self.ax1.get_legend() and self.ax1.get_legend().remove()
            self.ax2.get_legend() and self.ax2.get_legend().remove()

        self.ax1.grid(self.grid.currentIndex() in (1,3))
        self.ax2.grid(self.grid.currentIndex() in (2,3))

        self.ax1.set_xlabel(TIME_LEGENDS[self.time_scale.currentIndex()])
        #self.figure.tight_layout()
        self.canvas.draw()

    def __add_to_axe(self, side):
        '''adds selected results to main axis graph'''
        if self.tabWidget.currentIndex() == 0:
            self.lines.append(SimulationLine(
                self.combo_model.currentText(),
                self.combo_scenario.currentText(),
                self.combo_table.currentText(),
                self.combo_item.currentText(),
                self.combo_result.currentText(),
                side))

            # look for measures for this element
            res = self.project.fetchall(f"""
                select s.name, s.type_
                from api.sensor s
                join api.{self.combo_table.currentText()} n on st_dwithin(s.geom, n.geom, .01)
                where n.name=%s
                """, (self.combo_item.currentText(),))

            wish = re.sub(r' *\(.*', '', self.combo_result.currentText())
            for name, type_ in res:
                if re.match(f'^{wish}'+r' *\(.*\)', type_) or (
                        wish == 'z' and type_.startswith('p')) or (
                        wish == 'p' and type_.startswith('z')):
                    self.measures.append(MeasureLine(name, side, self.combo_result.currentText()))

        else:
            self.measures.append(MeasureLine(self.combo_sensor.currentText(), side))

        self.__draw()

    def __add_to_axe_1(self):
        self.__add_to_axe('left')

    def __add_to_axe_2(self):
        self.__add_to_axe('right')

    def __title_changed(self, title):
        self.ax1.set_title(self.title.text())
        self.figure.tight_layout()
        self.canvas.draw()

    def __hover_canvas(self, event) :
        '''fonction called when hovering in the canvas graph'''
        if self.__hover_dot is not None :
            self.__hover_dot.remove() #pop(0).remove()
            self.__hover_dot = None

        self.hover_label.setText("X:        Y:")
        for line in self.lines + self.measures:
            if line.line is not None:
                cont, ind = line.line.contains(event)
                if cont :
                    x, y = [v[ind['ind'][0]] for v in line.line.get_data()]

                    if TIME_UNITS[self.time_scale.currentIndex()] == '':
                        self.hover_label.setText(f"X: {mdates.num2date(x).strftime('%Y-%m-%d %H:%M:%S')}   Y: {y:.4g}")
                    else:
                        self.hover_label.setText(f"X: {x:.2f}   Y: {y:.4g}")
                    if self.__hover_dot is not None :
                        self.__hover_dot.remove() #pop(0).remove()
                    self.__hover_dot, = line.line.axes.plot(x, y, linestyle=None, marker=".", color='red')
                    self.ax1.get_xlim() # fix resize on plot

        self.canvas.draw()

    def __clicked(self, event):
        self.__selected_line = None
        for i, line in enumerate(self.lines + self.measures):
            if line.line is not None:
                cont, ind = line.line.contains(event)
                if cont:
                    self.__selected_line = line.line, i%max(len(self.lines), 1), 'line' if i < len(self.lines) else 'measure'
                    m = QMenu()
                    m.addAction('remove').triggered.connect(self.__remove_line)
                    m.move(self.canvas.mapToGlobal(QPoint(event.x, self.canvas.height() - event.y)))
                    m.show()
                    m.exec_()

    def __remove_line(self):
        if self.__selected_line is not None:
            if self.__selected_line[2] == 'line':
                del self.lines[self.__selected_line[1]]
            else:
                del self.measures[self.__selected_line[1]]
            if self.__hover_dot is not None :
                self.__hover_dot.remove()
                self.__hover_dot = None
            self.__draw()

    def __clear(self):
        '''removes everything from graph'''
        self.lines = []
        self.measures = []
        self.hover_label.setText("X: - / Y: -")
        self.ax1.clear()
        self.ax2.clear()
        self.ax1.set_title(self.title.text())
        self.canvas.draw()

    def __copy_data(self):
        cb = QApplication.clipboard()
        cb.clear(mode=cb.Clipboard )
        data_to_export = ""
        if len(self.lines+self.measures):
            data_to_export += "\t".join([f"time[{self.time_scale.currentText()}]\t{ll.line.get_label()}" for ll in self.lines+self.measures])+"\n"
            max_length = max([len(ll.line.get_xdata()) for ll in self.lines+self.measures])
            if self.time_scale.currentIndex() != 3:
                for i in range(max_length):
                    data_to_export += "\t".join([f"{ll.line.get_xdata()[i]}\t{ll.line.get_ydata()[i]}" if len(ll.line.get_ydata())>i else '\t' for ll in self.lines+self.measures])+"\n"
            else:
                for i in range(max_length):
                    data_to_export += "\t".join([f"{mdates.num2date(ll.line.get_xdata()[i]).strftime('%Y-%m-%d %H:%M:%S')}\t{ll.line.get_ydata()[i]}" if len(ll.line.get_ydata())>i else '\t' for ll in self.lines+self.measures])+"\n"
            cb.setText(data_to_export, mode=cb.Clipboard)

if __name__ == "__main__":
    import sys
    import getopt
    from qgis.PyQt.QtWidgets import QApplication
    from ..utility.log import LogManager, ConsoleLogger

    try:
        optlist, args = getopt.getopt(sys.argv[1:],
                "h",
                ["help"])
    except Exception as e:
        sys.stderr.write(str(e)+"\n")
        exit(1)

    optlist = dict(optlist)

    if "-h" in optlist or "--help" in optlist:
        help(sys.modules[__name__])
        exit(0)

    app = QApplication(sys.argv)

    if len(args) ==  4:
        object_id = args[0]
        table_name = args[1]
        scenario_name = args[2]
        project_name = args[3]

        instanced_project = Project(project_name, log_manager=LogManager(ConsoleLogger(), 'Expresseau - Result dialog'))
        dialog = ResultWindow(object_id, table_name, scenario_name, project_name)
    else:
        sensor_name = args[0]
        project_name = args[1]

        instanced_project = Project(project_name, log_manager=LogManager(ConsoleLogger(), 'Expresseau - Result dialog'))
        dialog = ResultWindow(project_name=project_name, sensor_name=sensor_name)

    dialog.exec_()
