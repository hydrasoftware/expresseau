# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of EXPRESSEAU, a QGIS plugin for hydraulics                          ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


import os
from functools import partial
from qgis.PyQt import uic, QtWidgets, QtCore
from qgis.PyQt.QtWidgets import QDialog, QTableWidgetItem, QComboBox
from .widgets.table_widget import TableWidget
from .widgets.array_widget import ArrayWidget
from .widgets.graph_widget import GraphWidget
from ..utility.string import get_nullable_sql_float, get_str, list_to_sql_array

class WaterDeliverySectorSettings(QDialog):
    def __del__(self):
        self.cursor.close()
        self.conn.close()

    def __init__(self, project, parent=None):
        QDialog.__init__(self, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "water_delivery_sector_settings.ui"), self)
        self.conn = project.connect()
        self.cursor = self.conn.cursor()

        self.buttonBox.accepted.connect(self.save)
        self.buttonBox.rejected.connect(self.close)

        self.__isloading = True
        self.active_sector = None
        self.active_modulation=None

        self.table_sectors.setHorizontalHeaderLabels([self.tr('Sector'), self.tr('Comment'), self.tr('Total domestic\nvolume (m3/d)')])
        self.set_sectors_table()

        self.table_sector_nodes.setHorizontalHeaderLabels([self.tr('Model'), self.tr('Name'), self.tr('Domestic\nvolume (m3/d)'), self.tr('Domestic\ncurve')])
        self.table_sector_nodes.resizeColumnsToContents()

        self.table_modulation = TableWidget(self.cursor,
            ("name", "comment"), (self.tr("Name"), self.tr("Comment")),
            "api", "hourly_modulation_curve", (self.new_modulation, self.delete_modulation), "", pk="name",
            parent=self.table_modulation_placeholder)
        self.table_modulation.data_edited.connect(self.save_modulation)
        self.table_modulation.table.itemSelectionChanged.connect(self.set_active_modulation)

        self.graph_modulation = GraphWidget(self.graph_modulation_placeholder, bar=True)

        self.array_modulation = ArrayWidget([self.tr("Weekday\nHourly coefficients"), self.tr("Weekend\nHourly coefficients")], [24,2], [[1,1]]*24,
                parent=self.array_modulation_placeholder, vertical_headers=[f'{i}h-{i+1}h' for i in range(24)])
        self.array_modulation.valuesChanged.connect(self.__draw_graph)
        self.array_modulation.add.hide()
        self.array_modulation.delete.hide()
        self.array_modulation.setEnabled(False)

    def set_sectors_table(self):
        self.__isloading = True
        '''populates sectors table'''
        self.cursor.execute("""select s.name, s.comment, coalesce(sum(n._domestic_volume), 0)
                                          from api.water_delivery_sector as s
                                          left join api._user_node_affected_volume as n on ST_Contains(s.geom, n.geom)
                                          group by s.name, s.comment
                                          order by s.name asc
                                          """)
        sectors = self.cursor.fetchall()

        self.table_sectors.setRowCount(len(sectors))

        for row in range(self.table_sectors.rowCount()):
            sector = sectors[row]
            self.table_sectors.setItem(row,0,QTableWidgetItem(get_str(sector[0])))
            self.table_sectors.setItem(row,1,QTableWidgetItem(get_str(sector[1])))
            self.table_sectors.setItem(row,2,QTableWidgetItem(get_str(sector[2])))

            self.table_sectors.item(row,0).setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
            self.table_sectors.item(row,2).setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)

        self.table_sectors.resizeColumnsToContents()
        self.table_sectors.horizontalHeader().setStretchLastSection(True)
        self.table_sectors.itemSelectionChanged.connect(self.set_active_sector)
        self.table_sectors.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)

        self.__isloading = False

    def set_active_sector(self):
        '''set active sector and populates hydrographs table based on selected sector'''
        if not self.active_modulation is None:
            self.save_modulation()

        items = self.table_sectors.selectedItems()
        if len(items)>0:
            selected_row = self.table_sectors.row(items[0])
            self.active_sector = self.table_sectors.item(selected_row, 0).text()
            self.set_sector_nodes()

    def save_sectors_table(self):
        '''save sectors table '''
        for i in range(0, self.table_sectors.rowCount()):
            self.cursor.execute(f"""update api.water_delivery_sector
                                    set comment='{self.table_sectors.item(i,1).text()}' where name='{self.table_sectors.item(i,0).text()}'""")

    def set_sector_nodes(self):
        self.__isloading = True
        '''set nodes in hy table for selected sector'''
        self.cursor.execute("select name from api.hourly_modulation_curve order by name")
        modulations = self.cursor.fetchall()
        self.table_sector_nodes.setRowCount(0)
        if self.active_sector is not None:
            self.cursor.execute(f"""select n.model, n.name, coalesce(v._domestic_volume, 0), n.domestic_curve
                                    from api.user_node as n
                                    left join api._user_node_affected_volume as v on v._user_node=n.id
                                    where n._sector='{self.active_sector}'
                                    order by name;""")
            nodes = self.cursor.fetchall()

            self.table_sector_nodes.setRowCount(len(nodes))

            for row in range(0, self.table_sector_nodes.rowCount()):
                node=nodes[row]
                # Model
                self.table_sector_nodes.setItem(row, 0, QTableWidgetItem(node[0]))
                self.table_sector_nodes.item(row, 0).setFlags(QtCore.Qt.ItemIsEnabled)
                # Name
                self.table_sector_nodes.setItem(row, 1, QTableWidgetItem(node[1]))
                self.table_sector_nodes.item(row, 1).setFlags(QtCore.Qt.ItemIsEnabled)
                # Domestic
                ## volume
                self.table_sector_nodes.setItem(row, 2, QTableWidgetItem(get_str(node[2])))
                self.table_sector_nodes.item(row, 2).setFlags(QtCore.Qt.ItemIsEnabled)
                ## curve
                self.table_sector_nodes.setItem(row, 3, QTableWidgetItem(get_str(node[3])))
                cbox = self.get_new_modulation_combo(modulations, node[3])
                cbox.currentIndexChanged.connect(partial(self.combo_modulation_index_changed, row))
                self.table_sector_nodes.setCellWidget(row, 3, cbox)

            self.table_sector_nodes.resizeColumnsToContents()
            self.table_sector_nodes.horizontalHeader().setStretchLastSection(True)
        self.__isloading = False

    def combo_modulation_index_changed(self, irow):
        '''triggerred at modulation curve changed in combo'''
        self.cursor.execute(f"""update api.user_node set domestic_curve='{self.table_sector_nodes.cellWidget(irow, 3).currentText()}'
                                where name='{self.table_sector_nodes.item(irow, 1).text()}' and model='{self.table_sector_nodes.item(irow, 0).text()}';""")

    def get_new_modulation_combo(self, modulations, current_modulation):
        '''creates combobox with "constant" option + all modulation curves'''
        combobox = QComboBox()
        combobox.addItem("Constant")
        for modulation, in modulations:
            combobox.addItem(modulation)
        combobox.setCurrentText(current_modulation)
        return combobox

    def new_modulation(self):
        '''add a modulation curve'''
        self.table_modulation.add_row(['default'])

    def delete_modulation(self):
        '''delete selected modulation curve'''
        self.table_modulation.del_selected_row()

    def save_modulation(self):
        '''save selected modulation curve'''
        if self.active_modulation is not None:
            self.table_modulation.save_selected_row()
            items = self.array_modulation.get_table_items()
            if items is not None:
                self.cursor.execute(f"""update api.hourly_modulation_curve
                                         set hourly_modulation_array='{list_to_sql_array(items)}'
                                         where name='{self.active_modulation}'""")

    def set_active_modulation(self):
        '''set active modulation curve and refresh array data'''
        items = self.table_modulation.table.selectedItems()
        if len(items)>0:
            selected_row = self.table_modulation.table.row(items[0])
            if not self.active_modulation is None:
                self.save_modulation()

            self.active_modulation = self.table_modulation.table.item(selected_row,0).text()
            self.cursor.execute(f"""select hourly_modulation_array::real[] from api.hourly_modulation_curve where name='{self.active_modulation}';""")
            hourly_modulation_array, = self.cursor.fetchone()
            self.array_modulation.set_table_items(hourly_modulation_array)
            self.__draw_graph()
            self.array_modulation.setEnabled(True)

    def __draw_graph(self):
        '''refresh modulation curve graph'''
        title = ''
        current_row = self.table_modulation.table.currentRow()
        if current_row != -1:
            title = self.table_modulation.table.item(current_row,1).text()
        datas = self.array_modulation.get_table_items()
        if len(datas)>0:
            self.graph_modulation.clear()
            self.graph_modulation.add_line(
                range(24)[:len(datas)],
                [r[0] for r in datas],
                "red",
                title,
                "t (h)", "Hourly coefficients")
            self.graph_modulation.add_line(
                range(24)[:len(datas)],
                [r[1] for r in datas],
                "orange")
        else:
            self.graph_modulation.canvas.setVisible(False)

    def save(self):
        '''saves sectors, hydrographs table and modulation curves'''
        self.save_sectors_table()
        if not self.active_modulation is None:
            self.save_modulation()
        self.conn.commit()
        self.close()

if __name__=="__main__":
    import sys
    from qgis.PyQt.QtWidgets import QApplication
    from ..project import Project

    project_name = sys.argv[1]
    app = QApplication(sys.argv)

    project = Project(project_name, debug=True)

    test_dialog = WaterDeliverySectorSettings(project)
    test_dialog.exec_()
