# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

import os
from qgis.PyQt import uic
from qgis.PyQt import QtWidgets, QtCore
from qgis.PyQt.QtCore import pyqtSignal
from qgis.PyQt.QtWidgets import QTableWidgetItem, QWidget, QGridLayout
from ...utility.string import quote

class TableWidget(QWidget):
    data_edited = pyqtSignal()
    data_updated = pyqtSignal()

    class DelegateLineEdit(QtWidgets.QItemDelegate):
        def __init__(self, parent=None, maxlen=24):
            QtWidgets.QItemDelegate.__init__(self, parent)
            self.maxlen = maxlen

        def createEditor(self, parent, option, index):
            line_edit = QtWidgets.QLineEdit(parent)
            line_edit.setMaxLength(self.maxlen)
            return line_edit


    def __init__(self, cursor, data_columns,
            title_columns, schema_name, table_name, callbacks, filter="",
            pk="name", order_by='', parent=None, autoInit=True):

        self.__is_loading = True
        QWidget.__init__(self, parent)

        if parent:
            tmplayout = QGridLayout(parent)
            tmplayout.addWidget(self)

        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "table_widget.ui"), self)

        self.cursor = cursor
        self.data_cols = data_columns
        self.__schema_name = schema_name
        self.__table_name = table_name
        self.__filter = filter
        self.__pk = pk
        self.__order_by = order_by
        self.__selected_pk = None

        self.table.setColumnCount(0)
        self.table.setColumnCount(len(self.data_cols))
        self.table.setHorizontalHeaderLabels(title_columns)
        self.table.horizontalHeader().setStretchLastSection(True)
        self.table.setSelectionMode(QtWidgets.QAbstractItemView.SingleSelection)
        self.table.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)
        self.table.resizeColumnsToContents()
        self.table.resizeRowsToContents()
        self.table.setEditTriggers(QtWidgets.QAbstractItemView.AllEditTriggers)

        self.table.itemChanged.connect(self.on_data_edited)
        self.table.itemSelectionChanged.connect(self.on_selection_changed)

        self.btnAdd.clicked.connect(callbacks[0])
        self.btnDel.clicked.connect(callbacks[1])

        if autoInit:
            self.update_data()

        self.show()
        self.__is_loading = False

    def on_data_edited(self, new_value):
        self.save_selected_row()
        if len(self.table.selectedItems())>0:
            self.__selected_pk = self.table.item(self.table.row(self.table.selectedItems()[0]), self.data_cols.index(self.__pk)).text()
        if not self.__is_loading:
            self.table.resizeColumnsToContents()
            self.table.horizontalHeader().setStretchLastSection(True);
            self.data_edited.emit()

    def on_selection_changed(self):
        if len(self.table.selectedItems())>0:
            self.__selected_pk = self.table.item(self.table.row(self.table.selectedItems()[0]), self.data_cols.index(self.__pk)).text()

    def set_filter(self, filter):
        if (filter != self.__filter):
            self.__filter = filter
            self.update_data()

    def setDelegate(self, delegate, column, argmap={}):
        if delegate == "LineEdit":
            if column is not None:
                self.table.setItemDelegateForColumn(column, TableWidget.DelegateLineEdit(parent=None, **argmap))
            else:
                self.table.setItemDelegate(TableWidget.DelegateLineEdit(parent=None, **argmap))

    def add_row(self, new_row_data=(), additional_rows=None):
        additional_rows = "," + ",".join(additional_rows) if additional_rows is not None else ""

        if len(new_row_data)<len(self.data_cols):
            new_row_data = list(new_row_data)
            while len(new_row_data)<len(self.data_cols)-1:
                new_row_data.append('null')

        new_row_formated = []
        for i in range(len(new_row_data)):
            new_row_formated.append(str(new_row_data[i]))

        self.cursor.execute(f"""insert into {self.__schema_name}.{self.__table_name}
                            ({",".join([x for x in self.data_cols if x != self.__pk]) + additional_rows})
                            values ({",".join(quote(str(value)) for value in new_row_formated)});""")
        self.update_data()
        self.table.selectRow(self.table.rowCount()-1)

    def save_selected_row(self):
        if len(self.table.selectedItems())>0:
            data = list()
            for i in range(0, len(self.data_cols)):
                try:
                    value = float(self.table.item(self.table.row(self.table.selectedItems()[0]), i).text())
                except:
                    value = self.table.item(self.table.row(self.table.selectedItems()[0]), i).text()
                if value is not None:
                    data.append("{}={}".format(self.data_cols[i], quote(str(value))))
            self.cursor.execute(f"""update {self.__schema_name}.{self.__table_name} set {",".join(data)} where {self.__pk}='{self.__selected_pk}';""")

    def del_selected_row(self):
        if len(self.table.selectedItems())>0:
            for item in self.table.selectedItems():
                self.cursor.execute(f"delete from {self.__schema_name}.{self.__table_name} where {self.__pk}='{self.__selected_pk}';")
            self.update_data()

    def update_data(self):
        self.__is_loading = True
        self.cursor.execute(f"""select {",".join(self.data_cols)}
                                from {self.__schema_name}.{self.__table_name}
                                {self.__filter}
                                order by {self.__order_by or self.__pk};""")
        datas = self.cursor.fetchall()

        self.table.setRowCount(0)
        for item in datas:
            row = self.table.rowCount()
            self.table.setRowCount(row + 1)
            for i in range(0, len(item)):
                new_item = QTableWidgetItem(self.__to_str(item[i]))
                self.table.setItem(row, i, new_item)
        self.table.resizeColumnsToContents()
        self.table.resizeRowsToContents()
        self.table.horizontalHeader().setStretchLastSection(True)
        self.data_updated.emit()
        self.__is_loading = False

    def __to_str(self, value):
        if value is None:
            return ""
        elif type(value)==int:
            return str(value)
        elif type(value)==float:
            return str(value)
        else:
            return value
