# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

import os
import itertools
from qgis.PyQt import uic
from qgis.PyQt.QtCore import pyqtSignal, Qt
from qgis.PyQt.QtWidgets import QApplication, QTableWidgetItem, QWidget, QGridLayout, QHeaderView


class ArrayWidget(QWidget):
    valuesChanged = pyqtSignal()

    def __init__(self, title_columns, size, array, parent=None, vertical_headers=None):
        QWidget.__init__(self, parent)

        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "array_widget.ui"), self)

        if parent:
            tmplayout = QGridLayout(parent)
            tmplayout.addWidget(self)

        self.max_len = size[0]
        self.width = size[1]

        assert len(title_columns) == self.width
        self.table.setColumnCount(self.width)
        self.table.setHorizontalHeaderLabels(title_columns)

        self.vertical_headers = vertical_headers
        if self.vertical_headers:
            self.table.setVerticalHeaderLabels(self.vertical_headers)

        if array:
            assert len(array) <= self.max_len
            self.set_table_items(array)

        self.table.cellChanged.connect(self.__cell_changed)
        self.table.currentCellChanged.connect(self.emit_values_changed)

        self.add.clicked.connect(self.__add_row)
        self.delete.clicked.connect(self.__delete_row)
        self.show()

        self.table.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)

    def emit_values_changed(self):
        self.valuesChanged.emit()

    def __add_row(self):
        if self.table.rowCount() <self.max_len:
            self.table.insertRow(self.table.currentRow())
        elif self.table.rowCount()==self.max_len:
            if not self.__row_is_full(self.max_len-1):
                self.table.insertRow(self.table.currentRow())
                self.table.setRowCount(self.max_len)
        self.__set_length()

    def __delete_row(self):
        selected = self.table.selectedRanges()
        if selected:
            for i in range(selected[0].topRow(), selected[0].bottomRow() + 1):
                self.table.removeRow(selected[0].topRow())
        self.__set_length()

    def set_table_items(self, array):
        self.table.blockSignals(True)
        self.table.setRowCount(0) # clear data first
        if array is not None:
            # pasted array may not have the right shape, reshape
            flatten_list = list(itertools.chain(*array))
            n = len(flatten_list)//self.width
            array = [
                [flatten_list[r*self.width + k] for k in range(self.width)]
                for r in range(n)]

            assert len(array) <= self.max_len
            effective_rows = 0
            for row in range(0, n):
                if tuple(array[row]) != tuple([None for k in range(self.width)]):
                    self.table.insertRow(self.table.rowCount())
                    for i in range(self.width):
                        self.table.setItem(effective_rows,i,QTableWidgetItem(str(array[row][i])))
                    effective_rows = effective_rows +1
        self.__set_length()
        self.table.blockSignals(False)

    def get_table_items(self):
        n = self.table.rowCount()
        result = list()
        index_list=0
        for i in range(0, n):
            if self.__row_is_full(i):
                result.append(list())
                m = self.table.columnCount()
                for j in range(self.width):
                    result[index_list].append(float(self.table.item(i,j).text()))
                index_list = index_list + 1
        return result

    def __cell_changed(self,row,column):
        if row == (self.table.rowCount()-1):
            self.__set_length()
        self.valuesChanged.emit()

    def __set_length(self):
        n = self.table.rowCount()
        if self.__row_is_full(n-1) and n < self.max_len:
            self.table.setRowCount(n+1)
        if self.table.rowCount() > self.max_len:
            self.table.setRowCount(self.max_len)
        elif self.table.rowCount()==0:
            self.table.setRowCount(1)

        if self.vertical_headers:
            self.table.setVerticalHeaderLabels(self.vertical_headers)

    def __row_is_full(self, row):
        row_full = True
        for i in range(self.width):
            if self.table.item(row,i) is None:
                row_full = False
        return row_full

    def keyPressEvent(self, event):
        if self.table.hasFocus():
            if event.key() == Qt.Key_V and  (event.modifiers() & Qt.ControlModifier):
                self.__paste()
                event.accept()
            elif event.key() == Qt.Key_C and  (event.modifiers() & Qt.ControlModifier):
                self.__copy()
                event.accept()
            else:
                event.ignore()
        else:
            event.ignore()

    def __paste(self):
        text = QApplication.clipboard().text()
        row0 = self.table.currentRow()
        array = [[celltext for j, celltext in enumerate(textline.split('\t')) if celltext.strip()!=''] for i, textline in enumerate(text.split('\n'))]
        self.set_table_items(array)

    def __copy(self):
        selected = self.table.selectedRanges()
        copied_text = ""
        for i in range(selected[0].topRow(), selected[0].bottomRow() + 1):
            for j in range(selected[0].leftColumn(), selected[0].rightColumn() + 1):
                try:
                    copied_text += self.table.item(i, j).text() + "\t"
                except AttributeError:
                    # quand une case n'a jamais été initialisée
                    copied_text += "\t"
            copied_text = copied_text[:-1] + "\n"  # le [:-1] élimine le '\t' en trop
        copied_text = copied_text[:-1]  # le [:-1] élimine le '\n' en trop
        # enregistrement dans le clipboard
        QApplication.clipboard().setText(copied_text)
