# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of XPRESSO, a QGIS plugin for hydraulics                             ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


import os
from math import pi, sqrt
from qgis.PyQt import uic
from qgis.PyQt.QtGui import QPixmap
from qgis.PyQt.QtWidgets import QWidget, QDialog, QLabel
from .array_widget import ArrayWidget
from .graph_widget import GraphWidget

class ArrayAndGraph(QWidget):
    def __init__(self, parent=None, array=None, max_len=10, title='TITRE', columns=["COL1", "COL2"], vertical_headers=None, image=None, bar=False, rotate=False):
        QWidget.__init__(self, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "array_and_graph.ui"), self)

        self.title = title
        self.columns = columns

        self.graph_widget = GraphWidget(self.graph_placeholder, bar=bar, rotate=rotate)
        self.other_graph_widget = GraphWidget(self.other_graph_placeholder, bar=bar, rotate=rotate)
        self.array_widget = ArrayWidget(columns, [max_len, 2], array, parent=self.array_placeholder, vertical_headers=vertical_headers)
        self.array_widget.valuesChanged.connect(self.draw_graph)

        # display image if provided
        if image is not None:
            img_label = QLabel(parent=self)
            img_label.setPixmap(QPixmap(image))
            self.layout().addWidget(img_label)

    def draw_graph(self):
        '''refresh curve graph'''
        datas = self.array_widget.get_table_items()
        if len(datas)>0:
            self.graph_widget.clear()
            self.other_graph_widget.clear()
            if self.title == 'Hourly coefs.':
                self.graph_widget.add_line(
                    range(24),
                    [r[0] for r in datas],
                    "dodgerblue" if abs(sum([round(r[0], 4) for r in datas]) - 24) < 0.001 else 'red',
                    self.title,
                    "t (h)", "Hourly coefficients")
                self.graph_widget.add_line(
                    range(24),
                    [r[1] for r in datas],
                    "aqua" if abs(sum([round(r[1], 4) for r in datas]) - 24) < 0.001 else 'red')
                self.other_graph_widget.canvas.setVisible(False)
            elif self.title == 'S(Z) Curve':
                self.graph_widget.add_line(
                    [r[0] for r in datas],
                    [r[1] for r in datas],
                    "r",
                    self.title,
                    self.columns[0], self.columns[1])
                # calcul du volume pour une géométrie de révolution
                v = [0]
                for i in range(len(datas) - 1):
                    h = datas[i+1][0] - datas[i][0]
                    r0, r1 = sqrt(datas[i][1]/pi), sqrt(datas[i+1][1]/pi)
                    v.append(v[-1] +  pi*h*(r1**2 + r0**2 + r1*r0)/3)
                self.other_graph_widget.add_line(
                    [r[0] for r in datas],
                    v,
                    "b",
                    "V(Z) Curve",
                    self.columns[0], "Volume (m3)")
            else:
                self.graph_widget.add_line(
                    [r[0] for r in datas],
                    [r[1] for r in datas],
                    "r",
                    self.title,
                    self.columns[0], self.columns[1])
                self.other_graph_widget.canvas.setVisible(False)
        else:
            self.graph_widget.canvas.setVisible(False)
            self.other_graph_widget.canvas.setVisible(False)

if __name__=="__main__":
    import sys
    from qgis.PyQt.QtWidgets import QApplication, QDialog, QHBoxLayout

    app = QApplication(sys.argv)
    dialog = QDialog()
    layout = QHBoxLayout()
    widget = ArrayAndGraph(dialog, [[1,2],[3,4]])
    layout.addWidget(widget)
    dialog.setLayout(layout)
    dialog.exec_()
