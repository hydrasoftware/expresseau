# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


import os
from functools import partial
from qgis.PyQt import uic, QtCore
from qgis.PyQt.QtWidgets import QDialog, QTableWidgetItem, QComboBox
from .widgets.table_widget import TableWidget
from ..utility.string import get_nullable_sql_float, get_str, get_sql_float

class WaterDeliveryManager(QDialog):
    def __del__(self):
        self.cursor.close()
        self.conn.close()

    def __init__(self, project, parent=None):
        QDialog.__init__(self, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "water_delivery_manager.ui"), self)

        self.conn = project.connect()
        self.cursor = self.conn.cursor()
        self.cursor.execute("select name from api.hourly_modulation_curve order by name")
        self.modulation_curves = self.cursor.fetchall()

        self.__is_loading = True

        self.buttonBox.accepted.connect(self.save)
        self.buttonBox.rejected.connect(self.close)

        self.table_water_delivery_scenario = TableWidget(self.cursor,
            ("name", "comment"), ("Name", "Comment"),
            "api", "water_delivery_scenario",
            (self.new_water_delivery_scenario, self.delete_water_delivery_scenario), "",
            pk="name", order_by="name='WD_SCN_REF' desc, name asc",
            parent=self.table_water_delivery_scenario_placeholder)
        for row in range(self.table_water_delivery_scenario.table.rowCount()):
            if self.table_water_delivery_scenario.table.item(row, 0).text() == 'WD_SCN_REF':
                # disable SCN REF editing
                self.table_water_delivery_scenario.table.item(row, 0).setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
                self.table_water_delivery_scenario.table.item(row, 1).setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
        self.table_water_delivery_scenario.data_edited.connect(self.set_current_water_delivery_scenario)
        self.table_water_delivery_scenario.data_edited.connect(self.save_water_delivery_scenario)
        self.table_water_delivery_scenario.table.itemSelectionChanged.connect(self.set_active_water_delivery_scenario)

        self.table_sectors_settings.setHorizontalHeaderLabels([self.tr('Sector'), self.tr('Domestic\nvolume (m3/d)'), self.tr('Adjustment\ncoef.'), self.tr('Leak efficiency\nrate')])
        self.table_sectors_settings.resizeColumnsToContents()
        self.table_sectors_settings.horizontalHeader().setStretchLastSection(True)

        self.table_industrial_volumes.setHorizontalHeaderLabels([self.tr('Water delivery\npoint'), self.tr('Industrial\nvolume (m3/d)'), self.tr('Target\nnode'), self.tr('Model'), self.tr('Industrial\ncurve')])
        self.table_industrial_volumes.itemChanged.connect(self.volume_changed)

        self.active_water_delivery_scenario=None
        self.set_active_water_delivery_scenario()

        self.__is_loading = False

    def set_current_water_delivery_scenario(self):
        items = self.table_water_delivery_scenario.table.selectedItems()
        if len(items)>0:
            self.active_water_delivery_scenario = items[0].text()
        else:
            self.active_water_delivery_scenario = None

    def set_active_water_delivery_scenario(self):
        '''populates water delivery scn settings based on select water delivery scn'''
        items = self.table_water_delivery_scenario.table.selectedItems()
        if len(items)>0:
            if self.active_water_delivery_scenario is not None:
                self.save_water_delivery_scenario()

            self.active_water_delivery_scenario = items[0].text()
            self.cursor.execute(f"""select period from api.water_delivery_scenario where name='{self.active_water_delivery_scenario}'""")
            period, =  self.cursor.fetchone()
            self.radioWeekdays.setEnabled(True and self.active_water_delivery_scenario != 'WD_SCN_REF')
            self.radioWeekdays.setChecked(period=='Weekday')
            self.radioWeekend.setEnabled(True and self.active_water_delivery_scenario != 'WD_SCN_REF')
            self.radioWeekend.setChecked(period=='Weekend')
            self.radioAuto.setEnabled(True and self.active_water_delivery_scenario != 'WD_SCN_REF')
            self.radioAuto.setChecked(period=='Auto')
        else:
            self.active_water_delivery_scenario = None
            self.radioWeekdays.setEnabled(False)
            self.radioWeekend.setEnabled(False)
            self.radioAuto.setEnabled(False)
        self.set_water_delivery_scenario_settings_table_items()
        self.refresh_water_delivery_table()

    def new_water_delivery_scenario(self):
        '''creates water delivery scn'''
        self.table_water_delivery_scenario.add_row(['default'])

    def delete_water_delivery_scenario(self):
        '''delete selected water delivery scn'''
        self.table_water_delivery_scenario.del_selected_row()

    def save_water_delivery_scenario(self):
        '''saves water delivery scns'''
        self.table_water_delivery_scenario.save_selected_row()
        if self.active_water_delivery_scenario is not None and self.active_water_delivery_scenario != 'WD_SCN_REF':
            period = 'Weekday' if self.radioWeekdays.isChecked() else ('Weekend' if self.radioWeekend.isChecked() else 'Auto')
            self.cursor.execute(f"""update api.water_delivery_scenario
                        set period='{period}'
                        where name='{self.active_water_delivery_scenario}';""")
            items = self.get_water_delivery_scenario_settings_table_items()
            if len(items)>0:
                self.cursor.execute(f"""insert into api.water_delivery_scenario_sector_setting(scenario, sector, volume_m3j, adjust_coef, leak_efficiency)
                    values {','.join([f"('{self.active_water_delivery_scenario}','{item[0]}',{item[1]},{item[2]},{item[3]})" for item in items])}""")

    def set_water_delivery_scenario_settings_table_items(self):
        '''populates water delivery scn settings'''
        self.table_sectors_settings.setRowCount(0)
        if self.active_water_delivery_scenario is not None:
            self.cursor.execute(f"""select wds.name, settings.volume_m3j, coalesce(settings.adjust_coef, 1), coalesce(settings.leak_efficiency, 1)
                                    from api.water_delivery_sector as wds
                                    left join api.water_delivery_scenario_sector_setting as settings on wds.name = settings.sector and settings.scenario='{self.active_water_delivery_scenario}'
                                    order by wds.name;""")
            sectors_settings = self.cursor.fetchall()
            self.table_sectors_settings.setRowCount(len(sectors_settings))
            for row in range(0, len(sectors_settings)):
                for col in range(0, 4):
                    self.table_sectors_settings.setItem(row, col, QTableWidgetItem(get_str(sectors_settings[row][col])))
                    if col == 0 or self.active_water_delivery_scenario == 'WD_SCN_REF':
                        # disable editing of sectors names & REF SCN properties
                        self.table_sectors_settings.item(row, col).setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
        self.table_sectors_settings.resizeColumnsToContents()
        self.table_sectors_settings.horizontalHeader().setStretchLastSection(True)

    def get_water_delivery_scenario_settings_table_items(self):
        '''returns current water delivery scn settings'''
        result = list()
        index_list=0
        for i in range(0, self.table_sectors_settings.rowCount()):
            result.append(list())
            for o in range(0, 1):
                result[index_list].append(self.table_sectors_settings.item(i,o).text())
            for o in range(1, 4):
                result[index_list].append(get_nullable_sql_float(self.table_sectors_settings.item(i,o).text()))
            index_list = index_list + 1
        return result

    def refresh_water_delivery_table(self):
        self.__is_loading = True
        self.table_industrial_volumes.setRowCount(0)
        if self.active_water_delivery_scenario is not None:
            self.cursor.execute(f"""select s.water_delivery_point, s.volume, n.name, n.model, n.industrial_curve
                                    from api.water_delivery_point as p
                                    join api.water_delivery_scenario_industrial_volume as s
                                        on p.name = s.water_delivery_point and s.water_delivery_scenario='{self.active_water_delivery_scenario}'
                                    join api.user_node as n on n.id = p._node
                                    order by s.water_delivery_point;""")
            points = self.cursor.fetchall()
            total_industrial_volumes = {}
            self.table_industrial_volumes.setRowCount(len(points))
            for row in range(0, self.table_industrial_volumes.rowCount()):
                name, volume, target_node, target_model, curve =points[row]
                # Point name
                self.table_industrial_volumes.setItem(row, 0, QTableWidgetItem(name))
                self.table_industrial_volumes.item(row, 0).setFlags(QtCore.Qt.ItemIsEnabled)
                # Volume
                self.table_industrial_volumes.setItem(row, 1, QTableWidgetItem(str(volume)))
                if self.active_water_delivery_scenario=='WD_SCN_REF':
                    self.table_industrial_volumes.item(row, 1).setFlags(QtCore.Qt.ItemIsEnabled)
                # Target node
                self.table_industrial_volumes.setItem(row, 2, QTableWidgetItem(target_node))
                self.table_industrial_volumes.item(row, 2).setFlags(QtCore.Qt.ItemIsEnabled)
                # Target node model
                self.table_industrial_volumes.setItem(row, 3, QTableWidgetItem(target_model))
                self.table_industrial_volumes.item(row, 3).setFlags(QtCore.Qt.ItemIsEnabled)
                # Industrial curve on target node
                cbox = self.get_new_modulation_combo(curve)
                cbox.currentIndexChanged.connect(partial(self.combo_modulation_index_changed, row))
                self.table_industrial_volumes.setCellWidget(row, 4, cbox)
        self.table_industrial_volumes.resizeColumnsToContents()
        self.table_industrial_volumes.horizontalHeader().setStretchLastSection(True)
        self.__is_loading = False

    def get_new_modulation_combo(self, selected_curve):
        '''creates combobox with "constant" option + all modulation curves'''
        combobox = QComboBox()
        combobox.addItem("Constant")
        for curve, in self.modulation_curves:
            combobox.addItem(curve)
        combobox.setCurrentText(selected_curve)
        return combobox

    def combo_modulation_index_changed(self, irow):
        '''triggerred at modulation curve changed in combo'''
        if not self.__is_loading:
            self.cursor.execute(f"""update api.user_node set industrial_curve='{self.table_industrial_volumes.cellWidget(irow, 4).currentText()}'
                                     where name='{self.table_industrial_volumes.item(irow, 2).text()}' and model='{self.table_industrial_volumes.item(irow, 3).text()}';""")

    def volume_changed(self, item):
        '''triggerred when water delivery point volume is changed'''
        if item.column() != 1:
            return
        row = item.row()
        if not self.__is_loading and self.active_water_delivery_scenario is not None:
            if item.column() == 1:
                name = self.table_industrial_volumes.item(row, 0).text()
                volume = get_sql_float(self.table_industrial_volumes.item(row, 1).text())
                self.cursor.execute(f"""update api.water_delivery_scenario_industrial_volume
                                            set volume={volume}
                                            where water_delivery_point='{name.replace("'", "''")}'
                                            and water_delivery_scenario='{self.active_water_delivery_scenario}';""")

    def save(self):
        '''saves form and commit'''
        if self.active_water_delivery_scenario is not None:
            self.save_water_delivery_scenario()
        self.conn.commit()
        self.close()

if __name__=="__main__":
    import sys
    from qgis.PyQt.QtWidgets import QApplication
    from ..project import Project

    project_name = sys.argv[1]
    app = QApplication(sys.argv)

    project = Project(project_name, debug=True)

    test_dialog = WaterDeliveryManager(project)
    test_dialog.exec_()
