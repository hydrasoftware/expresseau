# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of XPRESSO, a QGIS plugin for xpressoulics                           ##
##     (see <http://xpresso-software.net/>).                                                  ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

"""
Main expresseau menu
"""

import os
from pathlib import Path
from qgis.PyQt.QtCore import QCoreApplication
from qgis.PyQt.QtWidgets import QToolBar, QToolButton, QPushButton, QMenu, QCheckBox, QLabel
from ..project import Project
from ..qgis_utilities import QGisProjectManager
from wstar.w14_to_gpkg import process, result_exists
from ..service import get_service
from qgis import processing
from pathlib import Path
#from qgis.core import (
#    QgsProcessingContext,
#    QgsProcessingAlgRunnerTask,
#    QgsProcessingFeedback,
#    QgsApplication,
#)
#
# why this is here ? https://github.com/qgis/QGIS/issues/38583#issuecomment-692517863
#context = QgsProcessingContext()
#feedback = QgsProcessingFeedback()

def tr(msg):
    return QCoreApplication.translate('@default', msg)

class ExpresseauToolbar(QToolBar):
    def __init__(self, log_manager, parent=None):
        QToolBar.__init__(self, tr("Expresseau toolbar"), parent)
        self.__parent = parent
        self.__log_manager = log_manager

        self.__model_menu = QToolButton()
        self.__model_menu.setMenu(QMenu())
        self.__model_menu.menu().aboutToShow.connect(self.__refresh_model_menu)
        self.__model_menu.setPopupMode(QToolButton.MenuButtonPopup)
        self.__model_menu.setToolTip(self.tr("Current model"))

        self.__config_menu = QToolButton()
        self.__config_menu.setMenu(QMenu())
        self.__config_menu.menu().aboutToShow.connect(self.__refresh_config_menu)
        self.__config_menu.setPopupMode(QToolButton.MenuButtonPopup)
        self.__config_menu.setToolTip(self.tr("Current configuration"))

        project = Project(QGisProjectManager.project_name(), self.__log_manager)
        self.__config_menu.setText(project.current_config or self.tr('Default'))

        self.__scn_menu = QToolButton()
        self.__scn_menu.setMenu(QMenu())
        self.__scn_menu.menu().aboutToShow.connect(self.__refresh_scn_menu)
        self.__scn_menu.setPopupMode(QToolButton.MenuButtonPopup)
        self.__scn_menu.setToolTip(self.tr("Current scenario"))

        self.__run_button = QPushButton(self.tr('Run computation'), self)
        self.__run_button.clicked.connect(self.__run)

        self.__postprocess = QCheckBox(self.tr('Postprocess'))
        self.__postprocess.setChecked(False)
        self.__postprocess.setToolTip(self.tr('If checked, post-processing is run automatically after computation, you can also check to launch post-processing for the current scenario.'))
        self.__postprocess.toggled.connect(self.__postprocess_toggled)

        self.addWidget(QLabel(self.tr('Configuration:')))
        self.addWidget(self.__config_menu)
        self.addWidget(QLabel(self.tr('Current model:')))
        self.addWidget(self.__model_menu)
        self.addWidget(QLabel(self.tr('Current scenario:')))
        self.addWidget(self.__scn_menu)
        self.addWidget(self.__run_button)
        self.addWidget(self.__postprocess)

        self.variables_changed()
        self.__log_manager.notice(self.tr("toolbar created"))

    def variables_changed(self):
        project = Project(QGisProjectManager.project_name(), self.__log_manager)
        self.__model_menu.setText(QGisProjectManager.get_qgis_variable('current_model') or '')
        scenario = QGisProjectManager.get_qgis_variable('current_scenario') or ''
        run = "✓" if scenario and (Path(project.directory) / scenario.upper()).exists() else ""
        self.__scn_menu.setText(scenario+run)
        self.__run_button.setEnabled(self.__scn_menu.text() != '')
        ll = QGisProjectManager.get_layer('measure')
        if ll:
            res = project.measure_interval
            if res:
                ll.setSubsetString(f"t>='{res[0].isoformat()}' and t<='{res[1].isoformat()}'")
            else:
                ll.setSubsetString("")
            ll.reload()

    def __refresh_model_menu(self):
        menu = self.__model_menu.menu()
        menu.clear()
        project = Project(QGisProjectManager.project_name(), self.__log_manager)
        for model in project.models:
            if model != self.__model_menu.text():
                action = menu.addAction(model)
                action.triggered.connect(self.__set_curent_model)

    def __set_curent_model(self):
        QGisProjectManager.set_qgis_variable('current_model', self.sender().text() or '')

    def __refresh_config_menu(self):
        menu = self.__config_menu.menu()
        menu.clear()
        action = menu.addAction(self.tr('Default'))
        action.triggered.connect(self.__set_curent_config)
        project = Project(QGisProjectManager.project_name(), self.__log_manager)
        for config in project.configurations:
            if config != self.__config_menu.text():
                action = menu.addAction(config)
                action.triggered.connect(self.__set_curent_config)

    def __set_curent_config(self):
        project = Project(QGisProjectManager.project_name(), self.__log_manager)
        project.current_config = self.sender().text() if self.sender().text() != self.tr('Default') else None
        self.__config_menu.setText(project.current_config or self.tr('Default'))
        QGisProjectManager.refresh_layers()

    def __refresh_scn_menu(self):
        menu = self.__scn_menu.menu()
        menu.clear()
        project = Project(QGisProjectManager.project_name(), self.__log_manager)
        for scenario in project.scenarios:
            if scenario != self.__scn_menu.text():
                run = "✓" if (Path(project.directory) / scenario.upper()).exists() else ""
                action = menu.addAction(scenario+run)
                action.triggered.connect(self.__set_curent_scn)

    def __set_curent_scn(self):
        QGisProjectManager.set_qgis_variable('current_scenario', self.sender().text().replace("✓", "") or '')
        ll = QGisProjectManager.get_layer('measure')
        if ll:
            res = Project(QGisProjectManager.project_name(), self.__log_manager).measure_interval
            if res:
                ll.setSubsetString(f"t>='{res[0].isoformat()}' and t<='{res[1].isoformat()}'")
            else:
                ll.setSubsetString("")
            ll.reload()

    def __run(self):
        QGisProjectManager.exit_edition()
        scenario = self.__scn_menu.text().replace("✓", "")
        project = Project(QGisProjectManager.project_name(), self.__log_manager)
        QGisProjectManager.remove_results(scenario)

        params = { "project": project.name, "scenario": scenario, "postprocess":  self.__postprocess.isChecked()}
        processing.execAlgorithmDialog('expresseau:simulate', params)

    def __postprocess_toggled(self, flag=False):
        scenario = self.__scn_menu.text().replace("✓", "")
        project = Project(QGisProjectManager.project_name(), self.__log_manager)
        result_dir = Path(project.directory) / scenario.upper() / 'hydraulique'
        gpkg = os.path.join(result_dir, f'{scenario.upper()}.gpkg')
        QGisProjectManager.remove_results(scenario)
        comput_mode, = project.fetchone("""select comput_mode from api.scenario s where s.name =%s""", (scenario,))
        models = [m[0] for m in project.fetchall(f"SELECT regexp_split_to_table(models, ' ') FROM api.model_grouping('{scenario}') ORDER BY priority;")]
        if flag:
            #if not os.path.exists(gpkg) and result_exists(scenario, models)
            if not os.path.exists(gpkg) and result_exists(result_dir, scenario, models): #fix marjo 21/10/24 pour adaptation à modif wstar
                #process(get_service(), project, scenario)
                process(get_service(), project.name, scenario) #fix marjo 21/10/24 pour adaptation à modif wstar
            if os.path.exists(gpkg):
                QGisProjectManager.load_results(scenario, gpkg, comput_mode)
