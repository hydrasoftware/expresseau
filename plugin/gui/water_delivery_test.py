# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


"""
run test to check if tables used exist on the database

USAGE

   python -m plugin.gui.water_delivery_test [-dhk]

OPTIONS

   -h, --help
        print this help

   -d, --debug
        run in debug mode (lots of output)

   -k, --keep
        keeps database after test
"""
assert(__name__ == "__main__")

import sys
import getopt
from qgis.PyQt import QtCore
from qgis.PyQt.QtWidgets import QApplication, QTableWidgetItem
from plugin.project import Project
from plugin.gui.water_delivery_manager import WaterDeliveryManager
from plugin.gui.water_delivery_sector_settings import WaterDeliverySectorSettings
from plugin.database import TestProject, project_exists, create_project

try:
    optlist, args = getopt.getopt(sys.argv[1:],
            "hdk",
            ["help", "debug", "keep"])
except Exception as e:
    sys.stderr.write(str(e)+"\n")
    exit(1)

optlist = dict(optlist)

if "-h" in optlist or "--help" in optlist:
    help(sys.modules[__name__])
    exit(0)

if not project_exists(TestProject.NAME):
    sys.stdout.write("creating template database... ")
    TestProject.reset_template()
    sys.stdout.write("ok\n")

debug = "-d" in optlist or "--debug" in optlist
keep = "-k" in optlist or "--keep" in optlist

dbname = "water_delivery_xtest"
model_name = "model"
test_project = TestProject(dbname, keep)
project_test = Project(dbname, debug=debug)
project_test.add_new_model(model_name)
project_test.current_model = model_name

project_test.execute("insert into api.water_delivery_sector(name,geom) values('sect_1','SRID=2154; MULTIPOLYGON(((988700 6561800, 991000 6561800, 991000 6559500, 988700 6559500, 988700 6561800)))'::geometry);")
project_test.execute("insert into api.water_delivery_sector(name,geom) values('sect_2','SRID=2154; MULTIPOLYGON(((988700 6561800, 991000 6561800, 991000 6559500, 988700 6559500, 988700 6561800)))'::geometry);")
project_test.execute("insert into api.water_delivery_sector(name,geom) values('sect_3','SRID=2154; MULTIPOLYGON(((988700 6561800, 991000 6561800, 991000 6559500, 988700 6559500, 988700 6561800)))'::geometry);")
project_test.execute("insert into api.water_delivery_sector(name,geom) values('sector_1','SRID=2154; MULTIPOLYGON(((5034.74868256 1244835.8256,6239.35294856 1241081.94253,12332.4094103 1244093.4532,8830.65282309 1244821.81857,5034.74868256 1244835.8256)))'::geometry);")
project_test.execute("insert into api.water_delivery_sector(name,geom) values('sector_2','SRID=2154; MULTIPOLYGON(((2233.3434128 1248435.63137,1336.89372647 1246894.85847,4614.5378921 1247006.91468,5104.7838143 1248113.46976,2233.3434128 1248435.63137)))'::geometry);")
project_test.execute("insert into api.user_node(model, geom) values('model', 'SRID=2154; POINT (6730 1247206)'::geometry);")
project_test.execute("insert into api.user_node(model, geom) values('model', 'SRID=2154; POINT (2376 1247444)'::geometry);")
project_test.execute("insert into api.user_node(model, geom) values('model', 'SRID=2154; POINT (4378 1247254)'::geometry);")
project_test.execute("insert into api.user_node(model, geom) values('model', 'SRID=2154; POINT (2149 1247959)'::geometry);")
project_test.execute("insert into api.pipe_link(geom) values('SRID=2154; LINESTRING (6730 1247206, 2376 1247444)'::geometry);")
project_test.execute("insert into api.pipe_link(geom) values('SRID=2154; LINESTRING (2376 1247444, 4378 1247254)'::geometry);")
project_test.execute("insert into api.pipe_link(geom) values('SRID=2154; LINESTRING (4378 1247254, 2149 1247959)'::geometry);")
project_test.execute("insert into api.pipe_link(geom) values('SRID=2154; LINESTRING (2149 1247959, 6730 1247206)'::geometry);")
project_test.execute("insert into api.pipe_link(geom) values('SRID=2154; LINESTRING (2376 1247444, 2149 1247959)'::geometry);")
project_test.execute("insert into api.water_delivery_point(geom, volume, industrial) values('SRID=2154; POINT (4379 1247258)'::geometry, 12, True);")
project_test.execute("insert into api.water_delivery_point(geom, volume, industrial) values('SRID=2154; POINT (2378 1247475)'::geometry, 15, True);")
project_test.execute("insert into api.water_delivery_point(geom, volume, industrial) values('SRID=2154; POINT (6737 1247250)'::geometry, 20, False);")
project_test.execute("insert into api.water_delivery_point(geom, volume, industrial) values('SRID=2154; POINT (2145 1247960)'::geometry, 18, False);")

app = QApplication(sys.argv)

# WaterDeliveryManager
water_delivery_manager_dialog = WaterDeliveryManager(project_test)
water_delivery_manager_dialog.new_water_delivery_scenario()
water_delivery_manager_dialog.new_water_delivery_scenario()
water_delivery_manager_dialog.new_water_delivery_scenario()
water_delivery_manager_dialog.table_water_delivery_scenario.table.selectRow(2)
water_delivery_manager_dialog.delete_water_delivery_scenario()
water_delivery_manager_dialog.table_water_delivery_scenario.table.selectRow(1)
water_delivery_manager_dialog.table_sectors_settings.setItem(0, 1, QTableWidgetItem("500.5"))
water_delivery_manager_dialog.table_sectors_settings.setItem(0, 2, QTableWidgetItem("1.28"))
water_delivery_manager_dialog.table_sectors_settings.setItem(0, 3, QTableWidgetItem("0.6"))
water_delivery_manager_dialog.table_sectors_settings.setItem(1, 1, QTableWidgetItem("10280"))
water_delivery_manager_dialog.table_sectors_settings.setItem(1, 2, QTableWidgetItem("1.092"))
water_delivery_manager_dialog.table_sectors_settings.setItem(1, 3, QTableWidgetItem("0.85"))
water_delivery_manager_dialog.table_water_delivery_scenario.table.selectRow(1)
assert water_delivery_manager_dialog.table_industrial_volumes.rowCount()==2
settings = water_delivery_manager_dialog.get_water_delivery_scenario_settings_table_items()
assert float(settings[0][1])==500.5
assert float(settings[0][2])==1.28
assert float(settings[0][3])==0.6
assert float(settings[1][1])==10280
assert float(settings[1][2])==1.092
assert float(settings[1][3])==0.85

# WaterDeliverySectorSettings
water_delivery_sectors_settings_dialog = WaterDeliverySectorSettings(project_test)
water_delivery_sectors_settings_dialog.table_sectors.selectRow(4)
assert water_delivery_sectors_settings_dialog.table_sectors.item(4, 0).text()=='sector_2'
assert water_delivery_sectors_settings_dialog.table_sector_nodes.rowCount()==3
water_delivery_sectors_settings_dialog.new_modulation()
water_delivery_sectors_settings_dialog.new_modulation()
water_delivery_sectors_settings_dialog.new_modulation()
water_delivery_sectors_settings_dialog.table_modulation.table.selectRow(2)
water_delivery_sectors_settings_dialog.delete_modulation()

sys.stdout.write("ok\n")

