# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of XPRESSO, a QGIS plugin for xpressoulics                           ##
##     (see <http://xpresso-software.net/>).                                                  ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

"""
Main expresseau menu
"""

import os
import webbrowser
from datetime import datetime
from qgis.PyQt.QtCore import Qt
from qgis.PyQt.QtGui import QIcon
from qgis.PyQt.QtWidgets import QMenu, QFileDialog, QMessageBox, QInputDialog
from ..project import Project
from ..qgis_utilities import QGisProjectManager
from .project_manager import ProjectManager
from .new_project_dialog import NewProjectDialog
from .new_model_dialog import NewModelDialog
from .water_delivery_sector_settings import WaterDeliverySectorSettings
from .water_delivery_manager import WaterDeliveryManager
from .scenario_manager import ScenarioManager
from ..bin.extract import Extract
from ..bin.animo import Animo
from ..utility.license import check_license, activate_license, save_key, get_serial, get_build, build_file
from qgis.PyQt.QtGui import QPixmap

_current_dir = os.path.dirname(__file__)

class ExpresseauMenu(QMenu):
    def __init__(self, log_manager, parent=None):
        QMenu.__init__(self, "&Expresseau", parent)
        self.__log_manager = log_manager
        self.setIcon(QIcon(os.path.join(_current_dir, '..', 'ressources', 'images', 'expresseau_logo.png')))
        self.aboutToShow.connect(self.__refresh)
        self.pm = None

    def __refresh(self):
        self.clear()
        self.addAction(self.tr("&Manage projects")).triggered.connect(self.__manage_projects)
        self.addAction(self.tr("&New project")).triggered.connect(self.__project_new)

        if QGisProjectManager.project_name() != '':
            project = Project(QGisProjectManager.project_name(), self.__log_manager)
            current_model = QGisProjectManager.get_qgis_variable('current_model')

            '''returns the model menu, creates it if needed'''
            model_menu = self.addMenu(self.tr("&Models"))
            set_current_menu = model_menu.addMenu(self.tr("&Set current"))
            for model in project.models:
                action = set_current_menu.addAction(model)
                action.triggered.connect(self.__set_current_model)
                if model == current_model:
                    f = action.font()
                    f.setBold(True)
                    action.setFont(f)

            model_menu.addAction(self.tr("&Add model")).triggered.connect(self.__add_model)
            model_menu.addAction(self.tr("&Delete current model")).triggered.connect(self.__delete_current_model)

            '''Creates the hydrology menu'''
            water_delivery_menu = self.addMenu(self.tr("&Water delivery"))
            water_delivery_menu.addAction(self.tr("&Water delivery sector settings")).triggered.connect(self.__water_delivery_sector_settings)
            water_delivery_menu.addAction(self.tr("Water delivery &scenario")).triggered.connect(self.__water_delivery_scenario)
            water_delivery_menu.addAction(self.tr("&Regulation file from selected fire hydrants")).triggered.connect(self.__regulation_file_from_selected_hydrants)

            '''Creates the scenario menu'''
            self.addAction(self.tr("&Scenarios")).triggered.connect(self.__scenario_manager)

            '''Creates the advanced tools menu'''
            advanced_menu = self.addMenu(self.tr("&Advanced tools"))
            advanced_menu.addAction(self.tr("&Extract")).triggered.connect(self.__extract)
            advanced_menu.addAction(self.tr("&Anim'eau")).triggered.connect(self.__animo)
            advanced_menu.addAction(self.tr("&Skeletonize")).triggered.connect(self.__skeletonize)

        self.addAction(self.tr("Licence activation")).triggered.connect(self.__license_activation)
        self.addAction(self.tr("Save custom layer styles")).triggered.connect(self.__save_layer_styles)
        self.addAction(self.tr("&Help")).triggered.connect(self.__help)
        self.addAction(self.tr("About")).triggered.connect(self.__about)

    def __help(self):
        webbrowser.open('https://xpresseau.hydra-software.net', new=0)

    def __save_layer_styles(self):
        confirm = QMessageBox(QMessageBox.Question, self.tr("Save custom layers styles"), self.tr("This will overwrite all your custom layer styles"), QMessageBox.Ok | QMessageBox.Cancel).exec_()
        if confirm == QMessageBox.Ok:
            QGisProjectManager.save_custom_qml()

    def __skeletonize(self):
        project = Project(QGisProjectManager.project_name(), self.__log_manager)
        project.execute("select api.skeletonize()")

    def __regulation_file_from_selected_hydrants(self):
        project = Project(QGisProjectManager.project_name(), self.__log_manager)
        layer = QGisProjectManager.get_layer('fire_hydrant')
        if layer is None or layer.selectedFeatureCount() == 0:
            return
        file, __ = QFileDialog.getSaveFileName(self, self.tr("Select a file"), filter="Regulation files (*.ctl)", directory=project.directory)
        if file:
            names = ','.join([f"'{f['name']}'" for f in layer.selectedFeatures()])
            with open(file, 'w') as ctl:
                models = set()

                for hydrants_names, nominal_flow_m3h, draw_time_h, node_name, model in project.fetchall(f"""
                        select string_agg(h.name, ', '), sum(h.nominal_flow_m3h), max(h.draw_time_h), n.name, n.model
                        from api.fire_hydrant h
                        join api.node n on n.id=h._node
                        where h.name in ({names})
                        group by n.model, n.name
                        order by n.model, n.name
                        """):
                    if model not in models:
                        ctl.write(f"$case ({model})" + "\n\n")
                        models.add(model)

                    ctl.write(f"! hydrants : {hydrants_names}" + "\n\n"
                        + f"CONS '{node_name}'" + "\n"
                        + f"IF ( TIME GT 0.00 ) THEN (QINC {nominal_flow_m3h}) ENDIF" + "\n"
                        + f"IF ( TIME GT {draw_time_h:.2f} ) THEN (QINC 0) ENDIF" + "\n\n"
                        )

    def __manage_projects(self):
        self.pm and self.pm.setParent(None)
        self.pm = ProjectManager(self.parent())
        self.pm.show()

    def __project_new(self):
        dialog = NewProjectDialog(self.parent())
        if dialog.Accepted == dialog.exec_():
            project = Project.create_new_project(dialog.return_name(), dialog.return_srid(), dialog.return_directory(), self.__log_manager)
            QGisProjectManager.open_project(project.qgs, project.srid)

    def __set_current_model(self):
        QGisProjectManager.set_qgis_variable('current_model', self.sender().text())

    def __add_model(self):
        dialog = NewModelDialog(self)
        ok = dialog.exec_()
        if ok == dialog.Accepted:
            project = Project(QGisProjectManager.project_name(), self.__log_manager)
            project.add_new_model(dialog.return_name())

    def __delete_current_model(self):
        current_model = QGisProjectManager.get_qgis_variable('current_model')
        confirm = QMessageBox(QMessageBox.Question, self.tr("Delete model"), self.tr("This will delete model ")+current_model+self.tr(". Proceed?"), QMessageBox.Ok | QMessageBox.Cancel).exec_()
        if confirm == QMessageBox.Ok:
            project = Project(QGisProjectManager.project_name(), self.__log_manager)
            project.delete_model(current_model)

    def __water_delivery_sector_settings(self):
        project = Project(QGisProjectManager.project_name(), self.__log_manager)
        WaterDeliverySectorSettings(project).exec_()

    def __water_delivery_scenario(self):
        project = Project(QGisProjectManager.project_name(), self.__log_manager)
        WaterDeliveryManager(project).exec_()

    def __scenario_manager(self):
        project = Project(QGisProjectManager.project_name(), self.__log_manager)
        ScenarioManager(project).exec_()

    def __extract(self):
        project = Project(QGisProjectManager.project_name(), self.__log_manager)
        Extract(project.extract_directory).exec_()

    def __animo(self):
        project = Project(QGisProjectManager.project_name(), self.__log_manager)
        if project.current_scenario is not None and project.current_model is not None:
            Animo(project, project.current_scenario, project.current_model).animo()
        else:
            QMessageBox.critical(self.parent(), self.tr('Error'), self.tr('Please choose a model and a scenario.'), QMessageBox.Ok)


    def __license_activation(self):
        if not check_license():
            self.user_activate_license()
        else:
            confirm = QMessageBox(QMessageBox.Question, self.tr('Licence already activated'), self.tr('A license has already been activated. Do you want to activate it again?'), QMessageBox.Ok | QMessageBox.Cancel).exec_()
            if confirm == QMessageBox.Ok:
                self.user_activate_license()

    def user_activate_license(self):
        license, ok = QInputDialog.getText(self.parent(), self.tr('Hydra license activation'), self.tr('Enter your license number:'))
        if ok:
            try:
                key = activate_license(license)
                save_key(key)
                self.__log_manager.notice(self.tr("Licence activated"))
                QMessageBox.information(self.parent(), self.tr('Activation successfull'), self.tr('Your license has successfully been activated!'), QMessageBox.Ok)
            except Exception as e:
                self.__log_manager.error(str(e))
                QMessageBox.critical(self.parent(), self.tr('Error'), self.tr('There has been an error during your license activation. Please contact technical support.'), QMessageBox.Ok)
        if not check_license():
            self.__log_manager.warning(self.tr("No license activated"))


    def __about(self):
        build_info = get_build().split('\n')
        version =  build_info[1].split(':')[1]
        date_ = datetime.fromtimestamp(os.stat(build_file).st_ctime).strftime("%Y-%m-%d")

        about_pic = QPixmap(os.path.join(_current_dir, '..', 'ressources', 'images', 'expresseau_logo.png')).scaledToHeight(32, Qt.SmoothTransformation)

        about_text = f"<b>Expresseau version {version} {date_}</b><br><br>"
        about_file = os.path.join(_current_dir, '..', 'license_short.txt')
        with open(about_file, 'r') as f:
            about_text += f.read()
        about_text += f"<br><br><b>Build info</b><br>{'<br>'.join([line.replace('    ', '- ') for line in build_info])}"
        about_text += f"<br><b>Serial number</b><br>{get_serial()}"

        about_box = QMessageBox()
        about_box.setWindowTitle(self.tr('About Expresseau'))
        about_box.setIconPixmap(about_pic)
        about_box.setTextFormat(Qt.RichText)
        about_box.setText(about_text)
        about_box.exec_()
