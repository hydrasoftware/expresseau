# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


import os
import re
import csv
import datetime
from qgis.PyQt import uic
from qgis.PyQt.QtCore import Qt
from qgis.PyQt.QtWidgets import QDialog
from .widgets.table_widget import TableWidget
from .scn_widgets.comput_settings import ComputSettingsWidget
from .scn_widgets.model_ordering import ModelOrderingWidget
from .scn_widgets.regulation_and_configuration import RegulationConfigurationWidget
from .scn_widgets.comput_options import ComputOptionsWidget
from .scn_widgets.water_quality import WaterQualityWidget

class ScenarioManager(QDialog):
    def __del__(self):
        self.cursor.close()
        self.conn.close()

    def __init__(self, project, parent=None):
        QDialog.__init__(self, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "scenario_manager.ui"), self)
        self.setWindowFlags(self.windowFlags() | Qt.WindowSystemMenuHint | Qt.WindowMinMaxButtonsHint)
        self.setWindowState(Qt.WindowMaximized)

        self.project = project
        self.conn = project.connect()
        self.cursor = self.conn.cursor()

        self.is_loading = True

        self.buttonBox.accepted.connect(self.save)
        self.buttonBox.rejected.connect(self.close)
        self.clone.clicked.connect(self.clone_scenario)

        self.comput_settings = ComputSettingsWidget(self.cursor, self.project.directory, self.comput_settings_placeholder)
        self.model_ordering = ModelOrderingWidget(self.cursor, self.model_ordering_placeholder)
        self.regulation_and_configuration = RegulationConfigurationWidget(self.cursor, self.project.directory, self.regulation_and_configuration_placeholder)
        self.comput_options = ComputOptionsWidget(self.cursor, self.project.directory, self.project.models, self.comput_options_placeholder)
        self.water_quality = WaterQualityWidget(self.cursor, self.project.models, self.water_quality_placeholder)


        self.table_scenario = TableWidget(self.cursor,
            ("name", "comment"),
            (self.tr("Name"), self.tr("Comment")),
             "api",  "scenario",
            (self.new_scenario, self.delete_scenario), "", pk="name",
            parent=self.table_scenario_placeholder, autoInit=False)
        self.table_scenario.update_data()
        self.table_scenario.setDelegate("LineEdit", 0, {'maxlen': 24})

        self.select_scenario(self.project.current_scenario)

        self.active_scn = None
        self.set_active_scenario()
        self.table_scenario.data_edited.connect(self.update_scenario_metadata)
        self.table_scenario.table.itemSelectionChanged.connect(self.set_active_scenario)

        self.comput_settings.steady_state.toggled.connect(self.comput_mode_changed)
        self.comput_settings.gradual_transient.toggled.connect(self.comput_mode_changed)
        self.comput_settings.fast_transient.toggled.connect(self.comput_mode_changed)
        self.comput_settings.exterior_fire_defense_nominal_flow.toggled.connect(self.comput_mode_changed)
        self.comput_settings.exterior_fire_defense_flow_at_1bar.toggled.connect(self.comput_mode_changed)
        self.comput_settings.break_criticality.toggled.connect(self.comput_mode_changed)
        self.comput_mode_changed()

        self.comput_settings.flag_rest.toggled.connect(self.source_scn_activated)
        self.comput_settings.flag_rstart.toggled.connect(self.source_scn_activated)
        self.comput_settings.scenario_rstart.currentIndexChanged.connect(self.source_scn_activated)
        self.comput_settings.scenario_ref_btn.stateChanged.connect(self.source_scn_activated)
        self.comput_settings.scenario_ref.currentIndexChanged.connect(self.source_scn_activated)
        self.source_scn_activated()

        self.is_loading = False

    def comput_mode_changed(self):
        self.comput_settings.duration.setEnabled(not self.comput_settings.steady_state.isChecked())
        self.comput_settings.timestep.setEnabled(not self.comput_settings.steady_state.isChecked())
        self.comput_settings.refined_discretisation.setEnabled(
            self.comput_settings.gradual_transient.isChecked() or
            self.comput_settings.steady_state.isChecked() or
            self.comput_settings.exterior_fire_defense_nominal_flow.isChecked() or
            self.comput_settings.exterior_fire_defense_flow_at_1bar.isChecked() or
            self.comput_settings.break_criticality.isChecked()
            )
        if self.comput_settings.fast_transient.isChecked():
            self.comput_settings.refined_discretisation.setChecked(True)
        if self.comput_settings.exterior_fire_defense_nominal_flow.isChecked() \
                or self.comput_settings.exterior_fire_defense_flow_at_1bar.isChecked() \
                or self.comput_settings.break_criticality.isChecked():
            self.comput_settings.flag_rstart.setChecked(True)


    def source_scn_activated(self):
        '''disable useless parts of the scenario parameters UI based
        on the activation of a Reference Scenario or Hot-start scenario'''
        hot_start_active = self.comput_settings.flag_rstart.isChecked()
        hot_start_scenario = str(self.comput_settings.scenario_rstart.currentText()) or None

        if hot_start_active:
            # self.comput_options.equipment_failure.setEnabled(False)
            # self.comput_options.water_vaporization.setEnabled(False)
            self.water_quality.setEnabled(False)
            if self.active_scn:
                #self.__copy_failures(self.active_scn, hot_start_scenario) # 19/04/24 marjo comment this line (debug #494)
                self.__copy_water_quality(self.active_scn, hot_start_scenario)
        else:
            # self.comput_options.equipment_failure.setEnabled(True)
            # self.comput_options.water_vaporization.setEnabled(True)
            self.water_quality.setEnabled(True)

        if self.active_scn:
            self.regulation_and_configuration.set(self.active_scn)
            self.comput_options.set(self.active_scn) # 19/04/24 marjo uncomment this line  (debug #494)
            self.water_quality.set(self.active_scn)

    def __copy_failures(self, target_scn, source_scn):
        ''' sets computation options parameters to a
        copy of those of the source scenario'''
        self.cursor.execute(f"""delete from api.scenario_link_failure where scenario='{target_scn}';""")
        self.cursor.execute(f"""delete from api.scenario_pump_failure where scenario='{target_scn}';""")
        if source_scn is not None:
            self.cursor.execute(f"""
                insert into api.scenario_link_failure (scenario, link, link_type, failure_timestamp, failure_curve)
                select '{target_scn}', link, link_type, failure_timestamp, failure_curve
                from api.scenario_link_failure where scenario='{source_scn}';""")
            self.cursor.execute(f"""
                insert into api.scenario_pump_failure (scenario, link, link_type, failure_timestamp, failure_curve, failure_mode)
                select '{target_scn}', link, link_type, failure_timestamp, failure_curve, failure_mode
                from api.scenario_pump_failure where scenario='{source_scn}';""")

    def __copy_water_quality(self, target_scn, source_scn):
        ''' sets water_quality parameters to a
        copy of those of the source scenario'''
        self.cursor.execute(f"""delete from api.scenario_water_origin where scenario='{target_scn}';""")

        if source_scn:
            self.cursor.execute(f"""update api.scenario
                set (quality_output, chlorine, trihalomethane, passive_tracer, travel_time,
                chlorine_water_reaction_coef, chlorine_wall_reaction_coef, trihalomethane_reaction_parameter, trihalomethane_concentration_limit) =
                    (select quality_output, chlorine, trihalomethane, passive_tracer, travel_time,
                chlorine_water_reaction_coef, chlorine_wall_reaction_coef, trihalomethane_reaction_parameter, trihalomethane_concentration_limit
                     from api.scenario where name='{source_scn}')
                where name='{target_scn}';""")

            self.cursor.execute(f"""
                insert into api.scenario_water_origin (scenario, imposed_piezometry_singularity, comment)
                select '{target_scn}', imposed_piezometry_singularity, comment
                from api.scenario_water_origin where scenario='{source_scn}';
                """)

    def clone_scenario(self):
        if self.active_scn is not None:
            self.save_active_scn()
            scn_fields = """comment, comput_mode, starting_date, duration, timestep,
                            refined_discretisation, water_delivery_scenario,
                            tini, flag_save, tsave_hr, scenario_rstart, trstart_hr,
                            dt_output, tbegin_output_hr, tend_output_hr, scenario_ref, model_connect_settings,
                            option_file, four_quadrant_file, vaporization, quality_output,
                            chlorine, trihalomethane, passive_tracer, travel_time,
                            chlorine_water_reaction_coef, chlorine_wall_reaction_coef,
                            trihalomethane_reaction_parameter, trihalomethane_concentration_limit"""

            self.cursor.execute(f"""
                insert into api.scenario({scn_fields})
                select {scn_fields}
                from api.scenario where name='{self.active_scn}'
                returning name;
                """)
            name_new_scenario, = self.cursor.fetchone()

            # Model ordering
            self.cursor.execute(f"""
                do
                $$
                    declare
                        old_group api.mixed%rowtype;
                    begin
                        for old_group in select * from api.mixed where scenario='{self.active_scn}' loop
                            with new_group as (
                                insert into api.groupe default values returning name
                            ),
                            grp_mod as (
                                insert into api.groupe_model(groupe, model)
                                select n.name, gm.model
                                from new_group as n, api.groupe_model as gm
                                where gm.groupe=old_group.groupe
                            )
                            insert into api.mixed (priority, scenario, groupe)
                            select mx.priority, '{name_new_scenario}', ng.name
                            from api.mixed as mx, new_group as ng
                            where mx.scenario='{self.active_scn}' and mx.groupe=old_group.groupe;
                        end loop;
                    end;
                $$;
                ;;
                """)

            self.cursor.execute(f"""
                insert into api.cascade(priority, scenario, model)
                select priority, '{name_new_scenario}', model
                from api.cascade where scenario='{self.active_scn}';
                """)

            # Regulation
            self.cursor.execute(f"""
                insert into api.regulation_scenario(priority, regulation_file, scenario)
                select priority, regulation_file, '{name_new_scenario}'
                from api.regulation_scenario where scenario='{self.active_scn}';
                """)

            # Fichiers hydrologiques
            self.cursor.execute(f"""
                insert into api.hydrology_scenario(hydrology_file, scenario)
                select hydrology_file, '{name_new_scenario}'
                from api.hydrology_scenario where scenario='{self.active_scn}';
            """)

            self.__copy_failures(name_new_scenario, self.active_scn)
            self.__copy_water_quality(name_new_scenario, self.active_scn)

            self.table_scenario.update_data()
            self.select_scenario(name_new_scenario)
            return name_new_scenario

    def delete_scenario(self):
        if self.active_scn is not None:
            self.cursor.execute(f"""delete from api.scenario where name='{self.active_scn}'""")
            self.active_scn = None
            self.table_scenario.update_data()

    def new_scenario(self):
        self.save_active_scn()
        self.active_scn=None
        scn = self.cursor.execute("""insert into api.scenario default values returning name""")
        self.table_scenario.update_data()
        self.select_scenario(scn)

    def select_scenario(self, scenario_name):
        for i in range(0, self.table_scenario.table.rowCount()):
            if self.table_scenario.table.item(i,0).text()==scenario_name:
                self.table_scenario.table.setCurrentCell(i,0)

    def set_active_scenario(self):
        items = self.table_scenario.table.selectedItems()
        self.is_loading = True
        if len(items)>0:
            if self.active_scn is not None:
                self.save_active_scn()
            self.active_scn = items[0].text()
            self.init_ui_scn_settings()
        else:
            self.active_scn = None
            self.gbxScenarioSettings.setEnabled(False)
        self.is_loading = False

    def init_ui_scn_settings(self):
        self.gbxScenarioSettings.setEnabled(True)
        if self.active_scn is not None:
            self.comput_settings.set(self.active_scn)
            self.model_ordering.set(self.active_scn)
            self.regulation_and_configuration.set(self.active_scn)
            self.comput_options.set(self.active_scn)
            self.water_quality.set(self.active_scn)

            self.comput_mode_changed()
            self.source_scn_activated()

    def save_active_scn(self):
        if self.active_scn is not None:
            self.comput_settings.save()
            self.model_ordering.save()
            self.regulation_and_configuration.save()
            self.comput_options.save()
            self.water_quality.save()

    def update_scenario_metadata(self):
        items = self.table_scenario.table.selectedItems()
        self.active_scn = items[0].text()

        self.comput_settings.update_current_scenario_name(self.active_scn)
        self.model_ordering.update_current_scenario_name(self.active_scn)
        self.regulation_and_configuration.update_current_scenario_name(self.active_scn)
        self.comput_options.update_current_scenario_name(self.active_scn)
        self.water_quality.update_current_scenario_name(self.active_scn)

    def save(self):
        self.save_active_scn()
        if self.active_scn:
            self.project.current_scenario = self.active_scn
        self.conn.commit()
        self.close()

if __name__=="__main__":
    import sys
    from qgis.PyQt.QtWidgets import QApplication
    from ..project import Project

    project_name = sys.argv[1]
    app = QApplication(sys.argv)

    project = Project(project_name, debug=False)

    test_dialog = ScenarioManager(project)
    test_dialog.exec_()
