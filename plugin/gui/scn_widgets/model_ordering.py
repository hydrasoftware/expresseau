# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

import os
from qgis.PyQt import uic
from qgis.PyQt.QtGui import QStandardItem, QStandardItemModel
from qgis.PyQt.QtWidgets import QWidget, QGridLayout

class ModelOrderingWidget(QWidget):

    def __init__(self, cursor, parent=None):
        QWidget.__init__(self, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "model_ordering.ui"), self)
        tmplayout = QGridLayout()
        tmplayout.addWidget(self,0,0)
        parent.setLayout(tmplayout)
        self.__current_scenario = None
        self.cursor = cursor

        self.btnAddGroup.clicked.connect(self.add_groupe)
        self.btnDelGroup.clicked.connect(self.delete_groupe)
        self.btnAddModelGroup.clicked.connect(self.add_model_to_groupe)
        self.btnRemoveModelGroup.clicked.connect(self.remove_model_from_groupe)
        self.btn_up.clicked.connect(self.cascade_up)
        self.btn_down.clicked.connect(self.cascade_down)

        self.__list_model = QStandardItemModel(self.listModels)
        self.__tree_groupes = QStandardItemModel(self.treeGroups)
        self.listModels.setModel(self.__list_model)
        self.treeGroups.setModel(self.__tree_groupes)

        self.radio_mode_connexion_global.toggled.connect(self.__refresh_stacked)
        self.radio_mode_connexion_mixed.toggled.connect(self.__refresh_lists_groupes)
        self.radio_mode_connexion_cascade.toggled.connect(self.__refresh_list_cascade)

        self.show()

    def cascade_up(self):
        current_index = self.list_cascade.currentRow()
        if current_index is None:
            return
        item = self.list_cascade.takeItem(current_index)
        self.list_cascade.insertItem(current_index - 1, item)
        self.list_cascade.setCurrentRow(current_index - 1)

    def cascade_down(self):
        current_index = self.list_cascade.currentRow()
        if current_index is None:
            return
        item = self.list_cascade.takeItem(current_index)
        self.list_cascade.insertItem(current_index + 1, item)
        self.list_cascade.setCurrentRow(current_index + 1)

    def __get_selected_model_cascade(self):
        items = self.list_cascade.selectedIndexes()
        if len(items)>0:
            name = self.__list_cascade.item(items[0].row(),0).text()
            return name
        return None

    def set(self, scn):
        self.__current_scenario = scn
        if self.__current_scenario:
            self.cursor.execute(f"""
                select model_connect_settings from api.scenario
                where name='{self.__current_scenario}';""")
            model_connect_settings, = self.cursor.fetchone()
            if model_connect_settings=="Global":
                self.radio_mode_connexion_global.setChecked(True)
                self.__refresh_stacked()
            elif model_connect_settings=="Cascade":
                self.radio_mode_connexion_cascade.setChecked(True)
                self.__refresh_list_cascade()
            elif model_connect_settings=="Mixed":
                self.radio_mode_connexion_mixed.setChecked(True)
                self.__refresh_lists_groupes()
                self.treeGroups.setCurrentIndex(self.__tree_groupes.index(0, 0))

    def update_current_scenario_name(self, new_scenario_name):
        self.__current_scenario = new_scenario_name

    def __refresh_stacked(self):
        if self.radio_mode_connexion_global.isChecked():
            self.stacked.setCurrentIndex(0)
        elif self.radio_mode_connexion_cascade.isChecked():
            self.stacked.setCurrentIndex(1)
        elif self.radio_mode_connexion_mixed.isChecked():
            self.stacked.setCurrentIndex(2)

    def __refresh_list_cascade(self):
        self.__refresh_stacked()
        self.list_cascade.clear()
        self.cursor.execute(f"""
            select model from (
                select priority, model from api.cascade where scenario='{self.__current_scenario}'
                    union all
                select 999 as priority, name as model
                from api.model
                where not exists (select 1 from api.cascade where scenario='{self.__current_scenario}' and name=model)
                ) as patched_cascade
            order by priority;
        """)
        models = [m for m, in self.cursor.fetchall()]
        for model in models:
            self.list_cascade.addItem(model)

    def save(self):
        if self.__current_scenario:
            if self.radio_mode_connexion_global.isChecked():
                model_connect_settings = "Global"
            elif self.radio_mode_connexion_cascade.isChecked():
                model_connect_settings = "Cascade"
                self.__save_cascade()
            elif self.radio_mode_connexion_mixed.isChecked():
                model_connect_settings = "Mixed"

            self.cursor.execute(f"""
                update api.scenario set model_connect_settings='{model_connect_settings}' where name='{self.__current_scenario}';""")

    def __save_cascade(self):
        for i in range(0, self.list_cascade.count()):
            self.cursor.execute(f"""insert into api.cascade(priority, scenario, model) values ({i+1}, '{self.__current_scenario}', '{self.list_cascade.item(i).text()}')
                                     on conflict (scenario, model) do
                                     update set priority={i+1} where cascade.scenario='{self.__current_scenario}' and cascade.model='{self.list_cascade.item(i).text()}';""")

    def add_groupe(self):
        if not self.__current_scenario is None:
            self.__refresh_stacked()
            self.cursor.execute(f"insert into api.groupe default values returning name")
            groupe = [g for g, in self.cursor.fetchall()][0]
            self.cursor.execute(f"""insert into api.mixed(priority, scenario, groupe)
                                   values ({self.__tree_groupes.rowCount()+1}, '{self.__current_scenario}', '{groupe}')""")
            self.__refresh_lists_groupes()
            self.select_groupe_by_name(groupe)
            return groupe

    def select_groupe_by_name(self, groupe_name):
        select_groupe = self.__tree_groupes.findItems(groupe_name)
        if select_groupe:
            groupe_item = select_groupe[0]
            self.treeGroups.setCurrentIndex(groupe_item.index())

    def remove_model_from_groupe(self):
        if not self.__current_scenario is None:
            groupe, model = self.get_selected_groupe_model()
            if model is None:
                return
            self.cursor.execute(f"""delete from api.groupe_model where groupe='{groupe}' and model='{model}'""")
            self.__refresh_lists_groupes()

    def add_model_to_groupe(self):
        if not self.__current_scenario is None:
            model = self.get_selected_model()
            if model is None:
                return
            groupe = self.get_selected_groupe()
            if groupe is None:
                groupe = self.add_groupe()
            self.cursor.execute(f"""insert into api.groupe_model(groupe, model) values ('{groupe}', '{model}')""")
            self.__refresh_lists_groupes()

    def get_selected_model(self):
        items = self.listModels.selectedIndexes()
        if len(items)>0:
            return self.__list_model.item(items[0].row(),0).text()
        return None

    def get_selected_groupe_model(self):
        items = self.treeGroups.selectedIndexes()
        if len(items)>0:
            model_index = items[0]
            groupe_index = model_index.parent()
            if groupe_index is None:
                return None, None # A groupe is selected, not a model
            model = str(self.__tree_groupes.data(model_index))
            groupe = str(self.__tree_groupes.data(groupe_index))

            return groupe, model
        return None, None

    def get_selected_groupe(self):
        items = self.treeGroups.selectedIndexes()
        if len(items)>0:
            selected_item = items[0]
            if self.__tree_groupes.data(selected_item.parent()) is not None:
                return None # A model is selected, not a groupe
            return self.__tree_groupes.item(items[0].row(),0).text()
        return None

    def delete_groupe(self):
        if not self.__current_scenario is None:
            groupe = self.get_selected_groupe()
            if groupe is not None:
                self.cursor.execute(f"""delete from api.groupe where name='{groupe}'""")
                self.__refresh_lists_groupes()

    def __refresh_lists_groupes(self):
        mixed = self.radio_mode_connexion_mixed.isChecked()
        self.gbx_groupes.setVisible(mixed)

        if mixed and not self.__current_scenario is None:
            self.__list_model.setRowCount(0)
            self.__tree_groupes.setRowCount(0)

            self.cursor.execute(f"""
                select name
                from api.model
                where name not in (
                    select model
                    from api.mixed, api.groupe, api.groupe_model
                    where groupe.name = groupe_model.groupe
                    and mixed.groupe = groupe.name
                    and mixed.scenario = '{self.__current_scenario}');""")
            models = [m for m, in self.cursor.fetchall()]
            for model in models:
                item = QStandardItem(model)
                item.setEditable(False)
                self.__list_model.appendRow(item)

            self.cursor.execute(f"""
                select groupe
                from api.mixed, api.groupe
                where mixed.groupe=groupe.name
                and mixed.scenario='{self.__current_scenario}'
                order by mixed.priority""")
            groupes = [g for g, in self.cursor.fetchall()]
            for groupe in groupes:
                node_groupe = QStandardItem(groupe)
                node_groupe.setEditable(False)
                self.__tree_groupes.appendRow(node_groupe)

                self.cursor.execute(f"""
                    select name
                    from api.model
                    where name in (
                        select model from api.groupe_model
                        where groupe='{groupe}')""")
                model_groupes = [mg for mg, in self.cursor.fetchall()]
                for model in model_groupes:
                    item = QStandardItem(model)
                    item.setEditable(False)
                    node_groupe.appendRow(item)

            self.treeGroups.expandAll()
