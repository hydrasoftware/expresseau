# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

import os
from qgis.PyQt import uic
from qgis.PyQt.QtWidgets import QWidget, QTableWidgetItem, QGridLayout
from ..widgets.table_widget import TableWidget

class WaterQualityWidget(QWidget):

    def __init__(self, cursor, models, parent=None):
        QWidget.__init__(self, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "water_quality.ui"), self)
        tmplayout = QGridLayout()
        tmplayout.addWidget(self,0,0)
        parent.setLayout(tmplayout)
        self.__current_scenario = None
        self.cursor = cursor

        self.quality_output.addItem('None')
        self.quality_output.addItem('Tracer')
        self.quality_output.addItem('Water origin')
        self.quality_output.currentIndexChanged.connect(self.quality_output_changed)
        self.quality_output_changed()

        self.btn_add.clicked.connect(self.__add_water_source)
        self.btn_delete.clicked.connect(self.__delete_water_source)

        self.combo_singularity_type.addItems(['Flow injection', 'Model connection', 'Imposed piezometry'])
        self.combo_name.clear()
        for model in models:
            self.combo_model.addItem(model)
        self.combo_singularity_type.currentIndexChanged.connect(self.__refresh_water_origin_list)
        self.combo_model.currentIndexChanged.connect(self.__refresh_water_origin_list)

        self.show()

    def __refresh_water_origin_table(self):
        assert(self.__current_scenario is not None)

        self.cursor.execute(f"""
            select model, _type, name, coalesce(comment, '')
            from api.scenario_water_origin
            where scenario = '{self.__current_scenario}'
            order by name""")

        self.comment.clear()
        rows = self.cursor.fetchall()
        self.btn_add.setEnabled(len(rows) < 8)
        self.table_water_origin.setRowCount(len(rows))
        for row, item in enumerate(rows):
            for i in range(len(item)):
                self.table_water_origin.setItem(row, i, QTableWidgetItem(str(item[i])))

        self.__refresh_water_origin_list()

    def __refresh_water_origin_list(self):
        assert(self.__current_scenario is not None)

        if self.combo_model.currentIndex()!=-1:
            self.cursor.execute(f"""
                select name from api.{self.combo_singularity_type.currentText().lower().replace(' ','_')}_singularity
                where _model = %s
                and name not in (select name from api.scenario_water_origin where scenario=%s)
                order by name
                """, (self.combo_model.currentText(), self.__current_scenario))

            self.combo_name.clear()
            for name, in self.cursor.fetchall():
                self.combo_name.addItem(name)

    def __add_water_source(self):
        if self.combo_model.currentIndex()!=-1 and self.combo_name.currentIndex()!=-1:
            self.cursor.execute(f"""
                insert into api.scenario_water_origin(model, name, scenario, comment)
                values (%s, %s, %s, %s)""", (self.combo_model.currentText(), self.combo_name.currentText(), self.__current_scenario, self.comment.text() or None))
            self.__refresh_water_origin_table()

    def __delete_water_source(self):
        items = self.table_water_origin.selectedItems()
        if len(items)>0:
            selected_row = self.table_water_origin.row(items[0])
            self.cursor.execute(f"delete from api.scenario_water_origin where scenario=%s and name = %s and model = %s", (
                self.__current_scenario,
                self.table_water_origin.item(selected_row, 2).text(),
                self.table_water_origin.item(selected_row, 0).text()))
            self.__refresh_water_origin_table()

    def quality_output_changed(self):
        for widget in [self.chlorine, self.trihalomethane, self.passive_tracer, self.travel_time]:
            widget.setEnabled(self.quality_output.currentText() == 'Tracer')
            if not widget.isEnabled():
                widget.setChecked(False)

        self.water_origin.setEnabled(self.quality_output.currentText() == 'Water origin')

    def set(self, scn):
        self.__current_scenario = scn
        if self.__current_scenario:

            self.cursor.execute(f"""
                select quality_output, chlorine, trihalomethane, passive_tracer, travel_time,
                    chlorine_water_reaction_coef, chlorine_wall_reaction_coef,
                    trihalomethane_reaction_parameter, trihalomethane_concentration_limit
                from api.scenario where name=%s""", (self.__current_scenario,))
            quality_output, chlorine, trihalomethane, passive_tracer, travel_time, \
                chlorine_water_reaction_coef, chlorine_wall_reaction_coef, \
                trihalomethane_reaction_parameter, trihalomethane_concentration_limit = self.cursor.fetchone()

            self.quality_output.setCurrentText(quality_output)
            self.chlorine.setChecked(chlorine)
            self.trihalomethane.setChecked(trihalomethane)
            self.passive_tracer.setChecked(passive_tracer)
            self.travel_time.setChecked(travel_time)
            self.chlorine_water_reaction_coef.setText(str(chlorine_water_reaction_coef or ''))
            self.chlorine_wall_reaction_coef.setText(str(chlorine_wall_reaction_coef or ''))
            self.trihalomethane_reaction_parameter.setText(str(trihalomethane_reaction_parameter or ''))
            self.trihalomethane_concentration_limit.setText(str(trihalomethane_concentration_limit or ''))

            self.__refresh_water_origin_table()

    def update_current_scenario_name(self, new_scenario_name):
        self.__current_scenario = new_scenario_name

    def save(self):
        if self.__current_scenario:
            self.cursor.execute(f"""
                update api.scenario set
                    quality_output='{self.quality_output.currentText()}',
                    chlorine={self.chlorine.isChecked()},
                    trihalomethane={self.trihalomethane.isChecked()},
                    passive_tracer={self.passive_tracer.isChecked()},
                    travel_time={self.travel_time.isChecked()},
                    chlorine_water_reaction_coef={self.chlorine_water_reaction_coef.text() or "null"},
                    chlorine_wall_reaction_coef={self.chlorine_wall_reaction_coef.text() or "null"},
                    trihalomethane_reaction_parameter={self.trihalomethane_reaction_parameter.text() or "null"},
                    trihalomethane_concentration_limit={self.trihalomethane_concentration_limit.text() or "null"}
                where name='{self.__current_scenario}';""")
