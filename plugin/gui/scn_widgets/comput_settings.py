# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

import os
import re
from qgis.PyQt import uic
from qgis.PyQt.QtWidgets import QWidget, QGridLayout, QFileDialog, QAbstractItemView
from qgis.PyQt.QtGui import QColor
from ..widgets.time_input_widget import TimeInputWidget
from ...utility.license import license_type

_expresseau_dir = os.path.join(os.path.expanduser('~'), ".expresseau")

class ComputSettingsWidget(QWidget):

    def __init__(self, cursor, directory, parent=None):
        QWidget.__init__(self, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "comput_settings.ui"), self)
        tmplayout = QGridLayout()
        tmplayout.addWidget(self,0,0)
        parent.setLayout(tmplayout)

        self.directory = directory
        self.cursor = cursor
        self.__current_scenario= None

        self.flag_rstart.toggled.connect(self.flag_rstart_changed)
        self.flag_rest.toggled.connect(self.flag_rstart_changed)
        self.scenario_ref_btn.stateChanged.connect(self.scenario_ref_statechanged)

        self.duration = TimeInputWidget(self.duration_placeholder)
        self.duration.set_format(hr=99999, min=59, sec=59)

        self.tsave_hr = TimeInputWidget(self.tsave_hr_placeholder)
        self.tsave_hr.set_format(hr=99999, min=59, sec=59)

        self.tini = TimeInputWidget(self.tini_placeholder)
        self.tini.set_format(hr=99, min=59, sec=59)
        self.trstart_hr = TimeInputWidget(self.trstart_hr_placeholder)
        self.trstart_hr.set_format(hr=99999, min=59, sec=59)

        self.tbegin_output_hr = TimeInputWidget(self.tbegin_output_hr_placeholder)
        self.tbegin_output_hr.set_format(hr=99999, min=59, sec=59, can_be_negative=True)
        self.tend_output_hr = TimeInputWidget(self.tend_output_hr_placeholder)
        self.tend_output_hr.set_format(hr=99999, min=59, sec=59)

        self.add_hydrology_file.clicked.connect(self.__add_hydrology_file)
        self.remove_hydrology_file.clicked.connect(self.__remove_hydrology_file)
        self.edit_hydrology_file.clicked.connect(self.__edit_hydrology_file)

        if license_type() != 'pro':
            self.fast_transient.setEnabled(False)
            self.exterior_fire_defense_nominal_flow.setEnabled(False)
            self.exterior_fire_defense_flow_at_1bar.setEnabled(False)
            self.flag_rstart.setEnabled(False)
            self.skeletonization.setEnabled(False)

        self.scenario_rstart.currentIndexChanged.connect(self.__scenario_rstart_changed)

        self.hydrology_files.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.hydrology_files.setSelectionMode(QAbstractItemView.ExtendedSelection)

        self.show()

    def __add_hydrology_file(self):
        fileNames, __ = QFileDialog.getOpenFileNames(self, self.tr('Add hydrology file'), self.directory)
        for fileName in fileNames:
            self.cursor.execute("""
                insert into api.hydrology_scenario(scenario, hydrology_file)
                values (%s, %s)""", (self.__current_scenario, fileName))
            self.__refresh_hydrology_files()

    def __remove_hydrology_file(self):
        items = self.hydrology_files.selectedItems()
        for item in items:
            fileName = item.toolTip() or item.text()
            self.cursor.execute("""
                delete from api.hydrology_scenario where scenario=%s and hydrology_file=%s
                """, (self.__current_scenario, fileName))

        self.__refresh_hydrology_files()

    def __edit_hydrology_file(self): # ajout marjo 06/12/23
        items = self.hydrology_files.selectedItems()
        if len(items)>0:
            file = items[0].text()
            os.startfile(file)


    def __refresh_hydrology_files(self):
        self.hydrology_files.clear()
        if self.__current_scenario:
            self.cursor.execute("select hydrology_file from api.hydrology_scenario where scenario=%s", (self.__current_scenario,))
            self.hydrology_files.addItems([f for f, in self.cursor.fetchall()])
            for i in range(self.hydrology_files.count()):
                fil = self.hydrology_files.item(i).text()
                if not os.path.exists(fil):
                    nfil = re.sub(r'.*\.expresseau', _expresseau_dir.replace('\\', '/'), fil.replace('\\', '/'))
                    if os.path.exists(nfil):
                        self.hydrology_files.item(i).setToolTip(fil)
                        self.hydrology_files.item(i).setText(nfil)
                        self.hydrology_files.item(i).setForeground(QColor(0,0,255))
                    else:
                        self.hydrology_files.item(i).setForeground(QColor(255,0,0))
                else:
                    self.hydrology_files.item(i).setToolTip(None)

    def set(self, scn):
        self.__current_scenario = scn
        if self.__current_scenario:
            self.cursor.execute(f"""select name from api.scenario where name != '{scn}' order by name;""")
            other_scenarios = [name for name, in self.cursor.fetchall()]
            self.cursor.execute("""select name from api.water_delivery_scenario order by name;""")
            wd_scn_name = [name for name, in self.cursor.fetchall()]

            self.cursor.execute(f"""
                select comput_mode, starting_date, duration, timestep, refined_discretisation, vaporization, water_delivery_scenario,
                    tini, flag_save, tsave_hr, scenario_rstart, trstart_hr, dt_output, tbegin_output_hr, tend_output_hr, scenario_ref,
                    skeletonization
                from api.scenario
                where name='{scn}';""")
            comput_mode, starting_date, duration, timestep, refined_discretisation, vaporization, water_delivery_scenario, \
                tini, flag_save, tsave_hr, scenario_rstart, trstart_hr, dt_output, \
                tbegin_output_hr, tend_output_hr, scenario_ref, skeletonization = self.cursor.fetchone()

            self.set_comput_mode(comput_mode)
            self.starting_date.setDate(starting_date)
            self.starting_date.setTime(starting_date.time())
            self.duration.set_time(duration)
            self.timestep.setValue(timestep)
            self.refined_discretisation.setChecked(refined_discretisation)
            self.vaporization.setChecked(vaporization)
            self.skeletonization.setChecked(skeletonization)

            self.water_delivery_scenario.clear()
            for wd_scn in wd_scn_name:
                self.water_delivery_scenario.addItem(wd_scn)
            self.water_delivery_scenario.setCurrentText(water_delivery_scenario)

            self.scenario_ref_btn.setChecked(scenario_ref is not None)

            self.scenario_ref.clear()
            for scenario in other_scenarios:
                self.scenario_ref.addItem(scenario)
            if scenario_ref:
                self.scenario_ref.setCurrentText(scenario_ref)
            else:
                self.scenario_ref.setCurrentIndex(-1)

            self.tini.set_time(tini)
            self.flag_save.setChecked(flag_save)
            self.tsave_hr.set_time(tsave_hr)
            self.flag_rest.setChecked(not scenario_rstart)

            self.flag_rstart.setChecked(bool(scenario_rstart))

            self.scenario_rstart.blockSignals(True)
            self.scenario_rstart.clear()
            for scenario in other_scenarios:
                self.scenario_rstart.addItem(scenario)
            if scenario_rstart:
                self.scenario_rstart.setCurrentText(scenario_rstart)
            else:
                self.scenario_rstart.setCurrentIndex(-1)
            self.scenario_rstart.blockSignals(False)

            self.trstart_hr.set_time(trstart_hr)
            self.dt_output.setValue(dt_output)
            self.tbegin_output_hr.set_time(tbegin_output_hr)
            self.tend_output_hr.set_time(tend_output_hr)

            #self.flag_rstart_changed()
            #self.scenario_ref_statechanged()

            self.__refresh_hydrology_files()


    def update_current_scenario_name(self, new_scenario_name):
        self.__current_scenario = new_scenario_name

    def save(self, dummy=None):
        if self.__current_scenario:
            if self.steady_state.isChecked():
                comput_mode = "Steady state"
            elif self.gradual_transient.isChecked():
                comput_mode = "Gradual transient"
            elif self.fast_transient.isChecked():
                comput_mode = "Fast transient"
            elif self.exterior_fire_defense_nominal_flow.isChecked():
                comput_mode = "Exterior fire defense nominal flow"
            elif self.exterior_fire_defense_flow_at_1bar.isChecked():
                comput_mode = "Exterior fire defense flow at 1bar"
            elif self.break_criticality.isChecked():
                comput_mode = "Break criticality"

            self.cursor.execute(f"""
                update api.scenario set
                    comput_mode='{comput_mode}', starting_date='{str(self.starting_date.dateTime().toString("yyyy-MM-dd hh:mm:ss"))}',
                    duration='{self.duration.get_time()}', timestep={self.timestep.value()},
                    refined_discretisation={self.refined_discretisation.isChecked()}, vaporization={self.vaporization.isChecked()},
                    skeletonization={self.skeletonization.isChecked()},
                    water_delivery_scenario={"'"+self.water_delivery_scenario.currentText()+"'" if self.water_delivery_scenario.currentIndex()!=-1 else "null"},
                    tini='{self.tini.get_time()}', flag_save={self.flag_save.isChecked()}, tsave_hr='{self.tsave_hr.get_time()}',
                    scenario_rstart={"'"+self.scenario_rstart.currentText()+"'" if self.scenario_rstart.currentIndex()!=-1 and self.scenario_ref_btn.isChecked() else "null"},
                    trstart_hr='{self.trstart_hr.get_time()}', dt_output={self.dt_output.value()},
                    tbegin_output_hr='{self.tbegin_output_hr.get_time()}', tend_output_hr='{self.tend_output_hr.get_time()}',
                    scenario_ref={"'"+self.scenario_ref.currentText()+"'" if self.scenario_ref.currentIndex()!=-1 and self.scenario_ref_btn.isChecked() else "null"}
                where name='{self.__current_scenario}'""")

    def set_comput_mode(self, comput_mode):
        if comput_mode=="Steady state":
            self.steady_state.setChecked(True)
        elif comput_mode=="Gradual transient":
            self.gradual_transient.setChecked(True)
        elif comput_mode=="Fast transient":
            self.fast_transient.setChecked(True)
        elif comput_mode=="Exterior fire defense nominal flow":
            self.exterior_fire_defense_nominal_flow.setChecked(True)
        elif comput_mode=="Exterior fire defense flow at 1bar":
            self.exterior_fire_defense_flow_at_1bar.setChecked(True)
        elif comput_mode=="Break criticality":
            self.break_criticality.setChecked(True)
        else:
            raise "Unknown comput mode"

    def flag_rstart_changed(self):
        self.scenario_rstart.setEnabled(self.flag_rstart.isChecked())
        self.trstart_hr.setEnabled(self.flag_rstart.isChecked())

        if not self.flag_rstart.isChecked():
            self.scenario_rstart.setCurrentIndex(-1)
        elif not self.scenario_ref_btn.isChecked():
            self.scenario_ref_btn.setChecked(True)

    def scenario_ref_statechanged(self):
        self.scenario_ref.setEnabled(self.scenario_ref_btn.isChecked())
        if self.scenario_ref_btn.isChecked():
            self.scenario_ref.setCurrentIndex(-1)

    def __scenario_rstart_changed(self):
        if self.scenario_rstart.currentText() and not self.scenario_ref.currentText():
            self.scenario_ref_btn.setChecked(True)
            self.scenario_ref.setCurrentText(self.scenario_rstart.currentText())