# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


import os
import re
from qgis.PyQt import uic
from qgis.PyQt.QtCore import QItemSelectionModel
from qgis.PyQt.QtWidgets import QWidget, QGridLayout, QFileDialog
from qgis.PyQt.QtGui import QColor

_expresseau_dir = os.path.join(os.path.expanduser('~'), ".expresseau")

class RegulationConfigurationWidget(QWidget):

    def __init__(self, cursor, directory, parent=None):
        QWidget.__init__(self, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "regulation_and_configuration.ui"), self)
        tmplayout = QGridLayout()
        tmplayout.addWidget(self,0,0)
        parent.setLayout(tmplayout)
        self.__current_scenario = None
        self.cursor = cursor
        self.directory = directory

        self.add_btn.clicked.connect(self.__ask_and_add_reg_file)
        self.del_btn.clicked.connect(self.del_reg_file)
        self.edit_btn.clicked.connect(self.edit_reg_file)
        self.up_btn.clicked.connect(self.move_up)
        self.down_btn.clicked.connect(self.move_down)

        self.refresh_reg_list()

        self.add_config_btn.clicked.connect(self.__add_config)
        self.remove_config_btn.clicked.connect(self.__remove_config)
        self.up_btn_config.clicked.connect(self.__move_config_up)
        self.down_btn_config.clicked.connect(self.__move_config_down)

        self.refresh_conf_list()

        self.show()

    def refresh_reg_list(self):
        self.reg_file_list.clear()
        self.cursor.execute(f"""select regulation_file
                                from api.regulation_scenario
                                where scenario='{self.__current_scenario}'
                                order by priority;""")
        self.reg_file_list.addItems([f for f, in self.cursor.fetchall()])
        for i in range(self.reg_file_list.count()):
            fil = self.reg_file_list.item(i).text()
            if not os.path.exists(fil):
                nfil = re.sub(r'.*\.expresseau', _expresseau_dir.replace('\\', '/'), fil.replace('\\', '/'))
                if os.path.exists(nfil):
                    self.reg_file_list.item(i).setToolTip(fil)
                    self.reg_file_list.item(i).setText(nfil)
                    self.reg_file_list.item(i).setForeground(QColor(0,0,255))
                else:
                    self.reg_file_list.item(i).setForeground(QColor(255,0,0))
            else:
                self.reg_file_list.item(i).setToolTip(None)

    def refresh_conf_list(self, selected_config=None):
        self.available_configurations.clear()
        self.configurations.clear()
        self.cursor.execute(f"""select name
                                from api.configuration
                                except
                                select config as name
                                from api.scenario_configuration
                                where scenario='{self.__current_scenario}'
                                order by name""")
        available =  self.cursor.fetchall()

        self.cursor.execute(f"""select config
                             from api.scenario_configuration
                             where scenario='{self.__current_scenario}'
                             order by rank_""")
        configuration = self.cursor.fetchall()

        self.available_configurations.addItems([v for v, in available])
        self.configurations.addItems([v for v, in configuration])
        if selected_config is not None:
            self.configurations.setCurrentRow(selected_config, QItemSelectionModel.Select)

    def __move_config_up(self):
        items = self.configurations.selectedItems()
        if len(items) > 0 and self.configurations.row(items[0]) != 0:
            self.cursor.execute(f"select rank_ from api.scenario_configuration where scenario='{self.__current_scenario}' and config='{items[0].text()}'")
            old_rank = self.cursor.fetchone()[0]
            self.cursor.execute(f"update api.scenario_configuration set rank_=999999 where scenario='{self.__current_scenario}' and config='{items[0].text()}'")
            self.cursor.execute(f"update api.scenario_configuration set rank_={old_rank} where scenario='{self.__current_scenario}' and rank_={old_rank-1}")
            self.cursor.execute(f"update api.scenario_configuration set rank_={old_rank-1} where scenario='{self.__current_scenario}' and config='{items[0].text()}'")
            self.refresh_conf_list(max(old_rank-2, 0))

    def __move_config_down(self):
        items = self.configurations.selectedItems()
        if len(items) > 0 and self.configurations.row(items[0]) != self.configurations.count()-1:
            self.cursor.execute(f"select rank_ from api.scenario_configuration where scenario='{self.__current_scenario}' and config='{items[0].text()}'")
            old_rank = self.cursor.fetchone()[0]
            self.cursor.execute(f"update api.scenario_configuration set rank_=999999 where scenario='{self.__current_scenario}' and config='{items[0].text()}'")
            self.cursor.execute(f"update api.scenario_configuration set rank_={old_rank} where scenario='{self.__current_scenario}' and rank_={old_rank+1}")
            self.cursor.execute(f"update api.scenario_configuration set rank_={old_rank+1} where scenario='{self.__current_scenario}' and config='{items[0].text()}'")
            self.refresh_conf_list(min(old_rank, self.configurations.count()-1))

    def __add_config(self):
        items = self.available_configurations.selectedItems()
        if len(items) > 0:
            self.cursor.execute("""
                insert into api.scenario_configuration(scenario, config, rank_)
                values (%s, %s, coalesce((select max(rank_)+1 from api.scenario_configuration where scenario=%s), 1))
                """, (self.__current_scenario, items[0].text(), self.__current_scenario))
            self.refresh_conf_list()

    def __remove_config(self):
        items = self.configurations.selectedItems()
        if len(items) > 0:
            self.cursor.execute(f"delete from api.scenario_configuration where scenario='{self.__current_scenario}' and config='{items[0].text()}'")
            self.refresh_conf_list()

    def __ask_and_add_reg_file(self):
        if  self.__current_scenario is not None:
            fileNames, __ = QFileDialog.getOpenFileNames(self, self.tr('Add regulation file'), self.directory, options=QFileDialog.DontConfirmOverwrite)
            for fileName in fileNames:
                self.add_reg_file(fileName)

    def add_reg_file(self, filePath):
        if self.__current_scenario is not None:
            self.cursor.execute(f"""
                insert into api.regulation_scenario(priority, regulation_file, scenario)
                select coalesce(max(priority) + 1, 1), '{filePath}', '{self.__current_scenario}'
                from api.regulation_scenario
                where scenario='{self.__current_scenario}';""")
            self.refresh_reg_list()
            self.reg_file_list.setCurrentRow(self.reg_file_list.count()-1)

    def del_reg_file(self):
        if self.__current_scenario is not None:
            items = self.reg_file_list.selectedItems()
            if len(items)>0:
                file = items[0].toolTip() or items[0].text()
                self.cursor.execute(f"""
                    delete from api.regulation_scenario
                    where regulation_file='{file}' and scenario='{self.__current_scenario}';""")
                self.refresh_reg_list()

    def edit_reg_file(self):
        if self.__current_scenario is not None:
            items = self.reg_file_list.selectedItems()
            if len(items)>0:
                file = items[0].text()
                os.startfile(file)

    def move_up(self):
        if self.__current_scenario is not None:
            items = self.reg_file_list.selectedItems()
            if len(items)>0:
                file = items[0].toolTip() or items[0].text()
                self.cursor.execute(f"""select regulation_file
                                        from api.regulation_scenario
                                        where scenario='{self.__current_scenario}'
                                        order by priority;""")
                reg_files = [f for f, in self.cursor.fetchall()]
                index = reg_files.index(file)
                if index > 0:
                    reg_files.insert(index-1, reg_files.pop(index))
                    self.cursor.execute(f"""
                        delete from api.regulation_scenario
                        where scenario='{self.__current_scenario}';""")
                    for i, file in enumerate(reg_files):
                        self.cursor.execute(f"""
                            insert into api.regulation_scenario(priority, regulation_file, scenario)
                            values ({i+1}, '{file}', '{self.__current_scenario}');""")
                    self.refresh_reg_list()
                    self.reg_file_list.setCurrentRow(index-1)

    def move_down(self):
        if self.__current_scenario is not None:
            items = self.reg_file_list.selectedItems()
            if len(items)>0:
                file = items[0].toolTip() or items[0].text()
                self.cursor.execute(f"""select regulation_file
                                        from api.regulation_scenario
                                        where scenario='{self.__current_scenario}'
                                        order by priority;""")
                reg_files = [f for f, in self.cursor.fetchall()]
                index = reg_files.index(file)
                if index < len(reg_files)-1:
                    reg_files.insert(index+1, reg_files.pop(index))
                    self.cursor.execute(f"""
                        delete from api.regulation_scenario
                        where scenario='{self.__current_scenario}';""")
                    for i, file in enumerate(reg_files):
                        self.cursor.execute(f"""
                            insert into api.regulation_scenario(priority, regulation_file, scenario)
                            values ({i+1}, '{file}', '{self.__current_scenario}');""")
                    self.refresh_reg_list()
                    self.reg_file_list.setCurrentRow(index+1)

    def set(self, scn):
        self.__current_scenario = scn
        self.refresh_reg_list()
        self.refresh_conf_list()

    def update_current_scenario_name(self, new_scenario_name):
        self.__current_scenario = new_scenario_name

    def save(self):
        return
