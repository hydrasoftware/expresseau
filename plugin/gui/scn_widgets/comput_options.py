# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

import os
from functools import partial
from qgis.PyQt import uic
from qgis.PyQt import QtGui, QtCore
from qgis.PyQt.QtWidgets import QWidget, QGridLayout, QTableWidgetItem, QComboBox, QFileDialog
from ..widgets.time_input_widget import TimeInputWidget

_link_types = ['check_valve', 'flow_regulator', 'pressure_regulator', 'valve']

class ComputOptionsWidget(QWidget):
    def __init__(self, cursor, directory, models, parent=None):
        QWidget.__init__(self, parent)
        current_dir = os.path.dirname(__file__)
        uic.loadUi(os.path.join(current_dir, "comput_options.ui"), self)
        tmplayout = QGridLayout()
        tmplayout.addWidget(self,0,0)
        parent.setLayout(tmplayout)
        self.__current_scenario = None
        self.cursor = cursor
        self.directory = directory
        self.models = models

        self.btnFailureAdd.clicked.connect(self.__failure_add)
        self.btnFailureRemove.clicked.connect(self.__failure_remove)
        self.btnPumpAdd.clicked.connect(self.__pump_add)
        self.btnPumpRemove.clicked.connect(self.__pump_remove)

        self.failure_time = TimeInputWidget(self.failure_time_placeholder)
        self.failure_time.set_format(hr=23, min=59, sec=59)
        self.pump_time = TimeInputWidget(self.pump_time_placeholder)
        self.pump_time.set_format(hr=23, min=59, sec=59)

        self.__init_combos()

        self.cboFailureModel.activated.connect(self.__refresh_failure_list)
        self.cboFailureType.activated.connect(self.__refresh_failure_list)
        self.cboPumpModel.activated.connect(self.__refresh_pump_list)
        self.cboPumpMode.activated.connect(self.__refresh_pump_curves)
        self.btnAdd_four_quadrant_file.clicked.connect(self.__select_four_quadrant_file)
        self.btnEdit_four_quadrant_file.clicked.connect(self.__edit_four_quadrant_file)
        self.btnHelp_four_quadrant_file.clicked.connect(self.__help_four_quadrant_file)

        self.btnAdd_option_file.clicked.connect(self.__select_option_file)
        self.btnEdit_option_file.clicked.connect(self.__edit_option_file)
        self.btnHelp_option_file.clicked.connect(self.__help_option_file)

        self.show()

    def set(self, scn):
        self.__current_scenario = scn
        if self.__current_scenario:
            self.__refresh_failure_table()
            self.__refresh_pump_table()

            self.cursor.execute(f"""select four_quadrant_file, option_file from api.scenario where name='{self.__current_scenario}' limit 1;""")
            four_quadrant_file, option_file = self.cursor.fetchone()
            self.four_quadrant_file.setText(four_quadrant_file)
            self.option_file.setText(option_file)

    def update_current_scenario_name(self, new_scenario_name):
        self.__current_scenario = new_scenario_name

    def __init_combos(self):
        self.cboFailureModel.clear()
        self.cboPumpModel.clear()
        models = self.models
        for model in models:
            self.cboFailureModel.addItem(model)
            self.cboPumpModel.addItem(model)
        self.cboFailureModel.setCurrentIndex(0)
        self.cboPumpModel.setCurrentIndex(0)

        self.cboFailureType.clear()
        for type in _link_types:
            self.cboFailureType.addItem(type)
        self.cboFailureType.setCurrentIndex(0)

        self.cboFailureCurve.clear()
        self.cursor.execute("select name from api.failure_curve;")
        for curve, in self.cursor.fetchall():
            self.cboFailureCurve.addItem(curve)
        self.cboFailureCurve.setCurrentIndex(0)

        self.cboPumpMode.clear()
        self.cursor.execute("select unnest(enum_range(null::___.pump_failure_mode));")
        for mode, in self.cursor.fetchall():
            self.cboPumpMode.addItem(mode)
        self.cboPumpMode.setCurrentIndex(0)

        self.__refresh_pump_curves()

    def __refresh_pump_curves(self):
        self.cboPumpCurve.clear()
        if self.cboPumpMode.currentText()=='Alpha(t) curve':
            self.cursor.execute("select name from api.failure_curve;")
            for curve, in self.cursor.fetchall():
                self.cboPumpCurve.addItem(curve)
            self.cboPumpCurve.setCurrentIndex(0)

    # LINK FAILURES
    def __refresh_failure_table(self):
        assert(self.__current_scenario is not None)
        if self.cboFailureModel.currentIndex()!=-1:
            self.cursor.execute(f"""
                select l._model, f.link_type, l.name, f.failure_curve, f.failure_timestamp
                from api.scenario_link_failure as f
                join ({' union '.join([f"select id, _model, '{link_type}' as link_type, name from api.{link_type}_link" for link_type in _link_types])}) as l on f.link=l.id
                where f.scenario='{self.__current_scenario}'
                order by l._model, f.link_type, l.name;""")

            self.failure_table.setRowCount(0)
            for item in self.cursor.fetchall():
                rowcount = self.failure_table.rowCount() + 1
                self.failure_table.setRowCount(rowcount)
                row = rowcount - 1
                self.failure_table.setItem(row, 0, QTableWidgetItem(str(item[0])))
                self.failure_table.setItem(row, 1, QTableWidgetItem(str(item[1])))
                self.failure_table.setItem(row, 2, QTableWidgetItem(str(item[2])))
                self.failure_table.setItem(row, 3, QTableWidgetItem(str(item[3])))
                self.failure_table.setItem(row, 4, QTableWidgetItem(str(item[4])))

            self.failure_table.resizeColumnsToContents()
            self.failure_table.horizontalHeader().setStretchLastSection(True)
            self.__refresh_failure_list()

    def __refresh_failure_list(self):
        assert(self.__current_scenario is not None)
        if self.cboFailureModel.currentIndex()!=-1 and self.cboFailureType.currentIndex()!=-1:
            self.cursor.execute(f"""
                select name from api.{self.cboFailureType.currentText()}_link
                where _model = '{self.cboFailureModel.currentText()}' and
                    id not in (
                        select link from api.scenario_link_failure
                        where scenario='{self.__current_scenario}'
                        and link_type='{self.cboFailureType.currentText()}'
                ) order by name;""")
            self.cboFailureName.clear()
            for name, in self.cursor.fetchall():
                self.cboFailureName.addItem(name)
            self.cboFailureName.setCurrentIndex(0)

    def __failure_add(self):
        if self.cboFailureModel.currentIndex()==-1:
            return
        if self.cboFailureType.currentIndex()==-1:
            return
        if self.cboFailureName.currentIndex()==-1:
            return
        if self.cboFailureCurve.currentIndex()==-1:
            return

        self.cursor.execute(f"""insert into api.scenario_link_failure(scenario, link, link_type, failure_curve, failure_timestamp)
                                 select '{self.__current_scenario}', id, '{self.cboFailureType.currentText()}', '{self.cboFailureCurve.currentText()}', '{self.failure_time.get_time()}'
                                 from api.{self.cboFailureType.currentText()}_link
                                 where name='{self.cboFailureName.currentText()}' and _model = '{self.cboFailureModel.currentText()}';""")
        self.__refresh_failure_table()

    def __failure_remove(self):
        items = self.failure_table.selectedItems()
        if len(items)>0:
            selected_row = self.failure_table.row(items[0])
            self.cursor.execute(f"""delete from api.scenario_link_failure
                                     where scenario='{self.__current_scenario}'
                                     and link_type='{self.failure_table.item(selected_row, 1).text()}'
                                     and link=(select id from api.{self.failure_table.item(selected_row, 1).text()}_link
                                               where name='{self.failure_table.item(selected_row, 2).text()}'
                                               and _model='{self.failure_table.item(selected_row, 0).text()}'
                                     );""")
            self.__refresh_failure_table()

    # PUMP FAILURES
    def __refresh_pump_table(self):
        assert(self.__current_scenario is not None)
        if self.cboPumpModel.currentIndex()!=-1:
            self.cursor.execute(f"""
                select l._model, l.name, f.failure_curve, f.failure_timestamp, f.failure_mode
                from api.scenario_pump_failure as f
                join (select id, _model, name from api.pump_link) as l on f.link=l.id
                where f.scenario='{self.__current_scenario}'
                order by l._model, f.link_type, l.name;""")

            self.pump_table.setRowCount(0)
            for item in self.cursor.fetchall():
                rowcount = self.pump_table.rowCount() + 1
                self.pump_table.setRowCount(rowcount)
                row = rowcount - 1
                self.pump_table.setItem(row, 0, QTableWidgetItem(str(item[0])))
                self.pump_table.setItem(row, 1, QTableWidgetItem(str(item[1])))
                self.pump_table.setItem(row, 2, QTableWidgetItem(str(item[2])))
                self.pump_table.setItem(row, 3, QTableWidgetItem(str(item[3])))
                self.pump_table.setItem(row, 4, QTableWidgetItem(str(item[4])))

            self.pump_table.resizeColumnsToContents()
            self.pump_table.horizontalHeader().setStretchLastSection(True)
            self.__refresh_pump_list()

    def __refresh_pump_list(self):
        assert(self.__current_scenario is not None)
        if self.cboPumpModel.currentIndex()!=-1:
            self.cursor.execute(f"""
                select name from api.pump_link
                where _model = '{self.cboPumpModel.currentText()}' and
                    id not in (
                        select link from api.scenario_pump_failure
                        where scenario='{self.__current_scenario}'
                        and link_type='pump'
                ) order by name;""")
            self.cboPumpName.clear()
            for name, in self.cursor.fetchall():
                self.cboPumpName.addItem(name)
            self.cboPumpName.setCurrentIndex(0)

    def __pump_add(self):
        if self.cboPumpModel.currentIndex()==-1:
            return
        if self.cboPumpName.currentIndex()==-1:
            return
        if not (self.cboPumpCurve.currentIndex()>-1 or self.cboPumpMode.currentText()!='Alpha(t) curve'):
            return

        self.cursor.execute(f"""insert into api.scenario_pump_failure(scenario, link, link_type, failure_curve, failure_timestamp, failure_mode)
                                 select '{self.__current_scenario}', id, 'pump', nullif('{self.cboPumpCurve.currentText()}', ''), '{self.pump_time.get_time()}', '{self.cboPumpMode.currentText()}'
                                 from api.pump_link
                                 where name='{self.cboPumpName.currentText()}' and _model = '{self.cboPumpModel.currentText()}';""")
        self.__refresh_pump_table()

    def __pump_remove(self):
        items = self.pump_table.selectedItems()
        if len(items)>0:
            selected_row = self.pump_table.row(items[0])
            self.cursor.execute(f"""delete from api.scenario_pump_failure
                                     where scenario='{self.__current_scenario}'
                                     and link_type='pump'
                                     and link=(select id from api.pump_link
                                               where name='{self.pump_table.item(selected_row, 1).text()}'
                                               and _model='{self.pump_table.item(selected_row, 0).text()}'
                                     );""")
            self.__refresh_pump_table()

    def __select_four_quadrant_file(self):
        file_url, __ = QFileDialog.getSaveFileName(self, 'Select options file', self.directory, 'All file (*.*)', options=QFileDialog.DontConfirmOverwrite)
        if file_url:
            # creates file empty if not exists
            open(file_url, 'a').close()
            self.four_quadrant_file.setText(file_url)

    def __edit_four_quadrant_file(self):
        file = self.four_quadrant_file.text()
        os.startfile(file)

    def __help_four_quadrant_file(self):
        pass

    # OPTION FILE
    def __select_option_file(self):
        file_url, __ = QFileDialog.getSaveFileName(self, 'Select options file', self.directory, 'All file (*.*)', options=QFileDialog.DontConfirmOverwrite)
        if file_url:
            # creates file empty if not exists
            open(file_url, 'a').close()
            self.option_file.setText(file_url)

    def __edit_option_file(self):
        file = self.option_file.text()
        os.startfile(file)

    def __help_option_file(self):
        pass

    def save(self):
        if self.__current_scenario is not None:
            self.cursor.execute(f"""update api.scenario set four_quadrant_file='{self.four_quadrant_file.text()}', option_file='{self.option_file.text()}' where name='{self.__current_scenario}';""")
        return
