# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


"""
run project tests

USAGE

   project_test.py [-dkh]

OPTIONS

   -h, --help
        print this help

   -d, --debug
        run in debug mode (lots of output)

   -k, --keep
        keeps database after test

"""

assert(__name__ == "__main__")

import sys
import getopt
from plugin.project import Project
from plugin.database import TestProject, project_exists

try:
    optlist, args = getopt.getopt(sys.argv[1:],
            "hdk",
            ["help", "debug", "keep"])
except Exception as e:
    sys.stderr.write(str(e)+"\n")
    exit(1)

optlist = dict(optlist)

if "-h" in optlist or "--help" in optlist:
    help(sys.modules[__name__])
    exit(0)

if not project_exists(TestProject.NAME):
    sys.stdout.write("creating template database... ")
    TestProject.reset_template()
    sys.stdout.write("ok\n")

debug = "-d" in optlist or "--debug" in optlist
keep = "-k" in optlist or "--keep" in optlist

# test database creation
dbname = "project_xtest"
test_project = TestProject(dbname, keep)
project_test = Project(dbname, debug=debug)

project_test.add_new_model('model1')
project_test.add_new_model('model2')
project_test.current_model = 'model1'
assert(project_test.models == ['model1', 'model2'])
assert(project_test.current_model == 'model1')
project_test.delete_model('model1')
assert(project_test.current_model == 'model2')

project_test.add_new_scenario('scenario1')
project_test.add_new_scenario('scenario2')
project_test.current_scenario = 'scenario1'
assert(project_test.scenarios == ['scenario1', 'scenario2'])
assert(project_test.current_scenario == 'scenario1')
project_test.delete_scenario('scenario1')
assert(project_test.current_scenario == 'scenario2')

# test configuration

project_test.add_new_config("config1")
assert(project_test.current_config == 'config1')
project_test.add_new_config("config2")
assert(project_test.current_config == 'config2')
project_test.current_config = "config1"
assert(project_test.current_config == 'config1')
project_test.execute("insert into api.user_node(geom, model) values ('SRID=2154; POINT(0 0)', 'model2')")
project_test.execute("insert into api.user_node(geom, model) values ('SRID=2154; POINT(1 0)', 'model2')")
project_test.execute("insert into api.user_node(geom, model) values ('SRID=2154; POINT(0 1)', 'model2')")
project_test.execute("update api.user_node set q0=666 where id <= 2")
assert(project_test.fetchone("select q0 from api.user_node where id=2")[0] == 666)
project_test.current_config = None
assert(project_test.fetchone("select q0 from api.user_node where id=2")[0] == 0)

project_test.current_config = "config2"
project_test.execute("update api.user_node set q0=999 where id = 2")
assert(project_test.fetchone("select q0 from api.user_node where id = 2")[0] == 999)
project_test.current_config = "config1"
assert(project_test.fetchone("select q0 from api.user_node where id = 2")[0] == 666)

project_test.add_new_config("config3")
project_test.execute("update api.configured set config='config3' where name='NOD_2' and config='config2'")
project_test.current_config = "config3"
assert(project_test.fetchone("select q0 from api.user_node where id = 2")[0] == 999)
project_test.current_config = "config2"
assert(project_test.fetchone("select q0 from api.user_node where id = 2")[0] == 0)



project_test.execute("insert into api.scenario(name) values ('scn')")
project_test.execute("insert into api.scenario_configuration(scenario, config, rank_) values ('scn', 'config1', 2)")
project_test.execute("insert into api.scenario_configuration(scenario, config, rank_) values ('scn', 'config2', 1)")

project_test.execute("delete from api.configured where name='NOD_1'")

#print(project_test.fetchone("select api.file_dat('scn', 'model2')")[0])
#print("#"*81)
#print(project_test.fetchone("select api.file_config_dat('scn', 'model2')")[0])

sys.stdout.write("ok\n")
