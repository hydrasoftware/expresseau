import os

from qgis.PyQt.QtCore import QCoreApplication
from qgis.core import (
    QgsProcessingAlgorithm,
    QgsProcessingParameterString,
    QgsProcessingException
)
from ..project import Project
from wstar.w16_to_ugrid import process, has_results
from ..qgis_utilities import QGisProjectManager
from ..database import autoconnection
from .utility import ProjectWidget, ScenarioWidget
from pathlib import Path

class W16ToUgrid(QgsProcessingAlgorithm):
    """
    Algorithm running xeau
    """

    PROJECT = "project"
    SCENARIO = "scenario"

    def tr(self, string):
        return QCoreApplication.translate("Processing", string)

    def createInstance(self):
        return W16ToUgrid()

    def name(self):
        return "w16_to_ugrid"

    def displayName(self):
        return self.tr("w16_to_ugrid")

    def group(self):
        return self.tr("expresseau")

    def groupId(self):
        return ""

    def shortHelpString(self):
        return self.tr("Post-process expresseau scenario for dynamic cartography")

    def initAlgorithm(self, config=None):
        """
        Here we define the inputs and output of the algorithm, along
        with some other properties.
        """

        param = QgsProcessingParameterString(self.PROJECT, "Project")
        param.setMetadata({ 'widget_wrapper': {'class': ProjectWidget}})
        self.addParameter(param)

        param = QgsProcessingParameterString(self.SCENARIO, "Scenario")
        param.setMetadata({ 'widget_wrapper': {'class': ScenarioWidget}})
        self.addParameter(param)


    def prepareAlgorithm(self, parameters, context, feedback):
        self.success = False
        self.project = Project(self.parameterAsString(parameters, self.PROJECT, context))
        self.scenario = self.parameterAsString(parameters, self.SCENARIO, context)
        self.nc = []


        if self.project.name == QGisProjectManager.project_name():
            QGisProjectManager.exit_edition()
            QGisProjectManager.remove_ugrid(self.scenario)


        return True

    def processAlgorithm(self, parameters, context, feedback):
        feedback.pushInfo(self.tr('post-processing'))
        feedback.setProgress(10)

        #préparation liste des models et fichiers
        result_dir = Path(self.project.directory) / self.scenario.upper() / 'hydraulique'
        starting_date, = self.project.fetchone("select starting_date from api.scenario where name ilike %s", (self.scenario,))
        for model, in self.project.fetchall("select name from api.model"):
            w16 = result_dir / f"{self.scenario.upper()}_{model.upper()}.w16"
            nc = result_dir / f"{self.scenario.upper()}_{model.upper()}.nc"

            feedback.pushCommandInfo(f'rm {nc}')
            if nc.exists():
                os.remove(nc)
                feedback.pushConsoleInfo(self.tr("old ugrid file removed"))
            else:
                feedback.pushConsoleInfo(self.tr("no ugrid file to remove"))

            if not has_results(w16):
                raise QgsProcessingException(self.tr('no results for scenario ')+self.scenario)

            process(w16, nc, model, self.scenario, starting_date, autoconnection(self.project.name), feedback)
            self.nc.append(nc)

        feedback.setProgress(100)

        return {}

    def postProcessAlgorithm(self, context, feedback):
        if self.project.name == QGisProjectManager.project_name():
            if len(self.nc):
                QGisProjectManager.load_ugrid(self.scenario, self.nc)
                feedback.pushConsoleInfo(self.tr("ugrid mesh loades"))
            else:
                feedback.pushConsoleInfo(self.tr("no ugrid mesh to load"))

        return {}
