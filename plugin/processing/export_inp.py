
from qgis.PyQt.QtCore import QCoreApplication
from qgis.core import (
    QgsProcessingAlgorithm,
    QgsProcessingParameterString
)
from ..project import Project
from .utility import ProjectWidget, ScenarioWidget, ModelWidget, InpOutputWidget
from ..database.export_inp import export_ as export_inp
from ..database import is_expresseau_db

class ExportInp(QgsProcessingAlgorithm):
    """
    Algorithm running xeau
    """

    PROJECT = "project"
    MODEL = "model"
    SCENARIO = "scenario" ## pas sûre
    FILE = "file"

    def tr(self, string):
        """
        Returns a translatable string with the self.tr() function.
        """
        return QCoreApplication.translate("Processing", string)

    def createInstance(self):
        return ExportInp()

    def name(self):
        return "export inp"

    def displayName(self):
        """
        Returns the translated algorithm name, which should be used for any
        user-visible display of the algorithm name.
        """
        return self.tr("export inp")

    def group(self):
        return self.tr("expresseau")

    def groupId(self):
        return ""

    def shortHelpString(self):
        return self.tr("Export model to an epanet .inp file.")

    def initAlgorithm(self, config=None):

        param = QgsProcessingParameterString(self.PROJECT, "Project")
        param.setMetadata({ 'widget_wrapper': {'class': ProjectWidget}})
        self.addParameter(param)

        param = QgsProcessingParameterString(self.MODEL, "Model")
        param.setMetadata({ 'widget_wrapper': {'class': ModelWidget}})
        self.addParameter(param)

        param = QgsProcessingParameterString(self.SCENARIO, "Scenario")
        param.setMetadata({ 'widget_wrapper': {'class': ScenarioWidget}})
        self.addParameter(param)

        param = QgsProcessingParameterString(self.FILE, "Output EPANET inp file name")
        param.setMetadata({ 'widget_wrapper': {'class': InpOutputWidget}})
        self.addParameter(param)

        self.success = False

    def prepareAlgorithm(self, parameters, context, feedback):
        project_name = self.parameterAsString(parameters, self.PROJECT, context)
        self.project = Project(project_name)
        self.model = self.parameterAsString(parameters, self.MODEL, context)
        self.scenario = self.parameterAsString(parameters, self.SCENARIO, context)
        self.file = self.parameterAsString(parameters, self.FILE, context)

        feedback.pushConsoleInfo(f"output file : {self.file}")

        if not is_expresseau_db(project_name):
            feedback.pushConsoleInfo(f"project {project_name} do not exists")
            return False
        else:
            return True

    def processAlgorithm(self, parameters, context, feedback):

        warnings, errors = export_inp(self.file, self.project, self.model, feedback, self.scenario)
        if errors:
            feedback.reportError(self.model+self.tr(' exported to ')+self.file+self.tr(' with ')+str(errors)+self.tr(' errors and ')+str(warnings)+self.tr(' warnings'))
        elif warnings:
            feedback.pushWarning(self.model+self.tr(' exported to ')+self.file+self.tr(' with ')+str(warnings)+self.tr(' warnings') )
        else:
            feedback.pushWarning(self.model+self.tr(' exported to ')+self.file)

        self.success = True
        return {}

    def postProcessAlgorithm(self, context, feedback):
        if self.success :
            with open(self.file, encoding="utf8") as inp:
                del inp # to avoid ruf wrning on unused variable (but why do we open the file ?)

        return {}
