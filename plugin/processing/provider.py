from .simulate import Simulate
from .import_inp import ImportInp
from .export_inp import ExportInp
from .wfiles_to_csv import W14ToCsv, W17ToCsv
from .w14_to_gpkg import W14ToGpkg
from .w16_to_ugrid import W16ToUgrid
from .autograph import Autograph
from .analyse import Analyse
from qgis.core import QgsProcessingProvider

class Provider(QgsProcessingProvider):
    def loadAlgorithms(self, *args, **kwargs):
        self.addAlgorithm(Simulate())
        self.addAlgorithm(ImportInp())
        self.addAlgorithm(ExportInp())
        self.addAlgorithm(W14ToCsv())
        self.addAlgorithm(W17ToCsv())
        self.addAlgorithm(W14ToGpkg())
        self.addAlgorithm(W16ToUgrid())
        self.addAlgorithm(Autograph())
        self.addAlgorithm(Analyse())

    def id(self, *args, **kwargs):
        return 'expresseau'

    def name(self, *args, **kwargs):
        return 'expresseau'

    def icon(self):
        return QgsProcessingProvider.icon(self)
