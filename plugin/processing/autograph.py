import webbrowser

import matplotlib
matplotlib.use('Qt5Agg')

from qgis.PyQt.QtCore import QCoreApplication
from qgis.core import (
    QgsProcessingAlgorithm,
    QgsProcessingParameterString,
)
from ..service import get_service
from ..project import Project
from wstar.autograph import process
from .utility import ProjectWidget, ScenarioWidget

########################################################################

########################################################################

class Autograph(QgsProcessingAlgorithm):
    """
    Algorithm creating graphs for model calibration
    """

    PROJECT = "project"
    SCENARIO = "scenario"

    def tr(self, string):
        return QCoreApplication.translate("Processing", string)

    def createInstance(self):
        return Autograph()

    def name(self):
        return "autograph"

    def displayName(self):
        return self.tr("autograph")

    def group(self):
        return self.tr("expresseau")

    def groupId(self):
        return ""

    def shortHelpString(self):
        return self.tr("Creates measure/simualtion comparison graphs for model calibration")

    def initAlgorithm(self, config=None):

        param = QgsProcessingParameterString(self.PROJECT, "Project")
        param.setMetadata({ 'widget_wrapper': {'class': ProjectWidget}})
        self.addParameter(param)

        param = QgsProcessingParameterString(self.SCENARIO, "Scenario")
        param.setMetadata({ 'widget_wrapper': {'class': ScenarioWidget}})
        self.addParameter(param)

    def prepareAlgorithm(self, parameters, context, feedback):
        self.success = False
        self.project = Project(self.parameterAsString(parameters, self.PROJECT, context))
        self.scenario = self.parameterAsString(parameters, self.SCENARIO, context)

        return True

    def processAlgorithm(self, parameters, context, feedback):

        service = get_service()
        dbname = self.parameterAsString(parameters, self.PROJECT, context)
        scn_name = self.parameterAsString(parameters, self.SCENARIO, context)

        self.success = process(service, dbname, scn_name, feedback=feedback)

        return {}

    def postProcessAlgorithm(self, context, feedback):

        if self.success:
            url = f'http://127.0.0.1:5001/{self.project.name}/graph/{self.scenario.upper()}/autograph.html'
            feedback.pushInfo(url)
            webbrowser.open(url, new=0)

        return {}