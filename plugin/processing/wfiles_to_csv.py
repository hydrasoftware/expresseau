from pathlib import Path

from qgis.PyQt.QtCore import QCoreApplication
from qgis.core import (
    QgsProcessingAlgorithm,
    QgsProcessingParameterString
)
from ..project import Project
from .utility import ProjectWidget, ScenarioWidget
from wstar.w14_to_csv import convert as convert_w14
from wstar.w17_to_csv import convert as convert_w17


class W14ToCsv(QgsProcessingAlgorithm):

    PROJECT = "project"
    SCENARIO = "scenario"

    def tr(self, string):
        return QCoreApplication.translate("Processing", string)

    def createInstance(self):
        return W14ToCsv()

    def name(self):
        return "w14 to csv"

    def displayName(self):
        return self.tr("w14 to csv")

    def group(self):
        return self.tr("expresseau")

    def groupId(self):
        return ""

    def shortHelpString(self):
        return self.tr("Convert w14 to csv")

    def initAlgorithm(self, config=None):

        param = QgsProcessingParameterString(self.PROJECT, "Project")
        param.setMetadata({ 'widget_wrapper': {'class': ProjectWidget}})
        self.addParameter(param)

        param = QgsProcessingParameterString(self.SCENARIO, "Scenario")
        param.setMetadata({ 'widget_wrapper': {'class': ScenarioWidget}})
        self.addParameter(param)

    def processAlgorithm(self, parameters, context, feedback):
        project = Project(self.parameterAsString(parameters, self.PROJECT, context))
        scenario = self.parameterAsString(parameters, self.SCENARIO, context)

        w14s = [str(v) for v in (Path(project.directory) / scenario.upper() / "hydraulique").glob('*.w14')]
        for i, w14 in enumerate(w14s):
            feedback.setProgressText(f"converting {w14}")
            feedback.setProgress(int(100*(i+1)/len(w14s)))
            with open(w14[:-4]+'.csv', 'w') as out:
                convert_w14(w14, out)

        return {}

class W17ToCsv(QgsProcessingAlgorithm):

    PROJECT = "project"
    SCENARIO = "scenario"

    def tr(self, string):
        return QCoreApplication.translate("Processing", string)

    def createInstance(self):
        return W17ToCsv()

    def name(self):
        return "w17 to csv"

    def displayName(self):
        return self.tr("w17 to csv")

    def group(self):
        return self.tr("expresseau")

    def groupId(self):
        return ""

    def shortHelpString(self):
        return self.tr("Convert w17 to csv")

    def initAlgorithm(self, config=None):

        param = QgsProcessingParameterString(self.PROJECT, "Project")
        param.setMetadata({ 'widget_wrapper': {'class': ProjectWidget}})
        self.addParameter(param)

        param = QgsProcessingParameterString(self.SCENARIO, "Scenario")
        param.setMetadata({ 'widget_wrapper': {'class': ScenarioWidget}})
        self.addParameter(param)

    def processAlgorithm(self, parameters, context, feedback):
        project = Project(self.parameterAsString(parameters, self.PROJECT, context))
        scenario = self.parameterAsString(parameters, self.SCENARIO, context)

        w17s = [str(v) for v in (Path(project.directory) / scenario.upper() / "hydraulique").glob('*.w17')]
        for i, w17 in enumerate(w17s):
            feedback.setProgressText(f"converting {w17}")
            feedback.setProgress(int(100*(i+1)/len(w17s)))
            with open(w17[:-4]+'.csv', 'w') as out:
                convert_w17(w17, out)

        return {}

