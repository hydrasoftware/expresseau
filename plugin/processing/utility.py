import os
from pathlib import Path
from encodings.aliases import aliases
from shutil import which
from processing.gui.wrappers import WidgetWrapper
from qgis.PyQt.QtWidgets import QComboBox, QLineEdit
from ..qgis_utilities import QGisProjectManager
from qgis.core import QgsProcessingException
#from qgis.gui import QgsCrsSelectionWidget only from QGIS 3.24
from qgis.gui import QgsFileWidget
from ..database import get_projects_list
from ..project import Project
from ..database import is_expresseau_db

class ProjectCombo(QComboBox):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.addItem(QGisProjectManager.project_name())

    def mousePressEvent(self, e):
        if self.count() == 1:
            self.clear()
            self.addItems(get_projects_list())
        return QComboBox.mousePressEvent(self, e)

class ProjectWidget(WidgetWrapper):
    last_combo = None
    def createWidget(self):
        self.__combo = ProjectCombo()
        self.__combo.setEditable(True)
        self.__combo.setObjectName('projectCombo')
        ProjectWidget.last_combo = self.__combo
        return self.__combo

    def value(self):
        return self.__combo.currentText()

    def setValue(self, txt):
        self.__combo.setCurrentText(txt)


class SridWidget(WidgetWrapper):
    def createWidget(self):
        self.__widget = QLineEdit()
        c = ProjectWidget.last_combo
        if c is not None:
            self.__project_changed(c.currentText())
            c.currentTextChanged.connect(self.__project_changed)
        return self.__widget

    def value(self):
        return self.__widget.text()

    def __project_changed(self, project_name):
        self.__widget.setEnabled(True)
        if project_name and is_expresseau_db(project_name):
            project = Project(project_name)
            self.__widget.setText(f"{project.srid}")
            self.__widget.setEnabled(False)


class ScenarioWidget(WidgetWrapper):
    last_combo = None
    def createWidget(self):
        self.__combo = QComboBox()
        c = ProjectWidget.last_combo
        if c is not None:
            self.__project_changed(c.currentText())
            c.currentTextChanged.connect(self.__project_changed)
        ScenarioWidget.last_combo = self.__combo
        return self.__combo

    def value(self):
        return self.__combo.currentText()

    def setValue(self, txt):
        self.__combo.setCurrentText(txt)

    def __project_changed(self, project_name):
        self.__combo.clear()
        if project_name and is_expresseau_db(project_name):
            project = Project(project_name)
            self.__combo.addItems(project.scenarios)
            current_scenario = project.current_scenario
            if current_scenario and current_scenario in project.scenarios:
                self.__combo.setCurrentText(current_scenario)

class ModelWidget(WidgetWrapper):
    last_combo = None
    def createWidget(self):
        self.__combo = QComboBox()
        c = ProjectWidget.last_combo
        if c is not None:
            self.__project_changed(c.currentText())
            c.currentTextChanged.connect(self.__project_changed)
        ModelWidget.last_combo = self.__combo
        return self.__combo

    def value(self):
        return self.__combo.currentText()

    def setValue(self, txt):
        self.__combo.setCurrentText(txt)

    def __project_changed(self, project_name):
        self.__combo.clear()
        if project_name and is_expresseau_db(project_name):
            project = Project(project_name)
            self.__combo.addItems(project.models)
            current_model = project.current_model
            if current_model and current_model in project.models:
                self.__combo.setCurrentText(current_model)

class InpOutputWidget(WidgetWrapper):
    def createWidget(self):
        self.__fw = QgsFileWidget()
        self.__pc = ProjectWidget.last_combo
        self.__mc = ModelWidget.last_combo
        self.__sc = ScenarioWidget.last_combo
        for c in (self.__pc, self.__mc, self.__sc):
            if c is not None:
                self.__changed(c.currentText())
                c.currentTextChanged.connect(self.__changed)
        return self.__fw

    def value(self):
        return self.__fw.filePath()

    def setValue(self, txt):
        self.__fw.setFilePath(txt)

    def __changed(self, dummy):
        project_name = self.__pc.currentText() if self.__pc else None
        model_name = self.__mc.currentText() if self.__mc else None
        scn_name = self.__sc.currentText() if self.__sc else None
        if project_name and model_name and scn_name and is_expresseau_db(project_name):
            project = Project(project_name)
            self.__fw.setFilePath(project.directory + os.sep + model_name + '_' + scn_name + '.inp')

class EncodingWidget(WidgetWrapper):
    def createWidget(self):
        self.__widget = QComboBox()
        self.__widget.addItems(sorted(aliases.keys()))
        return self.__widget

    def value(self):
        return self.__widget.currentText()

    def setParameterValue(self, value):
        self.__widget.setCurrentText(value)


def find_exe(program):
    program_path = which(program)
    if program_path is None:
        kernel_exe = Path(__file__).parent / '..' / 'bin' / f"{program}.exe"
        if kernel_exe.exists():
            program_path = str(kernel_exe)
        else:
            raise QgsProcessingException(f"error: {kernel_exe} not found")

    return program_path

class Logger:
    def __init__(self, feedback):
        self.feedback = feedback

    def notice(self, msg):
        self.feedback.pushInfo(msg)

    def warning(self, msg):
        self.feedback.pushWarning(msg)

    def error(self, msg):
        self.feedback.reportError(msg)

    def cleanup(self):
        pass


if __name__ == '__main__':
    from qgis.core import QgsApplication, QgsProject
    from qgis.PyQt.QtWidgets import QDialog, QVBoxLayout, QApplication
    import os
    import sys
    from qgis.core import QgsProcessingParameterString

    QgsApplication.setPrefixPath(os.environ['QGIS_PREFIX_PATH'])
    a = QgsApplication([], True)

    if len(sys.argv) > 1:
        QgsProject.instance().read(sys.argv[1])

    d = QDialog()

    param = QgsProcessingParameterString('project', "Project")
    param.setMetadata({ 'widget_wrapper': {'class': ProjectWidget}})
    p = ProjectWidget(param, d)

    param = QgsProcessingParameterString('scenario', "Scenario")
    param.setMetadata({ 'widget_wrapper': {'class': ScenarioWidget}})
    s = ScenarioWidget(param, d)

    d.setLayout(QVBoxLayout())
    d.layout().addWidget(p.createWidget())
    d.layout().addWidget(s.createWidget())

    d.exec_()

