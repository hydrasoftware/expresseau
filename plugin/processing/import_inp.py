
from qgis.PyQt.QtCore import QCoreApplication
from qgis.core import (
    QgsProcessingAlgorithm,
    QgsProcessingParameterString,
    QgsProcessingParameterFile
)
from ..project import Project, EXPRESSEAU_DIR
from ..qgis_utilities import QGisProjectManager
from .utility import ProjectWidget, SridWidget, EncodingWidget, Logger
from ..database.import_inp import import_ as import_inp
from ..database import is_expresseau_db

class ImportInp(QgsProcessingAlgorithm):
    """
    Algorithm running xeau
    """

    PROJECT = "project"
    PROJECT_SRID = "project srid"
    PROJECT_DIRECTORY = "project directory"
    MODEL = "model"
    FILE = "file"
    ENCODING = "encoding"
    INPUT_SRID = "input srid"

    def tr(self, string):
        """
        Returns a translatable string with the self.tr() function.
        """
        return QCoreApplication.translate("Processing", string)

    def createInstance(self):
        return ImportInp()

    def name(self):
        return "import inp"

    def displayName(self):
        """
        Returns the translated algorithm name, which should be used for any
        user-visible display of the algorithm name.
        """
        return self.tr("import inp")

    def group(self):
        return self.tr("expresseau")

    def groupId(self):
        return ""

    def shortHelpString(self):
        msg = "Import EPANET model from .inp file.\n\n"
        msg += "You need to know your INP file's encoding. Open it with Notepad++ to find the encoding.\n"
        msg += "You need to know the SRID (projection) of the imported model. Look at the model's coordinates to find the SRID.\n"
        msg += "Warning : Project name must not exceed 16 characters.\n"
        msg += "Warning : Model name must not exceed 24 characters.\n"
        return self.tr(msg)

    def initAlgorithm(self, config=None):

        param = QgsProcessingParameterString(self.PROJECT, "Project")
        param.setMetadata({ 'widget_wrapper': {'class': ProjectWidget}})
        self.addParameter(param)

        param = QgsProcessingParameterString(self.PROJECT_SRID, "Project SRID")
        param.setMetadata({ 'widget_wrapper': {'class': SridWidget}})
        self.addParameter(param)

        param = QgsProcessingParameterString(self.PROJECT_DIRECTORY, "Project directory", defaultValue=EXPRESSEAU_DIR)
        self.addParameter(param)

        param = QgsProcessingParameterString(self.MODEL, "Model")
        self.addParameter(param)

        param = QgsProcessingParameterFile(self.FILE, "Epanet inp file", extension='inp')
        self.addParameter(param)

        param = QgsProcessingParameterString(self.ENCODING, "Epanet file encoding", defaultValue='utf8')
        param.setMetadata({ 'widget_wrapper': {'class': EncodingWidget}})
        self.addParameter(param)

        param = QgsProcessingParameterString(self.INPUT_SRID, "Epanet file SRID")
        self.addParameter(param)

        self.success = False

    def prepareAlgorithm(self, parameters, context, feedback):
        project_name = self.parameterAsString(parameters, self.PROJECT, context)

        if len(project_name) > 24:
            feedback.pushWarning(self.tr("Error : The project name exceeds 24 characters."))

        project_srid = int(self.parameterAsString(parameters, self.PROJECT_SRID, context))
        project_dir = self.parameterAsString(parameters, self.PROJECT_DIRECTORY, context)
        self.model = self.parameterAsString(parameters, self.MODEL, context)

        if len(self.model) > 16:
            feedback.pushWarning(self.tr("Error : The model name exceeds 16 characters."))

        self.file = self.parameterAsString(parameters, self.FILE, context)
        self.encoding = self.parameterAsString(parameters, self.ENCODING, context)
        self.input_srid = int(self.parameterAsString(parameters, self.INPUT_SRID, context))

        if not is_expresseau_db(project_name):
            Project.create_new_project(project_name, project_srid, project_dir, Logger(feedback))
            feedback.pushConsoleInfo(f"project {project_name} created")

        self.project = Project(project_name)
        self.project.add_new_model(self.model)
        feedback.pushConsoleInfo(f"model {self.model} created")

        return True



    def processAlgorithm(self, parameters, context, feedback):
        warnings, errors = import_inp(self.project, self.model, self.file, feedback, self.encoding, self.input_srid)
        if errors:
            feedback.reportError(self.model+self.tr(' imported from ')+self.file+self.tr(' with ')+str(errors)+self.tr(' errors and ')+str(warnings)+self.tr(' warnings'))
        elif warnings:
            feedback.pushWarning(self.model+self.tr(' imported from ')+self.file+self.tr(' with ')+str(warnings)+self.tr(' warnings') )
        else:
            feedback.pushWarning(self.model+self.tr(' imported from ')+self.file)

        self.success = True
        return {}

    def postProcessAlgorithm(self, context, feedback):
        if self.success and self.project.name != QGisProjectManager.project_name():
            QGisProjectManager.open_project(self.project.qgs, self.project.srid)
        return {}
