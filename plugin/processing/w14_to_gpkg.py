import os

from qgis.PyQt.QtCore import QCoreApplication
from qgis.core import (
    QgsProcessingAlgorithm,
    QgsProcessingParameterString,
)
from ..project import Project
from ..service import get_service
from wstar.w14_to_gpkg import process
from ..qgis_utilities import QGisProjectManager
from .utility import ProjectWidget, ScenarioWidget

class W14ToGpkg(QgsProcessingAlgorithm):
    """
    Algorithm running xeau
    """

    PROJECT = "project"
    SCENARIO = "scenario"

    def tr(self, string):
        return QCoreApplication.translate("Processing", string)

    def createInstance(self):
        return W14ToGpkg()

    def name(self):
        return "w14_to_gpkg"

    def displayName(self):
        return self.tr("w14_to_gpkg")

    def group(self):
        return self.tr("expresseau")

    def groupId(self):
        return ""

    def shortHelpString(self):
        return self.tr("Post-process expresseau scenario")

    def initAlgorithm(self, config=None):
        """
        Here we define the inputs and output of the algorithm, along
        with some other properties.
        """

        param = QgsProcessingParameterString(self.PROJECT, "Project")
        param.setMetadata({ 'widget_wrapper': {'class': ProjectWidget}})
        self.addParameter(param)

        param = QgsProcessingParameterString(self.SCENARIO, "Scenario")
        param.setMetadata({ 'widget_wrapper': {'class': ScenarioWidget}})
        self.addParameter(param)


    def prepareAlgorithm(self, parameters, context, feedback):
        self.success = False
        self.project = Project(self.parameterAsString(parameters, self.PROJECT, context))
        self.scenario = self.parameterAsString(parameters, self.SCENARIO, context)

        self.tini, self.duration, self.comput_mode = self.project.fetchone("""
            select (-extract(epoch from tini)/3600)::float, (extract(epoch from duration)/3600)::float, comput_mode
            from api.scenario where name=%s
            """, (self.scenario,))

        self.gpkg = os.path.join(self.project.directory, self.scenario.upper(), 'hydraulique', f'{self.scenario.upper()}.gpkg')

        if self.project.name == QGisProjectManager.project_name():
            QGisProjectManager.exit_edition()
            QGisProjectManager.remove_results(self.scenario)

        feedback.pushCommandInfo(f'rm {self.gpkg}')
        if os.path.exists(self.gpkg):
            os.remove(self.gpkg)
            feedback.pushConsoleInfo("old gpkg removed")
        else:
            feedback.pushConsoleInfo("no gpkg to remove")

        return True

    def processAlgorithm(self, parameters, context, feedback):
        feedback.pushInfo('post-processing')
        feedback.setProgress(10)
        process(get_service(), self.project, self.scenario)
        feedback.setProgress(100)

        return {}

    def postProcessAlgorithm(self, context, feedback):
        if self.project.name == QGisProjectManager.project_name():
            if os.path.exists(self.gpkg):
                QGisProjectManager.load_results(self.scenario, self.gpkg, self.comput_mode)
                feedback.pushConsoleInfo("gpkg charged")
            else:
                feedback.pushConsoleInfo("no gpkg to charge")

        return {}
