from pathlib import Path
import subprocess
import os
import shutil
import asyncio
from math import ceil
from osgeo import ogr, osr
import numpy
from scipy.optimize import minimize
from math import sqrt, log10

from qgis.PyQt.QtCore import QCoreApplication
from qgis.core import (
    QgsProcessingAlgorithm,
    QgsProcessingParameterBoolean,
    QgsProcessingParameterString,
    QgsProcessingParameterNumber,
)
from qgis import processing
from ..project import Project
from ..service import get_service
from ..utility.license import license_file
from ..database.export_calcul import ExportCalcul
from wstar.w14_to_gpkg import process
import wstar
from ..qgis_utilities import QGisProjectManager
from .utility import ProjectWidget, ScenarioWidget, find_exe

############################################################################################
def Colebrook(DN, eps, Re) :
    '''Calcul du coef de perte de charge linéaire dans les canalisations
    utilisée ici pour le calcul des pertes de charge dans les branchements des hydrants'''

    if Re==0:
        return 0
    elif Re < 0:
        Re = abs(Re)

    def residu_colebrook(x):
        lambda_ = x[0] if isinstance(x, list) else x
        return (1/sqrt(lambda_) + 2*log10((eps/1000)/(3.7*DN) + (2.51/Re)/sqrt(lambda_)))**2

    def sol_lambda_colebrook():
        return minimize(residu_colebrook, x0=[0.01], bounds=[(1e-9,10)])

    return sol_lambda_colebrook().x[0]

class Logger:
    def __init__(self, feedback):
        self.feedback = feedback

    def log(self, msg):
        self.feedback.pushInfo(msg)

class Progress:
    total = 0
    current = 0

############################################################################################

async def compute_all_ctl(feedback, dst_dir, scenario, ctl, project, gpkg_filename, progress, comput_mode):
    workdir = dst_dir / 'hydraulique'
    cmd = dst_dir / 'travail' / (scenario.upper()+'.cmd')
    gpkg_filename = str(workdir / (comput_mode.lower().replace(' ', '_') + '.gpkg'))

    with open(cmd) as f:
        saved_cmd = f.readlines()
        regul_start = None
        regul_end = None

        for i, ll in enumerate(saved_cmd):
            if ll.lower().startswith("*f_regul"):
                regul_start = i
            if regul_start is not None and ll.strip()=="":
                regul_end = i
                break

    drv = ogr.GetDriverByName("GPKG")
    srs = osr.SpatialReference()
    srs.ImportFromEPSG(project.srid)
    ds = drv.CreateDataSource(gpkg_filename) #  file creation

    if comput_mode == 'Exterior fire defense nominal flow':
        min_pressure = ds.CreateLayer('fire_hydrant_min_pressure', srs, ogr.wkbPoint)
        f = ogr.FieldDefn("name", ogr.OFTString)
        f.SetWidth(24)
        min_pressure.CreateField(f)
        min_pressure.CreateField(ogr.FieldDefn("p_min_mce", ogr.OFTReal))

    elif comput_mode == 'Exterior fire defense flow at 1bar':
        flow_1bar = ds.CreateLayer('fire_hydrant_flow_at_1bar', srs, ogr.wkbPoint)
        f = ogr.FieldDefn("name", ogr.OFTString)
        f.SetWidth(24)
        flow_1bar.CreateField(f)
        flow_1bar.CreateField(ogr.FieldDefn("flow_m3h", ogr.OFTReal))

    elif comput_mode == 'Break criticality':
        wd_loss_broken_pipe = ds.CreateLayer('wd_loss_broken_pipe', srs, ogr.wkbLineString)
        wd_loss_broken_pipe.CreateField(ogr.FieldDefn("fid", ogr.OFTInteger))
        f = ogr.FieldDefn("name", ogr.OFTString)
        f.SetWidth(24)
        wd_loss_broken_pipe.CreateField(f)
        wd_loss_broken_pipe.CreateField(ogr.FieldDefn("volume_cons", ogr.OFTReal))
        wd_loss_broken_pipe.CreateField(ogr.FieldDefn("volume_sim", ogr.OFTReal))
        wd_loss_broken_pipe.CreateField(ogr.FieldDefn("missing_volume", ogr.OFTReal))
        wd_loss_broken_pipe.CreateField(ogr.FieldDefn("pourcentage_missing", ogr.OFTReal))

    for j, c in enumerate(ctl):
        with open(cmd, 'w') as f:
            if regul_start is None:
                f.write("".join(saved_cmd))
                f.write('\n*f_regul\n'+str(c)+'\n\n')
            else:
                f.write("".join(saved_cmd[:regul_end]))
                f.write(str(c)+'\n')
                f.write("".join(saved_cmd[regul_end:]))

        feedback.pushInfo(f'start {c}')

        if os.name == 'posix':
            proc = subprocess.Popen(find_exe('xeau'),
                        cwd=workdir,
                        stdin=subprocess.DEVNULL,
                        stdout=subprocess.PIPE,
                        stderr=subprocess.STDOUT,
                        env= {**os.environ, **{'EXPRESSEAU_LICENSE': license_file}},
                        )
        else:
            CREATE_NO_WINDOW = 0x08000000
            proc = subprocess.Popen(find_exe('xeau'),
                        cwd=workdir,
                        creationflags=CREATE_NO_WINDOW,
                        stdin=subprocess.DEVNULL,
                        stdout=subprocess.PIPE,
                        stderr=subprocess.STDOUT,
                        env= {**os.environ, **{'EXPRESSEAU_LICENSE': license_file}},
                        )

        log = ''

        for line in proc.stdout:
            ll = line.decode("utf-8", "replace").strip()
            log += ll + '\n'
            if ll.find('attention') != -1:
                feedback.pushWarning(ll)
            elif feedback.isCanceled():
                feedback.reportError("CANCELLED")
                proc.terminate()
                proc.stdout.close()
                proc.wait()
                return False
            else:
                err = ll.find('forrtl: severe') != -1
                if not err:
                    feedback.pushConsoleInfo(ll)
                else:
                    feedback.reportError(ll)

        proc.stdout.close()
        proc.wait()

        with open(workdir / "xeau.err") as e, open(workdir / "xeau.log") as ll:
            if int(e.readlines()[0]) != 0:
                #feedback.pushConsoleInfo(err.decode("utf-8", "replace"))
                feedback.pushInfo(log)
                feedback.reportError(f"Contenu de {ll.name}:")
                for line in ll.readlines():
                    feedback.pushConsoleInfo(line)
                feedback.reportError(f"ERROR : {c}")
                return

        if comput_mode == 'Exterior fire defense nominal flow':
            # create table with minimum pressure
            hydrant = c.parts[-1][:-4] #nom du ctl sans chemin ni l'extension, donc nom de l'hydrant
            name, wkb, node_name, model = project.fetchone("""
                select h.name, st_asbinary(h.geom), upper(n.name), n.model
                from api.fire_hydrant h
                join api.node n on n.id=h._node
                where h.name = %s""", (hydrant,))

            w14_filename = workdir / f'{scenario.upper()}_{model.upper()}.w14'

            with open(w14_filename, 'rb') as f:
                f.seek(0)
                nb_step, nb_elem, nb_link = numpy.frombuffer(f.read(32), dtype=numpy.int32)[:3]
                names = []
                node_idx = []
                n = nb_elem + nb_link
                for i in range(n):
                    names.append(b''.join(numpy.frombuffer(f.read(24), dtype='S1')
                        ).decode('utf-8').strip().upper())
                    it = numpy.frombuffer(f.read(4), dtype=numpy.int32)
                    if it == 1:
                        node_idx.append(i)
                    f.seek(4, 1)
                name_idx = {n: i for i, n in enumerate(names)}
                stride = 8*(n+1)

            data_w14 = numpy.fromfile(str(w14_filename), dtype=numpy.float32)[stride:]
            col_p = data_w14[(name_idx[node_name]+1)*8 + 1 :: stride]
            col_q = data_w14[(name_idx[node_name]+1)*8 + 2 :: stride]

            ### correction du Pmin avec la perte de charge dans le branchement (marjo 26/07/23)
            nu = 1.3E-6 # eau à 10°C
            eps = 0.1 # arbitraire !

            hyd_name, DN, len_branch = project.fetchone("""
                select P_hyd.name, P_hyd.diameter, ST_Length(ST_Makeline(P_hyd.P1, P_hyd.P2)) as len_branch from
                (select name, diameter, ST_PointN(hydrant.geom,1) as P1, ST_PointN(hydrant.geom,2) as P2 from
                (select name, diameter, ST_GeomFromText(_affectation_geom) as geom from api.fire_hydrant) as hydrant) as P_hyd
                where P_hyd.name=%s""", (hydrant,))

            tab_v = [(col_q[i]/3600) / (3.14*DN*DN/4) for i in range(nb_step)] #pi=3.14 like in kernel
            tab_Re = [abs(tab_v[i])*DN/nu for i in range(nb_step)]
            tab_Lamb_C = [Colebrook(DN, eps, tab_Re[i]) for i in range(nb_step)]
            tab_deltaH = [(tab_Lamb_C[i]*len_branch/DN)*((tab_v[i]**2)/(2*9.81)) for i in range(nb_step)]
            Pmin = round(float(numpy.min([col_p[i] - tab_deltaH[i] for i in range(nb_step)])), 3)

            feature = ogr.Feature(min_pressure.GetLayerDefn())
            feature.SetField("name", hydrant)
            #feature.SetField("p_min_mce", round(float(numpy.min(data_w14[col_h::stride])), 3))
            feature.SetField("p_min_mce", Pmin) # modif marjo 26/07/23
            geom = ogr.CreateGeometryFromWkb(wkb)
            feature.SetGeometry(geom)
            min_pressure.CreateFeature(feature)
            feature.Destroy()

        if comput_mode == 'Exterior fire defense flow at 1bar':
            hydrant = c.parts[-1][:-4] #nom du ctl sans chemin ni l'extension, donc nom de l'hydrant
            name, wkb, node_name, model = project.fetchone("""
                select h.name, st_asbinary(h.geom), upper(n.name), n.model
                from api.fire_hydrant h
                join api.node n on n.id=h._node
                where h.name = %s""", (hydrant,))

            w14_filename = workdir / f'{scenario.upper()}_{model.upper()}.w14'
            with open(w14_filename, 'rb') as f:
                f.seek(0)
                nb_step, nb_elem, nb_link = numpy.frombuffer(f.read(32), dtype=numpy.int32)[:3]
                names = []
                node_idx = []
                n = nb_elem + nb_link
                for i in range(n):
                    names.append(b''.join(numpy.frombuffer(f.read(24), dtype='S1')
                        ).decode('utf-8').strip().upper())
                    it = numpy.frombuffer(f.read(4), dtype=numpy.int32)
                    if it == 1:
                        node_idx.append(i)
                    f.seek(4, 1)
                name_idx = {n: i for i, n in enumerate(names)}
                stride = 8*(n+1)

            data_w14 = numpy.fromfile(str(w14_filename), dtype=numpy.float32)[stride:]

            col_p = data_w14[(name_idx[node_name]+1)*8 + 1 :: stride]
            col_q = data_w14[(name_idx[node_name]+1)*8 + 2 :: stride]

            ### correction du P avec la perte de charge dans le branchement (marjo 26/07/23)
            nu = 1.3E-6 # eau à 10°C
            eps = 0.1 # arbitraire !

            hyd_name, DN, len_branch = project.fetchone("""
                select P_hyd.name, P_hyd.diameter, ST_Length(ST_Makeline(P_hyd.P1, P_hyd.P2)) as len_branch from
                (select name, diameter, ST_PointN(hydrant.geom,1) as P1, ST_PointN(hydrant.geom,2) as P2 from
                (select name, diameter, ST_GeomFromText(_affectation_geom) as geom from api.fire_hydrant) as hydrant) as P_hyd
                where P_hyd.name=%s""", (hydrant,))

            tab_v = [(col_q[i]/3600) / (3.14*DN*DN/4) for i in range(nb_step)] #pi=3.14 like in kernel
            tab_Re = [tab_v[i]*DN/nu for i in range(nb_step)]
            tab_Lamb_C = [Colebrook(DN, eps, tab_Re[i]) for i in range(nb_step)]
            tab_deltaH = [(tab_Lamb_C[i]*len_branch/DN)*((tab_v[i]**2)/(2*9.81)) for i in range(nb_step)]
            tab_p_corr = [col_p[i] - tab_deltaH[i] for i in range(nb_step)]

            #q_1_bar = numpy.interp(10, col_p[::-1], col_q[::-1])
            q_1_bar = numpy.interp(10, tab_p_corr[::-1], col_q[::-1]) # modif marjo 26/07/23

            feature = ogr.Feature(flow_1bar.GetLayerDefn())
            feature.SetField("name", hydrant)
            feature.SetField("flow_m3h", round(float(q_1_bar), 0))
            geom = ogr.CreateGeometryFromWkb(wkb)
            feature.SetGeometry(geom)
            flow_1bar.CreateFeature(feature)
            feature.Destroy()

        elif comput_mode == 'Break criticality':

            # create table with missing volume
            pipe_name = c.parts[-1][:-4] #nom du ctl sans chemin ni .ctl donc nom de la cana
            fid, name, wkb = project.fetchone("""
                select p.id, p.name, st_asbinary(p.geom)
                from api.pipe_link p
                where p.name = %s""", (pipe_name,))

            ###Calcul volumes (de consigne, calculé, perdu) yc correction du volume perdu avec les conso connectées à la cana fermée
            ###(modulo le ratio effectivement consommé d'après la simu)

            vcons = 0
            v = 0
            vloss = 0
            correction_vol = 0
            nodes = []

            # 1) récupérer dans la base les consos connectées sur le pipe et le ou les noeuds sur lesquels elles tirent
            for sql_pipe_name, sql_volume_on_pipe, sql_node_name, sql_model, sql_volume_on_node in project.fetchall('''
            with
            volume_as_pipe as (
                select pipe.name, pipe.id, pipe.volume from
                    (select p.name, p.id, sum(w.volume) as volume
                    from api.pipe_link p
                    join api.water_delivery_point w ON p.id = w.pipe_link
                    group by p.name, p.id) as pipe
                ),
            pipes_and_nodes as (
                select p.name as pipe_name, p.id as id, n1.id as id_am, n1.name as name_am, n1.model as model_am, n2.id as id_av, n2.name as name_av, n2.model as model_av
                    from api.pipe_link as p
                    join api.user_node as n1 on p.up= n1.id
                    join api.user_node as n2 on p.down = n2.id
                    order by p.name
                    ),
            volume_as_node as (
                select node.name, node.pipe, node.volume from
                    (select n.name, w.pipe_link as pipe, sum(w.volume) as volume
                    from api.user_node as n
                    join api.water_delivery_point w ON n.id = w._node
                    group by n.name, w.pipe_link) as node
                    )
            select * from (
                select vap.name as pipe_name, vap.volume as volume_pipe, pan.name_am as node_name, pan.model_am as node_model, van.volume
                    from volume_as_pipe as vap,
                    pipes_and_nodes as pan,
                    volume_as_node as van
                    where vap.id = pan.id
                    and pan.name_am = van.name
                    and vap.id = van.pipe
                union
                    select vap.name as pipe_name, vap.volume as volume_pipe, pan.name_av as node_name, pan.model_av as node_model, van.volume
                    from volume_as_pipe as vap,
                    pipes_and_nodes as pan,
                    volume_as_node as van
                    where vap.id = pan.id
                    and pan.name_av = van.name
                    and vap.id = van.pipe) as resultat
            where resultat.pipe_name = %s''', (pipe_name,)):

                nodes.append([sql_pipe_name, sql_volume_on_pipe, sql_node_name, sql_model, sql_volume_on_node])
            #print("nodes : ", nodes)

            for w14_filename in workdir.glob('*.w14'):
                # 2) inchangé : calcul de v, vcons et vloss d'après la simulation
                with open(w14_filename, 'rb') as f:
                    f.seek(0)
                    nb_step, nb_elem, nb_link = numpy.frombuffer(f.read(32), dtype=numpy.int32)[:3]
                    names = []
                    node_idx = []
                    n = nb_elem + nb_link
                    for i in range(n):
                        names.append(b''.join(numpy.frombuffer(f.read(24), dtype='S1')
                            ).decode('utf-8').strip().upper())
                        it = numpy.frombuffer(f.read(4), dtype=numpy.int32)
                        if it == 1:
                            node_idx.append(i)
                        f.seek(4, 1)
                    name_idx = {n: i for i, n in enumerate(names)}
                    stride = 8*(n+1)

                node_idx = numpy.array(node_idx)

                data_w14 = numpy.fromfile(str(w14_filename), dtype=numpy.float32)[stride:]
                t = data_w14[0::stride]
                dt = t[1:] - t[:-1]
                for idx in node_idx:
                    q = data_w14[(idx+1)*8 + 2 :: stride]
                    qcons = data_w14[(idx+1)*8 + 3 :: stride]
                    q = 0.5*(q[:-1] + q[1:])
                    qcons = 0.5*(qcons[:-1] + qcons[1:])
                    vcons += numpy.sum(qcons*dt)
                    v += numpy.sum(q*dt)
                    vloss += numpy.sum((qcons - q)*dt)

                #3) Récupérer les ratios de conso sur les nœuds amont et aval de la cana et les appliquer aux consos connectées à la cana cassée et tirant sur ces noeuds
                for i, n in enumerate(nodes):
                    node_name = n[2]
                    #node_model = n[3]
                    if node_name in name_idx:
                        num_node = name_idx[node_name]
                        q_nod =  data_w14[(num_node+1)*8 + 2 :: stride]
                        q_nod = 0.5*(q_nod[:-1] + q_nod[1:])
                        v_nod = numpy.sum(q_nod*dt)
                        qcons_nod = data_w14[(num_node+1)*8 + 3 :: stride]
                        qcons_nod = 0.5*(qcons_nod[:-1] + qcons_nod[1:])
                        vcons_nod = numpy.sum(qcons_nod*dt)
                        ratio = v_nod / vcons_nod if vcons_nod!=0 else 0
                        #print("ratio =", v_nod, "/", vcons_nod, "=", ratio)
                        correction_vol += ratio * n[4]
                    else:
                        pass

            #4) Apporter la correction aux volumes calculés précédemment

            # prendre le prorata du temps de simu sur le volume de correction
            simulation_duration, = project.fetchone(f"select duration from api.scenario where name = '{scenario}'")
            simulation_duration = simulation_duration.total_seconds() / 3600
            correction_vol = simulation_duration * correction_vol /24

            vcons = vcons
            v     = v - correction_vol
            vloss = vloss + correction_vol


            feature = ogr.Feature(wd_loss_broken_pipe.GetLayerDefn())
            feature.SetField("fid", fid)
            feature.SetField("name", pipe_name)
            pc_loss = 100*(vloss/vcons)
            feature.SetField("volume_cons", round(vcons, 0))
            feature.SetField("volume_sim", round(v, 0))
            feature.SetField("missing_volume", round(vloss, 0))
            feature.SetField("pourcentage_missing", round(pc_loss, 0))
            geom = ogr.CreateGeometryFromWkb(wkb)
            feature.SetGeometry(geom)
            wd_loss_broken_pipe.CreateFeature(feature)
            feature.Destroy()

        progress.current += 1
        feedback.setProgress(int(100*progress.current/progress.total))

    ds.Destroy()

# @todo move that function in prostprocess ?
def aggregate_results(srcs, dst, project, comput_mode):
    drv = ogr.GetDriverByName("GPKG")

    if not os.path.exists(dst):
        ds = drv.CreateDataSource(dst) #  file creation
    else:
        ds = ogr.Open(str(dst), 1)

    srs = osr.SpatialReference()
    srs.ImportFromEPSG(project.srid)

    if comput_mode == 'Exterior fire defense nominal flow':
        min_pressure = ds.CreateLayer('fire_hydrant_min_pressure', srs, ogr.wkbPoint)
        f = ogr.FieldDefn("name", ogr.OFTString)
        f.SetWidth(24)
        min_pressure.CreateField(f)
        min_pressure.CreateField(ogr.FieldDefn("p_min_mce", ogr.OFTReal))

        for src in srcs:
            sds = ogr.Open(str(src))
            src_layer = sds.GetLayer('fire_hydrant_min_pressure')
            for s in src_layer:
                feature = ogr.Feature(min_pressure.GetLayerDefn())
                feature.SetField('name', s['name'])
                feature.SetField('p_min_mce', s['p_min_mce'])
                feature.SetGeometry(s.GetGeometryRef())
                min_pressure.CreateFeature(feature)
                feature.Destroy()
            sds.Destroy()

    elif comput_mode == 'Exterior fire defense flow at 1bar':
        flow_1bar = ds.CreateLayer('fire_hydrant_flow_at_1bar', srs, ogr.wkbPoint)
        f = ogr.FieldDefn("name", ogr.OFTString)
        f.SetWidth(24)
        flow_1bar.CreateField(f)
        flow_1bar.CreateField(ogr.FieldDefn("flow_m3h", ogr.OFTReal))

        for src in srcs:
            sds = ogr.Open(str(src))
            src_layer = sds.GetLayer('fire_hydrant_flow_at_1bar')
            for s in src_layer:
                feature = ogr.Feature(flow_1bar.GetLayerDefn())
                feature.SetField('name', s['name'])
                feature.SetField('flow_m3h', s['flow_m3h'])
                feature.SetGeometry(s.GetGeometryRef())
                flow_1bar.CreateFeature(feature)
                feature.Destroy()
            sds.Destroy()

    elif comput_mode == 'Break criticality':
        wd_loss_broken_pipe = ds.CreateLayer('wd_loss_broken_pipe', srs, ogr.wkbLineString)
        wd_loss_broken_pipe.CreateField(ogr.FieldDefn("fid", ogr.OFTInteger))
        f = ogr.FieldDefn("name", ogr.OFTString)
        f.SetWidth(24)
        wd_loss_broken_pipe.CreateField(f)
        wd_loss_broken_pipe.CreateField(ogr.FieldDefn("volume_cons", ogr.OFTReal))
        wd_loss_broken_pipe.CreateField(ogr.FieldDefn("volume_sim", ogr.OFTReal))
        wd_loss_broken_pipe.CreateField(ogr.FieldDefn("missing_volume", ogr.OFTReal))
        wd_loss_broken_pipe.CreateField(ogr.FieldDefn("pourcentage_missing", ogr.OFTReal))

        for src in srcs:
            sds = ogr.Open(str(src))
            src_layer = sds.GetLayer('wd_loss_broken_pipe')
            for s in src_layer:
                feature = ogr.Feature(wd_loss_broken_pipe.GetLayerDefn())
                feature.SetField('fid', s.GetFID())
                feature.SetField('name', s['name'])
                feature.SetField('volume_cons', s['volume_cons'])
                feature.SetField('volume_sim', s['volume_sim'])
                feature.SetField('missing_volume', s['missing_volume'])
                feature.SetField('pourcentage_missing', s['pourcentage_missing'])
                feature.SetGeometry(s.GetGeometryRef())
                wd_loss_broken_pipe.CreateFeature(feature)
                feature.Destroy()
            sds.Destroy()

    # layer_style
    layer_styles = ds.CreateLayer('layer_styles', None, ogr.wkbNone, options=['FID=name'])
    layer_styles.CreateField(ogr.FieldDefn("f_table_catalog", ogr.OFTString))
    layer_styles.CreateField(ogr.FieldDefn("f_table_schema", ogr.OFTString))
    layer_styles.CreateField(ogr.FieldDefn("f_table_name", ogr.OFTString))
    layer_styles.CreateField(ogr.FieldDefn("f_geometry_column", ogr.OFTString))
    layer_styles.CreateField(ogr.FieldDefn("styleName", ogr.OFTString))
    layer_styles.CreateField(ogr.FieldDefn("styleQML", ogr.OFTString))
    layer_styles.CreateField(ogr.FieldDefn("styleSLD", ogr.OFTString))
    layer_styles.CreateField(ogr.FieldDefn("useAsDefault", ogr.OFTInteger))
    layer_styles.CreateField(ogr.FieldDefn("description", ogr.OFTString))
    layer_styles.CreateField(ogr.FieldDefn("owner", ogr.OFTString))
    layer_styles.CreateField(ogr.FieldDefn("ui", ogr.OFTString))
    layer_styles.CreateField(ogr.FieldDefn("update_time", ogr.OFTDateTime))

    if comput_mode == 'Exterior fire defense nominal flow':
        qml_file = 'p_min_fire_hydrant.qml'
    elif comput_mode == 'Exterior fire defense flow at 1bar':
        qml_file = 'flow_at_1bar_fire_hydrant.qml'
    elif comput_mode == 'Break criticality':
        qml_file = 'wd_loss_broken_pipe.qml'

    with open(os.path.join(os.path.dirname(wstar.__file__), 'ressources', 'qml', qml_file)) as q: #TODO déplacer aussi dans wstar
        qml = q.read()

    # modif marjo 27/06/23
    if comput_mode == 'Exterior fire defense nominal flow':
        ds.ExecuteSQL(f"""
            insert into layer_styles(f_table_schema, f_table_name, f_geometry_column, styleName, useAsDefault, styleQML)
            values ('', "fire_hydrant_min_pressure", 'geom', 'p_min_fire_hydrant', 1, '{qml.replace("'","''")}')
            """)
    if comput_mode == 'Exterior fire defense flow at 1bar':
        ds.ExecuteSQL(f"""
            insert into layer_styles(f_table_schema, f_table_name, f_geometry_column, styleName, useAsDefault, styleQML)
            values ('', "fire_hydrant_flow_at_1bar", 'geom', 'flow_at_1bar_fire_hydrant', 1, '{qml.replace("'","''")}')
            """)
    elif comput_mode == 'Break criticality':
        ds.ExecuteSQL(f"""
            insert into layer_styles(f_table_schema, f_table_name, f_geometry_column, styleName, useAsDefault, styleQML)
            values ('', "wd_loss_broken_pipe", 'geom', 'wd_loss_broken_pipe', 1, '{qml.replace("'","''")}')
            """)

    ds.ExecuteSQL("vacuum")
    ds.Destroy()

############################################################################################

class Simulate(QgsProcessingAlgorithm):
    """
    Algorithm running xeau
    """

    PROJECT = "project"
    SCENARIO = "scenario"
    POSTPROCESS = "postprocess"
    AUTOGRAPH = "autograph"
    NB_PROCESS = "nb_process"

    def tr(self, string):
        """
        Returns a translatable string with the self.tr() function.
        """
        return QCoreApplication.translate("Processing", string)

    def createInstance(self):
        return Simulate()

    def name(self):
        return "simulate"

    def displayName(self):
        """
        Returns the translated algorithm name, which should be used for any
        user-visible display of the algorithm name.
        """
        return self.tr("simulate")

    def group(self):
        return self.tr("expresseau")

    def groupId(self):
        return ""

    def shortHelpString(self):
        return self.tr("Run expresseau simulation.")

    def initAlgorithm(self, config=None):
        """
        Here we define the inputs and output of the algorithm, along
        with some other properties.
        """

        param = QgsProcessingParameterString(self.PROJECT, self.tr("Project"))
        param.setMetadata({ 'widget_wrapper': {'class': ProjectWidget}})
        self.addParameter(param)

        param = QgsProcessingParameterString(self.SCENARIO, "Scenario")
        param.setMetadata({ 'widget_wrapper': {'class': ScenarioWidget}})
        self.addParameter(param)

        param = QgsProcessingParameterBoolean(self.AUTOGRAPH, self.tr('Autograph'), defaultValue=True)
        self.addParameter(param)

        param = QgsProcessingParameterBoolean(self.POSTPROCESS, self.tr('Postprocess'), defaultValue=False)
        self.addParameter(param)

        param = QgsProcessingParameterNumber(self.NB_PROCESS, self.tr('Number of process (Exterior fire defense and Break criticality only)'), defaultValue=2, minValue=1)
        self.addParameter(param)

    def prepareAlgorithm(self, parameters, context, feedback):
        self.success = False
        self.project = Project(self.parameterAsString(parameters, self.PROJECT, context))
        self.scenario = self.parameterAsString(parameters, self.SCENARIO, context)
        self.postpro = self.parameterAsBool(parameters, self.POSTPROCESS, context)
        self.autograph = self.parameterAsBool(parameters, self.AUTOGRAPH, context)
        self.nb_process = self.parameterAsInt(parameters, self.NB_PROCESS, context)

        self.tini, self.duration, self.comput_mode = self.project.fetchone("""
            select (-extract(epoch from tini)/3600)::float, (extract(epoch from duration)/3600)::float, comput_mode
            from api.scenario where name=%s
            """, (self.scenario,))

        self.gpkg = os.path.join(self.project.directory, self.scenario.upper(), 'hydraulique', f'{self.scenario.upper()}.gpkg')
        self.fire_defense_gpkg = None

        if self.project.name == QGisProjectManager.project_name():
            QGisProjectManager.exit_edition()
            QGisProjectManager.remove_results(self.scenario)

        feedback.pushCommandInfo(f'rm {self.gpkg}')
        if os.path.exists(self.gpkg):
            os.remove(self.gpkg)
            feedback.pushConsoleInfo("old gpkg removed")
        else:
            feedback.pushConsoleInfo("no gpkg to remove")

        return True

    def processAlgorithm(self, parameters, context, feedback):
        feedback.pushInfo('processAlgorithm')
        feedback.setProgress(0)
        logger = Logger(feedback)
        exporter = ExportCalcul(self.project)
        exporter.log.connect(logger.log)
        exporter.export(self.scenario)

        if feedback.isCanceled():
            feedback.reportError('CANCELLED')
            return {}

        workdir = Path(self.project.directory) / self.scenario.upper() / "travail"
        success = self._run_external('xeaugen', feedback, workdir=workdir)
        if not success:
            self.success = False
            return

        gpkg_filenames = []

        if self.comput_mode in ('Exterior fire defense nominal flow', 'Exterior fire defense flow at 1bar', 'Break criticality'):
            scn_dir = Path(self.project.directory) / self.scenario.upper()
            ctl = list(scn_dir.glob('*.ctl'))
            chunksize = ceil(len(ctl)/self.nb_process)
            proc = []

            progress = Progress
            progress.total = len(ctl)

            for i, ll in [(i, ctl[i*chunksize: (i+1)*chunksize]) for i in range(self.nb_process)]:
                dst_dir = Path(str(scn_dir) + f'_PROCESS_{i}')
                if os.path.exists(dst_dir):
                    shutil.rmtree(dst_dir)
                shutil.copytree(scn_dir / 'travail', dst_dir / 'travail')
                shutil.copytree(scn_dir / 'hydraulique', dst_dir / 'hydraulique')
                shutil.copy(scn_dir / 'scenario.nom', dst_dir / 'scenario.nom')
                for f in ll:
                    shutil.copy(f, dst_dir)

                gpkg_filename = str(dst_dir / 'hydraulique' / (self.comput_mode.lower().replace(' ', '_') + '.gpkg'))
                gpkg_filenames.append(gpkg_filename)
                proc.append(compute_all_ctl(feedback, dst_dir, self.scenario, ll, self.project, gpkg_filename, progress, self.comput_mode))

            async def main(processes):
                await asyncio.gather(*processes)
            asyncio.run(main(proc))
            self.success = True

        else:
            workdir = Path(self.project.directory) / self.scenario.upper() / "hydraulique"
            self.success = self._run_external('xeau', feedback, workdir=workdir)

        if self.success:
            if feedback.isCanceled():
                feedback.reportError('CANCELLED')
                return {}

            if self.comput_mode in ('Exterior fire defense nominal flow', 'Exterior fire defense flow at 1bar', 'Break criticality'):
                feedback.pushInfo('Aggregate Results')
                aggregate_results(gpkg_filenames, self.gpkg, self.project, self.comput_mode)
            elif self.postpro:
                process(get_service(), self.project.name, self.scenario, feedback=feedback)

        feedback.setProgress(100)

        return {}

    def postProcessAlgorithm(self, context, feedback):
        if self.success :
            if self.postpro and self.project.name == QGisProjectManager.project_name():
                if os.path.exists(self.gpkg):
                    QGisProjectManager.load_results(self.scenario, self.gpkg, self.comput_mode)
                    feedback.pushConsoleInfo("gpkg charged")
                else:
                    feedback.pushConsoleInfo("no gpkg to charge")
            if self.comput_mode not in ('Exterior fire defense nominal flow', 'Exterior fire defense flow at 1bar', 'Break criticality'):
                if self.autograph :
                    processing.run(
                        "expresseau:autograph",
                        {"project":self.project.name, "scenario":self.scenario}, feedback=feedback)

        return {}

    def _run_external(self, command, feedback, workdir=None):
        feedback.pushCommandInfo(command)
        success = False
        retry_count = 0
        while not success:
            loglines = []
            loglines.append(self.name() + " execution console output")
            try:
                with subprocess.Popen(
                    find_exe(command),
                    shell=True,
                    stdout=subprocess.PIPE,
                    stdin=subprocess.DEVNULL,
                    stderr=subprocess.STDOUT,
                    env= {**os.environ, **{'EXPRESSEAU_LICENSE': license_file}},
                    errors='replace',
                    cwd=workdir,
                ) as proc:
                    err = False
                    for line in proc.stdout:
                        ll = line.strip()
                        if ll.startswith('th:'):
                            splt = ll.split()
                            t = float(splt[1])
                            feedback.setProgress(int(100 * (t - self.tini)/(self.duration - self.tini)))

                        if ll.find('attention') != -1:
                            feedback.pushWarning(ll)
                        elif feedback.isCanceled():
                            feedback.reportError("CANCELLED")
                            proc.terminate()
                            proc.stdout.close()
                            proc.wait()
                            return False
                        else:
                            err |= ll.find('forrtl: severe') != -1
                            if not err:
                                feedback.pushConsoleInfo(ll)
                            else:
                                feedback.reportError(ll)

                        loglines.append(line.strip())
                    success = True
            except IOError as e:
                if retry_count < 5:
                    retry_count += 1
                else:
                    raise IOError(
                        str(e)
                        + "\nTried 5 times without success. Last iteration stopped after reading {} line(s).\nLast line(s):\n{}".format(
                            len(loglines), "\n".join(loglines[-10:])
                        )
                    )
        with open(os.path.join(workdir, f"{command}.err")) as e, open(os.path.join(workdir, f"{command}.log")) as ll:
            if int(e.readlines()[0]) != 0:
                feedback.reportError(f"Contenu de {ll.name}:")
                for line in ll.readlines():
                    feedback.pushConsoleInfo(line)
                feedback.reportError("ERROR")
                return False
            else:
                feedback.pushInfo("OK")
                return True
