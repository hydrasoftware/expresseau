# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of XPRESSO, a QGIS plugin for hydraulics                             ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

import requests
import subprocess
import os
import glob
import re
from subprocess import run
from shutil import which
import tempfile

license_file = os.path.join(os.path.expanduser('~'), '.expresseau', 'expresseau_licence.key') \
    if 'EXPRESSEAU_LICENSE' not in os.environ else os.environ['EXPRESSEAU_LICENSE']
build_file = os.path.join(os.path.dirname(__file__), '..', 'build.txt')

class LicenceActivationError(BaseException):
    pass

def check_license():
    return os.path.isfile(license_file)

def get_serial():
    if os.name == 'nt':
        out = subprocess.check_output(["wmic", "bios", "get", "serialnumber"], shell=True).decode('cp437')
        mac = out.split('\n')[1].strip()
    else:
        out = subprocess.check_output(["cat"]+glob.glob("/sys/class/net/*/address"))
        m = re.search('([a-f0-9]{2}:){5}[a-f0-9]{2}', out.decode('utf8'))
        mac = m.group(0)
    return mac

def get_build():
    if os.path.isfile(build_file):
        with open(build_file, 'r') as bf:
            build_info = bf.read()
    else:
        build_info = None
    return build_info

def activate_license(id):
    response = requests.post('https://licence.hydra-software.net/api/rpc/activate_license', data={'id':id, 'mac':get_serial(), 'build_info':get_build()})
    if response.status_code != 200:
        raise LicenceActivationError("error {} : {}\n{}".format(response.status_code, response.json()['message'], response.json()['hint']))
    return response.json()

def save_key(key):
    with open(license_file, 'w') as f:
        f.write(key)

def get_key():
    if check_license():
        with open(license_file, 'r') as f:
            key = f.readlines()[0]
        return key
    else:
        return None

def program_path(program):
    prog_path = which(program)
    if prog_path is None:
        kernel_exe = os.path.join(os.path.abspath(os.path.dirname(__file__)), '..', 'bin', f"{program}.exe")
        if os.path.isfile(kernel_exe):
            prog_path = kernel_exe
    return prog_path

def license_type():
    prog_path = program_path('xeau')
    if prog_path is not None:
        env = os.environ.copy()
        env['EXPRESSEAU_LICENSE'] = license_file
        p = run([prog_path, '-t'], cwd=tempfile.gettempdir(), capture_output=True, env=env)
        return p.stdout.decode('ascii').strip()
    return None

if __name__=='__main__':
    import sys

    key = activate_license(sys.argv[1])
    save_key(key)
