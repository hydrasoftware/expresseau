# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of EXPRESSEAU, a QGIS plugin for hydraulics                          ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


"""
smart access to expresseau binary files

Results implement __getitem__(self, keys) operator
If keys arg is a string or a list of string then access is made
by element name and return a ResultFromElem object.
If keys arg is a int or a list of int or a slice then access is made
by time step number and return a ResultFromTimeStep object.

ResultFromElem and ResultFromTimeStep also implement __getitem__(self, keys) operator
ResultFromElem accept slice, int and list of int to indicates the desired time steps of the selected elements

ResultFromTimeStep accept string, list of string, slice, int and list of int to indicates the desired elements for the selected time steps

Results are return in the following format: [time, [list of 8 floats]]

USAGE

    results = Results(file_path)

    Access by time steps:
        results[0]["NOD_1"]                 --> return the result of "NOD_1" at the first time step
        results[0][["NOD_1", "NOD_2"]]      --> return the result of "NOD_1" and "NOD_2" for the first time step
        results[0:3][["NOD_1", "NOD_2"]]    --> return the result of "NOD_1" and "NOD_2" for the first 3 time steps
        results[[0,5]][["NOD_1", "NOD_2"]]  --> return the result of "NOD_1" and "NOD_2" for the first time step 0 and 5
        results[0][:]                       --> return the result of all elements for the first time step
        results[:][:]                       --> return the result of all elements for all time steps

    Access by elements:
        results["NOD_1"][:]                 --> return the result of all time steps for the element "NOD_1"
        results[["NOD_1", "NOD_2"]][:]      --> return the result of all time steps for the element "NOD_1" and "NOD_2"

"""

import struct
import os
import numpy
import collections
from .string import isint


class Results(object):
    @staticmethod
    def read_binary(file_stream, time_step, i_elem, nb_elem):
        start = (1+nb_elem)*32 + time_step*(nb_elem+1)*32
        file_stream.seek(start)
        time = numpy.frombuffer(file_stream.read(32), dtype=numpy.float32)[0]
        file_stream.seek(32*(i_elem), 1)
        results = numpy.frombuffer(file_stream.read(32), dtype=numpy.float32)
        return [time, results]

    @staticmethod
    def get_slice_limits(_slice, _nb):
        assert(isinstance(_slice, slice))
        assert(isint(_nb))
        start = 0 if (_slice.start is None or _slice.start<0) else _slice.start if _slice.start<_nb else _nb
        stop = _nb if (_slice.stop is None or _slice.stop>_nb) else _slice.stop if _slice.stop>0 else 0
        return start, stop

    class ResultFromElem(object):
        def __init__(self, path, i_elem, nb_step, nb_elem):
            self.__path = path
            self.__i_elem = i_elem
            self.__nb_step = nb_step
            self.__nb_elem = nb_elem

        def __getitem__(self, keys):
            with open(self.__path, 'rb') as f :
                if isinstance(keys, slice):
                    start, stop = Results.get_slice_limits(keys, self.__nb_step)
                    iter_values = list(range(start, stop))
                elif isinstance(keys, int):
                    start = 0 if keys<0 else keys if keys<self.__nb_step else self.__nb_step
                    stop = start+1
                    iter_values = list(range(start, stop))
                elif isinstance(keys, list):
                    iter_values = keys
                else:
                    raise Exception("Key type exception in getitem operator.")

                values = list()
                if isinstance(self.__i_elem, list):
                    for elem in self.__i_elem:
                        for i in iter_values:
                            values.append(Results.read_binary(f, i, elem, self.__nb_elem))
                else:
                    for i in iter_values:
                        values.append(Results.read_binary(f, i, self.__i_elem, self.__nb_elem))
            return values

        def __len__(self):
            return self.__nb_step

    class ResultFromTimeStep(object):
        def __init__(self, path, i_step, nb_step, names_elem):
            self.__path = path
            self.__i_step = i_step
            self.__nb_step = nb_step
            self.__names_elem = names_elem

        def __getitem__(self, keys):
            with open(self.__path, 'rb') as f :
                iter_values = list()
                if isinstance(keys, str):
                    iter_values.append(int(self.__names_elem.index(keys.upper())))
                elif isinstance(keys, int):
                    iter_values = [keys]
                elif isinstance(keys, list):
                    for key in keys:
                        if isinstance(key, int):
                            iter_values.append(key)
                        else:
                            iter_values.append(int(self.__names_elem.index(key.upper())))
                elif isinstance(keys, slice):
                    nb_elem = len(self.__names_elem)
                    start, stop = Results.get_slice_limits(keys, nb_elem)
                    iter_values = list(range(start, stop))
                else:
                    raise Exception("Key type exception in getitem operator.")
                values = list()
                if isinstance(self.__i_step, list):
                    for step in self.__i_step:
                        for i_elem in iter_values:
                            values.append(Results.read_binary(f, step, i_elem, len(self.__names_elem)))
                elif isinstance(self.__i_step, slice):
                    istep_st, istep_ed = Results.get_slice_limits(self.__i_step, self.__nb_step)
                    for istep in range(istep_st, istep_ed):
                        for i_elem in iter_values:
                            values.append(Results.read_binary(f, istep, i_elem, len(self.__names_elem)))
                else:
                    for i_elem in iter_values:
                        values.append(Results.read_binary(f, self.__i_step, i_elem, len(self.__names_elem)))
                return values

        def __len__(self):
            return self.__nb_step

    def __init__(self, file_path):
        self.path = file_path
        with open(self.path, 'rb') as f :
            f.seek(0)
            self.nb_step, self.nb_elem, self.nb_link = numpy.frombuffer(f.read(32), dtype=numpy.int32)[:3]
            self.names = list()
            for i in range(self.nb_elem + self.nb_link):
                self.names.append(b''.join(struct.unpack('s'*24, f.read(24))).strip().upper().decode('utf-8'))
                f.seek(8, 1)

    def __getitem__(self, keys):
        with open(self.path, 'rb') as f :
            if isinstance(keys, slice) or isinstance(keys, int): # from time step
                return Results.ResultFromTimeStep(self.path, keys, self.nb_step, self.names)
            elif isinstance(keys, str): # from elem name
                i_elem = int(self.names.index(keys.upper()))
                return Results.ResultFromElem(self.path, i_elem, self.nb_step, self.nb_elem + self.nb_link)
            elif isinstance(keys, list):
                if len(keys)>0:
                    if isinstance(keys[0], int): # from time step
                        return Results.ResultFromTimeStep(self.path, keys, self.nb_step, self.names)
                    elif isinstance(keys[0], str): # from elem name
                        indices = list()
                        for name in keys:
                            indices.append(int(self.names.index(name.upper())))
                        return Results.ResultFromElem(self.path, indices, self.nb_step, self.nb_elem + self.nb_link)
                    else:
                        raise Exception("Key type exception in getitem operator.")
            else:
                raise Exception("Key type exception in getitem operator.")
            return None

    def __setitem__(self, key, item):
        raise Exception("Set item is not allowed")

if __name__=="__main__":
    # python -m results file
    import sys

    res = Results(sys.argv[1])
