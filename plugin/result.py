from .project import Project
from wstar.result import Result as WSResult

class Result(WSResult):
    def __init__(self, project: str, scenario: str, model: str, only_w14: bool =False):
        project = Project(project)
        super().__init__(project.directory, scenario, model, project.engine, only_w14)
