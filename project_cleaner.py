# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of XPRESSEAU, a QGIS plugin for hydraulics                           ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

"""get rid of useless test projects

USAGE
    expresseau.utility.project_cleaner [-t, -h]
        deletes all prjects ending with "_test"

OPTIONS
    -t, --template
        also delete template_project

    -h, --help
        print this help

    -a, --all
        delete all databases

"""

from __future__ import absolute_import # important to read the doc !
import os
import sys
import getopt
import psycopg2
import shutil
from plugin import database

current_dir = os.path.dirname(__file__)
expresseau_dir = os.path.join(os.path.expanduser('~'), ".expresseau")

def get_directories(a_dir):
    return [name for name in os.listdir(a_dir)
            if os.path.isdir(os.path.join(a_dir, name))]

def remove_project(project_name):
    con = psycopg2.connect(service="expresseau")
    con.set_isolation_level(0)
    cur = con.cursor()

    if project_name in database.get_projects_list():
        try:
            con_project = psycopg2.connect(database=project_name, service="expresseau")
            cur_project = con_project.cursor()
            cur_project.close()
            # close all remaining opened connection to this database if any
            cur.execute("select pg_terminate_backend(pg_stat_activity.pid) \
                        from pg_stat_activity \
                        where pg_stat_activity.datname='"+project_name+"';")
            cur.execute("drop database %s"%(project_name))
            print('Database "', project_name, '" dropped')
        except Exception:
            print("Cannot drop DB {}".format(project_name))

def clean(all_=False):
    for project in database.get_projects_list():
        if (project[-6:] == '_xtest' or all_):
            remove_project(project)
    for directory in get_directories(expresseau_dir):
        if (directory[-6:] == '_xtest' or directory not in database.get_projects_list() or all_):
            print(directory)
            shutil.rmtree(os.path.join(expresseau_dir, directory))

try:
    optlist, args = getopt.getopt(sys.argv[1:],
            "hta",
            ["help", "template", "all"])
except Exception as e:
    sys.stderr.write(str(e)+"\n")
    exit(1)

optlist = dict(optlist)

if "-h" in optlist or "--help" in optlist:
    help(sys.modules[__name__])
    exit(0)

if "-t" in optlist or "--template" in optlist:
    remove_project('template_expresseau')


if "-a" in optlist or "--all" in optlist:
    valid = {"yes": True, "y": True, "ye": True,"no": False, "n": False, '':False}
    sys.stdout.write(f"Are you sure you want to delete all databases on service expresseau {('from '+ os.environ['PGSERVICEFILE']) if 'PGSERVICEFILE' in os.environ else ''}? [y/N]")
    choice = input().lower()
    if choice in valid:
        if valid[choice]:
            clean(all_=True)
    else:
        sys.stdout.write("Answer not clear.\n")
else:
    clean()

