# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


"""
run import_legacy + export on test models

USAGE

   conso_test.py [-dhk] [-l location]

OPTIONS

   -h, --help
        print this help

   -d, --debug
        run in debug mode (lots of output)

   -k, --keep
        keeps database after test

   -l location
        uses location as directory for .dat files

   -r refresh
        instead of creating project from scratch and loading data, only recreates APi schema

"""


import os
import sys
import getopt
import subprocess
from plugin.database import TestProject, project_exists, remove_project
from plugin.project import Project

assert(__name__ == "__main__")

try:
    optlist, args = getopt.getopt(sys.argv[1:],
            "hdkl:",
            ["help", "debug", "keep", "location="])
except Exception as e:
    sys.stderr.write(str(e)+"\n")
    exit(1)

optlist = dict(optlist)

if "-h" in optlist or "--help" in optlist:
    help(sys.modules[__name__])
    exit(0)

if not project_exists(TestProject.NAME):
    sys.stdout.write("creating template database... ")
    TestProject.reset_template()
    sys.stdout.write("ok\n")


debug = "-d" in optlist or "--debug" in optlist
keep = "-k" in optlist or "--keep" in optlist


# test database creation
dbname = "conso_xtest"

if project_exists(dbname):
    remove_project(dbname)
test_project = TestProject(dbname, keep)

project_test = Project(dbname, debug=debug)

if "-l" in optlist:
    test_dir = optlist['-l']
elif "--location" in optlist:
    test_dir = optlist['--location']
else:
    test_dir = os.path.join(os.path.dirname(__file__), 'test_data', 'consos')

for file in os.listdir(test_dir):
    if os.path.isfile(os.path.join(test_dir, file)) and file[-4:] == ".shp":
        cmd = ['ogr2ogr', f'PG:service=expresseau dbname={dbname}', '-lco', 'GEOMETRY_NAME=geom', '-a_srs', 'EPSG:2154', '-nln', file.split('.')[0], os.path.join(test_dir, file)]
        sys.stdout.write(" ".join(cmd) + "\n")
        subprocess.run(cmd)

project_test.execute("update api.metadata set unique_name_per_model = false")
project_test.vacuum_analyse()

project_test.add_new_model('model')
project_test.execute("""insert into api.water_delivery_sector(geom) select ST_Multi((ST_Dump(geom)).geom) from sectors;""")
project_test.execute("""insert into api.user_node(model, name, geom, zground) select 'model', id, ST_SetSRID(ST_MakePoint(ST_X(geom), ST_Y(geom)), (select srid from ___.metadata)), z::real from node;""")
project_test.execute("""insert into api.pipe_link(name, diameter, thickness_mm, geom) select idcana, diametre_m, epaisseur_, (ST_Dump(geom)).geom from cana;""")
project_test.execute("""insert into api.water_delivery_point(name, geom, volume, industrial) select qbano, geom, vol, case when grosconso='OUI' then True else False end from volumes where vol >= 0;""")

project_test.execute("""insert into api.scenario(name) values ('scn');""")

project_test.execute("""select * from api.water_delivery_point;""")
affected = project_test.fetchall("""select * from api._user_node_affected_volume;""")
export_bloc, = project_test.fetchone("""select api._bloc_user_node('scn', 'model');""")

assert(len(affected) >= 2645)
assert(len(export_bloc) > 1000)

# Makes some nodes industrial
project_test.execute("""update api.water_delivery_point set industrial=not mod(volume::integer, 30)::boolean;""")
# Creates wd scenarios for tests
project_test.execute("""insert into api.water_delivery_scenario default values;""")
project_test.execute("""insert into api.water_delivery_scenario default values;""")
project_test.execute("""insert into api.water_delivery_scenario default values;""")
project_test.execute("""insert into api.hourly_modulation_curve default values;""")
project_test.execute("""insert into api.hourly_modulation_curve default values;""")
project_test.execute("""insert into api.hourly_modulation_curve default values;""")


project_test.execute("update api.metadata set unique_name_per_model = true")

sys.stdout.write("ok\n")