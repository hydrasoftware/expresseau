# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


"""
run import_legacy + export on test models

USAGE

   import_export_test.py [-dh] [-l location]

OPTIONS

   -h, --help
        print this help

   -d, --debug
        run in debug mode (lots of output)

   -k, --keep
        keeps database after test

   -l location
        uses location as directory for .dat files

"""

assert(__name__ == "__main__")

import os
import sys
import getopt
from plugin.database import TestProject, project_exists, remove_project
from plugin.project import Project
from plugin.database.import_legacy import import_
from plugin.database.export_calcul import ExportCalcul

try:
    optlist, args = getopt.getopt(sys.argv[1:],
            "hdkl:",
            ["help", "debug", "keep", "location="])
except Exception as e:
    sys.stderr.write(str(e)+"\n")
    exit(1)

optlist = dict(optlist)

if "-h" in optlist or "--help" in optlist:
    help(sys.modules[__name__])
    exit(0)

if not project_exists(TestProject.NAME):
    sys.stdout.write("creating template database... ")
    TestProject.reset_template()
    sys.stdout.write("ok\n")


debug = "-d" in optlist or "--debug" in optlist
keep = "-k" in optlist or "--keep" in optlist

# test database creation
dbname = "import_export_xtest"
if project_exists(dbname):
    remove_project(dbname)
test_project = TestProject(dbname, keep)

project_test = Project(dbname, debug=debug)
project_test.add_new_scenario('SCN_export')

if "-l" in optlist:
    test_dir = optlist['-l']
elif "--location" in optlist:
    test_dir = optlist['--location']
else:
    test_dir = os.path.join(os.path.dirname(__file__), 'test_data', 'GPS')


for model_name in os.listdir(test_dir):
    sys.stdout.write(model_name)
    if os.path.isdir(os.path.join(test_dir, model_name)):
        project_test.delete_model(model_name)
        project_test.add_new_model(model_name)
        with project_test.connect() as con, con.cursor() as cur:
            import_(cur, model_name, os.path.join(os.path.join(test_dir, model_name)))
            con.commit()

exporter = ExportCalcul(project_test)
exporter.export('SCN_export')

sys.stdout.write("ok\n")
