# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of HYDRA, a QGIS plugin for hydraulics                               ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec Hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


"""
run all tests

USAGE
    python -m test_integration [-h, -j nbproc, -l]

OPTIONS
    -h
        print this help

    -j nbproc
        runs nbproc tests in parallel

    -d
        run test in debug

    -l
        list tests instead of running them
"""

import os
import re
import sys
import getopt
import tempfile
from time import time, sleep
from subprocess import Popen, PIPE
from tempfile import gettempdir
from multiprocessing.pool import ThreadPool
from plugin.database import TestProject

def list_tests():
    "return module names for tests"
    tests = []
    dir = os.path.dirname(__file__)
    for file in os.listdir(dir):
        if os.path.isfile(os.path.join(dir, file)):
            if re.match(r".*_test.py$", file):
                test = '.'.join(
                            os.path.abspath(
                                os.path.join(dir, file)
                            ).replace(dir, "test_integration").split(os.sep))[:-3]
                tests.append(test)
    return tests

def run(test):
    start = time()
    process = Popen([sys.executable, "-m", test], stderr=PIPE, stdout=PIPE, encoding='utf-8')
    our, err = process.communicate()
    exit_code = process.returncode
    if exit_code!=0:
        return exit_code, f"{test}\n{err}"
    return  0, f"{test} ran in {time()-start:.2f} sec"

try:
    optlist, args = getopt.getopt(sys.argv[1:],
            "hj:ld",
            ["help"])
except Exception as e:
    sys.stderr.write(str(e)+"\n")
    exit(1)

optlist = dict(optlist)

if "-h" in optlist or "--help" in optlist:
    help(sys.modules[__name__])
    exit(0)

tests = list_tests()

if '-l' in optlist:
    print('\n'.join(tests))
    exit(0)

start = time()

nb_proc = int(optlist['-j']) if '-j' in optlist else len(tests)

tmpdir = tempfile.mkdtemp()

if nb_proc > 1:
    print("start %d processes..."%(nb_proc))

i = 0


for test_group in [tests[i*nb_proc:i*nb_proc+nb_proc] for i in range(len(tests)//nb_proc)]:
    RUNNING_PROCESSES = []
    for test in test_group:
        log = open(os.path.join(tmpdir, test+'.log'), 'w')
        RUNNING_PROCESSES.append((Popen([sys.executable, "-m", test], stderr=log, stdout=log), test, time(), log))

    while len(RUNNING_PROCESSES):
        sleep(.1)
        for p, (process, test, strt, log) in enumerate(RUNNING_PROCESSES):
            if process.poll() is not None:
                process.communicate()
                logfile = log.name
                log.close()
                exit_code = process.returncode
                if exit_code!=0:
                    print("error in",test)
                    print(open(logfile).read())
                    exit(1)
                print("%2d/%d %s after %.2f sec"%(i+1, len(tests), test, time() - strt))
                i += 1
                del RUNNING_PROCESSES[p]
                break

print(f"everything is fine {time()-start:.2f} sec")