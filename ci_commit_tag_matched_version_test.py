from plugin.database.version import __version__
import os

if 'CI_COMMIT_TAG' in os.environ:
    assert os.environ['CI_COMMIT_TAG'] == 'v'+__version__

