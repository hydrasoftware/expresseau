# Privilèges de l'utilisateur expresseau

- createdb pour de nouveaux projets
- pg_signal_backend pour terminer le backend (pour les tests au moins)


# Config dev environment

In order to push some changes, some pre-commit hooks are used to ensure code quality.

```
pip install --user pre-commit
pre-commit install
```

ou

```
sudo apt install pre-commit 
pre-commit install
```

Next, when you make a commit, Git will run the pre-commit hook.

Manually check your changed files:
```
pre-commit run
```

Manually check all files on the repository:
```
pre-commit run --all-files
```

