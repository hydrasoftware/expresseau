# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of XPRESSEAU, a QGIS plugin for hydraulics                           ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################

"""
packaging script for the expresseau project

USAGE
    package.py [-h, -i, -u] [directory],

OPTIONS
    -h, --help
        print this help

    -i, --install [directory]
        install the package in the .qgis2 directory, if directory is ommited,
        install in the QGis plugin directory

    -u, --uninstall
        uninstall (remove) the package from .qgis2 directory

"""

import os
import zipfile
import re
import shutil
import subprocess

qgis_plugin_dir = os.path.abspath(os.path.join(os.path.expanduser('~'), "AppData", "Roaming", "QGIS", "QGIS3", "profiles", "default", "python", "plugins"))
plugin_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), 'plugin'))
expresseau_plugin_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), 'expresseau'))

zipname = "expresseau"
zipext = ".zip"

def uninstall(install_dir):
    target_dir = os.path.join(install_dir, "expresseau")
    __drop(target_dir)
    print(f"Removed plugin from {install_dir}")

def install(install_dir, zip_filename):
    uninstall(install_dir)
    with zipfile.ZipFile(zip_filename, "r") as z:
        z.extractall(install_dir)
    print(f"Installed plugin in {install_dir}")

def zip_(zip_filename):
    '''the zip file doesn't include tests, demos or docs'''
    dir = plugin_dir if os.path.isdir(plugin_dir) else expresseau_plugin_dir
    with zipfile.ZipFile(zip_filename, 'w') as package:
        for root, dirs, files in os.walk(dir, followlinks=True):
            for file_ in files:
                if (re.match(r'CHANGELOG', file_) or re.match(r".*\.(py|ui|qml|txt|png|svg|qgs|sql|json|jpg|exe|dll|ts|qm|xml|html|css|js|whl)$", file_)) and not re.match(r".*_test\.py$", file_):
                    fake_root = root.replace(dir, "expresseau")
                    package.write(os.path.join(root, file_), os.path.join(fake_root, file_))
    print(f"Compressed zip file {zip_filename}")

def __drop(name):
    if os.path.isfile(name):
        os.remove(name)
        print(f"Del file {name}".format(name))
    if os.path.isdir(name):
        shutil.rmtree(name)
        print(f"Del folder {name}".format(name))

if __name__ == "__main__":

    import getopt
    import sys

    try:
        optlist, args = getopt.getopt(sys.argv[1:],
                "hiu",
                ["help", "install", "uninstall"])
    except Exception as e:
        sys.stderr.write(str(e)+"\n")
        exit(1)

    optlist = dict(optlist)

    if "-h" in optlist or "--help" in optlist:
        help(sys.modules[__name__])
        exit(0)

    # translation
    if os.name != 'nt':
        subprocess.run(['python', os.path.join(os.path.dirname(__file__), 'translate.py')])
        subprocess.run(['lrelease', os.path.join(plugin_dir, 'i18n', 'fr.ts'), '-qm', os.path.join(plugin_dir, 'i18n', 'fr.qm')])

    zip_filename = os.path.join(os.path.dirname(__file__), zipname+zipext)
    zip_(zip_filename)

    if "-u" in optlist or "--uninstall" in optlist or "-i" in optlist or "--install" in optlist:
        install_dir = qgis_plugin_dir if len(args)==0 else args[0]

    if "-u" in optlist or "--uninstall" in optlist:
        uninstall(install_dir)

    if "-i" in optlist or "--install" in optlist:
        install(install_dir, zip_filename)
