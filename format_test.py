# coding=utf-8

################################################################################################
##                                                                                            ##
##     This file is part of XPRESSEAU, a QGIS plugin for hydraulics                           ##
##     (see <http://hydra-software.net/>).                                                    ##
##                                                                                            ##
##     Copyright (c) 2017 by HYDRA-SOFTWARE, which is a commercial brand                      ##
##     of Setec hydratec, Paris.                                                              ##
##                                                                                            ##
##     Contact: <contact@hydra-software.net>                                                  ##
##                                                                                            ##
##     You can use this program under the terms of the GNU General Public                     ##
##     License as published by the Free Software Foundation, version 3 of                     ##
##     the License.                                                                           ##
##                                                                                            ##
##     You should have received a copy of the GNU General Public License                      ##
##     along with this program. If not, see <http://www.gnu.org/licenses/>.                   ##
##                                                                                            ##
################################################################################################


"""
check that there are not space ending lines

fix problems with option -f
"""

import os
import re
import sys

expresseau_dir = os.path.abspath(os.path.dirname(__file__))

def run(fix=False, is_cygwin=False):

    whitespaces = 0
    prints = 0

    for root, dirs, files in os.walk(expresseau_dir, followlinks=True):
        for file_ in files:
            if 'wstar' in root.split(os.sep):
                continue
            f = os.path.join(root, file_)
            ferr = f.replace('\\', '/').replace('C:/', '/c/') if is_cygwin else f
            if re.match(r".*\.(py|sql|json)$", file_):
                lines = open(f).readlines()
                err = False
                for i, line in enumerate(lines):
                    if re.match(r'^.* +$', line):
                        sys.stderr.write(f'{ferr}:{i+1} error: line ends with whitespace(s)'+'\n')
                        whitespaces += 1
                        err = True
                if fix and err:
                    open(f, 'w').write("\n".join([ll.rstrip() for ll in lines]))
                    whitespaces = 0 # reset trailing spaces after fix
            if re.match(r".*.(py)$", file_) \
                    and not re.match(r"(package.py|project_cleaner.py|test.py|icons_generator.py|build.py|opengl_layer.py)", file_) and not re.match(r".*(__main__|_test)\.py", file_):
                lines = open(f).readlines()
                inside_main = False
                for i, line in enumerate(lines):
                    if re.match(r"""^if\s+__name__\s*==\s*["']__main__["']\s*:$""", line):
                        inside_main=True
                    if not inside_main and re.match(r'^\s*print[\(\s].*', line) and not re.match(r'^\s*#\s*print\s.*', line):
                        sys.stderr.write(f"""{ferr}:{i+1} error: line starts with print (and not in "main" section")"""+"\n")
                        prints += 1

    if whitespaces > 0 :
        raise SyntaxError(f"WARNING: {whitespaces} lines found with trailing whitespace(s)")
    if prints > 0 :
        raise SyntaxError(f"WARNING: {prints} lines found starting with print")


if __name__=='__main__':
    run(fix=len(sys.argv)==2 and sys.argv[1] == '-f')
    print('ok')
